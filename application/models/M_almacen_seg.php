<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen_seg extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("fecha", "asc");
		$query=$this->db->get('almacen_seg');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen_seg',['idas' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM almacen_seg WHERE idas='$id'");
		return $query->result();
	}
    function get_row($col,$val){
    	$this->db->order_by('fecha','desc');
    	$this->db->order_by('idas','desc');
     	$query=$this->db->get_where('almacen_seg',[$col=>$val]);
      	return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('almacen_seg',[$col=>$val,$col2=>$val2]);
      return $query->result();
    }
	function get_count_active($idu){
		$this->db->select("idu, usuario,max(fecha) as fecha,COUNT(idas) as cantidad");
	    $this->db->from("almacen_seg");
	    $this->db->where("idu != '$idu'");
	    $this->db->group_by("idu");
	    $query=$this->db->get();
	    return $query->result();		
	}
	function insertar($ida,$almacen,$idu,$usuario,$fecha,$accion){
		$datos=array(
			'ida' => $ida,
			'almacen' => $almacen,
			'idu' => $idu,
			'usuario' => $usuario,
			'accion' => $accion,
		);
		if($this->db->insert('almacen_seg',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('almacen_seg',['idas' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_almacen_seg.php */
/* Location: ./application/models/m_almacen_seg.php*/