<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_vario extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_vario');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_vario',['idmv' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_vario WHERE idmv='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('material_vario',[$col => $val]);
		return $query->result();
	}
	function insertar($idmi){
		$datos=array(
			'idmi' => $idmi
		);
		if($this->db->insert('material_vario',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('material_vario',$datos,array('idmv'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_vario',['idmv' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_material_vario.php */
/* Location: ./application/models/m_material_vario.php*/