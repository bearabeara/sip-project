<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_actualizacion extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('actualizacion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('actualizacion',['idac' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM actualizacion WHERE idac='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('actualizacion',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val,$order_by,$order){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("actualizacion");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="idac" || $col=="fecha_inicio" || $col=="fecha_fin" || $col=="estado"){
				$this->db->where("$col = '$val'");
			}
		}
		if($order_by!="" && $order_by!=NULL && $order_by!=false && $order!="" && $order!=NULL && ($order=="DESC" || $order=="ASC" || $order=="desc" || $order=="asc")){
			$this->db->order_by($order_by,$order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idac,$fecha_inicio,$fecha_fin,$descripcion,$estado){
		$datos=array(
			'idac' => $idac,
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => $fecha_fin,
			'descripcion' => $descripcion,
			'estado' => $estado
		);
		if($this->db->insert('actualizacion',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fecha_inicio,$fecha_fin,$descripcion,$estado){
		$datos=array(
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => $fecha_fin,
			'descripcion' => $descripcion,
			'estado' => $estado
		);
		if($this->db->update('actualizacion',$datos,array('idac' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('actualizacion',$datos,array('idac' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('actualizacion',['idac' => $id])){
			return true;
		}else{
			return false;
		}
	}
	  function max_col($col){
	    $query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM actualizacion");
	    $result=$query->result();
	    return $result[0]->max*1;
	  } 
}
/* End of file m_actualizacion.php */
/* Location: ./application/models/m_actualizacion.php*/