<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_atributo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('atributo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('atributo',['idatr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM atributo WHERE idatr='$id'");
		return $query->result();
	}
	function get_search($col,$val){
		$this->db->select("*");
		$this->db->from("atributo");
		if($col!="" && $val!=""){
			if($col=="idatr"){
				$this->db->where("$col = '$val'");
			}
			if($col=="atributo"){
				$this->db->where("$col like '$val%'");
			}
		}
		$this->db->order_by("atributo", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($atributo){
		$datos=array('atributo' => $atributo);
		if($this->db->insert('atributo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$atributo){
		$datos=array('atributo' => $atributo);
		if($this->db->update('atributo',$datos,array('idatr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('atributo',['idatr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_atributo.php */
/* Location: ./application/models/m_atributo.php*/