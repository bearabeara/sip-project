<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_sucursal_cliente extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nit", "asc");
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('sucursal_cliente');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('sucursal_cliente',['idsc' => $id]);
		return $query->result();
	}
	function get_row($col,$val){//en uso, Clientes, 
		$this->db->order_by("nit", "asc");
		$this->db->order_by("nombre", "asc");
      	$query=$this->db->get_where('sucursal_cliente',[$col => $val]);
      return $query->result();
    }
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM sucursal_cliente WHERE idsc='$id'");
		return $query->result();
	}
	function get_search($col,$val){
		$this->db->select("*");
		$this->db->from("sucursal_cliente");
		if($col!="" && $val!=""){
			if($col=="idsc" || $col=="idcl"){
				$this->db->where("$col = '$val'");
			}
			if($col=="nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->order_by("nit", "asc");
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_complet($col,$val){
		$this->db->select("sc.idsc, sc.fotografia as fotografia_sucursal, sc.nit as nit_sucursal, sc.nombre as nombre_sucursal, sc.responsable, sc.telefono as telefono_sucursal, sc.email, sc.direccion,
			c.idcl,c.nit,c.razon,c.gerente,c.telefono,c.url,c.fotografia,c.observacion");
		$this->db->from("sucursal_cliente sc");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="sc.idsc" || $col=="c.idcl"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('cliente c','c.idcl = sc.idcl','inner');
		$this->db->order_by("sc.nit", "asc");
		$this->db->order_by("sc.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idcl,$nit,$nombre,$responsable,$telefono,$email,$direccion,$fotografia){
		$datos=array(
			'idcl' => $idcl,
			'nit' => $nit,
			'nombre' => $nombre,
			'responsable' => $responsable,
			'telefono' => $telefono,
			'email' => $email,
			'direccion' => $direccion,
			'fotografia' => $fotografia
		);
		if($this->db->insert('sucursal_cliente',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nit,$nombre,$responsable,$telefono,$email,$direccion,$fotografia){
		$datos=array(
			'nit' => $nit,
			'nombre' => $nombre,
			'responsable' => $responsable,
			'telefono' => $telefono,
			'email' => $email,
			'direccion' => $direccion,
			'fotografia' => $fotografia
		);
		if($this->db->update('sucursal_cliente',$datos,array('idsc' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('sucursal_cliente',['idsc' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_sucursal_cliente.php */
/* Location: ./application/models/m_sucursal_cliente.php*/