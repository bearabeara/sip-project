<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_atributo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('producto_atributo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_atributo',['idpatr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_atributo WHERE idpatr='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_atributo',[$col => $val]);
		return $query->result();
	}
	function get_atributo($col,$val){
		$cols="a.idatr, a.atributo,
			pa.idpatr, pa.idp, pa.valor";
		$this->db->select($cols);
		$this->db->from("producto_atributo pa");
		$this->db->join('atributo a','a.idatr = pa.idatr','inner');
		$this->db->where("$col = '$val'");
		$this->db->order_by("a.atributo", "asc");
		$query=$this->db->get();
		return $query->result();
	}

	function insertar($idp,$idatr,$valor){
		$datos=array(
			'idp' => $idp,
			'idatr' => $idatr,
			'valor' => $valor
		);
		if($this->db->insert('producto_atributo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$valor){
		$datos=array(
			'valor' => $valor
		);
		if($this->db->update('producto_atributo',$datos,array('idpatr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_atributo',['idpatr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_producto_atributo.php */
/* Location: ./application/models/m_producto_atributo.php*/