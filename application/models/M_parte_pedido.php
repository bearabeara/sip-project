<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_parte_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('parte_pedido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('parte_pedido',['idpp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM parte_pedido WHERE idpp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by("tipo","asc");
		$this->db->order_by("numero","asc");
		$query=$this->db->get_where('parte_pedido',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){
		$this->db->select("*");
		$this->db->from("parte_pedido");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="idpe" || $col=="idpp"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->order_by("tipo","asc");
		$this->db->order_by("numero","asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_complet($col,$val){
		$c_descuento="`".$this->lib->encriptar_str("descuento")."`";
		$cols="pp.idpp,pp.numero,pp.fecha, pp.tipo,pp.observacion as observacion_parte,
			p.idpe,p.idcl,p.nombre,p.".$c_descuento." as descuento,p.observacion as observacion_pedido";
		$this->db->select($cols);
		$this->db->from("parte_pedido pp");
		if($col!="" && $val!=""){
			if($col=="p.idpe" || $col=="pp.idpp"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('pedido p','p.idpe = pp.idpe','inner');
		$this->db->order_by("pp.idpp", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_detalle($col,$val){
		$c_cu="`".$this->lib->encriptar_str("cu")."`";
		$c_cv="`".$this->lib->encriptar_str("cv")."`";
		$cols="pp.idpp,pp.idpe,pp.numero,pp.fecha, pp.tipo,pp.observacion as observacion_parte,
			dp.iddp, dp.idpp, dp.idpgrc,dp.posicion,dp.observacion, dp.porcentaje, dp.".$c_cu." as cu, dp.".$c_cv." as cv";
		$this->db->select($cols);
		$this->db->from("parte_pedido pp");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="pp.idpe" || $col=="pp.idpp" || $col=="dp.iddp"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('detalle_pedido dp','dp.idpp = pp.idpp','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpp,$idpe,$numero,$fecha,$tipo,$observacion){
		$datos=array(
			'idpp' => $idpp,
			'idpe' => $idpe,
			'numero' => $numero,
			'fecha' => $fecha,
			'tipo' => $tipo,
			'observacion' => $observacion
		);
		if($this->db->insert('parte_pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$numero,$fecha,$tipo,$observacion){
		$datos=array(
			'numero' => $numero,
			'fecha' => $fecha,
			'tipo' => $tipo,
			'observacion' => $observacion
		);
		if($this->db->update('parte_pedido',$datos,array('idpp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row_2n($id,$col,$val,$col2,$val2){
		$datos=array( $col => $val, $col2 => $val2 );
		if($this->db->update('parte_pedido',$datos,array('idpp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('parte_pedido',['idpp' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM parte_pedido");
		$max=$query->result();
		return $max[0]->max*1;
	}
	function max_where($col,$where){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM parte_pedido WHERE $where");
		$max=$query->result();
		return $max[0]->max*1;
	}
}

/* End of file m_parte_pedido.php */
/* Location: ./application/models/m_parte_pedido.php*/