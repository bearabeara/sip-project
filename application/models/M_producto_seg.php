<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_seg extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_seg');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_seg',['idps' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_seg WHERE idps='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by('fecha','desc');
		$this->db->order_by('idps','desc');
		$query=$this->db->get_where('producto_seg',array($col => $val));
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$this->db->order_by('fecha','desc');
		$this->db->order_by('idps','desc');
		$query=$this->db->get_where('producto_seg',array($col => $val,$col2 => $val2));
		return $query->result();
	}
	function get_producto($idp,$type){
		$this->db->select("*");
		$this->db->from("producto_seg");
		$this->db->where("idp = '$idp'");
		switch ($type){
			case 'producto': $this->db->where("accion IN ('c','d','u','cm','dm','um','cf','df','uf','cp','dp','up')"); break;
			case 'producto_fotografia': $this->db->where("accion IN ('cf','df','uf')"); break;
			case 'producto_material': $this->db->where("accion IN ('cm','dm','um')"); break;
			case 'producto_pieza': $this->db->where("accion IN ('cp','dp','up')"); break;
		}
		$this->db->order_by('fecha','desc');
		$this->db->order_by('idps','desc');
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){// en uso: PROVEEDOR,PRODUCTO
		$cols="mi.idps,mi.codigo,mi.nombre,mi.descripcion,
			m.idm,m.idco,m.costo_unitario,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_seg mi");
		if($col!="" && $val!=""){
			if($col=="mi.idps"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('material m','m.idps=mi.idps','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idp,$producto,$accion,$msj_adicional,$idu,$usuario){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'idp' => $idp,
			'producto' => $producto,
			'accion' => $accion,
			'msj_adicional' => $msj_adicional,
			'idu' => $idu,
			'usuario' => $usuario,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('producto_seg',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idps,$idu,$codigo,$nombre,$descripcion){
		$datos=array(
			'idu' => $idu,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'descripcion' => $descripcion
		);
		if($this->db->update('producto_seg',$datos,array('idps'=>$idps))){
			return true;
		}else{
			return false;
		}
	}
	function null($col,$val){
		$datos=array( $col => NULL );
		if($this->db->update('producto_seg',$datos,array($col => $val))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_seg',['idps' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){// en uso, ACTIVO FIJO,
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_seg");
		$max=$query->result();
		return $max[0]->max*1;
	}
	function max_where($atrib,$col,$val){// en uso
		$this->db->select("*");
		$this->db->from("producto_seg");
		$this->db->where("$col = '$val'");
		$this->db->order_by("$atrib","desc");
		$this->db->limit(5);
		$query=$this->db->get();
		return $query->result();
	}
}

/* End of file m_producto_seg.php */
/* Location: ./application/models/m_producto_seg.php*/