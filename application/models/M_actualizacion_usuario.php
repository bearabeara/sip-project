<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_actualizacion_usuario extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('actualizacion_usuario');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('actualizacion_usuario',['idau' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM actualizacion_usuario WHERE idau='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('actualizacion_usuario',[$col => $val]);
		return $query->result();
	}
	function get_usuario($col,$val,$order_by,$order){
		$cols="a.idau, a.idac,
			u.idus,u.usuario,u.tipo,
			p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo";
		$this->db->select($cols);
		$this->db->from("actualizacion_usuario a");
		$this->db->join('usuario u','u.idus = a.idus','inner');
		$this->db->join('persona p','p.ci = u.ci','inner');
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="a.idau" || $col=="a.idac" || $col=="p.ci"){
				$this->db->where("$col = '$val'");
			}
			if($col=="p.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("a.idau", "asc");
		}else{
			if($col=='nombre_completo'){ 
				$this->db->order_by("nombre_completo", $order);
			}else{
				$this->db->order_by($order_by, $order);
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_actualizacion($col1,$val1,$col2,$val2,$order_by,$order){
		$cols="au.idau, au.idus,
			ac.idac, ac.descripcion, ac.fecha_inicio, ac.fecha_fin, ac.estado";
		$this->db->select($cols);
		$this->db->from("actualizacion_usuario au");
		$this->db->join('actualizacion ac','ac.idac = au.idac','inner');
		if($col1!="" && $val1!="" && $col1!=NULL && $val1!=NULL){
			if($col1=="au.idau" || $col1=="au.idus" || $col1=="ac.idac" || $col1=="ac.fecha_inicio" || $col1=="ac.fecha_fin" || $col1=="ac.estado"){
				$this->db->where("$col1 = '$val1'");
			}
		}
		if($col2!="" && $val2!="" && $col2!=NULL && $val2!=NULL){
			if($col2=="au.idau" || $col2=="au.idus" || $col2=="ac.idac" || $col2=="ac.fecha_inicio" || $col2=="ac.fecha_fin" || $col2=="ac.estado"){
				$this->db->where("$col2 = '$val2'");
			}
		}
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("au.idau", "asc");
		}else{
			$this->db->order_by($order_by, $order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idac,$idus){
		$datos=array(
			'idac' => $idac,
			'idus' => $idus
		);
		if($this->db->insert('actualizacion_usuario',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpa,$nombre,$abreviatura){
		$datos=array(
			'idpa' => $idpa,
			'nombre' => $nombre,
			'abreviatura' => $abreviatura
		);
		if($this->db->update('actualizacion_usuario',$datos,array('idau' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('actualizacion_usuario',['idau' => $id])){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file m_actualizacion_usuario.php */
/* Location: ./application/models/m_actualizacion_usuario.php*/