<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen_material_seg extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("fecha", "asc");
		$query=$this->db->get('almacen_material_seg');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen_material_seg',['idams' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM almacen_material_seg WHERE idams='$id'");
		return $query->result();
	}
    function get_row($col,$val){
    	$this->db->order_by('fecha','desc');
    	$this->db->order_by('idams','desc');
      $query=$this->db->get_where('almacen_material_seg',[$col=>$val]);
      return $query->result();
    }
	function insertar($ida,$almacen,$idm,$material,$idu,$usuario,$fecha,$accion){
		$datos=array(
			'ida' => $ida,
			'almacen' => $almacen,
			'idm' => $idm,
			'material' => $material,
			'idu' => $idu,
			'usuario' => $usuario,
			'accion' => $accion
		);
		if($this->db->insert('almacen_material_seg',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('almacen_material_seg',['idams' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_almacen_material_seg.php */
/* Location: ./application/models/m_almacen_material_seg.php*/