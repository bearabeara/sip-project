<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_grupo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo',['idpgr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo WHERE idpgr='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo',[$col => $val]);
		return $query->result();
	}
	function get_complet($col,$val){
		$cols="p.idp,p.codigo,p.codigo_aux,p.nombre as nombre_p,p.fecha_creacion,p.observaciones, p.portada,
			pg.idpgr,
			g.idgr, g.nombre as nombre_g, g.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");
		$this->db->join('producto p','p.idp = pg.idp','inner');
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		if($col!="" && $val!=""){ $this->db->where("$col = '$val'"); }
		$this->db->order_by("g.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	/*function get_grupo_color($col,$val){
		$cols="pgc.idpgrc,pgc.idco,pgc.idpgr, pgc.costo, pgc.portada,
			c.idgr,c.nombre,c.abr,
			pg.idpgr,pg.idp,
			g.idgr,g.nombre,g.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");
		$this->db->join('producto_grupo_color pgc','pg.idpgr = pgc.idpgr','inner');
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		if($col!="" && $val!=""){ $this->db->where("$col = '$val'"); }
		$this->db->order_by("g.nombre", "asc");
		$this->db->order_by("pgc.idpgrc", "asc");
		$query=$this->db->get();
		return $query->result();
	}*/
	function get_grupo_colores($col,$val){
		$cols="pg.idpgr,pg.idp,
			g.idgr,g.nombre as nombre_g, g.abr as abr_g,
			pgc.idpgrc,pgc.idpgr, pgc.costo, pgc.portada,
			c.idco,c.nombre as nombre_c,c.abr as abr_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');		
		$this->db->join('producto_grupo_color pgc','pg.idpgr = pgc.idpgr','inner');
		$this->db->join('color c','c.idco = pgc.idco','inner');
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="pg.idpgr" || $col=="pg.idp" || $col=="g.idgr" || $col=="pgc.idpgrc"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->order_by("g.nombre", "asc");
		$this->db->order_by("c.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_grupo($col,$val){
		$cols="pg.idpgr,pg.idp,
			g.idgr,g.nombre,g.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			$this->db->where("$col = '$val'");
		}
		$this->db->order_by("g.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_color($col,$val){
		$cols="pg.idpgr,pg.idp,
			g.idgr,g.nombre,g.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		if($col!="" && $val!=""){ $this->db->where("$col = '$val'"); }
		$this->db->order_by("g.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_colores($col,$val){
		$cols="pg.idpgr,pg.idp,
			pgc.idpgrc,pgc.idpgr, pgc.costo, pgc.portada,
			c.idco,c.nombre,c.abr, c.codigo";
		$this->db->select($cols);
		$this->db->from("producto_grupo pg");	
		$this->db->join('producto_grupo_color pgc','pg.idpgr = pgc.idpgr','inner');
		$this->db->join('color c','c.idco = pgc.idco','inner');
		if($col!="" && $val!=""){ $this->db->where("$col = '$val'"); }
		$this->db->order_by("c.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpgr,$idp,$idgr){
		$datos=array(
			'idpgr' => $idpgr,
			'idp' => $idp,
			'idgr' => $idgr
		);
		if($this->db->insert('producto_grupo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idgr){
		$datos=array(
			'idgr' => $idgr
		);
		if($this->db->update('producto_grupo',$datos,array('idpgr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo',['idpgr' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_grupo");
		$max=$query->result();
		return $max[0]->max*1;
	}
}

/* End of file m_producto_grupo.php */
/* Location: ./application/models/m_producto_grupo.php*/