<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_unidad extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
	    $this->db->order_by("control","desc");
	    $this->db->order_by("nombre","asc");
		$query=$this->db->get('unidad');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('unidad',['idu' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM unidad WHERE idu='$id'");
		return $query->result();
	}
	function get_all_use_insumo(){
		$query=$this->db->query("SELECT idu, nombre, abreviatura,equivalencia, (SELECT count(*) FROM material WHERE material.idu=unidad.idu)as nro FROM unidad");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('unidad',array($col => $val));
		return $query->result();
	}
	function insertar($nombre,$abr,$equ,$descripcion_equ){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr,
			'equ' => $equ,
			'descripcion_equ' => $descripcion_equ 
		);
		if($this->db->insert('unidad',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre, $abr,$equ,$descripcion_equ){
		$datos=array(
			'nombre' => $nombre,
			'abr' => $abr,
			'equ' => $equ,
			'descripcion_equ' => $descripcion_equ 
		);
		if($this->db->update('unidad',$datos,array('idu'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('unidad',['idu' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_unidad.php */
/* Location: ./application/models/m_unidad.php*/