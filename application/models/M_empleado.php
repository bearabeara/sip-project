<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_empleado extends CI_Model{
	function __construct(){
        parent::__construct();
    }
    function get_all(){
        $query=$this->db->get('empleado');
        return $query->result();
    }
    function get($ide){
        $query=$this->db->get_where('empleado',array('ide' => $ide));
        return $query->result();
    }
    function get_row($col,$val){
      $query=$this->db->get_where('empleado',[$col => $val]);
      return $query->result();
    }
    function get_empleado($col,$val,$order_antiguedad,$estado){//EN USO: CAPITAL HUMANO
        $cols="e.ide,e.idtc,e.codigo,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.direccion,e.estado, e.grado_academico,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato,tc.principal,tc.descripcion as descripcion_tc";
        $this->db->select($cols);
        $this->db->from("empleado e");
        $this->db->join("persona p","p.ci = e.ci","inner");
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
        $this->db->join('tipo_contrato tc','tc.idtc = e.idtc','inner');
        if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
            if($col=="e.ide" || $col=='e.tipo' || $col=='e.idtc'){ $this->db->where("$col = '$val'");}
            if($col=='e.codigo' || $col=='p.ci' || $col=='p.telefono' || $col=='e.salario'){ $this->db->where("$col like '$val%'");}
            if($col=='nombre_completo'){ $this->db->where("CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) like '%$val%'");}
        }
        if($estado!="" && $estado!=NULL){$this->db->where("e.estado = '$estado'");}
        if($order_antiguedad){ $this->db->order_by("e.fecha_ingreso","asc");}
        $this->db->order_by("p.nombre");
        $query=$this->db->get();
        return $query->result();
    }
    function insertar($idtc,$ci,$codigo,$salario,$fecha_ingreso,$fecha_nacimiento,$tipo,$direccion,$grado_academico){//en uso:capital humano
        $data = array(
          'idtc' => $idtc,
          'ci' => $ci,
          'codigo' => $codigo,
          'salario' => $salario,
          'fecha_ingreso' => $fecha_ingreso,
          'fecha_nacimiento' => $fecha_nacimiento,
          'tipo' => $tipo,
          'direccion' => $direccion,
          'grado_academico' => $grado_academico,
          'estado' => '1'
        );
       if ($this->db->insert('empleado', $data)){
          return true;
       }else{
          return false;
       }
    }
    function modificar($id,$idtc,$ci,$codigo,$salario,$fecha_ingreso,$fecha_nacimiento,$tipo,$direccion,$grado_academico){//en uso:capital humano,  
        $data = array(
          'idtc' => $idtc,
          'ci' => $ci,
          'codigo' => $codigo,
          'salario' => $salario,
          'fecha_ingreso' => $fecha_ingreso,
          'fecha_nacimiento' => $fecha_nacimiento,
          'tipo' => $tipo,
          'direccion' => $direccion,
          'grado_academico' => $grado_academico,
          'estado' => '1'
        );
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }
    function modificar_row($id,$atrib,$val){//en uso:capital humano,  
        $data = array( $atrib => $val );
        if ($this->db->update('empleado', $data, array('ide' => $id))){
          return true;
        }else{
          return false;
        }
    }

    
    public function eliminar($id){
        if($this->db->delete('empleado',['ide'=>$id])){
            return true;
        }else{
            return false;
        }
    }













  /* function get_all(){// en uso: capital humano,
      $cols="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,c_descuento,e.tipo,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->join("tipo_contrato tc ","e.idtc=tc.idtc","inner");
      $this->db->where("e.tipo <= 1");
      $this->db->order_by("e.fecha_ingreso","asc");
      $this->db->order_by("p.nombre");
      $query=$this->db->get();
      return $query->result();
    }

    function get_complet($ide){// en uso: VENTAS->REPORTES,PLANILLA
      $cols="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,c_descuento,e.tipo,e.admin_sistema,e.usuario,e.c_descuento,e.estado,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad, ci.abreviatura,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($cols);
      $this->db->from("empleado e");
      $this->db->where("e.ide = '$ide'");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->join("tipo_contrato tc ","e.idtc = tc.idtc","inner");
      $query=$this->db->get();
      return $query->result();
    }




    function get_ci($ci){
      $query=$this->db->get_where('empleado',['ci' => $ci]);
       return $query->result();
    }
    function get_tc(){//en uso CAPITAL HUMANO,PRODUCCION,TALLER
      $col="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_feriado,e.c_descuento,e.estado,
          p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
          pa.idpa,pa.nombre as pais,
          ci.idci,ci.nombre as ciudad,
          tc.idtc,tc.horas,tc.tipo as tipo_hora,tc.principal,tc.descripcion";
    	$this->db->select($col);
    	$this->db->from("empleado e");
    	$this->db->join("tipo_contrato tc ","e.idtc=tc.idtc","inner");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->where("e.tipo <= 1");
      $this->db->where("e.tipo >= 0");
      $this->db->order_by("p.nombre");
    	$query=$this->db->get();
    	return $query->result();
    }

    function get_tc_id($ide){//en uso: CAPITAL HUMANO,
       $col="e.ide,e.codigo,e.nombre2,e.paterno,e.materno,e.cargo,e.formacion,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.tipo,e.c_feriado,e.c_descuento,e.estado,
            p.ci,p.nombre,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,
            tc.idtc,tc.horas,tc.tipo as tipo_contrato";
      $this->db->select($col);
      $this->db->from("empleado e");
      $this->db->where("e.ide = '$ide'");
      $this->db->where("e.tipo <= 2");
      $this->db->join("tipo_contrato tc","e.idtc=tc.idtc","inner");
      $this->db->join("persona p","e.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $query=$this->db->get();
      return $query->result();
    }

*/
}

/* End of file m_empleado.php */
/* Location: ./application/models/m_empleado.php */