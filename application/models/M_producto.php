<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto',['idp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto WHERE idp='$id'");
		return $query->result();
	}
	function get_search($col,$val,$estado){
		$cols="idp, codigo, codigo_aux, nombre, fecha_creacion, observaciones,portada,(IF ( codigo REGEXP('[0-9]'),IF(CONVERT(SUBSTR(codigo FROM 2) , SIGNED)!=0,CONVERT(SUBSTR(codigo FROM 2) ,SIGNED),IF(CONVERT(SUBSTR(codigo FROM 3) , SIGNED)!=0,CONVERT(SUBSTR(codigo FROM 3) , SIGNED),IF(CONVERT(SUBSTR(codigo FROM 4) , SIGNED)!=0,CONVERT(SUBSTR(codigo FROM 4) , SIGNED),NULL))), NULL )) as numero, estado";
		$where="";
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="fecha_creacion" || $col=="idp"){
				$where.=$col." = '".$val."'";
			}else{
				if($col=="nombre" || $col=="codigo" || $col=="codigo_aux"){
					$where.=$col." like '%".$val."%'";
				}
			}
		}
		if($estado!="" && $estado!=NULL){ if($where!=""){$where.=" AND";}$where.=" estado = '".$estado."'";}
		if($where!=""){ $where="WHERE ".$where;}
		$query=$this->db->query("SELECT $cols FROM producto ".$where." order by codigo, numero");
		//$query=$this->db->query("SELECT $cols FROM producto ".$where." codigo_aux");
		return $query->result();
	}
	function get_colores($col,$val,$estado){
		$cols="p.idp, p.codigo, p.codigo_aux, p.nombre, p.fecha_creacion, p.observaciones,p.portada as portada_producto,(IF ( p.codigo REGEXP('[0-9]'),IF(CONVERT(SUBSTR(p.codigo FROM 2) , SIGNED)!=0,CONVERT(SUBSTR(p.codigo FROM 2) ,SIGNED),IF(CONVERT(SUBSTR(p.codigo FROM 3) , SIGNED)!=0,CONVERT(SUBSTR(p.codigo FROM 3) , SIGNED),IF(CONVERT(SUBSTR(p.codigo FROM 4) , SIGNED)!=0,CONVERT(SUBSTR(p.codigo FROM 4) , SIGNED),NULL))), NULL )) as numero, p.estado,
			pg.idpgr,pg.idgr,
			g.idgr,g.nombre as nombre_g, g.abr as abr_g,
			pgc.idpgrc,pgc.idpgr, pgc.costo, pgc.portada,
			c.idco,c.nombre as nombre_c,c.codigo as codigo_c,c.abr as abr_c";
		$where="";
		if($col!="" && $val!=""){
			if($col=="fecha_creacion"){
				$where.=$col." = '".$val."'";
			}else{
				if($col=="p.idp" || $col=="pgc.idpgrc"){
					$where.=$col." = '".$val."'";
				}else{
					if($col=="codigo" || $col=="codigo_aux"){
						$where.=$col." like '".$val."%'";
					}else{
						if($col=="p.nombre"){
							$where.=$col." like '%".$val."%'";
						}
					}
				}
			}
		}
		if($estado!="" && $estado!=NULL){ if($where!=""){$where.=" AND";}$where.=" p.estado = '".$estado."'";}
		if($where!=""){ $where="WHERE ".$where;}
		if($where!=""){ $where.=" AND";}else{ $where="WHERE";}
		$query=$this->db->query("SELECT $cols FROM producto p, producto_grupo pg, producto_grupo_color pgc, color c, grupo g ".$where." p.idp=pg.idp AND pg.idpgr=pgc.idpgr AND pg.idgr=g.idgr AND pgc.idco=c.idco order by p.codigo, numero, g.nombre, c.nombre");
		return $query->result();
	}
	function insertar($idp,$codigo,$nombre,$fecha_creacion,$observaciones,$codigo_aux){
		$datos=array(
			'idp' => $idp,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fecha_creacion' => $fecha_creacion,
			'observaciones' => $observaciones,
			'codigo_aux' => $codigo_aux,
			'estado' => '1'
		);
		if($this->db->insert('producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$codigo,$nombre,$fecha_creacion,$observaciones,$codigo_aux){
		$datos=array(
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fecha_creacion' => $fecha_creacion,
			'observaciones' => $observaciones,
			'codigo_aux' => $codigo_aux
		);
		if($this->db->update('producto',$datos,array('idp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('producto',$datos,array('idp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto',['idp' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto");
		$max=$query->result();
		return $max[0]->max*1;
	}
	
}

/* End of file m_producto.php */
/* Location: ./application/models/m_producto.php*/