<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_mensaje extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('mensaje');
		return $query->result();
	}
	function get($idm){
		$query=$this->db->get_where('mensaje',['idm' => $idm]);
		return $query->result();
	}
	function get_row($col,$val){// en uso en chat
		$query=$this->db->get_where('mensaje',[$col => $val,'status' => '1']);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){// en uso en chat
		$query=$this->db->get_where('mensaje',[$col => $val,$col2 => $val2, 'status' => '1']);
		return $query->result();
	}
	function get_col($idm,$col){
		$query=$this->db->query("SELECT $col FROM mensaje WHERE idm='$idm'");
		return $query->result();
	}
	function get_mensajes($emisor,$reseptor){
		$query=$this->db->query("SELECT * from (SELECT * FROM mensaje WHERE (emisor='$emisor' or emisor='$reseptor') and (reseptor='$emisor' or reseptor='$reseptor') order by idm desc limit 12) as elemento order by elemento.fecha asc");
		return $query->result();
	}
	function insertar($emisor,$reseptor,$fecha,$mensaje,$status){
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'fecha' => $fecha,
			'mensaje' => $mensaje,
			'status' => $status
		);
		if($this->db->insert('mensaje',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idm,$nombre,$codigo){
		$datos=array(
			'mensaje' => $mensaje,
			'timestamp' => $timestamp,
			'status' => $status,
			'tipo' => $tipo
		);
		if($this->db->update('mensaje',$datos,array('idm' => $idm))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_mensajes_leidos($emisor,$reseptor){
		$datos=array('status' => '0');
		if($this->db->update('mensaje',$datos,array('emisor' => $emisor,'reseptor' => $reseptor))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($idm){
		if($this->db->delete('mensaje',['idm' => $idm])){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file M_mensaje.php */
/* Location ./application/models/M_mensaje.php*/