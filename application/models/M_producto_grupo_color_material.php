<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_color_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_grupo_color_material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_color_material',['idpgcm' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_color_material WHERE idpgcm='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo_color_material',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_grupo_color_material',[$col => $val, $col2 => $val2]);
		return $query->result();
	}
	function get_material($col,$val){
		$cols="pgcm.idpgcm,pgcm.idpgrc,pgcm.cantidad,pgcm.verificado,pgcm.observacion,
			m.idm, m.idmi, m.idmg, m.idco, m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color_material pgcm");
		$this->db->join('material m','pgcm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		if($col!="" && $col!=NULL && $val!="" && $val!=NULL){
			$this->db->where("$col = '$val'");
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_search_grupo_color($idpgrc,$col,$val){// en uso: PRODUCTO
		$cols="pgcm.idpgcm,pgcm.idpgrc,pgcm.cantidad,pgcm.verificado,pgcm.observacion,
			m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c,c.abr as abr_c,
			u.idu, u.nombre as nombre_u,u.abr as abr_u";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color_material pgcm");
		if($col!="" && $val!=""){
			if($col=="mi.idmi" || $col=="pgcm.idpgcm" || $col=="pgcm.idpgrc"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->where("pgcm.idpgrc = '$idpgrc'");
		$this->db->join('material m','pgcm.idm = m.idm','inner');
		$this->db->join('material_item mi','m.idmi = mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_grupo($col,$val){
		$cols="pgcm.idpgcm,pgcm.idm,pgcm.cantidad,pgcm.verificado,pgcm.observacion,
			pgc.idpgrc, pgc.idpgr, pgc.costo, pgc.portada,
			c.idco,c.nombre as nombre_c,c.codigo as codigo_c,c.abr as abr_c,
			pg.idpgr,pg.idp,
			g.idgr,g.nombre as nombre_g,g.abr as abr_g";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color_material pgcm");
		if($col!="" && $val!=""){
			if($col=="pgcm.idm"){
				$this->db->where("$col = '$val'");
			}
		}
		$this->db->join('producto_grupo_color pgc','pgc.idpgrc = pgcm.idpgrc','inner');
		$this->db->join('color c','c.idco = pgc.idco','inner');
		$this->db->join('producto_grupo pg','pg.idpgr = pgc.idpgr','inner');
		$this->db->join('grupo g','g.idgr = pg.idgr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpgrc,$idm,$cantidad,$observacion){
		$datos=array(
			'idpgrc' => $idpgrc,
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion
		);
		if($this->db->insert('producto_grupo_color_material',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$cantidad,$verificado,$observacion){
		$datos=array(
			'cantidad' => $cantidad,
			'verificado' => $verificado,
			'observacion' => $observacion
		);
		if($this->db->update('producto_grupo_color_material',$datos,array('idpgcm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar2($id,$idm,$cantidad,$observacion){
		$datos=array(
			'idm' => $idm,
			'cantidad' => $cantidad,
			'observacion' => $observacion
		);
		if($this->db->update('producto_grupo_color_material',$datos,array('idpgcm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_color_material',['idpgcm' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_2n($col,$val,$col2,$val2){
		if($this->db->delete('producto_grupo_color_material',[$col => $val, $col2 => $val2])){
			return true;
		}else{
			return false;
		}
	}
	
}

/* End of file m_producto_grupo_color_material.php */
/* Location: ./application/models/m_producto_grupo_color_material.php*/