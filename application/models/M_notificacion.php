<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_notificacion extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('notificacion');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('notificacion',['idno' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM notificacion WHERE idno='$id'");
		return $query->result();
	}
    function get_row($col,$val){
      $query=$this->db->get_where('notificacion',[$col=>$val]);
      return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('notificacion',[$col=>$val,$col2=>$val2]);
      return $query->result();
    }
	function get_count_active($idu){
		$this->db->select("idu, usuario,max(fecha) as fecha,COUNT(idno) as cantidad");
	    $this->db->from("notificacion");
	    $this->db->where("status = '1'");
	    $this->db->where("idu != '$idu'");
	    $this->db->group_by("idu");
	    $query=$this->db->get();
	    return $query->result();		
	}
	function insertar_almacen($emisor,$reseptor,$almacen){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'almacen' => $almacen,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}	
	}
	function insertar_material($emisor,$reseptor,$material){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'material' => $material,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}	
	}
	function insertar_producto($emisor,$reseptor,$producto){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'producto' => $producto,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}	
	}
	function insertar_producto_categoria($emisor,$reseptor,$producto_categoria){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'producto_categoria' => $producto_categoria,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}	
	}
	function insertar_pedido($emisor,$reseptor,$pedido){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'pedido' => $pedido,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}	
	}
	function insertar($emisor,$reseptor,$almacen,$material,$producto,$empleado){
		date_default_timezone_set("America/La_Paz");
		$datos=array(
			'emisor' => $emisor,
			'reseptor' => $reseptor,
			'almacen' => $almacen,
			'material' => $material,
			'producto' => $producto,
			'empleado' => $empleado,
			'fecha' => date('Y-m-d H:i:s')
		);
		if($this->db->insert('notificacion',$datos)){
			return true;
		}else{
			return false;
		}
	}
    function modificar_row($id,$col,$val){//en uso:capital humano,
    	date_default_timezone_set("America/La_Paz");
        $data = array(
          $col => $val,
          'fecha' => date('Y-m-d H:i:s')
        );
        if ($this->db->update('notificacion', $data, array('idno' => $id))){
          return true;
        }else{
          return false;
        }
    }
	function eliminar($id){
		if($this->db->delete('notificacion',['idno' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_notificacion.php */
/* Location: ./application/models/m_notificacion.php*/