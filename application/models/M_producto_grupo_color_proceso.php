<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_color_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_grupo_color_proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_color_proceso',['idpgrp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_color_proceso WHERE idpgrp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo_color_proceso',[$col => $val]);
		return $query->result();
	}
	function insertar($idpgrc,$idppr){
		$datos=array(
			'idpgrc' => $idpgrc,
			'idppr' => $idppr,
		);
		if($this->db->insert('producto_grupo_color_proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpa,$nombre,$abreviatura){
		$datos=array(
			'idpa' => $idpa,
			'nombre' => $nombre,
			'abreviatura' => $abreviatura
		);
		if($this->db->update('producto_grupo_color_proceso',$datos,array('idpgrp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_color_proceso',['idpgrp' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_grupo_color_proceso.php */
/* Location: ./application/models/m_producto_grupo_color_proceso.php*/