<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_banco extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
	    $this->db->order_by("razon","asc");
		$query=$this->db->get('banco');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('banco',['idba' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM banco WHERE idba='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('banco',array($col => $val));
		return $query->result();
	}
	function insertar($fotografia,$razon,$url){
		$datos=array(
			'fotografia' => $fotografia,
			'razon' => $razon,
			'url' => $url
		);
		if($this->db->insert('banco',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fotografia,$razon,$url){
		$datos=array(
			'fotografia' => $fotografia,
			'razon' => $razon,
			'url' => $url
		);
		if($this->db->update('banco',$datos,array('idba'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('banco',['idba' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}
/* End of file m_banco.php */
/* Location: ./application/models/m_banco.php*/