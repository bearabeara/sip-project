<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_detalle_descuento_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("iddesp","asc");
		$this->db->order_by("iddes","asc");
		$query=$this->db->get('detalle_descuento_producto');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('detalle_descuento_producto',['iddesp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM detalle_descuento_producto WHERE iddesp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('detalle_descuento_producto',array($col => $val));
		return $query->result();
    }
	function get_search($col,$val){
		$cols="iddesp,iddes,fotografia,descripcion";
		$this->db->select($cols);
		$this->db->from("detalle_descuento_producto");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="iddesp" || $col=="iddes"){
				$this->db->where("$col = '$val'");
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($iddes,$fotografia,$descripcion){
		$datos=array(
			'iddes' => $iddes,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->insert('detalle_descuento_producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$fotografia,$descripcion){
		$datos=array(
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->update('detalle_descuento_producto',$datos,array('iddesp'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('detalle_descuento_producto',['iddesp' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}
/* End of file m_detalle_descuento_producto.php */
/* Location: ./application/models/m_detalle_descuento_producto.php*/