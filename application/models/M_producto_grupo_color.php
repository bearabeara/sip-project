<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_color extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('producto_grupo_color');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_color',['idpgrc' => $id]);
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_grupo_color',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_grupo_color',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_color($col,$val){
		$cols="pgc.idpgrc, pgc.idpgr, pgc.costo, pgc.portada,
			co.idco,co.nombre,co.codigo,co.abr";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color pgc");
		$this->db->join('color co','co.idco = pgc.idco','inner');
	if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
		$this->db->where("$col = '$val'");
	}
		$this->db->order_by("co.nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_grupo_color($col,$val){
		$cols="pgc.idpgrc,pgc.idco,pgc.idpgr, pgc.costo, pgc.portada,
			pg.idpgr,pg.idp,pg.idgr";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color pgc");
		$this->db->join('producto_grupo pg','pgc.idpgr = pg.idpgr','inner');
		$this->db->where("$col = '$val'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_producto($col,$val){
		$cols="pgc.idpgrc,pgc.idco,pgc.idpgr, pgc.costo, pgc.portada,
			pg.idpgr,pg.idgr,
			p.idp,p.codigo,p.codigo_aux,p.nombre,p.fecha_creacion,p.observaciones";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color pgc");
		$this->db->join('producto_grupo pg','pgc.idpgr = pg.idpgr','inner');
		$this->db->join('producto p','pg.idp = p.idp','inner');
		if($col!="" && $val!=""){
			$this->db->where("$col = '$val'");
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_color WHERE idpgrc='$id'");
		return $query->result();
	}
	function insertar($idpgrc,$idpgr,$idco){
		$datos=array(
			'idpgrc' => $idpgrc,
			'idpgr' => $idpgr,
			'idco' => $idco
		);
		if($this->db->insert('producto_grupo_color',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$costo){
		$datos=array( 'costo' => $costo);
		if($this->db->update('producto_grupo_color',$datos,array('idpgrc' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('producto_grupo_color',$datos,array('idpgrc' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_color',['idpgrc' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_grupo_color");
		$max=$query->result();
		return $max[0]->max*1;
	}	
}

/* End of file m_producto_grupo_color.php */
/* Location: ./application/models/m_producto_grupo_color.php*/