<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_ciudad extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('ciudad');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('ciudad',['idci' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM ciudad WHERE idci='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('ciudad',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){
		$cols="c.idci, c.nombre as ciudad, c.abreviatura,
			p.idpa,p.nombre as pais";
		$this->db->select($cols);
		$this->db->from("ciudad c");
		$this->db->join('pais p','c.idpa = p.idpa','inner');
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="c.idci" || $col=="p.idpa"){
				$this->db->where("$col = '$val'");
			}
			if($col=="p.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpa,$nombre,$abreviatura){
		$datos=array(
			'idpa' => $idpa,
			'nombre' => $nombre,
			'abreviatura' => $abreviatura
		);
		if($this->db->insert('ciudad',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idpa,$nombre,$abreviatura){
		$datos=array(
			'idpa' => $idpa,
			'nombre' => $nombre,
			'abreviatura' => $abreviatura
		);
		if($this->db->update('ciudad',$datos,array('idci' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('ciudad',['idci' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_ciudad.php */
/* Location: ./application/models/m_ciudad.php*/