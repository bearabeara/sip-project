<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pago extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$c_fecha="`".$this->lib->encriptar_str("fecha")."`";
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$c_monto_bs="`".$this->lib->encriptar_str("monto_bs")."`";
		$cols="idpa,idpe,idbp,idus,idtc,".$c_fecha." as fecha,".$c_monto." as monto,".$c_monto_bs." as monto_bs,tipo_cambio,observacion";
		$this->db->select($cols);
		$this->db->from("pago");
		$this->db->order_by("idpa", "asc");
	      $query=$this->db->get();
	      return $query->result();
	}
	function get($id){
		$c_fecha="`".$this->lib->encriptar_str("fecha")."`";
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$c_monto_bs="`".$this->lib->encriptar_str("monto_bs")."`";
		$cols="idpa,idpe,idbp,idus,idtc,".$c_fecha." as fecha,".$c_monto." as monto,".$c_monto_bs." as monto_bs,tipo_cambio,observacion";
		$this->db->select($cols);
		$this->db->from("pago");
		$this->db->where("idpa = '$id'");
		$this->db->order_by("idpa", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row($col,$val){
		$c_fecha="`".$this->lib->encriptar_str("fecha")."`";
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$c_monto_bs="`".$this->lib->encriptar_str("monto_bs")."`";
		$cols="idpa,idpe,idbp,idus,idtc,".$c_fecha." as fecha,".$c_monto." as monto,".$c_monto_bs." as monto_bs,tipo_cambio,observacion";
		$this->db->select($cols);
		$this->db->from("pago");
		if($col!="" && $val!=""){
			$this->db->where("$col = '$val'");
		}
		$this->db->order_by("idpa", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($atrib,$val,$order_col,$order_by){
		$c_fecha="`".$this->lib->encriptar_str("fecha")."`";
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$c_monto_bs="`".$this->lib->encriptar_str("monto_bs")."`";
		$cols="p.idpa,p.idpe,p.idbp,p.idus,p.idtc,p.depositante,p.".$c_fecha." as fecha,p.".$c_monto." as monto,p.".$c_monto_bs." as monto_bs,p.tipo_cambio,p.observacion,
      	u.idus,u.ci as ci_usuario,u.usuario,u.tipo, u.fecha_ingreso,
      	bp.idba,bp.ci as ci_persona,bp.cuenta,bp.tipo, bp.estado";
      $this->db->select($cols);
      $this->db->from("pago p");
      $this->db->join('usuario u','u.idus = p.idus','inner');
      $this->db->join('banco_persona bp','bp.idbp = p.idbp','inner');
    if($atrib!="" && $atrib!=NULL && $val!="" && $val!=NULL){
    	if($atrib=="p.idpe" || $atrib=="p.idpa"){
    		$this->db->where("$atrib = $val");
    	}
    }
    if($order_col!="" && $order_col!=NULL && $order_by!="" && $order_by!=NULL){
    	$this->db->order_by($order_col, $order_by);
    }
    $this->db->order_by("p.idpa");
      $query=$this->db->get();
      return $query->result();

	}
	function insertar($idpe,$idbp,$idus,$idtc,$depositante,$fecha,$monto,$monto_bs,$tipo_cambio,$observacion){
		$c_fecha=$this->lib->encriptar_str("fecha");
		$fecha=$this->lib->encriptar_str($fecha);
		$c_monto=$this->lib->encriptar_str("monto");
		$monto=$this->lib->encriptar_num($monto);
		$c_monto_bs=$this->lib->encriptar_str("monto_bs");
		$monto_bs=$this->lib->encriptar_num($monto_bs);
		$datos=array(
			'idpe' => $idpe,
			'idbp' => $idbp,
			'idus' => $idus,
			'idtc' => $idtc,
			'depositante' => $depositante,
			$c_fecha => $fecha,
			$c_monto => $monto,
			$c_monto_bs => $monto_bs,
			'tipo_cambio' => $tipo_cambio,
			'observacion' => $observacion
		);
		if($this->db->insert('pago',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idbp,$idus,$idtc,$depositante,$fecha,$monto,$monto_bs,$tipo_cambio,$observacion){
		$c_fecha=$this->lib->encriptar_str("fecha");
		$fecha=$this->lib->encriptar_str($fecha);
		$c_monto=$this->lib->encriptar_str("monto");
		$monto=$this->lib->encriptar_num($monto);
		$c_monto_bs=$this->lib->encriptar_str("monto_bs");
		$monto_bs=$this->lib->encriptar_num($monto_bs);
		$datos=array(
			'idbp' => $idbp,
			'idus' => $idus,
			'idtc' => $idtc,
			'depositante' => $depositante,
			$c_fecha => $fecha,
			$c_monto => $monto,
			$c_monto_bs => $monto_bs,
			'tipo_cambio' => $tipo_cambio,
			'observacion' => $observacion
		);
		if($this->db->update('pago',$datos,array('idpa' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('pago',['idpa' => $id])){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file m_pago.php */
/* Location: ./application/models/m_pago.php*/