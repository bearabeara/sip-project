<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_imagen_color extends CI_Model{
	function __construct(){
		parent::__construct();

	}
	function get_all(){
		$query=$this->db->get('producto_imagen_color');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_imagen_color',['idpig' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_imagen_color WHERE idpig='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('producto_imagen_color',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_imagen_color',[$col => $val,$col2 => $val2]);
		return $query->result();
	}


	function insertar($idpgrc,$descripcion,$archivo){
		$datos=array(
			'idpgrc' => $idpgrc,
			'descripcion' => $descripcion,
			'archivo' => $archivo
		);
		if($this->db->insert('producto_imagen_color',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$descripcion,$archivo){
		$datos=array(
			'descripcion' => $descripcion,
			'archivo' => $archivo
		);
		if($this->db->update('producto_imagen_color',$datos,array('idpig' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('producto_imagen_color',$datos,array('idpig' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_imagen_color',['idpig' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val,$col2,$val2){
		if($this->db->delete('producto_imagen_color',[$col => $val, $col2 => $val2])){
			return true;
		}else{
			return false;
		}
	}
	
}

/* End of file m_producto_imagen_color.php */
/* Location: ./application/models/m_producto_imagen_color.php*/