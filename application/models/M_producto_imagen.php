<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_imagen extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("idpi", "asc");
		$query=$this->db->get('producto_imagen');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_imagen',['idpi' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_imagen WHERE idpi='$id'");
		return $query->result();
	}

	function get_row($col,$val){
		$this->db->order_by("idpi", "asc");
		$query=$this->db->get_where('producto_imagen',[$col => $val]);
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('producto_imagen',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function insertar($idp,$descripcion,$archivo){
		$datos=array(
			'idp' => $idp,
			'descripcion' => $descripcion,
			'archivo' => $archivo,
		);
		if($this->db->insert('producto_imagen',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$descripcion,$archivo){
		$datos=array(
			'descripcion' => $descripcion,
			'archivo' => $archivo,
		);
		if($this->db->update('producto_imagen',$datos,array('idpi' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('producto_imagen',$datos,array('idpi' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_imagen',['idpi' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_producto_imagen.php */
/* Location: ./application/models/m_producto_imagen.php*/