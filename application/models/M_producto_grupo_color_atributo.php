<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_grupo_color_atributo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('producto_grupo_color_atributo');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('producto_grupo_color_atributo',['idpgca' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM producto_grupo_atributo WHERE idpgca='$id'");
		return $query->result();
	}
	function get_atributo($col,$val){
		$cols="a.idatr, a.atributo,
			pgca.idpgca, pgca.idpgrc, pgca.valor";
		$this->db->select($cols);
		$this->db->from("producto_grupo_color_atributo pgca");
		$this->db->join('atributo a','a.idatr = pgca.idatr','inner');
		$this->db->where("$col = '$val'");
		$this->db->order_by("a.atributo", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){
		$this->db->select("*");
		$this->db->from("producto_grupo_atributo");
		if($col!="" && $val!=""){
			if($col=="idpgca"){
				$this->db->where("$col = '$val'");
			}
			if($col=="nombre" || $col=="abr"){
				$this->db->where("$col like '$val%'");
			}
		}
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpgrc,$idatr,$valor){
		$datos=array(
			'idpgrc' => $idpgrc,
			'idatr' => $idatr,
			'valor' => $valor,
		);
		if($this->db->insert('producto_grupo_color_atributo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$valor){
		$datos=array(
			'valor' => $valor
		);
		if($this->db->update('producto_grupo_color_atributo',$datos,array('idpgca' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_grupo_color_atributo',['idpgca' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_producto_grupo_atributo.php */
/* Location: ./application/models/m_producto_grupo_atributo.php*/