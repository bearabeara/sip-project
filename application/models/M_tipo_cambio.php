<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_tipo_cambio extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
	    $this->db->order_by("moneda","asc");
	    $this->db->order_by("tipo","asc");
		$query=$this->db->get('tipo_cambio');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('tipo_cambio',['idtc' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM tipo_cambio WHERE idtc='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('tipo_cambio',array($col => $val));
		return $query->result();
	}
	function insertar($moneda,$simbolo,$tipo,$monto){
		$datos=array(
			'moneda' => $moneda,
			'simbolo' => $simbolo,
			'tipo' => $tipo,
			'monto' => $monto 
		);
		if($this->db->insert('tipo_cambio',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$moneda,$simbolo,$tipo,$monto){
		$datos=array(
			'moneda' => $moneda,
			'simbolo' => $simbolo,
			'tipo' => $tipo,
			'monto' => $monto 
		);
		if($this->db->update('tipo_cambio',$datos,array('idtc'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('tipo_cambio',['idtc' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}
/* End of file m_tipo_cambio.php */
/* Location: ./application/models/m_tipo_cambio.php*/