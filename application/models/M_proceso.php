<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proceso extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso Produccion,
		$this->db->order_by("nombre","asc");
		$query=$this->db->get('proceso');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('proceso',['idpr' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proceso WHERE idpr='$id'");
		return $query->result();
	}
	function get_search($col,$val){
		$cols="idpr,nombre,detalle";
		$this->db->select($cols);
		$this->db->from("proceso");
		$this->db->order_by("nombre", "asc");
		if($col!="" && $val!=""){
			if($col=="nombre"){ $this->db->where("$col like '%$val%'");}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nombre){
		$datos=array( 'nombre' => $nombre );
		if($this->db->insert('proceso',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre){
		$datos=array(
			'nombre' => $nombre
		);
		if($this->db->update('proceso',$datos,array('idpr' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proceso',['idpr' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}