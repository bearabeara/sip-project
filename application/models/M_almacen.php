<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('almacen');
		return $query->result();
	}
    function get_row($col,$val){
      $query=$this->db->get_where('almacen',[$col=>$val]);
      return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('almacen',[$col=>$val,$col2=>$val2]);
      return $query->result();
    }
	function get_search($col,$val){// en uso: ALMACEN
		$cols="a.ida,a.codigo,a.nombre,a.tipo,a.fotografia,a.descripcion";
		$this->db->select($cols);
	    $this->db->from("almacen a");
	    if($col!="" && $val!=""){
	    	if($col=="a.ida" || $col=="a.tipo"){ $this->db->where("$col = '$val'"); }
	    	if($col=="a.nombre" || $col=="a.codigo"){ $this->db->where("$col like '%$val%'"); }
	    }
	    $this->db->order_by("a.tipo", "asc");
	    $this->db->order_by("a.nombre", "asc");
	    $query=$this->db->get();
	    return $query->result();
	}
	function get_all_not($ida){// en uso: materiales
		$this->db->select("*");
	    $this->db->from("almacen");
	    $this->db->where("ida != '$ida'");
	    $this->db->where("tipo = 'material'");
	    $query=$this->db->get();
	    return $query->result();
	}
	function get_all_cantidad(){
		$col="am.idam,am.ida,am.cantidad,
			m.idm,m.costo_unitario,
			mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu, u.nombre as medida,u.abreviatura, 
			g.idg,g.nombre as nombre_g, 
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("almacen_material am");
		$this->db->where("am.ida = '$ida'");
		$this->db->order_by("mi.nombre", "asc");
		$this->db->join('material m','m.idm = am.idm','inner');
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen',array('ida' => $id));
		return $query->result();
	}
	function get_id_mat($id,$tipo){
		$this->db->order_by("ida", "asc");
		$query=$this->db->get_where('almacen',array('ida' => $id,'tipo' => $tipo));
		
		return $query->result();
	}

	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM almacen WHERE ida='$id'");
		return $query->result();
	}

	function get_material(){//en uso: Proveedor, produccion,
		$this->db->order_by("ida", "asc");
		$query=$this->db->get_where('almacen',['tipo' => 'material']);
		return $query->result();
	}
	function get_producto(){
		$query=$this->db->get_where('almacen',['tipo' => 'producto']);
		return $query->result();
	}
	function insertar($ida,$codigo,$nombre,$tipo,$fotografia,$descripcion){
		$datos=array(
			'ida' => $ida,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'tipo' => $tipo,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->insert('almacen',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$codigo,$nombre,$fotografia,$descripcion){
		$datos=array(
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->update('almacen',$datos,array('ida' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->query("DELETE FROM almacen WHERE ida='$id'")){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM almacen");
		$max=$query->result();
		return $max[0]->max*1;
	}
	
}

/* End of file M_almacen.php */
/* Location ./application/models/M_almacen.php*/