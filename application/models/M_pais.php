<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pais extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('pais');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('pais',['idpa' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pais WHERE idpa='$id'");
		return $query->result();
	}
	function insertar($nombre){
		$datos=array(
			'nombre' => $nombre
		);
		if($this->db->insert('pais',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre){
		$datos=array(
			'nombre' => $nombre
		);
		if($this->db->update('pais',$datos,array('idpa' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('pais',['idpa' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_pais.php */
/* Location: ./application/models/m_pais.php*/