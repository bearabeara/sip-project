<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_descuento_producto extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nit", "asc");
		$this->db->order_by("nombre", "asc");
		$query=$this->db->get('descuento_producto');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('descuento_producto',['iddes' => $id]);
		return $query->result();
	}
	function get_row($col,$val){//en uso, Clientes, 
      	$query=$this->db->get_where('descuento_producto',[$col => $val]);
      return $query->result();
    }
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM descuento_producto WHERE iddes='$id'");
		return $query->result();
	}
	function get_search($col,$val){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$cols="iddes,idsdp,idpe,idus,".$c_cantidad." as cantidad,porcentaje,observacion";
		$this->db->select($cols);
		$this->db->from("descuento_producto");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="iddes" || $col=="idsdp" || $col=="idpe" || $col=="idus"){
				$this->db->where("$col = '$val'");
			}
			if($col=="nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idsdp,$idpe,$idus,$cantidad,$porcentaje,$observacion){
		$c_cantidad=$this->lib->encriptar_str("cantidad");
		$cantidad=$this->lib->encriptar_num($cantidad);
		$datos=array(
			'idsdp' => $idsdp,
			'idpe' => $idpe,
			'idus' => $idus,
			$c_cantidad => $cantidad,
			'porcentaje' => $porcentaje,
			'observacion' => $observacion
		);
		if($this->db->insert('descuento_producto',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$cantidad,$porcentaje,$observacion){
		$c_cantidad=$this->lib->encriptar_str("cantidad");
		$cantidad=$this->lib->encriptar_num($cantidad);
		$datos=array(
			$c_cantidad => $cantidad,
			'porcentaje' => $porcentaje,
			'observacion' => $observacion
		);
		if($this->db->update('descuento_producto',$datos,array('iddes' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('descuento_producto',['iddes' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_descuento_producto.php */
/* Location: ./application/models/m_descuento_producto.php*/