<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_producto_pieza extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("nombre","asc");
		$query=$this->db->get('producto_pieza');
		return $query->result();
	}
	function get($id){
		$this->db->order_by("nombre","asc");
		$query=$this->db->get_where('producto_pieza',['idppi' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$this->db->order_by("nombre");
		$query=$this->db->query("SELECT $col FROM producto_pieza WHERE idppi='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$this->db->order_by("nombre");
		$query=$this->db->get_where('producto_pieza',[$col => $val]);
		return $query->result();
	}
	function insertar($idp,$idrs,$idm,$nombre,$largo,$ancho,$fotografia,$cantidad,$descripcion){
		$datos=array(
			'idp' => $idp,
			'idrs' => $idrs,
			'idm' => $idm,
			'nombre' => $nombre,
			'largo' => $largo,
			'ancho' => $ancho,
			'fotografia' => $fotografia,
			'cantidad' => $cantidad,
			'descripcion' => $descripcion
		);
		if($this->db->insert('producto_pieza',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idrs,$idm,$nombre,$largo,$ancho,$fotografia,$cantidad,$descripcion){
		$datos=array(
			'idrs' => $idrs,
			'idm' => $idm,
			'nombre' => $nombre,
			'largo' => $largo,
			'ancho' => $ancho,
			'fotografia' => $fotografia,
			'cantidad' => $cantidad,
			'descripcion' => $descripcion
		);
		if($this->db->update('producto_pieza',$datos,array('idppi' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('producto_pieza',['idppi' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM producto_pieza");
		$max=$query->result();
		return $max[0]->max*1;
	}
}
/* End of file m_producto_pieza.php */
/* Location: ./application/models/m_producto_pieza.php*/