<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_sucursal_detalle_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('sucursal_detalle_pedido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('sucursal_detalle_pedido',['idsdp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM sucursal_detalle_pedido WHERE idsdp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$cols="idsdp,idsc,iddp,".$c_cantidad." as cantidad";
		$this->db->select($cols);
		$this->db->from("sucursal_detalle_pedido");
		if($col!="" && $col!=NULL && $val!="" && $val!=NULL){
			$this->db->where("$col = '$val'");
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$cols="idsdp,idsc,iddp,".$c_cantidad." as cantidad";
		$this->db->select($cols);
		$this->db->from("sucursal_detalle_pedido");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="idsdp" || $col=="idsc" || $col=="iddp"){ $this->db->where("$col = '$val'"); }
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idsc,$iddp,$cantidad){
		$c_cantidad=$this->lib->encriptar_str("cantidad");
		$cantidad=$this->lib->encriptar_num($cantidad);
		$datos=array(
			'idsc' => $idsc,
			'iddp' => $iddp,
			$c_cantidad => $cantidad
		);
		if($this->db->insert('sucursal_detalle_pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$cantidad){
		$c_cantidad=$this->lib->encriptar_str("cantidad");
		$cantidad=$this->lib->encriptar_num($cantidad);
		$datos=array(
			$c_cantidad => $cantidad
		);
		if($this->db->update('sucursal_detalle_pedido',$datos,array('idsdp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('sucursal_detalle_pedido',['idsdp' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_sucursal_detalle_pedido.php */
/* Location: ./application/models/m_sucursal_detalle_pedido.php*/