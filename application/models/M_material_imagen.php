<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_imagen extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_imagen');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_imagen',['idmig' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_imagen WHERE idmig='$id'");
		return $query->result();
	}
    function get_row($col,$val){
    	$this->db->order_by("portada","desc");
    	$this->db->order_by("idmig","asc");
      	$query=$this->db->get_where('material_imagen',[$col=>$val]);
      	return $query->result();
    }
    function get_row_2n($col,$val,$col2,$val2){
      $query=$this->db->get_where('material_imagen',[$col=>$val,$col2=>$val2]);
      return $query->result();
    }
	function insertar($idmi,$nombre,$portada){
		$datos=array(
			'idmi' => $idmi,
			'nombre' => $nombre,
			'portada' => $portada 
		);
		if($this->db->insert('material_imagen',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nombre,$titulo,$descripcion){
		$datos=array(
			'nombre' => $nombre,
			'titulo' => $titulo,
			'descripcion' => $descripcion
		);
		if($this->db->update('material_imagen',$datos,array('idmig' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array($col."" => $val);
		if($this->db->update('material_imagen',$datos,array('idmig' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function reset_portada($col,$val){
		$datos=array( 'portada' => "0");
		if($this->db->update('material_imagen',$datos,array($col => $val))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_imagen',['idmig' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}

/* End of file m_material_imagen.php */
/* Location: ./application/models/m_material_imagen.php*/