<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_tipo_contrato extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){// en uso: capital humano,
		$query=$this->db->get('tipo_contrato');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('tipo_contrato',['idtc' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM tipo_contrato WHERE idtc='$id'");
		return $query->result();
	}
	function get_row_2n($col,$val,$col2,$val2){
		$query=$this->db->get_where('tipo_contrato',[$col => $val,$col2 => $val2]);
		return $query->result();
	}
	function get_fecha_principal(){
		$cols="*";
		$this->db->select($cols);
		$this->db->from("tipo_contrato");
		$this->db->where("principal = '1'");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($tipo,$horas,$descripcion){
		$datos=array(
			'tipo' => $tipo,
			'horas' => $horas,
			'descripcion' => $descripcion
		);
		if($this->db->insert('tipo_contrato',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function update_row($id,$col,$val){
		$datos=array(
			$col => $val
		);
		if($this->db->update('tipo_contrato',$datos,array('idtc'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function reset_principal(){
		$datos=array(
			'principal' => 0
		);
		if($this->db->update('tipo_contrato',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$tipo,$horas,$descripcion){
		$datos=array(
			'tipo' => $tipo,
			'horas' => $horas,
			'descripcion' => $descripcion
		);
		if($this->db->update('tipo_contrato',$datos,array('idtc'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('tipo_contrato',['idtc' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_tipo_contrato.php */
/* Location: ./application/models/m_tipo_contrato.php*/