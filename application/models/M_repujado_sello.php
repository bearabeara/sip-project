<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_repujado_sello extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("tipo");
		$this->db->order_by("nombre");
		$query=$this->db->get('repujado_sello');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('repujado_sello',['idrs' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM repujado_sello WHERE idrs='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('repujado_sello',[$col => $val]);
		return $query->result();
	}
	function insertar($imagen,$nombre,$tipo){
		$datos=array(
			'imagen' => $imagen,
			'nombre' => $nombre,
			'tipo' => $tipo
		);
		if($this->db->insert('repujado_sello',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$imagen,$nombre,$tipo){
		$datos=array(
			'imagen' => $imagen,
			'nombre' => $nombre,
			'tipo' => $tipo
		);
		if($this->db->update('repujado_sello',$datos,array('idrs' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('repujado_sello',['idrs' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM repujado_sello");
		$max=$query->result();
		return $max[0]->max*1;
	}
}

/* End of file m_repujado_sello.php */
/* Location: ./application/models/m_repujado_sello.php*/