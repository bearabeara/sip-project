<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_descuento_pago extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$cols="iddpa,idpa,idus,".$c_monto." as monto, observacion";
		$this->db->select($cols);
		$this->db->from("descuento_pago");
		$this->db->order_by("iddpa","asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$cols="iddpa,idpa,idus,".$c_monto." as monto, observacion";
		$this->db->select($cols);
		$this->db->from("descuento_pago");
		$this->db->where("iddpa='$id'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM descuento_pago WHERE iddpa='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('descuento_pago',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){
		$c_monto="`".$this->lib->encriptar_str("monto")."`";
		$cols="iddpa,idpa,idus,".$c_monto." as monto, observacion";
		$this->db->select($cols);
		$this->db->from("descuento_pago");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="iddpa" || $col=="idpa"){$this->db->where("$col = '$val'");}
		}
		$this->db->order_by("iddpa","asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpa,$idus,$monto,$observacion){
		$c_monto=$this->lib->encriptar_str("monto");
		$monto=$this->lib->encriptar_num($monto);
		$datos=array(
			'idpa' => $idpa,
			'idus' => $idus,
			$c_monto => $monto,
			'observacion' => $observacion
		);
		if($this->db->insert('descuento_pago',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$monto,$observacion){
		$c_monto=$this->lib->encriptar_str("monto");
		$monto=$this->lib->encriptar_num($monto);
		$datos=array(
			$c_monto => $monto,
			'observacion' => $observacion
		);
		if($this->db->update('descuento_pago',$datos,array('iddpa' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('descuento_pago',['iddpa' => $id])){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file m_descuento_pago.php */
/* Location: ./application/models/m_descuento_pago.php*/