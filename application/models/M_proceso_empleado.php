<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proceso_empleado extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('proceso_empleado');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('proceso_empleado',['idpre' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proceso_empleado WHERE idpre='$id'");
		return $query->result();
	}
    function get_row($col,$val){
      $query=$this->db->get_where('proceso_empleado',[$col => $val]);
      return $query->result();
    }
	function get_proceso_empleado($idpr){
		$col="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,
			e.ide,e.idtc,e.codigo,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.direccion,e.estado, e.grado_academico,
			ep.idpre,ep.idpr,ep.tipo";
		$this->db->select($col);
		$this->db->from("proceso_empleado ep");
		$this->db->order_by("p.nombre", "asc");
		$this->db->where("ep.idpr = '$idpr'");
		$this->db->join('empleado e','ep.ide = e.ide','inner');
		$this->db->join("persona p","e.ci=p.ci","inner");
		$query=$this->db->get();
		return $query->result();
	}
	function get_proceso($ide){
		$col="ep.idpre,ep.ide,ep.tipo,
			pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($col);
		$this->db->from("proceso_empleado ep");
		$this->db->order_by("pr.nombre", "asc");
		$this->db->where("ep.ide = '$ide'");
		$this->db->join('proceso pr','pr.idpr = ep.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_complet($col,$val){
		$cols="ep.idpre,ep.ide,ep.tipo,
			pr.idpr,pr.nombre,pr.detalle";
		$this->db->select($cols);
		$this->db->from("proceso_empleado ep");
		$this->db->order_by("pr.nombre", "asc");
	if($col!="" && $val!=""){
		if($col=="ep.idpre"){
			$this->db->where("$col = '$val'");
		}
	}
		$this->db->join('proceso pr','pr.idpr = ep.idpr','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_empleado($col,$val,$order_by,$order){
		$cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
			e.ide,e.idtc,e.codigo,e.salario,e.fecha_ingreso,e.fecha_nacimiento,e.c_feriado,e.tipo,e.c_descuento,e.direccion,e.estado, e.grado_academico,
			ep.idpre,ep.idpr,ep.tipo";
		$this->db->select($cols);
		$this->db->from("proceso_empleado ep");
		$this->db->join('empleado e','ep.ide = e.ide','inner');
		$this->db->join("persona p","e.ci=p.ci","inner");
		if($col!="" && $val!=""){
			if($col=="ep.idpr"){
				$this->db->where("$col = '$val'");
			}
			if($col=="nombre_completo" || $col=="p.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->where("e.estado = '1'");
		$this->db->order_by("ep.tipo", "asc");
		if($order_by=="" || $order=="" || $order_by==NULL || $order==NULL){
			$this->db->order_by("p.nombre", "asc");
		}else{
			$this->db->order_by($order_by, $order);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpr,$ide,$tipo){
		$datos=array(
			'ide' => $ide,
			'idpr' => $idpr,
			'tipo' => $tipo
		);
		if($this->db->insert('proceso_empleado',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$tipo){
		$datos=array( 'tipo' => $tipo );
		if($this->db->update('proceso_empleado',$datos,array('idpre' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proceso_empleado',['idpre' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_proceso_empleado.php */
/* Location: ./application/models/m_proceso_empleado.php*/