<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_banco_persona extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
	    $this->db->order_by("razon","asc");
		$query=$this->db->get('banco_persona');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('banco_persona',['idbp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM banco_persona WHERE idbp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('banco_persona',array($col => $val));
		return $query->result();
	}
	function get_search($atrib,$val){
		$col="bp.idbp,bp.ci,bp.cuenta,bp.tipo,bp.estado,
			b.idba,b.fotografia,b.razon,b.url,b.descripcion";
		$this->db->select($col);
		$this->db->from("banco_persona bp");
		$this->db->join('banco b','b.idba = bp.idba','inner');
	if($atrib!="" && $atrib!=NULL && $val!="" && $val!=NULL){
		if($atrib=="bp.idbp" || $atrib=="bp.idba" || $atrib=="bp.ci"){
			echo "Entro";
			$this->db->where("$atrib = '$val'");
		}
	}
		$this->db->order_by("b.razon", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search_2n($atrib,$val,$atrib2,$val2){
		$col="bp.idbp,bp.ci,bp.cuenta,bp.tipo,bp.estado,
			b.idba,b.fotografia,b.razon,b.url,b.descripcion";
		$this->db->select($col);
		$this->db->from("banco_persona bp");
		$this->db->join('banco b','b.idba = bp.idba','inner');
	if($atrib!="" && $atrib!=NULL && $val!="" && $val!=NULL){
		if($atrib=="bp.idbp" || $atrib=="b.idba" || $atrib=="bp.ci"){
			$this->db->where("$atrib = '$val'");
		}
	}
	if($atrib2!="" && $atrib2!=NULL && $val2!="" && $val2!=NULL){
		if($atrib2=="bp.idbp" || $atrib2=="b.idba" || $atrib2=="bp.ci"){
			$this->db->where("$atrib2 = '$val2'");
		}
	}
		$this->db->order_by("b.razon", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idba,$ci,$cuenta,$tipo,$estado){
		$datos=array(
			'idba' => $idba,
			'ci' => $ci,
			'cuenta' => $cuenta,
			'tipo' => $tipo,
			'estado' => $estado
		);
		if($this->db->insert('banco_persona',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idba,$ci,$cuenta,$tipo){
		$datos=array(
			'idba' => $idba,
			'ci' => $ci,
			'cuenta' => $cuenta,
			'tipo' => $tipo
		);
		if($this->db->update('banco_persona',$datos,array('idbp'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('banco_persona',['idbp' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}
/* End of file m_banco_persona.php */
/* Location: ./application/models/m_banco_persona.php*/