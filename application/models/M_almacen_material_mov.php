<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_almacen_material_mov extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$cols="idamv,fecha_sistema,date(fecha_usuario) as fecha,time(fecha_usuario) as hora,usuario, solicitante, fecha_usuario, almacen, codigo, material, cantidad,ingreso,salida,observacion";
		$this->db->select($cols);
		$this->db->from("almacen_material_mov");
		$this->db->order_by("fecha_usuario","desc");
		$query=$this->db->get();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('almacen_material_mov',['idamv' => $id]);
		return $query->result();
	}

	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM almacen_material_mov WHERE idamv='$id'");
		return $query->result();
	}
	function get_row($col,$row,$tipo){
		$query=$this->db->query("SELECT * FROM almacen_material_mov WHERE $col like '$row%' and tipo='$tipo' order by 'fecha_sistema' desc");
		return $query->result();
	}
	function get_search($where){//en uso movimiento material, 
		$cols="idamv,fecha_sistema,date(fecha_usuario) as fecha,time(fecha_usuario) as hora,usuario, solicitante, fecha_usuario, almacen, codigo, material, cantidad,ingreso,salida,observacion";
		$this->db->select($cols);
	    $this->db->from("almacen_material_mov");
	    if($where!=""){ $this->db->where($where); }
	    $this->db->order_by("idamv","desc");
	    $query=$this->db->get();
	    return $query->result();
	}	
	function insertar($usuario,$solicitante,$fecha_usuario,$almacen,$codigo,$material,$cantidad,$ingreso,$salida,$observacion){
		$datos=array(
			'usuario' => $usuario,
			'solicitante' => $solicitante,
			'fecha_usuario' => $fecha_usuario,
			'almacen' => $almacen,
			'codigo' => $codigo,
			'material' => $material,
			'cantidad' => $cantidad,
			'ingreso' => $ingreso,
			'salida' => $salida,
			'observacion' => $observacion
		);
		if($this->db->insert('almacen_material_mov',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('almacen_material_mov',['idamv' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_all(){
		if($this->db->delete('almacen_material_mov',['1' => "1"])){
			return true;
		}else{
			return false;
		}
	}
	function eliminar_row($col,$val){
		if($this->db->delete('almacen_material_mov',[$col => $val])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_historial_insumo.php */
/* Location: ./application/models/m_historial_insumo.php*/