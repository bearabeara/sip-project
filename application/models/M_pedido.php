<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('pedido');
		return $query->result();
	}
	function get($id){
		$c_descuento="`".$this->lib->encriptar_str("descuento")."`";
		$cols="idpe, orden, idcl, nombre,".$c_descuento." as descuento, observacion, estado, estado_pago";
		$this->db->select($cols);
		$this->db->from("pedido");
		$this->db->where("idpe = '$id'");
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM pedido WHERE idpe='$id'");
		return $query->result();
	}
	function get_row($col,$val){
        $c_descuento="`".$this->lib->encriptar_str("descuento")."`";
        $cols="idpe,orden,idcl,nombre,".$c_descuento." as descuento,observacion,estado,estado_pago";
        $this->db->select($cols);
        $this->db->from("pedido");
        $this->db->where("$col = '$val'");
		$this->db->order_by("idpe", "desc");
        $query=$this->db->get();
        return $query->result();
	}
	function get_search($col,$val){
		$c_descuento="`".$this->lib->encriptar_str("descuento")."`";
		$cols="p.idpe,p.orden,p.nombre,p.".$c_descuento." as descuento,p.observacion as observacion_pedido,p.estado,p.estado_pago,
			c.idcl, c.nit, c.razon, c.gerente, c.telefono, c.url, c.fotografia, c.observacion,
			min(pp.fecha) as fecha";
		$this->db->select($cols);
		$this->db->from("pedido p");
		if($col!="" && $val!=""){
			if($col=="p.idpe" || $col=="c.idcl" || $col=="p.estado"){
				$this->db->where("$col = '$val'");
			}
			if($col=="c.nit" || $col=="p.nombre"){
				$this->db->where("$col like '%$val%'");
			}
			if($col=="numero"){
				$this->db->where("p.idpe like '%$val%'");
			}
		}
		$this->db->join('cliente c','c.idcl = p.idcl','inner');
		$this->db->join('parte_pedido pp','pp.idpe = p.idpe','inner');
		$this->db->group_by("p.idpe");
        $this->db->order_by("p.orden", "desc");
		$this->db->order_by("p.idpe", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idpe,$orden,$idcl,$nombre,$descuento,$observacion){
		$c_descuento=$this->lib->encriptar_str("descuento");
		$descuento=$this->lib->encriptar_num($descuento);
		$datos=array(
			'idpe' => $idpe,
            'orden' => $orden,
			'idcl' => $idcl,
			'nombre' => $nombre,
			$c_descuento => $descuento,
			'observacion' => $observacion
		);
		if($this->db->insert('pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$orden,$nombre,$descuento,$observacion,$estado,$estado_pago){
		$c_descuento=$this->lib->encriptar_str("descuento");
		$descuento=$this->lib->encriptar_num($descuento);
		$datos=array(
            'orden' => $orden,
			'nombre' => $nombre,
			$c_descuento => $descuento,
			'observacion' => $observacion,
			'estado' => $estado,
			'estado_pago' => $estado_pago
		);
		if($this->db->update('pedido',$datos,array('idpe' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		if($col=="descuento"){
			$col=$this->lib->encriptar_str($col);
			$val=$this->lib->encriptar_num($val);
		}
		$datos=array( $col => $val );
		if($this->db->update('pedido',$datos,array('idpe' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM pedido");
		$max=$query->result();
		return $max[0]->max*1;
	}
	function eliminar($id){
		if($this->db->delete('pedido',['idpe' => $id])){
			return true;
		}else{
			return false;
		}
	}
}
/* End of file m_pedido.php */
/* Location: ./application/models/m_pedido.php*/