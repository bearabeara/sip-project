<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_directivo extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('directivo');
		return $query->result();
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('directivo',['iddi' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM directivo WHERE iddi='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('directivo',array($col => $val));
		return $query->result();
	}
    function get_search($col,$val,$order,$by){//EN USO: CAPITAL HUMANO
        $cols="d.iddi,d.ci,d.fecha_nacimiento,d.cargo_directivo,
        	p.ci,p.idci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo";
        $this->db->select($cols);
        $this->db->from("directivo d");
        $this->db->join("persona p","p.ci = d.ci","inner");
        if($col!="" && $val!=""){
            if($col=="d.iddi"){$this->db->where("$col = '$val'");}
            if($col=="p.ci"){$this->db->where("$col like '%$val%'");}
            if($col=='nombre_completo'){ $this->db->where("CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) like '%$val%'");}
        }
        if($order!="" && $order!=NULL && $by!="" && $by!=NULL){
        	if($order=="nombre_completo"){
        		$this->db->order_by($order,$by);
        	}else{
        		$this->db->order_by($order,$by);
        	}
        }else{
        	$this->db->order_by("p.nombre");
        }
        $query=$this->db->get();
        return $query->result();
    }
	function insertar($ci,$fecha_nacimiento,$cargo_directivo){
		$datos=array(
			'ci' => $ci,
			'fecha_nacimiento' => $fecha_nacimiento,
			'cargo_directivo' => $cargo_directivo
		);
		if($this->db->insert('directivo',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$ci,$fecha_nacimiento,$cargo_directivo){
		$datos=array(
			'ci' => $ci,
			'fecha_nacimiento' => $fecha_nacimiento,
			'cargo_directivo' => $cargo_directivo
		);
		if($this->db->update('directivo',$datos,array('iddi'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('directivo',['iddi' => $id])){
			return true;
		}else{
			return false;
		}
	}

	
}
/* End of file m_directivo.php */
/* Location: ./application/models/m_directivo.php*/