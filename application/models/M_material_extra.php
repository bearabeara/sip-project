<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_extra extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_extra');
		return $query->result();
	}
	function get_material_item($idmi){// en uso: MATERIALES ADICIONALES
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idme";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->where("mi.idmi = '$idmi'");
		$this->db->join('material_extra m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_extra WHERE idme='$id'");
		return $query->result();
	}

	/*
	function material_proveedor(){// en uso: COMPRAS
		$cols="m.idme,
			mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as obs_material,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			mp.idmp,mp.costo_unitario as costo_material_proveedor,
			pr.idpro,pr.nit,pr.encargado,pr.url,
			p.ci,p.idci,p.nombre as nombre_proveedor,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("material_extra m");
		$this->db->join('material_item mi','mi.idmi=m.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->join('material_proveedor mp','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor pr','pr.idpro=mp.idpro','inner');
		$this->db->join('persona p','p.ci=pr.nit','inner');
		$this->db->group_by("p.ci");
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}*/
	function insertar($idmi,$cantidad){
		$datos=array(
			'idmi' => $idmi,
			'cantidad' => $cantidad
		);
		if($this->db->insert('material_extra',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('material_extra',$datos,array('idme'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_extra',['idme' => $id])){
			return true;
		}else{
			return false;
		}
	}	
}

/* End of file m_material_extra.php */
/* Location: ./application/models/m_material_extra.php*/