<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material',['idm' => $id]);
		return $query->result();
	}
	function get_row($col,$val){//en uso materiales->configuracion->color
		$this->db->order_by("idm", "asc");
		$query=$this->db->get_where('material',array($col => $val));
		return $query->result();
	}

	function insertar($idm,$idmi,$idmg,$idco,$costo_unitario){
		$datos=array(
			'idm' => $idm,
			'idmi' => $idmi,
			'idmg' => $idmg,
			'idco' => $idco,
			'costo_unitario' => $costo_unitario
		);
		if($this->db->insert('material',$datos)){
			return true;
		}else{
			return false;
		}
	}

	function modificar($id,$idmg,$idco){
		$datos=array(
			'idmg' => $idmg,
			'idco' => $idco
		);
		if($this->db->update('material',$datos,array('idm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material',array('idm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM material");
		$max=$query->result();
		return $max[0]->max*1;
	}
	function get_all_cantidad(){
		$col="m.idm,m.idmg,m.idco,m.costo_unitario,
			mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			SUM(am.cantidad) as cantidad";
		$this->db->select($col);
		$this->db->from("material m");
		$this->db->group_by("mi.nombre");
		$this->db->order_by("m.idm");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('almacen_material am','m.idm = am.idm','inner');
		$query=$this->db->get();
		return $query->result();
	}
	/*
	function modificar_row($id,$col,$val){
		$datos=array(
			'col' => $val
		);
		if($this->db->update('material',$datos,array('idm' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_idmi($idmi,$idg,$idco){
		$datos=array(
			'idg' => $idg,
			'idco' => $idco
		);
		if($this->db->update('material',$datos,array('idmi' => $idmi))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_idmi_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('material',$datos,array('idmi' => $id))){
			return true;
		}else{
			return false;
		}
	}*/


	/*

	function get_material_atributo_idm($idm,$atributo){// en uso ORDEN DE PRODUCCION
		$cols="m.idm,m.idg,m.idco,m.costo_unitario,";
		if($atributo=="grupo" || $atributo=="all"){
			$cols.="g.nombre as nombre_g,";
		}
		if($atributo=="color" || $atributo=="all"){
			$cols.="c.nombre as nombre_c,c.codigo as codigo_c,";
		}
		if($atributo=="unidad" || $atributo=="all"){
			$cols.="u.nombre as nombre_u,u.abreviatura,u.equivalencia,";
		}
		$cols.="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.observaciones";
		$this->db->select($cols);
		$this->db->from("material m");
		$this->db->where("m.idm = $idm");
		$this->db->order_by("m.idm");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		if($atributo=="grupo" || $atributo=="all"){
			$this->db->join('grupo g','g.idg = g.idg','inner');
		}
		if($atributo=="color" || $atributo=="all"){
			$this->db->join('color c','m.idco = c.idco','inner');
		}
		if($atributo=="unidad" || $atributo=="all"){
			$this->db->join('unidad u','mi.idu = u.idu','inner');
		}
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_color($idm){// en uso, PRODUCTO,PEDIDO->REPORTE->PRODUCTO,PEDIDO->CONFIG
		$cols="m.idm,m.idmi,m.idg,m.costo_unitario,
				c.idco, c.nombre, c.codigo";
		$this->db->select($cols);
		$this->db->from("material m");
		$this->db->where("m.idm = '$idm'");
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("m.idm");
		$query=$this->db->get();
		return $query->result();
	}
	function material_proveedor(){// en uso: COMPRAS
		$cols="m.idm,m.idg,m.idco,m.costo_unitario as costo_material,
			mi.idmi,mi.idu,mi.codigo,mi.nombre as nombre_material,mi.fotografia,mi.observaciones as obs_material,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			mp.idmp,mp.costo_unitario as costo_material_proveedor,
			pr.idpro,pr.nit,pr.encargado,pr.url,
			p.ci,p.idci,p.nombre as nombre_proveedor,p.telefono,p.email,p.direccion,p.fotografia,p.caracteristicas";
		$this->db->select($cols);
		$this->db->from("material m");
		$this->db->join('material_item mi','mi.idmi=m.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		$this->db->join('material_proveedor mp','mp.idmi=mi.idmi','inner');
		$this->db->join('proveedor pr','pr.idpro=mp.idpro','inner');
		$this->db->join('persona p','p.ci=pr.nit','inner');
		$this->db->group_by("p.ci");
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}*/
	/*	
	function get_insumo_grupo_unidad_color_id($idm){
		$col="m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre as nombre_m,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material m");
		$this->db->where("m.idm = '$idm'");
		$this->db->order_by("mi.codigo", "asc");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_col($id, $col){
		$query=$this->db->query("SELECT $col FROM material WHERE idm='$id'");
		return $query->result();
	}
	function get_insumo_grupo_unidad(){
		$col="m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.idco,mi.codigo,mi.nombre as nombre_m,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g";
		$this->db->select($col);
		$this->db->from("material m");
		$this->db->order_by("mi.codigo", "asc");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_insumo_grupo_unidad_color(){
		$col="m.idm,m.idg,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre as nombre_m,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material m");
		$this->db->order_by("mi.codigo", "asc");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','i.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','i.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_insumo_grupo_unidad_color_all(){
		$col="m.idm,m.idg,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre as nombre_m,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material m");
		//$this->db->where("i.ida = '$ida'");
		$this->db->order_by("mi.codigo", "asc");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}
	function get_where_insumo_grupo($cond,$val){
		$col="m.idm,m.idg,m.idco,m.costo_unitario,
				mi.idmi,mi.codigo,mi.nombre as nombre_m,mi.fotografia,mi.observaciones,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg,g.nombre as nombre_g";
		$this->db->select($col);
		$this->db->from("material m");
		$this->db->order_by("i.cod", "asc");
		$this->db->where("i.$cond like '$val%'");
		$this->db->join('material_item mi','mi.idmi = m.idmi','inner');
		$this->db->join('grupo g','m.idg = g.idg','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_where_insumo_grupo_unidad_color_almacen($ida,$cond,$val){
		$col="i.idm, i.idusu, i.ida, i.cod,i.nombre as nombre_i,i.c_u, i.cantidad,i.descripcion as descripcion_i, i.fecha_creacion,i.fotografia,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg, g.nombre as nombre_g, g.descripcion as descripcion_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material i");
		$this->db->order_by("i.cod", "asc");
		if($cond=="idg" || $cond=="idco"){
			$this->db->where("i.$cond = '$val' and i.ida='$ida'");
		}else{
			$this->db->where("i.$cond like '$val%' and i.ida='$ida'");
		}
		
		$this->db->join('grupo g','i.idg = g.idg','inner');
		$this->db->join('unidad u','i.idu = u.idu','inner');
		$this->db->join('color c','i.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_where_insumo_grupo_unidad_color($cond,$val){
		$col="i.idm, i.idusu, i.ida, i.cod,i.nombre as nombre_i,i.c_u, i.cantidad,i.descripcion as descripcion_i, i.fecha_creacion,i.fotografia,
				u.idu, u.nombre as medida,u.abreviatura, 
				g.idg, g.nombre as nombre_g, g.descripcion as descripcion_g, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material i");
		$this->db->order_by("i.cod", "asc");
		$this->db->where("i.$cond like '$val%'");
		$this->db->join('grupo g','i.idg = g.idg','inner');
		$this->db->join('unidad u','i.idu = u.idu','inner');
		$this->db->join('color c','i.idco = c.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	function get_insumo_color_almacen(){
		$col="i.idm, i.idusu, i.cod,i.nombre as nombre_i,i.c_u, i.cantidad,i.descripcion as descripcion_i, i.fecha_creacion,i.fotografia,i.idg,
				u.idu, u.nombre as medida,u.abreviatura, 
				c.idco, c.nombre as nombre_c, c.codigo as codigo_c,
				a.ida,a.codigo as codigo_a,a.nombre as nombre_a,a.tipo,a.fotografia as fotografia_a,a.descripcion as descripcion_a";
		$this->db->select($col);
		$this->db->from("material i");
		$this->db->order_by("i.cod", "asc");
		$this->db->join('almacen a','i.ida = a.ida','inner');
		$this->db->join('unidad u','i.idu = unidad.idu','inner');
		$this->db->join('color c','i.idco = color.idco','inner');
		$query=$this->db->get();
		return $query->result();
	}

	/*function get_col_where_insumo($ida,$id,$cond,$val){
		$query=$this->db->query("SELECT * FROM insumo WHERE idm='$id' and ida='$ida' and $cond='$val'");
		return $query->result();
	}*/


	/*function get_row_n2($col,$val,$col2,$val2){
		$this->db->select("*");
	    $this->db->from("material");
	    $this->db->order_by("nombre", "asc");
	    $this->db->where("$col = '$val'");
	    if($col2=='nombre' || $col2=='cod'){
	    	$this->db->where("$col2 like '$val2%'");
	    }else{
	    	$this->db->where("$col2 = '$val2'");
	    }
	    
	    $query=$this->db->get();
		return $query->result();
	}*/

}
/* End of file M_material.php */
/* Location ./application/models/M_material.php */