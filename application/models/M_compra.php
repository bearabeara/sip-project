<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_compra extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('compra');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('compra',['idc' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM compra WHERE idc='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('compra',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val,$order,$order_by){
		$cols="idc,idus,idmi,idpr,num_doc,material,unidad,abr,cantidad,costo_total,fecha,observacion";
		$this->db->select($cols);
		$this->db->from("compra");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="idc"){
				$this->db->where("$col = '$val'");
			}
			if($col=="fecha"){
				$this->db->where("date(fecha) = '$val'");	
			}
			if($col=="material"){
				$this->db->where("$col like '%$val%'");
			}
			if($col=="where"){
				$this->db->where($val);
			}
		}
		if($order!="" && $order!=NULL && $order_by!="" && $order_by!=NULL){
			$this->db->order_by($order,$order_by);
		}
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($idus,$idmi,$idpr,$num_doc,$material,$unidad,$abr,$cantidad,$costo_total,$fecha,$observacion){
		$datos=array(
			'idus' => $idus,
			'idmi' => $idmi,
			'idpr' => $idpr,
			'num_doc' => $num_doc,
			'material' => $material,
			'unidad' => $unidad,
			'abr' => $abr,
			'cantidad' => $cantidad,
			'costo_total' => $costo_total,
			'fecha' => $fecha,
			'observacion' => $observacion
		);
		if($this->db->insert('compra',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$idmi,$idpr,$num_doc,$material,$unidad,$abr,$cantidad,$costo_total,$fecha,$observacion){
		$datos=array(
			'idmi' => $idmi,
			'idpr' => $idpr,
			'num_doc' => $num_doc,
			'material' => $material,
			'unidad' => $unidad,
			'abr' => $abr,
			'cantidad' => $cantidad,
			'costo_total' => $costo_total,
			'fecha' => $fecha,
			'observacion' => $observacion
		);
		if($this->db->update('compra',$datos,array('idc' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('compra',['idc' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_compra.php */
/* Location: ./application/models/m_compra.php*/