<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_proveedor extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get('proveedor');
		return $query->result();
	}
	function get($id){
		$this->db->order_by("razon", "asc");
		$query=$this->db->get_where('proveedor',['idpr' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM proveedor WHERE idpr='$id'");
		return $query->result();
	}
	function get_row_2($col,$val){//en uso, proveedors, 
      $query=$this->db->get_where('proveedor',[$col => $val]);
      return $query->result();
    }
	function get_row($atrib,$val){
		$query=$this->db->get_where('proveedor',[$atrib => $val]);
		return $query->result();
	}
	function get_search($col,$val){// en uso:  proveedor
		$cols="idpr,nit,razon,responsable,direccion,telefono,url,email,fotografia,descripcion";
		$this->db->select($cols);
		$this->db->from("proveedor");
		if($col!="" && $val!=""){
			if($col=="idpr"){ $this->db->where("$col = '$val'");}
			if($col=="nit"){ $this->db->where("$col like '$val%'");}
			if($col=="responsable" || $col=="razon"){ $this->db->where("$col like '%$val%'");}
		}
		$this->db->order_by("razon", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($nit,$razon,$responsable,$direccion,$telefono,$url,$email,$fotografia,$descripcion){
		$datos=array(
			'nit' => $nit,
			'razon' => $razon,
			'responsable' => $responsable,
			'direccion' => $direccion,
			'telefono' => $telefono,
			'url' => $url,
			'email' => $email,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->insert('proveedor',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($id,$nit,$razon,$responsable,$direccion,$telefono,$url,$email,$fotografia,$descripcion){
		$datos=array(
			'nit' => $nit,
			'razon' => $razon,
			'responsable' => $responsable,
			'direccion' => $direccion,
			'telefono' => $telefono,
			'url' => $url,
			'email' => $email,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->update('proveedor',$datos,array('idpr'=>$id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('proveedor',['idpr' => $id])){
			return true;
		}else{
			return false;
		}
	}
}

/* End of file m_historial_insumo.php */
/* Location: ./application/models/m_historial_insumo.php*/