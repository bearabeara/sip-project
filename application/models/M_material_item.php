<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_material_item extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('material_item');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('material_item',['idmi' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM material_item WHERE idmi='$id'");
		return $query->result();
	}
	function get_row($col,$val){// en uso: Configuracion de material->Unidad
		$query=$this->db->get_where('material_item',array($col => $val));
		return $query->result();
	}
	function get_unidad($col,$val){// en uso: PROVEEDOR,PRODUCTO
		$cols="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			u.idu, u.nombre as medida,u.abr as abr_u";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		if($col!="" && $val!=""){
			if($col=="mi.idmi"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_search($col,$val){// en uso: PROVEEDOR,PRODUCTO
		$cols="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			m.idm,m.idco,m.costo_unitario,
			u.idu, u.nombre as medida,u.abr as abr_u,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c, c.abr as abr_c";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		if($col!="" && $val!=""){
			if($col=="mi.idmi"){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('material m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','mi.idu = u.idu','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material($col,$val){// en uso: PROVEEDOR,PRODUCTO
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
			m.idm,m.idco,m.costo_unitario";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		if($col!="" && $val!=""){
			if($col=="mi.idmi" || $col=="m.idm" ){
				$this->db->where("$col = '$val'");
			}
			if($col=="mi.codigo"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->join('material m','m.idmi=mi.idmi','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_extra($col,$val){//en uso: PROVEEDOR,INSUMO
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
				u.idu,u.nombre as nombre_u,u.abr,
				m.idme,m.cantidad";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		$this->db->join('material_extra m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		if($col!="" && $col!=NULL && $val!="" && $val!=NULL){
			if($col=="mi.idmi" || $col=="m.idme"){
				$this->db->where("$col = '$val'");		
			}
			if($col=="mi.codigo" || $col=="m.cantidad"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_otro_material($col,$val){//en uso: PROVEEDOR,INSUMO
		$cols="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.descripcion,
				u.idu,u.nombre as nombre_u,u.abr,
				m.idmv";
		$this->db->select($cols);
		$this->db->from("material_item mi");
		$this->db->join('material_vario m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');
		if($col!="" && $col!=NULL && $val!="" && $val!=NULL){
			if($col=="mi.idmi" || $col=="m.idmv"){
				$this->db->where("$col = '$val'");		
			}
			if($col=="mi.codigo" || $col=="m.cantidad"){
				$this->db->where("$col like '$val%'");
			}
			if($col=="mi.nombre"){
				$this->db->where("$col like '%$val%'");
			}
		}
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	/*function get_material(){// en uso: PROVEEDOR,PRODUCTO
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.fotografia,
			m.idm,m.idmi,m.idg,
			c.idco, c.nombre as nombre_c, c.codigo as codigo_c";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('material m','m.idmi=mi.idmi','inner');
		$this->db->join('color c','m.idco = c.idco','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}

	function get_activo_fijo(){
		$col="mi.idmi,mi.codigo,mi.nombre,mi.fotografia,mi.fotografia,
			m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('activo_fijo m','m.idmi=mi.idmi','inner');
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_material_vario(){
		$col="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.fotografia,mi.observaciones,
			u.idu,u.nombre as nombre_u,u.abreviatura,
			m.idmv";
		$this->db->select($col);
		$this->db->from("material_item mi");
		$this->db->join('material_varios m','m.idmi=mi.idmi','inner');
		$this->db->join('unidad u','u.idu=mi.idu','inner');		
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}
	function get_row_search($col,$val,$tipo){// en uso: PROVEEDOR
		$cols="mi.idmi,mi.idu,mi.codigo,mi.nombre,mi.fotografia,mi.fotografia,";
		if($tipo=="material"){ $cols.="m.idm,m.idmi,m.idg,m.idco,c.idco, c.nombre as nombre_c, c.codigo as codigo_c";}
		if($tipo=="material_adicional"){ $cols.="m.idma";}
		if($tipo=="material_varios"){ $cols.="m.idmv";}
		if($tipo=="activo_fijo"){ $cols.="m.idaf,m.iddpre,m.costo,m.fecha_inicio_trabajo";}
		$this->db->select($cols);
		$this->db->from("material_item mi");
		if($tipo=="material"){$this->db->join('material m','m.idmi=mi.idmi','inner');$this->db->join('color c','m.idco = c.idco','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="material_adicional"){$this->db->join('material_adicional m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="material_varios"){$this->db->join('material_varios m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		if($tipo=="activo_fijo"){$this->db->join('activo_fijo m','m.idmi=mi.idmi','inner'); $this->db->where("$col like '$val%'");}
		$this->db->order_by("mi.nombre");
		$query=$this->db->get();
		return $query->result();
	}*/
	function insertar($idmi,$idu,$codigo,$nombre,$fotografia,$descripcion){
		$datos=array(
			'idmi' => $idmi,
			'idu' => $idu,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->insert('material_item',$datos)){
			return true;
		}else{
			return false;
		}
	}
	function modificar($idmi,$idu,$codigo,$nombre,$fotografia,$descripcion){
		$datos=array(
			'idu' => $idu,
			'codigo' => $codigo,
			'nombre' => $nombre,
			'fotografia' => $fotografia,
			'descripcion' => $descripcion
		);
		if($this->db->update('material_item',$datos,array('idmi'=>$idmi))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('material_item',['idmi' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){// en uso, ACTIVO FIJO,
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM material_item");
		$max=$query->result();
		return $max[0]->max*1;
	}	
	
}

/* End of file m_material_item.php */
/* Location: ./application/models/m_material_item.php*/