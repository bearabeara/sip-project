<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_detalle_pedido extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_all(){
		$query=$this->db->get('detalle_pedido');
		return $query->result();
	}
	function get($id){
		$query=$this->db->get_where('detalle_pedido',['iddp' => $id]);
		return $query->result();
	}
	function get_col($id,$col){
		$query=$this->db->query("SELECT $col FROM detalle_pedido WHERE iddp='$id'");
		return $query->result();
	}
	function get_row($col,$val){
		$query=$this->db->get_where('detalle_pedido',[$col => $val]);
		return $query->result();
	}
	function get_search($col,$val){
		$c_cv="`".$this->lib->encriptar_str("cv")."`";
		$c_cu="`".$this->lib->encriptar_str("cu")."`";
		$cols="iddp,idpp,idpgrc,observacion,".$c_cu." as cu,".$c_cv." as cv,porcentaje,posicion";
		$this->db->select($cols);
		$this->db->from("detalle_pedido");
		if($col!="" && $val!="" && $col!=NULL && $val!=NULL){
			if($col=="iddp" || $col=="idpp"){ $this->db->where("$col = '$val'"); }
		}
		$this->db->order_by("iddp", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function producto_pedido_empleado($col,$val){
		$c_cv="`".$this->lib->encriptar_str("cv")."`";
		$c_cu="`".$this->lib->encriptar_str("cu")."`";
		$c_cantidad="`".$this->lib->encriptar_str("cantidad")."`";
		$c_terminado="`".$this->lib->encriptar_str("terminado")."`";
		$cols="dp.iddp,dp.idpp,dp.idpgrc,dp.observacion as observacion_detalle,dp.".$c_cu." as cu,dp.".$c_cv." as cv,dp.porcentaje,posicion,
			sdp.idsdp,sdp.idsc,sdp.".$c_cantidad." as cantidad_sucursal,
			ppe.idppe,ppe.idpre,ppe.".$c_cantidad." as cantidad,ppe.".$c_terminado." as terminado,ppe.fecha_inicio,ppe.fecha_fin,ppe.observacion,ppe.ide,ppe.idpr,ppe.proceso,ppe.tipo";
		$this->db->select($cols);
		$this->db->from("detalle_pedido dp");
		if($col!="" && $val!=""){
			if($col=="dp.iddp" || $col=="dp.idpp" || $col=="sdp.idsdp"){ $this->db->where("$col = '$val'"); }
		}
		$this->db->join('sucursal_detalle_pedido sdp','sdp.iddp = dp.iddp','inner');
		$this->db->join('producto_pedido_empleado ppe','ppe.idsdp = sdp.idsdp','inner');
		$this->db->order_by("dp.iddp", "asc");
		$this->db->order_by("sdp.idsdp", "asc");
		$query=$this->db->get();
		return $query->result();
	}
	function insertar($iddp,$idpp,$idpgrc,$posicion,$observacion,$cu,$cv){
		$col_cu=$this->lib->encriptar_str("cu");
		$col_cv=$this->lib->encriptar_str("cv");
		$enc_cu=$this->lib->encriptar_num($cu);
		$enc_cv=$this->lib->encriptar_num($cv);
		$datos=array(
			'iddp' => $iddp,
			'idpp' => $idpp,
			'idpgrc' => $idpgrc,
			'posicion' => $posicion,
			'observacion' => $observacion,
			$col_cu => $enc_cu,
			$col_cv => $enc_cv
		);
		if($this->db->insert('detalle_pedido',$datos)){
			return true;
		}else{
			return false;
		}
	}

	function modificar($id,$posicion,$observacion,$cu,$cv){
		$col_cu=$this->lib->encriptar_str("cu");
		$col_cv=$this->lib->encriptar_str("cv");
		$enc_cu=$this->lib->encriptar_num($cu);
		$enc_cv=$this->lib->encriptar_num($cv);
		if($posicion!="none"){
			$datos=array(
				'posicion' => $posicion,
				'observacion' => $observacion,
				$col_cu => $enc_cu,
				$col_cv => $enc_cv
			);
		}else{
			$datos=array(
				'observacion' => $observacion,
				$col_cu => $enc_cu,
				$col_cv => $enc_cv
			);
		}
		
		if($this->db->update('detalle_pedido',$datos,array('iddp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function modificar_row($id,$col,$val){
		$datos=array( $col => $val );
		if($this->db->update('detalle_pedido',$datos,array('iddp' => $id))){
			return true;
		}else{
			return false;
		}
	}
	function eliminar($id){
		if($this->db->delete('detalle_pedido',['iddp' => $id])){
			return true;
		}else{
			return false;
		}
	}
	function max($col){
		$query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM detalle_pedido");
		$max=$query->result();
		return $max[0]->max*1;
	}
}
/* End of file m_detalle_pedido.php */
/* Location: ./application/models/m_detalle_pedido.php*/