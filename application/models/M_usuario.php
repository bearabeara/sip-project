<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_usuario extends CI_Model {
	function __construct(){
        parent::__construct();
    }
    function get_all(){
      $query=$this->db->get_where('usuario');
      return $query->result();
   	}
    function get_col($col,$val){
      $query=$this->db->query("SELECT $col FROM usuario WHERE idus='$val'");
      return $query->result();
    }
    function get_row($col,$val){
      $query=$this->db->get_where('usuario',[$col=>$val]);
      return $query->result();
    }
    function get_complet_all(){
      $cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.cargo,p.telefono,p.email,p.fotografia,p.descripcion,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
      u.idus,u.usuario,u.password,u.tipo, u.fecha_ingreso";
      $this->db->select($cols);
      $this->db->from("usuario u");
      $this->db->order_by("p.nombre", "asc");
      $this->db->join('persona p','u.ci = p.ci','inner');
      $query=$this->db->get();
      return $query->result();
    }
    function get($idus){
    	$query=$this->db->get_where('usuario',['idus'=>$idus]);
    	return $query->result();
   	}
    function get_persona($idus){
      $cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
      u.idus";
      $this->db->select($cols);
      $this->db->from("usuario u");
      $this->db->order_by("p.nombre", "asc");
      $this->db->join('persona p','u.ci = p.ci','inner');
      if($idus!="" && $idus!=NULL){ $this->db->where("u.idus = '$idus'"); }
      $query=$this->db->get();
      return $query->result();
    }
    function get_search($atrib,$val){
      $cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
      u.idus, u.usuario";
      $this->db->select($cols);
      $this->db->from("usuario u");
      $this->db->order_by("p.nombre", "asc");
      $this->db->join('persona p','u.ci = p.ci','inner');
      if($atrib!="" && $atrib!=NULL && $val!="" && $val!=NULL){
        if($atrib=="u.usuario"){
          $this->db->where("$atrib = '$val'");
        }
      }
      $query=$this->db->get();
      return $query->result();
    }
    function get_privilegio($idus,$grupo){
      $cols="u.idus,u.ci,u.usuario,u.tipo";
      $c0="p.al,p.pr,p.mo,p.ca,p.cl,p.ac,p.ot,p.co,p.ad";
      $c1="p.mo1c, p.mo1r, p.mo1u, p.mo1d, p.mo1p, p.mo1a";
      $c2="p.mo2c, p.mo2r, p.mo2u, p.mo2d, p.mo2p";
      $c3="p.mo3c, p.mo3r, p.mo3u, p.mo3d, p.mo3p";
      
      switch ($grupo) {
        case 'pedido': if($cols!=""){$cols.=",";} $cols.=$c0.",".$c1; break;
        case 'venta': if($cols!=""){$cols.=",";} $cols.=$c0.",".$c1; break;
        case 'compra': if($cols!=""){$cols.=",";} $cols.=$c0.",".$c1; break;
        default: if($cols!=""){$cols.=",";} $cols.=$c0.",".$c1.",".$c2.",".$c3; break;
      }
      $this->db->select($cols);
      $this->db->from("usuario u");
      $this->db->join('privilegio p','p.idus = u.idus','inner');
      if($idus!=""){ $this->db->where("u.idus = '$idus'"); }
      $query=$this->db->get();
      return $query->result();
    }

    function get_row_complet($col,$val){
      $cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,ci.abreviatura,
            u.idus,u.usuario,u.tipo";
      $this->db->select($cols);
      $this->db->from("usuario u");
      $this->db->join("persona p","u.ci=p.ci","inner");
      $this->db->join('ciudad ci','ci.idci = p.idci','inner');
      $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      $this->db->where("$col = '$val'");
      $query=$this->db->get();
      return $query->result();
    }
    function validate($login,$pass){
      $this->db->reconnect();
      $salt="$2a$10$".sha1($pass)."$";
      $password=crypt($pass,$salt);
      $query=$this->db->get_where('usuario',['usuario'=>$login, 'password'=>$password]);
      return $query->result();
    }
    function search_usuarios($col,$val,$session){// en us USUARIOS
      $cols="p.ci,p.nombre,p.nombre2,p.paterno,p.materno,p.telefono,p.email,p.fotografia,p.descripcion,p.cargo,CONCAT(p.nombre,' ',p.nombre2,' ',p.paterno,' ',p.materno) as nombre_completo,
            pa.idpa,pa.nombre as pais,
            ci.idci,ci.nombre as ciudad,ci.abreviatura,
            u.idus,u.usuario,u.tipo, u.fecha_ingreso";
        $this->db->select($cols);
        $this->db->from("usuario u");
        $this->db->join("persona p","u.ci=p.ci","inner");
        $this->db->join('ciudad ci','ci.idci = p.idci','inner');
        $this->db->join('pais pa','pa.idpa = ci.idpa','inner');
      if($col!="" && $val!=""){
        if($col=="u.idus"){
            $this->db->where("$col = '$val'");        
        }
        if($col=="p.ci" || $col=="p.telefono"){
          $this->db->where("$col like '$val%'");
        }
        if($col=="p.cargo" || $col=="u.usuario"){
          $this->db->where("$col like '%$val%'");
        }
        if($col=="nombre"){
          $this->db->where("(p.nombre like '$val%' || p.nombre2 like '$val%' || p.paterno like '$val%' || p.materno like '$val%')");
        }
      }
      if(!$session){ $this->db->where("u.idus != ".$this->session->userdata('id')); }
      $this->db->order_by("p.nombre");
      $this->db->order_by("p.nombre2");
      $this->db->order_by("p.paterno");
      $query=$this->db->get();
      return $query->result();
    }
    function insertar($idus,$ci,$usuario,$tipo){//en uso:capital humano
        $salt="$2a$10$".sha1($ci)."$";
        $password=crypt($ci,$salt);
        $data = array(
          'idus' => $idus,
          'ci' => $ci,
          'usuario' => $usuario,
          'tipo' => $tipo,
          'password' => $password
        );
       if ($this->db->insert('usuario', $data)){
          return true;
       }else{
          return false;
       }
    }
    function modificar($id,$ci,$usuario,$tipo){//en uso:capital humano, 
        $data = array(
          'ci' => $ci,
          'usuario' => $usuario,
          'tipo' => $tipo
        );
        if ($this->db->update('usuario', $data, array('idus' => $id))){
          return true;
        }else{
          return false;
        }
    }
    function modificar_row($id,$col,$val){//en uso:capital humano, 
        $data = array( ''.$col => $val );
        if ($this->db->update('usuario', $data, array('idus' => $id))){
          return true;
        }else{
          return false;
        }
    }
    public function modificar_password($id,$ci){
      $salt="$2a$10$".sha1($ci)."$";
      $password=crypt($ci,$salt);
      $data = array(
        'password' => $password
      );
      if($this->db->update('usuario',$data,array('idus' => $id))){
        return true;
      }else{
        return false;
      }
    }
  function eliminar($id){
      if($this->db->delete('usuario',['idus'=>$id])){
          return true;
      }else{
          return false;
      }
  }
  function max_col($col){
    $query=$this->db->query("SELECT IFNULL(max($col),0) as max FROM usuario");
    $result=$query->result();
    return $result[0]->max*1;
  } 
}

/* End of file m_usuario.php */
/* Location: ./application/models/m_usuario.php */