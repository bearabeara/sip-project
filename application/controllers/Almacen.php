<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Almacen extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if($privilegio[0]->al==1){
				if(!isset($_GET['p'])){
					if($privilegio[0]->al1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->al2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->al3r==1){ 
								$listado['pestania']=3;
							}else{
								$listado['pestania']="";
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
				$this->load->view('v_almacen',$listado);
			}else{
				$this->val->redireccion($privilegio,$this->session->userdata("tipo"));
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE ALMACENES -------*/
	public function search_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"almacen");
			if(!empty($privilegio)){
				$listado['privilegio']=$privilegio;
				$this->load->view('almacen/almacenes/search',$listado);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";$type_search="";
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"almacen");
			if(!empty($privilegio)){
				if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['tip'])){
					if($_POST['cod']!=""){
						$atrib="codigo";$val=$_POST['cod'];$type_search="like";
					}else{
						if($_POST['nom']!=""){
							$atrib="nombre";$val=$_POST['nom'];$type_search="like";
						}else{
							if($_POST['tip']!=""){
								$atrib="tipo";$val=$_POST['tip'];$type_search="equals";
							}
						}
					}
				}
				$listado['privilegio']=$privilegio[0];
				$listado['atrib']=$atrib;
				$listado['type_search']=$type_search;
				$listado['val']=$val;
				$listado['almacenes']=$this->M_almacen->get_search("","");
				$this->load->view('almacen/almacenes/view',$listado);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
   	public function new_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('almacen/almacenes/3-nuevo/form');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"almacen");
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1" && $privilegio[0]->al1r=="1" && $privilegio[0]->al1c=="1"){
					if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['tip']) && isset($_POST['des'])){
						$cod=trim($_POST['cod'])."";
						$nom=trim($_POST['nom'])."";
						$tip=trim($_POST['tip'])."";
						$des=trim($_POST['des'])."";
						if($this->val->strSpace($cod,2,10) && $this->val->strSpace($nom,2,100) && ($tip=="0" || $tip=="1")){
							$control=true;
							if($des!=""){if(!$this->val->textarea($des,0,400)){ $control=false;}}
							if($control){
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/almacenes/','',$this->resize,NULL);
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									$ida=$this->M_almacen->max('ida')+1;
									if($this->M_almacen->insertar($ida,$cod,$nom,$tip,$img,$des)){
										//notificacion
										if($this->M_almacen_seg->insertar($ida,$nom,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"insert")){}
										$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"almacen");
										foreach ($resultados as $key => $result) {
											if($result->idno=="none"){
												if($this->M_notificacion->insertar_almacen($result->emisor,$result->reseptor,$result->cantidad)){}
											}else{
												if($this->M_notificacion->modificar_row($result->idno,"almacen",$result->cantidad)){}
											}
										}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['tip'])){
				$listado['visibles']=json_decode($_POST['visibles']);
				$listado['almacenes']=$this->M_almacen->get_all();
				$listado['tbl']=trim($_POST['tbl']);
				$listado['cod']=$_POST['cod'];
				$listado['nom']=$_POST['nom'];
				$listado['tip']=$_POST['tip'];
				$this->load->view('almacen/almacenes/4-imprimir/print_almacen',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function excel_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";
				if(isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['tip'])){
					if(trim($_GET['cod'])!=""){
						$atrib="a.codigo";$val=trim($_GET['cod']);
					}else{
						if(trim($_GET['nom'])!=""){
							$atrib="a.nombre";$val=trim($_GET['nom']);
						}else{
							if(trim($_GET['tip'])!=""){
								$atrib="a.tipo";$val=trim($_GET['tip']);
							}
						}
					}
				}
				$listado['almacenes']=$this->M_almacen->get_search($atrib,$val);
				$this->load->view('almacen/almacenes/4-imprimir/excel',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function word_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";$type_search="";
				if(isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['tip'])){
					if(trim($_GET['cod'])!=""){
						$atrib="a.codigo";$val=trim($_GET['cod']);
					}else{
						if(trim($_GET['nom'])!=""){
							$atrib="a.nombre";$val=trim($_GET['nom']);
						}else{
							if(trim($_GET['tip'])!=""){
								$atrib="a.tipo";$val=trim($_GET['tip']);
							}
						}
					}
				}
				$listado['almacenes']=$this->M_almacen->get_search($atrib,$val);
				$this->load->view('almacen/almacenes/4-imprimir/word',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function reporte_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['a'])){
				$ida=trim($_POST['a']);
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['almacen']=$almacen;
					$this->load->view('almacen/almacenes/5-reporte/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST["a"])){
				$ida=trim($_POST['a']);
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['almacen']=$almacen[0];
					$this->load->view('almacen/almacenes/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	
	public function update_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"almacen");
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1" && $privilegio[0]->al1r=="1" && $privilegio[0]->al1u=="1"){
					if(isset($_POST['a']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['des'])){
						$id=trim($_POST['a'])."";
						$cod=trim($_POST['cod'])."";
						$nom=trim($_POST['nom'])."";
						$des=trim($_POST['des'])."";
						if($this->val->entero($id,0,10) && $this->val->strSpace($cod,2,10) && $this->val->strSpace($nom,2,100)){
							$control=true;
							if($des!=""){if(!$this->val->textarea($des,0,400)){ $control=false;}}
							if($control){
								$almacen=$this->M_almacen->get($id);
								if(!empty($almacen)){
									$img=$almacen[0]->fotografia;
									$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/almacenes/','',$this->resize,$img,$id);
									if($img!='error' && $img!="error_type" && $img!="error_size_img"){
										if($this->M_almacen->modificar($id,$cod,$nom,$img,$des)){
											//notificacion
											if($this->M_almacen_seg->insertar($id,$nom,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"update")){}
												$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"almacen");
												foreach ($resultados as $key => $result) {
													if($result->idno=="none"){
														if($this->M_notificacion->insertar_almacen($result->emisor,$result->reseptor,$result->cantidad)){}
													}else{
														if($this->M_notificacion->modificar_row($result->idno,"almacen",$result->cantidad)){}
													}
												}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
										}else{
											echo "error";
										}
									}else{
										echo $img;
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['a'])){
				$ida=trim($_POST['a']);
				$url='./libraries/img/almacenes/miniatura/';
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['titulo']="eliminar el <b>".$almacen[0]->nombre."</b>";
					if($almacen[0]->tipo=='0'){
						$control=$this->M_almacen_material->get_row('ida',$ida);
					}else{
						$control=$this->M_almacen_producto->get_row('ida',$ida);
					}
					if(!empty($control)){
						$listado['control']=$control;
						$listado['open_control']="false";
						$listado['desc']="Imposible eliminar, vacie el almacen antes de eliminarlo";
						$listado['btn_ok']="none";
					}else{
						$listado['desc']="Se eliminara definitivamente";
					}
					$img='default.png';
					if($almacen[0]->fotografia!=NULL && $almacen[0]->fotografia!=""){ $img=$almacen[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_almacen(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"almacen");
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1" && $privilegio[0]->al1r=="1" && $privilegio[0]->al1d=="1"){
					if(isset($_POST['a']) && isset($_POST['u']) && isset($_POST['p'])){
						$id=trim($_POST['a']);
						$u=trim($_POST['u']);
						$p=trim($_POST['p']);
						if($u==$this->session->userdata("login")){
							$usuario=$this->M_usuario->validate($u,$p);
							if(!empty($usuario)){
								$almacen=$this->M_almacen->get($id);
								if(!empty($almacen)){
									if($this->lib->eliminar_imagen($almacen[0]->fotografia,'./libraries/img/almacenes/')){
										if($this->M_almacen->eliminar($id)){
											if($this->M_almacen_seg->insertar($id,$almacen[0]->nombre,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"delete")){}
											$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"almacen");
											foreach ($resultados as $key => $result) {
												if($result->idno=="none"){
													if($this->M_notificacion->insertar_almacen($result->emisor,$result->reseptor,$result->cantidad)){}
												}else{
													if($this->M_notificacion->modificar_row($result->idno,"almacen",$result->cantidad)){}
												}
											}
											$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
											echo json_encode($result);
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE ALMACENES -------*/
/*------- MANEJO DE MOVIMIENTO DE MATERIAL -------*/
	public function search_movimiento_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['privilegio']=$this->M_privilegio->get_almacen($this->session->userdata("id"),"historial_material");
			$this->load->view('almacen/movimiento_material/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_movimiento_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"historial_material");
			if(!empty($privilegio)){
				if(isset($_POST['nom']) && isset($_POST['alm']) && isset($_POST['usu']) && isset($_POST['sol']) && isset($_POST['fech_a']) && isset($_POST['fech_b']) && isset($_POST['tip'])){
					$nom=trim($_POST['nom']);
					$alm=trim($_POST['alm']);
					$usu=trim($_POST['usu']);
					$sol=trim($_POST['sol']);
					$fech_a=trim($_POST['fech_a']);
					$fech_b=trim($_POST['fech_b']);
					$tip=trim($_POST['tip']);
					if($nom!=""){$atrib.="material like '".$nom."%'"; }
					if($alm!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="almacen like '%".$alm."%'"; }
					if($usu!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="usuario like '%".$usu."%'"; }
					if($sol!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="solicitante like '%".$sol."%'"; }
					if($this->val->fecha($fech_a) || $this->val->fecha($fech_b)){
						if($this->val->fecha($fech_a)){
							if($this->val->fecha($fech_b)){
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) >= '".$fech_a."' and date(fecha_usuario) <= '".$fech_b."'";
							}else{
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_a."'"; 
							}
						}else{
							if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_b."'";
						}
					}
					if($tip==1){if($atrib!=""){$atrib.=" and ";}$atrib.="ingreso != '0'";}
					if($tip==2){if($atrib!=""){$atrib.=" and ";}$atrib.="salida != '0'";}
				}
				$listado['movimientos']=$this->M_almacen_material_mov->get_search($atrib);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('almacen/movimiento_material/view',$listado);	
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Imprimir ---*/
   	public function print_movimiento_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['nom']) && isset($_POST['alm']) && isset($_POST['usu']) && isset($_POST['sol']) && isset($_POST['fech_a']) && isset($_POST['fech_b'])){
				$listado['visibles']=json_decode($_POST['visibles']);
				$listado['tbl']=trim($_POST['tbl']);
				$listado['nom']=trim($_POST['nom']);
				$listado['alm']=trim($_POST['alm']);
				$listado['usu']=trim($_POST['usu']);
				$listado['sol']=trim($_POST['sol']);
				$listado['fech_a']=trim($_POST['fech_a']);
				$listado['fech_b']=trim($_POST['fech_b']);
				$listado['tip']=trim($_POST['tip']);
				$listado['movimientos']=$this->M_almacen_material_mov->get_all();
				$this->load->view('almacen/movimiento_material/4-imprimir/print_movimiento_material',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function excel_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";
				if(isset($_GET['nom']) && isset($_GET['alm']) && isset($_GET['usu']) && isset($_GET['sol']) && isset($_GET['fech_a']) && isset($_GET['fech_b']) && isset($_GET['tip'])){
					$nom=trim($_GET['nom']);
					$alm=trim($_GET['alm']);
					$usu=trim($_GET['usu']);
					$sol=trim($_GET['sol']);
					$fech_a=trim($_GET['fech_a']);
					$fech_b=trim($_GET['fech_b']);
					$tip=trim($_GET['tip']);
					if($nom!=""){$atrib.="material like '".$nom."%'"; }
					if($alm!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="almacen like '%".$alm."%'"; }
					if($usu!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="usuario like '%".$usu."%'"; }
					if($sol!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="solicitante like '%".$sol."%'"; }
					if($this->val->fecha($fech_a) || $this->val->fecha($fech_b)){
						if($this->val->fecha($fech_a)){
							if($this->val->fecha($fech_b)){
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) >= '".$fech_a."' and date(fecha_usuario) <= '".$fech_b."'";
							}else{
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_a."'"; 
							}
						}else{
							if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_b."'";
						}
					}
					if($tip==1){if($atrib!=""){$atrib.=" and ";}$atrib.="ingreso != '0'";}
					if($tip==2){if($atrib!=""){$atrib.=" and ";}$atrib.="salida != '0'";}
				}
				$listado['movimientos']=$this->M_almacen_material_mov->get_search($atrib);
				$this->load->view('almacen/movimiento_material/4-imprimir/excel_material',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function word_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";
				if(isset($_GET['nom']) && isset($_GET['alm']) && isset($_GET['usu']) && isset($_GET['sol']) && isset($_GET['fech_a']) && isset($_GET['fech_b']) && isset($_GET['tip'])){
					$nom=trim($_GET['nom']);
					$alm=trim($_GET['alm']);
					$usu=trim($_GET['usu']);
					$sol=trim($_GET['sol']);
					$fech_a=trim($_GET['fech_a']);
					$fech_b=trim($_GET['fech_b']);
					$tip=trim($_GET['tip']);
					if($nom!=""){$atrib.="material like '".$nom."%'"; }
					if($alm!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="almacen like '%".$alm."%'"; }
					if($usu!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="usuario like '%".$usu."%'"; }
					if($sol!=""){if($atrib!=""){ $atrib.=" and ";}$atrib.="solicitante like '%".$sol."%'"; }
					if($this->val->fecha($fech_a) || $this->val->fecha($fech_b)){
						if($this->val->fecha($fech_a)){
							if($this->val->fecha($fech_b)){
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) >= '".$fech_a."' and date(fecha_usuario) <= '".$fech_b."'";
							}else{
								if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_a."'"; 
							}
						}else{
							if($atrib!=""){ $atrib.=" and ";}$atrib.="date(fecha_usuario) = '".$fech_b."'";
						}
					}
					if($tip==1){if($atrib!=""){$atrib.=" and ";}$atrib.="ingreso != '0'";}
					if($tip==2){if($atrib!=""){$atrib.=" and ";}$atrib.="salida != '0'";}
				}
				$listado['movimientos']=$this->M_almacen_material_mov->get_search($atrib);
				$this->load->view('almacen/movimiento_material/4-imprimir/word_material',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_movimiento_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$listado['titulo']="eliminar el historial de movimiento de los almacenes de materiales";
				$listado['desc']="Todos los registros se eliminaran definitivamente";
				$this->load->view('estructura/form_eliminar',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_movimiento_material(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"historial_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1" && $privilegio[0]->al2r=="1" && $privilegio[0]->al2d=="1"){
					if(isset($_POST['u']) && isset($_POST['p'])){
						$u=$_POST['u'];
						$p=$_POST['p'];
						if($u==$this->session->userdata("login")){
							$usuario=$this->M_usuario->validate($u,$p);
							if(!empty($usuario)){
								if($this->M_almacen_material_mov->eliminar_all()){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}   			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MOVIMIENTO DE MATERIAL -------*/
/*------- MANEJO DE MOVIMIENTO DE PRODUCTO -------*/
/*------- END MANEJO DE MOVIMIENTO DE PRODUCTO -------*/
/*----MANEJO DE FUNCIONES PARA LOS ALMACENES DE MATERIALES ----*/

	public function material($ida){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(!empty($ida)){
					$exist=$this->M_almacen->get_id_mat($ida,'material');
					if(!empty($exist)){
						$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
							if(!isset($_GET['p'])){
								$listado['pestania']=1;
							}else{
								$listado['pestania']=$_GET['p'];
							}
							$listado['privilegio']=$privilegio;
							$listado['almacen']=$ida;
			date_default_timezone_set("America/La_Paz");
			if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
							$this->load->view('v_material',$listado);
					}else{
						echo "error";
					}
				}else{
					redirect(base_url(),301);
				}
			}else{
				redirect(base_url(),301);
			}
		
	}

//FUNCION PARA MANEJO DE ALMACEN DE PRODUCTOS
	public function producto($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(!empty($ida)){
				$exist=$this->M_almacen->get_id_mat($ida,'producto');
				if(!empty($exist)){
						$privilegio = $this->M_privilegio->get_row("idus",$this->session->userdata("id"));
						if($privilegio[0]->al=="1"){
							if(!isset($_GET['p'])){
								$listado['pestania']=1;
							}else{
								$listado['pestania']=$_GET['p'];
							}
							$listado['privilegio']=$privilegio;
							$listado['almacen']=$ida;
							$this->load->view('v_producto',$listado);
						}else{
							$this->val->redireccion($privilegio,$this->session->userdata("tipo"));
						}
				}else{
					echo "error";
				}
			}else{
				redirect(base_url(),301);
			}
		}else{
			redirect(base_url(),301);
		}	
	}
}
/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */