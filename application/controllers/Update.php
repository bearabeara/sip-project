<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Update extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){ //validamos usuario
		if(!empty($this->session_id)){
			if(!empty($this->actualizaciones)){
				$listado['actualizacion']=$this->actualizaciones[0];
				$this->load->view('estructura/controls/update/view',$listado);
			}else{
				redirect(base_url());
			}
		}else{
			redirect(base_url().'login/input',301);
		}
	}
	/*control de alertas*/
	public function terminar_actualizacion(){
		if(isset($_POST['ac'])){
			$idac=trim($_POST['ac']);
			$actualizacion=$this->M_actualizacion->get($idac);
			if(!empty($actualizacion)){
				date_default_timezone_set("America/La_Paz");
				$fecha_ahora = strtotime(date('Y-m-d H:i:s'));
				$fecha_fin = strtotime($actualizacion[0]->fecha_fin);
				if($fecha_fin<=$fecha_ahora){
					if($actualizacion[0]->estado!=2){
						if($this->M_actualizacion->modificar_row($idac,"estado","2")){
							echo "actualizado";
						}else{
							echo "error";
						}
					}else{
						echo "actualizado";
					}
				}else{
					echo date('Y-m-d H:i:s')."|".$actualizacion[0]->fecha_fin;
				}
			}else{
				echo "fail";
			}
		}else{
			echo "fail";
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */