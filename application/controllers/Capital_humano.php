<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Capital_humano extends CI_Controller{
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if($privilegio[0]->ca==1){
				if(!isset($_GET['p'])){
					if($privilegio[0]->ca1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->ca2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->ca5r==1){ 
								$listado['pestania']=5;
							}else{
								$listado['pestania']="";
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
				$this->load->view('v_capital_humano',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE CAPITAL HUMANO -------*/
	public function search_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['tipo_contratos']=$this->M_tipo_contrato->get_all();
			$listado['privilegio']=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"empleado");
			$this->load->view('capital_humano/capital_humano/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"");
			if(!empty($privilegio)){
				$atrib="";$val="";$estado=1;$type_search="";
				if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['ci']) && isset($_POST['con']) && isset($_POST['tip'])){
					if($_POST['cod']!=""){
						$atrib='codigo';$val=$_POST['cod'];$type_search="like";
					}else{
						if($_POST['nom']!=""){
							$atrib='nombre_completo';$val=$_POST['nom'];$type_search="like";
						}else{
							if($_POST['ci']!=""){
								$atrib='ci';$val=$_POST['ci'];$type_search="like";
							}else{
								if($_POST['con']!=""){
									$atrib='idtc';$val=$_POST['con'];$type_search="equals";
								}else{
									if($_POST['tip']!=""){
										$atrib='tipo';$val=$_POST['tip'];$type_search="equals";
									}
								}
							}
						}
					}
				}
				if(isset($_POST['est'])){$estado=$_POST['est'];}
				//$listado['empleados']=$this->M_empleado->get_empleado($atrib,$val,true,$estado);
				$listado['empleados']=$this->M_empleado->get_empleado(NULL,NULL,true,NULL);
				$listado['atrib']=$atrib;
				$listado['val']=$val;
				$listado['type_search']=$type_search;
				$listado['estado']=$estado;
			 	$listado['privilegio']=$privilegio[0];
				$this->load->view("capital_humano/capital_humano/view",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
	public function new_empleado(){
		 if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		 		$listado['contrato']=$this->M_tipo_contrato->get_all();
		 		$listado['ciudades']=$this->M_ciudad->get_all();
				$this->load->view("capital_humano/capital_humano/3-nuevo/view",$listado);
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
	}
	public function search_ci(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci']) && isset($_POST['type'])){
				$ci=trim($_POST['ci']);
				$type=trim($_POST['type']);
				if($ci!=""){
					$persona=$this->M_persona->get($ci);
					if(!empty($persona)){
						$persona=$persona[0];
						$control=$this->M_ciudad->get($persona->idci);
						if(!empty($control)){
							if($type=="directivo"){
								echo json_encode(array("0"=>$persona->fotografia,"1"=>$persona->idci,"2"=>$persona->nombre,"3"=>$persona->nombre2,"4"=>$persona->paterno,"5"=>$persona->materno,"6"=>$persona->telefono,"7"=>$persona->email,"8"=>$persona->descripcion));
							}
							if($type=="empleado"){
								echo json_encode(array("0"=>$persona->fotografia,"1"=>$persona->idci,"2"=>$persona->nombre,"3"=>$persona->nombre2,"4"=>$persona->paterno,"5"=>$persona->materno,"6"=>$persona->cargo,"7"=>$persona->telefono,"8"=>$persona->email,"9"=>$persona->descripcion));
							}
						}
					}else{
						
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['cod']) && isset($_POST['te']) && isset($_POST['car']) && isset($_POST['gra']) && isset($_POST['tc']) && isset($_POST['sal']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['ing']) && isset($_POST['dir']) && isset($_POST['obs']) && isset($_FILES)){
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$cod=trim($_POST['cod']);
				$te=trim($_POST['te']);
				$gra=trim($_POST['gra']);
				$car=trim($_POST['car']);
				$tc=trim($_POST['tc']);
				$sal=trim($_POST['sal']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$nac=trim($_POST['nac']);
				$ing=trim($_POST['ing']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20) && $this->val->entero($te,0,1) && $te>=0 && $te<=1 && $this->val->entero($tc,0,10) && $this->val->decimal($sal,4,1) && $sal>=0 && $sal<=9999.9){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($cod!=""){ if(!$this->val->entero($cod,0,10) || $cod<0 || $cod>9999999999){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($gra!=""){ if(!$this->val->strSpace($gra,0,200)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($ing!=""){ if(!$this->val->fecha($ing)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,800)){ $control=false;}}
					if($control){
						$control=$this->M_tipo_contrato->get($tc);
						if(!empty($control)){
							$control=$this->M_ciudad->get($ciu);
							if(!empty($control)){
								$control=$this->M_persona->get($ci);
								if(empty($control)){
									$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$ci);
									if($img!='error' && $img!="error_type" && $img!="error_size_img"){
										if($this->M_persona->insertar($ci,$ciu,$nom1,$nom2,$pat,$mat,$car,$tel,$ema,$img,$obs)){
											if($this->M_empleado->insertar($tc,$ci,$cod,$sal,$ing,$nac,$te,$dir,$gra)){
												echo "ok";
											}else{
												$this->M_persona->eliminar($nit);
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										echo $img;
									}
								}else{
									$control=$this->M_empleado->get_row('ci',$ci);
									if(empty($control)){
										if($this->M_empleado->insertar($tc,$ci,$cod,$sal,$ing,$nac,$te,$dir,$gra)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "ci_exist";
									}
								}
								
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_empleados(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['ci']) && isset($_POST['con']) && isset($_POST['tip']) && isset($_POST['est'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['empleados']=$this->M_empleado->get_empleado("","",true,"");
					$listado['privilegio']=$privilegio[0];
					$listado['procesos_empleados']=$this->M_proceso_empleado->get_complet("","");
					$listado['productos_empleados']=$this->M_producto_empleado->get_all();
					$listado['tbl']=$_POST['tbl'];
					$listado['cod']=trim($_POST['cod']);
					$listado['nom']=trim($_POST['nom']);
					$listado['ci']=trim($_POST['ci']);
					$listado['con']=trim($_POST['con']);
					$listado['tip']=trim($_POST['tip']);
					$listado['est']=trim($_POST['est']);
					$this->load->view('capital_humano/capital_humano/4-imprimir/print_empleados',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_empleados(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(isset($_GET['file']) && isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['ci']) && isset($_GET['con']) && isset($_GET['tip'])){
				$atrib="";$val="";$estado=1;
				if($_GET['cod']!=""){
					$atrib='e.codigo';$val=$_GET['cod'];
				}else{
					if($_GET['nom']!=""){
						$atrib='nombre_completo';$val=$_GET['nom'];
					}else{
						if($_GET['ci']!=""){
							$atrib='p.ci';$val=$_GET['ci'];
						}else{
							if($_GET['con']!=""){
								$atrib='e.idtc';$val=$_GET['con'];
							}else{
								if($_GET['tip']!=""){
									$atrib='e.tipo';$val=$_GET['tip'];
								}
							}
						}
					}
				}
				if(isset($_GET['est'])){ $estado=$_GET['est']; }
			 	$listado['empleados']=$this->M_empleado->get_empleado($atrib,$val,true,$estado);
				$listado['procesos_empleados']=$this->M_proceso_empleado->get_complet("","");
				$listado['productos_empleados']=$this->M_producto_empleado->get_all();
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('capital_humano/capital_humano/4-imprimir/export_empleados',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	public function img_empleado(){
			if(isset($_POST['e'])){
				$ide=trim($_POST['e']);
				$empleado=$this->M_empleado->get_empleado("e.ide",$ide,false,NULL);
				if(!empty($empleado)){
					$nombre=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno;
					$url=base_url()."libraries/img/";
					$img="sistema/default.jpg";
					$archivos = array();
					if($empleado[0]->fotografia!="" && $empleado[0]->fotografia!=NULL){
						$img="personas/".$empleado[0]->fotografia;
						$archivos[] = array('titulo' => $nombre,'url' =>  $url.$img,'descripcion' => "");
					}else{
						$archivos[] = array('titulo' => $nombre ,'url' =>  $url.$img,"descripcion"=>"");
					}
					$listado['imagenes']=json_decode(json_encode($archivos));
					$this->load->view('estructura/visor',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
	}
   	/*--- Estado ---*/
   	public function cambiar_estado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get($ide);
				if(!empty($empleado)){
					$estado=$empleado[0]->estado;
					if($estado==1){ $estado=0;}else{$estado=1;}
					if($this->M_empleado->modificar_row($ide,'estado',$estado)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End Estado ---*/
   	/*--- Reportes ---*/
   	public function detalle_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$ide=trim($_POST['e']);
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
				$calidad=array();
				$calidad_produccion=0;
				if(!empty($empleado)){
					$productos_empleados=$this->M_producto_empleado->get_all();
					$procesos_empleado=$this->M_proceso_empleado->get_proceso($ide);
					for($i=0; $i<count($procesos_empleado); $i++){ $proceso_empleado=$procesos_empleado[$i];
						$control=$this->lib->search_elemento($productos_empleados,'idpre',$proceso_empleado->idpre);
						if($control!=null){
							$calidad[]=$control->calidad;
						}
					}
					$total_calidad=0;
					for($i=0; $i < count($calidad); $i++){ $total_calidad+=$calidad[$i];}
					if(count($calidad)>0){ $calidad_produccion=number_format(($total_calidad/count($calidad)),0,'.','');}
					$listado['privilegio']=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"planilla");
					$listado['empleado']=$empleado[0];
					$listado['calidad_produccion']=$calidad_produccion;
					$listado['procesos']=$this->M_proceso_empleado->get_proceso($ide);
					$this->load->view("capital_humano/capital_humano/5-reportes/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function produccion_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$ide=$_POST['e'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
				$url=base_url().'libraries/img/';
				$t = array(0 => 'Maestro', 1 => 'Ayudante');
				if(!empty($empleado)){
					$productos_grupos_colores=$this->M_producto->get_colores("","","1");
					$procesos_empleado=$this->M_proceso_empleado->get_proceso($ide);
					$v_productos=[];
					for($i=0; $i < count($procesos_empleado); $i++){
						$productos_empleado=$this->M_producto_empleado->get_row("idpre",$procesos_empleado[$i]->idpre);
						for($j=0; $j < count($productos_empleado); $j++){ 
							$producto=$this->lib->search_elemento($productos_grupos_colores,"idpgrc",$productos_empleado[$j]->idpgrc);
							if($producto!=null){
								$img="sistema/miniatura/default.jpg";
								if($producto->portada!="" && $producto->portada!=null){
									$v=explode("|", $producto->portada);
									if(count($v==2)){
										if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
										if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
									}
								}else{
									if($producto->portada!="" && $producto->portada!=null){
										$v=explode("|", $producto->portada);
										if(count($v==2)){
											if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
											if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
										}
									}
								}
								$codigo=$producto->codigo;
								if($producto->abr_g!="" && $producto->abr_g!=null){ $codigo.="-".$producto->abr_g; }
								if($producto->abr_c!="" && $producto->abr_c!=null){ $codigo.="-".$producto->abr_c; }
								$nombre=$producto->nombre;
								if($producto->abr_g!="" && $producto->abr_g!=null){ $nombre.="-".$producto->nombre_g; }
								if($producto->nombre_c!="" && $producto->nombre_c!=null){ $nombre.="-".$producto->nombre_c; }
								$v_productos[]=array("idp"=>$producto->idp,"idpgr"=>$producto->idpgr,"idpgrc"=>$producto->idpgrc,"codigo"=>$codigo,"nombre"=>$nombre,"img"=>$url.$img,"nombre_proceso"=>$procesos_empleado[$i]->nombre." (".$t[$procesos_empleado[$i]->tipo].")","calidad"=>$productos_empleado[$j]->calidad);
							}
						}
					}
					$listado["productos"]=$this->lib->sort_2d($v_productos,"calidad","desc");
					$listado['empleado']=$empleado[0];
					$this->load->view("capital_humano/capital_humano/5-reportes/produccion",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function config_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$ide=trim($_POST['e']);
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$listado['contrato']=$this->M_tipo_contrato->get_all();
			 		$listado['ciudades']=$this->M_ciudad->get_all();
			 		$listado['procesos_empleado']=$this->M_proceso_empleado->get_proceso($ide);
					$this->load->view("capital_humano/capital_humano/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function config_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['asignados']) && isset($_POST['container'])){
				$asignados=$_POST['asignados'];
				$container=$_POST['container'];
				$t = array(0 => 'Maestro', 1 => 'Ayudante');
				$procesos_asignados = array();
				$procesos=$this->M_proceso->get_all();
				if($asignados!="" && $asignados!="[]"){
					$asignados=json_decode($asignados);
					foreach ($asignados as $key => $asignado) {
						if($asignado->pr.""!="undefined" && $asignado->tipo.""!="undefined" && $asignado->type_save.""!="undefined"){
							$proceso=$this->lib->search_elemento($procesos,"idpr",$asignado->pr);
							if($proceso!=null){
								if($asignado->type_save=="new"){
									$procesos_asignados[] = array('idpr' => $asignado->pr,'nombre' => $proceso->nombre,'tipo' => $asignado->tipo,'type_save' => $asignado->type_save);
								}
								if($asignado->type_save=="update"){
									
									if($asignado->pre!="undefined" && $asignado->json_db!="undefined"){
										$procesos_asignados[] = array('idpr' => $asignado->pr,'nombre' => $proceso->nombre,'tipo' => $asignado->tipo, 'type_save'=>$asignado->type_save, 'idpre'=>$asignado->pre, 'json_db'=>$asignado->json_db);
									}
								}
							}
						}
					}
				}
				$listado['procesos']=json_decode(json_encode($procesos_asignados));
				$listado['container']=$container;
				$this->load->view("capital_humano/capital_humano/6-config/configuracion/config_procesos_empleado",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function append_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr']) && isset($_POST['grado'])){
				$idpr=$_POST['pr'];
				$grado=$_POST['grado'];
				$proceso=$this->M_proceso->get($idpr);
				$t = array(0 => 'Maestro', 1 => 'Ayudante');
				if(!empty($proceso) && ($grado=="0" || $grado=="1")){
					echo " <span class='label label-md label-inverse-primary' style='font-size: .75rem; cursor: default;' data-pr='".$proceso[0]->idpr."' data-type-save='new' id='".rand(5,99999999)."'><label class='text-primary'>".$proceso[0]->nombre." (".$t[$grado*1].") </label> <span class='closed drop_this_elemento' data-padre='label-process' data-tag-padre='span' data-type-padre='tag' data-tipo='".$grado."' onclick='$(this).parent().remove();'>×</span></span>";
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pre'])){
				$idpre=$_POST['pre'];
				$proceso_empleado=$this->M_proceso_empleado->get($idpre);
				if(!empty($proceso_empleado)){
					$proceso=$this->M_proceso->get($proceso_empleado[0]->idpr);
					if(!empty($proceso)){
						$producto_empleado=$this->M_producto_empleado->get_row("idpre",$idpre);
						if(empty($producto_empleado)){
							$listado['open_control']="true";
							$listado['control']="";
							$listado['titulo']="quitar el proceso <strong>".$proceso[0]->nombre."</strong> del empleado";
							$listado['desc']="";
						}else{
							$listado['open_control']="false";
							$listado['control']="";
							$listado['desc']="Imposible eliminar, el proceso <strong>".$proceso[0]->nombre."</strong> esta asignado en ".count($producto_empleado)." producto(s) en el empleado.";
							$listado['btn_ok']="none";
						}
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pre'])){
				$idpre=$_POST['pre'];
				if($this->val->entero($idpre,0,10)){
					$producto_empleado=$this->M_producto_empleado->get_row("idpre",$idpre);
					if(empty($producto_empleado)){
						if($this->M_proceso_empleado->eliminar($idpre)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_empleado(){ // guarda datos del empleado si hacer cambios en el fotografia
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide']) && isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['cod']) && isset($_POST['te']) && isset($_POST['car']) && isset($_POST['tc']) && isset($_POST['gra']) && isset($_POST['sal']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['ing']) && isset($_POST['dir']) && isset($_POST['obs']) && isset($_FILES) && isset($_POST['procesos_eliminados']) && isset($_POST['procesos'])){
				$ide=trim($_POST['ide']);
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$cod=trim($_POST['cod']);
				$te=trim($_POST['te']);
				$car=trim($_POST['car']);
				$tc=trim($_POST['tc']);
				$sal=trim($_POST['sal']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$nac=trim($_POST['nac']);
				$ing=trim($_POST['ing']);
				$gra=trim($_POST['gra']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				$eliminados=$_POST['procesos_eliminados'];
				if($this->val->entero($ide,0,10) && $this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20) && $this->val->entero($te,0,1) && $te>=0 && $te<=1 && $this->val->entero($tc,0,10) && $this->val->decimal($sal,4,1) && $sal>=0 && $sal<=9999.9){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($cod!=""){ if(!$this->val->entero($cod,0,10) || $cod<0 || $cod>9999999999){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
					if($gra!=""){ if(!$this->val->strSpace($gra,0,200)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($ing!=""){ if(!$this->val->fecha($ing)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,800)){ $control=false;}}
					if($control){
						$control=$this->M_tipo_contrato->get($tc);
						if(!empty($control)){
							$control=$this->M_ciudad->get($ciu);
							if(!empty($control)){
								$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
								if(!empty($empleado)){
									$ci_org=$empleado[0]->ci;
									$control=$this->M_persona->get($ci);
									$guardar="ok";
									if(!empty($control)){
										if($ci==$ci_org){
											$guardar="ok";
										}else{
											$guardar="ci_exist";
										}
									}
									if($guardar=="ok"){
										$img=$empleado[0]->fotografia;
										$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ci);//cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id)
										if($img!='error' && $img!="error_type" && $img!="error_size_img"){
											if($this->M_persona->modificar($ci_org,$ci,$ciu,$nom1,$nom2,$pat,$mat,$car,$tel,$ema,$img,$obs)){
												if($this->M_empleado->modificar($empleado[0]->ide,$tc,$ci,$cod,$sal,$ing,$nac,$te,$dir,$gra)){
													if($eliminados!="" && $eliminados!="[]" && $eliminados!="undefined"){
														$eliminados=json_decode($eliminados);
														foreach($eliminados as $key => $proceso_empleado){
															if($proceso_empleado->status=="0"){
																if($this->M_proceso_empleado->eliminar($proceso_empleado->pre)){}
															}
														}
													}
													$procesos=json_decode($_POST['procesos']);
													foreach ($procesos as $key => $proceso) {
														if($proceso->type_save=="new"){
															if($this->M_proceso_empleado->insertar($proceso->pr,$ide,$proceso->tipo)){}
														}
														if($proceso->type_save=="update"){
															if($this->M_proceso_empleado->modificar($proceso->pre,$proceso->tipo)){}
														}
													}
													echo "ok";
												}else{
													$this->M_persona->eliminar($nit);
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo $img;
										}
									}else{
										echo $guardar;
									}
								}else{
									echo "fail";
								}								
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "failll";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$ide=$_POST['e'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$listado['productos_empleado']=$this->M_producto_empleado->get_complet("pre.ide",$ide);
					$listado['productos_grupos_colores']=$this->M_producto->get_colores("","","1");
					//$listado['procesos_empleado']=$this->M_proceso_empleado->get_row();
					$this->load->view("capital_humano/capital_humano/6-config/producto_empleado",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function refresh_producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				$control=false;
					//producto empleado
					if(isset($_POST['p']) && isset($_POST['tbl']) && isset($_POST['e'])){
						$producto=$this->M_producto->get($_POST['p']);
						$categorias=$this->M_producto->get_colores('p.idp',$_POST['p'],"1");
						$productos_empleado=$this->M_producto_empleado->get_complet("pre.ide",$_POST['e']);
						$colores_procesos=[];
						for($i=0;$i < count($categorias);$i++){ $cat=$categorias[$i];
							for($j=0; $j < count($productos_empleado); $j++){ $pe=$productos_empleado[$j];
								if($cat->idpgrc==$pe->idpgrc){
									$colores_procesos[] = array('idpgr'=>$cat->idpgr, 'idgr'=>$cat->idgr, 'nombre_g'=>$cat->nombre_g, 'abr_g'=>$cat->abr_g, 'idpgrc'=>$cat->idpgrc, 'portada'=>$cat->portada, 'costo'=>$cat->costo, 'idco'=>$cat->idco, 'nombre_c'=>$cat->nombre_c, 'codigo_c'=>$cat->codigo_c, 'abr_c'=>$cat->abr_c, 'idproe' => $pe->idproe,'calidad' => $pe->calidad,'idpre' => $pe->idpre,'ide' => $pe->ide,'tipo'=>$pe->tipo,'idpr'=>$pe->idpr,'nombre_proceso'=>$pe->nombre);
								}
							}
						}
						$v_pgc=$this->lib->array_multi_unique($colores_procesos,'idpgrc');
						$grupos=[];
						for($j=0; $j < count($v_pgc); $j++){ $pgc=$v_pgc[$j];
							$procesos=[];
							for($k=0; $k < count($colores_procesos) ; $k++){ $proc=$colores_procesos[$k];
								if($pgc['idpgrc']==$proc['idpgrc']){
									$procesos[]=array('idproe' => $proc['idproe'],'calidad' => $proc['calidad'],'idpre' => $proc['idpre'],'ide' => $proc['ide'],'tipo' => $proc['tipo'],'idpr' => $proc['idpr'],'nombre_proceso' => $proc['nombre_proceso']);
								}
							}
							$grupos[] = array('idpgr'=>$pgc['idpgr'],'idgr'=>$pgc['idgr'],'nombre_g' => $pgc['nombre_g'],'abr_g' => $pgc['abr_g'],'idpgrc' => $pgc['idpgrc'],'costo' => $pgc['costo'],'portada' => $pgc['portada'],'idco' => $pgc['idco'],'nombre_c' => $pgc['nombre_c'],'codigo_c' => $pgc['codigo_c'],'abr_c' => $pgc['abr_c'],'idproe' => $pgc['idproe'], 'procesos' => $procesos);
						}
						$listado['grupos']=json_decode(json_encode($grupos));
						$control=true;
					}
				if($control){
					$listado['producto']=$producto[0];
					$listado['idtb']=$_POST['tbl'];
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/categoria_producto',$listado);
				}else{
					echo "fail";
				}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}




	public function procesos_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc']) && isset($_POST['pres']) && isset($_POST['tbl']) && isset($_POST['type']) && isset($_POST['e'])){
				$idpgrc=trim($_POST['pgc']);
				$ide=trim($_POST['e']);
				$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
				if(!empty($producto_grupo_color)){
					$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
					//$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$producto_grupo[0]->idp,"p.nombre","asc");
					$listado['producto_procesos']=$this->M_producto_proceso->get_row("idp",$producto_grupo[0]->idp);
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['producto_grupo_color']=$producto_grupo_color[0];
					$listado['producto_grupo']=$producto_grupo[0];
					$listado['procesos_empleado']=$this->M_proceso_empleado->get_proceso($ide);
					$listado['procesos_asignados']=trim($_POST['pres']);
					$listado['tbl']=trim($_POST['tbl']);
					$listado['type']=$_POST['type'];
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/procesos/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function btn_add_proceso_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pre']) && isset($_POST['pgc']) && isset($_POST['type'])){
				$idpre=trim($_POST['pre']);
				$idpgrc=trim($_POST['pgc']);
				$type=trim($_POST['type']);
				$proceso_empleado=$this->M_proceso_empleado->get_complet("ep.idpre",$idpre);
				$producto_grupo_color=$this->M_producto_grupo_color->get_row("idpgrc",$idpgrc);
				if(!empty($proceso_empleado) && !empty($producto_grupo_color)){
					$listado['proceso_empleado']=$proceso_empleado[0];
					$listado['producto_grupo_color']=$producto_grupo_color[0];
					$listado['type']=$_POST['type'];
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/procesos/tr_proceso',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto_proceso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e']) && isset($_POST['pgc'])){
				$ide=$_POST['e'];
				$idpgrc=$_POST['pgc'];
				$empleado=$this->M_empleado->get($ide);
				$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
				if(!empty($empleado) && !empty($producto_grupo_color)){
					$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
					$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$producto_grupo[0]->idp,"p.nombre","asc");
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['producto_grupo_color']=$producto_grupo_color[0];
					$listado['producto_grupo']=$producto_grupo[0];
					$listado['empleado_procesos']=$this->M_proceso_empleado->get_row("ide",$ide);
					$listado['empleado']=$empleado[0];
					//$listado['procesos']=$this->M_proceso->get_search("","");
					$this->load->view("capital_humano/capital_humano/6-config/producto_empleado/procesos/view_procesos",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_producto_empleado(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['proe']) && isset($_POST['calidad'])){
					$idproe=trim($_POST['proe']);
					$calidad=trim($_POST['calidad']);
					$producto_empleado=$this->M_producto_empleado->get($idproe);
					if(!empty($producto_empleado)){
						if($this->M_producto_empleado->modificar($idproe,$calidad)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function colores_producto(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['p']) && isset($_POST['pgcs']) && isset($_POST['tbl']) && isset($_POST['e'])){
					$idp=trim($_POST['p']);
					$ide=trim($_POST['e']);
					$producto=$this->M_producto->get($idp);
					$empleado=$this->M_empleado->get($ide);
					if(!empty($producto) && !empty($empleado)){
						$listado['colores']=$this->M_producto->get_colores('p.idp',$idp,"1");
						$listado['colores_asignados']=json_decode(json_encode(trim($_POST['pgcs'])));
						$listado['producto']=$producto[0];
						$listado['tbl']=trim($_POST['tbl']);
						$procesos_empleado=$this->M_proceso_empleado->get_proceso($ide);
						if(!empty($procesos_empleado)){
							$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
							$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
							$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
							//$listado['producto_procesos']=$this->M_producto_proceso->get_row("idp",$idp);
						}
						if(isset($_POST['type'])){ $listado['type']=$_POST['type'];}
						$listado['empleado']=$empleado[0];
						$listado['procesos_empleado']=$procesos_empleado;
						$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/categoria_producto/view_categorias',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
	public function add_producto_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['pgc']) && isset($_POST['tbl']) && isset($_POST['type']) && isset($_POST['e'])){
				$idp=trim($_POST['p']);
				$idpgrc=trim($_POST['pgc']);
				$tbl=trim($_POST['tbl']);
				$type=trim($_POST['type']);
				$ide=trim($_POST['e']);
				$color=$this->M_producto->get_colores("pgc.idpgrc",$idpgrc,"1");
				$producto=$this->M_producto->get($idp);
				$empleado=$this->M_empleado->get($ide);
				if(!empty($color) && !empty($producto) && !empty($empleado)){
					$listado['producto_procesos']=$this->M_producto_proceso->get_row("idp",$idp);
					//$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['color']=$color[0];
					$listado['producto']=$producto[0];
					$listado['idtb']=trim($_POST['tbl']);
					$listado['type']=$_POST['type'];
					$listado['procesos_empleado']=$this->M_proceso_empleado->get_proceso($ide);
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/categoria_producto/tr_categoria',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_producto_colores_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p'])&& isset($_POST['e']) && isset($_POST['eliminados']) && isset($_POST['procesos'])){
				$idp=trim($_POST['p']);
				$ide=trim($_POST['e']);
				$eliminados=trim($_POST['eliminados']);
				$procesos=trim($_POST['procesos']);
				$producto=$this->M_producto->get($idp);
				$empleado=$this->M_empleado->get($ide);
				if(!empty($producto) && !empty($empleado) && $procesos!="[]" && $procesos!=""){
					$eliminados=json_decode($eliminados);
					//var_dump($procesos);
					$procesos=json_decode($procesos);

					foreach($eliminados as $key => $eliminado){
						if($eliminado->status=="0"){
							if($this->M_producto_empleado->eliminar($eliminado->idproe)){ }
						}
					}
					foreach($procesos as $key => $proceso){
						if($proceso->type=="update"){
							if($this->M_producto_empleado->modificar($proceso->proe,$proceso->calidad)){ }
						}
						if($proceso->type=="new"){
							$proceso_empleado=$this->M_proceso_empleado->get($proceso->pre);
							if(!empty($proceso_empleado)){
								if($proceso_empleado->idpr==$proceso->idpr){
									if($this->M_producto_empleado->insertar($proceso->pgc,$proceso->pre,$proceso->calidad,$proceso_empleado[0]->tipo)){}
								}
							}
						}
					}
					echo "ok";
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function search_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/productos/search');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pa']) && isset($_POST['e'])){
				$atrib="";$val="";
				if(isset($_POST['nom'])){
					if($_POST['nom']!=""){ $atrib='nombre';$val=trim($_POST['nom']); }
				}
					$listado['procesos_empleado']=$this->M_proceso_empleado->get_row('ide',$_POST['e']);
					$listado['productos']=$this->M_producto->get_search($atrib,$val,"1");
					$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("","","p.nombre","asc");
					$listado['colores']=$this->M_color->get_all();
					$listado['grupos_colores']=$this->M_producto_grupo->get_grupo_colores("","");
					$listado['pa']=$_POST['pa'];
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/productos/view',$listado);
				
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p']) && isset($_POST['e'])){
				$ide=trim($_POST['e']);
				$idp=trim($_POST['p']);
				$empleado=$this->M_empleado->get($ide);
				$producto=$this->M_producto->get($idp);
				if(!empty($empleado) && !empty($producto)){
					$listado['procesos_empleado']=$this->M_proceso_empleado->get_proceso($ide);
					$listado['producto_procesos']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['producto']=$producto[0];
					$listado['colores']=$this->M_color->get_all();
					$listado['grupos']=$this->M_producto_grupo->get_grupo_colores("pg.idp",$idp); 
					$this->load->view('capital_humano/capital_humano/6-config/producto_empleado/productos/row_producto',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_productos_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['productos']) && isset($_POST['eliminados']) && isset($_POST['e'])){
				if($_POST['eliminados']!="[]" && $_POST['eliminados']!=""){
					$eliminados=json_decode($_POST['eliminados']);
					foreach ($eliminados as $key => $eliminado) {
						if($eliminado->status=="0" || $eliminado->status==0){
							if($this->M_producto_empleado->eliminar($eliminado->idproe)){}
						}
					}
				}
				if($_POST['productos']!="[]" && $_POST['productos']!=""){
					$productos=json_decode($_POST['productos']);
					foreach ($productos as $key => $producto) {
						if($producto->type=="update"){
							if($this->M_producto_empleado->modificar($producto->proe,$producto->calidad)){}
						}
						if($producto->type=="new"){
							$proc_emp=$this->M_proceso_empleado->get($producto->pre);
							if(!empty($proc_emp)){
								if($this->M_producto_empleado->insertar($producto->pgc,$producto->pre,$producto->calidad,$proc_emp[0]->tipo)){}
							}
						}
					}
					
				}
				echo "ok";
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}	
	}






















		




	
	/*
	public function search_producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view("capital_humano/capital_humano/6-config/producto_empleado/productos/search");
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_producto_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$atrib="";$val="";
				if(isset($_POST['nom'])){
					if($_POST['nom']!=""){ $atrib='nombre';$val=trim($_POST['nom']); }
				}
				$listado['productos']=$this->M_producto->get_search($atrib,$val);
				$listado['colores']=$this->M_color->get_all();
				$listado['type']=$_POST['type'];


				$ide=$_POST['e'];
				$empleado=$this->M_empleado->get($ide);
				if(isset($empleado)){
					$col="";$val="";
					if(isset($_POST['nom'])){ $col="nombre";$val=$_POST['nom'];}
					$listado['procesos']=$this->M_proceso->get_search($col,$val);
					$this->load->view("capital_humano/capital_humano/6-config/producto_empleado/productos/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}*/
	/*
	public function config_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e'])){
				$ide=$_POST['e'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide,false,NULL);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$listado['procesos_empleado']=$this->M_proceso_empleado->get_proceso($ide);
					$this->load->view("capital_humano/capital_humano/6-config/configuracion",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}*/
	/*public function search_proceso_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['type-save']) && isset($_POST['pgc'])){
				$listado['type_save']=$_POST['type-save'];
				$listado['pgc']=$_POST['pgc'];
				$this->load->view("capital_humano/capital_humano/6-config/configuracion/proceso/search_proceso",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_proceso_producto(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e']) && isset($_POST['pgc'])){
				$ide=$_POST['e'];
				$idpgrc=$_POST['pgc'];
				$empleado=$this->M_empleado->get($ide);
				$producto_grupo_color=$this->M_producto_grupo_color->get($idpgrc);
				if(isset($empleado) && !empty($producto_grupo_color)){
					$col="";$val="";
					if(isset($_POST['nom'])){ $col="nombre";$val=$_POST['nom'];}
					$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$listado['producto_grupo_proceso']=$this->M_producto_grupo_proceso->get_all();
					$listado['producto_grupo_color_proceso']=$this->M_producto_grupo_color_proceso->get_all();
					$listado['procesos']=$this->M_proceso->get_search($col,$val);
					$listado['producto_proceso']=$this->M_producto_proceso->producto_proceso("pp.idp",$idp,"p.nombre","asc");
					$this->load->view("capital_humano/capital_humano/6-config/configuracion/proceso/view_proceso",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}*/
/*
	public function confirmar_proceso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr'])){
				$idpr=trim($_POST['pr']);
				$proceso=$this->M_proceso->get($idpr);
				if(!empty($proceso)){
					$control=$this->M_proceso_empleado->get_row("idpr",$idpr);
					$control2=$this->M_producto_proceso->get_row("idpr",$idpr);
					if(empty($control) && empty($control2)){
						$listado['open_control']="true";
						$listado['control']="";
						$listado['titulo']="eliminar el proceso <strong>".$proceso[0]->nombre."</strong> del sistema";
						$listado['desc']="Se eliminara definitivamente el proceso del empleado.";
					}else{
						$listado['open_control']="false";
						$listado['control']="";
						$listado['btn_ok']="none";
						$desc="Imposible eliminar el proceso <strong>".$proceso[0]->nombre."</strong>";
						$aux="";
						if(!empty($control)){
							$aux=", se encuentra asignado en ".count($control)." empleado";
							if(count($control)>1){ $aux.="s";}
						}
						if(!empty($control2)){
							if($aux!=""){ 
								$aux.="y en";
							}else{
								$aux=", se encuentra asignado en";
							}
							$aux.=" ".count($control2)." producto";
							if(count($control2)>0){ $aux.="s";}
						}
						$listado['desc']=$desc.$aux;
					}
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_proceso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pr'])){
				$idpr=$_POST['pr'];
				$proceso=$this->M_proceso->get($idpr);
				if(!empty($proceso)){
					$control=$this->M_proceso_empleado->get_row("idpr",$idpr);
					$control2=$this->M_producto_proceso->get_row("idpr",$idpr);
					if(empty($control) && empty($control2)){
						if($this->M_proceso->eliminar($idpr)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "faillll";
					}
				}else{
					echo "failll";
				}
			}else{
				echo "faill";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}*/
	public function add_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['e']) && isset($_POST['pr']) && isset($_POST['car'])){
				$ide=$_POST['e'];
				$idpr=$_POST['pr'];
				$car=$_POST['car']."";
				$empleado=$this->M_empleado->get($ide);
				$proceso=$this->M_proceso->get($idpr);
				if(!empty($empleado) && !empty($proceso) && $this->val->entero($car,0,10) && ($car=="0" || $car=="1")){
					if($this->M_proceso_empleado->insertar($idpr,$ide,$car)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function update_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pre']) && isset($_POST['cat'])){
				$idpre=$_POST['pre'];
				$cat=$_POST['cat']."";
				if($this->val->entero($idpre,0,10) && $this->val->entero($cat,0,10) && ($cat=="0" || $cat=="1")){
					if($this->M_proceso_empleado->modificar($idpre,$cat)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*public function confirmar_proceso_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pre'])){
				$idpre=$_POST['pre'];
				$procesos_empleado=$this->M_proceso_empleado->get($idpre);
				if($procesos_empleado){
					$producto_empleado=$this->M_producto_empleado->get_row("idpre",$idpre);
					if(empty($producto_empleado)){
						$listado['open_control']="true";
						$listado['control']="";
						$listado['titulo']="eliminar el proceso del empleado";
						$listado['desc']="Se eliminara definitivamente el proceso del empleado.";
					}else{
						$listado['open_control']="false";
						$listado['control']="";
						$listado['desc']="Imposible eliminar, el proceso esta asignado en ".count($producto_empleado)." producto(s) en el empleado.";
						$listado['btn_ok']="none";
					}
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}*/

/*--- End configuracion ---*/
/*--- Eliminar ---*/
   	public function confirmar_empleado(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['e'])){
				$ide=trim($_POST['e']);
				$url='./libraries/img/';
				$empleado=$this->M_empleado->get_empleado("e.ide",$ide,false,NULL);
				if(!empty($empleado)){
					$listado['titulo']="eliminar a <b>".$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno." ".$empleado[0]->materno."</b>";
					$listado['desc']="Se eliminara definitivamente el empleado";
					$img='sistema/miniatura/default.jpg';
					if($empleado[0]->fotografia!=NULL && $empleado[0]->fotografia!=""){ $img="personas/miniatura/".$empleado[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function drop_empleado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$id=trim($_POST['e']);
			$u=$_POST['u'];
			$p=$_POST['p'];
			if(strtolower($u)==strtolower($this->session->userdata("login"))){
				$usuario=$this->M_usuario->validate($u,$p);
				if(!empty($usuario)){
					if($id!=""){
						$empleado=$this->M_empleado->get_empleado("e.ide",$id,false,NULL);
						if(!empty($empleado)){
							//verificando si existe en usuario
							$usuario=$this->M_usuario->get_row('ci',$empleado[0]->ci);
							$directivo=$this->M_directivo->get_row('ci',$empleado[0]->ci);
							if(empty($usuario) && empty($directivo)){
								if($this->lib->eliminar_imagen($empleado[0]->fotografia,'./libraries/img/personas/')){
									if($this->M_persona->eliminar($empleado[0]->ci)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								if($this->M_empleado->eliminar($id)){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}			
					}else{
						echo "fail";
					}
				}else{
					echo "validate";
				}
			}else{
				echo "validate";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CAPITAL HUMANO -------*/
/*------- MANEJO DE PLANILLA DE SUELDOS -------*/
	public function search_planilla(){//vista de buscador de salario
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		 	$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("capital_humano/planilla/search",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_planilla(){//vista de los salarios de todos los empleados
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				$vf=explode("-", $_POST['f1']);
				$f1=$vf[0]."-".$vf[1]."-01";
				$vf=explode("-", $_POST['f2']);
				$f2=$vf[0]."-".$vf[1]."-".$this->lib->dosDigitos($this->lib->ultimo_dia($vf[1],$vf[0]));
			}else{
				$aux=strtotime('-1 month' ,strtotime(date('Y-m').'-01'));
				$f1=date('Y-m-d',$aux);
				$aux=strtotime('-1 month' ,strtotime(date('Y-m').'-'.$this->lib->dosDigitos($this->lib->ultimo_dia(date('m')-1,date('Y')))));
				$f2=date('Y-m-d',$aux);;
			}
			$atrib="";$val="";
			if(isset($_POST['nom']) && isset($_POST['ci'])){
				if($_POST['nom']!=""){
					$atrib='nombre';$val=$_POST['nom'];
				}else{
					if($_POST['ci']!=""){
						$atrib='p.ci';$val=$_POST['ci'];
					}
				}	
			}
			$listado['f1']=$f1;
			$listado['f2']=$f2;
		 	$listado['empleados']=$this->M_empleado->get_empleado_activo($atrib,$val);
			$listado['horas']=$this->M_hora_biometrico->exist_fecha("","",$f1,$f2);
			$listado['feriados']=$this->M_feriado->get_fechas($f1,$f2);
		 	$listado['privilegio']=$this->M_privilegio->get_row("ide",$this->session->userdata("id"));
			$this->load->view("capital_humano/planilla/view",$listado);
			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function exportar_excel(){ //vista de los salarios de todos los empleados
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['f1']) && isset($_POST['f2'])){
				$f1=$_POST['f1'];
				$f2=$_POST['f2'];
			}else{
				$f1=date('Y-m').'-01';
				$f2=date('Y-m').'-'.$this->validaciones->convierteMes($this->validaciones->ultimo_dia(date('m'),date('Y')));
			}
			$listado['f1']=$f1;
			$listado['f2']=$f2;
			$listado['empleado']=$this->M_empleado->get_all();
			$this->load->view("capital_humano/exportar",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	public function new_biometrico(){//vista de buscador de salario
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view("capital_humano/planilla/3-nuevo/view");
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_biometrico(){//vista de buscador de salario
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_FILES['archivo']) && isset($_POST['tipo'])){
				$tipo=$_POST['tipo'];//true or false
				$archivo=$_FILES['archivo'];
				/*Controlando si es una archivo válido*/
				$datos=$this->excel->read_file($archivo['tmp_name']);
				/*End Control*/
				if($this->lib->biometrico_valido($datos)){
					$nom_arch=$this->lib->subir_excel($archivo,'./libraries/files/biometrico/',rand(0,99999999).'-'.$this->session->userdata('id'),'xls');
					if($nom_arch!="error" && $nom_arch!="error_type"){
						if($this->M_archivo_biometrico->insertar($this->session->userdata('id'),$nom_arch,date('Y-m-d H:m:s'))){
							$datos=$this->excel->read_file('./libraries/files/biometrico/'.$nom_arch);//abrimos el archivo
							$fechas=json_decode($this->lib->min_max_fecha_biometrico($datos));
							if($this->M_hora_biometrico->eliminar_rango($fechas->fecha_min,$fechas->fecha_max)){
								$empleados=$this->M_empleado->get_all();
								//$hora_biometrico=$this->M_hora_biometrico->get_all();
								for($i=0; $i<count($empleados); $i++){ $empleado=$empleados[$i];
									$hras_empleado=json_decode($this->lib->para_insertar_biometrico($empleado,$tipo,$datos));
									foreach($hras_empleado as $row) :
										if($this->M_hora_biometrico->insertar($empleado->ide,$row->fecha,$row->hora)){ }
									endforeach;
								}
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						echo $nom_arch;
					}
				}else{
					echo "file_content_error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function imprimir_planilla(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['json'])){
				$listado['empleados']=$_POST['json'];
				$listado['f1']=$_POST['f1'];
				$listado['f2']=$_POST['f2'];
				$this->load->view('capital_humano/planilla/4-imprimir/config',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function arma_planilla(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['json']) && isset($_POST['f1']) && isset($_POST['f2'])){
				if(isset($_POST['v1'])){ if($_POST['v1']!="ok"){ $listado['v1']="ok";} }
				if(isset($_POST['v2'])){ if($_POST['v2']!="ok"){ $listado['v2']="ok";} }
				if(isset($_POST['v3'])){ if($_POST['v3']!="ok"){ $listado['v3']="ok";} }
				if(isset($_POST['v4'])){ if($_POST['v4']!="ok"){ $listado['v4']="ok";} }
				if(isset($_POST['v5'])){ if($_POST['v5']!="ok"){ $listado['v5']="ok";} }
				if(isset($_POST['v6'])){ if($_POST['v6']!="ok"){ $listado['v6']="ok";} }
				if(isset($_POST['v7'])){ if($_POST['v7']!="ok"){ $listado['v7']="ok";} }
				if(isset($_POST['v8'])){ if($_POST['v8']!="ok"){ $listado['v8']="ok";} }
				if(isset($_POST['v9'])){ if($_POST['v9']!="ok"){ $listado['v9']="ok";} }
				if(isset($_POST['v10'])){ if($_POST['v10']!="ok"){ $listado['v10']="ok"; } }
				if(isset($_POST['v11'])){ if($_POST['v11']!="ok"){ $listado['v11']="ok"; } }
				if(isset($_POST['nro'])){ $listado['nro']=$_POST['nro'];}else{ $listado['nro']=34;}
				$listado['f1']=$_POST['f1'];
				$listado['f2']=$_POST['f2'];
				$listado['empleados']=$_POST['json'];
				$listado['horas']=$this->M_hora_biometrico->exist_fecha("","",$_POST['f1'],$_POST['f2']);
				$listado['feriados']=$this->M_feriado->get_fechas($_POST['f1'],$_POST['f2']);
				$this->load->view('capital_humano/planilla/4-imprimir/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/

   	public function pae(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$empleado=$this->M_empleado->get_empleado('e.ide',$_POST['ide']);
				if(!empty($empleado)){
					$listado['ide']=$_POST['ide'];
					$listado['fe1']=$_POST['f1'];
					$listado['fe2']=$_POST['f2'];
					$listado['empleado']=$empleado[0];
					$listado['feriados']=$this->M_feriado->get_fechas($_POST['f1'],$_POST['f2']);
					$this->load->view('capital_humano/planilla/5-reportes/pae',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
	public function reporte_planilla(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(!empty($empleado)){
					if(!isset($_POST['nro'])){ $listado['nro']=63;
					}else{ $listado['nro']=$_POST['nro']; }
					$listado['ide']=$_POST['ide'];
					$listado['f1']=$_POST['f1'];
					$listado['f2']=$_POST['f2'];
					$listado['horas']=$this->M_hora_biometrico->exist_fecha('ide',$_POST['ide'],$_POST['f1'],$_POST['f2']);
					$vf=explode("-", $_POST['f1']);
					$fini=$vf[0]."-".$vf[1]."-01";
					$vf=explode("-", $_POST['f2']);
					$ffin=$vf[0]."-".$vf[1]."-31";
					$listado['feriados']=$this->M_feriado->get_fechas($fini,$ffin);
					$listado['empleado']=$empleado[0];
					$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$_POST['f1'],$_POST['f2']);
					$this->load->view("capital_humano/planilla/5-reportes/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}   	
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function detalle_anticipos(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$f1=$_POST['f1'];
				$f2=$_POST['f2'];
				if($this->val->fecha($f1) && $this->val->fecha($f2) && $this->val->entero($ide,0,10)){
					$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
					if(!empty($empleado)){
						$listado['empleado']=$empleado[0];
						$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$f1,$f2);
						$listado['f1']=$f1;
						$listado['f2']=$f2;
						$this->load->view('capital_humano/planilla/6-config/anticipos/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
   	public function config_planilla(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ide']) && isset($_POST['f1']) && isset($_POST['f2'])){
				$ide=$_POST['ide'];
				$empleado=$this->M_empleado->get_empleado('e.ide',$ide);
				if(count($empleado)>0){
					$listado['ide']=$_POST['ide'];
					$listado['f1']=$_POST['f1'];
					$listado['f2']=$_POST['f2'];
					$listado['horas']=$this->M_hora_biometrico->exist_fecha('ide',$_POST['ide'],$_POST['f1'],$_POST['f2']);
					$vf=explode("-", $_POST['f1']);
					$fini=$vf[0]."-".$vf[1]."-01";
					$vf=explode("-", $_POST['f2']);
					$ffin=$vf[0]."-".$vf[1]."-31";
					$listado['feriados']=$this->M_feriado->get_fechas($fini,$ffin);
					$listado['empleado']=$empleado[0];
					$listado['anticipos']=$this->M_anticipo->get_empleado_fecha($ide,$_POST['f1'],$_POST['f2']);
					$this->load->view("capital_humano/planilla/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
   	public function add_hora(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['fecha'])){
				$fecha=$_POST['fecha'];
				$listado['fecha']=$fecha;
				$this->load->view("capital_humano/planilla/6-config/add_hora",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_hora(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['fecha']) && isset($_POST['hora']) && isset($_POST['ide'])){
				$fecha=$_POST['fecha'];
				$hora=$_POST['hora'];
				$ide=$_POST['ide'];
				if($this->val->fecha($fecha) && $this->val->entero($ide,0,10)){
					if($this->M_hora_biometrico->insertar($ide,$fecha,$hora)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "error";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_hora(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idhb'])){
				$idhb=$_POST['idhb'];
				if($this->M_hora_biometrico->eliminar($idhb)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "error";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLANILLA DE SUELDOS -------*/
/*------- MANEJO DE PLANILLA DE SUELDOS -------*/
	public function search_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		 	$privilegio=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"directivo");
		 	if(!empty($privilegio)){
		 		$listado['privilegio']=$privilegio[0];
		 		$this->load->view("capital_humano/directivo/search",$listado);
		 	}else{
		 		echo "fail";
		 	}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"directivo");
		 	if(!empty($privilegio)){
		 		$atrib="";$val="";	
				if(isset($_POST['ci']) && isset($_POST['nom'])){
					if($_POST['ci']!=""){
						$atrib='p.ci';$val=$_POST['ci'];
					}else{
						if($_POST['nom']!=""){
							$atrib='nombre_completo';$val=$_POST['nom'];
						}
					}
				}
		 		$listado['privilegio']=$privilegio[0];
		 		$listado['directivos']=$this->M_directivo->get_search($atrib,$val,"nombre_completo","asc");
		 		$listado['cuentas']=$this->M_banco_persona->get_search_2n(NULL,NULL,NULL,NULL);
		 		$this->load->view("capital_humano/directivo/view",$listado);
		 	}else{
		 		echo "fail";
		 	}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
	public function detalle_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['di'])){
				$iddi=trim($_POST['di']);
				$directivo=$this->M_directivo->get_search("d.iddi",$iddi,"nombre_completo","asc");
				if(!empty($directivo)){
					$listado['directivo']=$directivo[0];
					$listado['cuentas']=$this->M_banco_persona->get_search_2n(NULL,NULL,NULL,NULL);
					$listado['ciudades']=$this->M_ciudad->get_search("","");
					$this->load->view("capital_humano/directivo/5-reporte/detalle",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- Configuracion ---*/
	public function config_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['di'])){
				$iddi=trim($_POST['di']);
				$directivo=$this->M_directivo->get_search("d.iddi",$iddi,"nombre_completo","asc");
				if(!empty($directivo)){
					$listado['directivo']=$directivo[0];
					$listado['cuentas']=$this->M_banco_persona->get_search(NULL,NULL);
					$listado['ciudades']=$this->M_ciudad->get_search("","");
					$listado['pagos']=$this->M_pago->get_all();
					$this->load->view("capital_humano/directivo/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['di']) && isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['car']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['obs']) && isset($_FILES) && isset($_POST['cuentas']) && isset($_POST['eliminados'])){
				$iddi=trim($_POST['di']);
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$car=trim($_POST['car']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$nac=trim($_POST['nac']);
				$obs=trim($_POST['obs']);
				$cuentas=$_POST['cuentas'];
				$eliminados=$_POST['eliminados'];
				$ciudad=$this->M_ciudad->get($ciu);
				$directivo=$this->M_directivo->get_search("d.iddi",$iddi,"nombre_completo","asc");
				if(!empty($ciudad) && !empty($directivo) && $this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20)){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,200)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,800)){ $control=false;}}
					if($control){
						$control=true;
						if($directivo[0]->ci!=$ci){
							$persona=$this->M_persona->get($ci);
							if(!empty($persona)){
								$control=false;
							}
						}
						if($control){
							$img=$directivo[0]->fotografia;
							$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ci);
							if($img!='error' && $img!="error_type" && $img!="error_size_img"){
								$ci_org=$directivo[0]->ci;
								$pagos=$this->M_pago->get_all();//para comparar si una cuenta esta asignada a un pago en el pedido
								if($this->M_persona->modificar($ci_org,$ci,$ciu,$nom1,$nom2,$pat,$mat,$directivo[0]->cargo,$tel,$ema,$img,$obs)){
									if($this->M_directivo->modificar($iddi,$ci,$nac,$car)){
										if($eliminados!="" && $eliminados!="[]" && $eliminados!="undefined"){
											$eliminados=json_decode($eliminados);
											foreach($eliminados as $key => $cuenta_empleado){
												if($cuenta_empleado->status=="0"){
													$control=$this->lib->search_elemento($pagos,"idbp",$cuenta_empleado->bp);
													if($control==null){
														if($this->M_banco_persona->eliminar($cuenta_empleado->bp)){}
													}
												}
											}
										}
										if($cuentas!="" && $cuentas!="[]" && $cuentas!="undefined"){
											$cuentas=json_decode($cuentas);
											foreach($cuentas as $key => $cuenta_empleado){
												if($cuenta_empleado->type_save=="new"){
													if($this->M_banco_persona->insertar($cuenta_empleado->ba,$ci,$cuenta_empleado->cuenta,$cuenta_empleado->tipo,"1")){}
												}
												if($cuenta_empleado->type_save=="update"){
													if($this->M_banco_persona->modificar($cuenta_empleado->bp,$cuenta_empleado->ba,$ci,$cuenta_empleado->cuenta,$cuenta_empleado->tipo)){}
												}
											}
										}
										echo "ok";
									}else{
										$this->M_persona->eliminar($nit);
										echo "error";
									}
								}else{
									echo "error";
								}
							}else{
								echo $img;
							}
						}else{
							echo "ci_exist";
						}						
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Configuracion ---*/
   	/*--- Eliminar ---*/
	public function confirm_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"directivo");
			if(!empty($privilegio)){
				if($privilegio[0]->ca3r=="1" && $privilegio[0]->ca3d=="1"){
					if(isset($_POST['di'])){
						$iddi=trim($_POST['di']);
						$directivo=$this->M_directivo->get_search("d.iddi",$iddi,NULL,NULL);
						if(!empty($directivo)){
							$control=true;
							$bancos_persona=$this->M_banco_persona->get_row("ci",$directivo[0]->ci);
							if(!empty($bancos_persona)){
								$pagos=$this->M_pago->get_all();
								for($i=0; $i < count($bancos_persona); $i++){
									$ele=$this->lib->search_elemento($pagos,"idbp",$bancos_persona[$i]->idbp);
									if($ele!=null){
										$control=false;
										break;
									}
								}
							}
								$url='libraries/img/';
								$listado['open_control']="false";
								$listado['control']="false";
								if($control){
									$listado['titulo']="se eliminara a <strong>".$directivo[0]->nombre_completo."</strong> como directivo.";
									$listado['desc']="Se eliminara definitivamente al directivo";
								}else{
									$listado['desc']="Imposible eliminar el directivo, algunas cuentas bancarias se encuentran asignadas en depositos de pedidos.";
									$listado['btn_ok']="none";
								}
								$img="sistema/miniatura/default.jpg";
						    	if($directivo[0]->fotografia!="" && $directivo[0]->fotografia!=NULL){ $img="personas/miniatura/".$directivo[0]->fotografia; }
								$listado['img']=$url.$img;
								$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_capital_humano($this->session->userdata("id"),"directivo");
			if(!empty($privilegio)){
				if($privilegio[0]->ca3r=="1" && $privilegio[0]->ca3d=="1"){
					if(isset($_POST['di'])){
						$iddi=trim($_POST['di']);
						$directivo=$this->M_directivo->get_search("d.iddi",$iddi,NULL,NULL);
						if(!empty($directivo)){
							$control=true;
							$bancos_persona=$this->M_banco_persona->get_row("ci",$directivo[0]->ci);
							if(!empty($bancos_persona)){
								$pagos=$this->M_pago->get_all();
								for($i=0; $i < count($bancos_persona); $i++){
									$ele=$this->lib->search_elemento($pagos,"idbp",$bancos_persona[$i]->idbp);
									if($ele!=null){
										$control=false;
										break;
									}
								}
							}
							if($control){
								//verificando si existe en las tablas usuario o empleado
								$empleado=$this->M_empleado->get_row("ci",$directivo[0]->ci);
								$usuario=$this->M_usuario->get_row("ci",$directivo[0]->ci);
								if(!empty($empleado) || !empty($usuario)){
									if($this->M_directivo->eliminar($iddi)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									if($this->lib->eliminar_imagen($directivo[0]->fotografia,'./libraries/img/personas/')){
										if($this->M_persona->eliminar($directivo[0]->ci)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
   	/*--- Imprimir ---*/
   	public function print_directivos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['nom']) && isset($_POST['ci'])){
		 			$atrib="";$val="";
			 		$listado['privilegio']=$privilegio[0];
			 		$listado['visibles']=json_decode($_POST['visibles']);
			 		$listado['directivos']=$this->M_directivo->get_search("","","nombre_completo","asc");
			 		$listado['cuentas']=$this->M_banco_persona->get_search_2n(NULL,NULL,NULL,NULL);
			 		$listado['ciudades']=$this->M_ciudad->get_search("","");
					$listado['tbl']=$_POST['tbl'];
					$listado['nom']=trim($_POST['nom']);
					$listado['ci']=trim($_POST['ci']);
			 		$this->load->view("capital_humano/directivo/4-imprimir/print_directivos",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_directivos(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_GET['file']) && isset($_GET['nom']) && isset($_GET['ci'])){
		 			$atrib="";$val="";
			 		if(isset($_GET['ci']) && isset($_GET['nom'])){
						if($_GET['ci']!=""){
							$atrib='p.ci';$val=$_GET['ci'];
						}else{
							if($_GET['nom']!=""){
								$atrib='nombre_completo';$val=$_GET['nom'];
							}
						}
					}
			 		$listado['privilegio']=$privilegio[0];
			 		$listado['file']=trim($_GET['file']);
			 		$listado['directivos']=$this->M_directivo->get_search($atrib,$val,"nombre_completo","asc");
			 		$listado['cuentas']=$this->M_banco_persona->get_search_2n(NULL,NULL,NULL,NULL);
			 		$listado['ciudades']=$this->M_ciudad->get_search("","");
			 		$this->load->view("capital_humano/directivo/4-imprimir/export_directivos",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Nuevo ---*/
	public function new_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
		 	$listado['ciudades']=$this->M_ciudad->get_all();
			$this->load->view("capital_humano/directivo/3-nuevo/view",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function new_cuenta_banco(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['container'])){
			 	$listado['bancos']=$this->M_banco->get_all();
			 	$listado['container']=trim($_POST['container']);
				$this->load->view("capital_humano/directivo/cuenta_bancaria/new",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
		/*--Banco--*/
		public function view_bancos(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['container'])){
					$listado['bancos']=$this->M_banco->get_all();
					$listado['container']=trim($_POST['container']);
					$this->load->view("capital_humano/directivo/cuenta_bancaria/banco/view",$listado);
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function new_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['container']) && isset($_POST['type'])){
					$listado['container']=trim($_POST['container']);
					$listado['type']=trim($_POST['type']);
					$this->load->view("capital_humano/directivo/cuenta_bancaria/banco/new",$listado);
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function save_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['url'])){
					$nom=trim($_POST['nom']);
					$url=trim($_POST['url']);
					if($this->val->strSpace($nom,4,200)){
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/bancos/','',$this->resize,"");
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_banco->insertar($img,$nom,$url)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function options_bancos(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['selected'])){
					$seleccionado=trim($_POST['selected']);
					$bancos=$this->M_banco->get_all();
					$options="<option value=''>Seleccionar...</option>";
					for($i=0; $i < count($bancos); $i++){ $banco=$bancos[$i];
						$selected="";
						if($seleccionado==$banco->idba){
							$selected="SELECTED='SELECTED'";
						}
						$options.="<option value='".$banco->idba."' ".$selected.">".$banco->razon."</option>";
					}
					echo $options;
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function config_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['ba']) && isset($_POST['container'])){
					$banco=$this->M_banco->get(trim($_POST['ba']));
					if(!empty($banco)){
						$listado['banco']=$banco[0];
						$listado['container']=trim($_POST['container']);
						$this->load->view("capital_humano/directivo/cuenta_bancaria/banco/modificar",$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function update_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_FILES) && isset($_POST['ba']) && isset($_POST['nom']) && isset($_POST['url'])){
					$idba=trim($_POST['ba']);
					$nom=trim($_POST['nom']);
					$url=trim($_POST['url']);
					$banco=$this->M_banco->get($idba);
					if(!empty($banco) && $this->val->strSpace($nom,4,200)){
						$img=$banco[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/bancos/','',$this->resize,$img,$banco[0]->idba);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_banco->modificar($idba,$img,$nom,$url)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
	   	public function confirm_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['ba'])){
					$idba=trim($_POST['ba']);
					$banco=$this->M_banco->get($idba);
					if(!empty($banco)){
						$banco_persona=$this->M_banco_persona->get_row("idba",$idba);
						$url='libraries/img/';
						$listado['open_control']="false";
						$listado['control']="false";
						if(empty($banco_persona)){
							$listado['titulo']="la entidad financiera ".$banco[0]->razon;
							$listado['desc']="Se eliminara definitivamente la entidad financiera";
						}else{
							$listado['desc']="Imposible eliminar la entidad financiera esta en uso en ".count($banco_persona)." empleado(s)";
							$listado['btn_ok']="none";
						}
		    			$img="sistema/miniatura/default.jpg";
				    	if($banco[0]->fotografia!="" && $banco[0]->fotografia!=NULL){ $img="bancos/miniatura/".$banco[0]->fotografia; }
						$listado['img']=$url.$img;
						$this->load->view('estructura/form_eliminar',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		public function drop_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['ba'])){
					$banco=$this->M_banco->get(trim($_POST['ba']));
					if(!empty($banco)){
						$banco_persona=$this->M_banco_persona->get_row("idba",trim($_POST['ba']));
						if(empty($banco_persona)){
							if($this->lib->eliminar_imagen($banco[0]->fotografia,'./libraries/img/bancos/')){
								if($this->M_banco->eliminar(trim($_POST['ba']))){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
		/*--End Banco--*/
		public function append_cuenta_banco(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['ba']) && isset($_POST['cta']) && isset($_POST['tip'])){
					$idba=trim($_POST['ba']);
					$cta=trim($_POST['cta']);
					$tip=trim($_POST['tip']);
					$banco=$this->M_banco->get($idba);
					if(!empty($banco) && $this->val->entero($cta,4,100) && ($tip=="1" || $tip=="2" || $tip=="3")){
						echo " <span class='label label-md label-inverse-primary' style='font-size: 1rem !important; cursor: default;' data-ba='".$banco[0]->idba."' data-cuenta='".$cta."'  data-tipo='".$tip."' data-type-save='new' id='".rand(5,99999999)."'><label class='text-primary'>".$banco[0]->razon." - ".$cta." </label> <span class='closed' data-padre='label-process' data-tag-padre='span' data-type-padre='tag' onclick='$(this).parent().remove();'>×</span></span>";
					}else{
						echo "fail $cta $tip ".count($banco);
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
		}
	public function save_directivo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['car']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['nac']) && isset($_POST['obs']) && isset($_FILES) && isset($_POST['cuentas'])){
				$ci=trim($_POST['ci']);
				$ciu=trim($_POST['ciu']);
				$nom1=trim($_POST['nom1']);
				$nom2=trim($_POST['nom2']);
				$pat=trim($_POST['pat']);
				$mat=trim($_POST['mat']);
				$car=trim($_POST['car']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$nac=trim($_POST['nac']);
				$obs=trim($_POST['obs']);
				$cuentas=json_decode($_POST['cuentas']);
				$ciudad=$this->M_ciudad->get($ciu);
				if($this->val->entero($ci,6,8) && $ci>100000 && $ci<=99999999 && !empty($ciudad) && $this->val->strSpace($nom1,2,20) && $this->val->strSpace($pat,2,20)){
					$control=true;
					if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
					if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
					if($car!=""){ if(!$this->val->strSpace($car,0,200)){ $control=false;}}
					if($tel!=""){ if(!$this->val->entero($tel,6,15) || $tel<100000 || $tel>999999999999999){ $control=false;}}
					if($ema!=""){ if(!$this->val->email($ema)){ $control=false;}}
					if($nac!=""){ if(!$this->val->fecha($nac)){ $control=false;}}
					if($obs!=""){ if(!$this->val->textarea($obs,0,800)){ $control=false;}}
					if($control){
						$directivo=$this->M_directivo->get_row("ci",$ci);
						if(empty($directivo)){
							$persona=$this->M_persona->get($ci);
							$control="ok";
							if(empty($persona)){
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$ci);
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_persona->insertar($ci,$ciu,$nom1,$nom2,$pat,$mat,"",$tel,$ema,$img,$obs)){
										$control="ok";
									}else{
										$this->lib->eliminar_imagen($img,'./libraries/img/personas/');
										$control="error";
									}
								}else{
									$control=$img;
								}
							}
							if($control=="ok"){
								if($this->M_directivo->insertar($ci,$nac,$car)){
									foreach($cuentas as $key => $cuenta){if($this->M_banco_persona->insertar($cuenta->ba,$ci,$cuenta->cuenta,$cuenta->tipo,"1")){}}
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo $control;
							}
						}else{
							echo "ci_exist_directivo";
						}						
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/ 	
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PLANILLA DE SUELDOS -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['paises']=$this->M_pais->get_all();
			$listado['ciudades']=$this->M_ciudad->get_all();
			$listado['feriados']=$this->M_feriado->get_all();
			$listado['tipo_contratos']=$this->M_tipo_contrato->get_all();
			$this->load->view('capital_humano/config/view',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Pais ---*/
   	public function save_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p'])){
				if($this->val->strSpace($_POST['p'],2,100)){
					if($this->M_pais->insertar($_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpa']) && isset($_POST['p'])){
				if($_POST['idpa']!="" && $this->val->strSpace($_POST['p'],2,100)){
					if($this->M_pais->modificar($_POST['idpa'],$_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpa'])){
				$idpa=$_POST['idpa'];
				$pais=$this->M_pais->get($idpa);
				if(!empty($pais)){
					$listado['titulo']="eliminar el pais <b>".$pais[0]->nombre."</b>";
					$control=$this->M_ciudad->get_row("idpa",$idpa);
					if(!empty($control)){
						$listado['desc']="Imposible eliminar, el pais esta siendo usado por ".count($control)." ciudad(es)";					
					}else{
						$listado['desc']="Se eliminara definitivamente el pais";
					}
						$listado['control']="";
						$listado['open_control']="true";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpa'])){
				$control=$this->M_ciudad->get_row("idpa",$_POST['idpa']);
				if(empty($control)){
					if($this->M_pais->eliminar($_POST['idpa'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Pais ---*/
   	/*--- Ciudad ---*/
   	public function save_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['cp']) && isset($_POST['c'])){
				if($this->val->strSpace($_POST['c'],2,100) && isset($_POST['cp'])!=""){
					if($this->M_ciudad->insertar($_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idci']) && isset($_POST['c']) && isset($_POST['cp'])){
				if($_POST['idci']!="" && $this->val->strSpace($_POST['c'],2,100) && $_POST['cp']!=""){
					if($this->M_ciudad->modificar($_POST['idci'],$_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idci'])){
				if($this->M_ciudad->eliminar($_POST['idci'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Ciudad ---*/
   	/*--- manejo de feriados ---*/
	public function save_feriado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['fec']) && isset($_POST['tip']) && isset($_POST['des'])){
				$f=$_POST['fec'];
				$tip=$_POST['tip'];
				$d=$_POST['des'];
				if($this->val->fecha($f) && $this->val->entero($tip,0,10) && $this->val->textarea($d,5,150)){
					if($this->M_feriado->insertar($f,$tip,$d)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_feriado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idf']) && isset($_POST['fec']) && isset($_POST['tip']) && isset($_POST['des'])){
				$idf=$_POST['idf'];
				$f=$_POST['fec'];
				$tip=$_POST['tip'];
				$d=$_POST['des'];
				if($this->val->entero($idf,0,10) && $this->val->fecha($f) && $this->val->entero($tip,0,10) && $this->val->textarea($d,5,150)){
					if($this->M_feriado->modificar($idf,$f,$tip,$d)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_feriado(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idf'])){
				$idf=$_POST['idf'];
				if($this->M_feriado->eliminar($idf)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}   	   	
   	/*--- End manejo de feriados ---*/
   	/*--- manejo de feriados ---*/
	public function save_tipo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['tip']) && isset($_POST['hor']) && isset($_POST['des'])){
				$tip=$_POST['tip'];
				$hor=$_POST['hor'];
				$des=$_POST['des'];
				if($this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $hor>=0 && $hor<=99 && $this->val->textarea($des,0,150)){
					if($this->M_tipo_contrato->insertar($tip,$hor,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function select_principal(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idtc'])){
				$idtc=$_POST['idtc'];
				if($this->val->entero($idtc,0,10)){
					//verificando si esta seleccionado
					$tipo=$this->M_tipo_contrato->get_row_2n('idtc',$idtc,'principal','1');
					if(empty($tipo)){
						if($this->M_tipo_contrato->reset_principal()){
							if($this->M_tipo_contrato->update_row($idtc,'principal','1')){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "error";
						}
					}else{
						if($this->M_tipo_contrato->reset_principal()){
							echo "ok";
						}else{
							echo "error";
						}	
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_tipo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idtc']) && isset($_POST['tip']) && isset($_POST['hor']) && isset($_POST['des'])){
				$idtc=$_POST['idtc'];
				$tip=$_POST['tip'];
				$hor=$_POST['hor'];
				$des=$_POST['des'];
				if($this->val->entero($idtc,0,1) && $this->val->entero($tip,0,1) && $tip>=0 && $tip<=1 && $hor>=0 && $hor<=99 && $this->val->textarea($des,0,150)){
					if($this->M_tipo_contrato->modificar($idtc,$tip,$hor,$des)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_tipo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idtc'])){
				$idtc=$_POST['idtc'];
				if($this->M_tipo_contrato->eliminar($idtc)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}   	   	
   	/*--- End manejo de feriados ---*/
/*------- END MANEJO DE CONFIGURACION -------*/

}
/* End of file capital_humano.php */
/* Location: ./application/controllers/capital_humano.php */