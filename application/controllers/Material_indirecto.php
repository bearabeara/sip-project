<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Material_indirecto extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			$this->load->view('v_material_indirecto',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE MATERIAL ADICIONAL -------*/
	public function search_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['privilegio']=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"indirectos");
			$this->load->view('material_indirecto/material_indirecto/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"indirectos");
			if(!empty($privilegio)){
				$atrib="";
				$val="";
				if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['can'])){
					if($_POST['cod']!=""){
						$atrib="mi.codigo";$val=$_POST['cod'];
					}else{
						if($_POST['nom']!=""){
							$atrib="mi.nombre";$val=$_POST['nom'];
						}else{
							if($_POST['can']!=""){
								$atrib="m.cantidad";$val=$_POST['can'];
							}
						}
					}
				}
				$listado['materiales']=$this->M_material_item->get_material_extra($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('material_indirecto/material_indirecto/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
   	public function new_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidad']=$this->M_unidad->get_all();
			$this->load->view('material_indirecto/material_indirecto/3-nuevo/form',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$idmi=$this->M_material_item->max('idmi')+1;
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales/','',$this->resize,$idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							if($this->M_material_item->insertar($idmi,$this->session->userdata('id'),$cod,$nom,$img,$des)){
								if($this->M_material_extra->insertar($idmi,0)){
									echo "ok";
								}else{
									$this->M_material_item->eliminar($idmi);
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_materiales(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['can'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['materiales']=$this->M_material_item->get_material_extra("","");
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=$_POST['tbl'];
					$listado['cod']=trim($_POST['cod']);
					$listado['nom']=trim($_POST['nom']);
					$listado['can']=trim($_POST['can']);
					$this->load->view('material_indirecto/material_indirecto/4-imprimir/print_materiales',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_materiales(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(isset($_GET['file']) && isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['can'])){
				$atrib="";
				$val="";
					if($_GET['cod']!=""){
						$atrib="mi.codigo";$val=$_GET['cod'];
					}else{
						if($_GET['nom']!=""){
							$atrib="mi.nombre";$val=$_GET['nom'];
						}else{
							if($_GET['can']!=""){
								$atrib="m.cantidad";$val=$_GET['can'];
							}
						}
					}
				$listado['materiales']=$this->M_material_item->get_material_extra($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('material_indirecto/material_indirecto/4-imprimir/export_materiales',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idme'])){
				$idme=$_POST['idme'];
				$material=$this->M_material_item->get_material_extra("m.idme",$idme);
				if(!empty($material)){
					$listado['material']=$material;
					$this->load->view('material_indirecto/material_indirecto/5-reportes/form',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idme'])){
				$idme=$_POST['idme'];
				$material=$this->M_material_item->get_material_extra("m.idme",$idme);
				if(!empty($material)){
					$listado['unidades']=$this->M_unidad->get_all();
					$listado['material']=$material[0];
					$this->load->view('material_indirecto/material_indirecto/6-config/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_FILES) && isset($_POST['me']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des'])){
				$idme=trim($_POST['me']);
				$nom=trim($_POST['nom']);
				$cod=trim($_POST['cod']);
				$med=trim($_POST['med']);
				$des=trim($_POST['des']);
				$material_extra=$this->M_material_item->get_material_extra('m.idme',$idme);
				if($this->val->strSpace($nom,3,200) && count($med)>0 && !empty($material_extra)){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$img=$material_extra[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/materiales/','',$this->resize,$img,$material_extra[0]->idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							if($this->M_material_item->modificar($material_extra[0]->idmi,$med,$cod,$nom,$img,$des)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idme'])){
				$idme=$_POST['idme'];
				$url='./libraries/img/';
				$material_extra=$this->M_material_item->get_material_extra('m.idme',$idme);
				if(!empty($material_extra)){
					$listado['desc']="Se eliminara definitivamente el material indirecto.";
					$listado['control']="";
					$listado['open_control']="false";
					$img='sistema/miniatura/default.jpg';
					if($material_extra[0]->fotografia!=NULL && $material_extra[0]->fotografia!=""){ $img="materiales/miniatura/".$material_extra[0]->fotografia; }
					$listado['titulo']="el material adicional <b>".$material_extra[0]->nombre."</b>";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail $idme";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['me'])){
				$idme=$_POST['me'];
				$material_extra=$this->M_material_item->get_material_extra('m.idme',$idme);
				if(!empty($material_extra)){
					if($this->lib->eliminar_imagen($material_extra[0]->fotografia,'./libraries/img/materiales/')){
						if($this->M_material_item->eliminar($material_extra[0]->idmi)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_aux(){//solo elimina datos de material item
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['mi'])){
				$idmi=$_POST['mi'];
				$material=$this->M_material_item->get($idmi);
				if(!empty($material)){
					if($this->lib->eliminar_imagen($material[0]->fotografia,'./libraries/img/materiales/')){
						if($this->M_material_item->eliminar($material[0]->idmi)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIAL ADICIONAL -------*/
/*------- MANEJO DE MATERIAL INGRESO -------*/
	public function search_ingreso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['privilegio']=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"ingresos");
			$this->load->view('material_indirecto/ingresos/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_ingreso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"ingresos");
			if(!empty($privilegio)){
				$atrib="";
				$val="";
				if(isset($_POST['nom']) && isset($_POST['can'])){
					if($_POST['nom']!=""){
						$atrib="mi.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['can']!=""){
							$atrib="m.cantidad";$val=$_POST['can'];
						}
					}
				}
				$listado['materiales']=$this->M_material_item->get_material_extra($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('material_indirecto/ingresos/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function save_movimiento(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['me']) && isset($_POST['sto']) && isset($_POST['fech']) && isset($_POST['can']) && isset($_POST['obs'])){
				$idme=trim($_POST['me']);
				$fech=str_replace('T', ' ', $_POST['fech']);
				$can=trim($_POST['can']);
				$obs=trim($_POST['obs']);
				$vfecha=explode(" ", $fech);
				$material_extra=$this->M_material_item->get_material_extra("m.idme",$idme);
				if(!empty($material_extra) && $this->val->fecha($vfecha[0]) && $can>0 && $this->val->decimal($can,10,7) && $this->val->textarea($obs,0,300)){
						$stock=$material_extra[0]->cantidad;
						$control=true;
						$ci="";
						$solicitante="";
						if(isset($_POST['emp'])){
							if($stock>=$can){
								$saldo=$stock-$can;
								$empleado=$this->M_empleado->get_empleado('e.ide',$_POST['emp'],false,1);
								if(!empty($empleado)){
									$ci=$empleado[0]->ci;
									$solicitante=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno;
								}else{
									$control=false;
								}
							}else{
								$control=false;
							}
						}else{
							$saldo=$stock+$can;
						}
						if($control){
							if($this->M_material_extra->modificar_row($idme,"cantidad",$saldo)){
								$usuario=$this->session->userdata('nombre')." ".$this->session->userdata('nombre2')." ".$this->session->userdata('paterno');
								if(isset($_POST['emp'])){
									$ing=0; $sal=$can;
								}else{
									$ing=$can; $sal="";
								}
								$resp=$this->M_almacen_material_mov->insertar($usuario,$solicitante,$fech,"MATERIALES INDIRECTOS",$material_extra[0]->codigo,$material_extra[0]->nombre,$stock,$ing,$sal,$obs);
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout|0";
		}
	}
	public function tr_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['me']) && isset($_POST['type']) && isset($_POST['item'])){
				$idme=trim($_POST['me']);
				$type=trim($_POST['type']);
				$material_extra=$this->M_material_item->get_material_extra("m.idme",$idme);
				$privilegio=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"ingresos");
				if(!empty($material_extra) && !empty($privilegio)){
					$listado['material']=$material_extra[0];
					$listado['item']=$_POST['item'];
					$listado['privilegio']=$privilegio[0];
					if($type=="i"){
						$this->load->view('material_indirecto/ingresos/tr_material',$listado);
					}
					if($type=="s"){
						$listado['empleados']=$this->M_empleado->get_empleado("","",false,1);
						$this->load->view('material_indirecto/salidas/tr_material',$listado);
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END MANEJO DE MATERIAL INGRESOS -------*/
/*------- MANEJO DE MATERIAL SALIDAS -------*/
	public function search_salida(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['privilegio']=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"salidas");
			$this->load->view('material_indirecto/salidas/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_salida(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"salidas");
			if(!empty($privilegio)){
				$atrib="";
				$val="";
				if(isset($_POST['nom']) && isset($_POST['can'])){
					if($_POST['nom']!=""){
						$atrib="mi.nombre";$val=$_POST['nom'];
					}else{
						if($_POST['can']!=""){
							$atrib="m.cantidad";$val=$_POST['can'];
						}
					}
				}
				$listado['empleados']=$this->M_empleado->get_empleado("","",false,1);
				$listado['materiales']=$this->M_material_item->get_material_extra($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('material_indirecto/salidas/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END MANEJO DE MATERIAL SALIDAS -------*/
/*------- MANEJO DE OTRO MATERIAL -------*/
	public function search_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidades']=$this->M_unidad->get_all();
			$listado['privilegio']=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"otros");
			$this->load->view('material_indirecto/otros_materiales/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_material_indirecto($this->session->userdata("id"),"otros");
			if(!empty($privilegio)){
				$atrib="";
				$val="";
				if(isset($_POST['cod']) && isset($_POST['nom'])){
					if($_POST['cod']!=""){
						$atrib="mi.codigo";$val=$_POST['cod'];
					}else{
						if($_POST['nom']!=""){
							$atrib="mi.nombre";$val=$_POST['nom'];
						}
					}
				}
				$listado['materiales']=$this->M_material_item->get_otro_material($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('material_indirecto/otros_materiales/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
   	public function new_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidad']=$this->M_unidad->get_all();
			$this->load->view('material_indirecto/otros_materiales/3-nuevo/form',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des'])){
				$nom=$_POST['nom'];
				$cod=$_POST['cod'];
				$med=$_POST['med'];
				$des=$_POST['des'];
				if($this->val->strSpace($nom,3,200) && count($med)>0){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$idmi=$this->M_material_item->max('idmi')+1;
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales/','',$this->resize,$idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							if($this->M_material_item->insertar($idmi,$this->session->userdata('id'),$cod,$nom,$img,$des)){
								if($this->M_material_vario->insertar($idmi)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_otros_materiales(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['nom'])){
					$listado['materiales']=$this->M_material_item->get_otro_material("","");
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=$_POST['tbl'];
					$listado['cod']=trim($_POST['cod']);
					$listado['nom']=trim($_POST['nom']);
					$this->load->view('material_indirecto/otros_materiales/4-imprimir/print_otros_materiales',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_otros_materiales(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(isset($_GET['file']) && isset($_GET['cod']) && isset($_GET['nom'])){
				$atrib="";
				$val="";
				if($_GET['cod']!=""){
					$atrib="mi.codigo";$val=$_GET['cod'];
				}else{
					if($_GET['nom']!=""){
						$atrib="mi.nombre";$val=$_GET['nom'];
					}
				}
				$listado['materiales']=$this->M_material_item->get_otro_material($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('material_indirecto/otros_materiales/4-imprimir/export_otros_materiales',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   public function detalle_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idmv'])){
				$idmv=$_POST['idmv'];
				$material=$this->M_material_item->get_otro_material("m.idmv",$idmv);
				if(!empty($material)){
					$listado['material']=$material[0];
					$this->load->view('material_indirecto/otros_materiales/5-reportes/form',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idmv'])){
				$idmv=$_POST['idmv'];
				$material=$this->M_material_item->get_otro_material("m.idmv",$idmv);
				if(!empty($material)){
					$listado['unidades']=$this->M_unidad->get_all();
					$listado['material']=$material[0];
					$this->load->view('material_indirecto/otros_materiales/6-config/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_FILES) && isset($_POST['mv']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['med']) && isset($_POST['des'])){
				$idmv=trim($_POST['mv']);
				$nom=trim($_POST['nom']);
				$cod=trim($_POST['cod']);
				$med=trim($_POST['med']);
				$des=trim($_POST['des']);
				$material_extra=$this->M_material_item->get_otro_material('m.idmv',$idmv);
				if($this->val->strSpace($nom,3,200) && count($med)>0 && !empty($material_extra)){
					$guardar=true;
					if($cod!=""){ if(!$this->val->strNoSpace($cod,2,15)){ $guardar=false;}}
					if($des!=""){ if(!$this->val->strSpace($des,5,700)){ $guardar=false;}}
					if($guardar){
						$img=$material_extra[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/materiales/','',$this->resize,$img,$material_extra[0]->idmi);
						if($img!="error_type" && $img!="error" && $img!="error_size_img"){
							if($this->M_material_item->modificar($material_extra[0]->idmi,$med,$cod,$nom,$img,$des)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idmv'])){
				$idmv=$_POST['idmv'];
				$url='./libraries/img/';
				$material_extra=$this->M_material_item->get_otro_material('m.idmv',$idmv);
				if(!empty($material_extra)){
					$listado['desc']="Se eliminara definitivamente el material.";
					$listado['control']="";
					$listado['open_control']="false";
					$img='sistema/miniatura/default.jpg';
					if($material_extra[0]->fotografia!=NULL && $material_extra[0]->fotografia!=""){ $img="materiales/miniatura/".$material_extra[0]->fotografia; }
					$listado['titulo']="el material <b>".$material_extra[0]->nombre."</b>";
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail $idme";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_otro_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['mv'])){
				$idmv=$_POST['mv'];
				$material_extra=$this->M_material_item->get_otro_material('m.idmv',$idmv);
				if(!empty($material_extra)){
					if($this->lib->eliminar_imagen($material_extra[0]->fotografia,'./libraries/img/materiales/')){
						if($this->M_material_item->eliminar($material_extra[0]->idmi)){
							echo "ok";
						}else{
							echo "error";
						}
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE OTRO MATERIAL -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['unidades']=$this->M_unidad->get_all();
			$this->load->view('material_indirecto/config/view',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Manejo de unidad ---*/
	public function save_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15c==1){
					if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
						$nom=$_POST['nom'];
						$abr=$_POST['abr'];
						$equ=$_POST['equ'];
						$des_equ=$_POST['des'];
						if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,8) && $this->val->textarea($des_equ,0,100)){
							if($this->M_unidad->insertar($nom,$abr,$equ,$des_equ)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function update_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15u==1){
					if(isset($_POST['idu']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
						$idu=$_POST['idu'];
						$nom=$_POST['nom'];
						$abr=$_POST['abr'];
						$equ=$_POST['equ'];
						$des_equ=$_POST['des'];
						if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,10) && $this->val->textarea($des_equ,0,100)){
							if($this->M_unidad->modificar($idu,$nom,$abr,$equ,$des_equ)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirmar_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['idu'])){
						$idu=$_POST['idu'];
						$unidad=$this->M_unidad->get($idu);
						if(!empty($unidad)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_material_item->get_row('idu',$idu);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar la unidad de medida esta siendo usada";	
							}else{
								$listado['titulo']="eliminar la unidad de medida <strong>".$unidad[0]->nombre."</strong>?";
								$listado['desc']="Se eliminara definitivamente la unidad de medida.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_unidad(){
		if (!empty($this->session_id)) {
			if(isset($_POST['idu'])){
				$idu=$_POST['idu'];
				$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
				if(!empty($privilegio)){
					if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
						$control=$this->M_material_item->get_row('idu',$idu);
						if(empty($control)){
							if($this->M_unidad->eliminar($idu)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}	
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "fail";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Manejo de unidad ---*/
/*------- END MANEJO DE CONFIGURACION -------*/















}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */