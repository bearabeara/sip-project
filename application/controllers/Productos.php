<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productos extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
			$this->load->view('v_producto');
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="1"){
					$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
					$this->val->redireccion($privilegio);
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE ALMACENES -------*/
	public function search(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view('producto/list/search');
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function view(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$atrib="";$val="";
			if(isset($_POST['nom'])){ $atrib="p.nombre";$val=$_POST['nom']; }
			$listado['productos']=$this->M_producto->get_colores($atrib,$val,"1");
			$this->load->view('producto/list/view',$listado);
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function detalle($idpgc){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($idpgc)){
				$listado['pgc']=$idpgc;
				$this->load->view('producto/detalle',$listado);
			}else{
				redirect(base_url().'productos');
			}
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function search_detalle(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc'])){
				$listado['pgc']=$_POST['pgc'];
				$this->load->view('producto/detalle/search',$listado);
			}
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function view_detalle(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['pgc'])){
				$idpgc=$_POST['pgc'];
				$producto=$this->M_producto->get_colores("pgc.idpgrc",$idpgc,"1");
				if(!empty($producto)){
					$listado['producto']=$producto[0];
					$listado['color_atributos']=$this->M_producto_grupo_color_atributo->get_atributo("pgca.idpgrc",$producto[0]->idpgrc);
					$listado['grupo_atributos']=$this->M_producto_grupo_atributo->get_atributo("pga.idpgr",$producto[0]->idpgr);
					$listado['producto_atributos']=$this->M_producto_atributo->get_atributo("pa.idp",$producto[0]->idp);
					$listado['color_materiales']=$this->M_producto_grupo_color_material->get_material('pgcm.idpgrc',$producto[0]->idpgrc);
					$listado['grupo_materiales']=$this->M_producto_grupo_material->get_material('pgm.idpgr',$producto[0]->idpgr);
					$listado['producto_materiales']=$this->M_producto_material->get_material('pm.idp',$producto[0]->idp);
					$listado['producto_piezas']=$this->M_producto_pieza->get_row('idp',$producto[0]->idp);
					$listado['color_piezas']=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$idpgc);
					$listado['all_materiales']=$this->M_material_item->get_material("","");
					//$listado['materiales_imagenes']=$this->M_material_imagen->get_row('portada','1');
					$this->load->view('producto/detalle/view',$listado);
				}else{
					echo "<h3>Producto no encontrado!</h3>";
				}
			}
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function view_visor(){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idp']) && isset($_POST['id']) && isset($_POST['type']) && isset($_POST['position']) && isset($_POST['pgc'])){
				$idp=$_POST['idp'];
				$type=$_POST['type'];
				$v=array();
				if($type=="producto"){
					if(isset($_POST['type-img'])){
						$idpgrc=$_POST['pgc'];
						$producto_grupo_color=$this->M_producto_grupo_color->get_row('idpgrc',$idpgrc);
						if(!empty($producto_grupo_color)){
							$producto_grupo=$this->M_producto_grupo->get_row('idpgr',$producto_grupo_color[0]->idpgr);
							$fotografias_color=$this->M_producto_imagen_color->get_row('idpgrc',$producto_grupo_color[0]->idpgrc);	
							$fotografias_producto=$this->M_producto_imagen->get_row('idp',$producto_grupo[0]->idp);
							$v = array();
						    for($i=0; $i < count($fotografias_color); $i++){
						        $v[] = array('tipo' => 'color', 'id' => $fotografias_color[$i]->idpig,'archivo' => "productos/".$fotografias_color[$i]->archivo,'descripcion' => $fotografias_color[$i]->descripcion,'position' => $_POST['position']);
						    }
						    for ($i=0; $i < count($fotografias_producto); $i++){
						        $v[] = array('tipo' => 'producto', 'id' => $fotografias_producto[$i]->idpi,'archivo' => "productos/".$fotografias_producto[$i]->archivo,'descripcion' => $fotografias_producto[$i]->descripcion,'position' => $_POST['position']);
						    }
						}else{
							echo "<h1>Error</h1>";
						}
					}else{
						echo "<h1>Error</h1>";
					}
				}
				if($type=="material"){
					$producto_grupo_color=$this->M_producto_grupo_color->get($_POST['pgc']);
					if(!empty($producto_grupo_color)){
						$producto_grupo=$this->M_producto_grupo->get($producto_grupo_color[0]->idpgr);
						$color_materiales=$this->M_producto_grupo_color_material->get_material('pgcm.idpgrc',$producto_grupo_color[0]->idpgrc);
						$grupo_materiales=$this->M_producto_grupo_material->get_material('pgm.idpgr',$producto_grupo[0]->idpgr);
						$producto_materiales=$this->M_producto_material->get_material('pm.idp',$producto_grupo[0]->idp);
	                        $v_material = array();
	                        for($i=0; $i < count($producto_materiales); $i++){ $material=$producto_materiales[$i];
	                            $img="sistema/default.jpg";//$fotografia=$this->M_material_imagen->get_row_2n("idmi",$material->idmi,"portada","1");
	                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/".$material->fotografia;}
	                            $unidad=$this->M_unidad->get($material->idu);
	                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
	                        }
	                        for($i=0; $i < count($grupo_materiales); $i++){ $material=$grupo_materiales[$i];
	                            $img="sistema/default.jpg";//$fotografia=$this->M_material_imagen->get_row_2n("idmi",$material->idmi,"portada","1");
	                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/".$material->fotografia;}
	                            $unidad=$this->M_unidad->get($material->idu);
	                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
	                        }
	                        for($i=0; $i < count($color_materiales); $i++){ $material=$color_materiales[$i];
	                            $img="sistema/default.jpg";//$fotografia=$this->M_material_imagen->get_row_2n("idmi",$material->idmi,"portada","1");
	                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/".$material->fotografia;}
	                            $unidad=$this->M_unidad->get($material->idu);
	                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
	                        }
	                        $materiales=$this->lib->sort_2d($v_material,'nombre','asc');
	                        foreach($materiales as $key => $material){
	                        	$descripcion="<b>".$material->nombre." - ".$material->color."</b>";
	                        	if($descripcion!=""){ $descripcion.=", ";}$descripcion.="cantidad: ".$material->cantidad." ".$material->medida;
	                        	if($material->observacion!="" || $material->observacion!=NULL){ if($descripcion!=""){ $descripcion.=", ";} $descripcion.=$material->observacion;}
						        $v[] = array('tipo' => 'material', 'id' => $material->idm,'archivo' => $material->imagen,'descripcion' => $descripcion,'position' => $_POST['position']);
	                        }
					}else{
						echo "fail";
					}
				}
				if($type=="repujado_sello"){
					$producto_piezas=$this->M_producto_pieza->get_row('idp',$idp);
					$color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$_POST['pgc']);
	                $v_rs=array();
	                for($i=0; $i < count($producto_piezas); $i++){ $pieza=$producto_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                     if($pieza->idrs!=0){
	                        $re_se=$this->M_repujado_sello->get($pieza->idrs);
	                        if(!empty($re_se)){
	                            $img_rs="sistema/default.jpg";
	                            if($re_se[0]->imagen!="" && $re_se[0]->imagen!=NULL){ $img_rs="repujados_sellos/".$re_se[0]->imagen;}
	                            $v_rs[]=array('rs'=>$re_se[0]->idrs,'imagen_rs' => $img_rs,'nombre' => $re_se[0]->nombre,'tipo' => $re_se[0]->tipo,'nombre_pieza' =>$pieza->nombre,'observacion' => $pieza->descripcion);
	                        }
	                     }
	                }
	                for($i=0; $i < count($color_piezas); $i++){ $pieza=$color_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                    if($pieza->idrs!=0){
	                        $re_se=$this->M_repujado_sello->get($pieza->idrs);
	                        if(!empty($re_se)){
	                            $img_rs="sistema/default.jpg";
	                            if($re_se[0]->imagen!="" && $re_se[0]->imagen!=NULL){ $img_rs="repujados_sellos/".$re_se[0]->imagen;}
	                            $v_rs[]=array('rs'=>$re_se[0]->idrs,'imagen_rs' => $img_rs,'nombre' => $re_se[0]->nombre,'tipo' => $re_se[0]->tipo,'nombre_pieza' =>$pieza->nombre,'observacion' => $pieza->descripcion);
	                        }
	                     }
	                }
	                $repujados_sellos=$this->lib->sort_2d($v_rs,'nombre','asc');
					foreach($repujados_sellos as $key => $rs){
						if($rs->tipo==0){ $tipo="Repujado";}if($rs->tipo==1){ $tipo="Sello";}
	                    $descripcion="<b>".$tipo." ".$rs->nombre."</b>";
	                    if($descripcion!=""){ $descripcion.=", ";}$descripcion.="posicion: en la pieza <b>".$rs->nombre_pieza."</b>";
	                    if($rs->observacion!="" || $rs->observacion!=NULL){ if($descripcion!=""){ $descripcion.=", ";} $descripcion.=$rs->observacion;}
						$v[] = array('tipo' => 'repujado_sello', 'id' => $rs->rs,'archivo' => $rs->imagen_rs,'descripcion' => $descripcion,'position' => $_POST['position']);
					}
				}
				if($type=="pieza"){
					$producto_piezas=$this->M_producto_pieza->get_row('idp',$idp);
					$color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$_POST['pgc']);
	                $v_piezas = array();
	                for($i=0; $i < count($producto_piezas); $i++){ $pieza=$producto_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                     $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idppi,'type' => 'ppi','rs' => $pieza->idrs,'fotografia' => $img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
	                }
	                for($i=0; $i < count($color_piezas); $i++){ $pieza=$color_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                    $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idpgcpi,'type' => 'pgcpi','rs' => $pieza->idrs,'fotografia' => $img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
	                }
	                $piezas=$this->lib->sort_2d($v_piezas,'nombre','desc');
					foreach($piezas as $key => $pieza){
	                    $descripcion="<b>".$pieza->nombre."</b>";
	                    if($descripcion!=""){ $descripcion.=", ";}$descripcion.="dimensiones: <b>".$pieza->ancho." [cm.] largo x ".$pieza->ancho." [cm.] ancho</b>";
	                    if($descripcion!=""){ $descripcion.=", ";}$descripcion.="cantidad ".$pieza->cantidad." [unid.]";
	                    if($descripcion!=""){ $descripcion.=", ";}$descripcion.=$pieza->observacion;
						$v[] = array('tipo' => 'pieza', 'id' => $pieza->id,'archivo' => $pieza->fotografia,'descripcion' => $descripcion,'position' => $_POST['position']);
					}
				}
				if($type=="pieza_repujado_sello"){
					$producto_piezas=$this->M_producto_pieza->get_row('idp',$idp);
					$color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$_POST['pgc']);
	                $v_piezas = array();
	                for($i=0; $i < count($producto_piezas); $i++){ $pieza=$producto_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                     $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idppi,'type' => 'ppi','rs' => $pieza->idrs,'fotografia' => $img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
	                }
	                for($i=0; $i < count($color_piezas); $i++){ $pieza=$color_piezas[$i];
	                    $img="sistema/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/".$pieza->fotografia;}
	                    $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idpgcpi,'type' => 'pgcpi','rs' => $pieza->idrs,'fotografia' => $img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
	                }
	                $piezas=$this->lib->sort_2d($v_piezas,'nombre','desc');
					foreach($piezas as $key => $pieza){
						if($pieza->rs!=0){
	                        $re_se=$this->M_repujado_sello->get($pieza->rs);
	                        if(!empty($re_se)){
	                        	$rs=$re_se[0];
	                        	if($rs->tipo==0){ $tipo="Repujado";}if($rs->tipo==1){ $tipo="Sello";}
			                    $descripcion="<b>".$tipo." ".$rs->nombre."</b>";
			                    if($descripcion!=""){ $descripcion.=", ";}$descripcion.="posicion: en la pieza <b>".$pieza->nombre."</b>";
			                    if($pieza->observacion!="" || $pieza->observacion!=NULL){ if($descripcion!=""){ $descripcion.=", ";} $descripcion.=$pieza->observacion;}
			                    $img="sistema/default.jpg";
								if($rs->imagen!="" && $rs->imagen!=NULL){ $img="repujados_sellos/".$rs->imagen; }
								$v[] = array('tipo' => 'pieza_repujado_sello', 'id' => $rs->idrs,'archivo' => $img,'descripcion' => $descripcion,'position' => $_POST['position']);
	                        }
	                    }
					}
				}
				$listado['fotografias']=json_decode(json_encode($v));
				$producto=$this->M_producto->get($idp);
				$listado['producto']=$producto[0];
				$this->load->view('producto/tools/visor',$listado);
			}else{
				echo "<h1>Error</h1>";
			}
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
	public function full_screan_galery($idpgc){
		if(!empty($this->session_id) && ($this->session_tipo=="0"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if (isset($idpgc)){
				$producto_grupo_color=$this->M_producto_grupo_color->get_row('idpgrc',$idpgc);
				if(!empty($producto_grupo_color)){
					$producto_grupo=$this->M_producto_grupo->get_row('idpgr',$producto_grupo_color[0]->idpgr);
					$listado['fotografias_color']=$this->M_producto_imagen_color->get_row('idpgrc',$idpgc);	
					$listado['fotografias_producto']=$this->M_producto_imagen->get_row('idp',$producto_grupo[0]->idp);
					$this->load->view('producto/detalle/full_screan_galery',$listado);
				}else{
					echo "Error";
				}
			}else{
				echo "Error";
			}
		}else{
				if(!empty($this->actualizaciones)){
					redirect(base_url().'update');
				}else{
					echo "logout";
				}
			}
	}
}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */