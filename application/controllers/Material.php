<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Material extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['']="";
			date_default_timezone_set("America/La_Paz");
			if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
			$this->load->view('v_material',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE MATERIAL -------*/
	public function search_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1){
					$listado['grupo']=$this->M_material_grupo->get_all();
					$listado['unidad']=$this->M_unidad->get_all();
					$listado['color']=$this->M_color->get_all();
					$listado['privilegio']=$privilegio[0];
					$this->load->view('material/materia_prima/search',$listado);
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_material($almacen){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1){
					$atrib="";$val="";$type_search="";
					if(isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['gru']) && isset($_POST['can']) && isset($_POST['col'])){
						$cod=$_POST['cod'];
						$nom=$_POST['nom'];
						$gru=$_POST['gru'];
						$can=$_POST['can'];
						$col=$_POST['col'];
						if($cod!=""){
							$atrib="codigo";$val=$cod;$type_search="like";
						}else{
							if($nom!=""){
								$atrib="nombre";$val=$nom;$type_search="like";
							}else{
								if($gru!=""){
									$atrib="idmg";$val=$gru;$type_search="equals";
								}else{
									if($can!=""){
										$atrib="cantidad";$val=$can;$type_search="like";
									}else{
										if($col!=""){
											$atrib="idco";$val=$col;$type_search="equals";
										}
									}
								}
							}
						}
					}
					$listado['materiales']=$this->M_almacen_material->get_search($almacen,"","");
					$listado["privilegio"]=$privilegio[0];
					$listado['atrib']=$atrib;
					$listado['val']=$val;
					$listado['type_search']=$type_search;
					$this->load->view('material/materia_prima/view',$listado);
				}else{
					$this->load->view('estructura/locked');
				}
			}else{
				$this->load->view('estructura/locked');
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
   	public function new_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al=="1" && $privilegio[0]->al11r=="1"){
					$listado['grupo']=$this->M_material_grupo->get_all();
					$listado['unidad']=$this->M_unidad->get_all();
					$listado['color']=$this->M_color->get_all();
					$this->load->view('material/materia_prima/3-nuevo/new',$listado);
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1 && $privilegio[0]->al11c==1){
					if(isset($ida) && isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['gru']) && isset($_POST['med']) && isset($_POST['col']) && isset($_POST['des'])){
						$nom=$_POST['nom'];
						$cod=$_POST['cod'];
						$gru=$_POST['gru'];
						$med=$_POST['med'];
						$col=$_POST['col'];
						$des=$_POST['des'];
						$almacen=$this->M_almacen->get_row_2n("ida",$ida,"tipo","0");
						if(count($_FILES)<=10 && $this->val->entero($gru,0,10) && $this->val->strSpace($cod,2,15) && $this->val->strSpace($nom,2,100) && $this->val->entero($med,0,10) && $this->val->entero($col,0,10) && !empty($almacen)){
							$control=true;
							if($des!=""){ if(!$this->val->textarea($des,0,300)){ $control=false; }}
							if($control){
								$idmi=$this->M_material_item->max("idmi")+1;
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/materiales/','',$this->resize,$idmi);
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_material_item->insertar($idmi,$med,$cod,$nom,$img,$des)){
										$idm=$this->M_material->max("idm")+1;
										if($this->M_material->insertar($idm,$idmi,$gru,$col,0)){
											if($this->M_almacen_material->insertar($ida,$idm,0)){
												//guardadndo notificaciones
													if($this->M_almacen_material_seg->insertar($ida,$almacen[0]->nombre,$idm,$nom,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"insert")){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"material");
													foreach ($resultados as $key => $result) {
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_material($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"material",$result->cantidad)){}
														}
													}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												$this->M_material_item->eliminar($idmi);
												echo "error";
											}
										}else{
											$this->M_material_item->eliminar($idmi);
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}								
							}else{
								echo "fail";
							}
						}else{
							echo "fail $ida";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function add_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($ida)){
				$listado['almacenes']=$this->M_almacen->get_all_not($ida);
				$this->load->view('material/materia_prima/3-nuevo/form_add',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_add_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($ida)){
				$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
				if(!empty($privilegio)){
					if(isset($_POST['alm'])){
						$almacen=$_POST['alm'];
						if($almacen!=""){
							$listado['materiales']=$this->M_almacen_material->get_not_material_where($ida,$almacen);
						}else{
							$listado['materiales']=$this->M_almacen_material->get_not_material($ida);	
						}
					}else{
						$listado['materiales']=$this->M_almacen_material->get_not_material($ida);	
					}
					$listado['ida']=$ida;
					$listado['privilegio']=$privilegio[0];
					$this->load->view('material/materia_prima/3-nuevo/material/add_material',$listado);
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_material_almacen($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1 && $privilegio[0]->al11c==1 && $privilegio[0]->al11u==1){
					if(isset($_POST['idm']) && isset($_POST['ida'])){
						if($_POST['idm']!="" && $_POST['ida']!=""){
							$idm=trim($_POST['idm']);
							$ida=trim($_POST['ida']);
							$existe=$this->M_almacen_material->get_row_2n('idm',$idm,'ida',$ida);
							if(!empty($existe)){
								if($existe[0]->cantidad==0){
									if($this->M_almacen_material->eliminar($existe[0]->idam)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								if($this->M_almacen_material->insertar($ida,$idm,0)){
									echo "ok";
								}else{
									echo "error";
								}
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			echo "logout";
		}
	}

   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
	public function print_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['cod']) && isset($_POST['nom']) && isset($_POST['gru']) && isset($_POST['can']) && isset($_POST['col'])){
				$almacen=$this->M_almacen->get($ida);
				if(!empty($almacen)){
					$listado['tbl']=trim($_POST['tbl']);
					$listado['cod']=trim($_POST['cod']);
					$listado['nom']=trim($_POST['nom']);
					$listado['gru']=trim($_POST['gru']);
					$listado['can']=trim($_POST['can']);
					$listado['col']=trim($_POST['col']);
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['materiales']=$this->M_almacen_material->get_search($ida,"","");
					$listado['almacen']=$almacen[0];
					$this->load->view('material/materia_prima/4-imprimir/print_material',$listado);
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function excel_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_GET['alm'])){
				$alm=$_GET['alm'];
				$almacen=$this->M_almacen->get($alm);
				if(!empty($almacen)){
					$atrib="";$val="";
					if(isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['gru']) && isset($_GET['can']) && isset($_GET['col'])){
						$cod=$_GET['cod'];
						$nom=$_GET['nom'];
						$gru=$_GET['gru'];
						$can=$_GET['can'];
						$col=$_GET['col'];
						if($cod!=""){
							$atrib="mi.codigo";$val=$cod;
						}else{
							if($nom!=""){
								$atrib="mi.nombre";$val=$nom;
							}else{
								if($gru!=""){
									$atrib="g.idmg";$val=$gru;
								}else{
									if($can!=""){
										$atrib="am.cantidad";$val=$can;
									}else{
										if($col!=""){
											$atrib="c.idco";$val=$col;
										}
									}
								}
							}
						}
					}
					$listado['materiales']=$this->M_almacen_material->get_search($almacen[0]->ida,$atrib,$val);
					$listado['almacen']=$almacen[0];
					$this->load->view('material/materia_prima/4-imprimir/excel_material',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function word_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_GET['alm'])){
				$alm=$_GET['alm'];
				$almacen=$this->M_almacen->get($alm);
				if(!empty($almacen)){
					$atrib="";$val="";
					if(isset($_GET['cod']) && isset($_GET['nom']) && isset($_GET['gru']) && isset($_GET['can']) && isset($_GET['col'])){
						$cod=$_GET['cod'];
						$nom=$_GET['nom'];
						$gru=$_GET['gru'];
						$can=$_GET['can'];
						$col=$_GET['col'];
						if($cod!=""){
							$atrib="mi.codigo";$val=$cod;
						}else{
							if($nom!=""){
								$atrib="mi.nombre";$val=$nom;
							}else{
								if($gru!=""){
									$atrib="g.idmg";$val=$gru;
								}else{
									if($can!=""){
										$atrib="am.cantidad";$val=$can;
									}else{
										if($col!=""){
											$atrib="c.idco";$val=$col;
										}
									}
								}
							}
						}
					}
					$listado['materiales']=$this->M_almacen_material->get_search($almacen[0]->ida,$atrib,$val);
					$listado['almacen']=$almacen[0];
					$this->load->view('material/materia_prima/4-imprimir/word_material',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	public function img_material(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idmi'])){
				$material=$this->M_material_item->get_row("idmi",$_POST['idmi']);
				if(!empty($material)){
					$imagenes=$this->M_material_imagen->get_row("idmi",$_POST['idmi']);
					$url=base_url()."libraries/img/materiales/";
					$img="default.png";
					$archivos = array();
					if(!empty($imagenes)){
						for ($i=0; $i < count($imagenes) ; $i++) { 
							$img=$imagenes[$i]->nombre;
							$titulo=$imagenes[$i]->titulo;
							$descripcion=$imagenes[$i]->descripcion;
							if($titulo=="" || $titulo==NULL){ $titulo=$material[0]->nombre; }
							$archivos[] = array('titulo' => $titulo,'url' =>  $url.$img,'descripcion' => $descripcion);
						}
					}else{
						$archivos[] = array('titulo' => $material[0]->nombre,'url' =>  $url.$img,'descripcion' => "");
					}
					$listado['imagenes']=json_decode(json_encode($archivos));
					$this->load->view('estructura/visor',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Reportes ---*/
   	public function detalle_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(!empty($_POST['am'])){
				$idam=trim($_POST['am']);
				$material=$this->M_almacen_material->get_row_2n_complet("am.ida",$ida,"am.idam",$idam);
				if(!empty($material)){
					$listado['material']=$material;
					$this->load->view('material/materia_prima/5-reportes/view',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function material_productos($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(!empty($_POST['am'])){
				$idam=trim($_POST['am']);
				$material=$this->M_almacen_material->get_row_2n_complet("am.ida",$ida,"am.idam",$idam);
				if(!empty($material)){
					$listado['material']=$material[0];
//					echo $material[0]->idm;
					$listado['productos_material']=$this->M_producto_material->get_row("idm",$material[0]->idm);
					$listado['productos_grupos_material']=$this->M_producto_grupo_material->get_grupo("pgm.idm",$material[0]->idm);
					$listado['productos_colores_material']=$this->M_producto_grupo_color_material->get_grupo("pgcm.idm",$material[0]->idm);
					$listado['productos']=$this->M_producto->get_colores("","","");
					$this->load->view('material/materia_prima/5-reportes/productos',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function config_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(!empty($_POST['am'])){
				$idam=trim($_POST['am']);
				$material=$this->M_almacen_material->get_row_2n_complet("am.ida",$ida,"am.idam",$idam);
				if(!empty($material)){
					$listado['grupo']=$this->M_material_grupo->get_all();
					$listado['unidad']=$this->M_unidad->get_all();
					$listado['color']=$this->M_color->get_all();
					$listado['material']=$material[0];
					$this->load->view('material/materia_prima/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1 && $privilegio[0]->al11u==1){
					if(isset($_POST['idm']) && isset($_FILES) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['gru']) && isset($_POST['med']) && isset($_POST['col']) && isset($_POST['des'])){
						$idm=trim($_POST['idm']);
						$nom=trim($_POST['nom']);
						$cod=trim($_POST['cod']);
						$gru=trim($_POST['gru']);
						$med=trim($_POST['med']);
						$col=trim($_POST['col']);
						$des=trim($_POST['des']);
						$material=$this->M_material->get($idm);
						$almacen=$this->M_almacen->get_row_2n("ida",$ida,"tipo","0");
						if(!empty($material) && !empty($almacen)){
							$material_item=$this->M_material_item->get($material[0]->idmi);
							if($this->val->entero($gru,0,10) && $this->val->strSpace($cod,2,15) && $this->val->strSpace($nom,1,100) && $this->val->entero($med,0,10) && $this->val->entero($col,0,10)){
								$img=$material_item[0]->fotografia;
								$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/materiales/','',$this->resize,$img,$material[0]->idmi);
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_material_item->modificar($material[0]->idmi,$med,$cod,$nom,$img,$des)){
										if($this->M_material->modificar($idm,$gru,$col)){
													//guardadndo notificaciones
														if($this->M_almacen_material_seg->insertar($ida,$almacen[0]->nombre,$idm,$nom,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"update")){}
														$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"material");
														foreach ($resultados as $key => $result) {
															if($result->idno=="none"){
																if($this->M_notificacion->insertar_material($result->emisor,$result->reseptor,$result->cantidad)){}
															}else{
																if($this->M_notificacion->modificar_row($result->idno,"material",$result->cantidad)){}
															}
														}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}	
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}	
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['am'])){
				$idam=trim($_POST['am']);
				$material=$this->M_almacen_material->get_row_2n_complet("am.ida",$ida,"am.idam",$idam);
				if(!empty($material)){
					$url='./../../libraries/img/';
					$listado['titulo']="el material ".$material[0]->nombre;
					if($material[0]->cantidad>0){
						$listado['open_control']="true";
						$listado['control']="";
						$listado['desc']="Imposible eliminar materiales que tengan una stock mayor a cero.";
						$listado['btn_ok']="none";
					}else{
						$listado['desc']="Se eliminara definitivamente el material del almacen";
					}
	    			$img="sistema/miniatura/default.jpg";
			    	if($material[0]->fotografia!="" && $material[0]->fotografia!=NULL){ $img="materiales/miniatura/".$material[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function drop_material($ida){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al11r==1 && $privilegio[0]->al11d==1){
		   			if(isset($_POST['am']) && isset($_POST['u']) && isset($_POST['p'])){
			   			$idam=trim($_POST['am']);
			   			$u=$_POST['u'];
						$p=$_POST['p'];
						if($u==$this->session->userdata("login")){
							$usuario=$this->M_usuario->validate($u,$p);
							if(!empty($usuario)){
								$material=$this->M_almacen_material->get_row_2n_complet("am.ida",$ida,"am.idam",$idam);
								$almacen=$this->M_almacen->get_row_2n("ida",$ida,"tipo","0");
								if(!empty($material) && !empty($almacen)){
									if($material[0]->cantidad<=0){
										$control=$this->M_almacen_material->get_row('idm',$material[0]->idm);
										if(count($control)>1){// si esta en mas de una almacen
											if($this->M_almacen_material->eliminar($idam)){
												//guardadndo notificaciones
													if($this->M_almacen_material_seg->insertar($ida,$almacen[0]->nombre,$material[0]->idm,$material[0]->nombre,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"delete")){}
													$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"material");
													foreach($resultados as $key => $result){
														if($result->idno=="none"){
															if($this->M_notificacion->insertar_material($result->emisor,$result->reseptor,$result->cantidad)){}
														}else{
															if($this->M_notificacion->modificar_row($result->idno,"material",$result->cantidad)){}
														}
													}
												$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
												echo json_encode($result);
											}else{
												echo "error";
											}
										}else{
											if($this->lib->eliminar_imagen($material[0]->fotografia,'./libraries/img/materiales/')){
				    							if($this->M_material_item->eliminar($material[0]->idmi)){
													//guardadndo notificaciones
														if($this->M_almacen_material_seg->insertar($ida,$almacen[0]->nombre,$material[0]->idm,$material[0]->nombre,$this->session->userdata('id'),$this->session->userdata('nombre')." ".$this->session->userdata('paterno'),date('Y-m-d'),"delete")){}
														$resultados=$this->lib->notifications($this->session->userdata('id'),$this->M_usuario->get_all(),$this->M_notificacion->get_all(),"material");
														foreach($resultados as $key => $result){
															if($result->idno=="none"){
																if($this->M_notificacion->insertar_material($result->emisor,$result->reseptor,$result->cantidad)){}
															}else{
																if($this->M_notificacion->modificar_row($result->idno,"material",$result->cantidad)){}
															}
														}
													$result=array();$result[] = array('result' => 'ok','tipo'=>'2');
													echo json_encode($result);
				    							}else{
				    								echo "error";
				    							}
											}else{
												echo "error";
											}
										}
									}else{
										echo "fail";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "validate";
						}
		   			}else{
		   				echo "fail";
		   			}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
   		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
   	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE MATERIAL -------*/
/*------- MANEJO DE INGRESO DE MATERIAL -------*/
	public function search_ingreso(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"ingreso");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al12r==1){
					$listado['grupo']=$this->M_material_grupo->get_all();
					$listado['unidad']=$this->M_unidad->get_all();
					$listado['color']=$this->M_color->get_all();
					$this->load->view('material/ingresos/search',$listado);
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_ingreso($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"ingreso");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al12r==1){
					$atrib="";$val="";$type_search="";
					if(isset($_POST['nom']) && isset($_POST['can']) && isset($_POST['uni'])){
						$nom=$_POST['nom'];
						$can=$_POST['can'];
						$uni=$_POST['uni'];
						if($nom!=""){
							$atrib="nombre";$val=$nom;$type_search="like";
						}else{
							if($can!=""){
								$atrib="cantidad";$val=$can;$type_search="like";
							}else{
								if($uni!=""){
									$atrib="idu";$val=$uni;$type_search="equals";
								}
							}
						}
					}
					$listado['materiales']=$this->M_almacen_material->get_search($ida,"","");
					$listado["privilegio"]=$privilegio[0];
					$listado['atrib']=$atrib;
					$listado['val']=$val;
					$listado['type_search']=$type_search;
					$this->load->view('material/ingresos/view',$listado);
				}else{
					$this->load->view('estructura/locked');
				}
			}else{
				$this->load->view('estructura/locked');
			}

		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
   	public function save_movimiento($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idam']) && isset($_POST['sto']) && isset($_POST['fech']) && isset($_POST['can']) && isset($_POST['obs'])){
				$idam=$_POST['idam'];
				$fech=str_replace('T', ' ', $_POST['fech']);
				$can=$_POST['can'];
				$obs=$_POST['obs'];
				$vfecha=explode(" ", $fech);
				if($this->val->entero($idam,0,10) && $this->val->fecha($vfecha[0]) && $can>0 && $this->val->decimal($can,10,7) && $this->val->textarea($obs,0,300)){
					$material=$this->M_almacen_material->get_row_2n_complet("am.idam",$idam,"","");
					$almacen=$this->M_almacen->get_col($ida,'nombre');
					if(!empty($material) && !empty($almacen)){
						$stock=$material[0]->cantidad;
						$control=true;
						$ci="";
						$solicitante="";
						if(isset($_POST['emp'])){
							if($stock>=$can){
								$saldo=$stock-$can;
								$empleado=$this->M_empleado->get_empleado('e.ide',$_POST['emp'],false,1);
								if(!empty($empleado)){
									$ci=$empleado[0]->ci;
									$solicitante=$empleado[0]->nombre." ".$empleado[0]->nombre2." ".$empleado[0]->paterno;
								}else{
									$control=false;
								}
							}else{
								$control=false;
							}
						}else{
							$saldo=$stock+$can;
						}
						if($control){
							if($this->M_almacen_material->modificar_cantidad($idam, $saldo)){
								$usuario=$this->session->userdata('nombre')." ".$this->session->userdata('nombre2')." ".$this->session->userdata('paterno');
								if(isset($_POST['emp'])){
									$ing=0; $sal=$can;
								}else{
									$ing=$can; $sal="";
								}
								$resp=$this->M_almacen_material_mov->insertar($usuario,$solicitante,$fech,$almacen[0]->nombre,$material[0]->codigo,$material[0]->nombre,$stock,$ing,$sal,$obs);
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			echo "logout";
		}
	}
	public function tr_material($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idam']) && isset($_POST['type']) && isset($_POST['item']) && isset($_POST['nom']) && isset($_POST['can']) && isset($_POST['uni'])){
				$idam=trim($_POST['idam']);
				$type=trim($_POST['type']);
				$item=trim($_POST['item']);
				$almacen=$this->M_almacen->get($ida);
				if($type=="ingreso"){
					$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"ingreso");
				}else{
					$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"salida");
				}
				if(!empty($almacen) && !empty($privilegio)){
					$material=$this->M_almacen_material->get_search($ida,"am.idam",$idam);
					if(!empty($material)){
						$nom=$_POST['nom'];
						$can=$_POST['can'];
						$uni=$_POST['uni'];
						$atrib="";$val="";$type_search="";
						if($nom!=""){
							$atrib="nombre";$val=$nom;$type_search="like";
						}else{
							if($can!=""){
								$atrib="cantidad";$val=$can;$type_search="like";
							}else{
								if($uni!=""){
									$atrib="idu";$val=$uni;$type_search="equals";
								}
							}
						}
						$listado['material']=$material[0];
						$listado['privilegio']=$privilegio[0];
						$listado['item']=$item;
						$listado['atrib']=$atrib;
						$listado['val']=$val;
						$listado['type_search']=$type_search;
						if($type=="ingreso"){
							$this->load->view('material/ingresos/tr_material',$listado);
						}
						if($type=="salida"){
							$listado['empleados']=$this->M_empleado->get_empleado("","",false,1);
							$this->load->view('material/salidas/tr_material',$listado);
						}
					}
				}else{
					echo "failll";
				}
			}else{
				echo "faill";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

   	/*--- End Nuevo ---*/
/*------- END MANEJO DE INGRESO DE MATERIAL -------*/
/*------- SALIDA DE MATERIAL -------*/
	public function search_salida(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"salida");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al13r==1){
					$listado['grupo']=$this->M_material_grupo->get_all();
					$listado['unidad']=$this->M_unidad->get_all();
					$listado['color']=$this->M_color->get_all();
					$this->load->view('material/salidas/search',$listado);
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_salida($ida){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"salida");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al13r==1){
					$atrib="";$val="";$type_search="";
					if(isset($_POST['nom']) && isset($_POST['can']) && isset($_POST['uni'])){
						$nom=$_POST['nom'];
						$can=$_POST['can'];
						$uni=$_POST['uni'];
						if($nom!=""){
							$atrib="nombre";$val=$nom;$type_search="like";
						}else{
							if($can!=""){
								$atrib="cantidad";$val=$can;$type_search="like";
							}else{
								if($uni!=""){
									$atrib="idu";$val=$uni;$type_search="equals";
								}
							}
						}
					}
					$listado['materiales']=$this->M_almacen_material->get_search($ida,"","");
					$listado['empleados']=$this->M_empleado->get_empleado("","",false,1);
					$listado["privilegio"]=$privilegio[0];
					$listado['atrib']=$atrib;
					$listado['val']=$val;
					$listado['type_search']=$type_search;
					$this->load->view('material/salidas/view',$listado);
				}else{
					$this->load->view('estructura/locked');
				}
			}else{
				$this->load->view('estructura/locked');
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END SALIDA DE MATERIAL -------*/
/*------- MANEJO DE CONFIGURACIONES -------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1){
					$listado['grupos']=$this->M_material_grupo->get_all();
					$listado['unidades']=$this->M_unidad->get_all();
					$listado['colores']=$this->M_color->get_all();
					$listado['privilegio']=$privilegio[0];
					$this->load->view('material/configuracion/view',$listado);
				}else{
					$this->load->view('estructura/locked');
				}
			}else{
				$this->load->view('estructura/locked');
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Manejo de unidad ---*/
	public function save_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15c==1){
					if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
						$nom=$_POST['nom'];
						$abr=$_POST['abr'];
						$equ=$_POST['equ'];
						$des_equ=$_POST['des'];
						if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,8) && $this->val->textarea($des_equ,0,100)){
							if($this->M_unidad->insertar($nom,$abr,$equ,$des_equ)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function update_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15u==1){
					if(isset($_POST['idu']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['equ']) && isset($_POST['des'])){
						$idu=$_POST['idu'];
						$nom=$_POST['nom'];
						$abr=$_POST['abr'];
						$equ=$_POST['equ'];
						$des_equ=$_POST['des'];
						if($this->val->strSpace($nom,2,40) && $this->val->strSpace($abr,1,10) && $this->val->textarea($des_equ,0,100)){
							if($this->M_unidad->modificar($idu,$nom,$abr,$equ,$des_equ)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function detalle_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1){
					if(isset($_POST['u'])){
						$idu=trim($_POST['u']);
						$unidad=$this->M_unidad->get($idu);
						if(!empty($unidad)){
							$control=$this->M_material_item->get_row('idu',$idu);
							echo "<strong>Nombre: </strong>".$unidad[0]->nombre."<br>";
							echo "<strong>Abreviatura </strong>".$unidad[0]->abr."<br>";
							echo "<strong>Equivalencia: </strong>".$unidad[0]->equ."<br>";
							echo "<strong>Descripción equivalencia: </strong>".$unidad[0]->descripcion_equ."<br>";
							if(!empty($control)){
								echo "<span class='text-danger'>No tiene la opcion de eliminar, pues la unidad de medida <strong>está siendo usado por ".count($control)." material(es)</strong></span><br>";
							}

						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirm_unidad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['u'])){
						$idu=$_POST['u'];
						$unidad=$this->M_unidad->get($idu);
						if(!empty($unidad)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_material_item->get_row('idu',$idu);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar la unidad de medida esta siendo usada";
								$listado['btn_ok']="none";
							}else{
								$listado['titulo']="eliminar la unidad de medida <strong>".$unidad[0]->nombre."</strong>?";
								$listado['desc']="Se eliminara definitivamente la unidad de medida.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_unidad(){
		if (!empty($this->session_id)){
			if(isset($_POST['u'])){
				$idu=trim($_POST['u']);
				$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
				if(!empty($privilegio)){
					if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
						$control=$this->M_material_item->get_row('idu',$idu);
						if(empty($control)){
							if($this->M_unidad->eliminar($idu)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}	
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "fail";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Manejo de unidad ---*/
	/*--- Manejo de color ---*/
	public function detalle_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1){
					if(isset($_POST['c'])){
						$idco=trim($_POST['c']);
						$color=$this->M_color->get($idco);
						if(!empty($color)){
							$control=$this->M_material->get_row('idco',$idco);
							echo "<strong>Nombre: </strong>".$color[0]->nombre."<br>";
							echo "<strong>Abreviatura </strong>".$color[0]->abr."<br>";
							if(!empty($control)){
								echo "<span class='text-danger'>No tiene la opcion de eliminar, pues el color <strong>está siendo usado por ".count($control)." material(es)</strong></span><br>";
							}

						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15c==1){
					if(isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['abr'])){
						$nom=trim($_POST['nom']);
						$cod=trim($_POST['cod']);
						$abr=trim($_POST['abr']);
						if($this->val->strSpace($nom,2,50) && $cod!="" && $this->val->strNoSpace($abr,0,10)){
							if($this->M_color->insertar($nom,$cod,$abr)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15u==1){
					if(isset($_POST['c']) && isset($_POST['nom']) && isset($_POST['cod']) && isset($_POST['abr'])){
						$idco=trim($_POST['c']);
						$nom=trim($_POST['nom']);
						$cod=trim($_POST['cod']);
						$abr=trim($_POST['abr']);
						$control=$this->M_color->get($idco);
						if(!empty($control)){
							if($this->val->strSpace($nom,2,50) && $cod!="" && $this->val->strNoSpace($abr,0,10)){
								if($this->M_color->modificar($idco,$nom,$cod,$abr)){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirm_color(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['c'])){
						$idco=trim($_POST['c']);
						$color=$this->M_color->get($idco);
						if(!empty($color)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_material->get_row('idco',$idco);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar, el color esta siendo usada por ".count($control)." material(es)";
								$listado['btn_ok']="none";
							}else{
								$listado['titulo']="eliminar el color <strong>".$color[0]->nombre."</strong>?";
								$listado['desc']="El color sera eliminado definitivamente.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_color(){
		if (!empty($this->session_id)) {
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['c'])){
						$control=$this->M_material->get_row('idco',$_POST['c']);
						if(empty($control)){
							if($this->M_color->eliminar($_POST['c'])){
								echo "ok";
							}else{
								echo "error";
							}	
						}else{
							echo "fail";
						}				
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Manejo de color ---*/
	/*--- Manejo de grupo ---*/
	public function detalle_grupo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1){
					if(isset($_POST['g'])){
						$idmg=trim($_POST['g']);
						$grupo=$this->M_material_grupo->get($idmg);
						if(!empty($grupo)){
							$control=$this->M_material->get_row('idmg',$idmg);
							echo "<strong>Nombre: </strong>".$grupo[0]->nombre."<br>";
							echo "<strong>Abreviatura: </strong>".$grupo[0]->abr."<br>";
							echo "<strong>Observaciónes: </strong>".$grupo[0]->descripcion."<br>";
							if(!empty($control)){
								echo "<span class='text-danger'>No tiene la opcion de eliminar, pues el grupo <strong>está siendo usado por ".count($control)." material(es)</strong></span><br>";
							}

						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_grupo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15c==1){
					if(isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['des'])){
						$nom=trim($_POST['nom']);
						$abr=trim($_POST['abr']);
						$des=trim($_POST['des']);
						if($this->val->strSpace($nom,2,50) && $this->val->strNoSpace($abr,0,10) && $this->val->textarea($des,0,100)){
							if($this->M_material_grupo->insertar($nom,$abr,$des)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}			
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}

	public function update_grupo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15u==1){
					if(isset($_POST['g']) && isset($_POST['nom']) && isset($_POST['abr']) && isset($_POST['des'])){
						$idmg=trim($_POST['g']);
						$nom=trim($_POST['nom']);
						$abr=trim($_POST['abr']);
						$des=trim($_POST['des']);
						$control=$this->M_material_grupo->get($idmg);
						if(!empty($control) && $this->val->strSpace($nom,2,50) && $this->val->strNoSpace($abr,0,10) && $this->val->textarea($des,0,100)){
							if($this->M_material_grupo->modificar($idmg,$nom,$abr,$des)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function confirm_grupo(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['g'])){
						$idmg=$_POST['g'];
						$grupo=$this->M_material_grupo->get($idmg);
						if(!empty($grupo)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_material->get_row('idmg',$idmg);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar, el grupo esta siendo usado por ".count($control)." material(es)";	
								$listado['btn_ok']="none";
							}else{
								$listado['titulo']="eliminar el grupo <strong>".$grupo[0]->nombre."</strong>?";
								$listado['desc']="El grupo sera eliminado definitivamente.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_grupo(){
		if (!empty($this->session_id)) {
			$privilegio=$this->M_privilegio->get_almacen($this->session->userdata("id"),"config_material");
			if(!empty($privilegio)){
				if($privilegio[0]->al==1 && $privilegio[0]->al15r==1 && $privilegio[0]->al15d==1){
					if(isset($_POST['g'])){
						$idmg=trim($_POST['g']);
						$listado['control']="";
						$control=$this->M_material->get_row('idmg',$idmg);
						if(empty($control)){
							if($this->M_material_grupo->eliminar($idmg)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Manejo de grupo ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END ALERTA ---------*/
}
/* End of file material.php */
/* Location: ./application/controllers/material.php */