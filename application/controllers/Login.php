<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		if($this->session->userdata("id")!=""){
			$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
		}
	}
	public function index(){//validamos usuario
		if(!empty($this->session_id) && empty($this->actualizaciones)){
			if($this->session_tipo=="1" || $this->session_tipo=="2"){
				$this->val->redireccion($this->M_privilegio->get_row("idus",$this->session->userdata("id")));
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					$this->logout();
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				redirect(base_url().'login/input',301);
			}
		}
	}
	public function input(){
		$variables['exist']="";
		if(empty($this->session_id)){
			$this->load->view('v_login');
			//echo "sess ".$this->session->userdata('login');
		}else{
			if(empty($this->actualizaciones)){
				if($this->session_tipo=="1" || $this->session_tipo=="2"){
					if(!empty($actualizaciones)){ redirect(base_url());}
					$this->val->redireccion($this->M_privilegio->get_row("idus",$this->session->userdata("id")));
				}else{
					if($this->session_tipo=="0"){
						redirect(base_url().'productos');
					}else{
						$this->logout();
					}
				}
			}else{
				redirect(base_url().'update');
			}
		}
	}

	public function logout(){
		$username=$this->session->userdata('login');
		$this->session->unset_userdata(array('login'=>'','id'=>'','nombre'=>'','paterno'=>'','cargo'=>'','fotografia'=>''));
		$this->session->sess_destroy('control_session');
		$this->load->library('session');
		$username=$this->lib->encriptar_str($username);
		redirect(base_url().'login/logout_session_user/?action='.$username,true,301);
	}
	public function logout_session_user(){
		if(isset($_GET['action'])){
			$listado['username']=$_GET['action'];
			$this->load->view('login/logout_session_user',$listado);
		}
	}
	public function form_cambiar_password(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus'])){
				$idus=$_POST['idus'];
				$empleado=$this->M_usuario->get_row_complet("u.idus",$idus);
				if(!empty($empleado)){
					$listado['empleado']=$empleado[0];
					$this->load->view('estructura/change_login',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function cambiar_password(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus']) && isset($_POST['pass']) && isset($_POST['nuevo']) && isset($_POST['nuevo2'])){
				$idus=$_POST['idus'];
				$pass=$_POST['pass'];
				$nuevo=$_POST['nuevo'];
				$nuevo2=$_POST['nuevo2'];
				if($this->val->password($nuevo,4,25)){
					if($nuevo==$nuevo2){
						$usuario=$this->M_usuario->get($idus);
						if(!empty($usuario)){
							if($usuario[0]->usuario==$this->session->userdata("login")){
								$control=$this->M_usuario->validate($this->session->userdata("login"),$pass);
								if(!empty($control)){
									if($this->M_usuario->modificar_password($idus,$nuevo)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function locked(){
		if(!empty($this->session_id)){
			$this->load->view('estructura/locked');
		}else{
			echo "logout";
		}
	}
	public function error(){
		if(!empty($this->session_id)){
			$this->load->view('estructura/404');
		}else{
			echo "logout";
		}
	}
	public function error_404(){
			$this->load->view('estructura/404');
	}
	public function user_session(){
		if(empty($this->actualizaciones)){
			if($this->session->userdata('login')!=""){
				$user=$this->M_usuario->get_search("u.usuario",$this->session->userdata('login'));
				if(!empty($user)){
					$listado["usuario"]=$user[0];
					if(isset($_POST['alert'])){$listado['alert']=$_POST['alert'];}
					//echo isset($_POST['alert']);
					$this->load->view('login/form_password',$listado);
				}else{
					$this->session->unset_userdata(array('login'=>'','id'=>'','nombre'=>'','paterno'=>'','cargo'=>'','fotografia'=>''));
					$this->load->view('login/form_user');
					//echo "nO EXISTE USARIO";
				}
			}else{
				//echo "VARIABLE NO INICIADA USARIO";
				$this->load->view('login/form_user');
			}
		}else{
			redirect(base_url().'update');
		}
	}
	public function closed_user(){
		if(empty($this->actualizaciones)){
			if($this->session->userdata('login')!=""){
				$this->session->unset_userdata(array('login'=>'','id'=>'','nombre'=>'','paterno'=>'','cargo'=>'','fotografia'=>''));
				$this->session->sess_destroy('control_session');
				echo "ok";
			}
		}else{
			echo "update";
		}
	}
	public function validate_user(){
		if(empty($this->actualizaciones)){
			if($this->session->userdata('login')==""){
				if(isset($_POST['username'])){
					$username=trim($_POST['username']);
					if($this->val->password($username,2,15)){
						if($this->M_usuario->get_row("usuario",$username)){
							$this->session->set_userdata('login',$username);
							echo "ok";
						}else{
							echo "user_no_exist";
						}
					}else{
						echo "user_invalid";
					}
				}else{
					echo "Error";
				}
			}else{
				echo "Error";
			}
		}else{
			echo "Error";
		}
	}
	public function adfgrrgb(){
		$variables['exist']="";
		if(empty($this->session_id)){
			date_default_timezone_set("America/La_Paz");
			$this->load->library('form_validation');
			if($this->input->post()){
				//$this->form_validation->set_rules('username','Nombre de usuario','required');
				$this->form_validation->set_rules('password','Password de usuario','required');
				if($this->form_validation->run() == true){
					if($this->session->userdata('login')!=""){
						$user=$this->M_usuario->validate($this->session->userdata('login'),$this->input->post('password'));
						if(!empty($user)){
							$persona=$this->M_persona->get($user[0]->ci);
							if(!empty($persona)){
								$this->session->set_userdata('control_session');
								$this->session->set_userdata('login',$this->session->userdata('login'));
								$this->session->set_userdata('id',$user[0]->idus,true);
								$this->session->set_userdata('tipo',$user[0]->tipo,true);
								$this->session->set_userdata('nombre',$persona[0]->nombre,true);
								$this->session->set_userdata('nombre2',$persona[0]->nombre2,true);
								$this->session->set_userdata('paterno',$persona[0]->paterno,true);
								$this->session->set_userdata('cargo',$persona[0]->cargo,true);
								$this->session->set_userdata('fotografia',$persona[0]->fotografia,true);
								//if($this->M_usuario->modificar_row($user[0]->idus,"fecha_ingreso",date("Y-m-d H:i:s"))){}
								if($user[0]->tipo=="1" || $user[0]->tipo=="2"){
									if(empty($this->actualizaciones)){
										$this->val->redireccion($this->M_privilegio->get_row("idus",$user[0]->idus));
									}else{
										redirect(base_url().'update');
									}
								}else{
									if($user[0]->tipo=="0"){
										redirect(base_url().'productos');
									}else{
										$this->logout();
									}
								}
							}else{
								$variables['exist']='invalid';
								$this->load->view('v_login',$variables);							
							}
						}else{
							$variables['exist']='invalid';
							$this->load->view('v_login',$variables);
						}
					}else{
						$this->session->unset_userdata(array('login'=>'','id'=>'','nombre'=>'','paterno'=>'','cargo'=>'','fotografia'=>''));
						$this->session->sess_destroy('control_session');
						redirect(base_url().'login/input');
					}
				}else{
					$variables['exist']='invalid';
					$this->load->view('v_login',$variables);
				}
			}else{
				$this->load->view('v_login',$variables);
			}
		}else{
			if(empty($this->actualizaciones)){
				if($this->session_tipo=="1" || $this->session_tipo=="2"){
					if(!empty($actualizaciones)){ redirect(base_url());}
					$this->val->redireccion($this->M_privilegio->get_row("idus",$this->session->userdata("id")));
				}else{
					if($this->session_tipo=="0"){
						redirect(base_url().'productos');
					}else{
						$this->logout();
					}
				}
			}else{
				redirect(base_url().'update');
			}
		}







	
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */