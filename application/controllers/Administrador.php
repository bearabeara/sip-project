<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Administrador extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!isset($_GET['p'])){
				$listado['pestania']=1;
			}else{
				$listado['pestania']=$_GET['p'];
			}
			$listado['privilegio']=$privilegio;
			$listado['ci']=$this->session->userdata("ci");
			date_default_timezone_set("America/La_Paz");
			if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
			$this->load->view('v_administrador',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE USUARIOS -------*/
	public function search_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
			if(!empty($privilegio)){
				$listado['privilegio']=$privilegio[0];
				if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1"){
					$this->load->view("administrador/usuario/search",$listado);
				}
			}else{

			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
			if(!empty($privilegio)){
				if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1"){
					$atrib="";
					$val="";
					if(isset($_POST['ci']) && isset($_POST['nom']) && isset($_POST['tel'])  && isset($_POST['car']) && isset($_POST['usu'])){//si tenemos busqueda 
						$ci=$_POST['ci'];
						$nom=$_POST['nom'];
						$tel=$_POST['tel'];
						$car=$_POST['car'];
						$usu=$_POST['usu'];
						if($ci!=""){
							$atrib="p.ci";$val=$ci;
						}else{
							if($nom!=""){
								$atrib="nombre";$val=$nom;
							}else{
								if($tel!=""){
									$atrib="p.telefono";$val=$tel;
								}else{
									if($car!=""){
										$atrib="p.cargo";$val=$car;
									}else{
										if($usu!=""){
											$atrib="u.usuario";$val=$usu;
										}
									}
								}
							}
						}
					}
					$listado['usuarios']=$this->M_usuario->search_usuarios($atrib,$val,false);
					$listado['privilegio']=$privilegio[0];
					$this->load->view("administrador/usuario/view",$listado);
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
	public function new_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['ciudades']=$this->M_ciudad->get_all();
			$this->load->view("administrador/usuario/3-nuevo/view",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function search_ci(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci'])){
				$ci=$_POST['ci'];
				$p=$this->M_persona->get($ci);
					if(!empty($p)){
						$ciudad=$this->M_ciudad->get($p[0]->idci);
						if(!empty($ciudad)){
							$nom1=$p[0]->nombre;
							$nom2=$p[0]->nombre2;
							$pat=$p[0]->paterno;
							$mat=$p[0]->materno;
							$car=$p[0]->cargo;
							$tel=$p[0]->telefono;
							$ciudades=$this->M_ciudad->get_all();
							$option_ciudad="<option value=''>Seleccionar...</option>";
							for($ci=0;$ci<count($ciudades);$ci++){ 
								$selected="";
								if($ciudades[$ci]->idci==$ciudad[0]->idci){ $selected="selected";}
								$option_ciudad.="<option value='".$ciudades[$ci]->idci."' ".$selected.">".$ciudades[$ci]->nombre."(".$ciudades[$ci]->abreviatura.")</option>";
							}
							
							echo $option_ciudad."|".$nom1."|".$nom2."|".$pat."|".$mat."|".$car."|".$tel;							
						}
					}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['tel']) && isset($_POST['usu']) && isset($_POST['car']) && isset($_POST['tip']) && isset($_FILES)){
				$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
				if(!empty($privilegio)){
					if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1" && $privilegio[0]->ad1c=="1"){
						$ci=trim($_POST['ci']);
						$ciu=trim($_POST['ciu']);
						$nom1=trim($_POST['nom1']);
						$nom2=trim($_POST['nom2']);
						$pat=trim($_POST['pat']);
						$mat=trim($_POST['mat']);
						$tel=trim($_POST['tel']);
						$usu=trim($_POST['usu']);
						$car=trim($_POST['car']);
						$tip=trim($_POST['tip']);
						if($this->val->entero($ci,6,9) && $ci>100000 && $ci<=999999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->entero($tel,0,15) && $this->val->strNoSpace($usu,4,15) && ($tip=="0" || $tip=="1" || $tip=="2")){
							$control=true;
							if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
							if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
							if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
							if($pat!=""){ if(!$this->val->strSpace($pat,2,20)){ $control=false;}}
							if($control){
								$control_usuario=$this->M_usuario->get_row('usuario',$usu);
								if(empty($control_usuario)){
									$persona=$this->M_persona->get_row('ci',$ci);
									if(empty($persona)){
										$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$ci);
										if($img!='error' && $img!="error_type" && $img!="error_size_img"){
											if($this->M_persona->insertar($ci,$ciu,$nom1,$nom2,$pat,$mat,$car,$tel,"",$img,"")){
												$idus=$this->M_usuario->max_col("idus")+1;
												if($this->M_usuario->insertar($idus,$ci,$usu,$tip)){
													if($this->M_privilegio->insertar($idus)){
														echo "ok";
													}else{
														if($this->M_persona->eliminar($ci)){}
														echo "error";
													}
												}else{
													if($this->M_persona->eliminar($ci)){}
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo $img;
										}
									}else{
										$user=$this->M_usuario->get_row('ci',$ci);
										if(empty($user)){
											$idus=$this->M_usuario->max_col("idus")+1;
											if($this->M_usuario->insertar($idus,$ci,$usu,$tip)){
												if($this->M_privilegio->insertar($idus)){
													echo "ok";
												}else{
													if($this->M_usuario->eliminar($idus)){}
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "user_registrer";
										}
									}
								}else{
									echo "name_user_exist";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";	
					}
				}else{
					echo "permiso_bloqueado";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_usuarios(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
			if(!empty($privilegio)){
				if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1" && $privilegio[0]->ad1p=="1"){
					if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['ci']) && isset($_POST['nom']) && isset($_POST['tel'])  && isset($_POST['car']) && isset($_POST['usu'])){//si tenemos busqueda 						
						$listado['usuarios']=$this->M_usuario->search_usuarios("","",false);
						$listado['visibles']=json_decode($_POST['visibles']);
						$listado['privilegio']=$privilegio[0];
						$listado['tbl']=$_POST['tbl'];
						$listado['ci']=trim($_POST['ci']);
						$listado['nom']=trim($_POST['nom']);
						$listado['tel']=trim($_POST['tel']);
						$listado['car']=trim($_POST['car']);
						$listado['usu']=trim($_POST['usu']);
						$this->load->view('administrador/usuario/4-imprimir/print_usuarios',$listado);
					}else{
						echo "fail";
					}
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_usuarios(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
			if(isset($_GET['file']) && isset($_GET['ci']) && isset($_GET['nom']) && isset($_GET['tel']) && isset($_GET['car']) && isset($_GET['usu'])){
				$atrib="";
				$val="";
				$ci=$_GET['ci'];
				$nom=$_GET['nom'];
				$tel=$_GET['tel'];
				$car=$_GET['car'];
				$usu=$_GET['usu'];
				if($ci!=""){
					$atrib="p.ci";$val=$ci;
				}else{
					if($nom!=""){
						$atrib="nombre";$val=$nom;
					}else{
						if($tel!=""){
							$atrib="p.telefono";$val=$tel;
						}else{
							if($car!=""){
								$atrib="p.cargo";$val=$car;
							}else{
								if($usu!=""){
									$atrib="u.usuario";$val=$usu;
								}
							}
						}
					}
				}
				$listado['usuarios']=$this->M_usuario->search_usuarios($atrib,$val,false);
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('administrador/usuario/4-imprimir/export_usuarios',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
	public function detalle_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus'])){
				$idus=$_POST['idus'];
				$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$this->load->view("administrador/usuario/5-reporte/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
	public function config_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus'])){
				$idus=$_POST['idus'];
				$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$listado['ciudades']=$this->M_ciudad->get_all();
					$this->load->view("administrador/usuario/6-config/view",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus']) && isset($_POST['ci']) && isset($_POST['ciu']) && isset($_POST['nom1']) && isset($_POST['nom2']) && isset($_POST['pat']) && isset($_POST['mat']) && isset($_POST['tel']) && isset($_POST['usu']) && isset($_POST['car']) && isset($_POST['tip']) && isset($_FILES)){
				$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
				if(!empty($privilegio)){
					if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1" && $privilegio[0]->ad1u=="1"){
						$idus=trim($_POST['idus']);
						$ci=trim($_POST['ci']);
						$ciu=trim($_POST['ciu']);
						$nom1=trim($_POST['nom1']);
						$nom2=trim($_POST['nom2']);
						$pat=trim($_POST['pat']);
						$mat=trim($_POST['mat']);
						$tel=trim($_POST['tel']);
						$usu=trim($_POST['usu']);
						$car=trim($_POST['car']);
						$tip=trim($_POST['tip']);
						if($this->val->entero($idus,0,10) && $this->val->entero($ci,6,9) && $ci>100000 && $ci<=999999999 && $this->val->entero($ciu,0,10) && $this->val->strSpace($nom1,2,20) && $this->val->entero($tel,0,15) && $this->val->password($usu,4,15) && ($tip=="0" || $tip=="1" || $tip=="2")){
							$control=true;
							if($nom2!=""){ if(!$this->val->strSpace($nom2,2,20)){ $control=false;}}
							if($mat!=""){ if(!$this->val->strSpace($mat,2,20)){ $control=false;}}
							if($car!=""){ if(!$this->val->strSpace($car,0,100)){ $control=false;}}
							if($pat!=""){ if(!$this->val->strSpace($pat,2,20)){ $control=false;}}
							if($control){
								$usuario=$this->M_usuario->get_row('idus',$idus);
								if(!empty($usuario)){
									$usuario=$usuario[0];
									$ci_org=$usuario->ci;
									$control_usuario=$this->M_usuario->get_row('usuario',$usu);
									if(empty($control_usuario)){
										$control=true;
									}else{
										if($usuario->idus==$control_usuario[0]->idus){
											$control=true;
										}else{
											$control=false;
										}
									}
									if($control){
										$control=$this->M_persona->get($ci);
										$guardar="ok";
										if(!empty($control)){
											if($ci==$ci_org){
												$guardar="ok";
											}else{
												$guardar="ci_exist";
											}
										}
										if($guardar=="ok"){
											$persona=$this->M_persona->get($ci_org);
											$img=$persona[0]->fotografia;
											$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$ci);//cambiar_imagen_miniatura($FILES,$ruta,$pos,$resize,$origen,$id)
											if($img!='error' && $img!="error_type" && $img!="error_size_img"){
												if($this->M_persona->modificar_usuario($ci_org,$ci,$ciu,$nom1,$nom2,$pat,$mat,$car,$tel,$img)){
													if($this->M_usuario->modificar($idus,$ci,$usu,$tip)){
														echo "ok";
													}else{
														echo "error";
													}
												}else{
													echo "error";
												}
											}else{
												echo $img;
											}
										}else{
											echo $guardar;
										}
									}else{
										echo "name_user_exist";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "permiso_bloqueado";	
					}
				}else{
					echo "permiso_bloqueado";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*reset pass*/
	public function reset_pass(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
			if(!empty($privilegio)){
				if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1" && $privilegio[0]->ad1u=="1"){
					if(isset($_POST['idus'])){
						$usuario=$this->M_usuario->get_persona($_POST['idus']);
						if(!empty($usuario)){
							if($this->M_usuario->modificar_password($_POST['idus'],$usuario[0]->ci)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_usuario(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			if(isset($_POST['idus'])){
				$idus=$_POST['idus'];
				$url="./libraries/img/personas/miniatura/";
				$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
				if(!empty($usuario)){
					$listado['titulo']="eliminar a <strong>".$usuario[0]->nombre." ".$usuario[0]->nombre2." ".$usuario[0]->paterno." ".$usuario[0]->materno."</strong> como usuario del sistema";
					$listado['desc']="Se eliminaran definitivamente el usuario y no podra ingresar mas al sistema.";
					$img='default.png';
					if($usuario[0]->fotografia!=NULL && $usuario[0]->fotografia!=""){ $img=$usuario[0]->fotografia; }
					$listado['img']=$url.$img;
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function drop_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idus']) && isset($_POST['u']) && isset($_POST['p'])){
				$idus=$_POST['idus'];
				$u=$_POST['u'];
				$p=$_POST['p'];
				$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"usuario");
				if(!empty($privilegio)){
					if($privilegio[0]->ad=="1" && $privilegio[0]->ad1r=="1" && $privilegio[0]->ad1d=="1"){
						if(strtolower($u)==strtolower($this->session->userdata("login"))){
							$usuario=$this->M_usuario->validate($u,$p);
							if(!empty($usuario)){
								$usuario=$this->M_usuario->search_usuarios('u.idus',$idus,true);
								if(!empty($usuario)){
									$empleado=$this->M_empleado->get_row('ci',$usuario[0]->ci);
									$directivo=$this->M_directivo->get_row('ci',$usuario[0]->ci);
									if(empty($empleado) && empty($directivo)){//eliminamos persona
										if($this->lib->eliminar_imagen($usuario[0]->fotografia,'./libraries/img/personas/')){
											if($this->M_persona->eliminar($usuario[0]->ci)){
												echo "ok";
											}else{
												echo "error";
											}
										}else{
											echo "error";
										}
									}else{
										if($this->M_usuario->eliminar($usuario[0]->idus)){
											echo "ok";
										}else{
											echo "error";
										}
									}
								}else{
									echo "fail";
								}

							}else{
								echo "validate";
							}
						}else{
							echo "validate";
						}
					}else{
						echo "permiso_bloqueado";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE USUARIOS -------*/
/*------- MANEJO DE PRIVILEGIOS -------*/
	public function search_privilegio(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"privilegio");
			if($privilegio[0]->ad=="1" && $privilegio[0]->ad2r=="1"){
				$this->load->view("administrador/privilegio/search");
			}
		}else{
			redirect(base_url().'login',301);
		}
	}
	public function view_privilegio(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"privilegio");
			if($privilegio[0]->ad=="1" && $privilegio[0]->ad2r=="1"){
				$atrib="";
				$val="";
				if(isset($_POST['ci']) && isset($_POST['nom'])){//si tenemos busqueda 
					$ci=$_POST['ci'];
					$nom=$_POST['nom'];
					if($ci!=""){
						$atrib="p.ci";$val=$ci;
					}else{
						if($nom!=""){
							$atrib="nombre";$val=$nom;
						}
					}
				}
				$listado['usuarios']=$this->M_usuario->search_usuarios($atrib,$val,true);
				$this->load->view("administrador/privilegio/view",$listado);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Nuevo ---*/
	public function update_privilegio(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"privilegio");
			if($privilegio[0]->ad2r=="1" && $privilegio[0]->ad2u=="1"){
				if(isset($_POST['idpri']) && isset($_POST['col'])){
					$idpri=trim($_POST['idpri']);
					$col=trim($_POST['col']);
					if($this->val->entero($idpri,0,10) && $this->val->strNoSpace($col,2,6)){
						$control=$this->M_privilegio->get_row_2n('idpri',$idpri,$col,"1");
						if(empty($control)){
							if($this->M_privilegio->modificar($idpri,$col,"1")){
								echo "1|ok";
							}else{
								echo "|error";
							}
						}else{
							if($this->M_privilegio->modificar($idpri,$col,"0")){
								echo "0|ok";
							}else{
								echo "|error";
							}
						}
					}else{
						echo "|fail";
					}
				}else{
					echo "|fail";
				}
			}else{
				echo "|Bloquedo";
			}
		}else{
			echo "|logout";
		}
	}
	public function detail_modal(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"privilegio");
			if($privilegio[0]->ad2r=="1" && $privilegio[0]->ad2u=="1"){
				if(isset($_POST['idpri']) && isset($_POST['col'])){
					$idpri=$_POST['idpri'];
					$col=$_POST['col'];
					if($this->val->entero($idpri,0,10) && $this->val->strNoSpace($col,2,6)){
						$privilegios=$this->M_privilegio->get($idpri);
						if(!empty($privilegios)){
							$listado['privilegio']=$privilegios[0];
							$listado['col']=$col;
							$this->load->view("administrador/privilegio/detalle/view",$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "bloqueado";
			}
		}else{
			redirect(base_url().'login',301);
		}
	}
   	/*--- End Nuevo ---*/
/*------- END MANEJO DE PRIVILEGIOS -------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['paises']=$this->M_pais->get_all();
			$listado['ciudades']=$this->M_ciudad->get_search("","");
			$listado['actualizaciones']=$this->M_actualizacion->get_search("","","idac","desc");
			$this->load->view("administrador/config/view",$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- Pais ---*/
   	public function save_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad3r==1 && $privilegio[0]->ad3c==1){
					if(isset($_POST['p'])){
						if($this->val->strSpace($_POST['p'],2,100)){
							if($this->M_pais->insertar($_POST['p'])){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad3r==1 && $privilegio[0]->ad3u==1){
					if(isset($_POST['idpa']) && isset($_POST['p'])){
						$pais=$this->M_pais->get($_POST['idpa']);
						if(!empty($pais)){
							if($this->val->strSpace($_POST['p'],2,100)){
								if($this->M_pais->modificar($_POST['idpa'],$_POST['p'])){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function alerta_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad==1 && $privilegio[0]->ad3r==1 && $privilegio[0]->ad3d==1){
					if(isset($_POST['idpa'])){
						$idpa=$_POST['idpa'];
						$pais=$this->M_pais->get($idpa);
						if(!empty($pais)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_ciudad->get_row('idpa',$idpa);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar, el pais esta siendo usada por varias ciudades.";	
							}else{
								$listado['titulo']="eliminar el pais <strong>".$pais[0]->nombre."</strong>?";
								$listado['desc']="El pais sera eliminado definitivamente.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad==1 && $privilegio[0]->ad3r==1 && $privilegio[0]->ad3d==1){
					if(isset($_POST['idpa'])){
						$idpa=$_POST['idpa'];
						$control=$this->M_ciudad->get_row('idpa',$idpa);
						if(empty($control)){
							$pais=$this->M_pais->get($_POST['idpa']);
							if(!empty($pais)){
								if($this->M_pais->eliminar($_POST['idpa'])){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Pais ---*/
   	/*--- Ciudad ---*/
   	public function save_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad3r==1 && $privilegio[0]->ad3c==1){
					if(isset($_POST['ciu']) && isset($_POST['sig']) && isset($_POST['pac'])){
						$ciu=$_POST['ciu'];
						$sig=$_POST['sig'];
						$idpa=$_POST['pac'];
						$pais=$this->M_pais->get($idpa);
						if($this->val->strSpace($ciu,2,100) && $this->val->strNoSpace($sig,1,4) && !empty($pais)){
							if($this->M_ciudad->insertar($idpa,$ciu,$sig)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}						
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad3r==1 && $privilegio[0]->ad3u==1){
					if(isset($_POST['idci']) && isset($_POST['ciu']) && isset($_POST['sig']) && isset($_POST['pac'])){
						$idci=$_POST['idci'];
						$ciu=$_POST['ciu'];
						$sig=$_POST['sig'];
						$idpa=$_POST['pac'];
						$ciudad=$this->M_ciudad->get($idci);
						$pais=$this->M_pais->get($idpa);
						if($this->val->strSpace($ciu,2,100) && $this->val->strNoSpace($sig,1,4) && !empty($pais) && !empty($ciudad)){
							if($this->M_ciudad->modificar($idci,$idpa,$ciu,$sig)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo "fail";
						}						
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function alerta_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad==1 && $privilegio[0]->ad3r==1 && $privilegio[0]->ad3d==1){
					if(isset($_POST['idci'])){
						$idci=$_POST['idci'];
						$ciudad=$this->M_ciudad->get($idci);
						if(!empty($ciudad)){
							$listado['open_control']="true";
							$listado['control']="";
							$control=$this->M_persona->get_row('idci',$idci);
							if(!empty($control)){
								$listado['titulo']="";
								$listado['desc']="Imposible eliminar, la ciudad a sido asignada a varias personas.";	
							}else{
								$listado['titulo']="eliminar la ciudad <strong>".$ciudad[0]->nombre."</strong>?";
								$listado['desc']="La ciudad sera eliminada definitivamente.";
							}
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_administrador($this->session->userdata("id"),"config");
			if(!empty($privilegio)){
				if($privilegio[0]->ad==1 && $privilegio[0]->ad3r==1 && $privilegio[0]->ad3d==1){
					if(isset($_POST['idci'])){
						$idci=$_POST['idci'];
						$control=$this->M_persona->get_row('idci',$idci);
						if(empty($control)){
							$ciudad=$this->M_ciudad->get($_POST['idci']);
							if(!empty($ciudad)){
								if($this->M_ciudad->eliminar($_POST['idci'])){
									echo "ok";
								}else{
									echo "error";
								}
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "permiso_bloqueado";	
				}
			}else{
				echo "permiso_bloqueado";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Ciudad ---*/
   	/*--- Actualizacion ---*/
	public function new_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$this->load->view("administrador/config/actualizacion/new");
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function new_config_usuario(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['content']) && isset($_POST['asignados'])){
					if($_POST['asignados']!="" && $_POST['asignados']!="[]"){
						$listado['asignados']=json_decode($_POST['asignados']);
					}else{
						$listado['asignados']=json_decode("{}");
					}
				$content=trim($_POST['content']);
				$listado['content']=$content;
				$listado['usuarios']=$this->M_usuario->search_usuarios("","",false);
				$this->load->view("administrador/config/actualizacion/usuarios",$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function add_user_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['u'])){
				$idu=trim($_POST['u']);
				$usuario=$this->M_usuario->search_usuarios("u.idus",$idu,false);
				if(!empty($usuario)){
					$listado['usuario']=$usuario[0];
					$this->load->view("administrador/config/actualizacion/row_usuario",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ini']) && isset($_POST['fin'])  && isset($_POST['des']) && isset($_POST['est'])  && isset($_POST['asignados'])){
				$fini=trim($_POST['ini']);
				$ffin=trim($_POST['fin']);
				$des=trim($_POST['des']);
				$est=trim($_POST['est']);
				$asignados=$_POST['asignados'];
				if($fini!="" && $fini!='undefined' && $ffin!="" && $ffin!='undefined' && $this->val->textarea($des,5,1000) && $asignados!="[]"  && $asignados!=""){
					$idac=$this->M_actualizacion->max_col('idac')+1;
					if($this->M_actualizacion->insertar($idac,$fini,$ffin,$des,$est)){
						$asignados=json_decode($asignados);
						foreach ($asignados as $key => $asignado){
							if($this->M_actualizacion_usuario->insertar($idac,$asignado)){}
						}
						$result=array();$result[] = array('result' => 'ok','tipo'=>'3');
						echo json_encode($result);
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function change_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ac'])){
				$idac=trim($_POST['ac']);
				$actualizacion=$this->M_actualizacion->get($idac);
				if(!empty($actualizacion)){
					$listado['actualizacion']=$actualizacion[0];
					$listado['asignados']=$this->M_actualizacion_usuario->get_usuario("a.idac",$idac,"nombre_completo","ASC");
					$this->load->view("administrador/config/actualizacion/update",$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ac']) && isset($_POST['ini']) && isset($_POST['fin'])  && isset($_POST['des']) && isset($_POST['est']) && isset($_POST['nuevos']) && isset($_POST['users_delete'])){
				$fini=trim($_POST['ini']);
				$ffin=trim($_POST['fin']);
				$des=trim($_POST['des']);
				$est=trim($_POST['est']);
				$idac=trim($_POST['ac']);
				$nuevos=$_POST['nuevos'];
				$users_delete=$_POST['users_delete'];
				if($fini!="" && $fini!='undefined' && $ffin!="" && $ffin!='undefined' && $this->val->textarea($des,5,1000) && $users_delete!=NULL){
					if($this->M_actualizacion->modificar($idac,$fini,$ffin,$des,$est)){
						if($nuevos!=NULL){
							$nuevos=json_decode($nuevos);
							foreach ($nuevos as $key => $nuevo) {
								if($this->M_actualizacion_usuario->insertar($idac,$nuevo)){}
							}
						}
						$users_delete=json_decode($users_delete);
						foreach($users_delete as $key => $user){
							if($user->status=="0"){
								if($this->M_actualizacion_usuario->eliminar($user->idau)){}
							}
						}
						$result=array();$result[] = array('result' => 'ok','tipo'=>'3');
						echo json_encode($result);
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function confirmar_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ac'])){
				$idac=$_POST['ac'];
				$actualizacion=$this->M_actualizacion->get($idac);
				if(!empty($actualizacion)){
					$listado['open_control']="false";
					$listado['control']="";
					$listado['titulo']="eliminar el registro de actualizacion";
					$listado['desc']="Se eliminara definitivamente el registro.";
					$this->load->view('estructura/form_eliminar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function drop_actualizacion(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['ac'])){
				$idac=$_POST['ac'];
				$actualizacion=$this->M_actualizacion->get($idac);
				if(!empty($actualizacion)){
					if($this->M_actualizacion->eliminar($idac)){
						$result=array();$result[] = array('result' => 'ok','tipo'=>'3');
						echo json_encode($result);
					}else{
						echo "error";
					}
				}else{
					echo "fail $idac";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End actualizacion ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END ALERTA ---------*/
}
/* End of file admin.php */
/* Location: ./application/controllers/admin.php */