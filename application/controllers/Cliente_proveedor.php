<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cliente_proveedor extends CI_Controller {
	private $session_id;
	private $session_tipo;
	private $actualizaciones;
	public function __construct(){
		parent::__construct();
		$this->session_id=$this->session->userdata('id');
		$this->session_tipo=$this->session->userdata('tipo');
		$this->actualizaciones=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","1","ac.idac","asc");
	}
	public function index(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if($privilegio[0]->cl==1){
				if(!isset($_GET['p'])){
					if($privilegio[0]->cl1r==1){
						$listado['pestania']=1;
					}else{
						if($privilegio[0]->cl2r==1){ 
							$listado['pestania']=2;
						}else{
							if($privilegio[0]->cl5r==1){ 
								$listado['pestania']=5;
							}else{
								$listado['pestania']="";
							}
						}
					}
				}else{
					$listado['pestania']=$_GET['p'];
				}
				$listado['privilegio']=$privilegio;
				date_default_timezone_set("America/La_Paz");
				if($this->M_usuario->modificar_row($this->session->userdata("id"),"fecha_ingreso",date('Y-m-d H:i:s'))){}
				$this->load->view('v_cliente_proveedor',$listado);
			}else{
				$this->val->redireccion($privilegio);
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				if($this->session_tipo=="0"){
					redirect(base_url().'productos');
				}else{
					redirect(base_url().'login/input',301);
				}
			}
		}
	}
/*------- MANEJO DE CLIENTES -------*/
	public function search_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['privilegio']=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
			$this->load->view('cliente_proveedor/cliente/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
			if(!empty($privilegio)){
				$atrib="";$val="";
				if(isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['resp'])){//si tenemos busqueda 
					$nit=$_POST['nit'];
					$nom=$_POST['nom'];
					$resp=$_POST['resp'];
					if($nit!=""){
						$atrib="nit";$val=$nit;
					}else{
						if($nom!=""){
							$atrib="razon";$val=$nom;
						}else{
							if($resp!=""){
								$atrib="gerente";$val=$resp;
							}
						}
					}
				}
				$listado['clientes']=$this->M_cliente->get_search($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('cliente_proveedor/cliente/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Buscador ---*/
   	/*--- End Buscador ---*/
   	/*--- Ver Todo ---*/
   	/*--- End Ver Todo ---*/
   	/*--- Nuevo ---*/
	public function new_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['paises']=$this->M_pais->get_all();
			$this->load->view('cliente_proveedor/cliente/3-nuevo/form',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['web']) && isset($_POST['obs'])){
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$web=trim($_POST['web']);
				$obs=trim($_POST['obs']);
				if($this->val->entero($nit,6,25) && $this->val->strSpace($raz,2,100)){
					$control=$this->M_cliente->get_row("nit",$nit);
					if(empty($control)){
						$control=true;
						if(!$this->val->strSpace($res,2,150)){ $control=false;}
						if(!$this->val->entero($tel,7,15)){ $control=false;}
						if(!$this->val->strNoSpace($web,2,150)){ $control=false;}
						if(!$this->val->textarea($obs,2,150)){ $control=false;}
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$nit);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_cliente->insertar($nit,$raz,$res,$tel,$web,$img,$obs)){
								echo "ok";
							}else{

								echo "error";
							}
						}else{
							echo $img;
						}

					}else{
						echo "ci_exist";//cliente ya registrado
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_clientes(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['res'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['clientes']=$this->M_cliente->get_search("","");
					$listado['all_sucursales']=$this->M_sucursal_cliente->get_all();
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=$_POST['tbl'];
					$listado['nit']=trim($_POST['nit']);
					$listado['nom']=trim($_POST['nom']);
					$listado['res']=trim($_POST['res']);
					$this->load->view('cliente_proveedor/cliente/4-imprimir/print_clientes',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_clientes(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(isset($_GET['file']) && isset($_GET['nit']) && isset($_GET['nom']) && isset($_GET['res'])){
				$atrib="";$val="";
				$nit=trim($_GET['nit']);
				$nom=trim($_GET['nom']);
				$res=trim($_GET['res']);
				if($nit!=""){
					$atrib="nit";$val=$nit;
				}else{
					if($nom!=""){
						$atrib="razon";$val=$nom;
					}else{
						if($res!=""){
							$atrib="gerente";$val=$res;
						}
					}
				}
			 	$listado['clientes']=$this->M_cliente->get_search($atrib,$val);
				$listado['all_sucursales']=$this->M_sucursal_cliente->get_all();
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('cliente_proveedor/cliente/4-imprimir/export_clientes',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detail_sucursal(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['sc'])){
				$idsc=$_POST['sc'];
				$sucursal=$this->M_sucursal_cliente->get($idsc);
				if(!empty($sucursal)){
					$cliente=$this->M_cliente->get($sucursal[0]->idcl);
					$listado['sucursal']=$sucursal[0];
					$listado['cliente']=$cliente[0];
					$this->load->view('cliente_proveedor/cliente/5-reportes/detalle_sucursal',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	public function detalle_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$idcl=$_POST['idcl'];
			$listado['cliente']=$this->M_cliente->get($idcl);
			$this->load->view('cliente_proveedor/cliente/5-reportes/detalle_cliente',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	public function form_update_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idcl'])){
				$idcl=$_POST['idcl'];
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($cliente)){
					$listado['cliente']=$cliente[0];
					$this->load->view('cliente_proveedor/cliente/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idcl']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['web']) && isset($_POST['obs'])){
				$idcl=trim($_POST['idcl']);
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$web=trim($_POST['web']);
				$obs=trim($_POST['obs']);
				$cliente=$this->M_cliente->get($idcl);
				if(!empty($cliente) && $this->val->entero($nit,6,25) && $this->val->strSpace($raz,2,100)){
					$guardar=true;
					if($cliente[0]->nit!=$nit){
						$control=$this->M_cliente->get_row("nit",$nit);
						if(!empty($control)){
							$guardar=false;
						}
					}
					if($guardar){
						$control=true;
						if(!$this->val->strSpace($res,2,150)){ $control=false;}
						if(!$this->val->entero($tel,7,15)){ $control=false;}
						if(!$this->val->strNoSpace($web,2,150)){ $control=false;}
						if(!$this->val->textarea($obs,2,150)){ $control=false;}
						$img=$cliente[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$idcl);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_cliente->modificar($idcl,$nit,$raz,$res,$tel,$web,$img,$obs)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "ci_exist";//cliente ya registrado
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	   	public function view_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['cl'])){
					$idcl=$_POST['cl'];
					$cliente=$this->M_cliente->get_row('idcl',$idcl);
					if(!empty($cliente)){
						$listado['sucursales']=$this->M_sucursal_cliente->get_search('idcl',$idcl);
						$listado['cliente']=$cliente[0];
						$this->load->view('cliente_proveedor/cliente/6-configuracion/sucursal/view',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	   	public function new_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['cl'])){
					$idcl=$_POST['cl'];
					$cliente=$this->M_cliente->get_row('idcl',$idcl);
					if(!empty($cliente)){
						$listado['cliente']=$cliente[0];
						$this->load->view('cliente_proveedor/cliente/6-configuracion/sucursal/new',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function save_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['cl']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['dir'])){
					$idcl=trim($_POST['cl']);
					$nit=trim($_POST['nit']);
					$raz=trim($_POST['raz']);
					$res=trim($_POST['res']);
					$tel=trim($_POST['tel']);
					$ema=trim($_POST['ema']);
					$dir=trim($_POST['dir']);
					$cliente=$this->M_cliente->get_row('idcl',$idcl);
					if(!empty($cliente)){
						if($this->val->strSpace($raz,2,100)){
							$control=$this->M_cliente->get_row("nit",$nit);
							if(empty($control)){
								$control=true;
								if(!$this->val->entero($nit,0,25)){ $control=false;}
								if(!$this->val->strSpace($res,2,150)){ $control=false;}
								if(!$this->val->entero($tel,7,15)){ $control=false;}
								if(!$this->val->email($ema)){ $control=false;}
								if(!$this->val->textarea($dir,5,200)){ $control=false;}
								$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,"suc");
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_sucursal_cliente->insertar($idcl,$nit,$raz,$res,$tel,$ema,$dir,$img)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
							}else{
								echo "ci_exist";//cliente ya registrado
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	   	public function change_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['sc'])){
					$idsc=$_POST['sc'];
					$sucursal=$this->M_sucursal_cliente->get($idsc);
					if(!empty($sucursal)){
						$cliente=$this->M_cliente->get_row('idcl',$sucursal[0]->idcl);
						$listado['cliente']=$cliente[0];
						$listado['sucursal']=$sucursal[0];
						$this->load->view('cliente_proveedor/cliente/6-configuracion/sucursal/change',$listado);
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
		public function update_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
				if(isset($_POST['sc']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['dir'])){
					$idsc=trim($_POST['sc']);
					$nit=trim($_POST['nit']);
					$raz=trim($_POST['raz']);
					$res=trim($_POST['res']);
					$tel=trim($_POST['tel']);
					$ema=trim($_POST['ema']);
					$dir=trim($_POST['dir']);
					$sucursal=$this->M_sucursal_cliente->get($idsc);
					if(!empty($sucursal)){
						$cliente=$this->M_cliente->get_row('idcl',$sucursal[0]->idcl);
						if($this->val->strSpace($raz,2,100)){
								$control=true;
								if(!$this->val->entero($nit,0,25)){ $control=false;}
								if(!$this->val->strSpace($res,2,150)){ $control=false;}
								if(!$this->val->entero($tel,7,15)){ $control=false;}
								if(!$this->val->email($ema)){ $control=false;}
								$img=$sucursal[0]->fotografia;
								$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,"suc-".$idsc);
								if($img!='error' && $img!="error_type" && $img!="error_size_img"){
									if($this->M_sucursal_cliente->modificar($idsc,$nit,$raz,$res,$tel,$ema,$dir,$img)){
										echo "ok";
									}else{
										echo "error";
									}
								}else{
									echo $img;
								}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
				}else{
					echo "fail";
				}
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
	   	public function confirmar_sucursal(){
	   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
	   			if(!empty($privilegio)){
	   				if($privilegio[0]->cl1r==1 && $privilegio[0]->cl1u==1){
	   					if(isset($_POST['sc'])){
							$idsc=$_POST['sc'];
							$sucursal=$this->M_sucursal_cliente->get($idsc);
							if(!empty($sucursal)){
								$url='./libraries/img/';
								$img="sistema/miniatura/default.jpg";
								if(!empty($sucursal[0]->fotografia)){ $img="personas/miniatura/".$sucursal[0]->fotografia; }
								$control=$this->M_sucursal_detalle_pedido->get_row('idsc',$idsc);
								if(empty($control)){
									$cliente=$this->M_cliente->get_row('idcl',$sucursal[0]->idcl);
									$listado['titulo']="eliminar la sucursal <strong>".$sucursal[0]->nombre."</strong> del cliente <strong>".$cliente[0]->razon."</strong>";
									$listado['desc']="Se eliminara definitivamente la sucursal del sistema";
								}else{
									$listado['desc']="Imposible eliminar la sucursal, la sucursal esta <strong>asignada a ".count($control)." producto(s) en pedidos</strong>";
									$listado['btn_ok']="none";
								}
								$listado['img']=$url.$img;
								$listado['control']="none";
								$listado['open_control']="none";
								$this->load->view('estructura/form_eliminar',$listado);
							}else{
								echo "fail";
							}
						}else{
							echo "fail";
						}
	   				}else{
	   					echo "permiso_bloqueado";
	   				}
	   			}else{
	   				echo "permiso_bloqueado";
	   			}
	   		}else{
	   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
	   		}
	   	}
	   	public function drop_sucursal(){
			if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
	   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
	   			if(!empty($privilegio)){
	   				if($privilegio[0]->cl1r==1 && $privilegio[0]->cl1u==1){
	   					if(isset($_POST['sc'])){
							$id=$_POST['sc'];
							$sucursal=$this->M_sucursal_cliente->get($id);
							if(!empty($sucursal)){
								$control=$this->M_sucursal_detalle_pedido->get_row('idsc',$id);
								if(empty($control)){
									if($this->lib->eliminar_imagen($sucursal[0]->fotografia,'./libraries/img/personas/')){
										if($this->M_sucursal_cliente->eliminar($sucursal[0]->idsc)){
											echo "ok";
										}else{
											echo "error";
										}
									}else{
										echo "error";
									}
								}else{
									echo "fail";
								}
							}else{
								echo "fail";
							}							
						}else{
							echo "fail";
						}	
					}else{
	   					echo "permiso_bloqueado";
	   				}
	   			}else{
	   				echo "permiso_bloqueado";
	   			}		
			}else{
				if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
			}
		}
   	/*--- Eliminar ---*/
   	public function confirmar_cliente(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
   			if(!empty($privilegio)){
   				if($privilegio[0]->cl1r==1 && $privilegio[0]->cl1d==1){
   					if(isset($_POST['id'])){
						$idcl=$_POST['id'];
						$cliente=$this->M_cliente->get($idcl);
						if(!empty($cliente)){
							$url='./libraries/img/';
							$img="sistema/miniatura/default.jpg";
							if(!empty($cliente[0]->fotografia)){ $img="personas/miniatura/".$cliente[0]->fotografia; }
							$control=$this->M_pedido->get_row('idcl',$idcl);
							if(empty($control)){
								$listado['titulo']="eliminar el cliente <strong>".$cliente[0]->razon."</strong>";
								$listado['desc']="Se eliminara definitivamente el cliente del sistema";
							}else{
								$listado['desc']="Imposible eliminar, el cliente esta <strong>asignado a ".count($control)." pedido(s)</strong>";
								$listado['btn_ok']="none";
								$listado['control']="none";
								$listado['open_control']="none";
							}
							$listado['img']=$url.$img;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail $idcl";
						}
					}else{
						echo "fail";
					}
   				}else{
   					echo "permiso_bloqueado";
   				}
   			}else{
   				echo "permiso_bloqueado";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function drop_cliente(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"cliente");
   			if(!empty($privilegio)){
   				if($privilegio[0]->cl1r==1 && $privilegio[0]->cl1d==1){
   					if(isset($_POST['idcl']) && isset($_POST['u']) && isset($_POST['p']) ){
						$id=$_POST['idcl'];
						$u=$_POST['u'];
						$p=$_POST['p'];
						if($id!=""){
							if($u==$this->session->userdata("login")){
								$usuario=$this->M_usuario->validate($u,$p);
								if(!empty($usuario)){
									if($id!=""){
										$cliente=$this->M_cliente->get($id);
										if(!empty($cliente)){
											$control=$this->M_pedido->get_row('idcl',$id);
											if(empty($control)){
												if($this->lib->eliminar_imagen($cliente[0]->fotografia,'./libraries/img/personas/')){
													$sucursales=$this->M_sucursal_cliente->get_row("idcl",$cliente[0]->idcl);
													for($i=0; $i < count($sucursales); $i++) { $sucursal=$sucursales[$i];
														if($this->lib->eliminar_imagen($sucursal->fotografia,'./libraries/img/personas/')){ }
													}
													if($this->M_cliente->eliminar($cliente[0]->idcl)){
														echo "ok";
													}else{
														echo "error";
													}
												}else{
													echo "error";
												}
											}else{
												echo "fail";
											}
										}else{
											echo "fail";
										}							
									}else{
										echo "fail";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
   					echo "permiso_bloqueado";
   				}
   			}else{
   				echo "permiso_bloqueado";
   			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE CLIENTES -------*/
/*------- MANEJO DE PROVEEDOR -------*/
	public function search_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['privilegio']=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"proveedor");
			$this->load->view('cliente_proveedor/proveedor/search',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function view_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"proveedor");
			if(!empty($privilegio)){
				$atrib="";$val="";
				if(isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['resp'])){//si tenemos busqueda 
					$nit=$_POST['nit'];
					$nom=$_POST['nom'];
					$resp=$_POST['resp'];
					if($nit!=""){
						$atrib="nit";$val=$nit;
					}else{
						if($nom!=""){
							$atrib="razon";$val=$nom;
						}else{
							if($resp!=""){
								$atrib="responsable";$val=$resp;
							}
						}
					}
				}
				$listado['proveedores']=$this->M_proveedor->get_search($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$this->load->view('cliente_proveedor/proveedor/view',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Buscador ---*/

   	/*--- End Buscador ---*/
   	/*--- Nuevo ---*/
   	public function new_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['paises']=$this->M_pais->get_all();
			$this->load->view('cliente_proveedor/proveedor/3-nuevo/form',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function save_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				if($this->val->entero($nit,6,25) && $this->val->strSpace($raz,2,100)){
					$control=$this->M_proveedor->get_row("nit",$nit);
					if(empty($control)){
						$control=true;
						if(!$this->val->strSpace($res,2,150)){ $control=false;}
						if(!$this->val->entero($tel,7,15)){ $control=false;}
						if(!$this->val->email($ema)){ $control=false;}
						if(!$this->val->textarea($dir,2,150)){ $control=false;}
						if(!$this->val->strNoSpace($web,2,150)){ $control=false;}
						if(!$this->val->textarea($obs,2,150)){ $control=false;}
						$img=$this->lib->subir_imagen_miniatura($_FILES,'./libraries/img/personas/','',$this->resize,$nit);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_proveedor->insertar($nit,$raz,$res,$dir,$tel,$web,$ema,$img,$obs)){
								echo "ok";
							}else{

								echo "error";
							}
						}else{
							echo $img;
						}

					}else{
						echo "ci_exist";//cliente ya registrado
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	public function print_proveedores(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(!empty($privilegio)){
				if(isset($_POST['visibles']) && isset($_POST['tbl']) && isset($_POST['nit']) && isset($_POST['nom']) && isset($_POST['res'])){
					$listado['visibles']=json_decode($_POST['visibles']);
					$listado['proveedores']=$this->M_proveedor->get_search("","");
					$listado['privilegio']=$privilegio[0];
					$listado['tbl']=$_POST['tbl'];
					$listado['nit']=trim($_POST['nit']);
					$listado['nom']=trim($_POST['nom']);
					$listado['res']=trim($_POST['res']);
					$this->load->view('cliente_proveedor/proveedor/4-imprimir/print_proveedores',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function export_proveedores(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$privilegio=$this->M_privilegio->get_row("idus",$this->session->userdata("id"));
			if(isset($_GET['file']) && isset($_GET['nit']) && isset($_GET['nom']) && isset($_GET['res'])){
				$atrib="";$val="";
				$nit=trim($_GET['nit']);
				$nom=trim($_GET['nom']);
				$res=trim($_GET['res']);
				if($nit!=""){
					$atrib="nit";$val=$nit;
				}else{
					if($nom!=""){
						$atrib="razon";$val=$nom;
					}else{
						if($res!=""){
							$atrib="gerente";$val=$res;
						}
					}
				}
			 	$listado['proveedores']=$this->M_proveedor->get_search($atrib,$val);
				$listado['privilegio']=$privilegio[0];
				$listado['file']=$_GET['file'];
				$this->load->view('cliente_proveedor/proveedor/4-imprimir/export_proveedores',$listado);
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Imprimir ---*/
   	/*--- Reportes ---*/
   	public function detalle_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$idpro=$_POST['idpro'];
			$listado['proveedor']=$this->M_proveedor->get($idpro);
			$this->load->view('cliente_proveedor/proveedor/5-reportes/detalle_proveedor',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	//modificar
   	public function form_update_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpro'])){
				$idpro=$_POST['idpro'];
				$proveedor=$this->M_proveedor->get($idpro);
				if(!empty($proveedor)){
					$listado['proveedor']=$proveedor[0];
					$listado['idpro']=$idpro;
					$this->load->view('cliente_proveedor/proveedor/6-configuracion/modificar',$listado);
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpr']) && isset($_POST['nit']) && isset($_POST['raz']) && isset($_POST['res']) && isset($_POST['tel']) && isset($_POST['ema']) && isset($_POST['web']) && isset($_POST['dir']) && isset($_POST['obs'])){
				$idpr=trim($_POST['idpr']);
				$nit=trim($_POST['nit']);
				$raz=trim($_POST['raz']);
				$res=trim($_POST['res']);
				$tel=trim($_POST['tel']);
				$ema=trim($_POST['ema']);
				$web=trim($_POST['web']);
				$dir=trim($_POST['dir']);
				$obs=trim($_POST['obs']);
				$proveedor=$this->M_proveedor->get($idpr);
				if(!empty($proveedor) && $this->val->entero($nit,6,25) && $this->val->strSpace($raz,2,100)){
					$guardar=true;
					if($proveedor[0]->nit!=$nit){
						$control=$this->M_proveedor->get_row("nit",$nit);
						if(!empty($control)){
							$guardar=false;
						}
					}
					if($guardar){
						$control=true;
						if(!$this->val->strSpace($res,2,150)){ $control=false;}
						if(!$this->val->entero($tel,7,15)){ $control=false;}
						if(!$this->val->email($ema)){ $control=false;}
						if(!$this->val->textarea($dir,2,150)){ $control=false;}
						if(!$this->val->strNoSpace($web,2,150)){ $control=false;}
						if(!$this->val->textarea($obs,2,150)){ $control=false;}
						$img=$proveedor[0]->fotografia;
						$img=$this->lib->cambiar_imagen($_FILES,'./libraries/img/personas/','',$this->resize,$img,$idpr);
						if($img!='error' && $img!="error_type" && $img!="error_size_img"){
							if($this->M_proveedor->modificar($idpr,$nit,$raz,$res,$dir,$tel,$web,$ema,$img,$obs)){
								echo "ok";
							}else{
								echo "error";
							}
						}else{
							echo $img;
						}
					}else{
						echo "ci_exist";//proveedor ya registrado
					}
				}else{
					echo "fail";
				}

			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	public function confirmar_proveedor(){
   		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"proveedor");
   			if(!empty($privilegio)){
   				if($privilegio[0]->cl2r==1 && $privilegio[0]->cl2d==1){
   					if(isset($_POST['idpr'])){
						$idpr=$_POST['idpr'];
						$proveedor=$this->M_proveedor->get($idpr);
						if(!empty($proveedor)){
							$url='./libraries/img/';
							$img="sistema/miniatura/default.jpg";
							if(!empty($proveedor[0]->fotografia)){ $img="personas/miniatura/".$proveedor[0]->fotografia; }
							$listado['titulo']="eliminar el proveedor <strong>".$proveedor[0]->razon."</strong>";
							$listado['desc']="Se eliminara definitivamente el proveedor del sistema";
							$listado['img']=$url.$img;
							$this->load->view('estructura/form_eliminar',$listado);
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}
   				}else{
   					echo "permiso_bloqueado";
   				}
   			}else{
   				echo "permiso_bloqueado";
   			}
   		}else{
   			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
   		}
   	}
   	public function drop_proveedor(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
   			$privilegio=$this->M_privilegio->get_cliente_proveedor($this->session->userdata("id"),"proveedor");
   			if(!empty($privilegio)){
   				if($privilegio[0]->cl2r==1 && $privilegio[0]->cl2d==1){
   					if(isset($_POST['idpr']) && isset($_POST['u']) && isset($_POST['p']) ){
						$id=$_POST['idpr'];
						$u=$_POST['u'];
						$p=$_POST['p'];
						if($id!=""){
							if($u==$this->session->userdata("login")){
								$usuario=$this->M_usuario->validate($u,$p);
								if(!empty($usuario)){
									if($id!=""){
										$proveedor=$this->M_proveedor->get($id);
										if(!empty($proveedor)){
											if($this->lib->eliminar_imagen($proveedor[0]->fotografia,'./libraries/img/personas/')){
												if($this->M_proveedor->eliminar($proveedor[0]->idpr)){
													echo "ok";
												}else{
													echo "error";
												}
											}else{
												echo "error";
											}
										}else{
											echo "fail";
										}							
									}else{
										echo "fail";
									}
								}else{
									echo "validate";
								}
							}else{
								echo "validate";
							}
						}else{
							echo "fail";
						}
					}else{
						echo "fail";
					}	
				}else{
   					echo "permiso_bloqueado";
   				}
   			}else{
   				echo "permiso_bloqueado";
   			}		
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Eliminar ---*/
/*------- END MANEJO DE PROVEEDOR -------*/
/*------- MANEJO DE CONFIGURACION -------*/
	public function view_config(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			$listado['paises']=$this->M_pais->get_all();
			$listado['ciudades']=$this->M_ciudad->get_all();
			$listado['utilidad_ventas']=$this->M_utilidad_venta->get_all();
			$this->load->view('cliente_proveedor/config/view',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- Pais ---*/
   	public function save_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['p'])){
				if($this->val->strSpace($_POST['p'],2,100)){
					if($this->M_pais->insertar($_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpa']) && isset($_POST['p'])){
				if($_POST['idpa']!="" && $this->val->strSpace($_POST['p'],2,100)){
					if($this->M_pais->modificar($_POST['idpa'],$_POST['p'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_pais(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idpa'])){
				if($this->M_pais->eliminar($_POST['idpa'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Pais ---*/
   	/*--- Ciudad ---*/
   	public function save_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['cp']) && isset($_POST['c'])){
				if($this->val->strSpace($_POST['c'],2,100) && isset($_POST['cp'])!=""){
					if($this->M_ciudad->insertar($_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idci']) && isset($_POST['c']) && isset($_POST['cp'])){
				if($_POST['idci']!="" && $this->val->strSpace($_POST['c'],2,100) && $_POST['cp']!=""){
					if($this->M_ciudad->modificar($_POST['idci'],$_POST['cp'],$_POST['c'])){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_ciudad(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['idci'])){
				if($this->M_ciudad->eliminar($_POST['idci'])){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
   	/*--- End Ciudad ---*/
	/*--- control de porcentaje de utilidad de venta ---*/
	public function save_porcentaje(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['des']) && isset($_POST['por'])){
				$des=$_POST['des'];
				$por=$_POST['por'];
				if($this->val->strSpace($des,2,150) && $this->validaciones->esNroDecimal($por,3,2) && $por<=100){
					$por=$por/100;
					if($this->M_utilidad_venta->insertar($des,$por)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function update_porcentaje(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['des']) && isset($_POST['por']) && isset($_POST['iduv'])){
				$iduv=$_POST['iduv'];
				$des=$_POST['des'];
				$por=$_POST['por'];
				if($this->val->strSpace($des,2,150) && $this->validaciones->esNroDecimal($por,3,2) && $por<=100 && $iduv!=""){
					$por=$por/100;
					if($this->M_utilidad_venta->modificar($iduv,$des,$por)){
						echo "ok";
					}else{
						echo "error";
					}
				}else{
					echo "fail";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	public function delete_porcentaje(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['iduv'])){
				$iduv=$_POST['iduv'];
				if($this->M_utilidad_venta->eliminar($iduv)){
					echo "ok";
				}else{
					echo "error";
				}
			}else{
				echo "fail";
			}
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
	/*--- End control de porcentaje de utilidad de venta ---*/
/*------- END MANEJO DE CONFIGURACION -------*/
/*------- ALERTA ---------*/
	public function alerta(){
		if(!empty($this->session_id) && ($this->session_tipo=="1"  || $this->session_tipo=="2") && empty($this->actualizaciones)){
			if(isset($_POST['titulo'])){
				$listado['titulo']=$_POST['titulo'];
			}else{
				$listado['titulo']="";
			}
			if(isset($_POST['descripcion'])){
				$listado['desc']=$_POST['descripcion'];
			}else{
				$listado['desc']="";
			}
			if(isset($_POST['img'])){
				$listado['img']=$_POST['img'];
			}else{
				$listado['img']=base_url().'libraries/img/sistema/warning.png';
			}
			$listado['control']="";
			$listado['open_control']="false";
			$this->load->view('estructura/form_eliminar',$listado);
		}else{
			if(!empty($this->actualizaciones)){
				redirect(base_url().'update');
			}else{
				echo "logout";
			}
		}
	}
/*------- END ALERTA ---------*/
}

/* End of file almacen.php */
/* Location: ./application/controllers/almacen.php */