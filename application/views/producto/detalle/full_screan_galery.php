<?php $url=base_url().'libraries/img/productos/';?>
<?php $v = array();
    for($i=0; $i < count($fotografias_color); $i++){
        $activo=false;
        $v[] = array('tipo' => 'color', 'id' => $fotografias_color[$i]->idpgrc,'archivo' => $fotografias_color[$i]->archivo,'descripcion' => $fotografias_color[$i]->descripcion,'activo' => $activo);
    }
    for ($i=0; $i < count($fotografias_producto); $i++) { 
        $activo=false;
        if($fotografias_producto[$i]->idpi=='23'){ $activo=true;}
        $v[] = array('tipo' => 'producto', 'id' => $fotografias_producto[$i]->idp,'archivo' => $fotografias_producto[$i]->archivo,'descripcion' => $fotografias_producto[$i]->descripcion,'activo' => $activo);
    }
    $fotografias=json_decode(json_encode($v));
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Detalle de productos</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta name="description" content="" />
    <meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
    <meta name="author" content="bearabeara.co.uk" />
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/icomon.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/gmdev.materialize.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/gmdev.css">
    <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
</head>
<body>
  <div class="carousel g-carousel carousel-slider center" data-indicators="true">
<?php foreach ($fotografias as $key => $fotografia) { ?>
    <div class="carousel-item white">
      <div style="height: 100%">
        <div class="carousel-title">
          <ul class="collapsible popout" data-collapsible="accordion">
            <li>
            <?php if($fotografia->descripcion!="" && $fotografia->descripcion!=null){ ?>
              <div class="collapsible-header"><i class="icon-info-circle"></i></div>
            <?php } ?>
              <div class="collapsible-body"><span><?php echo $fotografia->descripcion;?></span></div>
            </li>
          </ul>
        </div>
          <img height="100%" src="<?php echo $url.$fotografia->archivo;?>">
      </div>
    </div>
<?php } ?>
  </div>
        
<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Advertencia!!</h1>
    <p>Estás utilizando una versión desactualizada de Internet Explorer, actualiza por favor a cualquiera de los siguientes navegadores web para acceder a este sitio web.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Lo siento por los inconvenientes ocasionados!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="<?php echo base_url(); ?>libraries/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/materialize.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>libraries/js/gmdev.js"></script>
    <script>
$('.carousel.carousel-slider').carousel({fullWidth: true});
document.querySelector('.carousel.carousel-slider').style.height = window.innerHeight + "px";
    </script>
</body>
</html>
