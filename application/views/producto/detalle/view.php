<?php 
    $url=base_url()."libraries/img/";
    $seg1=$this->M_producto_seg->max_where('fecha','idp',$producto->idp);
    $seg2=$this->M_producto_grupo_color_seg->max_where('fecha','idpgrc',$producto->idpgrc);
    $tiempo_transcurrido=$this->lib->tiempo_transcurrido($seg1,$seg2,'d ml Y');
    $historial=$this->lib->movimientos_producto($seg1,$seg2,'desc');
?>
                                <div class="row" style="margin-bottom: 0px;"><br>
                                    <div class="col xl6 l7 m12 s12">
                                        <div class="row" style="margin-bottom: 0px;">
                                                <?php $gru="";$nom_gru=""; $col="";$nom_col=" - ".$producto->nombre_c;
                                                    if($producto->abr_g!="" && $producto->abr_g!=null){ $gru="-".$producto->abr_g;$nom_gru=" - ".$producto->nombre_g;}
                                                    if($producto->abr_c!="" && $producto->abr_c!=null){ $col="-".$producto->abr_c;}
                                                ?>
                                                <div class="col l12 m12 s12">
                                                    <ul class="collection with-header">
                                                        <li class="collection-header"><h4><?php echo $producto->nombre.$nom_gru.$nom_col;?></h4></li>
                                                        <li class="collection-item row">
                                                            <div class="col l6 m12 s12"  style="padding-left: 0px;">
                                                                <span class="txt-muted d-inline-block"><strong class="mb-1" style="font-weight: 600; padding-right:5px;">Codigo:</strong> <?php echo $producto->codigo.$gru.$col;?> </span>
                                                            </div>
                                                    <div class="col l6 m12 s12" style="padding-left: 0px;">
                                                        <span class="txt-muted d-inline-block"><strong class="mb-1" style="font-weight: 600; padding-right:5px;">Codigo anterior:</strong> <?php echo $producto->codigo_aux;?> </span>
                                                    </div>
                                                        </li>
                                                        <li class="collection-item">
                                                            <div class="d-flex w-100 justify-content-between">
                                                              <strong class="mb-1" style="font-weight: 600; padding-right:5px;">Creado el: </strong>
                                                            <span class="mb-1"> <?php echo $this->lib->format_date($producto->fecha_creacion,'dl ml Y');?></span>
                                                            </div>
                                                        </li> 
                                                <?php for ($i=0; $i < count($producto_atributos) ; $i++) { $atributo=$producto_atributos[$i]; ?>
                                                        <li class="collection-item">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <strong class="mb-1" style="font-weight: 600; padding-right:5px;"><?php echo $atributo->atributo;?>: </strong>
                                                                <span class="mb-1"> <?php echo $atributo->valor;?></span>
                                                            </div>
                                                        </li>
                                                <?php }?>
                                                <?php for ($i=0; $i < count($grupo_atributos) ; $i++) { $atributo=$grupo_atributos[$i]; ?>
                                                        <li class="collection-item">
                                                            <div class="d-flex w-100 justify-content-between">
                                                              <strong class="mb-1" style="font-weight: 600; padding-right:5px;"><?php echo $atributo->atributo;?>: </strong>
                                                            <span class="mb-1"> <?php echo $atributo->valor;?></span>
                                                            </div>
                                                        </li>
                                                <?php }?>
                                                <?php for ($i=0; $i < count($color_atributos) ; $i++) { $atributo=$color_atributos[$i]; ?>
                                                        <li class="collection-item">
                                                            <div class="d-flex w-100 justify-content-between">
                                                              <strong class="mb-1" style="font-weight: 600; padding-right:5px;"><?php echo $atributo->atributo;?>: </strong>
                                                            <span class="mb-1"> <?php echo $atributo->valor;?></span>
                                                            </div>
                                                        </li>
                                                <?php }?>
                                                        <li class="collection-item">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <strong class="mb-1" style="font-weight: 600; padding-right:5px;">Observaciónes: </strong>
                                                                <span class="mb-1"> <?php echo $producto->observaciones;?></span>
                                                            </div>
                                                        </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col xl6 l5 m12 s12">
                                        <div class="col l12 m12 s12">
                                            <ul class="collection">
                                                <li class="collection-item" style="text-align:right"><a class="" href="#modal1" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $producto->idp;?>"><small><?php echo $tiempo_transcurrido->msj;?></small>&nbsp;
                                                    <span class='icon-clock2'></span></a>
                                                <li class="collection-item">
                                                    <div class="slider g-slider">
                                                        <ul class="slides">
                                                            <?php $fotografias_color=$this->M_producto_imagen_color->get_row('idpgrc',$producto->idpgrc);?>
                                                            <?php $fotografias_producto=$this->M_producto_imagen->get_row('idp',$producto->idp);?>
                                                            <?php $v = array();
                                                                for($i=0; $i < count($fotografias_color); $i++){
                                                                    $v[] = array('tipo' => 'color', 'id' => $fotografias_color[$i]->idpig,'archivo' => $fotografias_color[$i]->archivo,'descripcion' => $fotografias_color[$i]->descripcion,'type'=>'pgcf','activo' => $this->lib->es_portada($fotografias_color[$i],'3',$producto->portada));
                                                                }
                                                                for ($i=0; $i < count($fotografias_producto); $i++) { 
                                                                    $v[] = array('tipo' => 'producto', 'id' => $fotografias_producto[$i]->idpi,'archivo' => $fotografias_producto[$i]->archivo,'descripcion' => $fotografias_producto[$i]->descripcion,'type'=>'pf','activo' => $this->lib->es_portada($fotografias_producto[$i],'1',$producto->portada_producto));
                                                                }
                                                                $fotografias=json_decode(json_encode($v));
                                                            ?>
                                                        <?php 
                                                            $count=0;
                                                            $portada=0;
                                                            foreach ($fotografias as $key => $fotografia){
                                                                if($fotografia->activo){ $portada=$count;}
                                                        ?>
                                                                <li>
                                                                    <a href="javascript:" onclick="view_visor($(this));" data-type="producto" data-p="<?php echo $producto->idp;?>" data-id='<?php echo $fotografia->id;?>' data-type-img="<?php echo $fotografia->type;?>" data-pgc="<?php echo $producto->idpgrc;?>"  data-position="<?php echo $count++;?>"><img src="<?php echo $url.'productos/'.$fotografia->archivo;?>" title="Ampliar galería" id="img-zoom"></a>
                                                                    <div class="caption g-caption center-align ">
                                                                    <?php if($fotografia->descripcion!="" || $fotografia->descripcion!=NULL){?>
                                                                        <h3><?php echo $fotografia->descripcion;?></h3>
                                                                    <?php }?>
                                                                    </div>
                                                                </li>
                                                        <?php } ?>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collection">
              <li class="collection-item">
                <div class="row">
                            <ul id="tabs-swipe-demo" class="tabs">
                    <li class="tab col s3"><a class="active" href="#t1">Fotografías</a></li>
                    <li class="tab col s3"><a href="#t2">Materiales</a></li>
                    <li class="tab col s3"><a href="#t3">Piezas</a></li>
                    <li class="tab col s3"><a href="#t4">Repujados o Sellos</a></li>
                  </ul>
                  <div id="t1" class="col s12">
                  <?php if(!empty($fotografias_color) || !empty($fotografias_producto)){ ?>
                        <ul class="collection g-collection">
                        <?php 
                        $count=0;
                        foreach ($fotografias as $key => $fotografia) {?>
                            <li class="collection-item avatar g-avatar col l6 m6 s12">
                            <a href="javascript:"  onclick="view_visor($(this));" data-type="producto" data-p="<?php echo $producto->idp;?>" data-id='<?php echo $fotografia->id;?>' data-type-img="<?php echo $fotografia->type;?>" data-pgc="<?php echo $producto->idpgrc;?>"  data-position="<?php echo $count++;?>">
                                <img src="<?php echo $url.'productos/miniatura/'.$fotografia->archivo;?>" class="img-thumbnail g-thumbnail-modal">
                            </a>
                              <span class="title"><?php echo $producto->nombre;?></span>
                              <p><?php echo $fotografia->descripcion;?></p>
                            </li>
                        <?php }?>
                          </ul>
                    <?php }else{
                        echo "<h5>Ninguna fotografia encontrada en el producto.</h5>";
                    }?>
                  </div>
                  <div id="t2" class="col s12">
                <?php if(!empty($producto_materiales) || !empty($grupo_materiales) || !empty($color_materiales)){ ?>
                    <table class="responsive-table highlight">
                        <thead>
                             <tr>
                                <th width="80px">Imagen<br></th>
                                <th width="30%">Nombre</th>
                                <th width="15%">Color</th>
                                <th width="15%">Cantidad</th>
                                <th width="40%">Observaciónes</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                <?php
                        $v_material = array();
                        for($i=0; $i < count($producto_materiales); $i++){ $material=$producto_materiales[$i];
                            $img="sistema/miniatura/default.jpg";
                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
                            $unidad=$this->M_unidad->get($material->idu);
                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
                        }
                        for($i=0; $i < count($grupo_materiales); $i++){ $material=$grupo_materiales[$i];
                            $img="sistema/miniatura/default.jpg";
                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
                            $unidad=$this->M_unidad->get($material->idu);
                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
                        }
                        for($i=0; $i < count($color_materiales); $i++){ $material=$color_materiales[$i];
                            $img="sistema/miniatura/default.jpg";
                            if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
                            $unidad=$this->M_unidad->get($material->idu);
                            $v_material[]= array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad' =>$material->cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad[0]->nombre,'abr_medida' => $unidad[0]->abr);
                        }
                        $materiales=$this->lib->sort_2d($v_material,'nombre','asc');
                        $count=0;
                        foreach ($materiales as $key => $material){ ?>
                        <tr>
                          <td>
                          <a href="javascript:" onclick="view_visor($(this));" data-type="material" data-p="<?php echo $producto->idp;?>" data-id='<?php echo $material->idm;?>' data-position="<?php echo $count++;?>" data-pgc='<?php echo $producto->idpgrc;?>'>
                            <img src="<?php echo $material->imagen;?>" class="img-thumbnail g-thumbnail-modal" width="60px" >
                            </a>
                          </td>
                          <td><?php echo $material->nombre;?></td>
                          <td><ul class="list-group" style="font-size:12px; padding: 1px">
                               <li class="list-group-item center" style="margin: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->color;?></li>
                                </ul>
                            </td>
                            <td><?php echo $material->cantidad."[".$material->abr_medida.".]"?></td>
                            <td><?php echo $material->observacion;?></td>
                            <td>
                                <a class="btn-floating btn-flat waves-effect waves-light tooltipped <?php if($material->verificado=='1'){?>green darken-1<?php }else{?>blue-grey lighten-3<?php }?>" data-position="left" data-delay="50" data-tooltip="<?php if($material->verificado=='1'){?>Datos verificados y confiables<?php }else{?>Esperando revisión de datos<?php }?>" style="width: 25px; height: 25px;"><i class="material-icons" style="line-height: 25px;">check</i></a>
                            </td>
                        </tr>
                <?php   } ?>
                    </tbody>
                </table>
                <?php }else{
                    echo "<h5>Ningun material asignado en el producto.</h5>";
                }?>
                  </div>
            <?php 
                $v_piezas = array();
                $v_rs=array();
                for($i=0; $i < count($producto_piezas); $i++){ $pieza=$producto_piezas[$i];
                    $img="sistema/miniatura/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/miniatura/".$pieza->fotografia;}
                     $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idppi,'type' => 'ppi','rs' => $pieza->idrs,'idm' => $pieza->idm,'fotografia' => $url.$img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
                     if($pieza->idrs!=0){
                        $re_se=$this->M_repujado_sello->get($pieza->idrs);
                        if(!empty($re_se)){
                            $img_rs="sistema/miniatura/default.jpg";
                            if($re_se[0]->imagen!="" && $re_se[0]->imagen!=NULL){ $img_rs="repujados_sellos/miniatura/".$re_se[0]->imagen;}
                            $v_rs[]=array('rs'=>$re_se[0]->idrs,'imagen_rs' => $url.$img_rs,'nombre' => $re_se[0]->nombre,'tipo' => $re_se[0]->tipo,'nombre_pieza' =>$pieza->nombre,'observacion' => $pieza->descripcion);
                        }
                     }
                }
                for($i=0; $i < count($color_piezas); $i++){ $pieza=$color_piezas[$i];
                    $img="sistema/miniatura/default.jpg";if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/miniatura/".$pieza->fotografia;}
                    $v_piezas[]= array('nombre'=>$pieza->nombre,'id'=>$pieza->idpgcpi,'type' => 'pgcpi','rs' => $pieza->idrs,'idm' => $pieza->idm,'fotografia' => $url.$img,'cantidad' => $pieza->cantidad,'largo'=>$pieza->largo,'ancho' =>$pieza->ancho,'observacion' => $pieza->descripcion);
                    if($pieza->idrs!=0){
                        $re_se=$this->M_repujado_sello->get($pieza->idrs);
                        if(!empty($re_se)){
                            $img_rs="sistema/miniatura/default.jpg";
                            if($re_se[0]->imagen!="" && $re_se[0]->imagen!=NULL){ $img_rs="repujados_sellos/miniatura/".$re_se[0]->imagen;}
                            $v_rs[]=array('rs'=>$re_se[0]->idrs,'imagen_rs' => $url.$img_rs,'nombre' => $re_se[0]->nombre,'tipo' => $re_se[0]->tipo,'nombre_pieza' =>$pieza->nombre,'observacion' => $pieza->descripcion);
                        }
                     }
                }
                $repujados_sellos=$this->lib->sort_2d($v_rs,'nombre','asc');
            ?>
                  <div id="t4" class="col s12">
                    <?php if(count($v_rs)>0){?>
                        <table class="responsive-table">
                            <thead>
                                 <tr>
                                    <th width="80px">Imagen</th>
                                    <th width="15%">Tipo</th>
                                    <th width="25%">Nombre</th>
                                    <th width="20%">Lugar en el producto</th>
                                    <th width="40%">Observaciónes</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php 
                        $count=0;
                        foreach ($repujados_sellos as $key => $rs) { ?>
                            <tr>
                                <td><img src="<?php echo $rs->imagen_rs;?>" class="img-thumbnail g-thumbnail-modal" width="60px" onclick="view_visor($(this));" data-type="repujado_sello" data-p="<?php echo $producto->idp;?>" data-id='<?php echo $rs->idrs;?>' data-position="<?php echo $count++;?>" data-pgc='<?php echo $producto->idpgrc;?>'></td>
                                <td><?php if($rs->tipo==0){ echo "Repujado";}if($rs->tipo==1){ echo "sello";}?></td>
                                <td><?php echo $rs->nombre;?></td>
                                <td><?php echo "En la pieza <b>".$rs->nombre_pieza."</b>";?></td>
                                <td><?php echo $rs->observacion;?></td>
                            </tr>
                    <?php }?>
                            </tbody>
                        </table>
                <?php }else{
                    echo "<h5>El producto no posee ningun repujado o sello.</h5>";
                    } ?>
                  </div>
                  <div id="t3" class="col s12">
                    <?php if(!empty($producto_piezas) || !empty($color_piezas)){ ?>
                        <table class="responsive-table">
                            <thead>
                                 <tr>
                                    <th width="80px">Imagen</th>
                                    <th width="22%">Nombre</th>
                                    <th width="15%">Cantidad</th>
                                    <th width="18%">Dimensiones</th>
                                    <th width="80px">Material</th>
                                    <th width="80px">Repujado o sello</th>
                                    <th width="40%">Observaciónes</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php $piezas=$this->lib->sort_2d($v_piezas,'nombre','desc');
                            $count1=0;
                            $count=0;
                            foreach ($piezas as $key => $pieza){
                                $material_pieza=$this->lib->search_elemento($all_materiales,"idm",$pieza->idm);
                                if($material_pieza!=null){
                                    $img="sistema/miniatura/default.jpg";
                                    $imagen=$this->lib->search_elemento($all_materiales,'idmi',$material_pieza->idmi);
                                    if($imagen!=null){
                                        if($imagen->fotografia!="" && $imagen->fotografia!=null){$img="materiales/miniatura/".$imagen->fotografia;}
                                    }
                                }
                    ?>
                            <tr>
                              <td>
                                <img src="<?php echo $pieza->fotografia;?>" class="img-thumbnail g-thumbnail-modal" width="60px" onclick="view_visor($(this));" data-type="pieza" data-p="<?php echo $producto->idp;?>" data-id='<?php echo $pieza->id;?>' data-type-img="<?php echo $pieza->type;?>" data-pgc="<?php echo $producto->idpgrc;?>"  data-position="<?php echo $count1++;?>">
                              </td>
                              <td><?php echo $pieza->nombre;?></td>
                              <td><?php echo $pieza->cantidad." [unid.]";?></td>
                              <td><?php echo $pieza->largo." [cm.] largo x ".$pieza->ancho." [cm.] ancho";?></td>
                              <td>
                                <?php if($material_pieza!=null){?>
                                    <img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail g-thumbnail-modal" width="60px" data-title="<?php echo $material_pieza->nombre;?>" data-desc="<br>">
                                <?php }?>
                              </td>
                              <td>
                                <?php if($pieza->rs!=0){

                                        $re_se=$this->M_repujado_sello->get($pieza->rs);
                                        if(!empty($re_se)){
                                            $img="sistema/miniatura/default.jpg";
                                            if($re_se[0]->imagen!="" && $re_se[0]->imagen!=NULL){
                                                $img="repujados_sellos/miniatura/".$re_se[0]->imagen;
                                            }
                                ?>
                                            <img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail g-thumbnail-modal" width="60px" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo '';?>">
                                <?php
                                        }
                                    }
                                ?>
                              </td>
                                <td><?php echo $pieza->observacion;?></td>
                            </tr>
                    <?php
                            }
                    ?>
                        </tbody>
                    </table>
                    <?php }else{
                        echo "<h5>Ninguna pieza creada en el producto.</h5>";
                    }?>

                  </div>
                </div>
                

              </li>
            </ul>
        </div>
    </div>
  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
      <h6>5 ultimos cambios</h6>
      <ul class="collection">
    <?php $count=0;
        foreach($historial as $key => $cambio){
            if($count>=5){ break;}
            $usuario=$this->M_usuario->get($cambio->idu);
            $img="sistema/avatar.jpg";
            if(!empty($usuario)){ 
                $persona=$this->M_persona->get($usuario[0]->ci);
                if($persona[0]->fotografia!=NULL && $persona[0]->fotografia!=""){
                    $img="personas/miniatura/".$persona[0]->fotografia;
                }
            }
            if($cambio->accion=="c"){
                $msj="Creó el producto";
            }else{
                $msj=$cambio->msj_adicional;
            }
    ?>
        <li class="collection-item avatar">
          <img src="<?php echo $url.$img;?>" alt="" class="circle">
          <span><?php echo $cambio->usuario.", ".$msj;?></span><br>
          <small><date><?php 
          $msj=$this->lib->mensaje_tiempo_transcurrido($cambio->fecha,$cambio->usuario,'dl ml Y');
          echo $msj->tiempo; ?></date></small>
        </li>
    <?php $count++;} ?>
      </ul>
    </div>
  </div>
<script>
    $('ul.tabs').tabs({swipeable:false});
    $('.slider').slider();
    $("div.tabs-content").removeAttr('style');
    $('div.tabs-content.carousel').hover(function(){ $(this).removeAttr('style');});
    $('.modal').modal();
    for(var i = 0; i < <?php echo $portada;?>; i++){$('.slider').slider('next');}
    $("img.g-img-thumbnail").visor();
$(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
</script>