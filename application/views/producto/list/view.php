<?php $url=base_url().'libraries/img/';?>

    <div class="row">
    <?php if(count($productos)){ ?>
        <?php for ($i=0; $i < count($productos) ; $i++){ $producto=$productos[$i]; 
            $img="sistema/miniatura/default.jpg";
            if($producto->portada!="" && $producto->portada!=null){
                $v=explode("|", $producto->portada);
                if(count($v==2)){
                    if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                    if($v[0]=="3"){ 
                        $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}
                    }
                }
            }else{
                //verificando si el producto tiene portada
                if($producto->portada_producto!="" && $producto->portada_producto!=null){
                    $v=explode("|", $producto->portada_producto);
                    if(count($v==2)){
                        if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                        if($v[0]=="3"){
                            $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}
                        }
                    }
                }
            }
    ?>
                <div class="col s12  m4 l3 xl2">
                     <?php 
                        $seg1=$this->M_producto_seg->max_where('fecha','idp',$producto->idp);
                        $seg2=$this->M_producto_grupo_color_seg->max_where('fecha','idpgrc',$producto->idpgrc);
                        $tiempo_transcurrido=$this->lib->tiempo_transcurrido($seg1,$seg2,'Y/m/d');
                    ?>
                    <div class="card">
                        <a href="<?php echo base_url().'productos/detalle/'.$producto->idpgrc;?>">
                            <div class="card-action center">
                              <span class="card-title"><strong><?php echo $producto->nombre;?></strong></span>
                            </div>
                        <?php if($tiempo_transcurrido->segundos<86400 && $tiempo_transcurrido->segundos>0){ ?>
                            <span class="new badge blue" data-badge-caption="">Modificado recientemente</span>
                        <?php }?>
                            <div class="card-image" style="text-align:center">
                              <img src="<?php echo $url.$img;?>" class="img-thumbnail">
                            </div>
                            <div class="card-content">
                            <?php $gru=""; $nom_gru="";if($producto->abr_g!="" && $producto->abr_g!=null){ $gru="-".$producto->abr_g;$nom_gru=$producto->nombre_g;} ?>
                                <span class="card-title"><?php echo $nom_gru;?></span>
                                <p>
                                    Código: <?php echo $producto->codigo.$gru;?>-<?php echo $producto->abr_c;?><br>
                                    Color: <?php echo $producto->nombre_c;?><br>
                                    Código anterior: <?php echo $producto->codigo_aux;?>
                                </p>
                            </div>
                            <div class="card-action" style="text-align:right;">
                                <small class="text-muted" style="font-size:.65rem;">Modificado <?php echo $tiempo_transcurrido->tiempo;?></small>
                            </div>
                        </a>
                    </div>
                </div>
                <?php if(($i+1)%3==0){ ?>
                    <i class='show-on-medium clearfix'></i>
                <?php } ?>
                <?php if(($i+1)%4==0){ ?>
                <i class="show-on-large clearfix"></i>
                <?php } ?>
                <?php if(($i+1)%6==0){ ?>
                <i class="show-on-xlarge clearfix"></i>
                <?php } ?>
        <?php }//end for ?>
    <?php }else{// end if 
        echo "<div class='center'><h3>0 Registros encontrados.</h3></div>";
    }?>
    </div>