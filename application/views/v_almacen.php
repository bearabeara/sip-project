<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Almacenes','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
				if($privilegio[0]->al1r==1){ $v_menu.="Almacenes/ver/icon-office|"; }
				if($privilegio[0]->al2r==1){ $v_menu.="Mov. de Mat./hist/fa fa-line-chart|";}
				if($privilegio[0]->al3r==1){ $v_menu.="Mov. de Producto/hist2/fa fa-bar-chart-o";}
		?>
			<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
			<?php $this->load->view('estructura/chat');?>
			<?php $almacenes=$this->M_almacen->get_all(); ?>
			<?php $this->load->view('estructura/menu_izq',['ventana'=>'almacen','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>			
			<div id="search"></div>
			<div id="contenido"></div>
			<?php $this->load->view('estructura/js',['js'=>$dir.'almacen'.$min.'.js']);?>
	</body>
<?php 
	$title="";$activo="";$search="";$view="";
	switch($pestania){
			case '1': if($privilegio[0]->al1r==1){ $title="Almacenes"; $activo='ver'; $search="almacen/search_almacen"; $view="almacen/view_almacen";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '2': if($privilegio[0]->al2r==1){ $title="Historial de movimiento de materiales"; $activo='hist'; $search="almacen/search_movimiento_material"; $view="almacen/view_movimiento_material";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
			case '3': if($privilegio[0]->al3r==1){ $title="Historial de movimiento de productos"; $activo='hist2'; $search="almacen/search_movimiento_producto"; $view="almacen/view_movimiento_producto";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
			default: $title="404"; $view=base_url()."login/error";
	} ?>
	<script>$(this).get_2n('<?php echo $search; ?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','almacen?p=<?php echo $pestania;?>');
	</script>
</html>