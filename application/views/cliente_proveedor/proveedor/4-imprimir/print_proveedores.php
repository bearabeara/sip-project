<?php 
	$url=base_url().'libraries/img/';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_proveedores', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'cliente_proveedor/export_proveedores?nit='.$nit.'&nom='.$nom.'&res='.$res."&file=xls");
		$word = array('controller' => 'cliente_proveedor/export_proveedores?nit='.$nit.'&nom='.$nom.'&res='.$res."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>NIT/CI</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 35%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre o razón social</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 15%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Responsable</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Telf./Cel.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 5%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Sitio web</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 5%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Correo electrónico</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Dirección</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 30%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="19" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE PROVEEDORES']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="4%" style="display: none;">#</th>
								<th class="celda th padding-4" data-column="2" width="4%">Fotografía</th>
								<th class="celda th padding-4" data-column="3" width="10%">NIT/CI</th>
								<th class="celda th padding-4" data-column="4" width="35%">Nombre o razón social</th>
								<th class="celda th padding-4" data-column="5" width="15%">Responsable</th>
								<th class="celda th padding-4" data-column="6" width="10%">Telf./Cel.</th>
								<th class="celda th padding-4" data-column="7" width="5%">Sitio Web</th>
								<th class="celda th padding-4" data-column="8" width="20%">Correo electrónico</th>
								<th class="celda th padding-4" data-column="9" width="20%">Dirección</th>
								<th class="celda th padding-4" data-column="10" width="30%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){ 
									$proveedor=$this->lib->search_elemento($proveedores,"idpr",$visible);
									if($proveedor!=null){
										$cont++;
										$img='sistema/miniatura/default.jpg';
										if($proveedor->fotografia!=NULL && $proveedor->fotografia!=""){ $img="personas/miniatura/".$proveedor->fotografia;}
							?>
								<tr class="fila">
									<td class="celda td padding-4"  data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $proveedor->nit;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $proveedor->razon;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $proveedor->responsable;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $proveedor->telefono;?></td>
									<td class="celda td padding-4" data-column="7"><?php echo $proveedor->url;?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $proveedor->email;?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $proveedor->direccion;?></td>
									<td class="celda td padding-4" data-column="10"><?php echo $proveedor->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>