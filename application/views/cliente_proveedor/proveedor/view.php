<?php 
	$url=base_url().'libraries/img/';
	$j_proveedores=json_encode($proveedores);
?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class="g-thumbnail">#Item</th>
			<th class="celda-sm-10 hidden-sm">NIT/CI</th>
			<th class="celda-sm-30">Nombre o Razon Social</th>
			<th style="width:25%" class="hidden-sm">Nombre de Responsable</th>
			<th style="width:15%" class="hidden-sm">Teléfono/Celular</th>
			<th style="width:20%" class="hidden-sm">Dirección</th>
			<th style="width:0%"></th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($proveedores)>0){ for ($i=0; $i < count($proveedores) ; $i++) { $proveedor=$proveedores[$i];
			$img="sistema/miniatura/default.jpg";
			if($proveedor->fotografia!=NULL && $proveedor->fotografia!=''){ $img="personas/miniatura/".$proveedor->fotografia;}
	?>
		<tr data-pr="<?php echo $proveedor->idpr;?>">
			<td class="g-thumbnail"><div id="item"><?php echo $i+1;?></div><div class="g-img"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail"></div></td>
			<td class="hidden-sm"><?php echo $proveedor->nit;?></td>
			<td><?php echo $proveedor->razon;?></td>
			<td class="hidden-sm"><?php echo $proveedor->responsable;?></td>
			<td class="hidden-sm"><?php echo $proveedor->telefono;?></td>
			<td class="hidden-sm"><?php echo $proveedor->direccion;?></td>
			<td class="text-right">
				<?php 
					$det=json_encode(array('function'=>'reportes_proveedor','atribs'=> json_encode(array('pr' => $proveedor->idpr)),'title'=>'Detalle'));
					$conf=""; if($privilegio->cl2u=="1"){ $conf=json_encode(array('function'=>'config_proveedor','atribs'=> json_encode(array('pr' => $proveedor->idpr)),'title'=>'Configurar'));}
					$del=""; if($privilegio->cl2d=="1"){ $del=json_encode(array('function'=>'confirm_proveedor','atribs'=> json_encode(array('pr' => $proveedor->idpr)),'title'=>'Eliminar'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
			</td>
		</tr>
	<?php }//end for
	}else{
		echo "<tr><td colspan='7'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<?php if($privilegio->cl2r=="1" && ($privilegio->cl2p=="1" || $privilegio->cl2c=="1")){?>
	<div id="fab-proveedor">
		<div id="btns-fab-wrapper" class="btns-fab-wrapper">
		<?php if($privilegio->cl2r=="1" && $privilegio->cl2p=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-yellow-darken waves-effect waves-light print_proveedores" data-tbl="table#tbl-container" data-toggle="tooltip" data-placement="left" title="Reportes">
			        <span><i class="icon-clipboard7"></i></span>
			    </button>
			</div>
		<?php }?>
		<?php if($privilegio->cl2r=="1" && $privilegio->cl2c=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-success waves-effect waves-light new_proveedor" data-toggle="tooltip" data-placement="left" title="Nuevo Empleado">
			        <span>+</span>
			    </button>
			</div>
		<?php }?>
		</div>
		<button id="btn-fab-main" class="gfab-main-btn bg-danger waves-effect waves-light"><span><i class="icon-menu"></i></span></button>
	</div>
<?php }?>
<script>$(".reportes_proveedor").reportes_proveedor();$(".config_proveedor").config_proveedor();$(".confirm_proveedor").confirm_proveedor();
<?php if($privilegio->cl2r=="1" && $privilegio->cl2c=="1" || $privilegio->cl2p=="1"){?>
	$("div#fab-proveedor").jqueryFab();
	<?php if($privilegio->cl2c=="1"){?>
		$("button.new_proveedor").new_proveedor();
	<?php }?>
	<?php if($privilegio->cl2p=="1"){?>
		$("button.print_proveedores").print_proveedores();
	<?php }?>
<?php }?>
</script>