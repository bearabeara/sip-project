<?php
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase las dimenciones el tamaño de 1.5MB</strong>, para evitar sobre cargar al sistema"';
	$help2='title="Número de NIT o CI" data-content="Ingrese un numero de NIT o CI con valores numericos <b>sin espacios</b>, de 6 a 25 digitos"';
	$help3='title="Ingresar nombre o razon social" data-content="Ingrese un Nombre o Razon social alfanumerico de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="Ingresar nombre de resonsable" data-content="Ingrese un Nombre alfanumerico de 2 a 150 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="teléfono o celular" data-content="Ingrese un Número de telefono o celular de 7 a 15 digitos <b>sin espacios</b>"';
	$help6='title="Email" data-content="Ingrese un Dirección de correo electronico con el siguientes formato ejemplo@dominio.com, <b>sin espacios</b>, el correo debe tener un maximo de 60 caracteres"';
	$help9='title="Dirección" data-content="Ingrese una de la empresa en formato alfanumerico de 5 a 200 caracteres <b>puede incluir espacios</b>, ademas la direccion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ/º,-.:)<b>"';
	$help10='title="Sitio Web" data-content="Ingrese una url de la pagina de la empresa con el formato http://wwww.ejemplo.com.bo, se acepta una url con una maximo de 150 caracteres"';
	$help11='title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 500 caracteres <b>puede incluir espacios</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-sm-2 col-xs-12 form-control-label">Fotografía:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-sm" id="new_file" type="file" placeholder='Seleccione fotografia'>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> NIT:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_nit" type="number" placeholder='NIT/CI' min="0" max="9999999999999999999999999" value="<?php echo $proveedor->nit;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Razón social:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_raz" type="text" placeholder='Razón Social' maxlength="90" value="<?php echo $proveedor->razon;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Resposable:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_res" type="text" placeholder='Nombre del responsable' maxlength="150"  value="<?php echo $proveedor->responsable;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Telf./Cel.:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_tel" type="number" placeholder='Número de teléfono o celular' min='0'  value="<?php echo $proveedor->telefono;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Email:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_ema" type="email" placeholder='example@dominio.com' maxlength="60"  value="<?php echo $proveedor->email;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Dirección:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_dir" type="text" placeholder='Z/Villa Fatima C/Saturnino Porcel' maxlength="200"  value="<?php echo $proveedor->direccion;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help9;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Sitio web:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<input class="form-control form-control-sm" id="new_web" type="text" placeholder='http://wwww.ejemplo.com.bo' maxlength="150"  value="<?php echo $proveedor->url;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help10;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>		
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return update_proveedor($(this))" data-pr="<?php echo $proveedor->idpr;?>">
						<div class="input-group">
							<textarea class="form-control form-control-sm" id="new_obs" placeholder='Observaciónes de Cliente' maxlength="500" rows="3"><?php echo $proveedor->descripcion;?></textarea>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>	
		</div>
	</div>
</div>
<script language='javascript'>Onfocus("new_nit");$('[data-toggle="popover"]').popover({html:true});</script>