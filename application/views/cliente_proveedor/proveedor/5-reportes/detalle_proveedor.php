<?php 
	$proveedor=$proveedor[0];
	$url=base_url().'libraries/img/personas/miniatura/';
	$img="default.png";
	if($proveedor->fotografia!=NULL && $proveedor->fotografia!=''){$img=$proveedor->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12">
		<div class="table-responsive">
			<table border="0" class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th">NIT/CI</th>
					<td class="celda td" colspan="3"><?php echo $proveedor->nit;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Nombre/Razon Social:</th><td class="celda td" colspan="3"><?php echo $proveedor->razon;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Responsable:</th><td class="celda td" colspan="3"><?php echo $proveedor->responsable;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Telf/Cel.:</th><td class="celda td" colspan="3"><?php echo $proveedor->telefono;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Correo electrónico:</th><td class="celda td" colspan="3"><?php echo $proveedor->email;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Sitio Web:</th><td class="celda td" colspan="3"><?php if( $proveedor->url!="" && $proveedor->url!=NULL){?><a target="__black" href="<?php if(strrpos($proveedor->url, 'http')===false){?>http://<?php }?><?php echo $proveedor->url;?>"><?php echo $proveedor->url;?></a><?php }?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Dirección:</th><td class="celda td" colspan="3"><?php echo $proveedor->direccion;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Observaciones:</th><td class="celda td" colspan="3"><?php echo $proveedor->descripcion;?></td>
				</tr>
			</table>
		</div>
	</div>
</div>