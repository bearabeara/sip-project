<?php $rand=rand(10,999999); ?>
<ul class="nav nav-tabs" role="tablist">
   <li class="nav-item"><a class="nav-link config_cliente<?php echo $rand;?>" href="javascript:" role="tab" data-cl='<?php echo $cliente->idcl;?>'><i class="icofont icofont-home"></i>Modificar cliente</a></div></li>
   <li class="nav-item"><a class="nav-link view_sucursal active" href="javascript:" role="tab" data-cl="<?php echo $cliente->idcl;?>"><i class="icofont icofont-ui-user"></i>Sucursales</a></div></li>
</ul>
<?php $url=base_url().'libraries/img/';?>
<div class="list-group" id="client" data-cl="<?php echo $cliente->idcl;?>">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive g-table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th class="img-thumbnail-50"><div class="img-thumbnail-50"></div> #Item</th>
						<th class="celda-sm-10">NIT</th>
						<th class="celda-sm-20">Nombre de sucursal</th>
						<th width="20%">Responsable</th>
						<th width="10%">Teléfono</th>
						<th width="15%">Email</th>
						<th width="20%">Dirección</th>
						<th width="5%"></th>
					</tr>
				</thead>
				<tbody>
				<?php if(count($sucursales)>0){ ?>
				<?php for($i=0; $i < count($sucursales); $i++){ $sucursal=$sucursales[$i]; 
						$img="sistema/miniatura/default.jpg";
						if($sucursal->fotografia!=NULL && $sucursal->fotografia!=''){$img="personas/miniatura/".$sucursal->fotografia;}
				?>
						<tr>
							<td class="img-thumbnail-50"><div id="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-50" data-title="<?php echo $cliente->razon."-".$sucursal->nombre;?>" data-desc="<br>"></td>
							<td><?php if($sucursal->nit!="" && $sucursal->nit!=NULL){ echo $cliente->nit."-".$sucursal->nit; }else{ echo $cliente->nit; }?></td>
							<td><?php echo $cliente->razon."-".$sucursal->nombre;?></td>
							<td><?php echo $sucursal->responsable;?></td>
							<td><?php echo $sucursal->telefono;?></td>
							<td><?php echo $sucursal->email;?></td>
							<td><?php echo $sucursal->direccion;?></td>
							<td>
								<div class="g-control-accordion" style="width:60px;">
		      						<button class="change_sucursal" data-sc="<?php echo $sucursal->idsc;?>"><i class="fa fa-cog"></i></button>
	      							<button class="confirmar_sucursal" data-sc="<?php echo $sucursal->idsc;?>"><i class="icon-trashcan2"></i></button>
	      						</div>
							</td>
						</tr>
				<?php } ?>
				<?php }else{ ?>
					<tr>
						<td colspan="8" class="text-center"><h2>0 sucursales registradas...</h2></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row text-right" style="padding-right:15px;">
          	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_sucursal">
          		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar sucursal</span>
            </button>
        </div>
	</div>
</div>
<script>$(".config_cliente<?php echo $rand;?>").config_cliente();$(".view_sucursal").view_sucursal();$("img.img-thumbnail-50").visor();$("button.new_sucursal").new_sucursal();$("button.change_sucursal").change_sucursal();$("button.confirmar_sucursal").confirmar_sucursal();</script>