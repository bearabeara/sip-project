<?php
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase el peso de <strong>1.5MB</strong>, para evitar sobre cargar al sistema"';
	$help2='title="Número de NIT o CI" data-content="Ingrese un numero de NIT o CI con valores numericos <b>sin espacios</b>, hasta 25 digitos"';
	$help3='title="Ingresar nombre de sucursal" data-content="Ingrese un nombre de sucursal alfanumerico de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="Ingresar nombre de responsable" data-content="Ingrese un nombre alfanumerico de 2 a 150 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="teléfono o celular" data-content="Ingrese un Número de telefono o celular de 7 a 15 digitos <b>sin espacios</b>"';
	$help6='title="Email" data-content="Ingrese un dirección de correo electronico con el siguientes formato ejemplo@dominio.com, <b>sin espacios</b>, el correo debe tener un maximo de 60 caracteres"';
	$help9='title="Dirección" data-content="Ingrese la direccion de la sucursal en formato alfanumerico de 5 a 200 caracteres <b>puede incluir espacios</b>, ademas la direccion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ/º,-.:)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Fotografía:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-xs" id="new_file" type="file" placeholder='Seleccione fotografia'>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"> NIT:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<span class="input-group-addon form-control-sm"><?php echo $cliente->nit;?></span>
							<input class="form-control form-control-xs" id="new_nit" type="number" placeholder='NIT' min="0" max="9999999999999999999999999" value="<?php echo $sucursal->nit;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Nombre de sucursal:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<span class="input-group-addon form-control-sm"><?php echo $cliente->razon;?></span>
							<input class="form-control form-control-xs" id="new_raz" type="text" placeholder='Nombre de sucursal' maxlength="90" value="<?php echo $sucursal->nombre;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Resposable:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="new_res" type="text" placeholder='Nombre del responsable' maxlength="150" value="<?php echo $sucursal->responsable;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Telf./Cel.:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="new_tel" type="number" placeholder='Número de teléfono o celular' min='0' value="<?php echo $sucursal->telefono;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Email:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="new_ema" type="email" placeholder='example@dominio.com' maxlength="60" value="<?php echo $sucursal->email;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Dirección:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="new_dir" type="text" placeholder='Z/Villa Fatima C/Saturnino Porcel' maxlength="200" value="<?php echo $sucursal->direccion;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help9;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
		</div>
	</div>
</div>
<script>Onfocus("new_nit");$('[data-toggle="popover"]').popover({html:true});</script>