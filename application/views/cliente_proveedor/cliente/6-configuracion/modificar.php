<?php
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase el peso de <strong>1.5MB</strong>, para evitar sobre cargar al sistema"';
	$help2='title="Número de NIT o CI" data-content="Ingrese un numero de NIT o CI con valores numericos <b>sin espacios</b>, de 6 a 25 digitos"';
	$help3='title="Ingresar razon social" data-content="Ingrese una razon social alfanumerica de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help4='title="Ingresar nombre del gerente" data-content="Ingrese un Nombre alfanumerico de 2 a 150 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="teléfono o celular" data-content="Ingrese un Número de telefono o celular de 7 a 15 digitos <b>sin espacios</b>"';
	$help6='title="Email" data-content="Ingrese un Dirección de correo electronico con el siguientes formato ejemplo@dominio.com, <b>sin espacios</b>, el correo debe tener un maximo de 60 caracteres"';
	$help9='title="Dirección" data-content="Ingrese una de la empresa en formato alfanumerico de 5 a 200 caracteres <b>puede incluir espacios</b>, ademas la direccion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ/º,-.:)<b>"';
	$help10='title="Sitio Web" data-content="Ingrese una url de la pagina de la empresa con el formato http://wwww.ejemplo.com.bo, se acepta una url con una maximo de 150 caracteres"';
	$help11='title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 500 caracteres <b>puede incluir espacios</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
   <li class="nav-item"><a class="nav-link config_cliente<?php echo $rand;?> active" href="javascript:" role="tab" data-cl='<?php echo $cliente->idcl;?>'><i class="icofont icofont-home"></i>Modificar cliente</a></div></li>
   <li class="nav-item"><a class="nav-link view_sucursal" href="javascript:" role="tab" data-cl="<?php echo $cliente->idcl;?>"><i class="icofont icofont-ui-user"></i>Sucursales</a></div></li>
</ul>
			<div class="list-group">
				<div class="list-group-item" style="max-width:100%">
					<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
					<div class="row">
						<div class="form-group">
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-sm-2 col-xs-12 form-control-label">Fotografía:</label></div>
							<div class="col-sm-10 col-xs-12">
								<div class="input-group">
									<input class="form-control form-control-xs" id="new_file1" type="file" placeholder='Seleccione fotografia'>
									<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
								</div>
							</div>
						</div><i class="clearfix"></i>
						<div class="form-group">
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> NIT:</label></div>
							<div class="col-sm-4 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<input class="form-control form-control-xs" id="new_nit1" type="number" placeholder='NIT/CI' min="0" max="9999999999999999999999999" value="<?php echo $cliente->nit;?>">
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Razón social:</label></div>
							<div class="col-sm-4 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<input class="form-control form-control-xs" id="new_raz1" type="text" placeholder='Razón Social' maxlength="90" value="<?php echo $cliente->razon;?>">
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
						</div><i class="clearfix"></i>
						<div class="form-group">
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Gerente:</label></div>
							<div class="col-sm-10 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<input class="form-control form-control-xs" id="new_res1" type="text" placeholder='Nombre del gerente' maxlength="150" value="<?php echo $cliente->gerente;?>">
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
						</div><i class="clearfix"></i>
						<div class="form-group">
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Telf./Cel.:</label></div>
							<div class="col-sm-4 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<input class="form-control form-control-xs" id="new_tel1" type="number" placeholder='Número de teléfono o celular' min='0' value="<?php echo $cliente->telefono;?>">
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Sitio web:</label></div>
							<div class="col-sm-4 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<input class="form-control form-control-xs" id="new_web1" type="text" placeholder='http://wwww.ejemplo.com.bo' maxlength="150"  value="<?php echo $cliente->url;?>">
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help10;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
						</div><i class="clearfix"></i>
						<div class="form-group">
							<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
							<div class="col-sm-10 col-xs-12">
								<form onsubmit="return save_cliente()">
									<div class="input-group">
										<textarea class="form-control form-control-xs" id="new_obs1" placeholder='Observaciónes del cliente' maxlength="500" rows="3"><?php echo $cliente->observacion;?></textarea>
										<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
									</div>
								</form>
							</div>
						</div><i class="clearfix"></i>	
					</div>
				</div>
			</div>
<script>Onfocus("new_nit1");$('[data-toggle="popover"]').popover({html:true});$(".config_cliente<?php echo $rand;?>").config_cliente();$(".view_sucursal").view_sucursal();</script>