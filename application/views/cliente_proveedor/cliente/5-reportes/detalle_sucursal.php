<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$img="default.png";
	if($sucursal->fotografia!=NULL && $sucursal->fotografia!=''){$img=$sucursal->fotografia;}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 col-xs-12">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th">NIT</th>
					<td class="celda td" colspan="3"><?php echo $cliente->nit;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Razon Social:</th><td class="celda td" colspan="3"><?php echo $cliente->razon;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Gerente:</th><td class="celda td" colspan="3"><?php echo $cliente->gerente;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Telf/Cel.:</th><td class="celda td" colspan="3"><?php echo $cliente->telefono;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Sitio Web:</th><td class="celda td" colspan="3"><a href="<?php echo $cliente->url;?>" target="__black"><?php echo $cliente->url;?></a></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Observaciones del cliente:</th><td class="celda td" colspan="3"><?php echo $cliente->observacion;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">NIT sucursal</th>
					<?php $nit=$cliente->nit; if($sucursal->nit!="" && $sucursal->nit!=NULL){ $nit.=" - ".$sucursal->nit;}?>
					<td class="celda td" colspan="3"><?php echo $nit;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Nombre de sucursal:</th><td class="celda td" colspan="3"><?php echo $cliente->razon." - ".$sucursal->nombre;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Responsable de sucursal:</th><td class="celda td" colspan="3"><?php echo $sucursal->responsable;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Telf/Cel. de sucursal:</th><td class="celda td" colspan="3"><?php echo $sucursal->telefono;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Correo electronico:</th><td class="celda td" colspan="3"><?php echo $sucursal->email;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Dirección:</th><td class="celda td" colspan="3"><?php echo $sucursal->direccion;?></td>
				</tr>
			</table>
		</div>
	</div>
</div>