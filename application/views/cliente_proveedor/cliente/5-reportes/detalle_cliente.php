<?php 
	$cliente=$cliente[0];
	$url=base_url().'libraries/img/personas/miniatura/';
	$img="default.png";
	if($cliente->fotografia!=NULL && $cliente->fotografia!=''){$img=$cliente->fotografia;}
	$sucursales=$this->M_sucursal_cliente->get_row("idcl",$cliente->idcl);
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 col-xs-12">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th">NIT</th>
					<td class="celda td" colspan="3"><?php echo $cliente->nit;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Razon Social:</th><td class="celda td" colspan="3"><?php echo $cliente->razon;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Gerente:</th><td class="celda td" colspan="3"><?php echo $cliente->gerente;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Telf/Cel.:</th><td class="celda td" colspan="3"><?php echo $cliente->telefono;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Sitio Web:</th><td class="celda td" colspan="3"><a href="<?php echo $cliente->url;?>" target="__black"><?php echo $cliente->url;?></a></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Sucursales:</th><td class="celda td" colspan="3">
						<ul class="list-group" style="font-size:10px; padding: 1px">
					<?php for ($j=0; $j < count($sucursales) ; $j++) { $sucursal=$sucursales[$j]; ?>
							<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px; width:100%;"><?php echo $cliente->razon." - ".$sucursal->nombre; ?></li>
					<?php }?>
						</ul>
					</td>
				</tr>
				<tr class="fila">
					<th class="celda th">Observaciones:</th><td class="celda td" colspan="3"><?php echo $cliente->observacion;?></td>
				</tr>
			</table>
		</div>
	</div>
</div>