<?php $url=base_url().'libraries/img/';
	$j_clientes=json_encode($clientes);
?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class="img-thumbnail-60"><div class="img-thumbnail-60"></div> #Item</th>
			<th class="celda-sm-15 hidden-sm">NIT/CI</th>
			<th class="celda-sm-30">Nombre o Razon Social</th>
			<th style="width:20%" class="hidden-sm">Nombre de gerente</th>
			<th style="width:10%" class="hidden-sm">Teléfono/Celular</th>
			<th style="width:25%">Sucursales</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if(count($clientes)>0){
	for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i];
			$img="sistema/miniatura/default.jpg";
			if($cliente->fotografia!=NULL && $cliente->fotografia!=''){$img="personas/miniatura/".$cliente->fotografia;}
	?>
		<tr data-cl="<?php echo $cliente->idcl;?>">
			<td>
				<div id="item"><?php echo $i+1;?></div>
				<div class="img-thumbnail-60">
					<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $cliente->razon;?>" data-desc="<br>">
				</div>
			</td>
			<td class="hidden-sm"><?php echo $cliente->nit;?></td>
			<td><?php echo $cliente->razon;?></td>
			<td class="hidden-sm"><?php echo $cliente->gerente;?></td>
			<td class="hidden-sm"><?php echo $cliente->telefono;?></td>
			<td>
			<?php $sucursales=$this->M_sucursal_cliente->get_row('idcl',$cliente->idcl);?>
			<div class="list-group" style="font-size:10px; padding: 1px">
		<?php for ($j=0; $j < count($sucursales) ; $j++) { $sucursal=$sucursales[$j]; ?>
				<a href="javascript:" class="list-group-item list-group-item-action detail_sucursal" data-sc="<?php echo $sucursal->idsc;?>" style="padding-top: 5px;padding-bottom: 5px; font-size: .75rem; width:100%;"><span class="hidden-sm"><?php echo $cliente->razon." - ";?></span><?php echo $sucursal->nombre; ?></a>
		<?php }?>
			</div>
			</td>
			<td class="text-right">
				<?php 
					$det=json_encode(array('function'=>'reportes_cliente','atribs'=> json_encode(array('cl' => $cliente->idcl)),'title'=>'Detalle'));
					$conf=""; if($privilegio->cl1u=="1"){ $conf=json_encode(array('function'=>'config_cliente','atribs'=> json_encode(array('cl' => $cliente->idcl)),'title'=>'Configurar'));}
					$del=""; if($privilegio->cl1d=="1"){ $del=json_encode(array('function'=>'confirmar_cliente','atribs'=> json_encode(array('cl' => $cliente->idcl)),'title'=>'Eliminar'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
			</td>
		</tr>
	<?php
	}}else{
		echo "<tr><td colspan='7'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<?php if($privilegio->cl1r=="1" && ($privilegio->cl1p=="1" || $privilegio->cl1c=="1")){?>
	<div id="fab-cliente">
		<div id="btns-fab-wrapper" class="btns-fab-wrapper">
		<?php if($privilegio->cl1r=="1" && $privilegio->cl1p=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-yellow-darken waves-effect waves-light print_clientes" data-tbl="table#tbl-container" data-toggle="tooltip" data-placement="left" title="Reportes">
			        <span><i class="icon-clipboard7"></i></span>
			    </button>
			</div>
		<?php }?>
		<?php if($privilegio->cl1r=="1" && $privilegio->cl1c=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-success waves-effect waves-light new_cliente" data-toggle="tooltip" data-placement="left" title="Nuevo Empleado">
			        <span>+</span>
			    </button>
			</div>
		<?php }?>
		</div>
		<button id="btn-fab-main" class="gfab-main-btn bg-danger waves-effect waves-light"><span><i class="icon-menu"></i></span></button>
	</div>
<?php }?>
<script>$("img.img-thumbnail-60").visor();$(".reportes_cliente").reportes_cliente();$("a.detail_sucursal").detail_sucursal();$(".config_cliente").config_cliente();$(".confirmar_cliente").confirmar_cliente();
<?php if($privilegio->cl1r=="1" && $privilegio->cl1c=="1" || $privilegio->cl1p=="1"){?>
	$("div#fab-cliente").jqueryFab();
	<?php if($privilegio->cl1c=="1"){?>
		$("button.new_cliente").new_cliente();
	<?php }?>
	<?php if($privilegio->cl1p=="1"){?>
		$("button.print_clientes").print_clientes();
	<?php }?>
<?php }?>
</script>