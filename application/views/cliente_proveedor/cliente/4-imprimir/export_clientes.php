<?php
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Clientes-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Clientes-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/';
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>REPORTE DE CLIENTES</h3></center>
<table border="1" cellpadding="5" cellspacing="0">
	<thead>
		<tr class="fila">
			<th>#</th>
		<?php if($file=="doc"){?>
			<th>Fotografía</th>
		<?php }?>
			<th>NIT/CI</th>
			<th>Nombre o razón social</th>
			<th>Responsable</th>
			<th>Telf./Cel.</th>
			<th>Sitio Web</th>
			<th>Sucursales</th>
			<th>Observaciónes</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i < count($clientes); $i++) { $cliente=$clientes[$i];
		$sucursales=$this->lib->select_from($all_sucursales,"idcl",$cliente->idcl);
		$img='sistema/miniatura/default.jpg';
		if($cliente->fotografia!=NULL && $cliente->fotografia!=""){ $img="personas/miniatura/".$cliente->fotografia;}
?>	
		<tr>
			<td><?php echo $i+1;?></td>
		<?php if($file=="doc"){?>
			<td><img src="<?php echo $url.$img;?>" style="width: 50px;"></td>
		<?php }?>
			<td><?php echo $cliente->nit;?></td>
			<td><?php echo $cliente->razon;?></td>
			<td><?php echo $cliente->gerente;?></td>
			<td><?php echo $cliente->telefono;?></td>
			<td><?php echo $cliente->url;?></td>
			<td>
			<?php if(count($sucursales)>0){?>
				<table border="1" cellpadding="5" cellspacing="0">
				<?php for($j=0; $j < count($sucursales); $j++){$sucursal=json_decode($sucursales[$j]);?>
					<tr><td><?php echo $cliente->razon;?> - <?php echo $sucursal->nombre;?></td></tr>
				<?php }//end for ?>
				</table>
			<?php }//end if ?>	
			</td>
			<td><?php echo $cliente->observacion;?></td>
		</tr>
<?php }?>
	</tbody>
</table>