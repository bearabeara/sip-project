<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Materiales indirecto','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
			<?php $v_menu="";
				if($privilegio[0]->ot1r==1){ $v_menu.="Mat. indirectos/material_indirecto/fa fa-cube|"; }
				if($privilegio[0]->ot11r==1){ $v_menu.="Ingreso/ingreso/fa fa-long-arrow-down|"; }
				if($privilegio[0]->ot12r==1){ $v_menu.="Salidas/salida/fa fa-long-arrow-up|"; }
				if($privilegio[0]->ot2r==1){ $v_menu.="Otros materiales/otro_material/fa fa-cube|";}
				if($privilegio[0]->ot3r==1){ $v_menu.="Configuración/config/fa fa-cogs";}
			?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'material_indirecto','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>		
		<div id="search"></div>
		<div id="contenido"></div>
	</body>
	<?php $this->load->view('estructura/js',['js'=>$dir.'material_indirecto'.$min.'.js']);?>
	<?php 
		$title="";$activo="";$search="";$view="";
		switch($pestania){
			case '1': if($privilegio[0]->ot1r==1){ $title="Materiales indirectos"; $activo='material_indirecto'; $search="material_indirecto/search_material"; $view="material_indirecto/view_material";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '2': if($privilegio[0]->ot11r==1){ $title="Ingresos de materiales indirectos"; $activo='ingreso'; $search="material_indirecto/search_ingreso"; $view="material_indirecto/view_ingreso";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '3': if($privilegio[0]->ot12r==1){ $title="Salidas de materiales indirectos"; $activo='salida'; $search="material_indirecto/search_salida"; $view="material_indirecto/view_salida";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '4': if($privilegio[0]->ot2r==1){ $title="Otros materiales"; $activo='otro_material'; $search="material_indirecto/search_otro_material"; $view="material_indirecto/view_otro_material";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
			case '5': if($privilegio[0]->ot3r==1){ $title="Configuración"; $activo='config'; $search=""; $view="material_indirecto/view_config";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
			default: $title="404"; $view=base_url()."login/error"; break;
		}
	?>
		<script>$(this).get_2n('<?php echo $search;?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','material_indirecto?p=<?php echo $pestania;?>');</script>
</html>