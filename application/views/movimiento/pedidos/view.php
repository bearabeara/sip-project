<?php 
	$productos=$this->M_producto->get_search(NULL,NULL,NULL);
	$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
	$productos_imagenes=$this->M_producto_imagen->get_all();
	$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
	$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
	$order_by="codigo";//codigo o posicion
	$detalles_partes_pedido=$this->M_parte_pedido->get_detalle(NULL,NULL);
	$descuentos=$this->M_descuento_producto->get_search(NULL,NULL);
	$sucursales_cliente=$this->M_sucursal_cliente->get_complet(NULL,NULL);
	$usuarios=$this->M_usuario->get_search(NULL,NULL);
	$partes=$this->M_parte_pedido->get_search(NULL,NULL);
?>
	<table class="table table-bordered table-hover" id="tbl-container">
		<thead>
			<tr>
				<th width="4%">#</th>
				<th width="10%" class="hidden-sm">Fecha de Pedido</th>
				<th width="10%" class="hidden-sm">N° Pedido</th>
				<th width="16%">Nombre de pedido</th>
				<th width="20%" class="hidden-sm">Cliente</th>
			<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){ ?>
				<th width="9%" class="hidden-sm">Total<br>costo (Bs.)</th>
				<th width="9%" class="hidden-sm">Total<br>descuento (Bs.)</th>
				<th width="9%">Total<br>final (Bs.)</th>
				<th width="9%" class="hidden-sm">Cancelado<br>(Bs.)</th>
				<th width="9%">Saldo<br>(Bs.)</th>
			<?php } ?>
				<th width="11%" class="hidden-sm">Estado<br>prod.</th>
				<th width="2%"></th>
			</tr>
		</thead>
		<tbody>
	<?php if(count($pedidos)>0){
			$cont=0;
	?>
		<?php for($i=0;$i<count($pedidos); $i++){ $pedido=$pedidos[$i];
				$j_partes=$this->lib->select_from($partes,"idpe",$pedido->idpe);
				$j_detalles_partes_pedido=$this->lib->select_from($detalles_partes_pedido,"idpe",$pedido->idpe);
				$j_descuentos=$this->lib->select_from($descuentos,"idpe",$pedido->idpe);
				$j_sucursales_cliente=$this->lib->select_from($sucursales_cliente,"idcl",$pedido->idcl);
				$v_productos=$this->lib->descuentos_pedidos($j_partes,$j_detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
				$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$j_descuentos,$j_sucursales_cliente,$usuarios);
				/*end para descuento*/ 
				$descuentos_producto=json_decode(json_encode($v_productos));
				$descuento=0;
				foreach ($descuentos_producto as $key => $descuento_producto) {
					$descuento+=($descuento_producto->descuento*1);
				}
			 	$control="true";
			 	if($atrib!="" && $val!="" && $type_search!=""){
			 		$control=$this->lib->search_where($pedido,$atrib,$val,$type_search);
			 	}
				$s_pagos=$this->lib->select_from($pagos,'idpe',$pedido->idpe);
				$cancelado=0;
				for($j=0; $j<count($s_pagos); $j++){$pago=$s_pagos[$j];
					$cancelado+=($this->lib->desencriptar_num($pago->monto_bs)*1);
				}
				if($atrib!="" && $val!="" && $type_search!=""){if($control!=null){$cont++;}}else{$cont++;}
		?>
			<tr<?php if($atrib!="" && $val!="" && $type_search!=""){if($control==null){?> style="display: none"<?php }}?> data-pe="<?php echo $pedido->idpe;?>">
				<td><?php echo $cont;?></td>
				<td class="hidden-sm"><?php echo $this->lib->format_date($pedido->fecha,'dl ml Y');?></td>
				<td class="hidden-sm" data-col="3">
					<?php if($atrib=="idpe" && $val!="" && $control!=null){
					    echo $this->lib->str_replace_all($pedido->orden,$this->lib->all_minuscula($val),"mark");
					}else{
					    echo $pedido->orden;
					}?>
				</td>
				<td data-col="4">
					<?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($pedido->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $pedido->nombre;}?>
				</td>
			<td class="hidden-sm" data-col="5" data-value="<?php echo $pedido->idcl;?>"><?php if($atrib=="idcl" && $val!="" && $control!=null){?><mark><?php }?><?php echo $pedido->razon;?><?php if($atrib=="idcl" && $val!="" && $control!=null){?></mark></mark><?php }?></td>
			<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){
					$costo_venta=0;
					$detalles=$this->lib->select_from($partes_detalles,"idpe",$pedido->idpe);
					foreach($detalles as $key => $detalle){
						//$detalle=json_decode($detalle);
						$costo_venta+=(($this->lib->desencriptar_num($detalle->cv)."")*1);
					}
					$costo_venta=number_format($costo_venta,1,'.','')*1;
					$cancelado=number_format($cancelado,1,'.','')*1;
					$saldo=($costo_venta-$descuento)-$cancelado;
					
			?>
				<td class="text-right hidden-sm"><?php echo number_format($costo_venta,2,'.',',');?></td>
				<td class="text-right hidden-sm"><?php echo number_format($descuento,2,'.',',');?></td>
				<td class="text-right"><ins><?php echo number_format($costo_venta-$descuento,2,'.',',');?></ins></td>
				<td class="text-right hidden-sm"><?php echo number_format($cancelado,2,'.',',');?></td>
				<td class="text-right"><?php echo number_format($saldo,2,'.',',');?>
					<span class="visible-sm">
						<?php if($pedido->estado==0){?><span class="label label-danger label-md">Pendiente</span><?php }?>
						<?php if($pedido->estado==1){?><span class="label label-info label-md">En producción</span><?php }?>
						<?php if($pedido->estado==2){?><span class="label label-success label-md">Entregado</span><?php }?>
					</span>
				</td>
			<?php }?>
				<td class="hidden-sm"><?php if($pedido->estado==0){?><span class="label label-danger label-md">Pendiente</span><?php }?>
					<?php if($pedido->estado==1){?><span class="label label-info label-md">En producción</span><?php }?>
					<?php if($pedido->estado==2){?><span class="label label-success label-md">Entregado</span><?php }?>
					<span data-col="11" data-value="<?php echo $pedido->estado;?>"></span>
				</td>
				<td class="celda td text-right">
				<?php
					$det=json_encode(array('function'=>'reporte_pedido','atribs'=> json_encode(array('pe' => $pedido->idpe)),'title'=>'Detalle'));
					$conf=""; if($privilegio->mo1u=="1"){$conf=json_encode(array('function'=>'config_pedido','atribs'=> json_encode(array('pe' => $pedido->idpe)),'title'=>'Configurar'));}
					$del=""; if($privilegio->mo1d=="1"){ $del=json_encode(array('function'=>'confirmar_pedido','atribs'=> json_encode(array('pe' => $pedido->idpe)),'title'=>'Eliminar'));}
					$this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);
				?>
				</td>
			</tr>
		<?php } ?>
	<?php }else{
			echo "<tr><td colspan='11'><h2>0 Registros encontrados...</h2></td></tr>";
		} ?>
		</tbody>
	</table>
	<?php
        $report = "";
        $help = "";
		if($privilegio->mo1r=="1" && ($privilegio->mo1p=="1" || $privilegio->mo1c=="1")){
			if($privilegio->mo1p=="1"){$report=json_encode(array('function'=>'print_pedidos','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
			if($privilegio->mo1c=="1"){$new=json_encode(array('function'=>'new_pedido','title'=>'Nuevo pedido'));}
			if($privilegio->mo1p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
			$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
		}
	?>
<script>$(".reporte_pedido").reporte_pedido();$(".config_pedido").config_pedido();$("button.confirmar_pedido").confirmar_pedido();if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}</script>