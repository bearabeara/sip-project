<?php
	$help1='title="Número de pedido" data-content="Representa al número de referencia del pedido, este numero es unico y <strong>generado automaticamente por el sistema</strong>, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help15='title="Nombre de pedido" data-content="Representa al nombre del pedido, el contenido debe ser en formato alfanumérico <b>de 3 a 150 caracteres puede incluir espacios</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help2='title="Cliente" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="Fecha de pedido" data-content="Ingrese la fecha de registro de este pedido."';
	$help31='title="Costo de pedido" data-content="Es el costo total del pedido segun el costo de venta de cada producto, este costo es <strong>generado automaticamente</strong> por el sistema."';
	$help32='title="Descuento al costo del pedido" data-content="Ingrese el monto de descuento que se relizara al <strong>Costo de pedido</strong>."';
	$help33='title="Costo final del pedido" data-content="Es el costo que representa al pedido, el valor de este costo es el resultado de la diferencia en entre el <strong>Costo de pedido</strong> y el <strong>Descuento al costo de pedido</strong>"';
	$help4='title="Observaciónes del pedido" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 300 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Número de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<div class="form-control form-control-xs disabled" id="p_nro"><?php echo $nro_pedido;?></div>
							<span class="input-group-addon form-control-sm" id="p_act"  title="Actualizar número de pedido" onclick="refresh_nro_orden();"><i class="fa fa-refresh"></i></span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Nombre de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_nom" type="text" placeholder='Nombre de pedido' maxlength="150">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help15;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Cliente:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<select id="p_cli" class="form-control form-control-xs">
								<option value="">Seleccionar...</option>
								<?php 
								for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
								<option value="<?php echo $cliente->idcl; ?>"><?php echo $cliente->razon;?></option>
								<?php } ?>
							</select>
							<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Fecha de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<input type="date" class="form-control form-control-xs" value="<?php echo date('Y-m-d');?>" id="p_fec">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>

			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"> Costo de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input type="number" class="form-control form-control-xs" id="p_cos" disabled="disabled" value="0.0" />
						<span class="input-group-addon form-control-sm">Bs.</span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help31;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Descuento al costo de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_des" type="number" placeholder='0,0' min="0" max="999999,9" value="0.0" oninput="total_costo_venta();" onkeyup="total_costo_venta();" onwheel="total_costo_venta();" disabled="disabled">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help32;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Costo final del pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_tot" type="number" disabled="disabled" value="0.0">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help33;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"></div>
				<div class="col-sm-4 col-xs-12">
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_pedido()">
						<div class="input-group">
							<textarea class="form-control form-control-xs" id="p_obs" placeholder="Observaciónes de pedido" maxlength="300" rows="3"></textarea>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row"><div class="form-group col-xs-12">
			<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true"></div>
		</div></div><br>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light view_producto" id="view_producto" data-type="new"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar producto</span></button>
		</div>
	</div>
</div>
<script>Onfocus("p_nom");$('[data-toggle="popover"]').popover({html:true}); $(".view_producto").view_producto();$("#list_product").sortable({out:function(event,ui){$(document).renumber_items("div#list_product div.accordion-panel a.accordion-msg div.item");},placeholder: "ui-state-highlight"});$("input#p_des").costo_venta();</script>