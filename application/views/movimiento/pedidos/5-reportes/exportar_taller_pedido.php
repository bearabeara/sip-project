<?php 
	$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$fecha=date("Y-m-d");
	$nombre=$pedido->nombre."-".$v[$parte_pedido->tipo]."-".$parte_pedido->numero;
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Pedido-$nombre-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Pedido-$nombre-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
?>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1p==1){?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<h3><?php echo $nombre;?></h3>
<table cellspacing="0" cellpadding="3" border="1">
	<?php $cpro=1; foreach($productos as $key => $producto){
		$grupos=json_decode(json_encode($producto->categorias));
		$tot_suc=[];
		$tot=0;for($i=0;$i<count($sucursales);$i++){ $tot_suc[$i]=0;}
	?>
	<tbody>
		<tr>
			<td width="54px" rowspan="<?php echo count($producto->categorias)+2;?>"><?php echo $cpro++;?></td>
			<td width="54px" rowspan="<?php echo count($producto->categorias)+2;?>">
			<?php if($file=="doc"){?>
				<img src="<?php echo $producto->fotografia;?>" width="54px;">
			<?php }?>
			</td>
			<td><strong><?php echo $this->lib->all_mayuscula($producto->codigo." - ".$producto->nombre); ?></strong></td>
		<?php for($s=0;$s<count($sucursales);$s++){ ?>
			<td><strong><?php echo $this->lib->ini_mayuscula($sucursales[$s]->nombre);?></strong></td>
		<?php } ?>
			<td><strong>Total [u.]</strong></td>
		</tr>
			<?php foreach ($grupos as $key => $grupo){ ?>
				<tr>
					<td><?php echo $grupo->nombre;?>
						<?php if($grupo->observacion!="" && strpos($grupo->observacion, "Precio") === false){?><span style="color: rgba(255, 0, 0, .7);">(<?php echo $grupo->observacion; ?>)</span><?php }?>
					</td>
				<?php $sub_total=0;
					for($i=0;$i<count($sucursales);$i++){ $sucursal=$sucursales[$i];
						$cantidad=0;
						$j_sucursales=json_decode(json_encode($grupo->sucursales));
						foreach($j_sucursales as $key => $suc){
							if($suc->idsc==$sucursal->idsc){ $cantidad=$suc->cantidad; break; }
						}
						$sub_total+=($cantidad*1);
				?>
					<td style="text-align: right;"><?php if($cantidad>0){ echo $cantidad; $tot_suc[$i]+=($cantidad*1);} ?></td>
				<?php } ?>
					<td style="text-align: right;"><?php echo $sub_total; $tot+=$sub_total; ?></td>
				</tr>
			<?php } ?>
				<tr>
					<td style="text-align: right;"><strong>TOTAL</strong></td>
				<?php for($s=0;$s<count($sucursales);$s++){ ?>
					<td style="text-align: right;"><strong><?php if($tot_suc[$s]>0){ echo $tot_suc[$s];}?></strong></td>
				<?php } ?>
					<td style="text-align: right;"><strong><?php if($tot>0){echo $tot;}?></strong></td>
				</tr>
			<?php } ?>
	</tbody>
</table>
<?php }?>