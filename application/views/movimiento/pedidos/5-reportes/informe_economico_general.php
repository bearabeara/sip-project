<?php 
$url=base_url().'libraries/img/';
$v_productos_grupos_colores=[];
$v_aux=[];
$v_productos_grupos=[];
for($i=0;$i < count($productos_grupos_colores); $i++){ $pgc=$productos_grupos_colores[$i];
	$detalle=$this->lib->search_elemento($detalle_pedido,'idpgrc',$pgc->idpgrc);
	if($detalle!=null){
		$v_aux[]=$pgc->idp.'-'.$pgc->idpgr;
		$nombre=$pgc->nombre;$codigo=$pgc->codigo;
		$nombre_g="";$abr_g="";$nombre_c="";$abr_c="";
		if($pgc->abr_g!=""){$nombre.=" - ".$pgc->nombre_g;$codigo.="-".$pgc->abr_g;}
		$nombre_c=$pgc->nombre_c;
		$codigo.="-".$pgc->abr_c;
		$p_img="sistema/miniatura/default.jpg";
		if($pgc->portada!="" && $pgc->portada!=null){
			$v=explode("|", $pgc->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
			}
		}else{
			if($pgc->portada_producto!="" && $pgc->portada_producto!=null){
				$v=explode("|", $pgc->portada_producto);
				if(count($v==2)){
					if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
					if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
				}
			}
		}
		$costo_total=0;
		$cantidad_total=0;
		//sumando el productos en todas las partes
		for ($p=0; $p < count($partes) ; $p++) { $parte=$partes[$p];
			$detalle_parte=$this->lib->search_elemento_2n($detalle_pedido,'idpgrc',$detalle->idpgrc,'idpp',$parte->idpp);
			if($detalle_parte!=null){
				$cantidades=$this->M_sucursal_detalle_pedido->get_row('iddp',$detalle_parte->iddp);
				$cant=0;
				for ($ca=0; $ca < count($cantidades) ; $ca++) { $cant+=($this->lib->desencriptar_num($cantidades[$ca]->cantidad)*1);}
				$idpp=$detalle_parte->idpp." = ".$parte->idpp;
				$costo_total+=(($this->lib->desencriptar_num($detalle_parte->cv)."")*1);
				$cantidad_total+=($cant*1);
			}//end if
		}
		$v_productos_grupos_colores[]=array("img" => $url.$p_img,"ids"=>$pgc->idp.'-'.$pgc->idpgr,"codigo" => $codigo,"nombre" => $nombre,"codigo_c"=>$pgc->abr_c,"nombre_c"=>$pgc->nombre_c,"cu" => $pgc->costo,"cantidad" => $cantidad_total,"costo" => $costo_total);
	}
}
$v_aux=array_unique($v_aux);
foreach($v_aux as $key => $value){
	$codigo="";$nombre="";$colores=[];
	foreach($v_productos_grupos_colores as $key => $pgc){
		if(trim($value)==trim($pgc['ids'])){
			$codigo=$pgc['codigo'];
			$nombre=$pgc['nombre'];
			$colores[]=$pgc;
		}
	}
	$v_productos_grupos[]=array('codigo'=>$codigo,'nombre'=>$nombre,'colores'=>$colores);
}
$productos=json_decode(json_encode($v_productos_grupos));
$total=0;
$rand=rand(10,99999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
	<li class="nav-item"><a class="nav-link taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"> Taller </a></div></li>
	<li class="nav-item"><a class="nav-link informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
	<li class="nav-item"><a class="nav-link active informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
    <li class="nav-item"><a class="nav-link informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
	<li class="nav-item"><a class="nav-link informe_materiales" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php
		$fun = array('function' => 'informe_economico_pedido_general', 'atribs' => array('pe' => $pedido->idpe));
		$excel = array('controller' => 'movimiento/exportar_pedido_informe_general?pe='.$pedido->idpe.'&file=xls');
		$word = array('controller' => 'movimiento/exportar_pedido_informe_general?pe='.$pedido->idpe.'&file=doc');
		$default = array('top' => 1,'right' => 1,'bottom' => 1,'left' => 1,'page' => 1);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10 table-hover">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda" colspan="<?php echo 5+(count($partes)*3);?>" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'DETALLE ECONÓMICO GENERAL','sub_titulo'=>'PEDIDO: '.$pedido->nombre]);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4">#</th>
								<th class="celda th padding-4" width="75%">Producto</th>
								<th class="celda th padding-4" width="10%">Cantidad [u.]</th>
								<th class="celda th padding-4" width="15%">Total [Bs.]</th>
							</tr>
						</thead>
						<tbody>
						<?php $cont=1;
							foreach ($productos as $key => $producto){
								$cantidad=0;
								$costo=0;
								$colores=json_decode(json_encode($producto->colores));
								foreach($colores as $key => $color){
									$cantidad+=$color->cantidad;
									$costo+=$color->costo;
								}
								$costo=number_format($costo,1,'.','')*1;
								$descuento=number_format($descuento,1,'.','')*1;
						?>
							<tr class="fila">
								<td class="celda td padding-4"><?php echo $cont++; ?></td>
								<td class="celda td padding-4"><?php echo $producto->codigo." ".$producto->nombre;?></td>
								<td class="celda td padding-4 text-right"><?php echo $cantidad; ?></td>
								<td class="celda td padding-4 text-right"><?php echo number_format($costo,2,'.',','); $total+=$costo; ?></td>
							</tr>
						<?php }// end for ?>
						<?php $total=number_format($total,1,'.','');?>
						<tr class="fila">
							<th class="celda td padding-4 text-right" colspan="3">TOTAL (Bs.)</th>
							<th class="celda td padding-4 text-right" style="text-align: right;"><?php echo number_format($total,2,'.',','); ?></th>
						</tr>
						<tr class="fila">
							<th class="celda td padding-4 text-right" colspan="3">DESCUENTO (Bs.)</th>
							<th class="celda td padding-4 text-right" style="text-align: right;"><?php echo number_format($descuento,2,'.',','); ?></th>
						</tr>
						<tr class="fila">
							<th class="celda td padding-4 text-right" colspan="3">TOTAL PEDIDO (Bs.)</th>
							<th class="celda td padding-4 text-right" style="text-align: right;"><?php echo number_format($total-$descuento,2,'.',','); ?></th>
						</tr>
					</tbody>
				</table>
			</div>
</div>
</div>
<script>$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$(".taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();</script>