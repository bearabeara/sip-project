<?php 
	$contPagina=1;
	$contReg=0;
	$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$url=base_url().'libraries/img/';
	//CALCULANDO EL MATERIAL NECESARIO
	$detalles_parte_pedido=$this->lib->select_from($detalles_pedido,'idpp',$parte->idpp);
	$v_material = array();
	for($i=0;$i<count($detalles_parte_pedido);$i++){$detalle_parte_pedido=$detalles_parte_pedido[$i];
		/*buscando material en producto*/
		$pgc=$this->lib->search_elemento($productos_grupos_colores,"idpgrc",$detalle_parte_pedido->idpgrc);
		if($pgc!=null){
			$pg=$this->lib->search_elemento($productos_grupos,"idpgr",$pgc->idpgr);
			if($pg!=null){
				//buscando cantidad pedida
				$cantidad=0;
				$sucursales=$this->lib->select_from($sucursales_detalles_pedidos,'iddp',$detalle_parte_pedido->iddp);
				for($j=0; $j<count($sucursales);$j++){ 
					$cantidad+=($this->lib->desencriptar_num($sucursales[$j]->cantidad)*1);
				}
				//buscando materiales de pila de productos
				$producto=$this->M_producto->get($pg->idp);
				$materiales=$this->lib->select_from($productos_materiales,"idp",$pg->idp);
				for($j=0; $j<count($materiales); $j++){$material=$materiales[$j];
					$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
					if($unidad!=null){
						$img="sistema/miniatura/default.jpg";
						if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
						$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
					}
				}
				$materiales=$this->lib->select_from($productos_grupos_materiales,"idpgr",$pg->idpgr);
				for($j=0; $j<count($materiales); $j++){$material=$materiales[$j];
					$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
					if($unidad!=null){
						$img="sistema/miniatura/default.jpg";
						if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
						$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
					}
				}
				$materiales=$this->lib->select_from($productos_colores_materiales,"idpgrc",$pgc->idpgrc);
				for($j=0; $j<count($materiales); $j++){$material=$materiales[$j];
					$unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
					if($unidad!=null){
						$img="sistema/miniatura/default.jpg";
						if($material->fotografia!="" && $material->fotografia!=NULL){ $img="materiales/miniatura/".$material->fotografia;}
						$v_material[]= array('idmi'=>$material->idmi,'nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $url.$img,'color' => $material->nombre_c,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad,'cantidad_pedida' =>$cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$unidad->nombre,'abr_medida' => $unidad->abr);
					}
				}
			}
		}
	}
	$materiales_productos=json_decode(json_encode($v_material));
	$total_materiales=array();
	for($i=0;$i<count($all_materiales);$i++){$all_material=$all_materiales[$i];
		$cantidad=0;
		$aux=array();
		foreach ($materiales_productos as $key => $producto_material) {
			if($all_material->idmi==$producto_material->idmi){
				$cantidad+=($producto_material->cantidad_pedida*1*$producto_material->cantidad_por_producto);
				$aux=$producto_material;
			}
		}
		if($cantidad>0){
			$material=json_decode(json_encode($aux));
			$total_materiales[]=array('nombre'=>$material->nombre,'codigo'=>$material->codigo,'idm' => $material->idm,'idmi' => $material->idmi,'imagen' => $material->imagen,'color' => $material->color,'codigo_c'=>$material->codigo_c,'cantidad_por_producto' =>$material->cantidad_por_producto,'cantidad_necesaria' =>$cantidad,'cantidad_almacenes' =>$all_material->cantidad,'verificado'=>$material->verificado,'observacion' => $material->observacion,'medida' =>$material->medida,'abr_medida' => $material->abr_medida);
		}
	}

	$total_materiales=json_decode(json_encode($total_materiales));
	//CALCULANDO MATERIAL FALTANTE
	$rand=rand(10,9999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
	<li class="nav-item"><a class="nav-link taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"> Taller </a></div></li>
	<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
	    <li class="nav-item"><a class="nav-link informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
	    <li class="nav-item"><a class="nav-link informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
        <li class="nav-item"><a class="nav-link informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
	<?php }?>
	<li class="nav-item"><a class="nav-link informe_materiales active" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
		<?php
			if(count($partes)>0){
				for($i=0;$i<count($partes);$i++){ $p=$partes[$i];
					$seleccionado=false;
					if($p->idpp==$parte->idpp){
						$seleccionado=true;
					}
		?>
		<button type="button" class="btn btn-<?php if(!$seleccionado){?>inverse-<?php }?>info btn-mini waves-effect informe_materiales" data-pe="<?php echo $pedido->idpe;?>" data-pp="<?php echo $p->idpp;?>" style="margin-bottom: 5px;"><?php echo $v[$p->tipo]." ".$p->numero;?></button>
		<?php 	}
			}
		?>
		<?php
		$fun = array('function' => 'informe_materiales', 'atribs' => array('pe' => $pedido->idpe,'pp' => $parte->idpp));
		$excel = array('controller' => 'movimiento/exportar_materiales_parte_pedido?pe='.$pedido->idpe."&pp=".$parte->idpp.'&file=xls');
		$word = array('controller' => 'movimiento/exportar_materiales_parte_pedido?pe='.$pedido->idpe."&pp=".$parte->idpp.'&file=doc');
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
		?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 5%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Código</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 41%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Material</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Color</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 14%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cantidad en almacenes</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 14%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cantidad necesaria</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Diferencia</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 13%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Stock</small></label>
								</div>
							</th>
						</tr>
			</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'MATERIALES NECESARIOS','sub_titulo'=>'PEDIDO: '.$pedido->nombre]);?>
								</td>
							</tr>
							<tr class="fila">
								<th class='celda th padding-4' data-column="1" style="display: none" width="1%">#</th>
								<th class='celda th padding-4 img-thumbnail-40' data-column="2">Fotografía</th>
								<th class='celda th padding-4' data-column="3" width="10%">Código</th>
								<th class='celda th padding-4' data-column="4" width="45%">Material</th>
								<th class='celda th padding-4' data-column="5" width="10%">Color</th>
								<th class='celda th padding-4' data-column="6" width="15%">Cantidad el almacenes</th>
								<th class='celda th padding-4' data-column="7" width="15%">Cantidad el nesesaria</th>
								<th class='celda th padding-4' data-column="8" width="10%">Diferencia</th>
								<th class='celda th padding-4' data-column="9" width="5%">Stock</th>
							</tr>
						</thead>
						<tbody>
						<?php $cont=0;
							foreach($total_materiales as $key=>$material){ $cont++;
						?>
							<tr class="fila">
								<td class="celda td padding-4" data-column="1" style="display: none"><?php echo $cont;?></td>
								<td class="celda td img" data-column="2">
									<div class="item"><?php echo $cont;?></div>
									<img src="<?php echo $material->imagen;?>" class="img-thumbnail-40">
								</td>
								<td class="celda td padding-4" data-column="3"><?php echo $material->codigo;?></td>
								<td class="celda td padding-4" data-column="4"><?php echo $material->nombre;?></td>
								<td class="celda td padding-4" data-column="5"><?php echo $material->color;?></td>
								<td class="celda td padding-4 text-right" data-column="6"><?php echo $material->cantidad_almacenes." ".$material->abr_medida.".";?></td>
								<td class="celda td padding-4 text-right" data-column="7"><?php echo $material->cantidad_necesaria." ".$material->abr_medida.".";?></td>
								<td class="celda td padding-4 text-right" data-column="8"><?php echo ($material->cantidad_almacenes-$material->cantidad_necesaria)." ".$material->abr_medida.".";?></td>
								<td class="celda td padding-4" data-column="9"><span class="label label-inverse-<?php if($material->cantidad_almacenes<$material->cantidad_necesaria){?>danger<?php }else{?>success<?php } ?>"><?php if($material->cantidad_almacenes<$material->cantidad_necesaria){?>insuficiente<?php }else{?>suficiente<?php } ?></span></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$(".taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();</script>