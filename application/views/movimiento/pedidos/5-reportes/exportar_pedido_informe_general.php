<?php 
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-informe-general.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-informe-general.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/';
	$v_productos_grupos_colores=[];
	$v_aux=[];
	$v_productos_grupos=[];
for($i=0;$i < count($productos_grupos_colores); $i++){ $pgc=$productos_grupos_colores[$i];
	$detalle=$this->lib->search_elemento($detalle_pedido,'idpgrc',$pgc->idpgrc);
	if($detalle!=null){
		$v_aux[]=$pgc->idp.'-'.$pgc->idpgr;
		$nombre=$pgc->nombre;$codigo=$pgc->codigo;
		$nombre_g="";$abr_g="";$nombre_c="";$abr_c="";
		if($pgc->abr_g!=""){$nombre.=" - ".$pgc->nombre_g;$codigo.="-".$pgc->abr_g;}
		$nombre_c=$pgc->nombre_c;
		$codigo.="-".$pgc->abr_c;
		$p_img="sistema/miniatura/default.jpg";
		if($pgc->portada!="" && $pgc->portada!=null){
			$v=explode("|", $pgc->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
			}
		}else{
			if($pgc->portada_producto!="" && $pgc->portada_producto!=null){
				$v=explode("|", $pgc->portada_producto);
				if(count($v==2)){
					if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
					if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
				}
			}
		}
		$costo_total=0;
		$cantidad_total=0;
		//sumando el productos en todas las partes
		for ($p=0; $p < count($partes) ; $p++) { $parte=$partes[$p];
			$detalle_parte=$this->lib->search_elemento_2n($detalle_pedido,'idpgrc',$detalle->idpgrc,'idpp',$parte->idpp);
			if($detalle_parte!=null){
				$cantidades=$this->M_sucursal_detalle_pedido->get_row('iddp',$detalle_parte->iddp);
				$cant=0;
				for ($ca=0; $ca < count($cantidades) ; $ca++) { $cant+=($this->lib->desencriptar_num($cantidades[$ca]->cantidad)*1);}
					$idpp=$detalle_parte->idpp." = ".$parte->idpp;
				$costo_total+=(($this->lib->desencriptar_num($detalle_parte->cv)."")*1);
				$cantidad_total+=($cant*1);
			}//end if
		}
		$v_productos_grupos_colores[]=array("img" => $url.$p_img,"ids"=>$pgc->idp.'-'.$pgc->idpgr,"codigo" => $codigo,"nombre" => $nombre,"cdoigo_c"=>$pgc->abr_c,"nombre_c"=>$pgc->nombre_c,"cu" => $pgc->costo,"cantidad" => $cantidad_total,"costo" => $costo_total);
	}
}
$v_aux=array_unique($v_aux);
foreach($v_aux as $key => $value){
	$codigo="";$nombre="";$colores=[];
	foreach($v_productos_grupos_colores as $key => $pgc){
		if(trim($value)==trim($pgc['ids'])){
			$codigo=$pgc['codigo'];
			$nombre=$pgc['nombre'];
			$colores[]=$pgc;
		}
	}
	$v_productos_grupos[]=array('codigo'=>$codigo,'nombre'=>$nombre,'colores'=>$colores);
}
$productos=json_decode(json_encode($v_productos_grupos));
$total=0;
?>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1p==1 && $privilegio->mo1u==1){?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><strong>DETALLE ECONÓMICO GENERAL</strong><br><ins>PEDIDO: <?php echo $pedido->nombre;?></ins></center>
<table border="1" cellspacing="0" cellpadding="4">
	<thead>
		<tr>
			<th>#</th>
			<th>Producto</th>
			<th>Cantidad [u.]</th>
			<th>Total [Bs.]</th>
		</tr>
	</thead>
	<tbody>
	<?php $cont=1;
		foreach ($productos as $key => $producto){
			$cantidad=0;
			$costo=0;
			$colores=json_decode(json_encode($producto->colores));
			foreach($colores as $key => $color){
				$cantidad+=$color->cantidad;
				$costo+=$color->costo;
			}
			$costo=number_format($costo,1,'.','');
	?>
			<tr>
				<td><?php echo $cont++; ?></td>
				<td><?php echo $producto->codigo." ".$producto->nombre;?></td>
				<td style="text-align: right;"><?php print_r($cantidad*1); ?></td>
				<td style="text-align: right;"><?php print_r(number_format($costo,1,',','')*1); $total+=$costo; ?></td>
			</tr>
	<?php }// end for ?>
	<?php $total=number_format($total,1,'.',''); ?>
			<tr>
				<th style="text-align: right;" colspan="3">TOTAL (Bs.)</th>
				<th style="text-align: right;" style="text-align: right;"><?php print_r(number_format($total,1,',','')*1); ?></th>
			</tr>
			<tr>
				<th style="text-align: right;" colspan="3">DESCUENTO (Bs.)</th>
				<?php $descuento=$this->lib->desencriptar_num($pedido->descuento)*1; ?>
				<th style="text-align: right;" style="text-align: right;"><?php print_r(number_format($descuento,1,',','')); ?></th>
			</tr>
			<tr>
				<th style="text-align: right;" colspan="3">TOTAL PEDIDO (Bs.)</th>
				<th style="text-align: right;" style="text-align: right;"><?php print_r(number_format(($total-$descuento),1,',','')); ?></th>
			</tr>
		</tbody>
	</table>
<?php }?>