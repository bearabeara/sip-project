<?php $rand=rand(10,999999);
    $total_monto=0;
    $total_monto_bs=0;
    $total_pedido=0;
    $total_descuento=0;
    $total_recibido=0;
    $total_recibido_bs=0;
    for($i=0; $i < count($detalles_pedido); $i++){ $detalle_pedido=$detalles_pedido[$i];
        $total_pedido+=$this->lib->desencriptar_num($detalle_pedido->cv);
    }
    $total_pedido-=$descuento;
?>
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item"><a class="nav-link reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
    <li class="nav-item"><a class="nav-link taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"> Taller </a></div></li>
    <li class="nav-item"><a class="nav-link informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
    <li class="nav-item"><a class="nav-link informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
    <li class="nav-item"><a class="nav-link active informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
    <li class="nav-item"><a class="nav-link informe_materiales" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
    <div class="list-group-item" style="max-width:100%; padding-bottom: 0px !important;">
        <?php
            $fun = array('function' => 'reporte_pedido', 'atribs' => array('pe' => $pedido->idpe));
            $excel = array('controller' => 'movimiento/exportar_informe_depositos?pe='.$pedido->idpe.'&file=xls');
            $word = array('controller' => 'movimiento/exportar_informe_depositos?pe='.$pedido->idpe.'&file=doc');
            $this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
        ?>
    </div>
    <div class="list-group-item" style="max-width:100%">
        <div class="table-responsive" id="area">
            <table class="tabla tbl-bordered font-10">
                <thead>
                <tr class="fila title" style="text-align: center;">
                    <td class="celda title" colspan="15" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
                        <?php $this->load->view('estructura/print/header-print',['titulo'=>'DETALLE DE DEPÓSITOS']);?>
                    </td>
                </tr>
                <tr class="fila">
                    <th class="celda th padding-4" width="1%">#</th>
                    <th class="celda th padding-4" width="15%">Registrado por</th>
                    <th class="celda th padding-4" width="11%">Fecha de deposito</th>
                    <th class="celda th padding-4" width="11%">Depositante</th>
                    <th class="celda th padding-4" width="16%">Propietario de cuenta</th>
                    <th class="celda th padding-4" width="13%">Banco</th>
                    <th class="celda th padding-4" width="5%">N° de cuenta</th>
                    <th class="celda th padding-4" width="6%">Monto depositado</th>
                    <th class="celda th padding-4" width="6%">Tipo de cambio</th>
                    <th class="celda th padding-4" width="6%">Monto [Bs.]</th>
                    <th class="celda th padding-4" width="6%">Descuento</th>
                    <th class="celda th padding-4" width="6%">Monto recibido</th>
                    <th class="celda th padding-4" width="6%">Monto recibido [Bs.]</th>
                    <th class="celda th padding-4" width="6%">Saldo Pendiente [Bs.]</th>
                    <th class="celda th padding-4" width="19%">Observaciónes</th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($pagos)>0){?>
                    <?php for($i=0; $i<count($pagos);$i++){$pago=$pagos[$i];
                        $usuario=$this->lib->search_elemento($personas,"ci",$pago->ci_usuario);
                        if($usuario!=null){$usuario=$usuario->nombre_completo;}

                        $fecha=$pago->fecha;
                        $banco=$this->lib->search_elemento($bancos,"idba",$pago->idba);
                        if($banco!=null){
                            $banco=$banco->razon;
                        }
                        $propietario=$this->lib->search_elemento($personas,"ci",$pago->ci_persona);
                        if($propietario!=null){
                            $propietario=$propietario->nombre_completo;
                        }
                        $monto_depositado=$this->lib->desencriptar_num($pago->monto);
                        $monto_bs=$this->lib->desencriptar_num($pago->monto_bs);
                        $moneda="";
                        $tc=$this->lib->search_elemento($tipos_cambio,"idtc",$pago->idtc);
                        if($tc!=null){
                            $moneda=$tc->simbolo;
                        }
                        $descuentos=$this->lib->select_from($descuentos_pagos,"idpa",$pago->idpa);
                        $s_descuento=0;
                        for($j=0;$j<count($descuentos);$j++){
                            $descuento=json_decode(json_encode($descuentos[$j]));
                            $s_descuento+=($this->lib->desencriptar_num($descuento->monto)*1);
                        }
                        $s_descuento=number_format($s_descuento,1,'.','');
                        $recibido_bs=number_format((($monto_depositado-$s_descuento)*$pago->tipo_cambio),1,'.','');
                        ?>
                        <tr class="fila">
                            <td class="celda td padding-4"><?php echo $i+1;?></td>
                            <td class="celda td padding-4"><?php echo $usuario;?></td>
                            <td class="celda td padding-4"><?php echo $this->lib->format_date($fecha,'dl ml Y');?></td>
                            <td class="celda td padding-4"><?php echo $pago->depositante;?></td>
                            <td class="celda td padding-4"><?php echo $propietario;?></td>
                            <td class="celda td padding-4"><?php echo $banco;?></td>
                            <td class="celda td padding-4"><?php echo $pago->cuenta;?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format($monto_depositado,2,'.',',').$moneda;$total_monto+=$monto_depositado;?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo $pago->tipo_cambio;?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format($monto_bs,2,'.',',')."Bs.";$total_monto_bs+=$monto_bs;?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format($s_descuento,2,'.',',').$moneda;$total_descuento+=$s_descuento; ?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format(($monto_depositado-$s_descuento),2,'.',',').$moneda;$total_recibido+=($monto_depositado-$s_descuento);?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format($recibido_bs,2,'.',',')."Bs.";$total_recibido_bs+=$recibido_bs;?></td>
                            <td class="celda td padding-4" style="text-align: right;"><?php echo number_format($total_pedido - $total_recibido_bs,2,'.',',')."Bs.";?></td>
                            <td class="celda td padding-4"><?php echo $pago->observacion;?></td>
                        </tr>
                    <?php } ?>
                <?php }else{?>
                    <tr class="fila"><th colspan="15" class="text-center"><h3>0 depositos registrados...</h3></th></tr>
                <?php }?>
                </tbody>
                <thead>
                <tr class="fila">
                    <th class="celda th padding-4" colspan="7" class="text-right">TOTAL</th>
                    <th class="celda th padding-4" class="text-right"><?php echo number_format($total_monto,2,'.',','); ?></th>
                    <th class="celda th padding-4"></th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_monto_bs,2,'.',','); ?></th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_descuento,2,'.',','); ?></th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_recibido,2,'.',','); ?></th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_recibido_bs,2,'.',','); ?></th>
                    <th class="celda th padding-4" colspan="2"></th>
                </tr>
                <tr class="fila">
                    <th colspan="12" class="celda th padding-4 text-right">TOTAL COSTO DE PEDIDO [Bs.]</th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_pedido,2,'.',','); ?></th>
                    <th class="celda th padding-4" colspan="2"></th>
                </tr>
                <tr class="fila">
                    <th colspan="12" class="celda th padding-4 text-right">SALDO PENDIENTE [Bs.]</th>
                    <th class="celda th padding-4 text-right"><?php echo number_format($total_pedido-$total_recibido_bs,2,'.',','); ?></th>
                    <th class="celda th padding-4" colspan="2"></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script>$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$("a.taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();</script>