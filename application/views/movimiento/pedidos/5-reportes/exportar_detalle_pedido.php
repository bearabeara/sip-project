<?php
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-detalle.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-detalle.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
?>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1p==1){?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>Reporte de pedido</h3></center>
<table border="1" cellspacing="0" cellpadding="4" width="70%">
	<tbody>
		<tr>
			<th>Nombre de pedido</th>
			<td><?php echo $pedido->nombre;?></td>
			<th>Nº DE PEDIDO:</th>
			<td><?php echo $pedido->orden;?></td>
		</tr>
		<tr>
			<th colspan="4">DATOS DE CLIENTE</th>
		</tr>
		<tr>
			<th>Nombre/Razon Social:</th><td><?php echo $pedido->razon;?></td>
			<th>NIT/CI:</th><td><?php echo $pedido->nit;?></td>
		</tr>
		<tr>
			<th>Sitio Web: </th><td><?php echo $pedido->url;?></td>
			<th>Observaciónes: </th><td><?php echo $pedido->observacion;?></td>
		</tr>
		<tr>
			<th colspan="4">DATOS DE PEDIDO</th>
		</tr>
		<tr>
			<th>Fecha de Pedido: </th><td><?php echo $pedido->fecha;?></td>
			<th>Cantidad de productos: </th><td><?php echo '0 Prod.';?></td>
		</tr>
		<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1p==1 && $privilegio->mo1u==1){ ?>
		<?php $costo_venta=0;$detalles=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido->idpe);
		for($j=0; $j < count($detalles); $j++){ $costo_venta+=(($this->lib->desencriptar_num($detalles[$j]->cv)."")*1);}
			?>
		<tr>
			<th>Monto de pedido (Bs.): </th><td><?php echo number_format($costo_venta,2,'.',',');?></td>
			<th>Descuento (Bs.): </th><td><?php echo number_format($this->lib->desencriptar_num($pedido->descuento)*1,2,'.',',');?></td>
		</tr>
		<tr>
			<th>Total Pedido (Bs.): </th><td><?php echo number_format($costo_venta-$this->lib->desencriptar_num($pedido->descuento),2,'.',',');?></td>
			<th></th><td></td>
		</tr>
		<?php }?>
		<tr>
			<th>Observaciónes: </th><td colspan="3"><?php echo $pedido->observacion;?></td>
		</tr>
	</tbody>
</table>
<?php }?>