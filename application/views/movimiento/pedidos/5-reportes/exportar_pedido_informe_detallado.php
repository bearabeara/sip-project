<?php 
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-informe-detallado.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Pedido-$pedido->nombre-$fecha-informe-detallado.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/'; 
	$vpartes=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$rand=rand(10,9999);
?>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1p==1 && $privilegio->mo1u==1){?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3><?php echo $pedido->nombre;?></h3></center>
<table border="1" cellspacing="0" cellpadding="4" style="font-size: 10px;">
	<thead>
		<tr class="fila">
			<th rowspan="2">#</th>
			<th rowspan="2" colspan="2">Producto</th>
			<th rowspan="2">C/U actual [Bs.]</th>
			<?php for ($i=0; $i < count($partes) ; $i++) { $v_tot_suc[$i]=0;?>
			<th colspan="3"><?php echo $vpartes[$partes[$i]->tipo]." - ".$partes[$i]->numero; ?></th>
			<?php } ?>
			<th rowspan="2">Total [Bs.]</th>
		</tr>
		<tr class="fila">
		<?php if(count($partes)>0){?>
			<?php for($i=0; $i < count($partes); $i++){ ?>
			<th>Cant. [u.]</th>
			<th>C/U venta [Bs.]</th>
			<th>Costo venta [Bs.]</th>
			<?php }?>
		<?php }?>
		</tr>
	</thead>
	<tbody>
					<?php $cont=1;$total=0;
						for($i=0;$i < count($productos_grupos_colores); $i++){ $pgc=$productos_grupos_colores[$i]; ?>
						<?php 
							$detalle=$this->lib->search_elemento($detalle_pedido,'idpgrc',$pgc->idpgrc);
							if($detalle!=null){
								$nombre=$pgc->nombre;$codigo=$pgc->codigo;
								$nombre_g="";$abr_g="";$nombre_c="";$abr_c="";
								if($pgc->abr_g!=""){$nombre.=" - ".$pgc->nombre_g;$codigo.="-".$pgc->abr_g;}
								$nombre.=" - ".$pgc->nombre_c;
								$codigo.="-".$pgc->abr_c;
								$p_img="sistema/miniatura/default.jpg";
								if($pgc->portada!="" && $pgc->portada!=null){
									$v=explode("|", $pgc->portada);
									if(count($v==2)){
										if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
										if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
									}
								}else{
									if($pgc->portada_producto!="" && $pgc->portada_producto!=null){
										$v=explode("|", $pgc->portada_producto);
										if(count($v==2)){
											if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
											if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
										}
									}
								}
								$sub_total_costo_venta=0;
								$costo_actual_venta=number_format($pgc->costo*1,1,'.','')

						?>
							<tr>
								<td><?php echo $cont++; ?></td>
								<td style="max-width: 30px; max-height: 30px">
								<?php if($file=="doc"){?>
									<img src="<?php echo $url.$p_img;?>" style="max-width: 30px; max-height: 30px">
								<?php }?>
								</td>
								<td><?php echo $codigo." ".$nombre;?></td>
								<td style="text-align: right;"><?php print_r(number_format($costo_actual_venta,1,',','')); ?></td>
							<?php if(count($partes)>0){?>
								<?php for($p=0; $p<count($partes) ;$p++){ $parte=$partes[$p];
										$cant="";$cu_venta="";$cv_venta="";$idpp="";
										$detalle_parte=$this->lib->search_elemento_2n($detalle_pedido,'idpgrc',$detalle->idpgrc,'idpp',$parte->idpp);
										if($detalle_parte!=null){
											$cantidades=$this->M_sucursal_detalle_pedido->get_row('iddp',$detalle_parte->iddp);
											$cant=0;
											for ($ca=0; $ca < count($cantidades) ; $ca++) { $cant+=($this->lib->desencriptar_num($cantidades[$ca]->cantidad)*1);}
											$idpp=$detalle_parte->idpp." = ".$parte->idpp;
											$cu_venta=$this->lib->desencriptar_num($detalle_parte->cu)."";
											$cv_venta=$this->lib->desencriptar_num($detalle_parte->cv)."";
										}//end if
										if($cant!=""){$cant=$cant*1;}
										if($cu_venta!=""){$cu_venta=number_format($cu_venta,1,'.','');}
										if($cv_venta!=""){$cv_venta=number_format($cv_venta,1,'.','');}
								 ?>

								<td style="text-align: right;"><?php if($cant!=""){ print_r($cant*1);}?></td>
								<td style="text-align: right;"><?php if($cu_venta!=""){ print_r(number_format($cu_venta,1,',',''));}?></td>
								<td style="text-align: right;<?php if((($cant*1)*($cu_venta*1)!=$cv_venta*1)){?> color: red;<?php }?>"><strong><?php if($cv_venta!=""){ print_r(number_format($cv_venta,1,',',''));$sub_total_costo_venta+=($cv_venta*1);$v_tot_suc[$p]+=($cv_venta*1);}?></strong></td>
								<?php }?>
							<?php }?>
							<?php $sub_total_costo_venta=number_format($sub_total_costo_venta,1,'.','');?>
								<td style="text-align: right;"><strong><?php print_r(number_format($sub_total_costo_venta,1,',','')); $total+=$sub_total_costo_venta; ?></strong></td>
							</tr>
						<?php }// end if?>
					<?php } ?>
	</tbody>
	<thead>
	<?php 
		$descuento=number_format($this->lib->desencriptar_num($pedido->descuento)*1,1,'.','');
		$total=number_format($total,1,'.','');
	?>
		<tr>
			<th style="text-align: right;" colspan="4">SUB TOTALES [BS.]</th>
		<?php for ($i=0; $i < count($v_tot_suc); $i++) { $tot=$v_tot_suc[$i];?>
			<th colspan="2"></th>
			<th style="text-align: right;"><?php print_r(number_format($tot,1,',',''));?></th>
		<?php } ?>
			<th style="text-align: right;"><?php print_r(number_format($total,1,',',''));?></th>
		</tr>
		<tr class="fila">
			<th style="text-align: right;" colspan="<?php echo 4+(count($partes)*3);?>">TOTAL DESCUENTO [Bs.]</th>
			<th style="text-align: right;">
				<?php 
				print_r(number_format($descuento,1,',','')); ?></th>
		</tr>
		<tr class="fila">
			<th style="text-align: right;" colspan="<?php echo 4+(count($partes)*3);?>">TOTAL FINAL [Bs.]</th>
			<th style="text-align: right;"><?php print_r(number_format($total-$descuento,1,',','')); ?></th>
		</tr>
	</thead>
</table>
<?php }?>