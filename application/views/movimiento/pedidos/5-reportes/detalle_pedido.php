<?php $rand=rand(10,9999); ?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link active reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
	<li class="nav-item"><a class="nav-link taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"> Taller </a></div></li>
<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
	<li class="nav-item"><a class="nav-link informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
	<li class="nav-item"><a class="nav-link informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
    <li class="nav-item"><a class="nav-link informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
<?php }?>
	<li class="nav-item"><a class="nav-link informe_materiales" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php
		$fun = array('function' => 'reporte_pedido', 'atribs' => array('pe' => $pedido->idpe));
		$excel = array('controller' => 'movimiento/exportar_reporte_pedido?pe='.$pedido->idpe.'&file=xls');
		$word = array('controller' => 'movimiento/exportar_reporte_pedido?pe='.$pedido->idpe.'&file=doc');
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'DETALLE DE PEDIDO']);?>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr class="fila">
								<th class="celda td padding-4" style="font-size: .95rem;">Nombre de pedido</th>
								<td class="celda td padding-4" style="font-size: .95rem;"><?php echo $pedido->nombre;?></td>
								<th class="celda td padding-4" style="font-size: .95rem;">Nº DE PEDIDO:</th>
								<td class="celda td padding-4" style="font-size: .95rem;"><?php echo $pedido->orden;?></td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" colspan="4">DATOS DE CLIENTE</th>
							</tr>
							<tr class="fila">
								<th class="celda td padding-4" width="20%">Nombre/Razon Social:</th><td class="celda td padding-4" width="30%"><?php echo $pedido->razon;?></td>
								<th class="celda td padding-4" width="17%">NIT/CI:</th><td class="celda td padding-4" width="33%"><?php echo $pedido->nit;?></td>
							</tr>
							<tr class="fila">
								<th class="celda td padding-4">Sitio Web: </th><td class="celda td padding-4"><?php echo $pedido->url;?></td>
								<th class="celda td padding-4">Observaciónes: </th><td class="celda td padding-4"><?php echo $pedido->observacion;?></td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" colspan="4">DATOS DE PEDIDO</th>
							</tr>
							<tr class="fila">
								<th class="celda td padding-4">Fecha de Pedido: </th><td class="celda td padding-4"><?php echo $pedido->fecha;?></td>
							<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){ ?>
								<?php $costo_venta=0;$detalles=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido->idpe);
									for($j=0; $j < count($detalles); $j++){ $costo_venta+=(($this->lib->desencriptar_num($detalles[$j]->cv)."")*1);}
								?>
								<th class="celda td padding-4">Monto de pedido (Bs.): </th><td class="celda td padding-4"><?php echo number_format($costo_venta,2,'.',',');?></td>
							<?php }?>
							</tr>
						<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){
								$total_pago=0;
								for($i=0; $i<count($pagos); $i++){ $pago=$pagos[$i];
									$total_pago+=($this->lib->desencriptar_num($pago->monto_bs)*1);
								}
						?>
							<tr class="fila">
								<th class="celda td padding-4">Descuento (Bs.): </th><td class="celda td padding-4"><?php echo number_format($descuento,2,'.',',');?></td>
								<th class="celda td padding-4">Total Pedido (Bs.): </th><td class="celda td padding-4"><?php echo number_format($costo_venta-$descuento,2,'.',',');?></td>
							</tr>
							<tr class="fila">
								<th class="celda td padding-4">Total cancelado (Bs.): </th><td class="celda td padding-4"><?php echo number_format($total_pago,2,'.',',');?></td>
								<th class="celda td padding-4">Saldo (Bs.): </th><td class="celda td padding-4"><?php echo number_format($costo_venta-$total_pago,2,'.',',');?></td>
							</tr>
						<?php }?>
							<tr class="fila">
								<th class="celda td padding-4">Observaciónes: </th><td class="celda td padding-4" colspan="3"><?php echo $pedido->observacion_pedido;?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$("a.taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();</script>