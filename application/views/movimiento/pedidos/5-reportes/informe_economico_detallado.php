<?php 
	$url=base_url().'libraries/img/'; 
	$vpartes=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$rand=rand(10,9999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link reporte_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab">Pedido</a></div></li>
	<li class="nav-item"><a class="nav-link taller_pedido" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"> Taller </a></div></li>
	<li class="nav-item"><a class="nav-link active informe_economico_pedido_detallado" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico det.</a></div></li>
	<li class="nav-item"><a class="nav-link informe_economico_pedido_general" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Económico gral.</a></div></li>
    <li class="nav-item"><a class="nav-link informe_depositos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Inf. Depositos</a></div></li>
	<li class="nav-item"><a class="nav-link informe_materiales" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>">Materiales necesarios</a></div></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php
		$fun = array('function' => 'informe_economico_pedido_detallado', 'atribs' => array('pe' => $pedido->idpe));
		$excel = array('controller' => 'movimiento/exportar_pedido_informe_detallado?pe='.$pedido->idpe.'&file=xls');
		$word = array('controller' => 'movimiento/exportar_pedido_informe_detallado?pe='.$pedido->idpe.'&file=doc');
		$default = array('top' => 1,'right' => 1,'bottom' => 1,'left' => 1,'page' => 1);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true,'default'=>json_encode($default)]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10 table-hover">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda" colspan="<?php echo 5+(count($partes)*3);?>" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'DETALLE ECONÓMICO ESPECIFICO','sub_titulo'=>'PEDIDO: '.$pedido->nombre]);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th" rowspan="2">#</th>
								<th class="celda th padding-4" width="80%" rowspan="2" colspan="2">Producto</th>
								<th class="celda th padding-4" width="15%" rowspan="2">C/U actual [Bs.]</th>
								<?php for ($i=0; $i < count($partes) ; $i++) { $v_tot_suc[$i]=0;?>
								<th class="celda th" colspan="3" width="30%"><?php echo $vpartes[$partes[$i]->tipo]." - ".$partes[$i]->numero; ?></th>
								<?php } ?>
								<th class="celda th padding-4" width="10%" rowspan="2">Total [Bs.]</th>
							</tr>
							<tr class="fila">
							<?php if(count($partes)>0){?>
								<?php for ($i=0; $i < count($partes) ; $i++) { ?>
								<th class="celda th" width="10%">Cant. [u.]</th>
								<th class="celda th" width="5%">C/U venta [Bs.]</th>
								<th class="celda th" width="20%">Costo venta [Bs.]</th>
								<?php }?>
							<?php }?>
							</tr>
						</thead>
						<tbody>
					<?php $cont=1;$total=0;
						for ($i=0;$i < count($productos_grupos_colores); $i++) { $pgc=$productos_grupos_colores[$i]; ?>
						<?php 
							$detalle=$this->lib->search_elemento($detalle_pedido,'idpgrc',$pgc->idpgrc);
							if($detalle!=null){
								$nombre=$pgc->nombre;$codigo=$pgc->codigo;
								$nombre_g="";$abr_g="";$nombre_c="";$abr_c="";
								if($pgc->abr_g!=""){$nombre.=" - ".$pgc->nombre_g;$codigo.="-".$pgc->abr_g;}
								$nombre.=" - ".$pgc->nombre_c;
								$codigo.="-".$pgc->abr_c;
								$p_img="sistema/miniatura/default.jpg";
								if($pgc->portada!="" && $pgc->portada!=null){
									$v=explode("|", $pgc->portada);
									if(count($v==2)){
										if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
										if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
									}
								}else{
									if($pgc->portada_producto!="" && $pgc->portada_producto!=null){
										$v=explode("|", $pgc->portada_producto);
										if(count($v==2)){
											if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
											if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
										}
									}
								}
						?>
							<tr class="fila">
							<?php $costo_venta=0;?>
								<td class="celda td"><?php echo $cont++; ?></td>
								<td class="celda td img-thumbnail-35"><div class="img-thumbnail-35"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail img-thumbnail-35" data-title="<?php echo $codigo." ".$nombre;?>" data-desc="<br>"></div></td>
								<td class="celda td padding-4" width="100%"><?php echo $codigo." ".$nombre;?></td>
								<td class="celda td" style="text-align: right;"><?php echo number_format($pgc->costo,2,'.',','); ?></td>
							<?php if(count($partes)>0){?>
							<?php for ($p=0; $p < count($partes) ; $p++){ $parte=$partes[$p];
									$cant="";$cu_venta="";$cv_venta="";$idpp="";
									$detalle_parte=$this->lib->search_elemento_2n($detalle_pedido,'idpgrc',$detalle->idpgrc,'idpp',$parte->idpp);
									if($detalle_parte!=null){
										$cantidades=$this->M_sucursal_detalle_pedido->get_row('iddp',$detalle_parte->iddp);
										$cant=0;
										for ($ca=0; $ca < count($cantidades) ; $ca++) { $cant+=($this->lib->desencriptar_num($cantidades[$ca]->cantidad)*1);}
										$idpp=$detalle_parte->idpp." = ".$parte->idpp;
										$cu_venta=$this->lib->desencriptar_num($detalle_parte->cu)."";
										$cv_venta=$this->lib->desencriptar_num($detalle_parte->cv)."";
									}//end if
									if($cu_venta!=""){$cu_venta=number_format($cu_venta,1,'.','')*1;}
									if($cv_venta!=""){$cv_venta=number_format($cv_venta,1,'.','')*1;}
							?>
								<td class="celda td text-right"><?php if($cant!=""){ echo number_format(($cant*1),0,'.',',');}?></td>
								<td class="celda td text-right"><?php if($cu_venta!=""){ echo number_format(($cu_venta*1),2,'.',',');}?></td>
								<td class="celda td text-right"<?php if((($cant*1)*($cu_venta)!=$cv_venta)){?> style="color: red;"<?php }?>><strong><?php if($cv_venta!=""){ echo number_format($cv_venta,2,'.',',');$costo_venta+=$cv_venta;$v_tot_suc[$p]+=$cv_venta;}?></strong></td>
								<?php }?>
							<?php }?>
							<?php $costo_venta=number_format($costo_venta,1,'.','');?>
								<td class="celda td text-right"><strong><?php echo number_format($costo_venta,2,'.',','); $total+=$costo_venta; ?></strong></td>
							</tr>
						<?php }//end if ?>
					<?php } ?>
						</tbody>
						<thead>
						<?php
							$total=number_format($total,1,'.','');
							$descuento=number_format($descuento,1,'.','');
						?>
							<tr class="fila disabled-print">
								<th class="celda td text-right" colspan="4" rowspan="2">SUB TOTALES [BS.]</th>
							<?php for ($p=0; $p < count($partes) ; $p++){?>
								<th class="celda td text-right" colspan="2"></th>
								<th class="celda td text-right"><?php echo number_format($v_tot_suc[$p],2,'.',','); ?></th>
							<?php }?>
								<th class="celda td text-right" rowspan="2"><?php echo number_format($total,2,'.',','); ?></th>
							</tr>
							<tr class="fila disabled-print">
							<?php for ($p=0; $p<count($partes); $p++){?>
								<th class="celda th" colspan="3"><?php echo $vpartes[$partes[$p]->tipo]." - ".$partes[$p]->numero; ?></th>
							<?php }?>
							</tr>
							<tr class="fila">
								<th class="celda td text-right padding-4" colspan="<?php echo 4+(count($partes)*3);?>">DESCUENTO [Bs.]</th>
								<th class="celda td text-right padding-4"><?php echo number_format(($descuento*1),2,'.',','); ?></th>
							</tr>
							<tr class="fila">
								<th class="celda td text-right padding-4" colspan="<?php echo 4+(count($partes)*3);?>">TOTAL COSTO DEL PEDIDO [Bs.]</th>
								<th class="celda td text-right padding-4"><?php echo number_format(($total-($descuento*1)),2,'.',','); ?></th>
							</tr>
						</thead>
					</table>
				</div>
	</div>
</div>
<script>$("a.reporte_pedido<?php echo $rand;?>").reporte_pedido();$(".taller_pedido").taller_pedido();$("a.informe_economico_pedido_detallado").informe_economico_pedido_detallado();$("a.informe_economico_pedido_general").informe_economico_pedido_general();$("a.informe_depositos").informe_depositos();$("a.informe_materiales").informe_materiales();$("img.img-thumbnail-35").visor();</script>