<?php if(isset($historial)){?>
	<table class="table table-bordered" style="font-size:.75em">
		<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
	    </thead>
	    <tbody>
		    <?php for ($i=0; $i < count($historial); $i++) { $alerta=$historial[$i]; ?>
		    	<tr>
		    		<td><?php echo $i+1;?></td>
		    		<td><?php $tiempo=$this->lib->mensaje_tiempo_transcurrido($alerta->fecha,"","Y-m-d"); echo $tiempo->tiempo;?></td>
		    		<td><?php echo $alerta->usuario;?></td>
					<td><?php echo $this->lib->alert_pedido($alerta);?></td>
		    	</tr>
		    <?php } ?>
	    </tbody>
	    </table>
<?php } ?>