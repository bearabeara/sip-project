<?php $url=base_url().'libraries/img/';

	$productos=$this->M_producto->get_search(NULL,NULL,NULL);
	$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
	$productos_imagenes=$this->M_producto_imagen->get_all();
	$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
	$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
	$order_by="codigo";//codigo o posicion
	$detalles_partes_pedido=$this->M_parte_pedido->get_detalle(NULL,NULL);
	$descuentos=$this->M_descuento_producto->get_search(NULL,NULL);
	$sucursales_cliente=$this->M_sucursal_cliente->get_complet(NULL,NULL);
	$usuarios=$this->M_usuario->get_search(NULL,NULL);
	$partes=$this->M_parte_pedido->get_search(NULL,NULL);
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_pedidos', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'movimiento/export_pedidos?num='.$num.'&nom='.$nom."&cli=".$cli."&file=xls");
		$word = array('controller' => 'movimiento/export_pedidos?num='.$num.'&nom='.$nom."&cli=".$cli."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Fecha</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 8%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>N° pedido</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 14%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Nombre de pedido</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 12%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Cliente</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Costo (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Descuento (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Total (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Cancelado (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Saldo (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 8%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="11" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Estado</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 15%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="12" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE PEDIDOS']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="3%">#</th>
								<th class="celda th padding-4" data-column="2" width="10%">Fecha</th>
								<th class="celda th padding-4" data-column="3" width="8%">N° pedido</th>
								<th class="celda th padding-4" data-column="4" width="14%">Nombre de pedido</th>
								<th class="celda th padding-4" data-column="5" width="12%">Cliente</th>
							<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
								<th class="celda th padding-4" data-column="6" width="6%">Costo (Bs.)</th>
								<th class="celda th padding-4" data-column="7" width="6%">Descuento (Bs.)</th>
								<th class="celda th padding-4" data-column="8" width="6%">Total (Bs.)</th>
								<th class="celda th padding-4" data-column="9" width="6%">Cancelado(Bs.)</th>
								<th class="celda th padding-4" data-column="10" width="6%" >Saldo (Bs.)</th>
							<?php }?>
								<th class="celda th padding-4" data-column="11" width="8%">Estado</th>
								<th class="celda th padding-4" data-column="12" width="15%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){ 
									$pedido=$this->lib->search_elemento($pedidos,"idpe",$visible);
									if($pedido!=null){
										$j_partes=$this->lib->select_from($partes,"idpe",$pedido->idpe);
										$j_detalles_partes_pedido=$this->lib->select_from($detalles_partes_pedido,"idpe",$pedido->idpe);
										$j_descuentos=$this->lib->select_from($descuentos,"idpe",$pedido->idpe);
										$j_sucursales_cliente=$this->lib->select_from($sucursales_cliente,"idcl",$pedido->idcl);
										$v_productos=$this->lib->descuentos_pedidos($j_partes,$j_detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
										$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$j_descuentos,$j_sucursales_cliente,$usuarios);
										/*end para descuento*/ 
										$descuentos_producto=json_decode(json_encode($v_productos));
										$descuento=0;
										foreach ($descuentos_producto as $key => $descuento_producto) {
											$descuento+=($descuento_producto->descuento*1);
										}

										$cont++;
										$s_pagos=$this->lib->select_from($pagos,'idpe',$pedido->idpe);
										$cancelado=0;
										for($j=0; $j < count($s_pagos); $j++){ $pago=$s_pagos[$j];$cancelado+=($this->lib->desencriptar_num($pago->monto_bs)*1);}
							?>
								<tr class="fila">
									<td class="celda td padding-4" data-column="1"><?php echo $cont;?></td>
									<td class="celda td padding-4" data-column="2"><?php echo $this->lib->format_date($pedido->fecha,'d/m/Y');?></td>
									<td class="celda td padding-4" data-column="3"><?php echo $pedido->orden;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $pedido->nombre;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $pedido->razon;?></td>
								<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){
										$costo_venta=0;
										$detalles=$this->lib->select_from($partes_detalles,"idpe",$pedido->idpe);
										foreach($detalles as $key => $detalle){
											$costo_venta+=(($this->lib->desencriptar_num($detalle->cv)."")*1);
										}
								?>
									<td class="celda td padding-4 text-right" data-column="6"><?php echo number_format($costo_venta,2,'.',',');?></td>
									<td class="celda td padding-4 text-right" data-column="7"><?php echo number_format($descuento,2,'.',',');?></td>
									<td class="celda td padding-4 text-right" data-column="8"><?php echo number_format($costo_venta-$descuento,2,'.',',');?></td>
									<td class="celda td padding-4 text-right" data-column="9"><?php echo number_format($cancelado,2,'.',',');?></td>
									<td class="celda td padding-4 text-right" data-column="10"><?php echo number_format(($costo_venta-$descuento)-$cancelado,2,'.',',');?></td>
								<?php }?>
									<td class="celda td padding-4" data-column="11"><?php if($pedido->estado==0){?>Pendiente<?php }?><?php if($pedido->estado==1){?>En producción<?php }?><?php if($pedido->estado==2){?>Entregado<?php }?></td>
									<td class="celda td padding-4" data-column="12"><?php echo $pedido->observacion;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>