<?php
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Pedidos-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Pedidos-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$productos=$this->M_producto->get_search(NULL,NULL,NULL);
	$productos_grupos_colores=$this->M_producto_grupo->get_grupo_colores(NULL,NULL);
	$productos_imagenes=$this->M_producto_imagen->get_all();
	$productos_imagenes_colores=$this->M_producto_imagen_color->get_all();
	$sucursales_detalles_pedidos=$this->M_sucursal_detalle_pedido->get_search(NULL,NULL);
	$order_by="codigo";//codigo o posicion
	$detalles_partes_pedido=$this->M_parte_pedido->get_detalle(NULL,NULL);
	$descuentos=$this->M_descuento_producto->get_search(NULL,NULL);
	$sucursales_cliente=$this->M_sucursal_cliente->get_complet(NULL,NULL);
	$usuarios=$this->M_usuario->get_search(NULL,NULL);
	$partes=$this->M_parte_pedido->get_search(NULL,NULL);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<h3>REPORTE DE PEDIDOS</h3>
<table border="1" cellpadding="5" cellspacing="0">
	<thead>
		<tr class="fila">
			<th>#</th>
			<th>Fecha</th>
			<th>N° pedido</th>
			<th>Nombre de pedido</th>
			<th>Cliente</th>
			<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){?>
			<th>Costo (Bs.)</th>
			<th>Descuento (Bs.)</th>
			<th>Total (Bs.)</th>
			<th>Cancelado(Bs.)</th>
			<th>Saldo (Bs.)</th>
			<?php }?>
			<th>Estado</th>
			<th>Observaciónes</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont=0;
		for ($i=0; $i < count($pedidos); $i++) { $pedido=$pedidos[$i];
			$j_partes=$this->lib->select_from($partes,"idpe",$pedido->idpe);
			$j_detalles_partes_pedido=$this->lib->select_from($detalles_partes_pedido,"idpe",$pedido->idpe);
			$j_descuentos=$this->lib->select_from($descuentos,"idpe",$pedido->idpe);
			$j_sucursales_cliente=$this->lib->select_from($sucursales_cliente,"idcl",$pedido->idcl);
			$v_productos=$this->lib->descuentos_pedidos($j_partes,$j_detalles_partes_pedido,$productos,$productos_grupos_colores,$productos_imagenes,
			$productos_imagenes_colores,$sucursales_detalles_pedidos,$order_by,$j_descuentos,$j_sucursales_cliente,$usuarios);
			/*end para descuento*/ 
			$descuentos_producto=json_decode(json_encode($v_productos));
			$descuento=0;
			foreach ($descuentos_producto as $key => $descuento_producto) {
				$descuento+=($descuento_producto->descuento*1);
			}

				$cont++;
				$pagos=json_decode(json_encode($this->lib->select_from($pagos,'idpe',$pedido->idpe)));
				$cancelado=0;
				for($pa=0;$pa<count($pagos); $pa++){$cancelado+=($this->lib->desencriptar_num($pagos[$pa]->monto)*1);}
					?>
				<tr class="fila">
					<td><?php echo $cont;?></td>
					<td><?php echo $this->lib->format_date($pedido->fecha,'d/m/Y');?></td>
					<td><?php echo $pedido->orden;?></td>
					<td><?php echo $pedido->nombre;?></td>
					<td><?php echo $pedido->razon;?></td>
					<?php if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){
						$costo_venta=0;
						$detalles=$this->lib->select_from($partes_detalles,"idpe",$pedido->idpe);
						foreach($detalles as $key => $detalle){
							$costo_venta+=(($this->lib->desencriptar_num($detalle->cv)."")*1);
						}
					?>
						<td style="text-align: right;"><?php print_r(number_format($costo_venta,1,',','.'));?></td>
						<td style="text-align: right;"><?php print_r(number_format($descuento,1,',','.'));?></td>
						<td style="text-align: right;"><?php print_r(number_format($costo_venta-$descuento,1,',','.'));?></td>
						<td style="text-align: right;"><?php print_r(number_format($cancelado,1,',',''));?></td>
						<td style="text-align: right;"><?php print_r(number_format(($costo_venta-$descuento)-$cancelado,1,',','.'));?></td>
					<?php }?>
						<td><?php if($pedido->estado==0){?>Pendiente<?php }?><?php if($pedido->estado==1){?>En producción<?php }?><?php if($pedido->estado==2){?>Entregado<?php }?></td>
						<td><?php echo $pedido->observacion;?></td>
					</tr>
		<?php }//end for ?>
	</tbody>
</table>