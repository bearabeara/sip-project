<table class="tabla tabla-border-false">
	<tr>
		<td class="hidden-sm" width="9.5%"></td>
		<td class='celda-sm-10 hidden-sm'>
			<form class="view_pedido" data-type="search">
				<input type="number" id="sp_num" placeholder='Nº de pedido' class="form-control form-control-sm search_movimiento" data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="3">
			</form>
		</td>
		<td style="width:16%">
			<form class="view_pedido" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="sp_nom" class="form-control form-control-sm search_movimiento" maxlength="100" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="4"/>
					<span class="input-group-addon form-control-sm view_pedido" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='celda-sm-15 hidden-sm'>
			<select id="sp_cli" class="form-control form-control-sm search_movimiento" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="5">
				<option value="">Seleccionar...</option>
		<?php for ($i=0; $i < count($clientes); $i++) { $cliente=$clientes[$i]; ?>
				<option value="<?php echo $cliente->idcl;?>"><?php echo $cliente->razon;?></option>
		<?php }//end for ?>
			</select>
		</td>
		<td class='celda-sm-10 hidden-sm'>
			<select id="sp_est" class="form-control form-control-sm search_movimiento" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="11">
				<option value="">Seleccionar...</option>
				<option value="0">Pendiente</option>
				<option value="1">En producción</option>
				<option value="2">Entregado</option>
			</select>
		</td>
		<td class='hidden-sm' style="width:44.5%"></td>
		<td class='text-right hidden-sm' style="width:5%">
			<?php 
				$search=json_encode(array('function'=>'view_pedido','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_pedido','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>$(".view_pedido").view_pedido();$(".search_movimiento").search_movimiento();$("input#sp_num").onfocus();</script>