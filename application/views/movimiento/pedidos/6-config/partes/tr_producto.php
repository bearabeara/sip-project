<?php $c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
$pgc=$grupo->idpgrc."-".rand(1,9999);
$img="sistema/miniatura/default.jpg";
if($grupo->portada!="" && $grupo->portada!=null){
	$v=explode("|", $grupo->portada);
	if(count($v==2)){
		if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
		if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
	}
}else{
	$img=$p_img;
} ?>
<td><div class="item"><?php echo $c2++;?></div><div class="g-img-modal"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail-modal"></div></td>
<?php $costo_unitario=$grupo->cu;if($parte_pedido->tipo==2){$costo_unitario*=2;}?>
<td>
	<?php echo $gr.$grupo->nombre_c;?>
	<?php 
	if($grupo->costo!=$costo_unitario){
		echo "<br><small class='text-danger'>Costo actual ".$grupo->costo."Bs.</small>";
		echo "<br><a href='javascript:' class='text-primary small refresh_costo_venta' data-padre='row-producto' data-costo='".$grupo->costo."'><strong><i class='fa fa-refresh'></i> Calcular costo de venta actual</strong></a>";
	}
?>
</td>
<td>
	<div class="input-group input-80">
		<input class="form-control form-control-sm cu" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario;?>" data-type='number' data-max="999999.9">
		<span class="input-group-addon form-control-sm">Bs.</span>
	</div>
</td>
<?php
$total_cantidad=0;
for($i=0; $i < count($sucursales); $i++){ $sucursal=$sucursales[$i]; 
	$pgc_sc=$grupo->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
	$cantidad=0;
	$j_sucursales=json_decode(json_encode($grupo->sucursales));
	$type_suc="";
	$idsdp="";
	foreach($j_sucursales as $key => $suc){
		if($suc->idsc==$sucursal->idsc){ $cantidad=$suc->cantidad; $type_suc="update"; $idsdp=$suc->idsdp; break; }
	}
	$total_cantidad+=$cantidad*1;
?>
	<td>
		<form class="update_detalle_pedido" data-padre="row-producto">
			<div class="input-group input-100">
				<input class="form-control form-control-sm cantidad" type="number" id="can<?php echo $pgc_sc;?>" data-pgc="<?php echo $pgc;?>" data-pgc-sc="<?php echo $pgc_sc;?>" data-p="<?php echo $id;?>" data-sc="<?php echo $sucursal->idsc;?>" placeholder="0" min="0" max="9999" data-type='number'<?php if($type_suc!=""){?> data-type-accion="<?php echo $type_suc;?>" data-sdp="<?php echo $idsdp;?>" <?php } ?> data-max="9999" oninput="costo_venta($(this))" onkeyup="costo_venta($(this))" onwheel="costo_venta($(this))" value="<?php echo $cantidad;?>">
				<span class="input-group-addon form-control-sm">u.</span>
			</div>
		</form>
	</td>
	<?php } ?>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm ct" type="text" id="ct<?php echo $pgc;?>" placeholder="0,0" disabled="disabled" value="<?php echo $total_cantidad;?>">
			<span class="input-group-addon form-control-sm">u.</span>
		</div>
	</td>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm st" type="text" id="st<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $total_cantidad*$costo_unitario;?>">
			<span class="input-group-addon form-control-sm">Bs.</span>
		</div>
	</td>
	<td>
		<form class="update_detalle_pedido" data-padre="row-producto">
			<div class="input-group input-100">
				<input class="form-control form-control-sm cv" type="number" id="cv<?php echo $pgc;?>" placeholder="0" min="0" max="9999999999.9" data-type='number' data-pgc="<?php echo $grupo->idpgrc;?>" data-cli="<?php echo $cliente->idcl;?>" data-max="9999999999.9" step="any" onclick="total_productos('<?php echo $idtb;?>')" onkeyup="total_productos('<?php echo $idtb;?>')" onwheel="total_productos('<?php echo $idtb;?>')" value="<?php echo $grupo->cv;?>">
				<span class="input-group-addon form-control-sm">Bs.</span>
			</div>
		</form>
	</td>
	<td>
		<div class="input-group input-150">
			<textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"><?php echo $grupo->observacion; ?></textarea>
			<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
		</div>
	</td>
	<td>
		<span class="g-control-accordion" style="width: 54px;">
			<button class="update_detalle_pedido" data-padre="row-producto"><i class="icon-floppy-o"></i></button>
			<button onclick="drop_elemento($(this))" data-padre="row-producto"><i class="icon-trashcan2"></i></button>
		</span>
	</td>