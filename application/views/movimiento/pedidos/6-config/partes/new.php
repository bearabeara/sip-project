<?php 
$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
$help2='title="Tipo de pedido" data-content="Seleccione el tipo de sub pedido si es una <strong>parte</strong>, <strong>extra</strong> o <strong>muestra</strong>, el número es generado automaticamente."';
$help3='title="Fecha de pedido" data-content="Ingrese la fecha de registro de este pedido."';
$help4='title="Observaciónes del pedido" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 300 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
$help5='title="Costo del sub pedido" data-content="Es el costo total del pedido segun el costo de venta de cada producto, este costo es <strong>generado automaticamente</strong> por el sistema."';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
$v =json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
$tipo="";
$rand=rand(10,99999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link config_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"><i></i>Pedido</a></div></li>
	<?php for($i=0; $i < count($partes) ; $i++){
		foreach($v as $clave => $opcion){
			if($clave==$partes[$i]->tipo){$tipo=$opcion;break;}
		}
	?>
		<li class="nav-item"><a class="nav-link config_parte_pedido" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></div></li>
		<?php }?>
		<li class="nav-item"><a class="nav-link new_parte_pedido active" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Nuevo + </a></li>
		<li class="nav-item"><a class="nav-link descuento_producto<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Descuentos</a></li>
		<li class="nav-item"><a class="nav-link pagos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Depósitos</a></li>
	</ul>
	<div class="list-group">
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Tipo de sub pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<form onsubmit="return save_pedido()">
							<div class="input-group">
								<select id="p_tip" class="form-control form-control-xs">
									<?php foreach ($v as $clave => $opcion) {?>
									<option value="<?php echo $clave?>"><?php echo $opcion;?></option>
									<?php } ?>
								</select>
								<span class="input-group-addon form-control-sm"></span>
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> fecha de pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_fec" type="date" value="<?php echo date('Y-m-d');?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
				</div><i class="clearfix"></i>
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
					<div class="col-sm-10 col-xs-12">
						<form onsubmit="return save_pedido()">
							<div class="input-group">
								<textarea class="form-control form-control-xs" id="p_obs" placeholder="Observaciónes del sub de pedido" maxlength="300" rows="3"></textarea>
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
				</div><i class="clearfix"></i>
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"> Costo del sub pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<div class="input-group">
							<input type="number" class="form-control form-control-xs" id="p_cos" disabled="disabled" value="0.0" />
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12"></div>
					<div class="col-sm-4 col-xs-12"></div>
				</div><i class="clearfix"></i>
			</div>
		</div>
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="form-group col-xs-12">
					<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true" data-cli='<?php echo $pedido->idcl;?>'></div>
				</div>
			</div><br>
			<div class="row text-right" style="padding-right:15px;">
				<button type="button" class="btn btn-primary btn-mini waves-effect waves-light view_producto" data-type="new_parte" data-cl="<?php echo $cliente->idcl;?>"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar producto</span></button>
			</div>
		</div>
	</div>
	<script>Onfocus("p_tip");$('[data-toggle="popover"]').popover({html:true});$("#list_product").sortable({out:function(event,ui){$(document).renumber_items("div#list_product div.accordion-panel a.accordion-msg div.item");},placeholder: "ui-state-highlight"});$(".view_producto").view_producto();
	
	$(".config_pedido<?php echo $rand;?>").config_pedido();$(".config_parte_pedido").config_parte_pedido();$(".new_parte_pedido").new_parte_pedido();
	$(".pagos").pagos();$(".descuento_producto<?php echo $rand;?>").descuento_producto();</script>