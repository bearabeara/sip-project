<?php
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help2='title="Tipo de sub pedido" data-content="Seleccione el tipo de sub pedido si es una <strong>parte</strong>, <strong>extra</strong> o <strong>muestra</strong>, el número es generado automaticamente."';
	$help3='title="Fecha de sub pedido" data-content="Ingrese la fecha de registro de este pedido."';
	$help4='title="Observaciónes del sub pedido" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 300 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help5='title="Costo del sub pedido" data-content="Es el costo total del pedido segun el costo de venta de cada producto, este costo es <strong>generado automaticamente</strong> por el sistema."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
	$v =json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
	$tipo="";
	$vd=[];
	foreach($productos as $key => $producto){
		$grupos=json_decode(json_encode($producto->categorias));
		foreach($grupos as $key => $grupo){
			$vd[]=array('dp'=>$grupo->iddp,'status' => '1');
		}
	}
	$detalles=json_encode($vd);
	//calculando costo de venta
	$cos_v=0;
	foreach($productos as $key => $producto){
		$grupos=json_decode(json_encode($producto->categorias));
		foreach($grupos as $key => $grupo){
			$cos_v+=$grupo->costo_venta;
		}
	}
	$rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link config_pedido<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-home"></i>Pedido</a>
	<span id="detalles" style="display: none;"><?php echo $detalles.'';?></span></li>
	<?php for($i=0; $i<count($partes); $i++){
			foreach($v as $clave => $opcion){
				if($clave==$partes[$i]->tipo){$tipo=$opcion;break;}
			}
	?>
	<li class="nav-item"><a class="nav-link config_parte_pedido <?php if($partes[$i]->idpp==$parte_pedido->idpp){ echo 'active';} ?>" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></li>
	<?php } ?>
	<li class="nav-item"><a class="nav-link new_parte_pedido" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Nuevo +</a></li>
	<li class="nav-item"><a class="nav-link descuento_producto<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Descuentos</a></li>
	<li class="nav-item"><a class="nav-link pagos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Depósitos</a></li>
</ul>
	<div class="list-group">
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="col-xs-12">
					<span class="g-control-accordion">
						<button class="refresh_prod config_parte_pedido" id="datos_parte_pedido" data-pp="<?php echo $parte_pedido->idpp;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-refresh"></i></button>
						<button class="parte_seg" data-pp="<?php echo $parte_pedido->idpp;?>"  data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-clock-o"></i></button>
					<?php if($pedido->estado!="2" && count($partes)>1){?>
						<button class="confirmar_parte_pedido" data-pp="<?php echo $parte_pedido->idpp;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="icon-bin"></i></button>
					<?php }?>
					</span>
				</div>
			</div>
		</div>
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Tipo de sub pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<form onsubmit="return save_pedido()">
							<div class="input-group">
								<select id="p_tip" class="form-control form-control-xs" disabled="disabled">
									<?php foreach ($v as $clave => $opcion) {?>
									<option value="<?php echo $clave?>" <?php if($parte_pedido->tipo==$clave){echo "selected";} ?>><?php echo $opcion;?></option>
									<?php } ?>
								</select>
								<span class="input-group-addon form-control-sm"><?php echo $parte_pedido->numero; ?></span>
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> fecha de pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_fec" type="date" value="<?php echo $parte_pedido->fecha;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
				</div><i class="clearfix"></i>
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
					<div class="col-sm-10 col-xs-12">
						<form onsubmit="return save_pedido()">
							<div class="input-group">
								<textarea class="form-control form-control-xs" id="p_obs" placeholder="Observaciónes del sub de pedido" maxlength="300" rows="3"><?php echo $parte_pedido->observacion; ?></textarea>
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
				</div><i class="clearfix"></i>
				<div class="form-group">
					<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"> Costo del sub pedido:</label></div>
					<div class="col-sm-4 col-xs-12">
						<div class="input-group">
							<input type="number" class="form-control form-control-xs" id="p_cos" disabled="disabled" value="<?php echo $cos_v;?>" />
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12"></div>
					<div class="col-sm-4 col-xs-12"></div>
				</div><i class="clearfix"></i>
			</div>
		</div>
		<div class="list-group-item" style="max-width:100%">
			<div class="row">
				<div class="form-group col-xs-12">
					<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true" data-cli='<?php echo $pedido->idcl;?>'>
						<?php $c1=1; 
							foreach($productos as $key => $producto){
							$id=$producto->idp."-".rand(0,999999); 
							$grupos=json_decode(json_encode($producto->categorias));
						?>
						<div class="accordion-panel" id="accordion-panel<?php echo $id;?>" data-cl="<?php echo $cliente->idcl;?>" data-tipo="<?php echo $parte_pedido->tipo;?>">
							<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
								<span class="card-title accordion-title">
									<?php $idtb=$producto->idp.'-'.rand(0,999999);?>
									<span class="g-control-accordion">
									<?php if($pedido->estado!="2"){?>
										<button class="refresh_producto_pedido" data-elemento="<?php echo $id;?>" data-type-refresh="db" data-type="update" data-p="<?php echo $producto->idp;?>" data-pp="<?php echo $parte_pedido->idpp;?>" data-tbl="<?php echo $idtb;?>" data-container="span#detalles" data-padre="accordion-panel" data-type-padre="atr">
											<i class="icon-refresh"></i>
										</button>
										<button class="drop_elemento<?php echo $rand;?>" data-padre="accordion-panel<?php echo $id;?>" data-type-padre="atr" data-container="span#detalles" data-control="db-producto" data-module="producto_pedido"><i class="icon-trashcan2"></i></button>
									<?php }?>
									</span>
									<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
										<div class="item"><?php echo $c1++;?></div>
										<span class="img-thumbnail-50"><img src="<?php echo $producto->fotografia;?>" class="img-thumbnail img-thumbnail-50"></span>
										<span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
										<?php $c_total=0;
											foreach($grupos as $key => $g){
												$j_sucursales=json_decode(json_encode($g->sucursales));
												foreach($j_sucursales as $key => $suc){ 
													$c_total+=($suc->cantidad*1);	
												}//end foreach
											}//end foreach
										?>
										<label class="badge badge-inverse-<?php if($c_total>0){?>primary<?php }else{?>danger<?php }?>" id="badge<?php echo $id;?>"> Total: <?php echo $c_total;?> unidades</label>
									</a>
								</span>
							</div>
								<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
									<div class="accordion-content accordion-desc" data-pr="<?php echo $producto->idp;?>">
										<div class="table-responsive g-table-responsive">
											<?php $vd_p=array();$c_calculado=0;$c_venta=0;
												foreach($grupos as $key => $grupo){
													$vd_p[]=array('dp'=>$grupo->iddp,'status' => '1');
												}
											?>
											<table class="table table-bordered table-hover" id="<?php echo $idtb;?>" data-accordion="div#accordion-panel<?php echo $id;?>" data-badge="label#badge<?php echo $id;?>">
												<thead>
													<tr>
														<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>#Item</th>
														<th width="32%">Producto</th>
														<th width="14%">C/U (Bs.)</th>
														<?php for ($i=0; $i < count($sucursales) ; $i++) { $sucursal=$sucursales[$i]; ?>
														<th><?php echo $sucursal->nombre; ?></th>
														<?php } ?>
														<th>Total</th>
														<th width="17%">Costo calculado(Bs.)</th>
														<th width="17%">Costo de venta(Bs.)</th>
														<th width="18%">Observaciónes</th>
													<?php if($pedido->estado!="2"){?>
														<td><span id="detalles-producto-<?php echo $producto->idp;?>" style="display: none;"><?php echo json_encode($vd_p); ?></span></td>
													<?php }?>
													</tr>
												</thead>
												<tbody>
													<?php if(!empty($grupos)){ $c2=1;?>
													<?php foreach ($grupos as $key => $grupo){$id_tr=$grupo->idpgrc."-".rand(10,999999); ?>
													<tr class="row-producto" id="<?php echo $id_tr;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-type="update" data-dp="<?php echo $grupo->iddp;?>" data-pp="<?php echo $parte_pedido->idpp;?>">
														<?php $pgc=$grupo->idpgrc."-".rand(1,9999);?>
														<td><div class="item"><?php echo $c2;?></div><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-45"  data-title="<?php echo $producto->nombre.' - '.$grupo->nombre;?>" data-desc="<br>"></td>
														<?php $costo_unitario=$grupo->costo_unitario;
												            $costo_unitario_venta=$grupo->costo_unitario_venta;
												           if($parte_pedido->tipo==2){$costo_unitario*=2;}
												        ?>
														<td>
														<?php echo $grupo->nombre;?>
														<?php if($pedido->estado!="2"){?>
															<?php if($costo_unitario_venta!=$costo_unitario){
																	echo "<br><small class='text-danger'>Costo actual ".$costo_unitario."Bs.</small>";
																	echo "<br><a href='javascript:' class='text-primary small refresh_producto_detalle_pedido' data-container-tr='".$id_tr."' data-dp='".$grupo->iddp."' data-p='".$grupo->idp."' data-item='".$c2."' data-tbl='".$idtb."' data-container='span#detalles' data-container-2='span#detalles-producto-".$producto->idp."'><strong title='Calcular el costo de venta con el costo unitario actual'><i class='fa fa-refresh'></i> Actua...</strong></a>";
																}
															?>
														<?php }?>
														
														</td>
														<td>
															<div class="input-group input-80">
																<input class="form-control form-control-sm cu" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario_venta;?>" data-value="<?php echo $costo_unitario_venta;?>">
																<span class="input-group-addon form-control-sm">Bs.</span>
															</div>
														</td>
														<?php $c2++;
														$total_cantidad=0;
														for($i=0;$i<count($sucursales);$i++){$sucursal=$sucursales[$i]; 
															$pgc_sc=$grupo->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
															$cantidad=0;
															$j_sucursales=json_decode(json_encode($grupo->sucursales));
															$type_suc="new";
															$idsdp="";
															foreach($j_sucursales as $key => $suc){
																if($suc->idsc==$sucursal->idsc){$cantidad=$suc->cantidad; $type_suc="update";$idsdp=$suc->idsdp; break;}
															}
															$total_cantidad+=$cantidad*1;
														?>
															<td>
																<div class="input-group input-100">
																	<input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" placeholder="0" min="0" max="9999" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $idtb;?>" <?php if($type_suc!=""){?> data-type="<?php echo $type_suc;?>"<?php }?><?php if($idsdp!=""){?> data-sdp="<?php echo $idsdp;?>" <?php }?> value="<?php echo $cantidad;?>">
																	<span class="input-group-addon form-control-sm">u.</span>
																</div>
															</td>
														<?php } ?>
															<td>
																<div class="input-group input-100">
																	<input class="form-control form-control-sm ct" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad;?>">
																	<span class="input-group-addon form-control-sm">u.</span>
																</div>
															</td>
															<td>
																<div class="input-group input-100">
																	<input class="form-control form-control-sm st" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad*$costo_unitario_venta;?>">
																	<span class="input-group-addon form-control-sm">Bs.</span>
																</div>
																<?php $c_calculado+=($total_cantidad*$costo_unitario_venta);?>
															</td>
															<td>
																<div class="input-group input-100">
																	<input class="form-control form-control-sm costo_venta<?php echo $rand;?>" type="number" id="costo_venta<?php echo $pgc;?>" placeholder="0" value="<?php echo $grupo->costo_venta;?>" disabled="disabled">
																		<span class="input-group-addon form-control-sm">Bs.</span>
																</div>
																<?php $c_venta+=($grupo->costo_venta*1);?>
															</td>
															<td>
																<div class="input-group input-150">
																	<textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"><?php echo $grupo->observacion; ?></textarea>
																	<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
																</div>
															</td>
														<?php if($pedido->estado!="2"){?>
															<td>
																<span class="g-control-accordion" style="/*width: 54px;*/">
																	<button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $idtb;?>' data-control="db-producto" data-dp="<?php echo $grupo->iddp;?>" data-container="span#detalles" data-container-2="span#detalles-producto-<?php echo $producto->idp;?>"><i class="icon-trashcan2"></i></button>
																</span>
															</td>
														<?php }?>
														</tr>    
														<?php }?>
														<?php }else{ ?>
														<tr><td colspan="7"><h3>0 colores registrados en el producto</h3></td></tr>
														<?php }?>
													</tbody>
													<thead>
														<tr class="row-total">
															<th colspan="<?php echo 3+count($sucursales);?>" class="text-right">Totales</th>
																<th class="sub_can_pro"><?php echo $c_total;?></th>
																<th class="sub_cos_pro"><?php echo $c_calculado;?></th>
																<th class="sub_tot_cos_pro"><?php echo $c_venta;?></th>
																<th></th>
															<?php if($pedido->estado!="2"){?>
																<th></th>
															<?php }?>
															</tr>
														</thead>
													</table>
												</div> 
											<?php if($pedido->estado!="2"){?>
												<div class="row text-right" style="padding-right:15px; margin-top: 5px;">
													<button type="button" class="btn btn-inverse-info waves-effect btn-mini colores_producto<?php echo $rand;?>" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>">
														<i class="fa fa-plus"></i><span class="m-l-10">categoría</span>
													</button>
													<button type="button" class="btn btn-inverse-primary waves-effect btn-mini update_producto_colores" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>" data-pp="<?php echo $parte_pedido->idpp;?>" data-accordion="accordion-panel<?php echo $id;?>" data-container="span#detalles" data-container-2="span#detalles-producto-<?php echo $producto->idp;?>">
														<i class="fa fa-floppy-o"></i><span class="m-l-10">guardar</span>
													</button>
												</div>
											<?php }?>
											</div>
										</div>
								</div>
									<?php } ?>
								</div>
							</div>
						</div><br>
					<?php if($pedido->estado!="2"){?>
						<div class="row text-right" style="padding-right:15px;">
							<button type="button" class="btn btn-primary btn-mini waves-effect waves-light view_producto<?php echo $rand;?>" data-type="new_parte" data-cl="<?php echo $cliente->idcl;?>"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar producto</span></button>
						</div>
					<?php }?>
					</div>
				</div>
<script>Onfocus("p_tip");$('[data-toggle="popover"]').popover({html:true});$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$(".drop_elemento<?php echo $rand;?>").drop_elemento();$(".colores_producto<?php echo $rand;?>").colores_producto();$(".view_producto<?php echo $rand;?>").view_producto();$("img.img-thumbnail-45").visor();
	<?php if($pedido->estado!="2"){?>$("#list_product").sortable({out:function(event,ui){$(document).renumber_items("div#list_product div.accordion-panel a.accordion-msg div.item");},placeholder: "ui-state-highlight"});<?php }?>$(".config_pedido<?php echo $rand;?>").config_pedido();$(".config_parte_pedido").config_parte_pedido();$(".parte_seg").parte_seg();$(".confirmar_parte_pedido").confirmar_parte_pedido();$(".refresh_producto_pedido").refresh_producto_pedido();$("a.refresh_producto_detalle_pedido").refresh_producto_detalle_pedido();$("button.colores_producto<?php echo $rand;?>").colores_producto();$('button.update_producto_colores').update_producto_colores();$(".new_parte_pedido").new_parte_pedido();$(".descuento_producto<?php echo $rand;?>").descuento_producto();$(".pagos").pagos();
</script>