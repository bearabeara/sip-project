<?php
	$help1='title="Número de pedido" data-content="Representa al número de referencia del pedido, este numero es unico y <strong>generado automaticamente por el sistema</strong>, si fuese el caso de que el numero fuera ya registrado puede pulsar el boton actualizar el número."';
	$help15='title="Nombre de pedido" data-content="Representa al nombre del pedido, el contenido debe ser en formato alfanumérico <b>de 3 a 150 caracteres puede incluir espacios</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help2='title="Cliente" data-content="Seleccione el cliente que realiza el pedido, si desea adicionar un nuevo cliente lo puede hacer accediendo por el menu principal izquierdo <b>Cliente/Proveedor</b>, o dando click en el boton <b>+</b>"';
	$help3='title="Fecha de pedido" data-content="Ingrese la fecha de registro de este pedido."';
	$help31='title="Costo de pedido" data-content="Es el costo total del pedido segun el costo de venta de cada producto, este costo es <strong>generado automaticamente</strong> por el sistema."';
	$help32='title="Descuento al costo del pedido" data-content="Ingrese el monto de descuento que se relizara al <strong>Costo de pedido</strong>."';
	$help33='title="Costo final del pedido" data-content="Es el costo que representa al pedido, el valor de este costo es el resultado de la diferencia en entre el <strong>Costo de pedido</strong> y el <strong>Descuento al costo de pedido</strong>"';
	$help4='title="Observaciónes del pedido" data-content="Ingrese algunas observaciónes del pedido si tuviera. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 300 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$help5='title="Estado de produccion" data-content="Representa a el estado de produccion en el cual se encuentra el pedido."';
	$help6='title="Estado de pagos" data-content="Representa a el estado de pago del pedido si quedan pagos pendientes o no."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$tipo="";
	$v=json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
	$e=json_decode(json_encode(array('0' => 'Pendiente', '1' => 'En produccion', '2' => 'Entregado')));
	$pa=json_decode(json_encode(array('0' => 'Saldo pendiente', '1' => 'Costo total cancelado')));
	$rand=rand(10,99999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link active config_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"><i class="icofont icofont-home"></i>Pedido</a></div></li>
<?php for ($i=0; $i < count($partes) ; $i++) { 
		foreach ($v as $clave => $opcion) {
			if($clave==$partes[$i]->tipo){ $tipo=$opcion;break;}
		}
?>
	<li class="nav-item"><a class="nav-link config_parte_pedido" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></div></li>
<?php } ?>
	<li class="nav-item"><a class="nav-link new_parte_pedido" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Nuevo +</a></li>
	<li class="nav-item"><a class="nav-link descuento_producto" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Descuentos</a></li>
	<li class="nav-item"><a class="nav-link pagos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Depósitos</a></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-xs-12">
				<span class="g-control-accordion">
					<button class="refresh_prod config_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-refresh"></i></button>
					<button class="pedido_seg<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-clock-o"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Número de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
                            <select class="form-control form-control-xs" id="p_nro">
                                <option value="">Seleccionar...</option>
                                <?php
                                for ($i=0; $i < count($pedidos) ; $i++) { $orden=$pedidos[$i]->orden; ?>
                                    <option value="<?php echo $orden; ?>" <?php if($pedido->orden==$orden){ echo "selected"; }?>><?php echo $orden;?></option>
                                <?php } ?>
                            </select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Nombre de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_nom" type="text" placeholder='Nombre de pedido' maxlength="150" value="<?php echo $pedido->nombre;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help15;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Cliente:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<select id="p_cli" class="form-control form-control-xs" disabled="disabled">
								<option value="">Seleccionar...</option>
								<?php 
								for ($i=0; $i < count($clientes) ; $i++) { $cliente=$clientes[$i]; ?>
								<option value="<?php echo $cliente->idcl; ?>" <?php if($pedido->idcl==$cliente->idcl){ echo "selected"; }?>><?php echo $cliente->razon;?></option>
								<?php } ?>
							</select>
							<a href="<?php echo base_url();?>cliente_proveedor" target="_blank" title="Ver Clientes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"> Costo de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
					<?php 
						$costo_venta=0;$detalles=$this->M_parte_pedido->get_detalle("pp.idpe",$pedido->idpe);
						for($j=0; $j < count($detalles); $j++){ $costo_venta+=(($this->lib->desencriptar_num($detalles[$j]->cv)."")*1);}
					?>
						<input type="number" class="form-control form-control-xs" id="p_cos" disabled="disabled" value="<?php echo $costo_venta;?>" />
						<span class="input-group-addon form-control-sm">Bs.</span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help31;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Descuento al costo de pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_des" type="number" placeholder='0,0' min="0" max="999999,9" value="<?php echo $descuento;?>" oninput="total_costo_venta();" onkeyup="total_costo_venta();" onwheel="total_costo_venta();" disabled="disabled">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help32;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Costo final del pedido:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<input class="form-control form-control-xs" id="p_tot" type="number" disabled="disabled" value="<?php echo $costo_venta-$descuento;?>">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help33;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Observaciónes:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<textarea class="form-control form-control-xs" id="p_obs" placeholder="Observaciónes de pedido" maxlength="300" rows="4"><?php echo $pedido->observacion;?></textarea>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Estado de producción:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<select id="p_est" class="form-control form-control-xs">
								<option value="">Seleccionar...</option>
								<?php 
								foreach ($e as $key => $estado) {?>
								<option value="<?php echo $key; ?>" <?php if($pedido->estado==$key){ echo "selected"; }?>><?php echo $estado;?></option>
								<?php } ?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Estado de pagos:</label></div>
				<div class="col-sm-4 col-xs-12">
					<form class="update_pedido" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<select id="p_est_pag" class="form-control form-control-xs">
								<option value="">Seleccionar...</option>
								<?php 
								foreach ($pa as $key => $estado_pago) {?>
								<option value="<?php echo $key; ?>" <?php if($pedido->estado_pago==$key){ echo "selected"; }?>><?php echo $estado_pago;?></option>
								<?php } ?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
		</div>
	</div>
</div>
<script>Onfocus("p_nom");$('[data-toggle="popover"]').popover({html:true});$(".config_pedido<?php echo $rand;?>").config_pedido();$(".config_parte_pedido").config_parte_pedido();$(".update_pedido").submit(function(e){ $(this).update_pedido();e.preventDefault();});$(".new_parte_pedido").new_parte_pedido();$(".pagos").pagos();$("button.pedido_seg<?php echo $rand;?>").pedido_seg();$("a.descuento_producto").descuento_producto();</script>