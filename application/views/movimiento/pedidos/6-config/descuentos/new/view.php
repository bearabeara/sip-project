<?php
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes de descuento en el producto. El contenido de las observaciones debe ser en formato alfanumérico <b>hasta 900 caracteres puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
	$tipo="";
	$v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
	$rand=rand(10,99999999);
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
<?php
	if(count($partes_pedido)>0){
		for($i=0;$i<count($partes_pedido);$i++){$parte=$partes_pedido[$i];
			$seleccionado=false;
			if($parte_pedido->idpp==$parte->idpp){
				$seleccionado=true;
			}
?>
	<button type="button" class="btn btn-<?php if(!$seleccionado){?>inverse-<?php }?>info btn-mini waves-effect new_descuento_producto<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" data-pp="<?php echo $parte->idpp;?>" style="margin-bottom: 5px;"><?php echo $v[$parte->tipo]." ".$parte->numero;?></button>
<?php }
	}
?>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true" data-cli='<?php echo $pedido->idcl;?>'>
						<?php $c1=1; 
							foreach($productos as $key => $producto){
							$id=$producto->idp."-".rand(0,999999); 
							$grupos=json_decode(json_encode($producto->categorias));
						?>
						<div class="accordion-panel" id="accordion-panel<?php echo $id;?>" data-tipo="<?php echo $parte_pedido->tipo;?>">
							<div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
								<span class="card-title accordion-title">
									<?php $idtb=$producto->idp.'-'.rand(0,999999);?>
									<a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
										<div class="item"><?php echo $c1++;?></div>
										<span class="img-thumbnail-50"><img src="<?php echo $producto->fotografia;?>" class="img-thumbnail img-thumbnail-50"></span>
										<span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
										<?php $c_total=0;
											
										?>
										<label class="badge badge-inverse-<?php if($c_total>0){?>primary<?php }else{?>danger<?php }?>" id="badge<?php echo $id;?>"> Total: <?php echo $c_total;?> unidades</label>
									</a>
								</span>
							</div>
								<div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
									<div class="accordion-content accordion-desc" data-pr="<?php echo $producto->idp;?>">
										<div class="table-responsive g-table-responsive">
											<?php $vd_p=array();$c_calculado=0;$c_venta=0;
												foreach($grupos as $key => $grupo){
													$vd_p[]=array('dp'=>$grupo->iddp,'status' => '1');
												}
											?>
											<table class="table table-bordered table-hover" id="<?php echo $idtb;?>" data-accordion="div#accordion-panel<?php echo $id;?>" data-badge="label#badge<?php echo $id;?>">
												<thead>
													<tr>
														<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>#Item</th>
														<th width="22%">Producto</th>
														<th width="5%">C/U venta (Bs.)</th>
													<?php if(count($grupo->sucursales)>0){?>
														<th width="12%">Sucursales</th>
														<th width="7%">Cant. pedido</th>
														<th width="7%">Total desc.</th>
														<th width="7%">Cant. a descontar</th>
														<th width="7%">% de descuento</th>
														<th width="7%">Descuento (Bs.)</th>
														<th width="30%">Observaciónes de descuento</th>
														<th></th>
													<?php }?>
													</tr>
												</thead>
												<tbody>
												<?php if(!empty($grupos)){ $c2=1;?>
													<?php foreach ($grupos as $key => $grupo){?>
													<tr>
														<td rowspan="<?php echo count($grupo->sucursales)?>"><div class="item"><?php echo $c2++;?></div><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-45"  data-title="<?php echo $producto->nombre.' - '.$grupo->nombre;?>" data-desc="<br>"></td>
														<td rowspan="<?php echo count($grupo->sucursales)?>"><?php echo $grupo->nombre;?></td>
														<td rowspan="<?php echo count($grupo->sucursales)?>" class="text-right"><?php echo number_format($grupo->costo_unitario_venta,2,'.',',');?></td>
												<?php if(count($grupo->sucursales)>0){ $sucursal_producto=$grupo->sucursales;
															$sucursal=$this->lib->search_elemento($sucursales,'idsc',$sucursal_producto[0]->idsc);
														if($sucursal!=null){
															$id_row=$sucursal_producto[0]->idsdp."-".rand(10,9999999);
															$total_descontado=0;
															$descuento_productos=$this->lib->select_from($descuentos_productos,'idsdp',$sucursal_producto[0]->idsdp);
															if(count($descuento_productos)>0){
																for ($d=0; $d < count($descuento_productos); $d++) { 
																	$cant=$this->lib->desencriptar_num($descuento_productos[$d]->cantidad)*1;
																	$total_descontado+=$cant;
																}
															}
												?>
														<td><?php echo $sucursal->nombre; ?></td>
														<td class="text-right"><?php echo $sucursal_producto[0]->cantidad." u."; ?></td>
														<td class="text-right"><?php echo $total_descontado." u."; ?></td>
														<td><div class="input-group input-80"><input class="form-control form-control-sm cantidad" type="number" id="cantidad<?php echo $id_row;?>" placeholder="0" min="0" max="<?php echo $sucursal_producto[0]->cantidad-$total_descontado;?>" data-id="<?php echo $id_row;?>" data-cu="<?php echo $grupo->costo_unitario_venta*1;?>" data-cantidad="<?php echo $sucursal_producto[0]->cantidad;?>" data-max="<?php echo $sucursal_producto[0]->cantidad-$total_descontado;?>"><span class="input-group-addon form-control-sm">u.</span></div></td>
														<td><div class="input-group input-80"><input class="form-control form-control-sm porcentaje" type="number" id="porcentaje<?php echo $id_row;?>" placeholder="0" min="0" max="100" data-id="<?php echo $id_row;?>" step="any"><span class="input-group-addon form-control-sm">%</span></div></td>
														<td><div class="input-group input-100"><input class="form-control form-control-sm" type="number" id="descuento<?php echo $id_row;?>" placeholder="0" disabled="disabled"><span class="input-group-addon form-control-sm">Bs.</span></div></td>
														<td>
															<div class="input-group input-200">
            												<textarea class="form-control form-control-xs" id="obs<?php echo $id_row;?>" placeholder="Observaciónes" maxlength="900" rows="3"></textarea>
            												<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
         													</div>
														</td>
														<td>
														<?php if($total_descontado>=$sucursal_producto[0]->cantidad){ ?>
															<button type="button" class="btn btn-inverse-default btn-mini waves-effect waves-light" disabled="disabled"><i class="fa fa-plus"></i> add</button>
														<?php }else{?>
															<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light save_descuento_producto" data-sdp="<?php echo $sucursal_producto[0]->idsdp;?>" data-pe="<?php echo $pedido->idpe;?>" data-id="<?php echo $id_row;?>"><i class="fa fa-plus"></i> add</button>
														<?php }?>
														</td>
												<?php }}?>
													</tr>
												<?php if(count($grupo->sucursales)>1){$sucursal_producto=$grupo->sucursales;
															for($i=1;$i<count($sucursal_producto);$i++){
																$sucursal=$this->lib->search_elemento($sucursales,'idsc',$sucursal_producto[$i]->idsc);
																if($sucursal!=null){
																	$id_row=$sucursal_producto[$i]->idsdp."-".rand(10,9999999);
																	$total_descontado=0;
																	$descuento_productos=$this->lib->select_from($descuentos_productos,'idsdp',$sucursal_producto[$i]->idsdp);
																	if(count($descuento_productos)>0){
																		for ($d=0; $d < count($descuento_productos); $d++) { 
																			$cant=$this->lib->desencriptar_num($descuento_productos[$d]->cantidad)*1;
																			$total_descontado+=$cant;
																		}
																	}
												?>
													<tr>
														<td><?php echo $sucursal->nombre; ?></td>
														<td class="text-right"><?php echo $sucursal_producto[$i]->cantidad." u."; ?></td>
														<td class="text-right"><?php echo $total_descontado." u."; ?></td>
														<td><div class="input-group input-80"><input class="form-control form-control-sm cantidad" type="number" id="cantidad<?php echo $id_row;?>" placeholder="0" min="0" max="<?php echo $sucursal_producto[$i]->cantidad-$total_descontado;?>" data-id="<?php echo $id_row;?>" data-cu="<?php echo $grupo->costo_unitario_venta*1;?>" data-cantidad="<?php echo $sucursal_producto[$i]->cantidad;?>" data-max="<?php echo $sucursal_producto[$i]->cantidad-$total_descontado;?>"><span class="input-group-addon form-control-sm">u.</span></div></td>
														<td><div class="input-group input-80"><input class="form-control form-control-sm porcentaje" type="number" id="porcentaje<?php echo $id_row;?>" placeholder="0" min="0" max="100" data-id="<?php echo $id_row;?>" step="any"><span class="input-group-addon form-control-sm">%</span></div></td>
														<td><div class="input-group input-100"><input class="form-control form-control-sm" type="number" id="descuento<?php echo $id_row;?>" placeholder="0" disabled="disabled"><span class="input-group-addon form-control-sm">Bs.</span></div></td>
														<td><div class="input-group input-200">
            												<textarea class="form-control form-control-xs" id="obs<?php echo $id_row;?>" placeholder="Observaciónes" maxlength="900" rows="3"></textarea>
            												<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
         													</div>
														</td>
														<td>
														<?php if($total_descontado>=$sucursal_producto[$i]->cantidad){ ?>
															<button type="button" class="btn btn-inverse-default btn-mini waves-effect waves-light" disabled="disabled"><i class="fa fa-plus"></i> add</button>
														<?php }else{?>
															<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light save_descuento_producto" data-sdp="<?php echo $sucursal_producto[$i]->idsdp;?>" data-pe="<?php echo $pedido->idpe;?>" data-id="<?php echo $id_row;?>"><i class="fa fa-plus"></i> add</button>
														<?php }?>
														</td>
													</tr>
													<?php		}//end if
															}//end for
													}//end if?>
													<?php }//end foreach?>
												<?php }else{ ?>
													<tr><td colspan="7"><h3>0 colores registrados en el producto</h3></td></tr>
												<?php }?>
												</tbody>
											</table>
										</div> 
									</div>
								</div>
							</div>
						<?php } ?>
						</div>
	</div>
</div>
<script>$(".cantidad").total_descuento_producto();$(".porcentaje").total_descuento_producto();$(".new_descuento_producto<?php echo $rand;?>").new_descuento_producto();$(".save_descuento_producto").save_descuento_producto();$('[data-toggle="popover"]').popover({html:true});</script>
