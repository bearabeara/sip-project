<?php
    $help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>"';
    $help2='title="Descripción" data-content="Ingrese un detalle de descuento, el contenido debe ser de un formato alfanumerico de 0 a 500 caracteres <b>puede incluir espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
    $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
<div class="row hidden-sm"><div class="col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
    <div class="list-group-item" style="max-width:100%">
        <div class="row">
                <div class="form-group">
                    <label for="example-text-input" class="col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Fotografia:</label>
                    <div class="col-xs-12">
                        <form class="save_banco">
                            <div class="input-group">
                                <input type='file' class="form-control form-control-xs" id="d_fot3">
                                <span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
                            </div>
                        </form>
                    </div>
                </div><i class='clearfix'></i>
                <div class="form-group">
                    <label for="example-text-input" class="col-xs-12  col-form-label form-control-label">Descripción:</label>
                    <div class="col-xs-12">
                        <form class="save_banco">
                            <div class="input-group">
                                <textarea class="form-control form-control-xs" id="d_obs3" rows="3" placeholder="Descripción de la fotografía"><?php echo $detalle_descuento_producto->descripcion;?></textarea>
                                <span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
                            </div>
                        </form>
                    </div>
                </div><i class='clearfix'></i>
            </div>
        </div>
    </div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});</script>