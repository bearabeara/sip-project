<?php 
    $url=base_url().'libraries/img/';
    $v=array(0 => 'Parte', 1 => 'Extra', 2 => 'Muestra');
    $rand=rand(10,99999999);
?>
<ul class="nav nav-tabs md-tabs" role="tablist" style="border-bottom: 0px;">
    <li class="nav-item">
        <a class="nav-link config_descuento_producto<?php echo $rand;?>" data-des="<?php echo $descuento_producto->iddes;?>" data-pe="<?php echo $parte_pedido->idpe;?>" href="javascript:" style="padding: 0px 0px 8px 0px !important;">Modificar</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link config_descuento_producto_detalle<?php echo $rand;?> active" data-des="<?php echo $descuento_producto->iddes;?>" data-pe="<?php echo $parte_pedido->idpe;?>" href="javascript:" style="padding: 0px 0px 8px 0px !important;">Detalles</a>
        <div class="slide"></div>
    </li>
</ul>
<div class="list-group">
    <div class="list-group-item" style="max-width:100%">
        <div class="table-responsive">
            <table class="table table-bordered" style="margin-bottom: 0rem;">
                <tr>
                    <td width="15%"><?php echo $v[$parte_pedido->tipo]." ".$parte_pedido->numero;?></td>
                    <td class='img-thumbnail-35'><div class="img-thumbnail-35"></div>
                    <img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-35" data-title="<?php echo $grupo->nombre;?>" data-desc="<br/>">
                    </td>
                    <td width="15%"><?php echo $grupo->codigo;?></td>
                    <td width="50%"><?php echo $grupo->nombre;?></td>
                    <td width="30%"><?php echo $sucursal->nombre_sucursal;?></td>
                </tr>
            </table>
        </div>
    </div>
	<div class="list-group-item" style="max-width:100%">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class='img-thumbnail-50'><div class="img-thumbnail-50">#Item</div></th>
                        <th width="100%">Detalles de descuento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php if(count($detalles_descuento_producto)>0){?>
                    <?php for($i=0;$i<count($detalles_descuento_producto);$i++){$detalle_descuento_producto=$detalles_descuento_producto[$i];
                        $img="sistema/default.jpg";
                        if($detalle_descuento_producto->fotografia!="" && $detalle_descuento_producto->fotografia!=NULL){
                            $img="errores_productos/miniatura/".$detalle_descuento_producto->fotografia;
                        }
                    ?>
                    <tr>
                        <td><div class="item"><?php echo $i+1;?></div>
                            <img src="<?php echo $url.$img;?>" alt="image" data-title="Detalle" data-desc="<?php if($detalle_descuento_producto->descripcion!=""){echo $detalle_descuento_producto->descripcion;}else{echo "<br>";}?>" class="img-thumbnail img-thumbnail-50<?php echo $rand; ?>">
                        </td>
                        <td><?php echo $detalle_descuento_producto->descripcion;?></td>
                        <td><?php
							$conf="";
							$del="";
							if($privilegio->mo1u=="1"){
								$conf=json_encode(array('function'=>'change_detalle_descuento','atribs'=> json_encode(array('desp' => $detalle_descuento_producto->iddesp,'des' => $descuento_producto->iddes,'pe' => $parte_pedido->idpe)),'title'=>'Configurar'));
								$del=json_encode(array('function'=>'confirm_detalle_descuento','atribs'=> json_encode(array('desp' => $detalle_descuento_producto->iddesp,'des' => $descuento_producto->iddes,'pe' => $parte_pedido->idpe)),'title'=>'Eliminar'));
							}
							$this->load->view("estructura/botones/btn_registro",["details"=>"",'config'=>$conf,'delete'=>$del]);
						?></td>
                    </tr>
                    <?php }?>
                <?php }else{?>
                    <tr>
                        <td class="text-center" colspan="3"><h3>0 Registros encontrados...</h3></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light adicionar_detalle_descuento<?php echo $rand;?>" data-des="<?php echo $descuento_producto->iddes;?>" data-pe="<?php echo $parte_pedido->idpe;?>"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar</span></button>
		</div>
    </div>
</div>
<script>$("img.img-thumbnail-35").visor();$(".config_descuento_producto<?php echo $rand;?>").config_descuento_producto();$(".config_descuento_producto_detalle<?php echo $rand;?>").config_descuento_producto_detalle();$(".adicionar_detalle_descuento<?php echo $rand;?>").adicionar_detalle_descuento();$("img.img-thumbnail-50<?php echo $rand; ?>").visor();$(".change_detalle_descuento").change_detalle_descuento();$(".confirm_detalle_descuento").confirm_detalle_descuento();</script>