<?php
$tipo="";
$v=json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
$rand=rand(10,99999);
$total_descuento=0;
$url=base_url().'libraries/img/';
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link config_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"><i class="icofont icofont-home"></i>Pedido</a></div></li>
	<?php for ($i=0; $i < count($partes) ; $i++) { 
		foreach($v as $clave => $opcion){
			if($clave==$partes[$i]->tipo){ $tipo=$opcion;break;}
		}
	?>
	<li class="nav-item"><a class="nav-link config_parte_pedido<?php echo $rand;?>" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></div></li>
	<?php } ?>
	<li class="nav-item"><a class="nav-link new_parte_pedido<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Nuevo +</a></li>
	<li class="nav-item"><a class="nav-link active descuento_producto<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Descuentos</a></li>
	<li class="nav-item"><a class="nav-link pagos<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Depósitos</a></li>
</ul>
	<div class="list-group">
		<div class="list-group-item" style="max-width:100%">
			<div class="table-responsive g-table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="9%">Usuario</th>
							<th width="7%">Lugar</th>
							<th class='img-thumbnail-50'><div class="img-thumbnail-50">#Item</div></th>
							<th width="6%">Código</th>
							<th width="20%">Nombre</th>
							<th width="5%">Sucursal</th>
							<th width="6%">Cant.</th>
							<th width="6%">C/U venta [Bs.]</th>
							<th width="6%">Total costo [Bs.]</th>
							<th width="6%">% de desc.</th>
							<th width="6%">Descuento [Bs.]</th>
							<th width="25%">Observaciónes</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
				<?php if(!empty($descuentos)){
						$total_descuento=0;
				?>
				<?php $i=1; foreach($descuentos as $key => $descuento){
						$parte=$this->lib->search_elemento($partes,'idpp',$descuento->idpp);
						if($parte!=null){
							$usuario=$descuento->nombre_usuario;
							if($descuento->idus==$this->session->userdata("id")){$usuario="Tú";}
							$detalles_descuento_producto=$this->lib->select_from($detalles_descuentos_productos,'iddes',$descuento->iddes);
				?>
						<tr>
							<td><?php echo $usuario;?></td>
							<td><?php echo $v[$parte->tipo].' '.$parte->numero;?></td>
							<td><div id="item"><?php echo $i++;?></div>
								<img src="<?php echo $descuento->fotografia;?>" width="100%" class="img-thumbnail img-thumbnail-50" data-title="<?php echo $descuento->codigo.' '.$descuento->nombre;?>" data-desc="<br>">
							</td>
							<td><?php echo $descuento->codigo;?></td>
							<td><?php echo $descuento->nombre;?></td>
							<td><?php echo $descuento->nombre_sucursal;?></td>
							<td class="text-right"><?php echo $descuento->cantidad.'u.';?></td>
							<td class="text-right"><?php echo number_format($descuento->costo_unitario_venta,2,'.',',');;?></td>
							<td class="text-right"><?php echo number_format($descuento->costo_total,2,'.',',');;?></td>
							<td class="text-right"><?php echo ($descuento->porcentaje*100).'%';?></td>
							<td class="text-right"><ins><?php echo number_format($descuento->descuento,2,'.',',');$total_descuento+=$descuento->descuento;?></ins></td>
							<td>
							<?php echo $descuento->observaciones;
								if($descuento->observaciones!=""){echo "<br/>";}
								for ($d=0; $d < count($detalles_descuento_producto); $d++) { $detalle=$detalles_descuento_producto[$d];
									$img="sistema/miniatura/default.png";
									if($detalle->fotografia!="" && $detalle->fotografia!=NULL){
										$img="errores_productos/miniatura/".$detalle->fotografia;
									}
							?>
									<img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail img-thumbnail-30" data-title="Detalle de descuento" data-desc="<?php echo $detalle->descripcion;?>">
							<?php
								}
							?></td>
							<td><?php
								$conf="";
								$del="";
								if($privilegio->mo1u=="1"){
									$conf=json_encode(array('function'=>'config_descuento_producto','atribs'=> json_encode(array('pe' => $pedido->idpe,'des' => $descuento->iddes)),'title'=>'Configurar'));
									$del=json_encode(array('function'=>'confirm_descuento_producto','atribs'=> json_encode(array('pe' => $pedido->idpe,'des' => $descuento->iddes)),'title'=>'Eliminar'));
								}
								$this->load->view("estructura/botones/btn_registro",["details"=>"",'config'=>$conf,'delete'=>$del]);
							?>
							</td>
						</tr>
				<?php
						}//end if
					}//end foreach ?>
				<?php }else{?>
						<tr>
							<td colspan="13" class="text-center"><h3>0 registros encontrados...</h3></td>
						</tr>
				<?php }?>
					</tbody>
					<thead>
						<tr>
							<th colspan="10" class="text-right">Total descuento: </th>
							<th class="text-right"><?php echo number_format($total_descuento,2,'.',',');?></th>
							<th colspan="2"></th>
						</tr>
					</thead>
				</table>
			</div>
		<?php if($pedido->estado_pago=="0"){?>
			<div class="row text-right" style="padding-right:15px;">
				<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_descuento_producto" data-pe="<?php echo $pedido->idpe;?>"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar registro</span></button>
			</div>
		<?php } ?>
		</div>
	</div>
	<script>$("img.img-thumbnail-50").visor();$("img.img-thumbnail-30").visor();$(".config_pedido<?php echo $rand;?>").config_pedido();$(".config_parte_pedido<?php echo $rand;?>").config_parte_pedido();$(".new_parte_pedido<?php echo $rand;?>").new_parte_pedido();$(".descuento_producto<?php echo $rand;?>").descuento_producto();$(".pagos<?php echo $rand;?>").pagos();$(".new_descuento_producto").new_descuento_producto();$(".config_descuento_producto").config_descuento_producto();$(".confirm_descuento_producto").confirm_descuento_producto();</script>