<?php
	$help1='title="Moneda" data-content="Ingrese el nombre de la moneda, el contenído puede ser alfanumérico de 2 a 100 caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help2='title="Simbolo" data-content="Ingrese el simbolo de la moneda <b>sin espacios</b> de 1 a 10 caractereres."';
	$help3='title="Tipo" data-content="Seleccione el tipo de movimiento si es de<b> Venta o Compra</b>"';
	$help4='title="Monto" data-content="Engrese el monto equivalente en bolivianos mayor a 0 y menor a 9999.99., <b>solo de acepta dos decimales</b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$movimientos = array('COMPRA');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-4 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Moneda:</label>
				<div class="col-sm-8 col-xs-12">
					<form class="save_tipo_cambio" data-type="<?php echo $type;?>" data-tipos="p_tip">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="tc_nom" minlength="2" maxlength="100" placeholder="Dólares">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-4 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Simbolo:</label>
				<div class="col-sm-8 col-xs-12">
					<form class="save_tipo_cambio" data-type="<?php echo $type;?>" data-tipos="p_tip">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="tc_abr" minlength="1" maxlength="10" placeholder="$">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-4 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Tipo:</label>
				<div class="col-sm-8 col-xs-12">
					<div class="input-group">
						<select class="form-control input-xs select_banco" disabled="disabled">
					<?php for($i=0; $i < count($movimientos); $i++){ ?>
							<option value="<?php echo $i;?>"><?php echo $movimientos[$i];?></option>
					<?php }?>
						</select>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-4 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Monto:</label>
				<div class="col-sm-8 col-xs-12">
					<form class="save_tipo_cambio" data-type="<?php echo $type;?>" data-tipos="p_tip">
						<div class="input-group">
							<input class='form-control input-xs' type="number" id="tc_mon" min="0" max="9999.99" placeholder="0.0">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
		</div>
	</div>
</div>
<script>Onfocus("tc_nom");$('[data-toggle="popover"]').popover({html:true});$("form.save_tipo_cambio").submit(function(e){$(this).save_tipo_cambio();e.preventDefault();});</script>