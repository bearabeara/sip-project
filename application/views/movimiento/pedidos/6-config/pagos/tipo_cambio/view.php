<?php $rand=rand(10,9999999);
	$movimiento = array('COMPRA','VENTA');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="0%">#item</th>
						<th width="45%">Moneda</th>
						<th width="15%">Simbolo</th>
						<th width="10%">tipo</th>
						<th width="30%">Monto [Bs.]</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?php if(count($tipos)>0){ $cont=1;?>
				<?php for($i=0; $i < count($tipos); $i++){ $tipo=$tipos[$i];
				?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo $tipo->moneda;?></td>
						<td><?php echo $tipo->simbolo;?></td>
						<td><?php echo $movimiento[$tipo->tipo];?></td>
						<td class="text-right"><?php echo number_format($tipo->monto,2,'.',',');?></td>
						<td>
						<?php if($tipo->tipo=="0"){?>
							<?php
								$conf=""; if(true){ $conf=json_encode(array('function'=>'config_tipo_cambio','atribs'=> json_encode(array('tc' => $tipo->idtc)),'title'=>'Configurar'));}
								$del=""; if(true){ $del=json_encode(array('function'=>'confirm_tipo_cambio','atribs'=> json_encode(array('tc' => $tipo->idtc)),'title'=>'Eliminar'));}
							?>
							<?php $this->load->view("estructura/botones/btn_registro",["details"=>"",'config'=>$conf,'delete'=>$del]);?>
						<?php }?>
						</td>
					</tr>
				<?php
				}//end for ?>
			<?php }else{?>
					<tr>
						<td colspan="6" class="text-center"><h3>0 registros encontrados...</h3></td>
					</tr>
			<?php }?>
				</tbody>
			</table>
		</div>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_tipo_cambio<?php echo $rand;?>" data-type="new-modal"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar tipo de cambio</span></button>
		</div>
	</div>
</div>
<script>;$(".new_tipo_cambio<?php echo $rand;?>").new_tipo_cambio();$(".config_tipo_cambio").config_tipo_cambio();$(".confirm_tipo_cambio").confirm_tipo_cambio();</script>