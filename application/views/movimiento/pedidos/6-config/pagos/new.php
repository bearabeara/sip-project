<?php
	$help1='title="Fecha de depósito" data-content="Ingrese la fecha cuando se realizó el depósito en formato 2000-01-31."';
	$help11='title="Depositante" data-content="Ingrese el nombre de quien realiza el deposito en formato alfanumérica de 2 a 500 caracteres, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$help2='title="Propietario" data-content="Seleccione el propietario de la cuenta bancaria."';
	$help3='title="Banco" data-content="Seleccione la entidad bancaria donde se deposito el pago."';
	$help4='title="N° De Cuenta" data-content="Ingrese el número de cuenta donde realizó el deposito."';
	$help5='title="Tipo de moneda" data-content="Seleccione el tipo de modena en que realizó el deposito."';
	$help6='title="Monto" data-content="Ingrese el monto depositado, mayor a 0 y menor o igual a 999999999.99"';
	$help7='title="Monto en bolivianos" data-content="contiene el monto en moneda boliviana, si es erroneo pulse en actualizar para volver a calcular el valor, el valor debe sr mayor a cero y menor o igual a '.$max.'"';
	$help8='title="Observaciónes del pago" data-content="Ingrese unas observaciónes alfanumérica de 0 a 900 caracteres, ademas la observación solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Fecha de deposito:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_pago" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<input class='form-control input-xs' type="date" id="p_fec" value="<?php echo date('Y-m-d');?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Depositante:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_pago" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<input type="text" class="form-control input-xs" id="p_dep" maxlength="500" placeholder="Nombre del depositante">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Propietario:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_pago" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<select class="form-control input-xs select_persona" id="p_pro" data-banco="p_ban" data-cuenta="p_cta">
								<option value="">Seleccionar...</option>
						<?php for($i=0; $i < count($personas); $i++){$persona=$personas[$i]; 
								$es_empleado=false;
								$es_usuario=false;
								$es_directivo=false;
								$empleado=$this->lib->search_elemento($empleados,"ci",$persona->ci);
								$usuario=$this->lib->search_elemento($usuarios,"ci",$persona->ci);
								$directivo=$this->lib->search_elemento($directivos,"ci",$persona->ci);
								if($empleado!=null){if($empleado->estado=="1"){$es_empleado=true;}}
								if($usuario!=null){$es_usuario=true;}
								if($directivo!=null){$es_directivo=true;}
								if($es_empleado || $es_usuario || $es_directivo){
						?>
								<option value="<?php echo $persona->ci;?>"><?php echo $persona->nombre_completo; ?></option>
						<?php }//end if
							}//end for ?>
							</select>
							<span class="input-group-addon form-control-sm view_personas"><i class='fa fa-cog'></i></span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Banco:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_pago" data-pe="<?php echo $pedido->idpe;?>">
						<div class="input-group">
							<select class="form-control input-xs select_banco" id="p_ban" data-cuenta="p_cta">
								<option value="">Seleccionar...</option>
							</select>
							<span class="input-group-addon form-control-sm new_banco" data-type="new"><i class='fa fa-plus'></i></span>
							<span class="input-group-addon form-control-sm view_bancos"><i class='fa fa-cog'></i></span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>N° de cuenta:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class="form-control input-xs" id="p_cta">
							<option value="">Seleccionar...</option>
						</select>
						<span class="input-group-addon form-control-sm new_cuenta"><i class='fa fa-plus'></i></span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Tipo de moneda:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class="form-control input-xs select_tipo_cambio" id="p_tip" data-abr-cta="p_abr_tc">
							<option value="">Seleccionar...</option>
					<?php for ($i=0; $i < count($tipos); $i++) { $tipo_cambio=$tipos[$i];
							if($tipo_cambio->tipo==0){
					?>
							<option value="<?php echo $tipo_cambio->idtc;?>"><?php echo $tipo_cambio->moneda.' ('.$tipo_cambio->monto.')';?></option>
				<?php }}?>
						</select>
						<span class="input-group-addon form-control-sm new_tipo_cambio" data-type="new"><i class='fa fa-plus'></i></span>
						<span class="input-group-addon form-control-sm view_tipos"><i class='fa fa-cog'></i></span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Monto depositado:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input class='form-control input-xs moneda_nacional' type="number" id="p_mon" min="0" placeholder="0.0" step="any" max="999999999.99">
						<span class="input-group-addon form-control-sm" id="p_abr_tc"></span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Monto [Bs.]:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input class='form-control input-xs' type="number" id="p_bs" min="0" placeholder="0.0" disabled="disabled" max="<?php echo $max;?>" data-max="<?php echo $max;?>">
						<span class="input-group-addon form-control-sm">Bs.</span>
						<span class="input-group-addon form-control-sm moneda_nacional"><i class="fa fa-refresh"></i></span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Descripción:</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<textarea class="form-control input-xs" id="p_des" placeholder='Descripción del material' maxlength="300" rows="3"></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help8;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>Onfocus("p_fec");$('[data-toggle="popover"]').popover({html:true});$(".view_personas").view_personas();$(".new_banco").new_banco();$(".view_bancos").view_bancos();$(".select_persona").select_persona();$(".select_banco").select_banco();$(".new_cuenta").new_cuenta();$(".new_tipo_cambio").new_tipo_cambio();$(".view_tipos").view_tipos();$(".select_tipo_cambio").select_tipo_cambio();$(".moneda_nacional").moneda_nacional();$("form.save_pago").submit(function(e){$(this).save_pago();e.preventDefault();});</script>