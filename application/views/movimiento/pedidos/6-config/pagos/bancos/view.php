<?php $url=base_url().'libraries/img/';$rand=rand(10,9999999);?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th class="img-thumbnail-50"><div class="img-thumbnail-50">#item</div></th>
						<th width="55%">Razón</th>
						<th width="5%">Web</th>
						<th width="40%">Descripción</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?php if(count($bancos)>0){ $cont=1;?>
				<?php for($i=0; $i < count($bancos); $i++){ $banco=$bancos[$i]; 
						$img="sistema/miniatura/default.jpg";
						if($banco->fotografia!="" && $banco->fotografia!=NULL){$img="bancos/miniatura/".$banco->fotografia;}
				?>
					<tr>
						<td><div class="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail img-thumbnail-50" data-title="<?php echo $banco->razon; ?>" data-desc="<br>"></td>
						<td><?php echo $banco->razon;?></td>
						<td><?php echo $banco->url;?></td>
						<td><?php echo $banco->descripcion;?></td>
						<td>
						<?php
							$conf=""; if(true){ $conf=json_encode(array('function'=>'config_banco','atribs'=> json_encode(array('ba' => $banco->idba)),'title'=>'Configurar'));}
							$del=""; if(true){ $del=json_encode(array('function'=>'confirm_banco','atribs'=> json_encode(array('ba' => $banco->idba)),'title'=>'Eliminar'));}
						?>
						<?php $this->load->view("estructura/botones/btn_registro",["details"=>"",'config'=>$conf,'delete'=>$del]);?>
						</td>
					</tr>
				<?php
				}//end for ?>
			<?php }else{?>
					<tr>
						<td colspan="4" class="text-center"><h3>0 registros encontrados...</h3></td>
					</tr>
			<?php }?>
				</tbody>
			</table>
		</div>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_banco<?php echo $rand;?>" data-type="new-modal"><i class="fa fa-plus"></i><span class="m-l-10">Adicionar banco</span></button>
		</div>
	</div>
</div>
<script>$(".img-thumbnail-50").visor();$(".new_banco<?php echo $rand;?>").new_banco();$(".config_banco").config_banco();$(".confirm_banco").confirm_banco();</script>