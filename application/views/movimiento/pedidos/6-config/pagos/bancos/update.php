<?php
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>"';
	$help2='title="Nombre o razón social" data-content="Ingrese el nombre o razón social del banco, el nombre debe tener de 2 a 200 caracteres y solo se aceptan los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help3='title="Sitio Web" data-content="Ingrese la dirección web del banco con el formato http://wwww.ejemplo.com.bo, se acepta una url con una maximo de 150 caracteres"';
	$help4='title="Descripción" data-content="la descripción debe poseer un formato alfanumerico de 0 a 500 caracteres <b>puede incluir espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row hidden-sm"><div class="col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label">Fotografía:</label>
				<div class="col-sm-9 col-xs-12">
					<form class="update_banco" data-ba="<?php echo $banco->idba;?>" data-bancos="p_ban" data-cuentas="p_cta">
						<div class="input-group">
							<input class='form-control input-xs' type="file" id="b_fot">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Razón:</label>
				<div class="col-sm-9 col-xs-12">
					<form class="update_banco" data-ba="<?php echo $banco->idba;?>" data-bancos="p_ban" data-cuentas="p_cta">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="b_raz" placeholder="Nombre de banco" minlength="2" maxlength="200" value="<?php echo $banco->razon;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label">Sitio web:</label>
				<div class="col-sm-9 col-xs-12">
					<form class="update_banco" data-ba="<?php echo $banco->idba;?>" data-bancos="p_ban" data-cuentas="p_cta">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="b_url" placeholder="http://www.banco.com.bo" maxlength="300" value="<?php echo $banco->url;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label">Descripción:</label>
				<div class="col-sm-9 col-xs-12">
					<form class="update_banco" data-ba="<?php echo $banco->idba;?>" data-bancos="p_ban" data-cuentas="p_cta">
						<div class="input-group">
							<textarea id="b_des" class="form-control input-xs" placeholder="Descripción de banco" rows="3" maxlength="500"><?php echo $banco->descripcion;?></textarea>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
		</div>
	</div>
</div>
<script>Onfocus("b_raz");$('[data-toggle="popover"]').popover({html:true});$("form.update_banco").submit(function(e){$(this).update_banco();e.preventDefault();});</script>