<?php 
	$v =json_decode(json_encode(array('0' => 'Parte', '1' => 'Extra', '2' => 'Muestra')));
	$rand=rand(10,999999);
	$total_monto=0;
	$total_monto_bs=0;
	$total_pedido=0;
	$total_descuento=0;
	$total_recibido=0;
	$total_recibido_bs=0;
	for($i=0; $i < count($detalles_pedido); $i++){ $detalle_pedido=$detalles_pedido[$i];
		$total_pedido+=$this->lib->desencriptar_num($detalle_pedido->cv);
	}
	$total_pedido-=$descuento;
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link config_pedido<?php echo $rand;?>" data-pe="<?php echo $pedido->idpe;?>" href="javascript:" role="tab"><i class="icofont icofont-home"></i>Pedido</a></div></li>
<?php for ($i=0; $i < count($partes) ; $i++) { 
		foreach ($v as $clave => $opcion) {
			if($clave==$partes[$i]->tipo){ $tipo=$opcion;break;}
		}
?>
	<li class="nav-item"><a class="nav-link config_parte_pedido<?php echo $rand;?>" href="javascript:" role="tab" data-pp="<?php echo $partes[$i]->idpp;?>"><i class="icofont icofont-ui-user"></i><?php echo $tipo;?> <?php echo $partes[$i]->numero;?></a></div></li>
<?php } ?>
	<li class="nav-item"><a class="nav-link new_parte_pedido<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Nuevo +</a></li>
	<li class="nav-item"><a class="nav-link descuento_producto<?php echo $rand;?>" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Descuentos</a></li>
	<li class="nav-item"><a class="nav-link active pagos" href="javascript:" role="tab" data-pe="<?php echo $pedido->idpe;?>"><i class="icofont icofont-ui-user"></i> Depósitos</a></li>
</ul>
<div class="list-group">
	<div class="list-group-item text-right" style="max-width:100%">
		<strong>Pedido: </strong><?php echo $pedido->nombre;?>
	</div>
</div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="1%">#</th>
						<th width="15%">Registrado por</th>
						<th width="11%">Fecha de deposito</th>
						<th width="11%">Depositante</th>
						<th width="16%">Propietario de cuenta</th>
						<th width="13%">Banco</th>
						<th width="5%">N° de cuenta</th>
						<th width="6%">Monto depositado</th>
						<th width="6%">Tipo de cambio</th>
						<th width="6%">Monto [Bs.]</th>
						<th width="6%">Descuento</th>
						<th width="6%">Monto recibido</th>
						<th width="6%">Monto recibido [Bs.]</th>
                        <th width="6%">Saldo Pendiente [Bs.]</th>
						<th width="19%">Observaciónes</th>
					<?php if($pedido->estado_pago=="0"){?>
						<th width="2%"></th>
					<?php } ?>
					</tr>
				</thead>
				<tbody>
			<?php if(count($pagos)>0){?>
				<?php for($i=0; $i<count($pagos);$i++){$pago=$pagos[$i];
					$usuario="";
					if($pago->idus==$this->session->userdata("id")){$usuario="Tú";}
					if($usuario==""){
						$usuario=$this->lib->search_elemento($personas,"ci",$pago->ci_usuario);
						if($usuario!=null){$usuario=$usuario->nombre_completo;}
					}
                    $fecha=$pago->fecha;
					$banco=$this->lib->search_elemento($bancos,"idba",$pago->idba);
					if($banco!=null){
						$banco=$banco->razon;
					}
					$propietario=$this->lib->search_elemento($personas,"ci",$pago->ci_persona);
					if($propietario!=null){
						$propietario=$propietario->nombre_completo;
					}
					$monto_depositado=$this->lib->desencriptar_num($pago->monto);
					$monto_bs=$this->lib->desencriptar_num($pago->monto_bs);
					$moneda="";
					$tc=$this->lib->search_elemento($tipos_cambio,"idtc",$pago->idtc);
					if($tc!=null){
						$moneda=$tc->simbolo;
					}
					$descuentos=$this->lib->select_from($descuentos_pagos,"idpa",$pago->idpa);
					$s_descuento=0;
					for($j=0;$j<count($descuentos);$j++){
                            $descuento=json_decode(json_encode($descuentos[$j]));
						$s_descuento+=($this->lib->desencriptar_num($descuento->monto)*1);
					}
					$s_descuento=number_format($s_descuento,1,'.','');
					$recibido_bs=number_format((($monto_depositado-$s_descuento)*$pago->tipo_cambio),1,'.','');
				?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo $usuario;?></td>
						<td><?php echo $this->lib->format_date($fecha,'dl ml Y');?></td>
						<td><?php echo $pago->depositante;?></td>
						<td><?php echo $propietario;?></td>
						<td><?php echo $banco;?></td>
						<td><?php echo $pago->cuenta;?></td>						
						<td class="text-right"><?php echo number_format($monto_depositado,2,'.',',').$moneda;$total_monto+=$monto_depositado;?></td>
						<td class="text-right"><?php echo $pago->tipo_cambio;?></td>
						<td class="text-right"><?php echo number_format($monto_bs,2,'.',',')."Bs.";$total_monto_bs+=$monto_bs;?></td>
						<td class="text-right"><ins><a href="" class="view_descuentos" data-pe="<?php echo $pedido->idpe;?>" data-pa="<?php echo $pago->idpa;?>"><?php echo number_format($s_descuento,2,'.',',').$moneda;$total_descuento+=$s_descuento; ?></a></ins></td>
						<td class="text-right"><?php echo number_format(($monto_depositado-$s_descuento),2,'.',',').$moneda;$total_recibido+=($monto_depositado-$s_descuento);?></td>
						<td class="text-right"><?php echo number_format($recibido_bs,2,'.',',')."Bs.";$total_recibido_bs+=$recibido_bs;?></td>
                        <td class="text-right"><?php echo number_format($total_pedido - $total_recibido_bs,2,'.',',')."Bs.";?></td>
						<td><?php echo $pago->observacion;?></td>
					<?php if($pedido->estado_pago=="0"){?>
						<td>
						<?php
						if($pago->idus==$this->session->userdata("id")){
							$conf=json_encode(array('function'=>'config_pago','atribs'=> json_encode(array('pa' => $pago->idpa,'pe'=>$pedido->idpe)),'title'=>'Configurar'));
							$del=json_encode(array('function'=>'confirm_pago','atribs'=> json_encode(array('pa' => $pago->idpa,'pe'=>$pedido->idpe)),'title'=>'Eliminar'));
							$this->load->view("estructura/botones/btn_registro",['config'=>$conf,'delete'=>$del]);
						}else{?>
							<div class="btn-group">
							  	<div class="dropdown-menu g-dropdown-menu-right">
									<button class="dropdown-item disabled" type="button"><i class="icon-tools2"></i><span class="dropdown-item-text"> Configurar</span></button>
									<button class="dropdown-item disabled" type="button"><i class="fa fa-trash"></i><span class="dropdown-item-text"> Eliminar</span></button>
								</div>
							</div>
					<?php } ?>
						</td>
					<?php } ?>
					</tr>
				<?php } ?>
			<?php }else{?>
					<tr><th colspan="15" class="text-center"><h3>0 depositos registrados...</h3></th></tr>
			<?php }?>
				</tbody>
				<thead>
					<tr>
						<th colspan="7" class="text-right">TOTAL</th>
						<th class="text-right"><?php echo number_format($total_monto,2,'.',','); ?></th>
						<th></th>
						<th class="text-right"><?php echo number_format($total_monto_bs,2,'.',','); ?></th>
						<th class="text-right" style="font-size: .9rem;"><?php echo number_format($total_descuento,2,'.',','); ?></th>
						<th class="text-right" style="font-size: .9rem;"><?php echo number_format($total_recibido,2,'.',','); ?></th>
						<th class="text-right" style="font-size: .9rem;"><?php echo number_format($total_recibido_bs,2,'.',','); ?></th>
						<th colspan="3"></th>
					</tr>
					<tr>
						<th colspan="12" class="text-right">TOTAL COSTO DE PEDIDO [Bs.]</th>
						<th class="text-right"><?php echo number_format($total_pedido,2,'.',','); ?></th>
						<th colspan="3"></th>
					</tr>
					<tr>
						<th colspan="12" class="text-right">SALDO PENDIENTE [Bs.]</th>
						<th class="text-right"><?php echo number_format($total_pedido-$total_recibido_bs,2,'.',','); ?></th>
						<th colspan="3"></th>
					</tr>
				</thead>
			</table>
		</div>
	<?php if($pedido->estado_pago=="0"){?>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light<?php if(($total_pedido-$total_monto_bs)>0){?> new_pago<?php }?>"<?php if(($total_pedido-$total_monto_bs)>0){?> data-pe="<?php echo $pedido->idpe;?>"<?php }else{?>disabled="disabled"<?php }?>><i class="fa fa-plus"></i><span class="m-l-10" id="datas" data-max="<?php echo number_format($total_pedido-$total_monto_bs,1,'.','');?>">Adicionar registro</span></button>
		</div>
	<?php } ?>
	</div>
</div>
<script>$(".config_pedido<?php echo $rand;?>").config_pedido();$(".config_parte_pedido<?php echo $rand;?>").config_parte_pedido();$(".new_parte_pedido<?php echo $rand;?>").new_parte_pedido();$(".pagos").pagos();$(".new_pago").new_pago();$(".config_pago").config_pago();$(".confirm_pago").confirm_pago();$("a.view_descuentos").view_descuentos();$(".descuento_producto<?php echo $rand;?>").descuento_producto();</script>