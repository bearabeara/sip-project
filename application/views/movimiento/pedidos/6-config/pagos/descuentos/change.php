<?php
	$help1='title="Monto" data-content="Ingrese el monto descontado en la transacción, el monto debe ser <strong>mayor a cero</strong> y <strong>menor o igual a 9999999.9</strong>"';
	$help2='title="Observación" data-content="Ingrese las observaciónes por la cuales se efectuo el descuento, solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$simbolo="";
	$tipo_cambio=$this->lib->search_elemento($tipos_cambio,"idtc",$pago->idtc);
	if($tipo_cambio!=null){$simbolo=$tipo_cambio->simbolo;}
	$descuento=$this->lib->desencriptar_num($descuento_pago->monto)*1;
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="form-group">
			<div class="col-sm-4 col-xs-12"><label for="example-text-input" class="form-control-label"> Monto descontado:</label></div>
			<div class="col-sm-8 col-xs-12">
				<form class="update_descuento" data-dpa="<?php echo $descuento_pago->iddpa;?>">
					<div class="input-group">
						<input type="number" class="form-control form-control-xs" id="p3_mon" placeholder="0" value="<?php echo $descuento;?>" />
						<span class="input-group-addon form-control-sm"><?php echo $simbolo;?></span>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-4 col-xs-12"><label for="example-text-input" class="form-control-label"> Observación:</label></div>
			<div class="col-sm-8 col-xs-12">
				<div class="input-group">
					<textarea class="form-control form-control-xs" id="p3_obs" rows="3" placeholder="Observaciónes de descuento"><?php echo $descuento_pago->observacion;?></textarea>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("form.update_descuento").submit(function(e){$(this).update_descuento();e.preventDefault();});</script>