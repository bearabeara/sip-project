<?php
	$total_descuento=0;
	$moneda="";
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%" id="datos-pago" data-pa="<?php echo $pago->idpa;?>" data-pe="<?php echo $pago->idpe;?>">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="1%">#</th>
						<th width="30%">Usuario</th>
						<th width="20%">Monto</th>
						<th width="50%">Observación</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?php if(!empty($descuentos)){?>
				<?php for($i=0;$i<count($descuentos);$i++){ $descuento=$descuentos[$i];
					$usuario="";
					$monto=$this->lib->desencriptar_num($descuento->monto)*1;
					if($descuento->idus==$this->session->userdata("id")){$usuario="Tú";}
					if($usuario==""){
						$usuario=$this->lib->search_elemento($usuarios,"idus",$descuento->idus);
						if($usuario!=null){$usuario=$usuario->nombre_completo;}
					}
					$tipo_cambio=$this->lib->search_elemento($tipos_cambio,"idtc",$pago->idtc);
					if($tipo_cambio!=null){$moneda=$tipo_cambio->simbolo;}
				?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo $usuario;?></td>
						<td class="text-right"><?php echo number_format($monto,2,'.',',').$moneda;$total_descuento+=$monto;?></td>
						<td><?php echo $descuento->observacion;?></td>
						<td>
					<?php
						if($descuento->idus==$this->session->userdata("id")){
							$conf=json_encode(array('function'=>'config_descuento','atribs'=> json_encode(array('dpa' => $descuento->iddpa,'pe'=>$pago->idpe)),'title'=>'Configurar'));
							$del=json_encode(array('function'=>'confirm_descuento','atribs'=> json_encode(array('dpa' => $descuento->iddpa,'pe'=>$pago->idpe)),'title'=>'Eliminar'));
							$this->load->view("estructura/botones/btn_registro",['config'=>$conf,'delete'=>$del]);
						}else{?>
							<div class="btn-group">
							  	<div class="dropdown-menu g-dropdown-menu-right">
									<button class="dropdown-item disabled" type="button"><i class="icon-tools2"></i><span class="dropdown-item-text"> Configurar</span></button>
									<button class="dropdown-item disabled" type="button"><i class="fa fa-trash"></i><span class="dropdown-item-text"> Eliminar</span></button>
								</div>
							</div>
					<?php } ?>
						</td>
					</tr>
				<?php }?>
			<?php }else{?>
					<tr><td colspan="5" class="text-center"><h3>0 registros encontrados.</h3></td></tr>
			<?php }?>
				</tbody>
				<thead>
					<tr>
						<th colspan="2" class="text-right">TOTAL DESCUENTO: </th>
						<th class="text-right"><?php echo number_format($total_descuento,2,'.',',').$moneda?></th>
						<th colspan="2"></th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_descuento"><i class="fa fa-plus"></i><span class="m-l-10" id="datas" data-max="227.4">Adicionar descuento</span></button>
		</div>
	</div>
</div>
<script>$(".new_descuento").new_descuento();$(".config_descuento").config_descuento();$(".confirm_descuento").confirm_descuento();</script>