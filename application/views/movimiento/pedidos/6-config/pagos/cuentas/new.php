<?php
	$help1='title="Banco" data-content="Banco donde se creara la cuenta"';
	$help2='title="Propietario" data-content="Propietario de la cuenta a ser creada."';
	$help3='title="N° de cuenta" data-content="Ingrese el número de cuenta del propietario en el banco, se acepta de <b>5 a 100 caracteres</b>."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row hidden-sm"><div class="col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Banco:</label>
				<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<select class="form-control input-xs" disabled="disabled">
								<option value="">Seleccionar...</option>
						<?php for ($i=0; $i < count($bancos); $i++) { ?>
								<option <?php if($bancos[$i]->idba==$banco->idba){?>SELECTED="SELECTED"<?php }?>><?php echo $bancos[$i]->razon;?></option>
						<?php } ?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Propietario de cuenta:</label>
				<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<select class="form-control input-xs" disabled="disabled">
								<option value="">Seleccionar...</option>
						<?php for ($i=0; $i < count($personas); $i++) { ?>
								<option <?php if($personas[$i]->ci==$persona->ci){?>SELECTED="SELECTED"<?php }?>><?php echo $personas[$i]->nombre_completo;?></option>
						<?php } ?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-3 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>N° Cuenta:</label>
				<div class="col-sm-9 col-xs-12">
					<form class="save_cuenta" data-ci="<?php echo $persona->ci;?>" data-ba="<?php echo $banco->idba;?>" data-cuentas="c_cta">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="n_cta" placeholder="N° de cuenta bancaria" minlength="5" maxlength="100">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
		</div>
	</div>
</div>
<script>Onfocus("n_cta");$('[data-toggle="popover"]').popover({html:true});</script>