<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<td width="100%">
						<form class="search_producto" data-type="search">
							<div class="input-group">
								<input type="search" id="s2_nom" class="form-control form-control-xs" maxlength="100" placeholder='Buscar nombre...'<?php if(isset($type)){?> data-type="<?php echo $type;?>"<?php }?><?php if(isset($tipo)){?> data-tipo="<?php echo $tipo;?>"<?php }?><?php if(isset($cliente)){?> data-cl="<?php echo $cliente->idcl;?>"<?php }?> style="margin-left: 0px;"/>
								<span class="input-group-addon form-control-sm search_producto" data-type="search"><i class='icon-search2'></i></span>
							</div>
						</form>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>Onfocus("s2_nom"); $(".search_producto").search_producto();</script>