<?php 
$help1='title="Alerta" data-content="El producto no tiene ninguna categoría registrada."';
$help2='title="Alerta" data-content="El empleado no tiene ningun proceso asignado."';
$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
$url=base_url().'libraries/img/';
$productos_asignados=json_decode($pa);
if($type=="emp_new_prod"){
	$str_proceso_empleado="";
	for($k=0; $k < count($procesos_empleado); $k++){ $pr=$procesos_empleado[$k];
		$proceso=$this->lib->search_elemento($procesos,'idpr',$pr->idpr);
		if($proceso!=null){
			if($str_proceso_empleado==""){ $str_proceso_empleado=$proceso->nombre;}else{ $str_proceso_empleado.=", ".$proceso->nombre;}
		}
	}
	if($str_proceso_empleado!=""){ $str_proceso_empleado="<strong>".$str_proceso_empleado."</strong>";}
}
?>
<div class="table-responsive g-table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="img-thumbnail-50"><div class="img-thumbnail-50"></div>#Item</th>
				<th width="10%" class="hidden-sm">Código</th>
				<th width="60%">Nombre de producto</th>
				<th width="25%" class="hidden-sm">Categorías</th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i <count($productos); $i++){ $producto=$productos[$i];
				$img="sistema/miniatura/default.jpg";
				if($producto->portada!="" && $producto->portada!=null){
					$v=explode("|", $producto->portada);
					if(count($v==2)){
						if($v[0]=="1"){
							$control=$this->lib->search_elemento($producto_imagenes,'idpi',$v[1]);
							if($control!=null){ $img="productos/miniatura/".$control->archivo;}
						}
						if($v[0]=="3"){
							$control=$this->lib->search_elemento($producto_imagenes_color,'idpig',$v[1]);
							if($control!=null){ $img="productos/miniatura/".$control->archivo;}
						}
					}
				}
				$grupos=$this->lib->select_from($productos_grupos,'idp',$producto->idp);
			?>
				<tr>
					<td>
						<div id="item"><?php echo $i+1;?></div>
						<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-50" data-title="<?php echo $producto->codigo.' '.$producto->nombre;?>" data-desc="<br>">
					</td>
					<td class="hidden-sm"><?php echo $producto->codigo;?></td>
					<td><?php echo $producto->nombre; ?>
					<?php if(count($grupos)>0){?>
							<br><ul class="list-group visible-sm" style="font-size:10px; padding: 1px">
								<?php for ($c=0; $c < count($grupos) ; $c++) { $grupo=$grupos[$c]; ?>
									<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
											$co="";if($grupo->nombre_c!=""){ $co=$grupo->nombre_c;}
									?>
										<li class="list-group-item" style="padding:2px 3px; width:100%; border-color:<?php echo $grupo->codigo_c;?>"><?php echo $gr.$co;?></li>
								<?php }?>
							</ul>
					<?php }?>
					</td>
					<td class="hidden-sm">
					<?php
						if(count($grupos)>0){
						?>
							<ul class="list-group" style="font-size:10px; padding: 1px">
							<?php foreach($grupos as $key => $grupo){?>
								<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
									?>
									<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; width:100%; background:<?php echo $grupo->codigo_c;?>"><?php echo $gr.$grupo->nombre_c;?></li>
							<?php }//end foreach?>
								</ul>
						<?php }//end if ?>
					</td>
					<td style="text-align: right;">
								<?php $asignado=false; foreach($productos_asignados as $key => $pa){ if($pa==$producto->idp){ $asignado=true;break; } } ?>
								<?php if($type=="new" || $type=="new_parte"){?>
								<button type="button" class="btn <?php if(!$asignado){ ?>btn-inverse-primary add_producto<?php }else{?>btn-default<?php }?>  btn-mini waves-effect waves-light"<?php if(!$asignado){ ?> data-p="<?php echo $producto->idp?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{?> disabled="disabled"<?php }?>>
									<i class="fa fa-plus"></i> add
								</button>
								<?php } ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<script>$('[data-toggle="popover"]').popover({html:true});$("img.img-thumbnail-50").visor();$(".add_producto").add_producto();
	//$(".btn-add-prod").click(function(e){$(this).add_producto();});$(".btn-change-prod").click(function(e){ $(this).add_producto();});$("button.btn_add_emp_prod").click(function(e){ $(this).btn_add_emp_prod();});</script>
