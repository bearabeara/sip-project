<?php 
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
	$img="sistema/miniatura/default.jpg";
	$id=$grupo->idp."-".rand(0,9999999);
	$control=0;
	$rand=rand(10,9999999);
?>
<tr class="row-producto" id="<?php echo $grupo->idpgrc."-".rand(1,99999);?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-p="<?php echo $producto->idp;?>"<?php if(isset($parte_pedido)){?> data-pp="<?php echo $parte_pedido->idpp;?>"<?php }?> <?php if(isset($iddp)){?> data-dp="<?php echo $iddp;?>"<?php }?>>
	<?php $pgc=$grupo->idpgrc."-".rand(1,999999);?>
	<td><div class="item"></div><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre." - ".$grupo->nombre;?>" data-desc="<br>"></td>
	<td><?php echo $grupo->nombre;?></td>
	<td>
		<?php $costo_unitario=$grupo->costo; if($tipo==2){$costo_unitario*=2;}?>
		<div class="input-group input-80">
			<input class="form-control form-control-sm cu<?php echo $rand;?>" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario;?>" data-value="<?php echo $costo_unitario;?>" data-type='number' data-max="999999.9">
			<span class="input-group-addon form-control-sm">Bs.</span>
		</div>
	</td>
	<?php for($i=0;$i<count($sucursales);$i++){$sucursal=$sucursales[$i]; 
		$pgc_sc=$grupo->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
	?>
        <td>
            <div class="input-group input-100">
            	<input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" placeholder="0" min="1" max="9999" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $idtb;?>" data-type="new">
                <span class="input-group-addon form-control-sm">u.</span>
            </div>
        </td>
	<?php }?>
		<td>
			<div class="input-group input-100">
				<input class="form-control form-control-sm" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled">
				<span class="input-group-addon form-control-sm">u.</span>
            </div>
		</td>
        <td>
          <div class="input-group input-100">
            <input class="form-control form-control-sm" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled">
            <span class="input-group-addon form-control-sm">Bs.</span>
          </div>
        </td>
        <td>
            <div class="input-group input-100">
              <input class="form-control form-control-sm costo_venta<?php echo $rand;?>" id="costo_venta<?php echo $pgc;?>" placeholder="0" disabled="disabled">
              <span class="input-group-addon form-control-sm">Bs.</span>
            </div>
        </td>
		<td>
			<div class="input-group input-150">
				<textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"></textarea>
				<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
			</div>
		</td>
		<td>
			<span class="g-control-accordion" style="text-align: right;">
 				<button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $idtb;?>'><i class="icon-close"></i></button>
			</span>
			<script>$('[data-toggle="popover"]').popover({html:true});$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$(".drop_elemento<?php echo $rand;?>").drop_elemento();$("img.img-thumbnail-45").visor();</script>
		</td>
</tr>