<?php 
	$help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
	$img="sistema/miniatura/default.jpg";
	$control=0;
	$rand=rand(10,9999999);
	$pgc=$grupo[0]->idpgrc."-".rand(1,9999);
?>
<td><div class="item"><?php echo $item;?></div><img src="<?php echo $grupo[0]->fotografia;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre." - ".$grupo[0]->nombre;?>" data-desc="<br>"></td>
<td><?php echo $grupo[0]->nombre;?></td>
<td>
<?php $costo_unitario=$grupo[0]->costo_unitario;
	$costo_unitario_venta=$grupo[0]->costo_unitario_venta;
	if($parte_pedido->tipo==2){$costo_unitario*=2;}
?>
	<div class="input-group input-80">
		<input class="form-control form-control-sm cu<?php echo $rand;?>" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario;?>" data-value="<?php echo $costo_unitario;?>" data-type='number' data-max="999999.9">
		<span class="input-group-addon form-control-sm">Bs.</span>
	</div>
</td>
<?php for($i=0;$i<count($sucursales);$i++){$sucursal=$sucursales[$i]; 
		$pgc_sc=$grupo[0]->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
		$cantidad=0;
		$j_sucursales=json_decode(json_encode($grupo[0]->sucursales));
		$type_suc="new";
		$idsdp="";
		foreach($j_sucursales as $key => $suc){
			if($suc->idsc==$sucursal->idsc){$cantidad=$suc->cantidad; $type_suc="update";$idsdp=$suc->idsdp; break;}
		}
		$total_cantidad+=$cantidad*1;
	?>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" placeholder="0" min="1" max="9999" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $tbl;?>" <?php if($type_suc!=""){?> data-type="<?php echo $type_suc;?>"<?php }?><?php if($idsdp!=""){?> data-sdp="<?php echo $idsdp;?>" <?php }?> value="<?php echo $cantidad;?>">
			<span class="input-group-addon form-control-sm">u.</span>
		</div>
	</td>
	<?php }?>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm ct" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad;?>">
			<span class="input-group-addon form-control-sm">u.</span>
		</div>
	</td>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm st" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad*$costo_unitario;?>">
			<span class="input-group-addon form-control-sm">Bs.</span>
		</div>
	</td>
	<td>
		<div class="input-group input-100">
			<input class="form-control form-control-sm costo_venta<?php echo $rand;?>" type="number" id="costo_venta<?php echo $pgc;?>" placeholder="0" value="<?php echo $total_cantidad*$costo_unitario;?>" disabled="disabled">
			<span class="input-group-addon form-control-sm">Bs.</span>
		</div>
	</td>
	<td>
		<div class="input-group input-150">
			<textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"></textarea>
			<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
		</div>
	</td>
	<td>
		<span class="g-control-accordion" style="text-align: right;">
			<button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $tbl;?>' data-control="db-producto" data-dp="<?php echo $grupo[0]->iddp;?>" data-container="<?php echo $container; ?>" data-container-2="<?php echo $container_2;?>"><i class="icon-trashcan2"></i></button>
		</span>
		<script>$('[data-toggle="popover"]').popover({html:true});$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$(".drop_elemento<?php echo $rand;?>").drop_elemento();$("img.img-thumbnail-45").visor();</script>
	</td>