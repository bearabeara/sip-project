<?php 
	$url=base_url().'libraries/img/';
	$p_img="sistema/miniatura/default.jpg";
	$colores_asignados=json_decode($colores_asignados);
	if($producto->portada!="" && $producto->portada!=null){
		$v=explode("|", $producto->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
		}
  	}
?>
<div class="table-responsive g-table-responsive" id="datos_producto_add_color" data-tbl="<?php echo $tbl;?>" data-p="<?php echo $producto->idp;?>"<?php if(isset($type)){?> data-type="<?php echo $type;?>"<?php }?><?php if(isset($tipo)){?> data-tipo="<?php echo $tipo;?>"<?php }?>>
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="g-thumbnail"><div class="g-img-modal"></div></th>
				<th width="25%">Código</th>
				<th width="60%">Producto</th>
				<th width="5%"></th>
			</tr>
		</thead>
	<?php if(count($colores)>0){?>
		<tbody>
		<?php 
			for($i=0; $i < count($colores); $i++){ $color=$colores[$i]; 
				$c_gr=""; $gr="";if($color->abr_g!="" || $color->abr_g!=NULL){ $gr=$color->nombre_g." - ";$c_gr=$color->abr_g."-";}
				if($gr!=""){$gr.=" - ";}$gr.=$color->nombre_c;
				if($c_gr!=""){$c_gr.="-";}$c_gr.=$color->abr_c;
				if($color->portada!="" && $color->portada!=null){
					$v=explode("|", $color->portada);
					if(count($v==2)){
						if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
						if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
					}
			  	}else{
					if($producto->portada!="" && $producto->portada!=null){
						$v=explode("|", $producto->portada);
						if(count($v==2)){
							if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
							if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
						}
				  	}
			  	}
		?>
			<tr>
				<td><span class="g-img-modal"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail g-thumbnail-modal"></span></td>
				<td><?php echo $producto->codigo."-".$c_gr; ?></td>
				<td><?php echo $producto->nombre." - ".$gr; ?></td>
				<?php $control=false;
					foreach($colores_asignados as $key => $pgc){ if($pgc==$color->idpgrc){ $control=true; break; } }
				?>
				<td><button type="button" class="btn <?php if($control){ ?>btn-default<?php }else{ ?>btn-inverse-primary add_producto_color<?php }?> btn-mini waves-effect waves-light" <?php if($control){ ?> disabled="disabled"<?php }else{?> data-pgc="<?php echo $color->idpgrc;?>"<?php }?>><i class="fa fa-plus"></i> add</button></td>
			</tr>
		<?php }?>
		</tbody>
	<?php }?>
	</table>
</div>
<script>$(".add_producto_color").add_producto_color();</script>