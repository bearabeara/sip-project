  <?php 
  $help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
  $id=$elemento; 
  $url=base_url().'libraries/img/'; $p_img="sistema/miniatura/default.jpg";
  if($producto->portada!="" && $producto->portada!=null){
    $v=explode("|", $producto->portada);
    if(count($v==2)){
      if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
      if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
    }
  }
  $rand=rand(10,999999);
?>
  <div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
    <span class="card-title accordion-title">
        <span class="g-control-accordion">
          <button class="view_producto<?php echo $rand;?>" data-module="producto_pedido" data-elemento="<?php echo $id;?>" data-type="<?php if(strrpos($type, '_change')===false){ $type.='_change';} echo $type;?>"><i class="fa fa-exchange"></i></button>
          <button class="drop_elemento<?php echo $rand;?>" data-padre="accordion-panel" data-type-padre="atr" data-module="producto_pedido"><i class="icon-bin"></i></button>
        </span>
      <a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
      <div class="item"></div>
        <span class="img-thumbnail-60"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail img-thumbnail-60"></span>
        <span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
        <label class="badge badge-inverse-danger" id="badge<?php echo $id;?>"> Total: 0 unidades</label>
      </a>
    </span>
  </div>
  <div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
    <div class="accordion-content accordion-desc" data-pr="<?php echo $producto->idp;?>">
        <div class="table-responsive g-table-responsive">
          <?php $idtb=$producto->idp.'-'.rand(0,999999);?>
          <table class="table table-bordered table-hover" id="<?php echo $idtb;?>" data-accordion="div#accordion-panel<?php echo $id;?>" data-badge="label#badge<?php echo $id;?>">
            <thead>
              <tr>
                <th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>#Item</th>
                <th width="32%">Producto</th>
                <th width="14%">C/U (Bs.)</th>
            <?php for ($i=0; $i < count($sucursales) ; $i++) { $sucursal=$sucursales[$i]; ?>
                <th><?php echo $sucursal->nombre; ?></th>
            <?php } ?>
                <th>Total</th>
                <th width="17%">Costo calculado(Bs.)</th>
                <th width="17%">Costo de venta(Bs.)</th>
                <th width="18%">Observaciónes</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            <?php $grupos=$this->M_producto_grupo->get_grupo_colores("pg.idp",$producto->idp); ?>
            <?php if(count($grupos)>0){ ?>
              <?php for ($c=0; $c < count($grupos) ; $c++){ $grupo=$grupos[$c]; ?>
              <tr class="row-producto" id="<?php echo $grupo->idpgrc."-".rand(10,99999);?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-type="<?php echo $type;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-p="<?php echo $producto->idp;?>" <?php if(isset($iddp)){?> data-dp="<?php echo $iddp;?>"<?php }?>>
                <?php $c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
                    $pgc=$grupo->idpgrc."-".rand(1,9999);
                    $img="sistema/miniatura/default.jpg";
                    if($grupo->portada!="" && $grupo->portada!=null){
                      $v=explode("|", $grupo->portada);
                      if(count($v==2)){
                        if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                        if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                      }
                    }else{
                      $img=$p_img;
                    }
                ?>
                    <td><div class="item"><?php echo $c+1;?></div><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-45"></td>
                    <td><?php echo $gr.$grupo->nombre_c;?></td>
                    <td>
                      <?php $costo_unitario=$grupo->costo; if(isset($parte_pedido)){if($parte_pedido->tipo==2){$costo_unitario*=2;}}?>
                      <div class="input-group input-80">
                        <input class="form-control form-control-sm cu<?php echo $rand;?>" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario;?>" data-value="<?php echo $costo_unitario;?>">
                        <span class="input-group-addon form-control-sm">Bs.</span>
                      </div>
                    </td>
                    <?php for($i=0; $i < count($sucursales) ; $i++){ $sucursal=$sucursales[$i]; 
                            $pgc_sc=$grupo->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
                    ?>
                    <td>
                      <div class="input-group input-100">
                        <input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" placeholder="0" min="1" max="9999" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $idtb;?>" data-type="new">
                        <span class="input-group-addon form-control-sm">u.</span>
                      </div>
                    </td>
                    <?php } ?>
                    <td>
                      <div class="input-group input-100">
                        <input class="form-control form-control-sm" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                        <span class="input-group-addon form-control-sm">u.</span>
                      </div>
                    </td>
                    <td>
                      <div class="input-group input-100">
                        <input class="form-control form-control-sm" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                        <span class="input-group-addon form-control-sm">Bs.</span>
                      </div>
                    </td>
                    <td>
                        <div class="input-group input-100">
                          <input class="form-control form-control-sm costo_venta<?php echo $rand;?>" id="costo_venta<?php echo $pgc;?>" placeholder="0" disabled="disabled">
                          <span class="input-group-addon form-control-sm">Bs.</span>
                        </div>
                    </td>
                    <td>
                      <div class="input-group input-150">
                        <textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"></textarea>
                        <span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
                      </div>
                    </td>
                    <td>
                      <span class="g-control-accordion">
                        <button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $idtb;?>'><i class="icon-close"></i></button>
                      </span>
                    </td>
              </tr>
              <?php }?>              
            <?php }else{ ?>
                <tr><td colspan="7"><h3>0 colores registrados en el producto</h3></td></tr>
            <?php } ?>
            </tbody>
            <thead>
              <tr class="row-total">
                <th colspan="<?php echo 3+count($sucursales);?>" class="text-right">Totales</th>
                <th class="sub_can_pro">0</th>
                <th class="sub_cos_pro">0</th>
                <th class="sub_tot_cos_pro">0</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
          </table>
        </div>
        <div class="row text-right" style="padding-right:15px; margin-top: 5px;">
          <button type="button" class="btn btn-inverse-info waves-effect btn-mini colores_producto<?php echo $rand;?>" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>"><i class="fa fa-plus"></i><span class="m-l-10">categoría</span>
          </button>
        </div>
    </div>
  </div>
  <script>$('[data-toggle="popover"]').popover({html:true});$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$(".drop_elemento<?php echo $rand;?>").drop_elemento();$("button.colores_producto<?php echo $rand;?>").colores_producto();$(".view_producto<?php echo $rand;?>").view_producto();$("img.img-thumbnail-45").visor();</script>
