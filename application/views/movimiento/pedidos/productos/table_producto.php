<?php 
  //USO EN ACTUALIZACION DE UNA PRODUCTO EN UN SUB PEDIDO
  $help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
  $id=$elemento; 
  $url=base_url().'libraries/img/';
  
?>
<?php 
if($type!="refresh_producto_empleado"){
  $c_total=0;
  foreach($grupos as $key => $g){
    $j_sucursales=json_decode(json_encode($g->sucursales));
    foreach($j_sucursales as $key => $suc){
        $c_total+=($suc->cantidad*1);
    }//end for
  }//end foreach
}
$rand=rand(10,99999);
if($type!="refresh_producto_empleado"){
  $id_form=rand(10,99999);
?>
  <?php $vd_p=array();$c_calculado=0;$c_venta=0;
    foreach($grupos as $key => $grupo){
      $vd_p[]=array('dp'=>$grupo->iddp,'status' => '1');
    }
  ?>
  <thead>
    <tr>
      <th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>#Item</th>
      <th width="32%">Producto</th>
      <th width="14%">C/U (Bs.)</th>
      <?php for ($i=0; $i < count($sucursales) ; $i++) { $sucursal=$sucursales[$i]; ?>
      <th><?php echo $sucursal->nombre; ?></th>
      <?php } ?>
      <th>Total</th>
      <th width="17%">Costo calculado(Bs.)</th>
      <th width="17%">Costo de venta(Bs.)</th>
      <th width="18%">Observaciónes</th>
      <?php if($pedido->estado!="2"){?>
      <td><span id="detalles-producto-<?php echo $producto->idp;?>" style="display: none;"><?php echo json_encode($vd_p); ?></span></td>
    <?php }?>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($grupos)){ ?>
    <?php $c2=1; foreach($grupos as $key => $grupo){ $iddp[]=$grupo->iddp; $id_tr=$grupo->idpgrc."-".rand(10,999999); ?>
    <tr class="row-producto" id="<?php echo $id_tr;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-type="update" data-dp="<?php echo $grupo->iddp;?>" data-pp="<?php echo $parte_pedido->idpp;?>">
      <?php $c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
      $pgc=$grupo->idpgrc."-".rand(1,9999);
      ?>
      <td><div class="item"><?php echo $c2;?></div><div class="img-thumbnail-45"><img src="<?php echo $grupo->fotografia;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre.' - '.$grupo->nombre;?>" data-desc="<br>"></div></td>
      <td><?php echo $gr.$grupo->nombre;?>
        <?php
          $costo_unitario=$grupo->costo_unitario;
          $costo_unitario_venta=$grupo->costo_unitario_venta;
          if($parte_pedido->tipo==2){$costo_unitario*=2;}?>
        <?php 
          if($costo_unitario_venta!=$costo_unitario){
            echo "<br><small class='text-danger'>Costo actual ".$costo_unitario."Bs.</small>";
            echo "<br><a href='javascript:' class='text-primary small refresh_producto_detalle_pedido".$rand."' data-container-tr='".$id_tr."' data-dp='".$grupo->iddp."' data-p='".$grupo->idp."' data-item='".$c2."' data-tbl='".$idtb."' data-container='span#detalles' data-container-2='span#detalles-producto-".$producto->idp."'><strong title='Calcular el costo de venta con el costo unitario actual'><i class='fa fa-refresh'></i> Actua...</strong></a>";
          }
          $c2++;
      ?>
      </td>
      <td>
        <div class="input-group input-80">
            <input class="form-control form-control-sm cu" type="text" id="cu<?php echo $pgc;?>" placeholder="0,00" disabled="disabled" value="<?php echo $costo_unitario_venta;?>" data-value="<?php echo $costo_unitario_venta;?>">
            <span class="input-group-addon form-control-sm">Bs.</span>
        </div>
      </td>
      <?php 
      $total_cantidad=0;
      for($i=0; $i < count($sucursales); $i++){ $sucursal=$sucursales[$i]; 
        $pgc_sc=$grupo->idpgrc."-".$sucursal->idsc."-".rand(1,9999);
        $cantidad=0;
        $j_sucursales=json_decode(json_encode($grupo->sucursales));
        $idsdp=NULL;
        $type_suc="new";
        foreach($j_sucursales as $key => $suc){
          if($suc->idsc==$sucursal->idsc){$cantidad=$suc->cantidad;$type_suc="update";$idsdp=$suc->idsdp; break;}
        }
        $total_cantidad+=$cantidad*1;
        ?>
        <td>
          <div class="input-group input-100">
            <input class="form-control form-control-sm cantidad<?php echo $rand;?>" type="number" id="cantidad<?php echo $pgc_sc;?>" placeholder="0" min="0" max="9999" data-sc="<?php echo $sucursal->idsc;?>" data-tbl="table#<?php echo $idtb;?>" <?php if($type_suc!=""){?> data-type="<?php echo $type_suc;?>"<?php }?><?php if($idsdp!=NULL){?> data-sdp="<?php echo $idsdp;?>" <?php }?> value="<?php echo $cantidad;?>">
            <span class="input-group-addon form-control-sm">u.</span>
          </div>
        </td>
        <?php } ?>
        <td>
          <div class="input-group input-100">
            <input class="form-control form-control-sm ct" type="text" id="ct<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad;?>">
            <span class="input-group-addon form-control-sm">u.</span>
          </div>
        </td>
        <td>
          <div class="input-group input-100">
            <input class="form-control form-control-sm st" type="text" id="st<?php echo $pgc;?>" placeholder="0" disabled="disabled" value="<?php echo $total_cantidad*$costo_unitario_venta;?>">
            <span class="input-group-addon form-control-sm">Bs.</span>
          </div>
          <?php $c_calculado+=($total_cantidad*$costo_unitario_venta);?>
        </td>
        <td>
          <div class="input-group input-100">
            <input class="form-control form-control-sm costo_venta<?php echo $rand;?>" type="number" id="costo_venta<?php echo $pgc;?>" placeholder="0" value="<?php echo $grupo->costo_venta;?>" disabled="disabled">
            <span class="input-group-addon form-control-sm">Bs.</span>
          </div>
          <?php $c_venta+=($grupo->costo_venta);?>
        </td>
        <td>
          <div class="input-group input-150">
            <textarea class="form-control form-control-xs" id="obs<?php echo $pgc;?>" placeholder="Observaciónes" maxlength="300" rows="3"><?php echo $grupo->observacion; ?></textarea>
            <span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class="fa fa-info-circle"></i></span>
          </div>
        </td>
<?php if($pedido->estado!="2"){?>
        <td>
          <span class="g-control-accordion" style="/*width: 54px;*/">
            <button class="drop_elemento<?php echo $rand;?>" data-padre="row-producto" data-type-padre="atr" data-module="producto_color_pedido" data-tbl='table#<?php echo $idtb;?>' data-control="db-producto" data-dp="<?php echo $grupo->iddp;?>" data-container="span#detalles" data-container-2="span#detalles-producto-<?php echo $producto->idp;?>"><i class="icon-trashcan2"></i></button>
          </span>
          <script>$(".cantidad<?php echo $rand;?>").costo_venta_tbl();$("img.img-thumbnail-45").visor();<?php if($costo_unitario_venta!=$costo_unitario){?>$("a.refresh_producto_detalle_pedido<?php echo $rand;?>").refresh_producto_detalle_pedido();<?php }?></script>
        </td>
<?php }?>
        <?php } ?>
      </tr>           
      <?php }else{ ?>
      <tr><td colspan="7"><h3>0 colores registrados en el producto</h3></td></tr>
      <?php } ?>
    </tbody>
    <?php $iddp=json_encode($iddp); ?>
    <thead>
      <tr class="row-total">
        <th colspan="<?php echo 3+count($sucursales);?>" class="text-right">Totales</th>
          <th class="sub_can_pro"><?php echo $c_total;?></th>
          <th class="sub_cos_pro"><?php echo $c_calculado;?></th>
          <th class="sub_tot_cos_pro"><?php echo $c_venta;?></th>
          <th></th>
      <?php if($pedido->estado!="2"){?>
          <th><script>$(".drop_elemento<?php echo $rand;?>").drop_elemento();</script></th>
      <?php }?>
        </tr>
      </thead>
      <?php } ?>