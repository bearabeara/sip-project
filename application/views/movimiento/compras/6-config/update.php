<?php
	$url=base_url().'libraries/img/';
	$img="sistema/miniatura/default.jpg";
	date_default_timezone_set("America/La_Paz");
	$help1='title="N° de documento" data-content="Ingrese el número de comprobante de compra si tuviera, solo se acepta numeros <b>de 0 a 15 digitos</b>"';
	$help2='title="Fecha y hora de compra" data-content="Ingrese la fecha y hora de la compra."';
	$help3='title="Material" data-content="Seleccione el materiales comprado."';
	$help4='title="Cantidad" data-content="Ingrese la cantida de material comprado, la cantidad debe ser <b>mayor a cero</b> y <b>menor a 999999999.99</b>"';
	$help5='title="Costo" data-content="Ingrese el costo total de compra, el costo debe ser <b>mayor o igual a cero</b> y <b>menor a 99999999.9</b>"';
	$help6='title="Proveedor" data-content="Seleccione el proveedor de la compra."';
	$help7='title="Descripción del material" data-content="Ingrese un descripcion alfanumerica de 0 a 700 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$proveedor=$this->lib->search_elemento($proveedores,'idpr',$compra->idpr);
	$usuario=$this->lib->search_elemento($usuarios,'idus',$compra->idus);
	$material=$this->lib->search_elemento($materiales,'idmi',$compra->idmi);
	if($material!=null){
		if($material->fotografia!="" && $material->fotografia!=null){
			$img="materiales/miniatura/".$material->fotografia;
		}
	}
?>
<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group hidden-sm">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">N° documento </label>
				<div class="col-sm-4 col-xs-12">
					<form class="update_compra" data-c="<?php echo $compra->idc;?>" >
						<div class="input-group">
							<input type="number" class='form-control form-control-xs' id="f1_num_doc" placeholder="0" min="0" max="15" value="<?php echo $compra->num_doc; ?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Fecha:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input class='form-control form-control-xs' type="datetime-local" id="f1_fec" value="<?php echo str_replace(' ', 'T', $compra->fecha);?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span>Material:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<div class="form-control form-control-sm textarea" id="f1_mat" data-pro="f1_pro" data-uni="f1_uni" style="min-height: 50px; cursor: pointer; height: auto; color: rgba(85, 89, 92, .71);">
							<div class="card-group" style="text-align: center !important;display: table;width: 100%;" data-mi="<?php echo $compra->idmi;?>">
								<div class="card img-thumbnail-35" style="display: table-cell;">
									<img class="card-img-top" src="<?php echo $url.$img;?>" alt="image" data-title="<?php echo $compra->material;?>" data-desc="<br>" style="vertical-align: top;">
								</div>
								<div class="card card-padding-0" style="vertical-align: middle !important;display: table-cell;">
									<div class="a-card-block">
										<p class="a-card-text" style="line-height: .8 !important;"><small style="color: #069bcc !important;"><?php echo $compra->material;?></small></p>
									</div>
								</div>
							</div>
						</div>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class="fa fa-info-circle"></i></span>
					</div>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Costo:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="update_compra" data-c="<?php echo $compra->idc;?>" >
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="f1_cos" placeholder='Costo' step="any" min="0" max="99999999.9" value="<?php echo $compra->costo_total;?>">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Cantidad:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_compra" data-c="<?php echo $compra->idc;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="f1_can" placeholder='Cantidad' step="any" min="0" max="999999999.99" value="<?php echo $compra->cantidad;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Unidad:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class='form-control form-control-xs' id="f1_uni">
							<option value="">Seleccionar...</option>
					<?php for($i=0; $i < count($unidades); $i++){ $unidad=$unidades[$i]; ?>
							<option value="<?php echo $unidad->idu;?>"<?php if($this->lib->all_minuscula($unidad->nombre)==$this->lib->all_minuscula($compra->unidad) && $this->lib->all_minuscula($unidad->abr)==$this->lib->all_minuscula($compra->abr)){?> SELECTED<?php }?>><?php echo $unidad->nombre." [".$unidad->abr.".]";?></option>
					<?php } ?>
						</select>
						<a class="input-group-addon form-control-sm new_unidad" data-container="f1_uni" title="Nueva unidad de medida" data-type="add"><i class="fa fa-plus"></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Proveedor:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class='form-control form-control-xs' id="f1_pro">
							<option value="">Seleccionar...</option>
						<?php for($i=0;$i<count($proveedores);$i++){$proveedor=$proveedores[$i]; ?>
							<option value="<?php echo $proveedor->idpr; ?>" <?php if($proveedor->idpr==$compra->idpr){?>SELECTED<?php }?>><?php echo $proveedor->razon;?></option>
						<?php } ?>
						</select>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Descripción:</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<textarea class="form-control form-control-xs" id="f1_des" placeholder='Descripción del material' maxlength="300" rows="3"><?php echo $compra->observacion;?></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	Onfocus("f1_num_doc");$('[data-toggle="popover"]').popover({html:true});$("form.save_compra").submit(function(e){$(this).save_compra();e.preventDefault();});
	$("div#f1_mat").view_materiales();$(".new_unidad").new_unidad();$(".new_proveedor").new_proveedor();
</script>