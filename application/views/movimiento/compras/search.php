<?php date_default_timezone_set("America/La_Paz");?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm' style="width:15%"></td>
		<td class='hidden-sm' style="width:23%">
			<form class="view_compras" data-type="search">
				<input type="text" class="form-control form-control-sm search_movimiento" id="s1_pro" placeholder='Producto'>
			</form>
		</td>
		<td class='hidden-sm celda-sm-10'>
			<input type="date" class="form-control form-control-sm search_movimiento" id="s1_fe1" style="max-width: 100% !important;">
		</td>
		<td class='hidden-sm celda-sm-10'>
			<input type="date" class="form-control form-control-sm search_movimiento" id="s1_fe2" style="max-width: 100% !important;">
		</td>
		<td style="width:18%;"">
			<input type="month" class="form-control form-control-sm search_movimiento" id="s1_mes" placeholder='2000-12' style="width: 100%;" value="<?php echo date('Y-m');?>">
		</td>
		<td class="hidden-sm" style="width:24%;"></td>
		<td class="text-right" style="width: 1%;">
			<?php 
				$search=json_encode(array('function'=>'view_compras','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_compras','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>$(".search_movimiento").search_movimiento();$(".view_compras").view_compras();
	//$("input#s1_pro").keyup(function(){ $(this).reset_input($(this).attr("id"));});$("input.form-control").change(function(){ $(this).reset_input($(this).attr("id"));});Onfocus("s1_pro");
</script>