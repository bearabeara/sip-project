<?php
	$tot=0;$ice=0;$exc=0;$net=0;$desc=0;
	$j_compras=json_encode($compras);
	$total=0;
?>
	<table class="table table-bordered table-hover" id="tbl-container">
		<thead>
			<tr>
				<th width="3%">#</th>
				<th width="9%" class="hidden-sm">Nº Doc.</th>
				<th width="19%">Producto</th>
				<th width="14%" class="hidden-sm">Fecha de compra</th>
				<th width="13%" class="hidden-sm">Proveedor</th>
				<th width="8%">Cantidad</th>
				<th width="8%">Costo<br>(Bs.)</th>
				<th width="26%" class="hidden-sm">Observaciones</th>
				<th width="1%"></th>
			</tr>
		</thead>
		<tbody>
<?php if(count($compras)>0){
		for ($i=0; $i < count($compras) ; $i++) { $compra=$compras[$i];
			$razon="-";
			$proveedor=$this->lib->search_elemento($proveedores,'idpr',$compra->idpr);
			if($proveedor!=null){
				$razon=$proveedor->razon;
			}
?>
		<tr data-c="<?php echo $compra->idc;?>">
			<td><?php echo $i+1;?></td>
			<td class="hidden-sm"><?php echo $compra->num_doc;?></td>
			<td><?php echo $compra->material;?></td>
			<td class="hidden-sm"><?php echo $this->lib->format_date($compra->fecha,"d/m/Y"); ?></td>
			<td class="hidden-sm"><?php echo $razon;?></td>
			<td class="text-right"><?php echo $compra->cantidad." ".$compra->abr.".";?></td>
			<td class="text-right"><?php echo number_format($compra->costo_total,2,'.',',');$total+=$compra->costo_total;?></td>
			<td class="hidden-sm"><?php echo $compra->observacion;?></td>
			<td class="text-right">
			<?php 
				$det=json_encode(array('function'=>'reporte_compra','atribs'=> json_encode(array('c' => $compra->idc)),'title'=>'Detalle'));
				$conf=""; if($privilegio->mo3u=="1"){ $conf=json_encode(array('function'=>'config_compra','atribs'=> json_encode(array('c' => $compra->idc)),'title'=>'Configurar'));}
				$del=""; if($privilegio->mo3d=="1"){ $del=json_encode(array('function'=>'confirm_compra','atribs'=> json_encode(array('c' => $compra->idc)),'title'=>'Eliminar'));}
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
			</td>
		</tr>	
<?php	}//end for
	}else{// en if
	}
?>
		</tbody>
		<thead class="visible-sm-thead">
			<tr>
				<td colspan="3" class="text-right"><strong>TOTAL Bs.:</strong></td>
				<td class="text-right"><?php echo number_format($total,2,'.',',');?></td>
				<td></td>
			</tr>
		</thead>
		<thead class="hidden-sm">
			<tr>
				<td colspan="6" class="text-right"><strong>TOTAL Bs.:</strong></td>
				<td class="text-right"><?php echo number_format($total,2,'.',',');?></td>
				<td colspan="2"></td>
			</tr>
		</thead>
	</table>
	<?php 
		if($privilegio->mo3r=="1" && ($privilegio->mo3p=="1" || $privilegio->mo3c=="1")){
			if($privilegio->mo3p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
			if($privilegio->mo3p=="1"){$report=json_encode(array('function'=>'print_compras','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
			if($privilegio->mo3c=="1"){$new=json_encode(array('function'=>'new_compra','title'=>'Nueva compra material no registrado','atribs'=> json_encode(array('type' => "create"))));}
			if($privilegio->mo3c=="1"){$cmat=json_encode(array('function'=>'new_compra','title'=>'Nueva compra material registrado','atribs'=> json_encode(array('type' => "add"))));}
			$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_cmat"=>$cmat,"fab_help"=>$help]);
		}
	?>
<script>$('[data-toggle="tooltip"]').tooltip({});$(".reporte_compra").reporte_compra();$(".config_compra").config_compra();$(".confirm_compra").confirm_compra();</script>