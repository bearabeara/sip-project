<?php 
	$url=base_url().'libraries/img/';
	$img="sistema/miniatura/default.jpg";
	$proveedor=$this->lib->search_elemento($proveedores,'idpr',$compra->idpr);
	$usuario=$this->lib->search_elemento($usuarios,'idus',$compra->idus);
	$material=$this->lib->search_elemento($materiales,'idmi',$compra->idmi);
	if($material!=null){
		if($material->fotografia!="" && $material->fotografia!=null){
			$img="materiales/miniatura/".$material->fotografia;
		}
	}
?>
<div class="row">
	<div class="col-sm-3 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-9 cols-xs-12">
	<table border="0" class="tabla tabla-border-true">
		<tr class="fila">
			<th class="celda th" width="30%">N° de Documento:</th>
			<td class="celda td" width="70%"><?php echo $compra->num_doc;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Fecha de compra:</th>
			<td class="celda td" colspan="3"><?php echo $this->lib->format_date($compra->fecha,"dl ml Y");?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Material:</th>
			<td class="celda td" colspan="3"><?php echo $compra->material;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Cantidad:</th>
			<td class="celda td" colspan="3"><?php echo $compra->cantidad." ".$compra->unidad;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Costo de compra (Bs.):</th>
			<td class="celda td" colspan="3"><?php echo number_format($compra->costo_total,2,'.',',')." Bs.";?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Proveedor:</th>
			<td class="celda td" colspan="3"><?php if($proveedor!=null){ echo $proveedor->razon;}else{ echo "-";}?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Observaciónes:</th>
			<td class="celda td" colspan="3"><?php echo $compra->observacion;?></td>
		</tr>
		<tr class="fila">
			<th class="celda th">Compra registrada por:</th>
			<td class="celda td" colspan="3"><?php if($usuario!=null){ echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno." ";}?></td>
		</tr>
	</table>
	</div>
</div>