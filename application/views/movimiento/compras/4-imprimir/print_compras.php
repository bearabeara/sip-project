<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_compras', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'movimiento/export_compras?pro='.$pro.'&fe1='.$fe1.'&fe2='.$fe2."&mes=".$mes."&file=xls");
		$word = array('controller' => 'movimiento/export_compras?pro='.$pro.'&fe1='.$fe1.'&fe2='.$fe2."&mes=".$mes."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 12%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Fecha de compra</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>N° Documento</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 20%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Producto</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 15%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Proveedor</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Cantidad</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Costo (Bs.)</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 20%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary"></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php
										$sub_titulo="Todos los registros";
										$mes=explode("-", $mes);
										if(count($mes)==2){
											$mes=$mes[1];
											$fecha1=date("Y")."-".$this->lib->dosDigitos($mes*1)."-01";
											$fecha2=date("Y")."-".$this->lib->dosDigitos($mes*1)."-".$this->lib->dosDigitos($this->lib->ultimo_dia($mes*1,date("Y")));
											$sub_titulo="Del ".$this->lib->format_date($fecha1,"d/m/Y")." al ".$this->lib->format_date($fecha2,"d/m/Y");
										}else{
											if($fe1!="" || $fe2!=""){
												if($this->val->fecha($fe1) && $this->val->fecha($fe2)){
													$sub_titulo="Del ".$this->lib->format_date($fe1,"d/m/Y")." al ".$this->lib->format_date($fe2,"d/m/Y");
												}else{
													if($this->val->fecha($fe1)){
														$sub_titulo="En fecha ".$this->lib->format_date($fe1,"d/m/Y");
													}else{
														if($this->val->fecha($fe2)){
															$sub_titulo="En fecha ".$this->lib->format_date($fe2,"d/m/Y");
														}
													}
												}
											}
										}
									?>
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE COMPRAS','sub_titulo'=>$sub_titulo]);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="3%">#</th>
								<th class="celda th padding-4" data-column="2" width="12%">Fecha de compra</th>
								<th class="celda th padding-4" data-column="3" width="10%">N° documento</th>
								<th class="celda th padding-4" data-column="4" width="20%">Producto</th>
								<th class="celda th padding-4" data-column="5" width="15%">Proveedor</th>
								<th class="celda th padding-4" data-column="6" width="10%">Cantidad</th>
								<th class="celda th padding-4" data-column="7" width="10%">Costo (Bs.)</th>
								<th class="celda th padding-4" data-column="8" width="20%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=1;$total=0;
								foreach($visibles as $key => $visible){ 
									$compra=$this->lib->search_elemento($compras,"idc",$visible);
									if($compra!=null){
										$razon="-";
										$proveedor=$this->lib->search_elemento($proveedores,'idpr',$compra->idpr);
										if($proveedor!=null){
											$razon=$proveedor->razon;
										}
							?>
								<tr class="fila">
									<td class="celda td padding-4" data-column="1"><?php echo $cont++;?></td>
									<td class="celda td padding-4" data-column="2"><?php echo $this->lib->format_date($compra->fecha,'d/m/Y');?></td>
									<td class="celda td padding-4" data-column="3"><?php echo $compra->num_doc;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $compra->material;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $razon;?></td>
									<td class="celda td padding-4 text-right" data-column="6"><?php echo $compra->cantidad." ".$compra->abr.".";?></td>
									<td class="celda td padding-4 text-right" data-column="7"><?php echo number_format($compra->costo_total,2,'.',',');$total+=$compra->costo_total;?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $compra->observacion;?></td>
								</tr>
							<?php }}//end for?>
						</tbody>
						<thead>
							<tr class="fila">
								<td class="celda td padding-4" data-column="1"></td>
								<td class="celda td padding-4" data-column="2"></td>
								<td class="celda td padding-4" data-column="3"></td>
								<td class="celda td padding-4" data-column="4"></td>
								<td class="celda td padding-4" data-column="5"></td>
								<td class="celda td padding-4" data-column="6"></td>
								<td class="celda td padding-4 text-right" data-column="7"><strong><?php echo number_format($total,2,'.',',');?></strong></td>
								<td class="celda td padding-4" data-column="8"></td>
							</tr>
						</thead>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>