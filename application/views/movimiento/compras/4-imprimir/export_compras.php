	<?php
	$total=0;
	$sub_titulo="Todos los registros";
	$mes=explode("-", $mes);
	if(count($mes)==2){
		$mes=$mes[1];
		$fecha1=date("Y")."-".$this->lib->dosDigitos($mes*1)."-01";
		$fecha2=date("Y")."-".$this->lib->dosDigitos($mes*1)."-".$this->lib->dosDigitos($this->lib->ultimo_dia($mes*1,date("Y")));
		$sub_titulo="Del ".$this->lib->format_date($fecha1,"d/m/Y")." al ".$this->lib->format_date($fecha2,"d/m/Y");
	}else{
		if($fe1!="" || $fe2!=""){
			if($this->val->fecha($fe1) && $this->val->fecha($fe2)){
				$sub_titulo="Del ".$this->lib->format_date($fe1,"d/m/Y")." al ".$this->lib->format_date($fe2,"d/m/Y");
			}else{
				if($this->val->fecha($fe1)){
					$sub_titulo="En fecha ".$this->lib->format_date($fe1,"d/m/Y");
				}else{
					if($this->val->fecha($fe2)){
						$sub_titulo="En fecha ".$this->lib->format_date($fe2,"d/m/Y");
					}
				}
			}
		}
	}
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Compras-$sub_titulo.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Compras-$sub_titulo.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center>
<strong>REGISTRO DE COMPRAS</strong><br/>
<span><?php echo $sub_titulo;?></span>
</center>
	<table border="1" cellpadding="3" cellspacing="0">
		<thead>
			<tr>
				<th>#</th>
				<th>Nº Doc.</th>
				<th>Producto</th>
				<th>Fecha de compra</th>
				<th>Proveedor</th>
				<th>Cantidad</th>
				<th>Total<br>(Bs.)</th>
				<th>Observaciones</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($compras)>0){
		for ($i=0; $i < count($compras) ; $i++) { $compra=$compras[$i];
			$razon="-";
			$proveedor=$this->lib->search_elemento($proveedores,'idpr',$compra->idpr);
			if($proveedor!=null){
				$razon=$proveedor->razon;
			}
?>
		<tr data-c="<?php echo $compra->idc;?>">
			<td><?php echo $i+1;?></td>
			<td><?php echo $compra->num_doc;?></td>
			<td><?php echo $compra->material;?></td>
			<td><?php echo $this->lib->format_date($compra->fecha,"d/m/Y");?></td>
			<td><?php echo $razon;?></td>
			<td style="text-align: right;"><?php echo $compra->cantidad." ".$compra->abr.".";?></td>
			<td style="text-align: right;"><?php echo number_format($compra->costo_total,2,',','.');$total+=$compra->costo_total;?></td>
			<td><?php echo $compra->observacion;?></td>
		</tr>	
<?php	}//end for
	}else{// en if
?>
		<tr>
			<td colspan="8" style="text-align: center;">0 registros encontrados.</td>
		</tr>
<?php
	}
?>
		</tbody>
		<thead>
			<tr>
				<td colspan="6" style="text-align:right"><strong>TOTAL Bs.:</strong></td>
				<td style="text-align:right"><?php echo number_format($total,2,',','.');?></td>
				<td></td>
			</tr>
		</thead>
	</table>