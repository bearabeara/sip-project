<?php
	date_default_timezone_set("America/La_Paz");
	$help1='title="N° de documento" data-content="Ingrese el número de comprobante de compra si tuviera, solo se acepta numeros <b>de 0 a 15 digitos</b>"';
	$help2='title="Fecha y hora de compra" data-content="Ingrese la fecha y hora de la compra."';
	$help3='title="Material" data-content="Seleccione el materiales comprado."';
	$help4='title="Cantidad" data-content="Ingrese la cantida de material comprado, la cantidad debe ser <b>mayor a cero</b> y <b>menor a 999999999.99</b>"';
	$help5='title="Costo" data-content="Ingrese el costo total de compra, el costo debe ser <b>mayor o igual a cero</b> y <b>menor a 99999999.9</b>"';
	$help6='title="Proveedor" data-content="Seleccione el proveedor de la compra."';
	$help7='title="Descripción del material" data-content="Ingrese un descripcion alfanumerica de 0 a 700 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group hidden-sm">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">N° documento </label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_compra" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input type="number" class='form-control form-control-xs' id="f1_num_doc" value="0" placeholder="0" min="0" max="15">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Fecha:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_compra" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="datetime-local" id="f1_fec" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span>Material:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<div class="form-control form-control-sm textarea" id="f1_mat" data-pro="f1_pro" data-uni="f1_uni" style="min-height: 50px; cursor: pointer; height: auto; color: rgba(85, 89, 92, .71);">Seleccionar...</div>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class="fa fa-info-circle"></i></span>
					</div>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Costo:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_compra" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="f1_cos" placeholder='Costo' step="any" min="0" max="99999999.9">
							<span class="input-group-addon form-control-sm">Bs.</span>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Cantidad:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="save_compra" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="f1_can" placeholder='Cantidad' step="any" min="0" max="999999999.99">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label"><span class='text-danger'>(*)</span> Unidad:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class='form-control form-control-xs' id="f1_uni">
							<option value="">Seleccionar...</option>						
						</select>
						<a class="input-group-addon form-control-sm new_unidad" data-container="f1_uni" title="Nueva unidad de medida" data-type="add"><i class="fa fa-plus"></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Proveedor:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class='form-control form-control-xs' id="f1_pro">
							<option value="">Seleccionar...</option>
						</select>
						<a class="input-group-addon form-control-sm new_proveedor" title="Nuevo proveedor" data-container="f1_pro" data-type="add"><i class="fa fa-plus"></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label">Descripción:</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<textarea class="form-control form-control-xs" id="f1_des" placeholder='Descripción del material' maxlength="300" rows="3"></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>Onfocus("f1_num_doc");$('[data-toggle="popover"]').popover({html:true});$("form.save_compra").submit(function(e){$(this).save_compra();e.preventDefault();});$("div#f1_mat").view_materiales();$(".new_unidad").new_unidad();$(".new_proveedor").new_proveedor();</script>