<?php
	$help1='title="Buscar Almacen" data-content="Seleccione el almacen donde desee buscar el material."';
	$help2='title="Material" data-content="Ingrese el nombre o parte del nombre del material que desea buscar."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$url=base_url().'libraries/img/';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; padding-top: 0px !important;">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-12 col-xs-12">
					<div class="input-group">
						<select class="form-control form-control-xs search2" id="s2_alm" style="margin:0px auto;" data-type="table-material-modal" data-table-search="table-search-2" data-column="span.text" data-material="input#s2_mat">
							<option value="">Seleccione...</option>
							<?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i];?>
							<option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre; ?></option>
							<?php }?>
							<option value="mi">Materiales indirectos</option>
							<option value="om">Otros materiales</option>
						</select>
						<a class="input-group-addon form-control-xs search2" title="buscar"><i class='icon-search2'></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<div class="col-sm-12 col-xs-12">
					<div class="input-group">
						<input class='form-control form-control-xs search2' type="text" id="s2_mat" placeholder='Material' data-type="table-material-modal" data-table-search="table-search-2" data-column="span.text" data-almacen="select#s2_alm" data-container="<?php echo $container;?>" <?php if(isset($container_proveedor)){?> data-container-proveedor="<?php echo $container_proveedor;?>"<?php }?><?php if(isset($container_unidad)){?> data-container-unidad="<?php echo $container_unidad;?>"<?php }?>>
						<a class="input-group-addon form-control-xs search2" title="buscar"><i class='icon-search2'></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<table class="table table-bordered table-hover" id="table-search-2">
			<thead>
				<tr>
					<th class="img-thumbnail-45"><div class="img-thumbnail-45">#Item</div></th>
					<th width="95%">Material</th>
					<th width="5%"></th>
				</tr>
			</thead>
			<tbody>
			<?php for($i=0;$i<count($materiales_item);$i++){ $material_item=$materiales_item[$i];
					$img="sistema/miniatura/default.jpg";
			    	if($material_item->fotografia!="" && $material_item->fotografia!=NULL){
						$img="materiales/miniatura/".$material_item->fotografia;
					}
					//buscando tipo de contenedor
					$tipo="";
					$text_tipo="";
					$s_material=$this->lib->search_elemento($materiales_indirectos,'idmi',$material_item->idmi);
						if($s_material!=NULL){
							$tipo="mi";$text_tipo="Materiales indirectos";
						}else{//end if
							$s_material=$this->lib->search_elemento($otros_materiales,'idmi',$material_item->idmi);
							if($s_material!=NULL){
								$tipo="om";$text_tipo="Otros materiales";
							}else{//end if
								$s_material=$this->lib->search_elemento($materiales,'idmi',$material_item->idmi);
								if($s_material!=NULL){
									for($j=0; $j < count($almacenes_materiales); $j++){
										if($almacenes_materiales[$j]->idm==$s_material->idm){
											$almacen=$this->lib->search_elemento($almacenes,'ida',$almacenes_materiales[$j]->ida);
											if($almacen!=NULL){
												$tipo=$almacen->ida;$text_tipo=$almacen->nombre;
											}
										}// end if
									}// end for
								}
							}
						}
			?>
				<tr class="search" data-text="<?php echo $material_item->nombre;?>" data-tipo="<?php echo $tipo?>">
					<td>
						<div class="item"><?php echo $i+1;?></div>
						<img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-title="<?php echo $material_item->nombre;?>" data-desc="<?php echo $material_item->descripcion;?>">
					</td>
					<td>
						<span class="text" data-a="<?php echo $tipo;?>"><?php echo $material_item->nombre; ?></span>
						<?php if($tipo!="" && $text_tipo!=""){?>
							<div class="label-main">
								<label class="label label-inverse-<?php if($tipo=='mi'){ echo 'warning';}else{if($tipo=='om'){ echo 'info';}else{ echo 'success';}}?> label-xs"><?php echo $text_tipo;?></label>
							</div>
						<?php }else{?>
								<br><a href="javascript:" class="drop_aux" data-mi="<?php echo $material_item->idmi;?>">Eliminar</a>
						<?php }?>
					</div>
					</td>
					<td>
					<?php if($tipo!="" && $text_tipo!=""){?>
						<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light add_material_compra" data-mi="<?php echo $material_item->idmi;?>"><i class="fa fa-plus"></i> add</button>
					<?php }?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$(".search2").search();$("button.add_material_compra").add_material_compra();$("a.drop_aux").drop_aux();Onfocus("s2_mat");$("img.img-thumbnail-45").visor();</script>