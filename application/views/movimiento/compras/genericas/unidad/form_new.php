<?php
date_default_timezone_set("America/La_Paz");
$help1='title="Nombre de unidad de medida" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 40 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
$help2='title="Abreviatura" data-content="Ingrese una abreviatura alfanumerica <strong>de 1 a 8 caracteres con espacios</strong>, ademas la abreviatura solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>. Ej. Centímetros = cm"';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
$rand=rand(10,999999);
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; padding-top: 0px !important;">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-7 col-xs-12">
					<form class="save_unidad<?php echo $rand;?>" data-container="<?php echo $container?>" data-type="<?php echo $type;?>">
						<label class="col-form-label">Nombre de unidad:</label>
						<div class="input-group">
							<input type="text" class='form-control form-control-xs' id="f2_uni" placeholder="Centímetros" minlength="2" maxlength="40">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-sm-5 col-xs-12">
					<form class="save_unidad<?php echo $rand;?>" data-container="<?php echo $container?>" data-type="<?php echo $type;?>">
						<label class="col-form-label">Abreviatura:</label>
						<div class="input-group">
							<input type="text" class='form-control form-control-xs' id="f2_abr" placeholder="cm" minlength="1" maxlength="8">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("form.save_unidad<?php echo $rand?>").submit(function(e){$(this).save_unidad();e.preventDefault();});Onfocus("f2_uni");</script>