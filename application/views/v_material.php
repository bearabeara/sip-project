<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Materiales','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
			if($privilegio[0]->al11r==1){ $v_menu.="Materiales/material/fa fa-cubes|"; }
			if($privilegio[0]->al12r==1){ $v_menu.="Ingresos/ingreso/fa fa-long-arrow-down|";}
			if($privilegio[0]->al13r==1){ $v_menu.="Salidas/salida/fa fa-long-arrow-up|";}
			if($privilegio[0]->al15r==1){ $v_menu.="Configuración/config/fa fa-cogs";}
		?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'almacen','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		<?php $this->load->view('estructura/js',['js'=>$dir.'material'.$min.'.js']);?>
		<script>$(document).ready(function(){inicio(<?php echo $almacen;?>)})</script>
	</body>
	<?php 
		$title="";$activo="";$search="";$view="";
		switch ($pestania){
			case '1': if($privilegio[0]->al==1 && $privilegio[0]->al11r==1){ $search="../../material/search_material"; $view="../../material/view_material/".$almacen; $activo='material'; $title="Materiales";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '2': if($privilegio[0]->al==1 && $privilegio[0]->al12r==1){ $search="../../material/search_ingreso"; $view="../../material/view_ingreso/".$almacen; $activo='ingreso'; $title="Ingreso de materiales";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '3': if($privilegio[0]->al==1 && $privilegio[0]->al13r==1){ $search="../../material/search_salida"; $view="../../material/view_salida/".$almacen; $activo='salida'; $title="Salida de materiales";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '5': if($privilegio[0]->al==1 && $privilegio[0]->al15r==1){ $search=""; $view="../../material/view_config"; $activo='config'; $title="Configuración de materiales";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			default: $title="404"; $view=base_url()."login/error";
		}
	?>
	<script>$(this).get_2n('<?php echo $search; ?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','<?php echo base_url().'almacen/material/'.$almacen;?>?p=<?php echo $pestania;?>');</script>
</html>