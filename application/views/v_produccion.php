<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Produccion','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
			if($privilegio[0]->pr1r==1){ $v_menu.="Productos/producto/icon-suitcase2|"; }
			if($privilegio[0]->pr2r==1){ $v_menu.="Producción/produccion/icon-factory|"; }
			if($privilegio[0]->pr2r==1){ $v_menu.="Seguimiento/seguimiento/fa fa-sliders|"; }
			if($privilegio[0]->pr5r==1){ $v_menu.="Configuración/config/fa fa-cogs";}
		?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'produccion','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		<?php $this->load->view('estructura/js',['js'=>$dir.'produccion'.$min.'.js']);?>
	</body>
	<?php 
		$title="";$activo="";$search="";$view="";
		switch ($pestania) {
			case '1': if($privilegio[0]->pr==1 && $privilegio[0]->pr1r==1){ $title="Productos"; $activo='producto'; $search="produccion/search_producto"; $view="produccion/view_producto";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '2': if($privilegio[0]->pr==1 && $privilegio[0]->pr2r==1){ $title="Producción"; $activo='produccion'; $search="produccion/search_produccion"; $view="produccion/view_produccion";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '3': if($privilegio[0]->pr==1 && $privilegio[0]->pr2r==1){ $title="Seguimiento de Producción"; $activo='seguimiento'; $search="produccion/search_seguimiento"; $view="produccion/view_seguimiento";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			case '5': if($privilegio[0]->pr==1 && $privilegio[0]->pr5r==1){ $title="Configuración"; $activo='config'; $search=""; $view="produccion/view_config";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
			default: $title="404"; $view=base_url()."login/error";
		}
	?>
	<script>$(this).get_2n('<?php echo $search; ?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','produccion?p=<?php echo $pestania;?>');</script>
</html>