<?php 
	$url=base_url().'libraries/img/';
	$tipo_contrato = array('Tiempo completo', 'Medio tiempo');
	$estado = array('Inactivo', 'Activo');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_empleados', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'capital_humano/export_empleados?cod='.$cod.'&nom='.$nom.'&ci='.$ci.'&con='.$con.'&tip='.$tip."&est=".$est."&file=xls");
		$word = array('controller' => 'capital_humano/export_empleados?cod='.$cod.'&nom='.$nom.'&ci='.$ci.'&con='.$con.'&tip='.$tip."&est=".$est."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cód. bio.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>CI</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 30%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre completo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Grado académico</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fecha de nacimiento</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Edad</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Teléfono</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Email</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="11" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fecha de ingreso</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="12" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cargo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="13" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Calidad producción</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="14" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Antiguedad</small></label>
								</div>
							</th>							
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="15" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Tipo de empleado</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="16" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Areas de trabajo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="17" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Tipo de contrato</small></label>
								</div>
							</th>
						<?php if($privilegio->ca2r==1){ ?>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="18" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Salario</small></label>
								</div>
							</th>
						<?php } ?>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="19" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Estado</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 35%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="20" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Características</small></label>
								</div>
							</th>
						</tr>
			</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="19" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE ALMACENES']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="5%" style="display: none;">#</th>
								<th class="celda th padding-4" data-column="2" width="6%">Fotografía</th>
								<th class="celda th padding-4" data-column="3" width="5%">Cód. bio.</th>
								<th class="celda th padding-4" data-column="4" width="10%">CI</th>
								<th class="celda th padding-4" data-column="5" width="33%">Nombre completo</th>
								<th class="celda th padding-4" data-column="6" width="10%" >Grado académico</th>
								<th class="celda th padding-4" data-column="7" width="7%">Fecha de nacimiento</th>
								<th class="celda th padding-4" data-column="8" width="10%">Edad</th>
								<th class="celda th padding-4" data-column="9" width="10%">Telf. o Cel.</th>
								<th class="celda th padding-4" data-column="10" width="5%" >Email</th>
								<th class="celda th padding-4" data-column="11" width="10%">Fecha de ingreso</th>
								<th class="celda th padding-4" data-column="12" width="10%">Cargo</th>
								<th class="celda th padding-4" data-column="13" width="10%">Calidad de producción</th>
								<th class="celda th padding-4" data-column="14" width="10%" >Antiguedad</th>
								<th class="celda th padding-4" data-column="15" width="10%">Tipo de empleado</th>
								<th class="celda th padding-4" data-column="16" width="15%">Areas de trabajo</th>
								<th class="celda th padding-4" data-column="17" width="10%">Tipo de contrato</th>
							<?php if($privilegio->ca2r==1){ ?>
								<th class="celda th padding-4" data-column="18" width="10%">Salario [Bs.]</th>
							<?php }?>
								<th class="celda th padding-4" data-column="19" width="5%">Estado</th>
								<th class="celda th padding-4" data-column="20" width="30%" >Características</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){ 
									$empleado=$this->lib->search_elemento($empleados,"ide",$visible);
									if($empleado!=null){
										$cont++;
										$img='sistema/miniatura/default.jpg';
										if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img="personas/miniatura/".$empleado->fotografia;}
										$calidad_produccion=0;
										$total_registros=0;
										$text_procesos="";
										$procesos_empleado=$this->lib->select_from($procesos_empleados,"ide",$empleado->ide);
										for($i=0;$i<count($procesos_empleado);$i++){$proceso_empleado=json_decode($procesos_empleado[$i]);
											$text_procesos.="- ".$proceso_empleado->nombre;
											if($proceso_empleado->tipo==0){$text_procesos.=" (Maestro)<br/>";}else{$text_procesos.=" (Ayudante) <br/>";}
											$producto_empleado=$this->lib->search_elemento($productos_empleados,'idpre',$proceso_empleado->idpre);
											if($producto_empleado!=null){
												$calidad_produccion+=$producto_empleado->calidad;
												$total_registros++;
											}
										}
										if($total_registros>0){$calidad_produccion=number_format(($calidad_produccion/$total_registros),0,'.','');}
										if($calidad_produccion>0){if($calidad_produccion>1){$calidad_produccion.="pts.";}else{$calidad_produccion.="pt.";}}else{ $calidad_produccion="";}
							?>
								<tr class="fila">
									<td class="celda td padding-4"  data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $empleado->codigo;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $empleado->ci.$empleado->abreviatura;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $empleado->nombre_completo;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $empleado->grado_academico;?></td>
									<td class="celda td padding-4" data-column="7"><?php if($this->val->fecha($empleado->fecha_nacimiento)){echo $this->lib->format_date($empleado->fecha_nacimiento,'d/m/Y');}?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $this->lib->calcula_edad($empleado->fecha_nacimiento);?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $empleado->telefono;?></td>
									<td class="celda td padding-4" data-column="10"><?php echo $empleado->email;?></td>
									<td class="celda td padding-4" data-column="11"><?php if($this->val->fecha($empleado->fecha_ingreso)){echo $this->lib->format_date($empleado->fecha_ingreso,'d/m/Y');}?></td>
									<td class="celda td padding-4" data-column="12"><?php echo $empleado->cargo;?></td>
									<td class="celda td padding-4" data-column="13"><?php echo $calidad_produccion;?></td>
									<td class="celda td padding-4" data-column="14"><?php echo $this->lib->antiguedad($empleado->fecha_ingreso);?></td>
									<td class="celda td padding-4" data-column="15"><?php if($empleado->tipo==0){ echo "Empleado en planta"; }if($empleado->tipo==1){ echo "Empleado administrativo";}?></td>
									<td class="celda td padding-4" data-column="16"><?php echo $text_procesos;?></td>
									<td class="celda td padding-4" data-column="17"><?php echo $tipo_contrato[$empleado->tipo_contrato]." (".$empleado->horas."hrs.)";?></td>
								<?php if($privilegio->ca2r==1){ ?>
									<td class="celda td padding-4 text-right" data-column="18"><?php echo number_format($empleado->salario,2,'.',',');?></td>
								<?php }?>
									<td class="celda td padding-4" data-column="19"><?php echo $estado[$empleado->estado];?></td>
									<td class="celda td padding-4" data-column="20"><?php echo $empleado->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>