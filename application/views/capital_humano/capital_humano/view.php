<?php $url=base_url().'libraries/img/';$j_empleados=json_encode($empleados); ?>
	<table class="table table-bordered table-hover" id="tbl-container">
		<thead>
			<tr>
				<th class="img-thumbnail-60"><div class="img-thumbnail-60">#Item</div></th>
				<th class='celda-sm-6 hidden-sm'>Cod.</th>
				<th class='celda-sm-10'>CI</th>
				<th class='celda-sm-25'>Nombre</th>
				<th class='hidden-sm' width="15%">Tipo de contrato</th>
				<th class='hidden-sm' width="13%">Tipo de empleado</th>
			<?php if($privilegio->ca2r==1){ ?>
				<th class='hidden-sm' width="12%">Salario(Bs.)</th>
			<?php }?>
				<th class='hidden-sm' width="15%">Procesos que realiza</th>
			<?php if($privilegio->ca1u=="1"){?>
				<th width="4%">Est.</th>
			<?php }?>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?php if(count($empleados)>0){$cont=0;
		for($i=0; $i < count($empleados); $i++){ $empleado=$empleados[$i]; $img="sistema/miniatura/default.jpg";
			if($empleado->fotografia!="" && $empleado->fotografia!=NULL){
				$img="personas/miniatura/".$empleado->fotografia;
			}
			$control=null;
			if($atrib!="" && $val!="" && $type_search!=""){
				$control=$this->lib->search_where($empleado,$atrib,$val,$type_search);
			}
?>
			<tr<?php if($empleado->estado!=1){?> class="inactivo" title="Empleado inactivo"<?php }?><?php if(($estado!="" && $estado!=$empleado->estado) || ($atrib!="" && $val!="" && $type_search!="" && $control==null)){?> style="display: none;"<?php }else{$cont++;}?> data-e="<?php echo $empleado->ide;?>">
				<td><div id="item"><?php echo $cont;?></div><img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail" data-title="<?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno; ?>" data-desc="<br>"></td>
				<td class="hidden-sm" data-col="2">
					<?php if($atrib=="codigo" && $val!="" && $control!=null){echo $this->lib->str_replace_all($empleado->codigo,$this->lib->all_minuscula($val),"mark");}else{echo $empleado->codigo;}?>
				</td>
				<td data-col="3">
					<?php if($atrib=="ci" && $val!="" && $control!=null){echo $this->lib->str_replace_all($empleado->ci." ".$empleado->abreviatura,$this->lib->all_minuscula($val),"mark");}else{echo $empleado->ci." ".$empleado->abreviatura;;}?>
				</td>
				<td data-col="4">
				<?php if($atrib=="nombre_completo" && $val!="" && $control!=null){echo $this->lib->str_replace_all($empleado->nombre_completo,$this->lib->all_minuscula($val),"mark");}else{echo $empleado->nombre_completo;}?>
				</td>
				<td class='hidden-sm' data-col="5" data-value="<?php echo $empleado->idtc;?>">
				<?php if($atrib=="idtc" && $val!="" && $control!=null){?><mark><?php }?>
					<?php if($empleado->tipo_contrato==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";} echo " (".$empleado->horas."hrs.)";?>
					<?php if($atrib=="idtc" && $val!="" && $control!=null){?></mark><?php }?>
				</td>
				<td class='hidden-sm' data-col="6" data-value="<?php echo $empleado->tipo;?>">
				<?php if($atrib=="tipo" && $val!="" && $control!=null){?><mark><?php }?>
					<?php if($empleado->tipo==0){ echo "Empleado en planta"; }if($empleado->tipo==1){ echo "Empleado administrativo";}?>
					<?php if($atrib=="tipo" && $val!="" && $control!=null){?></mark><?php }?>
				</td>
				<?php if($privilegio->ca2r==1){ ?>
				<td class='hidden-sm'><?php echo number_format($empleado->salario,2,'.',',');?></td>
				<?php }?>
				
			<?php $procesos=$this->M_proceso_empleado->get_proceso($empleado->ide); ?>
				<td class='hidden-sm'>
			<?php  if(!empty($procesos)){ 
					for($p=0; $p < count($procesos); $p++){ $proceso=$procesos[$p];
						if($proceso->tipo==0){$tipo="Maestro";}else{$tipo="Ayudante";}
			?>
						<?php echo $proceso->nombre."(".$tipo."), ";?>
			<?php 	}// end for
				}//end if
			?>
				</td>
			<?php if($privilegio->ca1u=="1"){?>
				<td><center>
					<div class="btn-circle confirm_estado btn-circle-<?php if($empleado->estado==1){ echo 'success';}else{ echo 'default';}?> btn-circle-30 " data-e="<?php echo $empleado->ide;?>" data-estado="<?php echo $empleado->estado;?>"></div>
				</center></td>
			<?php }?>
				<td class="text-right">
				<?php 
					$det=json_encode(array('function'=>'detalle_empleado','atribs'=> json_encode(array('e' => $empleado->ide)),'title'=>'Detalle'));
					$conf=""; if($privilegio->ca1u=="1"){ $conf=json_encode(array('function'=>'config_empleado','atribs'=> json_encode(array('e' => $empleado->ide)),'title'=>'Configurar'));}
					$del=""; if($privilegio->ca1d=="1"){ $del=json_encode(array('function'=>'confirm_empleado','atribs'=> json_encode(array('e' => $empleado->ide)),'title'=>'Eliminar'));}
					$this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);
				?>
				</td>
			</tr>
	<?php }// end for
		}else{//end if
			echo "<tr><td colspan='9'><h2>0 registros encontrados...</h2></td></tr>";
		}?>
		</tbody>
	</table>
	<?php 
		if($privilegio->ca1r=="1" && ($privilegio->ca1p=="1" || $privilegio->ca1c=="1")){
			if($privilegio->ca1p=="1"){$report=json_encode(array('function'=>'print_empleados','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
			if($privilegio->ca1c=="1"){$new=json_encode(array('function'=>'new_empleado','title'=>'Nuevo empleado'));}
			if($privilegio->ca1p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
			$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
		}
	?>
<script>$("img.img-thumbnail").visor();$(".confirm_estado").confirm_estado();$(".detalle_empleado").detalle_empleado();$(".config_empleado").config_empleado();$(".confirm_empleado").confirm_empleado();</script>