<?php $v = array('0' => 'Empleado en planta','1' => 'Empleado administrativo',);
	$es = array('1' => 'Empleado activo','0' => 'Empleado inactivo',);
?>
<table class="tabla tabla-border-false" id="tbl-container">
	<tr class="fila">
		<th class="img-thumbnail-60"><div class="img-thumbnail-60"></div></th>
		<td class='celda-sm-6 hidden-sm'>
			<form class="search-empleado">
				<input type="text" class="form-control form-control-sm search_capital_humano" id="s_cod" placeholder='Código'>
			</form>
			<script>Onfocus('s_cod');</script>
		</td>
		<td class='celda-sm-10 hidden-sm'>
			<form  class="search-empleado">
				<input type="number" class="form-control form-control-sm search_capital_humano" id="s_ci" maxlength="8" placeholder='CI' min='0'>
			</form>
		</td>

		<td class='celda-sm-25'>
			<form class="search-empleado">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_capital_humano" maxlength="100" placeholder='Buscar nombre...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_pedido" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' width="15%">
			<select id="s_con"  class="form-control form-control-sm search_capital_humano view_empleados">
				<option value="">Seleccionar</option>
			<?php for ($i=0; $i < count($tipo_contratos) ; $i++) { ?>
				<option value="<?php echo $tipo_contratos[$i]->idtc?>"><?php if($tipo_contratos[$i]->tipo==0){ echo "Tiempo completo";}else{ echo "Medio Tiempo";} echo " (".$tipo_contratos[$i]->horas."hrs.)";?></option>
			<?php }?>
			</select>
		</td>
		<td class='hidden-sm' width="13%">
			<select id="s_tip"  class="form-control form-control-sm search_capital_humano view_empleados">
				<option value="">Seleccionar</option>
			<?php foreach ($v as $key => $value) { ?>
				<option value="<?php echo $key;?>"><?php echo $value;?></option>
			<?php }?>
			</select>
		</td>
		<td class='celda-sm-15'>
			<select id="s_est"  class="form-control form-control-sm view_empleados">
			<?php foreach ($es as $key => $value) { ?>
				<option value="<?php echo $key;?>"><?php echo $value;?></option>
			<?php }?>
				<option value="">Todos</option>
			</select>
		</td>
		<td class='hidden-sm' style="width:15%"></td>
		<td class='text-right hidden-sm'>
			<?php 
				$search=json_encode(array('function'=>'view_empleados','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_empleados','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>$(".view_empleados").view_empleados();$(".search_capital_humano").search_capital_humano();
$("form.search-empleado").submit(function(e){ $(this).view_empleados();e.preventDefault();});$("select.search-empleado").change(function(){$(this).view_empleados();$(this).reset_input(this.id);});$("input.search-empleado").keyup(function(){$(this).reset_input(this.id);});$("input[type='number'].search-empleado").click(function(){$(this).reset_input(this.id);});</script>
