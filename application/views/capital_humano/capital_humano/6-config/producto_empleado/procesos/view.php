<?php 
	$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
	$pa=json_decode($procesos_asignados);
	$procesos=[];
	for($j=0; $j < count($producto_procesos); $j++){ $proceso=$producto_procesos[$j];
		$control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
		$control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
		if($control1==null && $control2==null){
			$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
		}else{
			$control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$producto_grupo->idpgr);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
			}
			$control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$producto_grupo_color->idpgrc);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr);
			}
		}
	}// enf for
	$procesos=json_decode(json_encode($procesos));
?>
<div class="table-responsive g-table-responsive" id="datos-proceso-empleado" data-tbl="<?php echo $tbl;?>" data-type="<?php echo $type;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="g-thumbnail">#</th>
				<th width="60%">Procesos en el empleado</th>
				<th width="25%">Categoría</th>
				<th width="5%"></th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
			<?php for($i=0; $i <count($procesos_empleado); $i++){ $proceso_empleado=$procesos_empleado[$i];
				$es_proceso=$this->lib->search_json($procesos,"idpr",$proceso_empleado->idpr);
			?>
				<tr>
					<td class="g-thumbnail"><?php echo $i+1;?></td>
					<td><?php echo $proceso_empleado->nombre;?></td>
					<td><?php echo $t[$proceso_empleado->tipo*1]; ?></td>
					<td>
						<?php $asignado=false; foreach($pa as $clave => $proceso_asignado){ if($proceso_empleado->idpre==$proceso_asignado){ $asignado=true; break;}} ?>
						<?php if($es_proceso==null){ $help='title="Imposible asignar" data-content="Imposible asignar el proceso, pues el producto no tiene asignado el proceso <strong>'.$proceso_empleado->nombre.'</strong>."';}?>
						<button type="button" class="btn <?php if(!$asignado && $es_proceso!=null){ ?>btn-inverse-primary btn_add_proceso_producto<?php }else{?>btn-default<?php }?> btn-mini waves-effect waves-light"<?php if(!$asignado && $es_proceso!=null){ ?> data-pre="<?php echo $proceso_empleado->idpre;?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{ if($es_proceso!=null){?> disabled="disabled"<?php }else{ echo $popover4.$help;}}?>> <?php if($es_proceso!=null){?><i class="fa fa-plus"></i> add <?php }else{?><i class="fa fa-info-circle"></i>  inf. <?php }?></button>
					</td>
					<td>
						<div class="g-control-accordion" style="width:30px;">
							<button class="confirmar_proceso_empleado" data-pre="<?php echo $proceso_empleado->idpre; ?>" data-type-save="pestania_producto"><i class="icon-trashcan2"></i></button>
						</div>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>	
	</div>
		<div class="row text-right" style="padding-right:15px;">
          	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light view_producto_proceso" data-type-save="pestania_producto">
          		<i class="fa fa-plus"></i><span class="m-l-10">proceso</span>
            </button>
        </div>
	<script>$("button.btn_add_proceso_producto").click(function(e){ $(this).add_proceso_producto();});$(".btn-change-prod").click(function(e){ $(this).add_producto();});$("button.view_producto_proceso").click(function(){ $(this).view_producto_proceso();});$("button.confirmar_proceso_empleado").click(function(){ $(this).confirmar_proceso_empleado();});$('[data-toggle="popover"]').popover({html:true});</script>
