<?php 
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<tr class="row-proceso" data-pre="<?php echo $proceso_empleado->idpre;?>">
	<td width="70%"><?php echo $proceso_empleado->nombre." (".$t[$proceso_empleado->tipo].")"; ?></td>
	<td width="30%">
		<?php $id_rango=rand(0,9999).'-'.$proceso_empleado->idpre; ?>
		<div class="rangos" id="rangos<?php echo $id_rango;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-pre="<?php echo $proceso_empleado->idpre;?>" data-type-save="new">
			<input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>"  data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>"  data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>"  data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>"  data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>"  data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>"  data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>"  data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>"  data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
			<input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>"  data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
		</div>
	</td>
	<td><span class="g-control-accordion" style="text-align: right; width: 54px;"><button class="drop_this_elemento" data-padre="row-proceso"  data-type="empleado_proceso" style="padding-bottom: 0px;"><i class="icon-close"></i></button></span><script>$("button.drop_this_elemento").click(function(){$(this).drop_this_elemento();});</script></td>
</tr>