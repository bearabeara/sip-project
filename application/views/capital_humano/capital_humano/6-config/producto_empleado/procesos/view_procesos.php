<?php
	$procesos=[];
	for($j=0; $j < count($producto_procesos); $j++){ $proceso=$producto_procesos[$j];
		$control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
		$control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
		if($control1==null && $control2==null){
			$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
		}else{
			$control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$producto_grupo->idpgr);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
			}
			$control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$producto_grupo_color->idpgrc);
			if($control!=null){
				$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
			}
		}
	}// enf for
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="75%">Procesos del producto</th>
					<th width="20%"></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($procesos)){ $procesos=json_decode(json_encode($procesos)); $i=1;?>
				<?php foreach($procesos as $key => $proceso){ ?>
					<?php $control=$this->lib->search_elemento($empleado_procesos,"idpr",$proceso->idpr);?>
				<tr>
					<td><?php echo $i++;?></td>
					<td><?php echo $proceso->nombre;?></td>
					<td>
						<select class="form-control form-control-sm input-140"<?php if($control==null){?> id="car<?php echo $proceso->idpr;?>"<?php }else{?> disabled="disabled"<?php }?>>
						<?php for ($j=0; $j < count($t); $j++) {  ?>
							<option value="<?php echo $j;?>" <?php if($control!=null){ if($control->tipo==$j){?> selected="selected"<?php }}?>><?php echo $t[$j];?></option>
						<?php } ?>
						</select>
					</td>
					<td>
						<button type="button" class="btn btn-<?php if($control==null){ ?>inverse-primary add_proceso_empleado <?php }else{?>default <?php }?>btn-mini waves-effect waves-light"<?php if($control==null){ ?> data-pr="<?php echo $proceso->idpr;?>" data-type="produccion" <?php }else{?> disabled="disabled"<?php }?>><i class="fa fa-plus"></i> add</button>
					</td>
				</tr>
				<?php }// for?>
			<?php }else{?>
				<tr><td colspan="3" class="text-center"><h3>0 registros encontrados...</h3></td></tr>
			<?php }?>
			</tbody>
		</table>
	</div>
</div>
<script>$("button.add_proceso_empleado").click(function(){ $(this).add_proceso_empleado();});</script>