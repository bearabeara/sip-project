<?php 
$help1='title="Alerta" data-content="El producto no tiene ninguna categoría registrada."';
$help2='title="Alerta" data-content="El empleado no tiene ningun proceso asignado."';
$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
$url=base_url().'libraries/img/productos/miniatura/';
$productos_asignados=json_decode($pa);
$alerta="";
$str_proceso_empleado="";
for($k=0; $k < count($procesos_empleado); $k++){ $pr=$procesos_empleado[$k];
	$proceso=$this->M_proceso->get($pr->idpr);
	if($str_proceso_empleado==""){ $str_proceso_empleado=$proceso[0]->nombre;}else{ $str_proceso_empleado.=", ".$proceso[0]->nombre;}
}
if($str_proceso_empleado!=""){ $str_proceso_empleado="<strong>".$str_proceso_empleado."</strong>";}
?>
<div class="table-responsive g-table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="g-thumbnail"><div class="g-thumbnail"></div>#Item</th>
				<th width="10%">Código</th>
				<th width="60%">Nombre de producto</th>
				<th width="25%">Categorías</th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
		<?php for($i=0; $i <count($productos); $i++){ $producto=$productos[$i];
				$img="default.png";
				if($producto->portada!="" && $producto->portada!=null){
					$v=explode("|", $producto->portada);
					if(count($v==2)){
						if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img=$control[0]->archivo;}}
						if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img=$control[0]->archivo;}}
					}
				}
		?>
				<tr>
					<td class="g-thumbnail">
						<div id="item"><?php echo $i+1;?></div>
						<a href="javascript:" onclick="img_producto($(this))" data-p="<?php echo $producto->idp;?>">
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-thumbnail">
						</a>
					</td>
					<td><?php echo $producto->codigo;?></td>
					<td><?php echo $producto->nombre; ?></td>
					<td>
					<?php $grupos=$this->lib->select_from($grupos_colores,"idp",$producto->idp);
						if(count($grupos)>0){
					?>
							<ul class="list-group" style="font-size:10px; padding: 1px">
							<?php 
								for ($c=0; $c < count($grupos) ; $c++) { $grupo=$grupos[$c]; ?>
								<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
							?>
									<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; width:100%; background:<?php echo $grupo->codigo_c;?>"><?php echo $gr.$grupo->nombre_c;?></li>
									<?php }?>
								</ul>
								<?php } ?>
					</td>
					<td style="text-align: right;">
						<?php if(count($grupos)>0){ ?>
							<?php 
								$procesos=array();
								for($k=0; $k < count($producto_proceso); $k++){ $proceso=$producto_proceso[$k];
									if($proceso->idp==$producto->idp){ $procesos[]=array("idppr" => $proceso->idppr,"idpr" => $proceso->idpr,"nombre"=>$proceso->nombre);}
								}
								$procesos_producto=json_decode(json_encode($procesos));
								$procesos_producto_empleado=array();
								$nombre_procesos="";
								foreach($procesos_producto as $key => $proceso_producto){
									$control=$this->lib->search_elemento($procesos_empleado,"idpr",$proceso_producto->idpr);
									if($control!=null){ $procesos_producto_empleado[]=array("idppr" => $proceso_producto->idppr,"idpr" => $proceso_producto->idpr,"nombre"=>$proceso_producto->nombre); }
								}
								if(count($procesos_producto_empleado)<=0){
									$alerta=$popover4.'title="Alerta" data-content="El producto no tiene ninguno de los siguientes procesos asignados: '.$str_proceso_empleado.'."';
								}
								if(count($procesos_producto_empleado)>0){?>
									<?php $asignado=false; foreach($productos_asignados as $key => $pa){ if($pa==$producto->idp){ $asignado=true;break; } } ?>
									<button type="button" class="btn <?php if(!$asignado){ ?>btn-inverse-primary add_producto<?php }else{?>btn-default<?php }?>  btn-mini waves-effect waves-light"<?php if(!$asignado){ ?> data-p="<?php echo $producto->idp?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{?> disabled="disabled"<?php }?>>
										<i class="fa fa-plus"></i> add
									</button>
							<?php }else{?>
									<button type="button" class="btn btn-default btn-mini waves-effect waves-light" <?php echo $alerta;?>> <i class="fa fa-info-circle"></i>  inf. </button>
						<?php	}
							?>
						<?php }else{
							$alerta=$popover4.'title="Alerta" data-content="El producto no tiene ningun color registrado."';
						?>
								<label class="text-danger" <?php echo $alerta;?>><i class="fa fa-info-circle"></i></label>
						<?php } ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<script>$("button.add_producto").click(function(e){ $(this).add_producto();});$("button.btn_add_emp_prod").click(function(e){ $(this).btn_add_emp_prod();});$('[data-toggle="popover"]').popover({html:true});</script>