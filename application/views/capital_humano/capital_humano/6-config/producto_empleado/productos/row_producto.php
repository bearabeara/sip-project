<?php 
  $help1='title="Observaciónes" data-content="Ingrese algunas observaciónes del producto, solo se acepta un contenido en formato alfanumérico <b>hasta 300 caracteres, puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;°ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
  $id=$producto->idp."-".rand(0,999999);
	$url=base_url().'libraries/img/'; $p_img="sistema/miniatura/default.jpg";
	if($producto->portada!="" && $producto->portada!=null){
		$v=explode("|", $producto->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
		}
  }
  $t = array(0 => 'Maestro', 1 => 'Ayudante');
 ?>
              <div class="accordion-panel" id="accordion-panel<?php echo $id;?>">
                <div class="accordion-heading" role="tab" id="heading<?php echo $id;?>">
                  <span class="card-title accordion-title">
                      <span class="g-control-accordion">
                        <button class="drop_producto" data-padre="accordion-panel"><i class="icon-bin"></i></button>
                      </span>
                    <a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id;?>" aria-expanded="false" aria-controls="collapse<?php echo $id;?>">
                    <div class="item item-1"><?php echo $c1++;?></div>
                      <span class="g-img-modal"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail g-thumbnail-modal"></span>
                      <span><?php $prod=$producto->codigo; if($prod!="" && $prod!=NULL){ $prod.=" - ".$producto->nombre;}else{ $prod=$producto->nombre;} echo $prod; ?></span>
                    </a>
                  </span>
                </div>
                <div id="collapse<?php echo $id;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id;?>" aria-expanded="false" style="height: 0px;">
                  <div class="accordion-content accordion-desc" data-p="<?php echo $producto->idp;?>">
                    <div class="g-table-responsive">
                      <?php $idtb="prod".$producto->idp.'-'.rand(0,999999);?>
                      <table class="table table-bordered" id="<?php echo $idtb;?>">
                        <thead>
                          <tr>
                            <th class="img-thumbnail-50"><div class="img-thumbnail-50"></div>#Item</th>
                            <th width="30%">Producto</th>
                            <th width="70%">Proceso - calidad</th>
                          </tr>
                        </thead>
                        <tbody class="producto">
                          <?php if(!empty($grupos)){ $c2=1; ?>
                          <?php foreach ($grupos as $key => $grupo){ 
                                  $procesos=[];
                                  for($j=0; $j < count($producto_procesos); $j++){ $proceso=$producto_procesos[$j];
                                    $control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
                                    $control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
                                    if($control1==null && $control2==null){
                                      $procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
                                    }else{
                                      $control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$grupo->idpgr);
                                      if($control!=null){
                                        $procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
                                      }
                                      $control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$grupo->idpgrc);
                                      if($control!=null){
                                        $procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
                                      }
                                    }
                                  }
                                  $procesos=json_decode(json_encode($procesos));
                                  //buscando si existe los procesos el empleado en el color del producto
                                  $procesos_producto_empleado = array();
                                  foreach ($procesos as $key => $proceso) {
                                    $control=$this->lib->search_elemento($procesos_empleado,"idpr",$proceso->idpr);
                                    if($control!=null){
                                      $procesos_producto_empleado[]=array('idpre'=>$control->idpre,'idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
                                    }
                                  }
                                  if(count($procesos_producto_empleado)>0){
                                    $procesos_producto_empleado=json_decode(json_encode($procesos_producto_empleado));
                                    $idtb_2="tbl-proc".rand(0,99999)."-".$grupo->idpgrc;
                          ?>
                          <tr class="row-producto" id="<?php echo $grupo->idpgrc."-".rand(10,99999);?>" data-pgc="<?php echo $grupo->idpgrc;?>">
                            <?php $c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
                            $pgc=$grupo->idpgrc."-".rand(1,9999);
                            $img="sistema/miniatura/default.jpg";
                            if($grupo->portada!="" && $grupo->portada!=null){
                              $v=explode("|", $grupo->portada);
                              if(count($v==2)){
                                if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                                if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
                              }
                            }else{
                              $img=$p_img;
                            } ?>
                            <td class="img-thumbnail-50"><div class="item item-2"><?php echo $c2++;?></div><div class="img-thumbnail-50"><img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50"></div></td>
                            <td><?php echo $gr.$grupo->nombre_c;?></td>
                            <td>
                              <table class="table table-bordered table-hover" id="<?php echo $idtb_2;?>" style="margin-bottom: 0px;">
                                <thead></thead>
                                <tbody>
                                  <?php foreach ($procesos_producto_empleado as $key => $proceso) {?>
                                  <tr class="row-proceso" data-pre="<?php echo $proceso->idpre;?>" data-save="new">
                                    <td width="70%"><?php echo $proceso->nombre." (".$t[0].")"; ?></td>
                                    <td width="30%">
                                      <?php $id_rango=rand(0,9999)."-".$grupo->idpgrc.'-'.$proceso->idpre;?>
                                      <div class="rangos" id="rangos_proe<?php echo $id_rango;?>" data-type-save="new" data-pre="<?php echo $proceso->idpre;?>" data-pgc="<?php echo $grupo->idpgrc;?>">
                                          <input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
                                          <input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
                                       </div>
                                    </td>
                                    <td>
                                      <div class="g-control-accordion" style="width:54px; text-align: right;">
                                        <button class="drop_this_elemento" data-proe="" data-padre="row-proceso"><i class="icon-close"></i></button>
                                      </div>
                                    </td>
                                  </tr>
                                <?php }// end foreach ?>
                                </tbody>
                              </table>
                              <div class="row text-right" style="padding-right:15px; margin-top: 5px;">
                                <button type="button" class="btn btn-flat flat-info txt-primary waves-effect btn-mini procesos_empleado" data-tbl="<?php echo $idtb_2;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-p="<?php echo $producto->idp;?>" data-type="empleado_proceso">
                                  <i class="fa fa-plus"></i><span class="m-l-10">proceso</span>
                                </button>
                              </div>
                            </td>
                              
                            </tr>
                              <?php }// end if(count($procesos)>0)?>
                          <?php }// end foreach?>
                        <?php }// end if?>
                        </tbody>
                      </table>
                    </div>
                    <div class="row text-right" style="padding-right:15px; margin-top: 5px;">
                      <button type="button" class="btn btn-inverse-info waves-effect btn-mini colores_producto" data-tbl="<?php echo $idtb;?>" data-p="<?php echo $producto->idp;?>" data-type="new_prod_emp">
                        <i class="fa fa-plus"></i><span class="m-l-10">categoría</span>
                      </button>
                    </div>
                    <script>$("button.update_producto_colores_empleado").click(function(){ $(this).update_producto_colores_empleado();});</script>
                  </div>
                </div>
                  <script> $("button.colores_producto").click(function(){ $(this).colores_producto();});</script>
                  <script>$(".view_prod").click(function(){ $(this).view_producto();});$('[data-toggle="popover"]').popover({html:true});</script>
              </div>
              <script>$("button.drop_producto").click(function(){ $(this).drop_producto();});$("button.drop_this_elemento").click(function(){$(this).drop_this_elemento();});$("button.procesos_empleado").click(function(){ $(this).procesos_empleado();});</script>