<?php 
	$url=base_url().'libraries/img/';
	$p_img="sistema/miniatura/default.jpg";
	$colores_asignados=json_decode($colores_asignados);
	if($producto->portada!="" && $producto->portada!=null){
		$v=explode("|", $producto->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
		}
  	}
?>
<div class="table-responsive g-table-responsive" id="datos_producto_add_color" data-tbl="<?php echo $tbl;?>" data-p="<?php echo $producto->idp;?>" <?php if(isset($type)){?> data-type="<?php echo $type;?>"<?php } ?>>
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="img-thumbnail-50"><div class="img-thumbnail-50"></div></th>
				<th width="25%">Código</th>
				<th width="60%">Producto</th>
				<th width="5%"></th>
			</tr>
		</thead>
	<?php if(count($colores)>0){ ?>
		<tbody>
		<?php 
			for($i=0; $i < count($colores); $i++){ $color=$colores[$i]; 
				$c_gr=""; $gr="";if($color->abr_g!="" || $color->abr_g!=NULL){ $gr=$color->nombre_g;$c_gr=$color->abr_g;}
				if($gr!=""){$gr.=" - ";}$gr.=$color->nombre_c;
				if($c_gr!=""){$c_gr.="-";}$c_gr.=$color->abr_c;
				if($color->portada!="" && $color->portada!=null){
					$v=explode("|", $color->portada);
					if(count($v==2)){
						if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
						if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
					}
			  	}else{
					if($producto->portada!="" && $producto->portada!=null){
						$v=explode("|", $producto->portada);
						if(count($v==2)){
							if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
							if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $p_img="productos/miniatura/".$control[0]->archivo;}}
						}
				  	}
			  	}
			  	$btn_info=true;
			  	if(!empty($procesos_empleado)){
					$procesos=[];
					for($j=0; $j < count($producto_procesos); $j++){ $proceso=$producto_procesos[$j];
						$control1=$this->lib->search_elemento($producto_grupo_proceso,"idppr",$proceso->idppr);
						$control2=$this->lib->search_elemento($producto_grupo_color_proceso,"idppr",$proceso->idppr);
						if($control1==null && $control2==null){
							$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
						}else{
							$control=$this->lib->search_elemento_2n($producto_grupo_proceso,"idppr",$proceso->idppr,"idpgr",$color->idpgr);
							if($control!=null){
								$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
							}
							$control=$this->lib->search_elemento_2n($producto_grupo_color_proceso,"idppr",$proceso->idppr,"idpgrc",$color->idpgrc);
							if($control!=null){
								$procesos[] = array('idppr'=>$proceso->idppr, 'idp'=>$proceso->idp, 'idpr'=>$proceso->idpr, 'nombre'=>$proceso->nombre, 'detalle'=>$proceso->detalle);
							}
						}
					}
					$procesos=json_decode(json_encode($procesos));
					for ($k=0; $k < count($procesos_empleado) ; $k++) { $proceso_empleado=$procesos_empleado[$k];
						foreach ($procesos as $key => $proceso) {
							if($proceso_empleado->idpr==$proceso->idpr){
								$btn_info=false;
								break;
							}
						}
					}
			  	}
		?>
			<tr>
				<td class="img-thumbnail-50"><span class="g-img-modal"><img src="<?php echo $url.$p_img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50"></span></td>
				<td><?php echo $producto->codigo."-".$c_gr; ?></td>
				<td><?php echo $producto->nombre." - ".$gr; ?></td>
				<?php $control=false;
					foreach($colores_asignados as $key => $pgc){ if($pgc==$color->idpgrc){ $control=true; break; }}
				?>
				<td><button type="button" class="btn <?php if($control || $btn_info){ ?>btn-default<?php }else{ ?>btn-inverse-primary btn-add-producto-color<?php }?> btn-mini waves-effect waves-light" <?php if($control){ if(!$btn_info){?> disabled="disabled"<?php }else{?> <?php }}else{?> data-pgc="<?php echo $color->idpgrc;?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }?>><?php if(!$btn_info){?><i class="fa fa-plus"></i> add<?php }else{?><i class="fa fa-info-circle"></i> inf.<?php }?></button></td>
			</tr>
		<?php } ?>
		</tbody>
	<?php } ?>
	</table>
</div>
<script>$(".btn-add-producto-color").click(function(){ $(this).add_producto_color();})</script>