<?php 
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png"';
	$help2='title="Nº Cédula" data-content="Ingrese un numero cédula de identidad con valores numericos <b>sin espacios</b>, de 6 a 8 digitos"';
	$help3='title="Ciudad" data-content="Seleccione la ciudad donde se difundio la cédula de identidad. Si desea adicionar una nueva ciudad puede hacerlo en la sección de configuración en el menu superior, o puede dar click el el boton <b>+</b>"';
	$help4='title="Primer nombre" data-content="Ingrese un nombre alfanumerico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="Segundo nombre" data-content="Ingrese un segundo nombre (si tuviera) alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el segundo nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="Apellido Paterno" data-content="Ingrese un apellido paterno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help7='title="Apellido Materno" data-content="Ingrese un apellido materno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help8='title="Código" data-content="Este código representa principalmente al codigo usado en el registro biométrico de 0 a 10 digitos."';
	$help9='title="Tipo de empleado" data-content="Seleccione un tipo de empleado, si el empleado trabaja en la planta de producción o es un empleado dedicado a actividades administrativas."';
	$help95='title="Grado académico" data-content="Ingrese el mayor grado académico del empleado. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 200 caractereres</b>."';
	$help10='title="Cargo" data-content="Ingrese el cargo o cargos que realiza. Ej. Encargado de cortado de piezas. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 100 caractereres</b>."';
	$help11='title="Grado académico" data-content="Representa al mayor grado académico alcanzado por el empleado. Ej. (Primaria, Secundaria, Técnico superior). El contenido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 100 caractereres</b>."';
	$help12='title="Tipo de contrato" data-content="Seleccione un tipo de contrato, esto va según la cargar horaria del empleado, si desea adicionar una nueva carga horaria puede realizarlo en la sección de configuración en el menu superior o pude dar click en el botón <b>+</b>."';
	$help13='title="Salario por mes" data-content="Representa al pago que recibira el empleado por cada mes trabajado, segun la carga horaria asignada, el valor acptado es de <b>0 a 9999.9</b> con una sola decimal."';
	$help14='title="Telf./Celular" data-content="Ingrese el número de teléfono o celular de referencia del empleado de 6 a 15 digitos numéricos."';
	$help15='title="Email" data-content="Representa al correo electrónico del empleado si tuviera, el formato del correo electrónico aceptado es <b>example@dominio.com</b>."';
	$help16='title="Fecha de nacimiento" data-content="Representa a la fecha de nacimiento del empleado, esta fecha es referencial para uso apropiado dentro la empresa."';
	$help17='title="Fecha de ingreso" data-content="Representa a la fecha de inicio de contrato en la empresa, esta fecha es referencial para uso apropiado dentro la empresa"';
	$help18='title="Dirección de domicilio" data-content="Representa a la dirección de domicilio del empleado, esta dirección tiene com fin como referencia y garantía para la empresa. El contenido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª/.,:;)</b>, con un <b>máximo de 200 caractereres</b>."';
	$help19='title="Característica" data-content="Representa a las característica propias del empleado si tuviera el contenido debe tener un formato alfanumerico de 0 a 900 caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>, con un <b>máximo de 900 caractereres</b>."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$v = array('0' => 'Empleado en planta','1' => 'Empleado administrativo');
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link active new_empleado" href="javascript:" role="tab"><i class="icofont icofont-home"></i>Nuevo empleado</a>
	</li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fotografía:</label></div>
			<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<input class="form-control form-control-xs" id="n_fot" type="file" placeholder='Seleccione fotografia'>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Nº Cédula:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_ci" type="number" data-type="empleado" placeholder='Número de cedula de identidad' min='100000' max='99999999' minlength="6" maxlength="8" step='any'>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Ciudad:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="n_ciu">
						<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($ciudades); $i++){ $ciudad=$ciudades[$i]; ?>
						<option value="<?php echo $ciudad->idci;?>">
							<?php echo $ciudad->nombre.'('.$ciudad->abreviatura.')';?>
						</option>
				<?php } ?>
					</select>
					<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<form onsubmit="return save_empleado()">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Primer nombre:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_nom1" type="text" placeholder='Primero nombre' maxlength="20">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</form>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<form onsubmit="return save_empleado()">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Segundo nombre:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_nom2" type="text" placeholder='Segundo nombre' maxlength="20">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</form>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Apellido paterno:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_pat" type="text" placeholder='Apellido paterno' maxlength="20">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Apellido materno:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_mat" type="text" placeholder='Apellido materno' maxlength="20">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Código:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" type="number" id="n_cod" placeholder='Código biométrico' min='0' max='9999999999' minlength="0" maxlength="10">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help8;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Tipo de empleado:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form onsubmit="return save_empleado()">
				<div class="input-group">
					<select id="n_tem" class="form-control form-control-xs">
						<option value="">Seleccionar</option>
					<?php foreach ($v as $key => $value) { ?>
						<option value="<?php echo $key;?>"><?php echo $value;?></option>
					<?php }?>
					</select>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help9;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Grado académico:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_gra" type="text" placeholder='Grado académico' maxlength="200" minlength="0">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help95;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Cargo:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_car" type="text" placeholder='Cargo del empleado' maxlength="100">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help10;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Telf./Celular:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_tel" type="number" placeholder='Número de teléfono o celular' min='100000' max="999999999999999" minlength="6" maxlength="15">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help14;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Email:</label></div>
			<div class="col-sm-4 col-xs-12">
			<form onsubmit="return save_empleado()">
				<div class="input-group">
					<input class="form-control form-control-xs color-class" id="n_ema" type="email" placeholder='example@dominio.com' maxlength="60">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help15;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fecha de nacimiento:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<input class="form-control form-control-xs" type="date"  id="n_fn">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help16;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fecha de ingreso:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<input class="form-control form-control-xs" type="date"  id="n_fi">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help17;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Tipo de contrato:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="n_tc">
						<option value="">Seleccionar...</option>
				<?php for ($i=0; $i < count($contrato); $i++) { $resp=$contrato[$i]; if($resp->tipo==0){ $nom_tip="Tiempo completo";}else{ $nom_tip="Medio tiempo";}
						?>
						<option value="<?php echo $resp->idtc;?>"><?php echo $nom_tip." (".$resp->horas."hrs.)";?></option>
				<?php } ?>
					</select>
					<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help12;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Salario mes (Bs.):</label></div>
			<div class="col-sm-4 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class='form-control form-control-xs color-class' type="number" id="n_sal" placeholder='Salario por mes' min='0' max="9999.9" minlength="0" maxlength="7" step="any" value="0">		
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help13;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Dirección de domicilio:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form onsubmit="return save_empleado()">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_dir" type="text" placeholder='Z/Villa Fatima C/Saturnino Porcel' maxlength="200">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help18;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Característica:</label></div>
			<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<textarea class="form-control form-control-xs color-class" id="n_obs" placeholder='Caracteristicas personales' maxlength="800" rows="3"></textarea>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help19;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
	</div>
</div>
<script>Onfocus("n_ci");$('[data-toggle="popover"]').popover({html:true});$('.color-class').maxlength();$("span.new_proceso_empleado").click(function(){$(this).new_proceso_empleado();});$("span.config_proceso_empleado").click(function(){$(this).config_proceso_empleado();});$("input#n_ci").search_ci();$("a.new_empleado").click(function(){$(this).new_empleado();});
</script>
