<?php 
	$url=base_url().'libraries/img/';
	$tipo_contrato = array('Tiempo completo', 'Medio tiempo');
	$estado = array('Inactivo', 'Activo');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_directivos', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'capital_humano/export_directivos?nom='.$nom.'&ci='.$ci."&file=xls");
		$word = array('controller' => 'capital_humano/export_directivos?nom='.$nom.'&ci='.$ci."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>CI</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 30%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre completo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cargo como directivo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fecha de nacimiento</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Edad</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Teléfono</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Email</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cuentas banarias</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 35%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="11" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Características</small></label>
								</div>
							</th>
						</tr>
			</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="19" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE DIRECTIVOS']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="5%" style="display: none;">#</th>
								<th class="celda th padding-4" data-column="2" width="6%">Fotografía</th>
								<th class="celda th padding-4" data-column="3" width="10%">CI</th>
								<th class="celda th padding-4" data-column="4" width="33%">Nombre completo</th>
								<th class="celda th padding-4" data-column="5" width="10%" >Cargo como directivo</th>
								<th class="celda th padding-4" data-column="6" width="7%">Fecha de nacimiento</th>
								<th class="celda th padding-4" data-column="7" width="10%">Edad</th>
								<th class="celda th padding-4" data-column="8" width="10%">Telf. o Cel.</th>
								<th class="celda th padding-4" data-column="9" width="5%" >Email</th>
								<th class="celda th padding-4" data-column="10" width="5%">Cuentas bancarias</th>
								<th class="celda th padding-4" data-column="11" width="30%" >Características</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){
									$directivo=$this->lib->search_elemento($directivos,"iddi",$visible);
									if($directivo!=null){
										$cont++;
										$img='sistema/miniatura/default.jpg';
										if($directivo->fotografia!=NULL && $directivo->fotografia!=""){ $img="personas/miniatura/".$directivo->fotografia;}
										$persona_cuentas=$this->lib->select_from($cuentas,'ci',$directivo->ci);
										$ciu="";
										$ciudad=$this->lib->search_elemento($ciudades,"idci",$directivo->idci);
										if($ciudad!=null){
											$ciu=$ciudad->abreviatura;
										}
							?>
								<tr class="fila">
									<td class="celda td padding-4"  data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $directivo->ci.$ciu;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $directivo->nombre_completo;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $directivo->cargo_directivo;?></td>
									<td class="celda td padding-4" data-column="6"><?php if($this->val->fecha($directivo->fecha_nacimiento)){echo $this->lib->format_date($directivo->fecha_nacimiento,'d/m/Y');}?></td>
									<td class="celda td padding-4" data-column="7"><?php echo $this->lib->calcula_edad($directivo->fecha_nacimiento);?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $directivo->telefono;?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $directivo->email;?></td>
									<td class="celda td padding-4" data-column="10">
								<?php
									if(count($persona_cuentas)>0){
								?>
										<table class="tabla tbl-bordered font-10">
								<?php 
										for($j=0;$j<count($persona_cuentas);$j++){$persona_cuenta=json_decode($persona_cuentas[$j]);
								?>
											<tr class="fila">
												<td class="celda td"><?php echo $persona_cuenta->razon." - ".$persona_cuenta->cuenta;?></td>
											</tr>
								<?php 
										}//end for
								?>
										</table>
								<?php 
									}//end if
								?>
									</td>
									<td class="celda td padding-4" data-column="11"><?php echo $directivo->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>