<?php 
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Directivos-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Directivos-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/';
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>REPORTE DE DIRECTIVOS</h3></center>
<table border="1" cellspacing="0" cellpadding="5">
	<thead>
		<tr>
			<th>#</th>
		<?php if($file=="doc"){?>
			<th>Fotografía</th>
		<?php }?>
			<th>CI</th>
			<th>Nombre completo</th>
			<th>Cargo como directivo</th>
			<th>Fecha de nacimiento</th>
			<th>Edad</th>
			<th>Telf. o Cel.</th>
			<th>Email</th>
			<th>Cuentas bancarias</th>
			<th>Características</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($directivos); $i++){$directivo=$directivos[$i];
		$img="sistema/miniatura/default.jpg";
		if($directivo->fotografia!="" && $directivo->fotografia!=NULL){$img="personas/miniatura/".$directivo->fotografia;}
		$persona_cuentas=$this->lib->select_from($cuentas,'ci',$directivo->ci);
		$ciu="";
		$ciudad=$this->lib->search_elemento($ciudades,"idci",$directivo->idci);
		if($ciudad!=null){$ciu=$ciudad->abreviatura;}
?>
	<tr>
		<td><?php echo $i+1;?></td>
	<?php if($file=="doc"){?>
		<td><img src="<?php echo $url.$img;?>" width="60px"></td>
	<?php }?>
		<td><?php echo $directivo->ci.$ciu;?></td>
		<td><?php echo $directivo->nombre_completo;?></td>
		<td><?php echo $directivo->cargo_directivo;?></td>
		<td class="celda td padding-4" data-column="6"><?php if($this->val->fecha($directivo->fecha_nacimiento)){echo $this->lib->format_date($directivo->fecha_nacimiento,'d/m/Y');}?></td>
		<td class="celda td padding-4" data-column="7"><?php echo $this->lib->calcula_edad($directivo->fecha_nacimiento);?></td>
		<td class="hidden-sm"><?php echo $directivo->telefono;?></td>
		<td><?php echo $directivo->email;?></td>
		<td class="hidden-sm">
	<?php if(count($persona_cuentas)>0){?>
			<ul style="padding-left: 20px;">
		<?php for($j=0; $j < count($persona_cuentas); $j++){ $cuenta=json_decode($persona_cuentas[$j]);?>
				<li><?php echo $cuenta->razon;?>-<?php echo $cuenta->cuenta;?></li>
		<?php } ?>
			</ul>
	<?php }?>
		</td>
		<td class="hidden-sm"><?php echo $directivo->descripcion; ?></td>
	</tr>
<?php } ?>
	</tbody>
</table>