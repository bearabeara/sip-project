<?php 
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png"';
	$help2='title="Nº Cédula" data-content="Ingrese un numero cédula de identidad con valores numericos <b>sin espacios</b>, de 6 a 8 digitos"';
	$help3='title="Ciudad" data-content="Seleccione la ciudad donde se difundio la cédula de identidad. Si desea adicionar una nueva ciudad puede hacerlo en la sección de configuración en el menu superior, o puede dar click el el boton <b>+</b>"';
	$help4='title="Primer nombre" data-content="Ingrese un nombre alfanumerico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help5='title="Segundo nombre" data-content="Ingrese un segundo nombre (si tuviera) alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el segundo nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help6='title="Apellido Paterno" data-content="Ingrese un apellido paterno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help7='title="Apellido Materno" data-content="Ingrese un apellido materno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help8='title="Cargo" data-content="Ingrese el cargo o cargos que ocupa como directivo en la empresa. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)</b>, con un <b>máximo de 200 caractereres</b>."';
	$help9='title="Telf./Celular" data-content="Ingrese el número de teléfono o celular de referencia del empleado de 6 a 15 digitos numéricos."';
	$help10='title="Email" data-content="Representa al correo electrónico del empleado si tuviera, el formato del correo electrónico aceptado es <b>example@dominio.com</b>."';
	$help11='title="Fecha de nacimiento" data-content="Representa a la fecha de nacimiento del directivo"';
	$help12='title="Cuentas bancarias" data-content="Ingrese las cuentas bancarias del directivo"';
	$help13='title="Característica" data-content="Representa a las característica propias del empleado si tuviera el contenido debe tener un formato alfanumerico de 0 a 900 caracteres <b>puede incluir espacios y sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑº+-ª.,:;)</b>, con un <b>máximo de 900 caractereres</b>."';
	$help14='title="Imposible eliminar" data-content="La cuenta esta asiganda en depositos de pedidos."';
	$popover1='data-toggle="popover" data-placement="top" data-trigger="hover"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$idci="";
	$ciudad=$this->lib->search_elemento($ciudades,"idci",$directivo->idci);
	if($ciudad!=null){$idci=$ciudad->idci;}
	$c = array(1 => 'Cuenta corriente', 2 => 'Caja de ahorros', 3 => 'Cuenta remunerada');
	$persona_cuentas=$this->lib->select_from($cuentas,'ci',$directivo->ci);
	$datos_cuenta=array();
	for ($i=0; $i < count($persona_cuentas); $i++) { $pc=json_decode($persona_cuentas[$i]);
		$datos_cuenta[] = array('bp' => $pc->idbp,'status'=>'1');
	}
	$datos_cuenta=json_encode($datos_cuenta);
	$rand=rand(10,99999999);
?>
<div class="list-group">
	<span id="datos_cuenta" style="display: none;"><?php echo $datos_cuenta;?></span>
	<div class="list-group-item" style="max-width:100%">
		<div class="hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fotografía:</label></div>
			<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<input class="form-control form-control-xs" id="n_fot" type="file" placeholder='Seleccione fotografia'>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Nº Cédula:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_ci" type="number" data-type="directivo" placeholder='Número de cedula de identidad' min='100000' max='99999999' minlength="6" maxlength="8" value="<?php echo $directivo->ci;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Ciudad:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="n_ciu">
						<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($ciudades); $i++){ $ciudad=$ciudades[$i]; ?>
						<option value="<?php echo $ciudad->idci;?>" <?php if($ciudad->idci==$idci){?>SELECTED='SELECTED'<?php }?>>
							<?php echo $ciudad->ciudad.'('.$ciudad->abreviatura.')';?>
						</option>
				<?php } ?>
					</select>
					<a href="<?php echo base_url();?>capital_humano?p=5" target="_blank" title="Ver configuraciónes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Primer nombre:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_nom1" type="text" placeholder='Primero nombre' maxlength="20" value="<?php echo $directivo->nombre;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</form>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Segundo nombre:</label></div>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_nom2" type="text" placeholder='Segundo nombre' maxlength="20" value="<?php echo $directivo->nombre2;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</form>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Apellido paterno:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_pat" type="text" placeholder='Apellido paterno' maxlength="20" value="<?php echo $directivo->paterno;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Apellido materno:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_mat" type="text" placeholder='Apellido materno' maxlength="20" value="<?php echo $directivo->materno;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Cargo directivo:</label></div>
			<div class="col-sm-10 col-xs-12">
				<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_car" type="text" placeholder='Cargo como directivo' maxlength="200"  value="<?php echo $directivo->cargo_directivo;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help8;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Telf./Celular:</label></div>
			<div class="col-sm-4 col-xs-12">
				<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n_tel" type="number" placeholder='Número de teléfono o celular' min='100000' max="999999999999999" minlength="6" maxlength="15" value="<?php echo $directivo->telefono;?>">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help9;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Email:</label></div>
			<div class="col-sm-4 col-xs-12">
			<form class="update_directivo" data-di="<?php echo $directivo->iddi;?>">
				<div class="input-group">
					<input class="form-control form-control-xs color-class" id="n_ema" type="email" placeholder='example@dominio.com' maxlength="60" value="<?php echo $directivo->email;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help10;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fecha de nacimiento:</label></div>
			<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<input type="date" class="form-control form-control-xs" id="n_fec" placeholder='2000-01-31' value="<?php if($directivo->fecha_nacimiento!="" && $directivo->fecha_nacimiento!=NULL && $directivo->fecha_nacimiento!='0000-00-00'){echo $directivo->fecha_nacimiento;}?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Cuentas bancarias:</label></div>
			<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<div class="form-control form-control-sm textarea" id="n_proc" style="min-height: 50px; height: auto; line-height: 1.75; color: rgba(85, 89, 92, .71);">
				<?php for($i=0;$i<count($persona_cuentas);$i++){ $pc=json_decode($persona_cuentas[$i]);
						$control=$this->lib->search_elemento($pagos,"idbp",$pc->idbp);
				?>
						<span class='label label-md label-inverse-primary' style='font-size: 1rem !important; cursor: default;'<?php if($control==null){?> data-bp='<?php echo $pc->idbp;?>' data-ba='<?php echo $pc->idba;?>' data-cuenta='<?php echo $pc->cuenta;?>' data-tipo='<?php echo $pc->tipo?>' data-type-save='update' id='<?php echo rand(5,99999999);?>' <?php }?>><label class='text-primary'><?php echo $pc->razon." - ".$pc->cuenta;?></label> <span class='closed <?php if($control==null){?>closed<?php echo $rand;?><?php }?>'<?php if($control==null){?> data-padre='span' data-type-padre='tag' data-bp='<?php echo $pc->idbp;?>' data-control="cuentas_directivo" data-container="span#datos_cuenta"<?php }else{ echo $popover1.$help14;}?>>×</span></span>
				<?php }?>
					</div>
					<span class="input-group-addon form-control-sm new_cuenta_banco" data-container="n_proc"><i class='fa fa-plus'></i></span>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help12;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Característica:</label></div>
			<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<textarea class="form-control form-control-xs color-class" id="n_obs" placeholder='Caracteristicas personales' maxlength="800" rows="3"><?php echo $directivo->descripcion;?></textarea>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help13;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
	</div>
</div>
<script>Onfocus("n_ci");$('[data-toggle="popover"]').popover({html:true});$('.color-class').maxlength();$(".new_cuenta_banco").new_cuenta_banco();$("form.update_directivo").submit(function(e){$(this).update_directivo();e.preventDefault();});$(".closed<?php echo $rand;?>").drop_elemento();</script>