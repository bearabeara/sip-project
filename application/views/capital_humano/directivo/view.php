<?php $url=base_url().'libraries/img/';?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class="img-thumbnail-60"><div class="img-thumbnail-60">#Item</div></th>
			<th class='hidden-sm celda-sm-10'>CI</th>
			<th class='celda-sm-25'>Nombre</th>
			<th class='hidden-sm' width="8%">Teléfono</th>
			<th class='hidden-sm' width="20%">Cuentas bancarias</th>
			<th width="12%">Cargo directivo</th>
			<th class='hidden-sm' width="25%">Descripción</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($directivos); $i++){$directivo=$directivos[$i];
		$img="sistema/miniatura/default.jpg";
		if($directivo->fotografia!="" && $directivo->fotografia!=NULL){$img="personas/miniatura/".$directivo->fotografia;}
		$persona_cuentas=$this->lib->select_from($cuentas,'ci',$directivo->ci);
?>
	<tr data-di="<?php echo $directivo->iddi;?>">
		<td><div id="item"><?php echo $i+1;?></div>
			<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $directivo->nombre_completo;?>" data-desc="<?php if($directivo->cargo_directivo!="" && $directivo->cargo_directivo!=NULL){ echo $directivo->cargo_directivo;}else{ echo '</br>';}?>">
		</td>
		<td class="hidden-sm"><?php echo $directivo->ci; ?></td>
		<td><?php echo $directivo->nombre_completo; ?></td>
		<td class="hidden-sm"><?php echo $directivo->telefono; ?></td>
		<td class="hidden-sm">
	<?php if(count($persona_cuentas)>0){?>
			<div class="list-group" style="font-size:10px; padding: 1px">
		<?php for($j=0; $j < count($persona_cuentas); $j++){ $cuenta=json_decode($persona_cuentas[$j]);?>
				<a href="javascript:" class="list-group-item list-group-item-action detail_cuenta" data-sc="8" style="padding: 5px 6px 5px 6px; font-size: .75rem; width:100%;"><span class="hidden-sm"><?php echo $cuenta->razon;?> - <?php echo $cuenta->cuenta;?></span></a>
		<?php } ?>
			</div>
	<?php }?>
		</td>
		<td><?php echo $directivo->cargo_directivo; ?></td>
		<td class="hidden-sm"><?php echo $directivo->descripcion; ?></td>
		<td>
	<?php 
		$det=json_encode(array('function'=>'reportes_directivo','atribs'=> json_encode(array('di' => $directivo->iddi)),'title'=>'Detalle'));
		$conf=""; if($privilegio->ca3u=="1"){ $conf=json_encode(array('function'=>'config_directivo','atribs'=> json_encode(array('di' => $directivo->iddi)),'title'=>'Configurar'));}
		$del=""; if($privilegio->ca3d=="1"){ $del=json_encode(array('function'=>'confirm_directivo','atribs'=> json_encode(array('di' => $directivo->iddi)),'title'=>'Eliminar'));}
		$this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);
	?>
		</td>
	</tr>
<?php } ?>
	</tbody>
</table>
<?php 
	if($privilegio->ca3r=="1" && ($privilegio->ca3p=="1" || $privilegio->ca3c=="1")){
		if($privilegio->ca3p=="1"){$report=json_encode(array('function'=>'print_directivos','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
		if($privilegio->ca3c=="1"){$new=json_encode(array('function'=>'new_directivo','title'=>'Nuevo'));}
		if($privilegio->ca3p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
	}
?>
<script>$("img.img-thumbnail-60").visor();if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}$(".reportes_directivo").reportes_directivo();$(".config_directivo").config_directivo();$(".confirm_directivo").confirm_directivo();$("button.manual").manual();</script>