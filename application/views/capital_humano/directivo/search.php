<?php 
?>
<table class="tabla tabla-border-false" id="tbl-container">
	<tr class="fila">
		<th class="hidden-sm img-thumbnail-60"><div class="img-thumbnail-60"></div></th>
		<td class='celda-sm-10 hidden-sm'>
			<form  class="view_directivos" data-type="search">
				<input type="number" class="form-control form-control-sm search_capital_humano" id="s_ci" maxlength="8" placeholder='CI' min='0'>
			</form>
		</td>

		<td class='celda-sm-25'>
			<form class="view_directivos" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_capital_humano" maxlength="100" placeholder='Buscar nombre...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_pedido" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' width="65%"></td>
		<td class='text-right hidden-sm'>
	<?php 
		$search=json_encode(array('function'=>'view_directivos','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
		$all=json_encode(array('function'=>'view_directivos','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
	?>
	<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>$(".search_capital_humano").search_capital_humano();$(".view_directivos").view_directivos();</script>