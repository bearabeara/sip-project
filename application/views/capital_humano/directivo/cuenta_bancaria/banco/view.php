<?php $url=base_url().'libraries/img/';$rand=rand(10,999999);?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive g-table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th class="img-thumbnail-40"><div class="img-thumbnail-40">#Item</div></th>
						<th width="75%">Razón social</th>
						<th width="25%">Dirección web</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
			<?php if(!empty($bancos)){
					for($i=0;$i<count($bancos);$i++){ $banco=$bancos[$i];
						$img="sistema/miniatura/default.jpg";
						if($banco->fotografia!="" && $banco->fotografia!=NULL){ $img="bancos/miniatura/".$banco->fotografia;}
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-40<?php echo $rand;?>" data-title="<?php echo $banco->razon;?>" data-desc="<br>"></td>
						<td><?php echo $banco->razon;?></td>
						<td><a href="<?php echo $banco->url;?>" target="__blank"><?php echo $banco->url;?></a></td>
						<td>
						<?php 
							$det="";
							$conf="";$conf=json_encode(array('function'=>'config_banco','atribs'=> json_encode(array('ba' => $banco->idba,'container'=>$container)),'title'=>'Configurar'));
							$del="";$del=json_encode(array('function'=>'confirm_banco','atribs'=> json_encode(array('ba' => $banco->idba,'container'=>$container)),'title'=>'Eliminar'));
							$this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);
						?>
						</td>
					</tr>
			<?php
					}// end for
			?>
			<?php }else{ ?>
					<tr><td colspan="5"><h3>0 registros encontrados...</h3></td></tr>
			<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row text-right" style="padding-right:15px;">
          	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_banco" data-type="new_modal" data-container="<?php echo $container;?>">
          		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar banco</span>
            </button>
		</div>
	</div>
</div>
<script>$(".img-thumbnail-40<?php echo $rand;?>").visor();$(".new_banco").new_banco();$(".config_banco").config_banco();$(".confirm_banco").confirm_banco();</script>