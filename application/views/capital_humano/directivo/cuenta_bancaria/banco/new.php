<?php 
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png"';
	$help2='title="Nombre de entidad" data-content="Ingrese un nombre de la entidad financiera en formato alfanumerico de 4 a 200 caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="Sitio Web" data-content="Ingrese una url de la pagina de la entidad financiera con el formato http://wwww.ejemplo.com.bo, se acepta una url con una maximo de 300 caracteres."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="form-group">
			<div class="col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Fotografía:</label></div>
			<div class="col-xs-12">
				<div class="input-group">
					<input type="file" class="form-control form-control-xs color-class" id="b4_fot" placeholder='Número de cuenta bancaria'>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label"><span class='text-danger'>(*)</span> Nombre de entidad:</label></div>
			<div class="col-xs-12">
				<form class="save_banco" data-container="<?php echo $container;?>" data-type="<?php echo $type;?>">
					<div class="input-group">
						<input type="text" class="form-control form-control-xs color-class" id="b4_nom" placeholder='Banca.SRL' minlength="4" maxlength="200">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div><i class="clearfix"></i>
		<div class="form-group">
			<div class="col-xs-12"><label for="example-text-input" class="col-xs-12 form-control-label">Dirección web:</label></div>
			<div class="col-xs-12">
				<div class="input-group">
					<input type="text" class="form-control form-control-xs color-class" id="b4_url" placeholder='http://banco.com.bo' maxlength="300">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div><i class="clearfix"></i>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("form.save_banco").submit(function(e){$(this).save_banco();e.preventDefault();});</script>