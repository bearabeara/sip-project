<?php 
	$url=base_url().'libraries/img/personas/miniatura/';
	$img='default.png';
	if($directivo->fotografia!="" && $directivo!=NULL){$img=$directivo->fotografia;}
	$ci="";
	$ciudad=$this->lib->search_elemento($ciudades,"idci",$directivo->idci);
	if($ciudad!=null){
		$ci=$ciudad->ciudad." ".$ciudad->abreviatura;
	}
	$persona_cuentas=$this->lib->select_from($cuentas,'ci',$directivo->ci);
	$c = array(1 => 'Cuenta corriente', 2 => 'Caja de ahorros', 3 => 'Cuenta remunerada');
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<?php $fun = array('function' => 'reportes_directivo', 'atribs' => array('di' => $directivo->iddi));
			$default = array('top' => '1','right' => '1','bottom' => '1','left' => '1.5');
			$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'default' => json_encode($default)]);?>
	</div>
	<div class="list-group-item" style="max-width:100%">
<div class="detalle table-responsive" id="area">
	<table class="tabla tbl-bordered font-10">
		<thead>
			<tr class="fila title" style="text-align: center;">
				<td class="celda title padding-4" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
					<?php $this->load->view('estructura/print/header-print',['titulo'=>'DIRECTIVO DE LA EMPRESA']);?>
				</td>
			</tr>
		</thead>
		<tr class='fila'>
			<td class="celda td padding-4 text-center" width="20%"><img src="<?php echo $url.$img; ?>" width='80%' class="img-thumbnail"></td>
			<td class="celda td padding-4" style="text-align: center;" width="80%"><span style="font-size: 1.9rem;"><?php echo $directivo->nombre_completo;?></span><p style="font-size: 1.3rem;"><?php echo $directivo->cargo_directivo; ?></p></td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos personales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Cédula de Identidad:</th>
						<td width="60%"><?php echo $ci;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Pais de origen:</th>
						<td width="60%"><?php echo $ciudad->pais;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Fecha de Nacimiento:</th>
						<td width="60%"><?php echo $this->lib->format_date($directivo->fecha_nacimiento,'dl ml Y')?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Teléfono:</th>
						<td width="60%"><?php if($directivo->telefono!=0 && $directivo->telefono!=''){ echo $directivo->telefono;}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Correo Electrónico:</th>
						<td width="60%"><?php if($directivo->email!=null && $directivo->email!=''){ echo $directivo->email;}?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left;">Edad:</th>
						<td width="60%"><?php echo $this->lib->calcula_edad($directivo->fecha_nacimiento);?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos laborales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Cargo como directivo:</th>
						<td width="60%"><?php echo $directivo->cargo_directivo;?></td>
					</tr>
					<tr>
						<th width="40%" style="text-align: left; vertical-align: top;">cuentas bancarias:</th>
						<td width="60%">
	<?php if(count($persona_cuentas)>0){?>
						<table border="0" width="100%">
		<?php for($j=0; $j < count($persona_cuentas); $j++){ $cuenta=json_decode($persona_cuentas[$j]);?>
							<tr>
								<td><?php echo $cuenta->razon;?></td>
								<td><?php echo $cuenta->cuenta;?></td>
								<td><?php echo $c[$cuenta->tipo]; ?></td>
							</tr>
		<?php } ?>
						</table>
	<?php }?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="fila">
			<td class="celda td padding-4" style="font-size: 1.2rem; text-align: right;"><strong>Datos generales</strong></td>
			<td class="celda td padding-4">
				<table border="0" width="100%">
					<tr>
						<th width="40%" style="text-align: left;">Caracteristicas Personales:</th>
						<td width="60%"><?php echo $directivo->descripcion;?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</div>
</div>
<script>$("a.detalle_empleado<?php echo $rand;?>").detalle_empleado();$("a.produccion_empleado").produccion_empleado()</script>