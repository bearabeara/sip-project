<?php  $j_movimientos=json_encode($movimientos); ?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
	    <tr>
	    	<th width='3%'>#</th>
			<th width='12%;'>Nombre de Material</th>
			<th width='13%' class="hidden-sm">Almacen</th>
			<th width='12%' class="hidden-sm">Usuario</th>
			<th width='12%'>Solicitante</th>
			<th width='12%'>fecha</th>  
			<th width='5%;' class="hidden-sm">Stock</th>
			<th width='5%;' class="hidden-sm">Ingreso</th>
			<th width='5%;' class="hidden-sm">Salida</th>
			<th width='5%;' class="hidden-sm">Saldo</th>
			<th width='16%;' class="hidden-sm">Observaciones</th>
	    </tr>
    </thead>

<?php
if(count($movimientos)<=0){ 
	echo "<tr><td colspan='11'><h3>0 registros encontrados...</h3></td></tr>";
}else{?>
    <tbody>
<?php for ($i=0; $i < count($movimientos) ; $i++){ $movimiento=$movimientos[$i]; ?>
    	<tr data-amv="<?php echo $movimiento->idamv;?>">
    		<td><?php echo $i+1;?></td>
			<td><?php echo $movimiento->material;?></td>
			<td class="hidden-sm"><?php echo $movimiento->almacen;?></td>
			<td class="hidden-sm"><?php echo $movimiento->usuario;?></td>
			<td><?php echo $movimiento->solicitante;?></td>
			<td><?php echo $this->lib->format_date($movimiento->fecha,'Y,m,d')." ".$movimiento->hora;?></td> 
			<td class="hidden-sm"><?php echo $movimiento->cantidad;?></td>
			<td class="hidden-sm"><?php echo $movimiento->ingreso;?></td>
			<td class="hidden-sm"><?php echo $movimiento->salida;?></td>
			<td class="hidden-sm"><?php echo ($movimiento->cantidad+$movimiento->ingreso-$movimiento->salida);?></td>
			<td class="hidden-sm"><?php echo $movimiento->observacion;?></td>
    	</tr>
    	<?php
    	}//end else
    }//end ifelse
    ?>
    </tbody>
</table>
<?php $report="";$delete="";$help="";
	if($privilegio->al2r=="1" && ($privilegio->al2p=="1" || $privilegio->al2d=="1")){
		if($privilegio->al2d=="1"){$delete=json_encode(array('function'=>'confirmar_movimiento_material','title'=>'Eliminar todo el historial'));}
		if($privilegio->al2p=="1"){$report=json_encode(array('function'=>'print_movimiento_material','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
		if($privilegio->al2p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_delete"=>$delete,"fab_help"=>$help]);
	}
?>
<script>if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}</script>