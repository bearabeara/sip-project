<?php $url=base_url().'libraries/img/';
	$fecha=date("Y-m-d");
	header("Content-type: application/vnd.ms-word; name='word'");
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: pre-check=0, post-check=0, max-age=0');
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	header('Content-type: application/vnd.ms-word;charset=utf-8');
	header('Content-type: application/x-msexcel; charset=utf-8');
	header("Content-Disposition: attachment; filename=Movimiento-de-materiales-$fecha.doc");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	if(!empty($movimientos)){
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<center><h3>REPORTE DE ALMACENES</h3></center>
<table class="font-10" border="1" cellspacing="0" cellpadding="5" width="100%">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th width="6%">Nombre de material</th>
			<th width="10%">Almacen</th>
			<th width="28.5%">Usuario</th>
			<th width="17.5%">Solicitante</th>
			<th width="15%">Fecha</th>
			<th width="33%">Hora</th>
			<th width="10%">Stock</th>
			<th width="10%">Ingreso</th>
			<th width="10%">Salida</th>
			<th width="10%">Saldo</th>
			<th width="33%">Observaciónes</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($movimientos); $i++){ $movimiento=$movimientos[$i]; ?>
	<tr>
		<td><?php echo $i+1;?></td>
		<td><?php echo $movimiento->material;?></td>
		<td><?php echo $movimiento->almacen;?></td>
		<td><?php echo $movimiento->usuario;?></td>
		<td><?php echo $movimiento->solicitante;?></td>
		<td><?php echo $movimiento->fecha;?></td>
		<td><?php echo $movimiento->hora;?></td>
		<td><?php echo $movimiento->cantidad;?></td>
		<td><?php echo $movimiento->ingreso;?></td>
		<td><?php echo $movimiento->salida;?></td>
		<td><?php echo ($movimiento->cantidad+$movimiento->ingreso-$movimiento->salida);?></td>
		<td><?php echo $movimiento->observacion;?></td>
	</tr>
<?php } ?>
	</tbody>
</table>
<?php }?>