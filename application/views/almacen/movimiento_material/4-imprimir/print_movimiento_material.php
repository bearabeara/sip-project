<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_movimiento_material', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'almacen/excel_material?nom='.$nom.'&alm='.$alm."&usu=".$usu."&sol=".$sol."&fech_a=".$fech_a."&fech_b=".$fech_b."&tip=".$tip);
		$word = array('controller' => 'almacen/word_material?nom='.$nom.'&alm='.$alm."&usu=".$usu."&sol=".$sol."&fech_a=".$fech_a."&fech_b=".$fech_b."&tip=".$tip);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre material</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 16%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Almacen</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 15%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Usuario</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 15%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Solicitante</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 5%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Hora</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 8%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fecha</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Stock</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Ingreso</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Salida</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="11" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Saldo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 12%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="12" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive" id="area">
			<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE MOVIMIENTO DE MATERIALES']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="5%">#</th>
								<th class="celda th padding-4" data-column="2" width="30%">Nombre de material</th>
								<th class="celda th padding-4" data-column="3" width="20%">Almacen</th>
								<th class="celda th padding-4" data-column="4" width="15%">Usuario</th>
								<th class="celda th padding-4" data-column="5" width="15%">Solicitante</th>
								<th class="celda th padding-4" data-column="6" width="5%">Hora</th>
								<th class="celda th padding-4" data-column="7" width="8%">Fecha</th>
								<th class="celda th padding-4" data-column="8" width="4%">Stock</th>
								<th class="celda th padding-4" data-column="9" width="4%">Ingreso</th>
								<th class="celda th padding-4" data-column="10" width="4%">Salida</th>
								<th class="celda th padding-4" data-column="11" width="4%">Saldo</th>
								<th class="celda th padding-4" data-column="12" width="22%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){
									$movimiento=$this->lib->search_elemento($movimientos,"idamv",$visible);
									if($movimiento!=null){
										$cont++;
							?>
								<tr class="fila">
									<td class="celda td padding-4" data-column="1"><?php echo $cont;?></td>
									<td class="celda td padding-4" data-column="2"><?php echo $movimiento->material;?></td>
									<td class="celda td padding-4" data-column="3"><?php echo $movimiento->almacen;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $movimiento->usuario;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $movimiento->solicitante;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $movimiento->hora;?></td>
									<td class="celda td padding-4" data-column="7"><?php echo $movimiento->fecha;?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $movimiento->cantidad;?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $movimiento->ingreso;?></td>
									<td class="celda td padding-4" data-column="10"><?php echo $movimiento->salida;?></td>
									<td class="celda td padding-4" data-column="11"><?php echo ($movimiento->cantidad+$movimiento->ingreso-$movimiento->salida);?></td>
									<td class="celda td padding-4" data-column="12"><?php echo $movimiento->observacion;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
			</table>
		</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();$("input[type='checkbox']").change_column_print();</script>