<table cellpadding="0" cellspacing="0">
	<tr class="fila">
		<td width="2.5%" class='hidden-sm'></td>
		<td width="11.8%" class="search-principal">
			<form class="view_movimiento_material">
				<div class="input-group input-group-sm">
					<input class='form-control form-control-sm search_movimiento' type="search" placeholder='Nombre de Material' id="s_nom">
					<span class="input-group-addon form-control-sm view_movimiento_material" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td width="12.8%" class='hidden-sm'>
			<form class="view_movimiento_material">
				<input class='form-control form-control-sm search_movimiento' type="search" placeholder='Almacen' id="s_alm">
			</form></td>
		<td width="11.8%" class='hidden-sm'>
			<form class="view_movimiento_material">
				<input class='form-control form-control-sm search_movimiento' type="search" placeholder='Usuario' id="s_usu">
			</form></td>
		<td width="11.8%" class='hidden-sm'>
			<form class="view_movimiento_material">
				<input class='form-control form-control-sm search_movimiento' type="search" placeholder='Solicitante' id="s_sol">
			</form></td>
		<td class='hidden-sm' width="12.4%"><input class='form-control form-control-sm' type="date" id="fech_a"></td>
		<td class='hidden-sm' width="12.4%"><input class='form-control form-control-sm' type="date" id="fech_b"></td>
		<td class='hidden-sm' width="9%">
			<select id="s_tip" class="form-control form-control-sm search_movimiento">
				<option value="">Seleccionar...</option>
				<option value="1">Ingresos</option>
				<option value="2">Salidas</option>
			</select>			
		</td>
		<td class='hidden-sm' width="6.5%"></td>
		<td class="hidden-sm text-right" width="9%">
		<?php 
			$search=json_encode(array('function'=>'view_movimiento_material','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
			$all=json_encode(array('function'=>'view_movimiento_material','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus('s_nom');
$(".search_movimiento").search_movimiento_material();$(".view_movimiento_material").view_movimiento_material();
</script>