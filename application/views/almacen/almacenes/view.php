<?php $url=base_url().'libraries/img/almacenes/';
	switch($atrib){
		case 'codigo': $col=1;break;
		case 'nombre': $col=2;break;
		case 'tipo': $col=3;break;
		default: $col=-1;break;
	}
	
?>
  <table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class="img-thumbnail-60"><div class="img-thumbnail-60">#Item</div></th>
			<th class="celda-sm-10">Código</th>
			<th style="width:33%">Nombre</th>
			<th class="hidden-sm" style="width:15%">Tipo</th>
			<th class="hidden-sm" style="width:12%">Cantidad</th>
			<th class="hidden-sm" style="width:30%">Descripción</th>
			<th width="0px;"></th>
		</tr>
	</thead>
<?php if(count($almacenes)>0){?>
	<tbody>
<?php $cont=0;
	for($i=0;$i<count($almacenes);$i++){ $almacen=$almacenes[$i];
	 	$control="true";
	 	if($atrib!="" && $val!="" && $type_search!=""){
	 		$control=$this->lib->search_where($almacen,$atrib,$val,$type_search);
	 	}
		$img="default.png";
		if($almacen->fotografia!=NULL && $almacen->fotografia!=""){$img=$almacen->fotografia;} 
		if($almacen->tipo=="0"){$aux=$this->M_almacen_material->get_row('ida',$almacen->ida);}
?>
		<tr<?php if($atrib!="" && $val!="" && $type_search!=""){if($control==null){?> style="display: none"<?php }else{$cont++;}}else{$cont++;}?> data-a="<?php echo $almacen->ida;?>">
			<td data-col='1'>
				<div class="item"><?php echo $cont;?></div>
				<img src="<?php echo $url.'miniatura/'.$img;?>" width="100%" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $almacen->nombre;?>" data-desc="<?php if($almacen->descripcion!=""){echo $almacen->descripcion;}else{ echo '<br>';}?>">
			</td>
			<td data-col='2'>
				<?php if($atrib=="codigo" && $val!="" && $control!=null){echo $this->lib->str_replace_all($almacen->codigo,$this->lib->all_minuscula($val),"mark");}else{echo $almacen->codigo;}?>
			</td>
			<?php if($almacen->tipo=="0"){ $tipo="material";$text="material(es)";}if($almacen->tipo=="1"){ $tipo="producto";$text="producto(s)";}?>
			<td>
				<a href="<?php echo base_url().'almacen/'.$tipo.'/'.$almacen->ida?>" data-col='3'><?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($almacen->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $almacen->nombre;}?></a>
				<br><span class="label label-inverse-<?php if(count($aux)>0){ echo 'success';}else{ echo 'danger';}?> visible-sm" style="font-size: .58rem;"><?php echo count($aux)." $text registrado(s)"; ?></span></td>
			</td>
			<td class="hidden-sm" data-col='4' data-value="<?php echo $almacen->tipo;?>">
			<?php if($atrib=="tipo"){?><mark><?php }?>
				<?php echo "Almacen de ".$text;?></td>
			<?php if($atrib=="tipo"){?></mark><?php }?>
			<td class="hidden-sm">
			<span class="label label-inverse-<?php if(count($aux)>0){ echo 'success';}else{ echo 'danger';}?>"><?php echo count($aux)." $text registrado(s)"; ?></span></td>
			<td class="hidden-sm"><?php echo $almacen->descripcion;?></td>
			<td class="text-right">
				<?php 
					$det=json_encode(array('function'=>'reportes_almacen','atribs'=> json_encode(array('a' => $almacen->ida)),'title'=>'Detalle'));
					$conf="";if($privilegio->al1u=="1"){ $conf=json_encode(array('function'=>'config_almacen','atribs'=> json_encode(array('a' => $almacen->ida)),'title'=>'Configurar'));}
					$del="";if($privilegio->al1d=="1"){ $del=json_encode(array('function'=>'confirm_almacen','atribs'=> json_encode(array('a' => $almacen->ida)),'title'=>'Eliminar'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
			</td>
		</tr>	
<?php }//end for?>
	</tbody>
<?php }else{ echo "<tr><td colspan='7'><p><br><h2>0 registros encontrados...</h2><br></p></td></tr>"; } ?>
</table>
<?php $report="";$new="";$help="";
	if($privilegio->al1r=="1" && ($privilegio->al1p=="1" || $privilegio->al1c=="1" || $privilegio->al1a=="1")){
		if($privilegio->al1p=="1"){$report=json_encode(array('function'=>'print_almacen','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
		if($privilegio->al1c=="1"){$new=json_encode(array('function'=>'new_almacen','title'=>'Nuevo almacen'));}
		if($privilegio->al1a=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
	}
?>
<script>$("button.reportes_almacen").reporte_almacen();$("button.config_almacen").config_almacen();$("button.confirm_almacen").confirm_almacen();$("img.img-thumbnail").visor();if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}</script>