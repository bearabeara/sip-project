<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_almacen', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'almacen/excel_almacen?cod='.$cod.' & nom='.$nom." & tip=".$tip);
		$word = array('controller' => 'almacen/word_almacen?cod='.$cod.' & nom='.$nom." & tip=".$tip);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 4%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Código</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 30%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre de almacen</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Tipo de almacen</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 35%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Descripción</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE ALMACENES']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="5%" style="display: none;">#Item</th>
								<th class="celda th padding-4" data-column="2" width="6%">Fotografía</th>
								<th class="celda th padding-4" data-column="3" width="10%">Código</th>
								<th class="celda th padding-4" data-column="4" width="28.5%">Nombre de almacen</th>
								<th class="celda th padding-4" data-column="5" width="17.5%" >Tipo de almacen</th>
								<th class="celda th padding-4" data-column="6" width="33%">Descripción</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){ 
									$almacen=$this->lib->search_elemento($almacenes,"ida",$visible);
									if($almacen!=null){
										$cont++;
										$img='sistema/miniatura/default.jpg';
										if($almacen->fotografia!=NULL && $almacen->fotografia!=""){ $img="almacenes/miniatura/".$almacen->fotografia;}
										$tipo="";
										if($almacen->tipo=="0"){ $tipo="materiales";$text="material(es)";}if($almacen->tipo=="1"){ $tipo="productos";$text="producto(s)";}
							?>
								<tr class="fila">
									<td class="celda td padding-4"  data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $almacen->codigo;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $almacen->nombre;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo "Almacen de ".$tipo;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $almacen->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>