<?php $url=base_url().'libraries/img/';
$fecha=date("Y-m-d");
header("Content-type: application/vnd.ms-word; name='word'");
header('Pragma: public');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0');
header('Pragma: no-cache');
header('Expires: 0');
header('Content-Transfer-Encoding: none');
header('Content-type: application/vnd.ms-word;charset=utf-8');
header('Content-type: application/x-msexcel; charset=utf-8');
header("Content-Disposition: attachment; filename=Almacenes-$fecha.doc");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
	if(!empty($almacenes)){
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<center><h3>REPORTE DE ALMACENES</h3></center>
<table class="font-10" border="1" cellspacing="0" cellpadding="5" width="100%">
	<thead>
		<tr>
			<th width="5%">#Item</th>
			<th></th>
			<th width="10%">Código</th>
			<th width="28.5%">Nombre de almacen</th>
			<th width="17.5%" >Tipo de almacen</th>
			<th width="42%">Descripción</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($almacenes); $i++){ $almacen=$almacenes[$i];
		$tipo="";
		if($almacen->tipo=="0"){ $tipo="materiales";$text="material(es)";}if($almacen->tipo=="1"){ $tipo="productos";$text="producto(s)";}
		$img='sistema/miniatura/default.jpg';
		if($almacen->fotografia!=NULL && $almacen->fotografia!=""){ $img="almacenes/miniatura/".$almacen->fotografia;}
?>
	<tr>
		<td><?php echo $i+1;?></td>
		<td><img width="40px" src="<?php echo $url.$img;?>"></td>
		<td><?php echo $almacen->codigo;?></td>
		<td><?php echo $almacen->nombre;?></td>
		<td><?php echo "Almacen ".$tipo;?></td>
		<td><?php echo $almacen->descripcion;?></td>
	</tr>
<?php } ?>
	</tbody>
</table>
<?php }?>

