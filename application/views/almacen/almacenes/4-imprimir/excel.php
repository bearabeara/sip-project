<?php $url=base_url().'libraries/img/';
	$fecha=date("Y-m-d");
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
	header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
	header("Content-Disposition: attachment; filename=Almacenes-$fecha.xls");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	if(!empty($almacenes)){
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<center><h3>REPORTE DE ALMACENES</h3></center>
<table class="font-10" border="1" cellspacing="0" cellpadding="5" width="100%">
	<thead>
		<tr>
			<th style="width:5%">#Item</th>
			<th style="width:10%">Código</th>
			<th style="width:28.5%">Nombre de almacen</th>
			<th style="width:17.5%" >Tipo de almacen</th>
			<th style="width:42%">Descripción</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($almacenes); $i++){ $almacen=$almacenes[$i];
		$tipo="";
		if($almacen->tipo=="0"){ $tipo="materiales";$text="material(es)";}if($almacen->tipo=="1"){ $tipo="productos";$text="producto(s)";}
?>
	<tr>
		<td style="width: 5%;"><?php echo $i+1;?></td>
		<td style="width: 10%;"><?php echo $almacen->codigo;?></td>
		<td style="width: 28.5%;"><?php echo $almacen->nombre;?></td>
		<td style="width: 17.5%;"><?php echo "Almacen ".$tipo;?></td>
		<td style="width: 42%;"><?php echo $almacen->descripcion;?></td>
	</tr>
<?php } ?>
	</tbody>
</table>
<?php }?>

