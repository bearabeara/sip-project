<?php 
//print_r($movimientos);
	$movimientos=json_decode($movimientos);
	$contPagina=1;
	$contReg=0;

?>
<div class="pagina">
	<?php $this->load->view('estructura/print/encabezado',['titulo'=>'MOVIMIENTO DE INGRESO Y SALIDA','pagina'=>$contPagina,'sub_titulo'=> 'ALMACENES DE PRODUCTO']);?>
	<div class="tabla tabla-border-true">
		<div class="fila">
				<?php if(!isset($v1)){?><div class="celda th" style="width:5% ">#Item</div><?php } ?>
				<?php if(!isset($v2)){?><div class="celda th" style="width:7% ">Cód. Producto</div><?php } ?>
				<?php if(!isset($v3)){?><div class="celda th" style="width:19%">Nombre de producto</div><?php } ?>
				<?php if(!isset($v4)){?><div class="celda th" style="width:17%">Almacen</div><?php } ?>
				<?php if(!isset($v5)){?><div class="celda th" style="width:13%">Usuario</div><?php } ?>
				<?php if(!isset($v6)){?><div class="celda th" style="width:7% ">Hora</div><?php } ?>
				<?php if(!isset($v7)){?><div class="celda th" style="width:8% ">Fecha</div><?php } ?>
				<?php if(!isset($v8)){?><div class="celda th" style="width:6% ">Stock</div><?php } ?>
				<?php if(!isset($v9)){?><div class="celda th" style="width:6% ">Ingreso</div><?php } ?>
				<?php if(!isset($v10)){?><div class="celda th" style="width:6% ">Salida</div><?php } ?>
				<?php if(!isset($v11)){?><div class="celda th" style="width:6% ">Saldo</div><?php } ?>
		</div>
	<?php $cont=1;  foreach ($movimientos as $key => $movimiento) { 
			if($contReg>=$nro){
			$contReg=0;
			$contPagina++;
		?>
			</div><!--cerramos tabla-->
		</div><!--cerramos pagina-->
	<div class="pagina">
	<div class="tabla tabla-border-true">
		<div class="fila">
				<?php if(!isset($v1)){?><div class="celda th" style="width:5%">#Item</div><?php } ?>
				<?php if(!isset($v2)){?><div class="celda th" style="width:7%">Cód. Producto</div><?php } ?>
				<?php if(!isset($v3)){?><div class="celda th" style="width:19%">Nombre Producto</div><?php } ?>
				<?php if(!isset($v4)){?><div class="celda th" style="width:17%">Almacen</div><?php } ?>
				<?php if(!isset($v5)){?><div class="celda th" style="width:13%">Usuario</div><?php } ?>
				<?php if(!isset($v6)){?><div class="celda th" style="width:7%">Hora</div><?php } ?>
				<?php if(!isset($v7)){?><div class="celda th" style="width:8%">Fecha</div><?php } ?>
				<?php if(!isset($v8)){?><div class="celda th" style="width:6%">Stock</div><?php } ?>
				<?php if(!isset($v9)){?><div class="celda th" style="width:6%">Ingreso</div><?php } ?>
				<?php if(!isset($v10)){?><div class="celda th" style="width:6%">Salida</div><?php } ?>
				<?php if(!isset($v11)){?><div class="celda th" style="width:6%">Saldo</div><?php } ?>
		</div>
		<?php } ?>
		<div class="fila">
			<?php if(!isset($v1)){?><div class="celda td"><?php echo $cont++;?></div><?php } ?>
			<?php if(!isset($v2)){?><div class="celda td"><?php echo $movimiento->cod;?></div><?php } ?>
			<?php if(!isset($v3)){?><div class="celda td"><?php echo $movimiento->nombre;?></div><?php } ?>
			<?php if(!isset($v4)){?><div class="celda td"><?php echo $movimiento->almacen;?></div><?php } ?>
			<?php if(!isset($v5)){?><div class="celda td"><?php echo $movimiento->usuario;?></div><?php } ?>
			<?php if(!isset($v6)){?><div class="celda td"><?php echo $movimiento->hora;?></div><?php } ?>
			<?php if(!isset($v7)){?><div class="celda td"><?php echo $movimiento->fecha;?></div><?php } ?>
			<?php if(!isset($v8)){?><div class="celda td"><?php echo $movimiento->cantidad;?></div><?php } ?>
			<?php if(!isset($v9)){?><div class="celda td"><?php echo $movimiento->ingreso;?></div><?php } ?>
			<?php if(!isset($v10)){?><div class="celda td"><?php echo $movimiento->salida;?></div><?php } ?>
			<?php if(!isset($v11)){?><div class="celda td"><?php echo $movimiento->cantidad+$movimiento->ingreso-$movimiento->salida;?></div><?php } ?>
		</div>
	<?php  $contReg++; } ?>
	</div>
</div>
<script type="text/javascript"> $(".nroPagina").html(<?php echo $contPagina;?>); </script>