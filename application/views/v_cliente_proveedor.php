<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Clientes','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
			if($privilegio[0]->cl1r==1){ $v_menu.="Clientes/cliente/icon-profile|"; }
			if($privilegio[0]->cl2r==1){ $v_menu.="Proveedores/proveedor/icon-addressbook|";}
			if($privilegio[0]->cl5r==1){ $v_menu.="Configuración/config/fa fa-cogs";}
		?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'cliente_proveedor','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>			
		<div id="search"></div>
		<div id="contenido"></div>
		<?php $this->load->view('estructura/js',['js'=>$dir.'cliente_proveedor'.$min.'.js']);?>
	</body>
<?php 
	$title="";$activo="";$search="";$view="";
	switch($pestania){
		case '1': if($privilegio[0]->cl1r==1){ $title="Clientes"; $activo='cliente'; $search="cliente_proveedor/search_cliente"; $view="cliente_proveedor/view_cliente";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
		case '2': if($privilegio[0]->cl2r==1){ $title="Proveedores"; $activo='proveedor'; $search="cliente_proveedor/search_proveedor"; $view="cliente_proveedor/view_proveedor";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		case '5': if($privilegio[0]->cl5r==1){ $title="Configuración"; $activo='config'; $search="cliente_proveedor/search_config"; $view="cliente_proveedor/view_config";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		default: $title="404"; $view=base_url()."login/error";
	} ?>
	<script>$(this).get_2n('<?php echo $search;?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','cliente_proveedor?p=<?php echo $pestania;?>');</script>
</html>