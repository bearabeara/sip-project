<?php $url=base_url().'libraries/img/';
$id_rand=rand(10,99999999);
?>
<ul class="nav nav-tabs md-tabs tabs-left b-none" role="tablist">
	<?php $sw=0;
	foreach($grupos as $key => $grupo){
		$idtbc=$producto->idp.'-'.$grupo->idpgr.'-'.$grupo->idpgrc;
		$c_gr=""; $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";$c_gr=$grupo->abr_g." - ";}
		$pgc=$grupo->idpgrc."-".rand(1,9999);
		$img="sistema/miniatura/default.jpg";
		if($grupo->portada!="" && $grupo->portada!=null){
			$v=explode("|", $grupo->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			}
		}else{
			if($producto->portada!="" && $producto->portada!=null){
				$v=explode("|", $producto->portada);
				if(count($v==2)){
					if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
					if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				}
			}
		}
		$porcentaje=$grupo->porcentaje*100;
		?>
		<li class="nav-item">
			<a class="nav-link <?php if($sw==0){?>active<?php $sw=1;}?>" data-toggle="tab" href="#color<?php echo $grupo->idpgrc;?>" role="tab" style="padding-top: 0px !important;">
				<table style="margin-left: 5px;" width="250px;">
					<tr>
						<td>
							<div class="img-thumbnail-45"><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail img-thumbnail-45"></div>
						</td>
						<td width="100%" style="padding-left: 6px;">
							<div class="progress" class="color-progress-<?php echo $idtbc;?>" style="height: 15px;font-size: 10px; width: 100%">
								<div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated<?php }?> bg-info" role="progressbar" style="width: <?php echo $porcentaje;?>%"><strong><?php if($porcentaje>15){ echo $porcentaje."%";} ?></strong>
								</div>
								<?php if($porcentaje<=15){?><strong style="margin-left: 5px;"><?php echo $porcentaje."%";?></strong><?php } ?>
								</div>
								<?php echo $gr.$grupo->nombre_c;?>
							</td>
						</tr>
					</table>		
				</a>
				<div class="slide" style="height: 45px;"></div>
			</li>
			<?php 
				}// end foreach
				?>
			</ul>
			<div class="tab-content tabs-left-content" style="padding-left: 10px; width: 100%;">
				<?php $sw=0;
				foreach($grupos as $key => $grupo){
					$idtbc=$producto->idp.'-'.$grupo->idpgr.'-'.$grupo->idpgrc;
					?>
					<div class="tab-pane <?php if($sw==0){?>active<?php $sw=1;}?>" id="color<?php echo $grupo->idpgrc;?>" role="tabpanel">
						<div class="panels-wells well well-sm">
							<span class="g-control-accordion">
								<button class="refresh_producto_color_pedido<?php echo $id_rand;?>" data-dp="<?php echo $grupo->iddp;?>" data-p="<?php echo $producto->idp;?>" data-pp="<?php echo $parte_pedido->idpp;?>" data-pgc="<?php echo $grupo->idpgrc;?>" data-tbl-color="<?php echo $idtbc;?>" data-tbl-producto="<?php echo $idtbl;?>"><i class="icon-refresh"></i></button>
							</span><br>
							<div id="content-colors-sucursal-<?php echo $idtbc;?>">
								<?php /*procesos del color*/
								$procesos=$this->lib->producto_grupo_color_procesos($grupo,$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
								$procesos=json_decode(json_encode($procesos));
								/*sucursales*/
								$j_sucursales=json_decode(json_encode($grupo->sucursales));
								for($i=0; $i < count($sucursales); $i++){ $sucursal=$sucursales[$i];
									$cantidad_pedida=0;
									$idsdp=null;
									foreach($j_sucursales as $key => $suc){
										if($suc->idsc==$sucursal->idsc){ $cantidad_pedida=$suc->cantidad; $idsdp=$suc->idsdp; break;}
									}
									if($cantidad_pedida>0 && $idsdp!=null){
										/*productos asignados*/
										$cantidad_produccion=0;
										$cantidad_terminada=0;
										$asignados=$this->lib->select_from($productos_pedido_empleado,"idsdp",$idsdp);
										if(count($asignados)>0){
											$cantidad_terminada=99999999999;
											foreach($procesos as $key => $proceso){
												$cantidad_produccion_proceso=0;
												$cantidad_terminada_proceso=0;
												for($y=0; $y<count($asignados); $y++){$ppe=$asignados[$y];
													$cantidad=$this->lib->desencriptar_num($ppe->cantidad)*1;
													if($ppe->idpr==$proceso->idpr){
														if($ppe->fecha_fin!="" && $ppe->fecha_fin!=null){
															$cantidad_terminada_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
														}else{
															$cantidad_produccion_proceso+=$this->lib->desencriptar_num($ppe->cantidad)*1;
														}
													}
												}
												if($cantidad_produccion<$cantidad_produccion_proceso){ $cantidad_produccion=$cantidad_produccion_proceso;}
												if($cantidad_terminada>$cantidad_terminada_proceso){ $cantidad_terminada=$cantidad_terminada_proceso;}
											}
										}
										?>
										<div class="panel panel-info">
											<div class="panel-heading bg-info"><?php echo $sucursal->nombre;?></div>
											<div class="panel-body" style="padding: 0px;">
												<div class="card-group text-xs-center">
													<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="cantidad" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
														<div class="card-block">
															<h2><?php echo $cantidad_pedida;?></h2>
															<p class="card-text">Cantidad total</p>
														</div>
													</div>
													<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="pendiente" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
														<div class="card-block">
															<h2><?php echo $cantidad_pedida-$cantidad_produccion-$cantidad_terminada; ?></h2>
															<p class="card-text">Pendiente</p>
														</div>
													</div>
													<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="produccion" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
														<div class="card-block">
															<h2><?php echo $cantidad_produccion; ?></h2>
															<p class="card-text">En producción</p>
														</div>
													</div>
													<div class="card card-inner-shadow waves-effect card-hover global-cards detalle_produccion<?php echo $id_rand;?>" data-type="terminado" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
														<div class="card-block">
															<h2><?php echo $cantidad_terminada;?></h2>
															<p class="card-text">Terminado</p>
														</div>
													</div>
												</div>
											</div>
											<div class="panel-footer txt-info">
												<div class="row text-right" style="padding-right:10px;">
													<button type="button" class="btn btn-inverse-info waves-effect btn-mini asignar_empleado_sucursal<?php echo $id_rand;?>" data-tbl-color="<?php echo $idtbc;?>" data-tbl-producto="<?php echo $idtbl;?>" data-sdp="<?php echo $idsdp;?>" data-p="<?php echo $producto->idp;?>">
														<i class="fa fa-cog"></i><span class="m-l-10">Configurar</span>
													</button>
												</div>
											</div>
										</div>
										<br/><?php 	
												}//end if
											}// end for 
											?>
							</div>
						</div>
					</div>
					<?php }// end foreach?>
</div>
<script>$("div.detalle_produccion<?php echo $id_rand;?>").detalle_produccion();$("button.refresh_producto_color_pedido<?php echo $id_rand;?>").refresh_producto_color_pedido();$("button.asignar_empleado_sucursal<?php echo $id_rand;?>").asignar_empleado_sucursal();</script>