<?php 
	$porcentaje=0;
	$c=0;
	for ($i=0; $i < count($colores) ; $i++) { $color=$colores[$i];
		//var_export($color);
		$detalle=$this->lib->search_elemento($detalles,'idpgrc',$color->idpgrc);
		if($detalle!=null){
			$porcentaje+=$detalle->porcentaje;$c++;
		}
	}
	if($c>0){
		$porcentaje/=$c;
	}
	$porcentaje=number_format($porcentaje,3,'.','');
	$porcentaje*=100;
 ?>
<div class="progress" style="height: 17px;">
	<div class="progress-bar progress-bar-striped <?php if($porcentaje<100){?>progress-bar-animated<?php }?>" role="progressbar" aria-valuemax="100" style="width: <?php echo $porcentaje."%";?>">
		<?php if($porcentaje>17){ echo $porcentaje."%";} ?>
	</div>
<?php if($porcentaje<=17){?><span style="margin-left: 5px;"><?php echo $porcentaje."%";?></span><?php } ?>
</div>