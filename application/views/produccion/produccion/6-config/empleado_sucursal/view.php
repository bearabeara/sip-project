<?php
	$url=base_url().'libraries/img/';
	$procesos=$this->lib->producto_grupo_color_procesos($producto_grupo_color,$producto_proceso,$producto_grupo_proceso,$producto_grupo_color_proceso);
	$cantidad=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1;
	$codigo=$producto->codigo;
	$nombre=$producto->nombre;
	if($producto_grupo_color->abr_g!="" && $producto_grupo_color->abr_g!=NULL){ $codigo.="-".$producto_grupo_color->abr_g; $nombre.=" - ".$producto_grupo_color->nombre_g;}
	if($producto_grupo_color->abr_c!="" && $producto_grupo_color->abr_c!=NULL){ $codigo.="-".$producto_grupo_color->abr_c; $nombre.=" - ".$producto_grupo_color->nombre_c;}
	$img="sistema/miniatura/default.jpg";
	if($producto_grupo_color->portada!="" && $producto_grupo_color->portada!=null){
		$v=explode("|", $producto_grupo_color->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
		}
	}else{
		if($producto->portada!="" && $producto->portada!=null){
			$v=explode("|", $producto->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			}
		}
	}
?>
<div class="list-group-item" style="max-width:100%" id="datos-procesos-empleados" data-tbl-color="<?php echo $tbl_color;?>" data-tbl-producto="<?php echo $tbl_producto;?>" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-dp="<?php echo $detalle_pedido->iddp;?>" data-pp="<?php echo $detalle_pedido->idpp;?>" data-sdp="<?php echo $sucursal_detalle_pedido->idsdp;?>">
	<table class="table table-bordered">
		<tr>
			<td class="g-thumbnail">
				<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $nombre;?>" data-desc="<?php echo 'Cantidad pedido: '.$cantidad.' u.';?>" width="100%">
			</td>
			<td><strong>Código</strong><p class="lead m-t-0"><?php echo $codigo; ?></td>
			<td><strong>Nombre</strong><p class="lead m-t-0"><?php echo $nombre; ?></td>
			<td><strong>Sucursal</strong><p class="lead m-t-0"><?php echo $sucursal->nombre_sucursal; ?></p></td>
			<td><strong>Cantidad</strong><p class="lead m-t-0"><?php echo $cantidad." u."; ?></p></td>
		</tr>
	</table>
</div>
<div class="list-group-item" style="max-width:100%">
	<div class="table-responsive g-table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="">#</th>
					<th width="16%">Proceso</th>
					<th width="5%">Cant. total</th>
					<th width="79%">Empleados encargados de producción</th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($procesos)){
					$procesos=json_decode(json_encode($procesos));
					$cont=1;
					foreach($procesos as $key => $proceso){
						$cantidad_asignada=0;
				?>
						<tr>
							<td><?php echo $cont++;?></td>
							<td><?php echo $proceso->nombre;?></td>
							<td><?php echo $cantidad."u.";?></td>
							<td class="cards">
								<div class="flex-container">
							<?php for($i=0; $i<count($productos_pedidos_empleados); $i++){ $producto_pedido_empleado=$productos_pedidos_empleados[$i];
									$estado="Asignado";$class="bg-danger";
									if(($producto_pedido_empleado->fecha_inicio!="" && $producto_pedido_empleado->fecha_inicio!=NULL) || ($producto_pedido_empleado->fecha_fin!="" && $producto_pedido_empleado->fecha_fin!=NULL)){
										if($producto_pedido_empleado->fecha_fin=="" && $producto_pedido_empleado->fecha_fin==NULL){
											$estado="producción";
											$class="bg-info";
										}else{
											$estado="Entregado";
											$class="bg-success";
										}
									}
									$empleado=$this->lib->search_elemento($productos_procesos_empleados,'ide',$producto_pedido_empleado->ide);
									if($producto_pedido_empleado->idpr==$proceso->idpr && $empleado!=NULL){
										$cantidad_empleado=$this->lib->desencriptar_num($producto_pedido_empleado->cantidad);
										$cantidad_asignada+=$cantidad_empleado*1;
										$img="sistema/miniatura/default.jpg";
										if($empleado->fotografia!="" && $empleado->fotografia!=null){ $img="personas/miniatura/".$empleado->fotografia;}
							?>
									<div class="flex-element flex-180">
										<div class="card-group">
											<div class="card img-thumbnail-45">
												<div class="label-main" style="float: left;"><label class="label <?php echo $class;?>" style="position: absolute;padding:2px;font-size: 51%;opacity: .9;bottom: 0px;margin-bottom: 0px;"><?php echo $estado;?></label></div>
												<img class="card-img-top" src="<?php echo $url.$img;?>" alt="image" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="<?php echo $proceso->nombre.', cantidad: '.$cantidad_empleado.' u.';?>" style="vertical-align: top;cursor: zoom-in;">
											</div>
											<div class="card card-padding-0 card-hover">
												<a href="javascript:" class="change_producto_empleado" data-sdp="<?php echo $producto_pedido_empleado->idsdp;?>" data-pr="<?php echo $proceso->idpr;?>" data-ppe="<?php echo $producto_pedido_empleado->idppe;?>" data-p="<?php echo $producto->idp; ?>">
													<div class="a-card-block">
														<p class="a-card-text">
															<small style="color: #069bcc !important;"><?php echo $empleado->nombre." ".$empleado->paterno; ?></small>
															<h6 style="color: #069bcc !important;"><?php echo $cantidad_empleado; ?> u.</h6>
														</p>
													</div>
												</a>
											</div>
											<div class="card card-padding-0 card-30 card-middle">
												<div class="card-block">
													<?php if($estado!="Entregado"){?>
													<div class="g-control-accordion" style="width:30px;">
														<button class="confirmar_producto_pedido_empleado" data-ppe="<?php echo $producto_pedido_empleado->idppe;?>"><i class="icon-trashcan2"></i></button>
													</div>
													<?php }?>
												</div>
											</div>
										</div>
									</div>
							<?php }
								} ?>
							<?php if($cantidad_asignada<$cantidad){?>
									<div class="flex-element flex-180">
										<div class="card-group">
											<div class="card img-thumbnail-45">
												<h2 class="card-text text-muted" style="margin-top: 10%;"><i class="fa fa-user"></i></h2>
											</div>
											<div class="card card-hover">
												<a href="javascript:" class="adicionar_empleado_sucursal_proceso" data-pr="<?php echo $proceso->idpr;?>">
													<div class="card-block" style="padding-top: 10px;"><h5 class="card-text text-muted"><i class="fa fa-plus"></i></h5></div>
												</a>
											</div>
											<div class="card card-padding-0" style="width:30px;vertical-align: middle"><div class="card-block"></div></div>
										</div>
									</div>
							<?php }?>
								</div>
							</td>
						</tr>
						<?php
				}// end for
			}// end if
			?>
		</tbody>
	</table>
</div>
</div>
<script>$("a.adicionar_empleado_sucursal_proceso").adicionar_empleado_sucursal_proceso();$("img.card-img-top").visor();$("a.change_producto_empleado").change_producto_empleado();

	$("button.confirmar_producto_pedido_empleado").confirmar_producto_pedido_empleado();

</script>