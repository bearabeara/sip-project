<?php
	$url=base_url().'libraries/img/';
	$codigo=$producto->codigo;
	$nombre=$producto->nombre;
	if($producto_grupo_color->abr_g!="" && $producto_grupo_color->abr_g!=NULL){ $codigo.="-".$producto_grupo_color->abr_g; $nombre.=" - ".$producto_grupo_color->nombre_g;}
	if($producto_grupo_color->abr_c!="" && $producto_grupo_color->abr_c!=NULL){ $codigo.="-".$producto_grupo_color->abr_c; $nombre.=" - ".$producto_grupo_color->nombre_c;}
	$img="sistema/miniatura/default.jpg";
	if($producto_grupo_color->portada!="" && $producto_grupo_color->portada!=null){
		$v=explode("|", $producto_grupo_color->portada);
		if(count($v==2)){
			if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
		}
	}else{
		if($producto->portada!="" && $producto->portada!=null){
			$v=explode("|", $producto->portada);
			if(count($v==2)){
				if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
			}
		}
	}
	$t = array(0 => 'Maestro', 1 => 'Ayudante');
	//verificando la cantidad asignada al empleado
	$cantidad_pedida=$this->lib->desencriptar_num($sucursal_detalle_pedido->cantidad)*1;
	$cantidad_asignada=0;
	$pendiente=0;
	for($i=0; $i < count($productos_pedido_empleado); $i++){
		if($productos_pedido_empleado[$i]->idpr==$idpr){
			$cantidad_asignada+=($this->lib->desencriptar_num($productos_pedido_empleado[$i]->cantidad)*1);
		}
	}
	if($cantidad_pedida>$cantidad_asignada){
		$pendiente=$cantidad_pedida-$cantidad_asignada;
	}
	
?>
<div class="list-group-item" style="max-width:100%">
	<table class="table table-bordered">
		<tr>
			<td class="g-thumbnail">
				<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $nombre;?>" data-desc="<?php echo 'Cantidad pedido: '.($sucursal_detalle_pedido->cantidad*1).' u., Cantidad pendiente: '.$pendiente.' u.';?>" width="100%">
			</td>
			<td><strong>Código</strong><p class="lead m-t-0"><?php echo $codigo; ?></td>
			<td><strong>Nombre</strong><p class="lead m-t-0"><?php echo $nombre; ?></td>
			<td><strong>Proceso</strong><p class="lead m-t-0"><?php echo $proceso->nombre; ?></p></td>
			<td><strong>Cantidad pendiente</strong><p class="lead m-t-0"><?php echo $pendiente." u."; ?></p></td>
		</tr>
	</table>
</div>
<div class="list-group-item" style="max-width:100%">
<div class="table-responsive g-table-responsive" id="datos-producto-empleado" data-pr="<?php echo $idpr;?>">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%" class="img-thumbnail-45">#Item</th>
				<th width="40%">Nombre</th>
				<th width="30%">Calidad</th>
				<th width="20%">Cantidad</th>
				<th width="5%"></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($empleados)){
				for($i=0; $i < count($empleados) ;$i++){ $empleado=$empleados[$i];
						$img="sistema/default.jpg";
						if($empleado->fotografia!=null && $empleado->fotografia!=""){ $img="personas/miniatura/".$empleado->fotografia;}
						$c_produccion=0;
						for ($j=0; $j < count($productos_pedidos_empleados); $j++){ 
							if($empleado->ide==$productos_pedidos_empleados[$j]->ide){ $c_produccion++;} }
			?>
					<tr>
						<td>
							<div class="img-thumbnail-45"></div>
							<div class="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="Area: <?php echo $t[$empleado->tipo*1]." - ".$proceso->nombre;?>, Calidad: <?php echo $empleado->calidad.' Pts.'?>">
						</td>
						<td><?php echo $empleado->nombre_completo;?><br><span class="label <?php if($empleado->tipo*1==0){?>label-inverse-primary<?php }else{?>label-inverse-info<?php }?> label-md"><?php echo $t[$empleado->tipo*1];?></span></td>
						<td>
							<div class="rangos" style="width: 225px;">
								<?php for($r=0; $r<$empleado->calidad;$r++){ ?>
									<label class="rango fa fa-star" style="color: #1b8bf9; cursor: default; float: left;"></label>
								<?php }?>
							</div>
						</td>
						<td>
							<form class="update_detalle_pedido" data-padre="row-producto">
								<div class="input-group input-90">
									<input class="form-control form-control-sm" type="number" id="can-3-<?php echo $empleado->idproe;?>" data-pre="<?php echo $empleado->idpre;?>" data-max="<?php echo $pendiente;;?>" placeholder="0" min="0" max="<?php echo $pendiente;?>" value="<?php echo $pendiente;?>">
									<span class="input-group-addon form-control-sm">u.</span>
								</div>
							</form>
						</td>
						<td>
							<button type="button" class="btn btn-inverse-primary add_producto_pedido_empleado btn-mini waves-effect waves-light" data-proe="<?php echo $empleado->idproe;?>"><i class="fa fa-plus"></i> add </button>
						</td>
						<td>
							<button type="button" class="btn btn-primary btn-mini waves-effect waves-light" style="padding: 5px 7px;">Prod.
							  <span class="badge" style="font-size: 10px; padding: 2px 4.5px; margin: 0px;"><?php echo $c_produccion;?></span>
							</button>
						</td>
					</tr>
					<?php 
					}// end for
				}else{?>
				<tr>
					<td colspan="6" class="text-center"><h5>0 empleados registrados con el proceso <strong><?php echo $proceso->nombre;?></strong> para el producto</h5></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	<div class="row text-right" style="padding-right:15px;">
		<button type="button" class="btn btn-primary btn-mini waves-effect waves-light adicionar_producto_empleado" data-type="produccion">
			<i class="fa fa-plus"></i><span class="m-l-10">empleado</span>
		</button>
	</div>
	<script>$("button.adicionar_producto_empleado").adicionar_producto_empleado();$("img.g-img-thumbnail").visor();$("button.add_producto_pedido_empleado").add_producto_pedido_empleado();</script>
</div>