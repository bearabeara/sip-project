<?php 
	$j_feriados=json_encode($feriados);
?>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="javascript:" onclick="tiempo_tarea('<?php echo $idpe;?>')">Tiempos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos_piezas('<?php echo $idpe;?>')">Control procesos-piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="para_armador('<?php echo $idpe;?>')">Para armadores</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos_general('<?php echo $idpe;?>')">Control productos</a></li>
</ul>
<div class="list-group">
  <div class="list-group-item active">
    <h5 class="list-group-item-heading" id="g_text">TIEMPOS DE TRABAJO</h5>
  </div>
  <div class="list-group-item">
  	<div class="row">
  		<div class="col-sm-3 col-sm-offset-9 col-xs-12">
  			<select id="nro" class="form-control input-sm">
  				<option value="">Seleccionar...</option>
  			<?php for ($i=1; $i <= 60; $i++) { ?>
  				<option value="<?php echo $i;?>" <?php if($nro==$i){ echo "selected";}?>><?php echo $i;?></option>
  			<?php }?>
  			</select>
  		</div>
  		<div class="col-xs-12">
  			<div id="area" class="table-responsive">
  			<?php $this->load->view('estructura/print/encabezado',['titulo'=>'DETALLE DE TABAJOS','pagina'=>'1']); ?>
  				<table class="tabla tabla-border-true">
	<?php for ($i=0; $i < count($productos) ; $i++) { $producto=$productos[$i]; $color=$this->M_categoria_producto->get_material($producto->idpim); ?>
					<tr class="fila">
						<td class="celda td" colspan="10">
							<strong>Código de producto: </strong><?php echo $producto->codigo;?>;
							<strong>Nombre: </strong><?php echo $producto->nombre.' - '.$color[0]->nombre_c;?>;
							<strong>Cantidad: </strong><?php echo $producto->cantidad.'u.';?>
						</td>
					</tr>
					<tr class="fila">
						<th class="celda th" width="3%">#item</th>
						<th class="celda th" width="27%">Tareas</th>
						<th class="celda th" width="20%">Empleado</th>
						<th class="celda th" width="6%">C/Req.</th>
						<th class="celda th" width="10%">Fecha inicio</th>
						<th class="celda th" width="10%">Fecha fin</th>
						<th class="celda th" width="5%">Tiempo total</th>
						<th class="celda th" width="5%">Tiempo por unidad</th>
						<th class="celda th" width="7%">Costo total(Bs.)</th>
						<th class="celda th" width="7%">Costo por unidad(Bs.)</th>
					</tr>
			<?php $tareas=$this->M_pedido_tarea->get_row_complet('dp.iddp',$producto->iddp);
				if(!empty($tareas)){
					for($t=0;$t < count($tareas); $t++) { $tarea=$tareas[$t];
						if($tarea->idpipr=="" || $tarea->idpipr==NULL){
							if($tarea->idppr!=NULL && $tarea->idppr!=""){
								$proceso=$this->M_producto_proceso->get_proceso($tarea->idppr);$tipo="prp";
							}else{
								$proceso=NULL;$tipo=""; 
							}
						}else{//es una tareas de pieza proceso
							$proceso=$this->M_pieza_proceso->get_proceso($tarea->idpipr);$tipo="pip";
						}
			?>
					<tr class="fila">
						<td class="celda td"><?php echo $t+1;?></td>
						<td class="celda td">
					<?php 	if(!empty($proceso)){
								if($tipo=="pip"){ 
									$grupo=$this->M_pieza_grupo->get($proceso[0]->idpig);
									$material=$this->M_pieza->get_material($proceso[0]->idpi);
							 		echo $proceso[0]->nombre_proceso.": ".$grupo[0]->nombre."<strong> de ".$material[0]->nombre."</strong> (".$proceso[0]->nombre_pieza.")";;
							 		$cantidad=$tarea->cantidad*$proceso[0]->unidades;
								} 
								if($tipo=="prp"){ 
									echo $proceso[0]->nombre.": ".$proceso[0]->sub_proceso;
									$cantidad=$tarea->cantidad;
								}
							}?></td>
						<td class="celda td"><?php echo $tarea->nombre." ".$tarea->nombre2." ".$tarea->paterno." ".$tarea->materno; ?></td>
						<td class="celda td"><?php echo $cantidad."u."; ?></td>
						<td class="celda td"><?php if($tarea->fecha_inicio!=NULL){ echo $tarea->fecha_inicio;}else{echo "-";} ?></td>
						<td class="celda td"><?php if($tarea->fecha_fin!=NULL){ echo $tarea->fecha_fin;}else{echo "-";} ?></td>
						<td class="celda td"><?php 
							$total_horas=0;
							if($tarea->fecha_inicio!=NULL && $tarea->fecha_fin!=NULL){
								$f1=explode(" ", $tarea->fecha_inicio);
								$f2=explode(" ", $tarea->fecha_fin);
								$horas=$this->M_hora_biometrico->exist_fecha('ide',$tarea->ide,$f1[0],$f2[0]);
								if(!empty($horas)){
									$j_horas=json_encode($horas);
									$total_horas=json_decode($this->lib->total_horas_trabajo($j_horas,$tarea->horas,$tarea->tipo_contrato,$j_feriados,$f1[0],$f2[0],$tarea->salario));
									//var_dump($total_horas);
									//echo $total_horas->hra_trabajada.' '.$total_horas->hra_trabajada_no_redondo."<br>-------------------------------------------------------------------<br>";
									$j_horas_antes=json_encode($this->M_hora_biometrico->horas_antes($tarea->ide,$tarea->fecha_inicio));
									$j_horas_antes=json_decode($this->lib->horas_trabajo_modificado($j_horas_antes,$tarea->ide,$f1[0],$f1[1],"fin",$tarea->horas,$tarea->tipo_contrato,$j_feriados));
									//var_dump($j_horas_antes);
									$j_horas_despues=json_encode($this->M_hora_biometrico->horas_despues($tarea->ide,$tarea->fecha_fin));
									$j_horas_despues=json_decode($this->lib->horas_trabajo_modificado($j_horas_despues,$tarea->ide,$f2[0],$f2[1],"ini",$tarea->horas,$tarea->tipo_contrato,$j_feriados));
									//var_dump($j_horas_despues);
									$horas_empleada=($total_horas->hra_trabajada_no_redondo*1)-($j_horas_antes->horas_original*1)-($j_horas_despues->horas_original*1);
									//echo $total_horas;
									echo $this->lib->hms($horas_empleada*60*60);
								}else{
									echo "NB";
								}
							}else{ echo "-";}?></td>
						<td class="celda td"><?php 
							if(!empty($horas)){
								if($tarea->fecha_inicio!=NULL && $tarea->fecha_fin!=NULL){ 
									if($horas_empleada>0){ 
										$por_unidad=$horas_empleada/$cantidad;
										echo $this->lib->hms($por_unidad*60*60);
									}
								}else{ echo "-";}
							}
							?>
						</td>
						<td class="celda td"><?php 
						if(!empty($horas)){
							if($tarea->fecha_inicio!=NULL && $tarea->fecha_fin!=NULL){ 
								if($horas_empleada>0){ 
									echo number_format(($horas_empleada*1)*($total_horas->por_hora*1),1,'.',',');
								}
							}else{ echo "-";}
						}?></td>
						<td class="celda td"><?php 
						if(!empty($horas)){
							if($tarea->fecha_inicio!=NULL && $tarea->fecha_fin!=NULL){ 
								if($horas_empleada>0){ 
									echo number_format((($horas_empleada*1)*($total_horas->por_hora*1))/$cantidad,1,'.',',');
								}
							}else{ echo "-";}
						}?></td>
					</tr>
			<?php
					}
				}else{
			?>
					<tr class="fila">
						<td class="celda th" colspan="11"><h3>0 tareas asignadas...</h3></td>
					</tr>
			<?php
				}
			}// end for?>
				</table>
  			</div>
  		</div>
  	</div>
  </div>
</div>
