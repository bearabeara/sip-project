<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="tiempo_tarea('<?php echo $idpe;?>')">Tiempos</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="procesos_piezas('<?php echo $idpe;?>')">Control procesos-piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="para_armador('<?php echo $idpe;?>')">Para armadores</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos_general('<?php echo $idpe;?>')">Control productos</a></li>
</ul>
<div class="list-group">
  <div class="list-group-item active"><h5 class="list-group-item-heading" id="g_text">PIEZAS EN EL PRODUCTO</h5></div>
  <div class="list-group-item">
  	<div class="row">
  		<div class="col-sm-3 col-sm-offset-9 col-xs-12">
  			<select id="emp" class="form-control input-sm" onchange="search_empleado_productos('<?php echo $idpe;?>')">
  				<option value="">Seleccionar...</option>
  		<?php for($i=0; $i < count($empleados); $i++){ $empleado=$empleados[$i]; 
                if($empleado->tipo_tarea==0){
        ?>
  				<option value="<?php echo $empleado->ide;?>"> <?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno; ?></option>
  			<?php
                }
            } ?>
  			</select>
  		</div>
  		<div id="content_2"></div>
  	</div>
  </div>
</div>
