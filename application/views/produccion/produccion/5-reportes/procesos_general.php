<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:" onclick="tiempo_tarea('<?php echo $idpe;?>')">Tiempos</a></li>
  <li role="presentation"><a href="javascript:" onclick="procesos_piezas('<?php echo $idpe;?>')">Control procesos-piezas</a></li>
  <li role="presentation"><a href="javascript:" onclick="para_armador('<?php echo $idpe;?>')">Para armadores</a></li>
  <li role="presentation" class="active"><a href="javascript:" onclick="procesos_general('<?php echo $idpe;?>')">Control productos</a></li>
</ul>
<div class="list-group">
  <div class="list-group-item active"><h5 class="list-group-item-heading" id="g_text">PROCESOS EN EL PRODUCTO</h5></div>
  <div class="list-group-item">
  	<div class="row">
  		<div class="col-sm-3 col-sm-offset-9 col-xs-12">
  			<select id="proc" class="form-control input-sm" onchange="search_procesos_general('<?php echo $idpe;?>',false)">
  				<option value="">Seleccionar...</option>
  			<?php for ($i=0; $i < count($procesos); $i++){ $proceso=$procesos[$i]; ?>
  				<option value="<?php echo $proceso->idpr;?>"> <?php echo $proceso->nombre; ?></option>
  			<?php }?>
  			</select>
  		</div>
  		<div id="content_2"></div>
  	</div>
  </div>
</div>
