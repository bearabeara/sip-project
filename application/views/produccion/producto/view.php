<?php 
	$url=base_url().'libraries/img/';
?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class='img-thumbnail-60'><div class="img-thumbnail-60">#Item</div></th>
			<th class="celda-sm-10">Código</th>
			<th class="celda-sm-10 hidden-sm">Código anterior</th>
			<th class="celda-sm-30">Nombre</th>
			<th style="width:15%" class="hidden-md">Fecha de Creación</th>
			<th style="width:15%" class="hidden-sm">Categorías</th>
			<th style="width:20%" class="hidden-sm">Observaciónes</th>
			<th>Est.</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php 
if(count($productos)>0){
	$cont=0;
	for($i=0; $i < count($productos); $i++){ $producto=$productos[$i];
		$control=NULL;
		 	if($atrib!="" && $val!="" && $type_search!=""){
		 		$control=$this->lib->search_where($producto,$atrib,$val,$type_search);
		 	}
			$img="sistema/miniatura/default.jpg";
			if($producto->portada!="" && $producto->portada!=null){
				$v=explode("|", $producto->portada);
				if(count($v==2)){
					if($v[0]=="1"){ $control_img=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control_img)){ $img="productos/miniatura/".$control_img[0]->archivo;}}
					if($v[0]=="3"){ $control_img=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control_img)){ $img="productos/miniatura/".$control_img[0]->archivo;}}
				}
			}
		$grupos=$this->lib->select_from($grupos_colores,"idp",$producto->idp);
?>
	<tr <?php if($producto->estado!=1){?> class="inactivo" title="Producto inactivo"<?php }?><?php if(($estado!="" && $estado!=$producto->estado) || ($atrib!="" && $val!="" && $type_search!="" && $control==null)){?> style="display: none;"<?php }else{$cont++;}?> data-p="<?php echo $producto->idp;?>">
		<td><div class="item"><?php echo $cont;?></div>
			<img src="<?php echo $url.$img;?>" width="100%" class="img-thumbnail img-thumbnail-60" data-id="<?php echo $producto->idp;?>" data-type="producto"/>
		</td>
		<td data-col="2" data-estado="<?php echo $producto->estado;?>"><?php if($atrib=="codigo" && $val!="" && $control!=null){echo $this->lib->str_replace_all($producto->codigo,$this->lib->all_minuscula($val),"mark");}else{echo $producto->codigo;}?></td>
		<td class="hidden-sm" data-col="3" data-estado="<?php echo $producto->estado;?>"><?php if($atrib=="codigo_aux" && $val!="" && $control!=null){echo $this->lib->str_replace_all($producto->codigo_aux,$this->lib->all_minuscula($val),"mark");}else{echo $producto->codigo_aux;}?></td>
		<td><span data-col="4" data-estado="<?php echo $producto->estado;?>"><?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($producto->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $producto->nombre;}?></span><br>
	<?php if(count($grupos)>0){?>
			<ul class="list-group visible-sm" style="font-size:10px; padding: 1px">
				<?php for ($c=0; $c < count($grupos) ; $c++) { $grupo=$grupos[$c]; ?>
					<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
							$co="";if($grupo->nombre_c!=""){ $co=$grupo->nombre_c;}
					?>
						<li class="list-group-item" style="padding:2px 3px; width:100%; border-color:<?php echo $grupo->codigo_c;?>"><?php echo $gr.$co;?></li>
				<?php }?>
			</ul>
	<?php }?>

		</td>
		<td class="hidden-md" data-col="5" data-value="<?php echo $producto->fecha_creacion;?>" data-estado="<?php echo $producto->estado;?>"><?php echo $this->lib->format_date($producto->fecha_creacion,'dl ml Y'); ?></td>
		<td class="hidden-sm">
<?php 	if(count($grupos)>0){ ?>
			<ul class="list-group" style="font-size:10px; padding: 1px">
				<?php for($c=0; $c < count($grupos) ; $c++){$grupo=$grupos[$c]; ?>
					<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
							$co="";if($grupo->nombre_c!=""){ $co=$grupo->nombre_c;}
					?>
						<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; width:100%; background:<?php echo $grupo->codigo_c;?>"><?php echo $gr.$co;?></li>
					<?php } ?>
			</ul>
	<?php } ?>
		</td>
		<td class="hidden-sm"><?php echo $producto->observaciones; ?></td>
		<td><center>
			<div class="btn-circle btn-circle-<?php if($producto->estado==1){ echo 'success';}else{ echo 'default';}?> btn-circle-30 change_estado" data-p="<?php echo $producto->idp;?>"></div>
		</center></td>
		<td>
		<?php 
			$det=json_encode(array('function'=>'view_reportes','atribs'=> json_encode(array('p' => $producto->idp)),'title'=>'Detalle de producto'));
			$conf=""; if($privilegio->pr1u=="1"){$conf=json_encode(array('function'=>'config_producto','atribs'=> json_encode(array('p' => $producto->idp)),'title'=>'Configurar producto'));}
			$del=""; if($privilegio->pr1d=="1"){$del=json_encode(array('function'=>'confirm_producto','atribs'=> json_encode(array('p' => $producto->idp)),'title'=>'Eliminar producto'));}
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
		</td>
	</tr>
	<?php }//end for
	}else{//end if
		echo "<tr><td colspan='8' class='text-center'><h3>0 registros encontrados...</h3></td></tr>";
	}?>
	</tbody>
</table>
<?php 
	if($privilegio->pr1r=="1" && ($privilegio->pr1p=="1" || $privilegio->pr1c=="1")){
		$report="";$new="";$help="";
		if($privilegio->pr1p=="1"){$report=json_encode(array('function'=>'print_producto','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
		if($privilegio->pr1c=="1"){$new=json_encode(array('function'=>'new_producto','title'=>'Nuevo producto'));}
		if($privilegio->pr1p=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
	}
?>
<script>$("img.img-thumbnail-60").visor();$("div.change_estado").change_estado();$(".view_reportes").view_reportes();$(".config_producto").config_producto();$(".confirm_producto").confirm_producto();if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}</script>
