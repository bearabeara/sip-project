<?php 
$url=base_url().'libraries/img/productos/miniatura/';
$url_mat=base_url().'libraries/img/materiales/miniatura/';
$url_img=base_url().'libraries/img/';
$help1='title="Contenedor general" data-content="Contiene todos los materiales generales de la categoría."';
$help2='title="Adicionar material" data-content="Adicione materiales en el contenedor general de la categoría."';
$help3='title="Contenedor general de procesos" data-content="Contiene todos los procesos generales de la categoría."';
$help4='title="Adicionar procesos" data-content="Adicione procesos en el contenedor general de la categoría."';
$help10='title="Contenedor general" data-content="Contiene todos los materiales del color de la categoría."';
$help11='title="Adicionar materiales" data-content="Adicione materiales en el color de la categoría."';
$help12='title="Contenedor general" data-content="Contenedor de todas las imágenes del producto, de sus categorias y colores."';
$help13='title="Adicionar fotografías" data-content="Adicione fotografías en el color de la categoría."';
$help14='title="Contenedor general" data-content="Contiene todos las piezas del color de la categoría."';
$help15='title="Adicionar piezas" data-content="Adicione piezas en el color de la categoría."';
$help16='title="Contenedor general" data-content="Contiene todos los procesos en el color la categoría."';
$help17='title="Adicionar procesos" data-content="Adicione procesos en el color de la categoría."';
$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
$popover1='data-toggle="popover" data-placement="top" data-trigger="hover"';
$grupo_color=$this->M_producto_grupo_color->get_grupo_color('pg.idp',$producto->idp);
$varios_grupo=$this->lib->varios_grupo($grupo_color);
$rand=rand(10,99999);
?>
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item"><a href="javascript:" class="nav-link config_producto<?php echo $rand;?>" data-p='<?php echo $producto->idp;?>'>Producto</a></li>
  <li class="nav-item"><a href="javascript:" class="nav-link active categoria" data-p="<?php echo $producto->idp;?>">Categorias</a></li>
  <li class="nav-item"><a href="javascript:" class="nav-link produccion" data-p="<?php echo $producto->idp;?>">Producción</a></li>
</ul>
<div class="list-group" id="datos-producto" data-p="<?php echo $producto->idp;?>" data-type="producto_color">
  <div class="list-group-item" style="max-width:100%"></div>
  <div class="list-group-item" style="max-width:100%">
    <div class="row">
      <div class="form-group col-xs-12">
        <div id="list_product" class="accordion-border" role="tablist" aria-multiselectable="true">
          <?php if(count($grupos)>0){?>
          <?php for ($i=0; $i < count($grupos) ; $i++) { $grupo=$grupos[$i]; 
            $pgc=$this->M_producto_grupo_color->get_row('idpgr',$grupo->idpgr);
            $materiales_grupo=$this->M_producto_grupo_material->get_material("pgm.idpgr",$grupo->idpgr);
            $atributos_grupo=$this->M_producto_grupo_atributo->get_atributo("pga.idpgr",$grupo->idpgr);
            $atributos_producto=$this->M_producto_atributo->get_atributo("pa.idp",$producto->idp);
            $abr_g="";if($grupo->abr!=""){ $abr_g="-".$grupo->abr;} 
            ?>
            <div class="accordion-panel" id="accordion-panel<?php echo $grupo->idpgr;?>" data-cli="">
              <div class="accordion-heading" role="tab" id="heading<?php echo $grupo->idpgr;?>">
                <span class="card-title accordion-title">
                    <span class="g-control-accordion">
                      <button class="refresh_producto_grupo" data-pg="<?php echo $grupo->idpgr;?>"><i class="fa fa-refresh"></i></button>
                      <button class="producto_grupo_color_seg" data-pg="<?php echo $grupo->idpgr;?>" data-type="producto_grupo"><i class="fa fa-clock-o"></i></button>
                      <button class="grupos" data-p="<?php echo $producto->idp;?>" data-pg="<?php echo $grupo->idpgr;?>" data-type="update"><i class="fa fa-exchange"></i></button>
                      <button class="confirm_producto_grupo" data-p="<?php echo $producto->idp;?>" data-pg="<?php echo $grupo->idpgr;?>"><i class="icon-bin"></i></button>
                    </span>
                  <a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $grupo->idpgr;?>" aria-expanded="false" aria-controls="collapse<?php echo $grupo->idpgr;?>" style="padding-right: 6px;">
                    <span id="titulo_grupo<?php echo $grupo->idpgr;?>"><ins><?php echo $this->lib->all_mayuscula($producto->codigo.$abr_g.": ".$producto->nombre." - ".$grupo->nombre);?></ins></span>
                  </a>
                </span>
              </div>
              <div id="collapse<?php echo $grupo->idpgr;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $grupo->idpgr;?>" aria-expanded="false" style="height: 0px;">
                <div class="accordion-content accordion-desc" style="padding: 0px 7px;">
                  <div class="list-group" id="pg<?php echo $grupo->idpgr;?>">
                  <?php //if(($varios_grupo && count($pgc)>1) || count($materiales_grupo)>0 || count($atributos_grupo)>0){ ?>
                      <!--stacks grupo-->
                        <?php if(($varios_grupo && count($pgc)>1) || count($materiales_grupo)>0){ ?>
                        <div class="list-group-item" style="max-width:100%">
                          <div class="row">
                            <!--stack material-->
                            <div class="col-lg-6 col-sm-12">
                              <div class="g-table-responsive table-responsive">
                                <table cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                      <div class="item help" <?php echo $popover1.$help1;?>>
                                        <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                      </div>
                                      <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_materiales" data-type="pgm-modal" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                                    </td>
                                    <?php if($varios_grupo && count($pgc)>1){ ?>
                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                      <div class="item help" <?php echo $popover1.$help2;?>>
                                        <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                      </div>
                                      <div class="img-thumbnail-45"></div><a href"javascript:" class="material" data-type="pgm" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                    </td>
                                    <?php }?>

                                    <?php for($m=0; $m < count($materiales_grupo); $m++){ $material=$materiales_grupo[$m]; 
                                      $archivo="";$desc="<br>";
                                      $desc="Cantidad ".$material->cantidad;
                                      $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                                      if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                                      if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                                      $tit=$material->nombre;
                                      $img_mat="sistema/miniatura/default.jpg";
                                      if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                                      ?>
                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                        <div id="item" style="border:1px solid rgba(255, 82, 82, .74);">
                                          <button type="button" class="close confirmar_material" title="Eliminar Imagen" data-pgm="<?php echo $material->idpgm;?>" data-pg="<?php echo $grupo->idpgr;?>" data-type="pgm" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                                        </div>
                                        <div class="img-thumbnail-45">
                                          <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail thumbnail-2 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                                      </div>
                                    </td>
                                    <?php }?>
                                    <?php $materiales_producto=$this->M_producto_material->get_material("pm.idp",$producto->idp);?>
                                    <?php for ($m=0; $m < count($materiales_producto) ; $m++) { $material=$materiales_producto[$m]; 
                                      $archivo="";$desc="<br>";
                                      $desc="Cantidad ".$material->cantidad;
                                      $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                                      if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                                      if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                                      $tit=$material->nombre;
                                      $img_mat="sistema/miniatura/default.jpg";
                                      if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                                      ?>
                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                        <div class="img-thumbnail-45">
                                          <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45 child" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                                        </div>
                                    </td>
                                    <?php }?>
                                  </tr>
                                </table>
                              </div>
                            </div><i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
                          <!--end stack material-->
                          <!--stack proceso-->
                            <div class="col-lg-6 col-sm-12">
                              <div class="g-table-responsive table-responsive">
                                <table cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                      <div class="item help" <?php echo $popover1.$help3;?>>
                                        <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                      </div>
                                      <div class="img-thumbnail-45"></div><a href="javascript:" data-type-save="pgpr-modal" class="pila_producto_proceso" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                                    </td>
                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                      <div class="item help" <?php echo $popover1.$help4;?>>
                                        <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                      </div>
                                      <div class="img-thumbnail-45"></div><a href="javascript:" class="new_producto_proceso" data-type-save="pgpr" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                    </td>
                                    <?php for($m=0; $m < count($producto_procesos); $m++){ $producto_proceso=$producto_procesos[$m]; 
                                      $control=$this->lib->search_elemento($producto_grupo_procesos,"idppr",$producto_proceso->idppr);
                                      $control2=$this->lib->search_elemento($producto_grupo_color_procesos,"idppr",$producto_proceso->idppr);
                                      $sw=true;
                                      if($control!=null){ if($control->idpgr!=$grupo->idpgr){ $sw=false;}}
                                      if($sw && $control2==null){ ?>
                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;">
                                          <?php if($control!=null){ ?>
                                          <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);">
                                            <button type="button" class="close confirmar_producto_proceso" title="Eliminar Imagen" data-ppr="<?php echo $producto_proceso->idppr;?>" data-type-save="pgpr" data-pg="<?php echo $grupo->idpgr;?>" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                                          </div>
                                          <?php } ?>
                                          <div class="img-thumbnail <?php if($control!=null){ echo 'thumbnail-2';}else{ echo 'child';}?> img-thumbnail-45" style="height: 45px; vertical-align: middle; overflow: hidden; line-height: .9;" title="<?php echo $producto_proceso->nombre;?>">
                                           <div style="height: 100%; display: table;">
                                            <small style="font-size: 61%; vertical-align: middle; display: table-cell;"><?php echo $producto_proceso->nombre;?></small>
                                          </div>
                                        </div>
                                      </td>
                                      <?php  }
                                    }
                                    ?>
                                  </tr>
                                </table>
                              </div>
                            </div><i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
                            <!--end stack proceso-->
                          </div>
                        </div>
                        <?php }// end if?>
                      <!--end stacks grupo-->
                      <!--atributos grupo-->
                        <?php if(($varios_grupo && count($pgc)>1) || count($atributos_grupo)>0){ ?>
                        <div class="list-group-item" style="max-width:100%">
                          <div class="row">
                           <div class="form-group" id="pga<?php echo $grupo->idpgr;?>">
                            <?php $count=0;
                            if(!empty($atributos_producto)){
                              ?>    
                              <?php 
                              for ($j=0; $j < count($atributos_producto) ; $j++){ $atributo=$atributos_producto[$j]; $count++; ?>
                              <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
                              <div class="col-sm-4 col-xs-12">
                                <div class="input-group">
                                  <div class="form-control form-control-sm textarea disabled"><?php echo $atributo->valor;?></div>
                                </div>
                              </div>
                              <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
                              <?php } ?>
                              <?php  } ?>
                              <input type="hidden" value="" id="delete-grupo-atributo<?php echo $grupo->idpgr;?>">
                              <?php 
                              for ($j=0; $j < count($atributos_grupo) ; $j++){ $atributo=$atributos_grupo[$j];  $count++;?>
                              <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
                              <div class="col-sm-4 col-xs-12">
                                <div class="input-group">
                                  <textarea id="atr<?php echo rand(1,9999).$atributo->idpga;?>" maxlength="150" class="form-control form-control-sm" data-pga="<?php echo $atributo->idpga;?>" data-pg="<?php echo $grupo->idpgr;?>" data-typele="db" data-type="grupo" placeholder="<?php echo $atributo->atributo;?>"><?php echo $atributo->valor;?></textarea>
                                  <?php $helpa='title="'.$atributo->atributo.'" data-content="Ingrese un contenido alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';?>
                                  <span class="input-group-addon form-control-sm  btn-default" <?php echo $popover4.$helpa; ?>><i class="fa fa-info-circle"></i></span>
                                  <a href="javascript:" class="input-group-addon form-control-sm btn-default eliminar_atributo"><i class="fa fa-trash-o"></i></a>
                                </div>
                              </div>
                              <span <?php if(($count)%2==0){?> class='clearfix'<?php }?>></span>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="row text-right" style="padding-right:15px;">
                            <?php if($varios_grupo){ ?>
                            <button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light atributos" data-type="grupo" data-pg="<?php echo $grupo->idpgr;?>"><i class="fa fa-plus"></i><span class="m-l-10">Atributo</span></button>
                            <?php } ?>
                            <?php if($varios_grupo || count($atributos_grupo)>0){ ?>
                            <button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light save_producto_grupo_atr" data-pg="<?php echo $grupo->idpgr;?>"><i class="icon-floppy-o"></i><span class="m-l-10">Guardar</span></button>
                            <?php } ?>
                          </div>
                        </div>
                        <?php }// end if?>
                      <!--end atributos grupo-->
                      <!--colores grupo-->
                      <div class="list-group-item" style="max-width:100%">
                        <div class="">
                          <div class="form-group">
                            <div id="list_product_color" class="accordion-border" role="tablist" aria-multiselectable="true">
                            <?php if(!empty($pgc)){ ?>
                              <?php for($co=0; $co < count($pgc) ; $co++){ $producto_grupo_color=$pgc[$co]; 
                                $color=$this->lib->search_elemento($colores,"idco",$producto_grupo_color->idco);
                                if($color!=null){
                                  $id_accordion=$grupo->idpgr."-".$producto_grupo_color->idpgrc;
                                  $producto_grupo_color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$producto_grupo_color->idpgrc);
                                  $img="default.png";
                                  $codigo_c=$producto->codigo;
                                  if($grupo->abr!="" && $grupo->abr!=null){ $codigo_c.="-".$grupo->abr;}
                                  $codigo_c.="-".$color->abr;
                              ?>
                                  <div class="accordion-panel" id="accordion-panel<?php echo $id_accordion;?>" data-cli="">
                                    <div class="accordion-heading" role="tab" id="heading<?php echo $id_accordion;?>">
                                      <span class="card-title accordion-title">
                                        <span class="g-control-accordion" style="font-size: .85em;">
                                          <button class="refresh_grupo_color" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><i class="icon-refresh"></i></button>
                                          <button class="producto_grupo_color_seg" data-pg="<?php echo $grupo->idpgr;?>" data-type="producto_grupo_color" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><i class="icon-clock"></i></button>
                                          <button class="colores" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="update"><i class="fa fa-exchange"></i></button>
                                          <button class="confirm_grupo_color" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-pg="<?php echo $grupo->idpgr;?>"><i class="icon-trashcan2"></i></button>
                                        </span>
                                        <a class="accordion-msg scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $id_accordion;?>" aria-expanded="false" aria-controls="collapse<?php echo $id_accordion;?>" style="padding-right: 6px;">
                                          <span id="titulo_color<?php echo $producto_grupo_color->idpgrc;?>"><?php echo $codigo_c.": ".$color->nombre;?></span>
                                        </a>
                                      </span>
                                    </div>
                                    <div id="collapse<?php echo $id_accordion;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $id_accordion;?>" aria-expanded="false" style="height: 0px; width: 100%">
                                      <div class="accordion-content accordion-desc" style="padding: 0px 7px;">
                                        <div class="list-group" id="pgc<?php echo $producto_grupo_color->idpgrc;?>">
                                        <!--stacks color-->
                                          <div class="list-group-item" style="max-width:100%">
                                            <div class="row">
                                              <!--Manejo de materiales en el color-->
                                              <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
                                                <div class="g-table-responsive table-responsive">
                                                  <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                        <div class="item help" <?php echo $popover1.$help10;?>>
                                                          <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                        </div>
                                                        <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_materiales" data-type="pgcm-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>">
                                                        <img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45" ></a>
                                                      </td>
                                                      <?php if(count($grupo_color)>1){?>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                        <div class="item help" <?php echo $popover1.$help11;?>>
                                                          <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                        </div>
                                                        <div class="img-thumbnail-45"></div><a href"javascript:" class="material" data-type="pgcm" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>">
                                                        <img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                      </td>
                                                      <?php } ?>
                                                      <?php $materiales_color=$this->M_producto_grupo_color_material->get_material("pgcm.idpgrc",$producto_grupo_color->idpgrc);?>
                                                      <?php for ($m=0; $m < count($materiales_color) ; $m++) { $material=$materiales_color[$m]; 
                                                        
                                                        $archivo="";$desc="<br>";
                                                        $desc="Cantidad ".$material->cantidad;
                                                        $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                                                        if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                                                        if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                                                        $tit=$material->nombre;
                                                        $img_mat="sistema/miniatura/default.jpg";
                                                        if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                                                        ?>
                                                        <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                                          <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_material" title="Eliminar Imagen" data-pgcm="<?php echo $material->idpgcm;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="pgcm" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                                                          <div class="img-thumbnail-45">
                                                            <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                                                        </div>
                                                      </td>
                                                      <?php } ?>
                                                      <?php $materiales_grupo=$this->M_producto_grupo_material->get_material("pgm.idpgr",$grupo->idpgr);?>
                                                      <?php for ($m=0; $m < count($materiales_grupo) ; $m++) { $material=$materiales_grupo[$m]; 
                                                        $archivo="";$desc="<br>";
                                                        $desc="Cantidad ".$material->cantidad;
                                                        $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                                                        if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                                                        if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                                                        $tit=$material->nombre;
                                                        $img_mat="sistema/miniatura/default.jpg";
                                                        if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                                                        ?>
                                                        <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                                          <div class="img-thumbnail-45">
                                                            <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail thumbnail-2 g-img-thumbnail img-thumbnail-45 child" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                                                        </div>
                                                      </td>
                                                      <?php }?>

                                                      <?php $materiales_producto=$this->M_producto_material->get_material("pm.idp",$producto->idp);?>
                                                      <?php for ($m=0; $m < count($materiales_producto) ; $m++) { $material=$materiales_producto[$m]; 
                                                        $archivo="";$desc="<br>";
                                                        $desc="Cantidad ".$material->cantidad;
                                                        $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                                                        if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                                                        if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                                                        $tit=$material->nombre;
                                                        $img_mat="sistema/miniatura/default.jpg";
                                                        if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                                                      ?>
                                                        <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                                          <div class="img-thumbnail-45">
                                                            <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail child g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                                                        </div>
                                                      </td>
                                                      <?php }?>
                                                    </tr>
                                                  </table>
                                                </div>
                                                <i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
                                              </div>
                                              <!--Imagenes en el color del grupo-->
                                              <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
                                                <div class="g-table-responsive table-responsive">
                                                  <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                        <div class="item help" <?php echo $popover1.$help12;?>>
                                                          <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                        </div>
                                                        <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_fotografias" data-type="pgcf-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                      </td>
                                                      <?php if(count($grupo_color)>1){?>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                        <div class="item help" <?php echo $popover1.$help13;?>>
                                                          <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                        </div>
                                                        <div class="img-thumbnail-45"></div><a href"javascript:" class="new_fotografias" data-type="pgcf" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                      </td>
                                                      <?php } ?>
                                                      <?php $imagenes_color=$this->M_producto_imagen_color->get_row('idpgrc',$producto_grupo_color->idpgrc);?>
                                                      <?php for ($im=0; $im < count($imagenes_color) ; $im++){ ?>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                        <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_fotografia" title="Eliminar Imagen" data-pgcf="<?php echo $imagenes_color[$im]->idpig;?>" data-type="pgcf"  data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                                                        <div class="img-thumbnail-45">
                                                           <?php $desc="<br>"; if($imagenes_color[$im]->descripcion!="" && $imagenes_color[$im]->descripcion!=null){ $desc=$imagenes_color[$im]->descripcion;}?>
                                                           <img src="<?php echo $url.$imagenes_color[$im]->archivo;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre."-".$color->nombre;?>" data-desc="<?php echo $desc;?>">
                                                       </div>
                                                     </td>
                                                     <?php }?>
                                                     <?php for ($im=0; $im < count($imagenes_producto) ; $im++){  ?>
                                                     <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                      <div class="img-thumbnail-45">
                                                         <?php $desc="<br>"; if($imagenes_producto[$im]->descripcion!="" && $imagenes_producto[$im]->descripcion!=null){ $desc=$imagenes_producto[$im]->descripcion; } ?>
                                                         <img src="<?php echo $url.$imagenes_producto[$im]->archivo;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45 child" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>">
                                                      </div>
                                                   </td>
                                                   <?php }?>
                                                 </tr>
                                               </table>
                                                </div>
                                              </div>
                                           <!--End imagenes en el color del grupo-->
                                           <!--Piezas en el color del grupo-->
                                           <i class="clearfix visible-lg-block"></i>
                                           <div class="col-lg-6 col-sm-12">
                                            <div class="g-table-responsive table-responsive">
                                              <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                    <div class="item help" <?php echo $popover1.$help14;?>>
                                                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                    </div>
                                                    <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_piezas" data-type="pgcpi-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                  </td>
                                                  <?php if(count($grupo_color)>1){?>
                                                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                    <div class="item help" <?php echo $popover1.$help15;?>>
                                                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                    </div>
                                                    <div class="img-thumbnail-45"></div><a href"javascript:" class="new_pieza" data-type="pgcpi" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                  </td>
                                                  <?php } ?>
                                                  <?php for ($pi=0; $pi < count($producto_grupo_color_piezas) ; $pi++) { $pieza=$producto_grupo_color_piezas[$pi]; 
                                                    if($pieza->fotografia!="" && $pieza->fotografia!=null){
                                                      $archivo=$url_img."piezas/miniatura/".$pieza->fotografia;
                                                    }else{
                                                      $archivo=$url_img."/sistema/miniatura/default.jpg";
                                                    }
                                                    $desc="Cantidad ".$pieza->cantidad." unid., largo ".$pieza->largo."cm., ancho ".$pieza->ancho."cm.";
                                                    if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc.=", ".$pieza->descripcion;}
                                                    ?>
                                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                                      <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_pieza" title="Eliminar pieza" data-pgcpi="<?php echo $pieza->idpgcpi;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="pgcpi" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                                                      <div class="img-thumbnail-45">
                                                        <img src="<?php echo $archivo;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
                                                    </div>
                                                  </td>
                                                  <?php }?>
                                                  <?php for ($pi=0; $pi < count($producto_piezas) ; $pi++) { $pieza=$producto_piezas[$pi]; 
                                                    if($pieza->fotografia!="" && $pieza->fotografia!=null){
                                                      $archivo=$url_img."piezas/miniatura/".$pieza->fotografia;
                                                    }else{
                                                      $archivo=$url_img."/sistema/miniatura/default.jpg";
                                                    }
                                                    $desc="Cantidad ".$pieza->cantidad." unid., largo ".$pieza->largo."cm., ancho ".$pieza->ancho."cm.";
                                                    if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc.=", ".$pieza->descripcion;}

                                                    ?>
                                                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                                                      <div class="img-thumbnail-45">
                                                        <img src="<?php echo $archivo;?>" class="img-thumbnail child g-img-thumbnail img-thumbnail-45" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
                                                      </div>
                                                  </td>
                                                  <?php }?>
                                                </tr>
                                              </table>
                                            </div>
                                          </div><i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
                                          <!--Procesos en el color del grupo-->
                                          <div class="col-lg-6 col-sm-12">
                                            <div class="g-table-responsive table-responsive">
                                              <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                    <div class="item help" <?php echo $popover1.$help16;?>>
                                                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                    </div>
                                                    <div class="img-thumbnail-45"></div><a href="javascript:" data-type-save="pgcpr-modal" class="pila_producto_proceso" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                  </td>
                                                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                                                    <div class="item help" <?php echo $popover1.$help17;?>>
                                                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                                                    </div>
                                                    <div class="img-thumbnail-45"></div><a href="javascript:" class="new_producto_proceso" data-type-save="pgcpr" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                                                  </td>
                                                  <?php for($pg3=0; $pg3 < count($producto_procesos); $pg3++){ $producto_proceso=$producto_procesos[$pg3];
                                                    $control=$this->lib->search_elemento($producto_grupo_procesos,"idppr",$producto_proceso->idppr);
                                                    $control2=$this->lib->search_elemento($producto_grupo_color_procesos,"idppr",$producto_proceso->idppr);
                                                    $sw=true;
                                                    if($control!=null){ if($control->idpgr!=$producto_grupo_color->idpgr){ $sw=false;}}
                                                    if($control2!=null){ if($control2->idpgrc!=$producto_grupo_color->idpgrc){ $sw=false;}}
                                                    if($sw){
                                                      ?>
                                                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;">
                                                        <?php if($control2!=null){?>
                                                        <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);">
                                                          <button type="button" class="close confirmar_producto_proceso" title="Eliminar proceso" data-ppr="<?php echo $producto_proceso->idppr;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type-save="pgcpr" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                                                        </div>
                                                        <?php }?>
                                                        <div class="img-thumbnail <?php if($control!=null){ echo 'thumbnail-2';}?> <?php if($control2!=null){ echo 'thumbnail-3';}?> img-thumbnail-45 <?php if($control2==null){ echo 'child';}?>" style="height: 45px; vertical-align: middle; overflow: hidden; line-height: .9;" title="<?php echo $producto_proceso->nombre;?>">
                                                         <div style="height: 100%; display: table;">
                                                          <small style="font-size: 61%; vertical-align: middle; display: table-cell;"><?php echo $producto_proceso->nombre;?></small>
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <?php }// end if
                                                        }// end for
                                                    ?>
                                                </tr>
                                                    </table>
                                                  </div>
                                          </div><i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
                                        </div>
                                      </div>
                                        <!--end stacks color-->
                                        <!--atributos color-->
                                          <div class="list-group-item" style="max-width:100%">
                                            <div class="row">
                                              <div class="form-group" id="pgc-atr<?php echo $producto_grupo_color->idpgrc;?>">
                                                <input type="hidden" value="" id="delete-color<?php echo $producto_grupo_color->idpgrc;?>">
                                                <?php $count=0;
                                                for ($ap=0; $ap < count($atributos_producto) ; $ap++){ $atributo=$atributos_producto[$ap]; $count++; ?>
                                                <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
                                                <div class="col-sm-4 col-xs-12">
                                                  <div class="input-group">
                                                    <div class="form-control form-control-sm textarea disabled"><?php echo $atributo->valor;?></div>
                                                  </div>
                                                </div>
                                                <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
                                                <?php } ?>
                                                <?php for($ap=0; $ap < count($atributos_grupo) ; $ap++){ $atributo=$atributos_grupo[$ap]; $count++; ?>
                                                <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
                                                <div class="col-sm-4 col-xs-12">
                                                  <div class="input-group">
                                                    <div class="form-control form-control-sm textarea disabled"><?php echo $atributo->valor;?></div>
                                                  </div>
                                                </div>
                                                <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
                                                <?php } ?>
                                                <?php $atributos=$this->M_producto_grupo_color_atributo->get_atributo("pgca.idpgrc",$producto_grupo_color->idpgrc);
                                                for ($k=0; $k < count($atributos) ; $k++){ $atributo=$atributos[$k]; $count++;?>
                                                <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
                                                <div class="col-sm-4 col-xs-12">
                                                  <div class="input-group">
                                                    <textarea id="atr<?php echo rand(1,9999).$atributo->idpgca;?>" maxlength="150" class="form-control form-control-sm" data-pgca="<?php echo $atributo->idpgca;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-typele="db" data-type="color" placeholder="<?php echo $atributo->atributo;?>"><?php echo $atributo->valor;?></textarea>
                                                    <?php $helpa='title="'.$atributo->atributo.'" data-content="Ingrese un contenido alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';?>
                                                    <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$helpa; ?>><i class="fa fa-info-circle"></i></span>
                                                    <a href="javascript:" class="input-group-addon form-control-sm btn-default eliminar_atributo"><i class="fa fa-trash-o"></i></a>
                                                  </div>
                                                </div>
                                                <span <?php if(($count)%2==0){?> class='clearfix'<?php }?>></span>
                                                <?php } ?>
                                              </div>
                                            </div>
                                            <div class="row text-right" style="padding-right:15px;">
                                              <?php if(count($grupo_color)>1){?>
                                              <button type="button" class="btn btn-inverse-info btn-mini waves-effect waves-light atributos" data-pg="<?php echo $grupo->idpgr;?>"  data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="color"><i class="fa fa-plus"></i><span class="m-l-10">Atributo</span></button>
                                              <?php } ?>
                                              <?php if(count($grupo_color)>1){?>
                                              <button type="button" class="btn btn-inverse-info btn-mini waves-effect waves-light save_producto_grupo_color_atr" data-pg="<?php echo $grupo->idpgr;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="color"><i class="icon-floppy-o"></i><span class="m-l-10">Guardar</span></button>
                                              <?php } ?>
                                            </div>
                                          </div>
                                        <!--end atributos color-->
                                        </div><br>
                                      </div>
                                    </div>
                                  </div>
                                <?php }//end if ?>
                              <?php }//end for ?>
                            <?php }//end if ?>
                            </div>
                          </div>
                        </div>
                        <div class="text-right">
                          <button type="button" class="btn btn-primary btn-mini waves-effect waves-light colores"  data-pg="<?php echo $grupo->idpgr;?>" data-type="new">
                            <i class="fa fa-plus"></i><span class="m-l-10">adicionar Color</span>
                          </button>
                        </div>
                      </div>
                      <!--end colores grupo-->
                  <?php //}// end if?>
                  <br>
                </div>
              </div>
            </div>
            </div><!--end accordios panel-->
            <?php }// end for?>
          <?php }// en if ?>
        </div><!--end accordion-->
      </div><!--end form-->
    </div><!--end row-->
  </div><!--end list-->
</div>
<br>
<div class="row text-right" style="padding-right:15px;">
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light grupos" data-type="new">
    <i class="fa fa-plus"></i><span class="m-l-10">adicionar Categoria</span>
  </button>
</div>
<script>$(function(){ var icons = { header: "fa fa-caret-right", activeHeader: "fa fa-caret-down" }; });
  $('[data-toggle="popover"]').popover({html:true});
  $(".config_producto<?php echo $rand;?>").config_producto();
  $("a.material").material();
  $("a.producto_materiales").producto_materiales();




  if(esmovil()){$(".g-table-responsive table-responsive").attr('class','g-table-responsive table-responsive');}






  $("a.pila_producto_proceso").pila_producto_proceso();
  $("a.new_producto_proceso").new_producto_proceso(); 
  $("button.confirmar_producto_proceso").confirmar_producto_proceso();
  $("img.g-img-thumbnail").click(function(){ view_imagen($(this));});
  $(".configuracion_producto").click(function(){ $(this).configuracion_producto();});
  $("a.categoria").categoria();
  $("a.produccion").click(function(){ $(this).produccion();});
  $("button.refresh_producto_grupo").refresh_producto_grupo();
  $("button.producto_grupo_color_seg").producto_grupo_color_seg();
  $("button.grupos").grupos();
  $("button.confirm_producto_grupo").confirm_producto_grupo();
  $("button.colores").colores();$("button.refresh_grupo_color").refresh_grupo_color();
  $("button.confirm_grupo_color").confirm_grupo_color();
  
  
  $("button.confirmar_material").confirmar_material();
  $("button.atributos").atributos();
  $("button.save_producto_grupo_atr").save_producto_grupo_atr();
  $("a.producto_fotografias").producto_fotografias();
  $("a.new_fotografias").new_fotografias();
  $("button.confirmar_fotografia").confirmar_fotografia();
  $("a.producto_piezas").producto_piezas();
  $("a.new_pieza").new_pieza();
  $("button.confirmar_pieza").confirmar_pieza();
  $("button.save_producto_grupo_color_atr").save_producto_grupo_color_atr();
  $("a.eliminar_atributo").eliminar_atributo();
</script>