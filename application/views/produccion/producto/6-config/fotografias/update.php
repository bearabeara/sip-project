<?php
	$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>, puede seleccionar <strong>hasta 10 imágenes</strong>"';
	$help3='title="Descripción" data-content="Ingrese un descripcion alfanumerica hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>

<div class="list-group">
		  <div class="list-group-item" style="max-width:100%">
		  	<div class="row">
				<div class="form-group">
					<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Fotografía:</label>
					<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<input class='form-control form-control-sm'type="file" id="imgs">
							<span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-3 col-xs-12 col-form-label form-control-label">Descripción:</label>
					<div class="col-sm-9 col-xs-12">
						<div class="input-group">
							<textarea class='form-control form-control-sm' id="des_img" placeholder='Descripción de la(s) imagen(es)' maxlength="150"><?php echo $fotografia->descripcion;?></textarea>
							<span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
<!--<div onClick="this.contentEditable='true';">
    Div editable
</div>-->
<script>$('[data-toggle="popover"]').popover({html:true});Onfocus("imgs");</script>