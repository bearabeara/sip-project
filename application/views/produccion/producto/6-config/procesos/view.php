
<div class="table-responsive" id="datos-producto-proceso">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">#Item</th>
				<th width="90%">Proceso</th>
				<th width="5%"></th>
			</tr>
		</thead>
		<tbody>
		<?php 
		if($type_save=="pgcpr-modal"){
			for($i=0; $i < count($procesos); $i++){ $proceso=$procesos[$i]; 
				$control=null;
				$control=$this->lib->search_elemento_2n($producto_grupo_color_procesos,"idppr",$proceso->idppr,'idpgrc',$pgc);
				//$control2=$this->lib->search_elemento_2n($producto_grupo_procesos,"idppr",$proceso->idppr,'idpgr',$pg);
				if($control!=null){
		?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $proceso->nombre; ?></td>
				<td>
			<?php if($type_save=="pgcpr-modal"){ ?>
					<div class="g-control-accordion" style="width:30px;">
				      	<button class="confirmar_producto_proceso" data-ppr="<?php echo $proceso->idppr;?>" data-type-save="<?php echo $type_save;?>"<?php if(isset($pgc)){?> data-pgc="<?php echo $pgc;?>"<?php }?><?php if(isset($pg)){?> data-pg="<?php echo $pg;?>"<?php }?>><i class="icon-trashcan2"></i></button>
					</div>
			<?php } ?>
				</td>
			</tr>
		<?php 		
				}//enf if
			}// end for
		}// end if
		?>
	<?php if($type_save=="pgpr-modal" || $type_save=="pgcpr-modal"){
			for($i=0; $i < count($procesos); $i++){ $proceso=$procesos[$i]; 
				$control=$this->lib->search_elemento_2n($producto_grupo_procesos,"idppr",$proceso->idppr,'idpgr',$pg);
				if($control!=null){
	?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $proceso->nombre; ?></td>
				<td>
			<?php if($type_save=="pgpr-modal"){ ?>
					<div class="g-control-accordion" style="width:30px;">
				      	<button class="confirmar_producto_proceso" data-ppr="<?php echo $proceso->idppr;?>" data-type-save="<?php echo $type_save;?>"<?php if(isset($pg)){?> data-pg="<?php echo $pg;?>"<?php }?>><i class="icon-trashcan2"></i></button>
					</div>
			<?php } ?>
				</td>
			</tr>
		<?php 	}//enf if
			}// end for
		}//end if
		?>
	<?php if($type_save=="ppr-modal" || $type_save=="pgpr-modal" || $type_save=="pgcpr-modal"){
			for($i=0; $i < count($procesos); $i++){ $proceso=$procesos[$i]; 
				$control=$this->lib->search_elemento($producto_grupo_procesos,"idppr",$proceso->idppr);
				$control2=$this->lib->search_elemento($producto_grupo_color_procesos,"idppr",$proceso->idppr);
				if($control==null && $control2==null){
		?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><?php echo $proceso->nombre; ?></td>
				<td>
			<?php if($type_save=="ppr-modal"){ ?>
					<div class="g-control-accordion" style="width:30px;">
				      	<button class="confirmar_producto_proceso" data-ppr="<?php echo $proceso->idppr;?>" data-type-save="<?php echo $type_save;?>"><i class="icon-trashcan2"></i></button>
					</div>
			<?php } ?>
				</td>
			</tr>
		<?php 	}//enf if
			}//end for
		}//end if
		?>
		</tbody>
	</table>
</div>
<div class="row text-right" style="padding-right:15px;">
  <button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_producto_proceso" data-type-save="<?php echo $type_save;?>"<?php if(isset($pg)){?> data-pg="<?php echo $pg;?>"<?php }?><?php if(isset($pgc)){?> data-pgc="<?php echo $pgc;?>"<?php }?>>
  	<i class="fa fa-plus"></i><span class="m-l-10">Adicionar proceso</span>
   </button>
</div>
<script>$("button.new_producto_proceso").new_producto_proceso();$("button.confirmar_producto_proceso").confirmar_producto_proceso();</script>