<?php 
	$help1='title="Procesos" data-content="Ingrese el nombre del nuevo proceso de 3 a 100 caracteres solo se aceptan los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-xs-12">
				<form id="f-save-proceso" <?php if(isset($type)){?> data-type="<?php echo $type;?>" <?php }?>>
					<div class="input-group">
						<input class="form-control form-control-xs color-class" id="n3_pr" type="text" placeholder='Nombre de proceso' minlength="3" maxlength="100" <?php if(isset($type_save)){?> data-type-save="<?php echo $type_save;?>" <?php }?>>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true}); $("form#f-save-proceso").submit(function(){ $(this).save_proceso();event.preventDefault(); });$('input.color-class').maxlength(); Onfocus("n3_pr");
</script>