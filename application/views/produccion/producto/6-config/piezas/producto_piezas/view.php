<?php $url=base_url().'libraries/img/';?>
    <div class="row">
      <div class="col-xs-12">
        <span class="g-control-accordion">
          <button class="producto_piezas" data-type="<?php echo $type;?>"  <?php if(isset($pgc)){ ?>data-pgc="<?php echo $pgc;?>"<?php }?>><i class="fa fa-refresh"></i></button>
          <button class="<?php if($type=="ppi-modal"){ ?>producto_seg<?php }else{?>producto_grupo_color_seg<?php } ?>" data-p="<?php echo $producto->idp;?>" data-type="<?php echo $type;?>" <?php if(isset($pgc)){ ?>data-pgc="<?php echo $pgc;?>"<?php }?>><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover" id="datos-pgc" <?php if(isset($pgc)){ ?>data-pgc="<?php echo $pgc;?>"<?php }?>>
	<?php $count=1;?>
	<?php if($type=="pgcpi-modal"){?>
		<thead>
			<tr><th colspan="8" class="text-center">PIEZAS EN EL COLOR DEL PRODUCTO</th></tr>
			<tr>
				<th class="g-thumbnail-modal"><div class="g-img-modal"></div></th>
				<th width="30%">Nombre</th>
				<th width="15%">Dimensiones</th>
				<th width="10%">Cantidad</th>
				<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>Mater.</th>
				<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>Repuj. sello</th>
				<th width="45%">Descripcion</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
	<?php if(count($color_piezas)>0){ ?>
		<?php for ($i=0; $i < count($color_piezas) ; $i++) { $pieza=$color_piezas[$i]; 
				$img="sistema/miniatura/default.jpg";
				if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/miniatura/".$pieza->fotografia; }
				$material=$this->M_material_item->get_material("m.idm",$pieza->idm);
				$img_m="/sistema/miniatura/default.jpg";
				if(!empty($material)){
              		if($material[0]->fotografia!="" && $material[0]->fotografia!=NULL){ $img_m="materiales/miniatura/".$material[0]->fotografia;}
				}
		?>
					<tr>
						<td class="item-relative"><div class="g-thumbnail-modal"></div><div id="item"><?php echo $count++;?></div><a href="javascript:" onclick="view_img($(this))">
							<?php $desc="<br>"; if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc=$pieza->descripcion;} ?>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
						</a></td>
						
						<td width="30%"><?php echo $pieza->nombre;?></td>
						<td width="15%"><?php echo $pieza->largo."[cm.] largo x ".$pieza->ancho."[cm.] ancho";?></td>
						<td width="10%"><?php echo $pieza->cantidad." [unid.]";?></td>
						<td><div class="img-thumbnail-45"></div>
							<img src="<?php echo $url.$img_m;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-title="<?php echo $material[0]->nombre;?>" data-desc="<?php echo '<br>';?>">
						</td>
						<td><div class="img-thumbnail-45"></div>
						<?php if($pieza->idrs!="0" && $pieza->idrs!="" && $pieza->idrs!=null){ ?>
						<?php $rs=$this->M_repujado_sello->get($pieza->idrs);?>
						<?php if(!empty($rs)){
								$img="sistema/miniatura/default.jpg";
								if($rs[0]->imagen!="" && $rs[0]->imagen!=NULL){
									$img="repujados_sellos/miniatura/".$rs[0]->imagen;
								}
								$tipo="";if($rs[0]->tipo==0){ $tipo="Repujado: ";}else{ $tipo="Sello: "; }
						?>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tipo.$rs[0]->nombre;?>" data-desc="<?php echo '<br>';?>">
							<?php } ?>
						 <?php } ?>
						</td>
						<td width="45%"><?php echo $pieza->descripcion;?></td>
						<td style="padding: 0px;" width="60px">
							<div class="g-control-accordion" style="width:60px;">
								<button class="configurar_pieza" data-pgcpi="<?php echo $pieza->idpgcpi;?>" data-type="<?php echo $type;?>"><i class="fa fa-cog"></i></button>
								<button class="confirmar_pieza" data-pgcpi="<?php echo $pieza->idpgcpi;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
							</div>
						</td>
					</tr>
			<?php } ?>
	<?php }else{
		echo "<tr><td colspan='8' class='text-center'><h4>0 piezas en el color del producto.</h4></td></tr>";
	}?>
		</tbody>
	<?php } ?>
			<thead>
				<tr><th colspan="8" class="text-center">PIEZAS EN EL PRODUCTO</th></tr>
			<?php if($type=="ppi-modal"){?>
				<tr>
					<th class="img-thumbnail-50"><div class="img-thumbnail-50"></div></th>
					<th width="30%">Nombre</th>
					<th width="15%">Dimensiones</th>
					<th width="10%">Cantidad</th>
					<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>Mater.</th>
					<th class="img-thumbnail-45"><div class="img-thumbnail-45"></div>Repuj. sello</th>
					<th width="45%">Descripcion</th>
					<th></th>
				</tr>
			<?php } ?>
			</thead>
		<tbody>
<?php for ($i=0; $i < count($producto_piezas); $i++){ $pieza=$producto_piezas[$i]; 
		$img="sistema/miniatura/default.jpg";
		if($pieza->fotografia!="" && $pieza->fotografia!=NULL){ $img="piezas/miniatura/".$pieza->fotografia;}
		$material=$this->M_material_item->get_material("m.idm",$pieza->idm);
		$img_m="/sistema/miniatura/default.jpg";
		if(!empty($material)){
			if($material[0]->fotografia!="" && $material[0]->fotografia!=NULL){ $img_m="materiales/miniatura/".$material[0]->fotografia;}
		}
?>
			<tr>
				<td class="item-relative"><div class="img-thumbnail-50"></div><div id="item"><?php echo $count++;?></div>
					<?php $desc="<br>"; if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc=$pieza->descripcion;} ?>
					<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
				</td>
				<td width="30%"><?php echo $pieza->nombre;?></td>
				<td width="15%"><?php echo $pieza->largo."[cm.] largo x ".$pieza->ancho."[cm.] ancho";?></td>
				<td width="10%"><?php echo $pieza->cantidad." [unid.]";?></td>
				<td><div class="img-thumbnail-45"></div>
					<img src="<?php echo $url.$img_m;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-title="<?php echo $material[0]->nombre;?>" data-desc="<?php echo '<br>';?>">
				</td>
				<td><div class="img-thumbnail-45"></div>
				<?php if($pieza->idrs!="0" && $pieza->idrs!="" && $pieza->idrs!=null){ ?>
				<?php $rs=$this->M_repujado_sello->get($pieza->idrs);?>
				<?php if(!empty($rs)){
						$img="sistema/miniatura/default.jpg";
						if($rs[0]->imagen!="" && $rs[0]->imagen!=NULL){
							$img="repujados_sellos/miniatura/".$rs[0]->imagen;
						}
						$tipo="";if($rs[0]->tipo==0){ $tipo="Repujado: ";}else{ $tipo="Sello: "; }
				?>
					<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tipo.$rs[0]->nombre;?>" data-desc="<?php echo '<br>';?>">
					<?php } ?>
				 <?php } ?>
				</td>
				<td width="45%" <?php if($type=="pgcpi-modal"){?> colspan="2" <?php }?>><?php echo $pieza->descripcion;?></td>
			<?php if($type=="ppi-modal"){?>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="configurar_pieza" data-ppi="<?php echo $pieza->idppi;?>" data-type="<?php echo $type;?>"><i class="fa fa-cog"></i></button>
						<button class="confirmar_pieza" data-ppi="<?php echo $pieza->idppi;?>" data-type="<?php echo $type;?>"><i class="icon-trashcan2"></i></button>
					</div>
				</td>
			<?php } ?>
			</tr>
	<?php } ?>
		</tbody>
	</table>
</div>
<div class="row text-right" style="padding-right:15px;">
  	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_pieza" data-type="<?php echo $type;?>">
  		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar pieza</span>
    </button>
</div>
<script>$("button.producto_piezas").producto_piezas();$("button.new_pieza").new_pieza();
<?php if($type=="ppi-modal"){ ?> $("button.producto_seg").producto_seg();<?php }else{?>$("button.producto_grupo_color_seg").producto_grupo_color_seg();<?php } ?>
$("img.g-img-thumbnail").visor();$("button.configurar_pieza").configurar_pieza();$("button.confirmar_pieza").confirmar_pieza();
</script>
