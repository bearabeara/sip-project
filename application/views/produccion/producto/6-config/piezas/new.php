<?php
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>."';
	$help2='title="Nombre" data-content="Ingrese un nombre de la pieza en formato alfanumerico de 2 a 100 caracteres <b>puede incluir espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help3='title="Alto" data-content="Ingrese la altura maxima de la pieza en centímetros, se acepta un valor maximo de 9999.999"';
	$help4='title="Ancho" data-content="Ingrese ancho maximo de la pieza en centímetros, se acepta un valor maximo de 9999.999"';
	$help5='title="Cantidad" data-content="Ingrese la cantidad requerida de pieza en el producto, se aceptan valores entre los rangos de 0 a 99"';
	$help6='title="Repujado o sello" data-content="Si la pieza leva algun repujado o sello seleccione una repujado o sello."';
	$help7='title="Material" data-content="Seleccione el material de la pieza."';
	$help8='title="Descripción" data-content="Ingrese un descripcion alfanumerica hasta 200 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-6 offset-sm-6 col-xs-12 text-xs-right"><span class="text-danger">(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		  	<div class="row">
				<div class="form-group">
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Fotografía:</label>
					<div class="col-sm-10 col-xs-12">
						<div class="input-group">
							<input class='form-control form-control-xs' type="file" id="img_pi">
							<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><span class="text-danger">(*)</span>Nombre:</label>
					<div class="col-sm-10 col-xs-12">
						<form class="save_pieza" data-type="<?php echo $type;?>">
							<div class="input-group">
								<input type="text" class='form-control form-control-xs' id="nom_pi" placeholder="Nombre de la pieza" maxlength="100">
								<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Largo:</label>
					<div class="col-sm-4 col-xs-12">
					<form class="save_pieza" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="alt_pi" placeholder="Alto de la pieza" value="0" min="0" step="any" max="9999.999">
							<span class="input-group-addon unid form-control-sm">cm.</span>
							<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
					</div><i class='clearfix visible-xs-block'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Ancho:</label>
					<div class="col-sm-4 col-xs-12">
					<form class="save_pieza" data-type="<?php echo $type;?>">
						<div class="input-group">
							<div class="input-group">
								<input class='form-control form-control-xs' type="number" id="anc_pi" placeholder="Ancho de la pieza" value="0" min="0" step="any" max="9999.999">
								<span class="input-group-addon unid form-control-sm">cm.</span>
								<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</div>
					</form>
					</div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Cantidad:</label>
					<div class="col-sm-4 col-xs-12">
					<form class="save_pieza" data-type="<?php echo $type;?>">
						<div class="input-group">
							<input class='form-control form-control-xs' type="number" id="can_pi" placeholder="Cantidad de piezas en el producto" value="0" min="0" max="99">
							<span class="input-group-addon unid form-control-sm">unid.</span>
							<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
					</div><i class='clearfix visible-xs-block'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Repujado o Sello:</label>
					<div class="col-sm-4 col-xs-12">
						<div class="input-group">
							<select class='form-control form-control-xs' id="rs_pi">
								<option value="">Ninguno</option>
							<?php for ($i=0; $i < count($repujados_sellos) ; $i++) { 
									$tipo="";
									if($repujados_sellos[$i]->tipo==0){
										$tipo="[Repujado]";
									}else{
										$tipo="[Sello]";
									}
							?>
								<option value="<?php echo $repujados_sellos[$i]->idrs;?>"><?php echo $tipo." ".$repujados_sellos[$i]->nombre;?></option>
							<?php }?>
							</select>
							<a href="javascript:" title="Nuevo repujado o sellos" class="input-group-addon form-control-xs new_repujado_sello" data-destino="rs_pi"; data-type="<?php echo 'rs';?>"><i class="fa fa-plus"></i></a>
							<a href="javascript:" title="Ver repujado y sellos" class="input-group-addon form-control-xs view_repujados_sellos" data-destino="rs_pi" data-type="<?php echo 'rs-modal';?>"><i class="fa fa-cog"></i></a>
							<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div><i class='clearfix'></i>
					<div class="form-group">
						<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label">Material:</label></div>
						<div class="col-sm-4 col-xs-12">
							<div class="input-group">
								<div class="form-control form-control-sm textarea" id="n_pi_mat" style="min-height: 50px; height: auto; color: rgba(85, 89, 92, .71);">
										<!--<div class="card-group" style="text-align: center !important;">
											<div class="card img-thumbnail-35">
												<img class="card-img-top" src="http://127.0.0.1/killa.v12/libraries/img/materiales/miniatura/845-5995.JPG" alt="image" data-title="" data-desc="" style="vertical-align: top;cursor: zoom-in;">
											</div>
											<div class="card card-padding-0" style="vertical-align: middle !important;">
												<div class="a-card-block">
													<p class="a-card-text" style="line-height: .8 !important;">
														<small style="color: #069bcc !important;"><?php echo "Boton de golpe sombrilla"; ?></small>
													</p>
												</div>
											</div>
											<div class="card card-padding-0 card-30 card-middle">
												<div class="card-block" style="padding: 9px 0px 5px 12px !important;">
													<div class="g-control-accordion" style="width:30px;">
														<button class="drop_material_pieza" data-type="pieza_material" data-padre="card-group" data-type-padre="atr"><i class="icon-trashcan2"></i></button>
													</div>
												</div>
											</div>
											<script>$("img.card-img-top").visor();$("button.drop_material_pieza").drop_elemento();</script>
										</div>-->
								</div>
								<span class="input-group-addon form-control-sm new_pieza_material" data-container="n_pi_mat" data-type="pim"><i class='fa fa-plus'></i></span>
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</div>
					</div><i class='clearfix visible-xs-block'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"></label>
					<div class="col-sm-4 col-xs-12"></div><i class='clearfix'></i>
					<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Descripción:</label>
					<div class="col-sm-10 col-xs-12">
						<div class="input-group">
							<textarea class='form-control form-control-xs' id="des_pi" placeholder='Descripción de la pieza' maxlength="200" rows="3"></textarea>
							<span class="input-group-addon form-control-xs btn-default" <?php echo $popover.$help8;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
<script>$('[data-toggle="popover"]').popover({html:true}); Onfocus("nom_pi");$("a.new_repujado_sello").new_repujado_sello();$("a.view_repujados_sellos").view_repujados_sellos();$("form.save_pieza").submit(function(e){$(this).save_pieza();e.preventDefault();});
$("span.new_pieza_material").click(function(){$(this).material();});
</script>