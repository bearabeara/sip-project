<?php $url=base_url().'libraries/img/';?>
    <div class="row">
      <div class="col-xs-12">
        <span class="g-control-accordion">
          <button class="view_repujados_sellos" data-type="<?php echo $type;?>" data-destino="<?php echo $destino;?>"><i class="fa fa-refresh"></i></button>
        </span>
      </div>
    </div>
<div class="table-responsive">
	<table class="table table-bordered table-hover" id="datos-pgc" <?php if(isset($idpgrc)){ ?>data-pgc="<?php echo $idpgrc;?>"<?php }?>>
		<thead>
			<tr><th colspan="4" class="text-center">REPUJADOS Y SELLOS EN EL PRODUCTO</th></tr>
			<tr>
				<th class="img-thumbnail-50"><div class="img-thumbnail-50"></div></th>
				<th width="20%">Tipo</th>
				<th width="80%">Nombre</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?php for ($i=0; $i < count($repujados_sellos) ; $i++) { $rs=$repujados_sellos[$i]; 
		$img="sistema/miniatura/default.jpg";
		if($rs->imagen!="" && $rs->imagen!=NULL){ $img="repujados_sellos/miniatura/".$rs->imagen; }
		$tipo="";if($rs->tipo==0){ $tipo="Repujado: ";}else{ $tipo="Sello: "; }
?>
			<tr>
				<td class="item-relative"><div class="img-thumbnail-50"></div><div id="item"><?php echo $i+1;?></div>
					<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $tipo.$rs->nombre;?>" data-desc="<?php echo '<br>';?>">
				</td>
				
				<td width="20%"><?php if($rs->tipo=="0"){ echo "Repujado";}else{ echo "Sello";}?></td>
				<td width="80%"><?php echo $rs->nombre;?></td>
				<td style="padding: 0px;" width="60px">
					<div class="g-control-accordion" style="width:60px;">
						<button class="config_repujado_sello" data-rs="<?php echo $rs->idrs;?>" data-type="<?php echo $type;?>" data-destino="<?php echo $destino;?>"><i class="fa fa-cog"></i></button>
						<button class="confirmar_repujado_sello" data-rs="<?php echo $rs->idrs;?>" data-type="<?php echo $type;?>" data-destino="<?php echo $destino;?>"><i class="icon-trashcan2"></i></button>
					</div>
				</td>
			</tr>
	<?php } ?>
		</tbody>
	</table>
</div>
<div class="row text-right" style="padding-right:15px;">
  	<button type="button" class="btn btn-primary btn-mini waves-effect waves-light new_repujado_sello" data-type="<?php echo $type;?>" data-destino="<?php echo $destino;?>">
		<i class="fa fa-plus"></i><span class="m-l-10">Adicionar repujado o sello</span>
   	</button>
</div>
<script>$("button.view_repujados_sellos").view_repujados_sellos();$("button.config_repujado_sello").config_repujado_sello();$("button.new_repujado_sello").new_repujado_sello();$("button.confirmar_repujado_sello").confirmar_repujado_sello();
$("img.g-img-thumbnail").visor();
</script>
