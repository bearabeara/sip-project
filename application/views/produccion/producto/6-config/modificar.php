<?php
  $url=base_url().'libraries/img/productos/miniatura/'; $img="default.png";
  $url_mat=base_url().'libraries/img/materiales/miniatura/';
  $url_img=base_url().'libraries/img/';
  $help0='title="Adicionar imagen" data-content="Ingresar una nueva imagen o fotografía general en el contenedor del producto."';
  $help01='title="Contenedor general" data-content="Contiene todas las imágenes o fotografías generales del producto."';
  $help05='title="Contenedor general" data-content="Contiene todos los materiales generales del producto."';
  $help06='title="Adicionar material" data-content="Adicione materiales en el contenedor general del producto."';
  $help07='title="Contenedor general" data-content="Contiene todas las piezas generales del producto."';
  $help08='title="Adicionar pieza" data-content="Adicione piezas en el contenedor general de piezas del producto."';
  $help09='title="Contenedor general" data-content="Contiene todos los procesos generales del producto."';
  $help10='title="Adicionar proceso" data-content="Adicione procesos en el contenedor general de procesos del producto."';
  $help1='title="Código" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $help2='title="Código anterior" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $help3='title="Nombre de producto" data-content="Ingrese un nombre alfanumerico de 2 a 90 caracteres <b>puede incluir espacios</b>, solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $help4='title="Fecha de creación" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
  $help5='title="Observaciónes" data-content="Ingrese un descripcion alfanumerica hasta 500 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';
  $popover2='data-toggle="popover" data-placement="right" data-trigger="hover"';
  $rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item"><a href="javascript:" class="nav-link active config_producto<?php echo $rand;?>" data-p='<?php echo $producto->idp;?>'>Producto</a></li>
  <li class="nav-item"><a href="javascript:" class="nav-link categoria" data-p="<?php echo $producto->idp;?>">Categorias</a></li>
  <li class="nav-item"><a href="javascript:" class="nav-link produccion" data-p="<?php echo $producto->idp;?>">Producción</a></li>
</ul>
<div class="list-group" id="datos-producto" data-p="<?php echo $producto->idp;?>">
  <div class="list-group-item" style="max-width:100%">
    <div class="row">
      <div class="col-xs-12">
        <span class="g-control-accordion">
          <button class="config_producto<?php echo $rand;?>" data-p='<?php echo $producto->idp;?>'><i class="fa fa-refresh"></i></button>
          <button class="producto_seg" data-p="<?php echo $producto->idp;?>" data-type="producto"><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
    </div>
  </div>
  <div class="list-group-item" style="max-width:100%">
    <div class="row">
      <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
        <div class="table-responsive g-table-responsive">
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                <div class="item help" <?php echo $popover2.$help05;?>>
                  <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                </div>
                <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_materiales" data-type="pm-modal"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
              </td>
              <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                <div class="item help" <?php echo $popover2.$help06;?>>
                  <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                </div>
                <div class="img-thumbnail-45"></div><a href"javascript:" class="material" data-type="pm"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
              </td>
              <?php for($m=0; $m < count($materiales) ; $m++){ $material=$materiales[$m]; 
                      $archivo="";$desc="<br>";
                      $desc="Cantidad ".$material->cantidad;
                      $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                      if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                      if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                      $tit=$material->nombre;
                      $img_mat="sistema/miniatura/default.jpg";
                      if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
              ?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                  <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);">
                    <button type="button" class="close confirmar_material" title="Eliminar Imagen"data-pm="<?php echo $material->idpm;?>" data-type="pm" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                  </div>
                  <div class="img-thumbnail-45">
                    <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-id="<?php echo $material->idmi;?>" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                  </div>
                </td>
                <?php } ?>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
          <div class="table-responsive g-table-responsive">
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover2.$help01;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_fotografias" data-type="pf-modal"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover2.$help0;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="new_fotografias" data-type="pf"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <?php for ($im=0; $im < count($imagenes) ; $im++){  ?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_fotografia" title="Eliminar Imagen" data-pi="<?php echo $imagenes[$im]->idpi;?>" data-type="pf" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                  <div class="img-thumbnail-45">
                  <?php $desc="<br>"; if($imagenes[$im]->descripcion!="" && $imagenes[$im]->descripcion!=null){ $desc=$imagenes[$im]->descripcion;}?>
                    <img src="<?php echo $url.$imagenes[$im]->archivo;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-id="<?php echo $imagenes[$im]->idpi;?>" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>">
                  </div>
                </td>
                <?php }?>
              </tr>
            </table>
          </div>
        </div>
        <i class="clearfix visible-lg-block"></i>
        <div class="col-lg-6 col-sm-12">
          <div class="table-responsive g-table-responsive">
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover2.$help07;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_piezas" data-type="ppi-modal"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover2.$help08;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="new_pieza" data-type="ppi"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <?php for ($m=0; $m < count($piezas); $m++) { $pieza=$piezas[$m]; 
                  if($pieza->fotografia!="" && $pieza->fotografia!=null){
                    $archivo=$url_img."piezas/miniatura/".$pieza->fotografia;
                  }else{
                    $archivo=$url_img."/sistema/miniatura/default.jpg";
                  }
                  $desc="Cantidad ".$pieza->cantidad." unid., largo ".$pieza->largo."cm., ancho ".$pieza->ancho."cm.";
                  if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc.=", ".$pieza->descripcion;}
                  ?>
                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                    <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_pieza" title="Eliminar Imagen" data-ppi="<?php echo $pieza->idppi;?>" data-type="ppi" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                    <div class="img-thumbnail-45">
                      <img src="<?php echo $archivo;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45" data-id="<?php echo $pieza->idppi;?>" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
                    </div>
                  </td>
                  <?php }?>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-lg-6 col-sm-12">
            <div class="table-responsive g-table-responsive">
              <table cellspacing="0" cellpadding="0">
                <tr>
                  <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                    <div class="item help" <?php echo $popover2.$help09;?>>
                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                    </div>
                    <div class="img-thumbnail-45"></div><a href="javascript:" data-type-save="ppr-modal" class="pila_producto_proceso"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                  </td>
                  <td class="img-thumbnail-40" style="border: 0px; padding: 0px 2px;">
                    <div class="item help" <?php echo $popover2.$help10;?>>
                      <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                    </div>
                    <div class="img-thumbnail-45"></div><a href="javascript:" class="new_producto_proceso" data-type-save="ppr"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                  </td>
                  <?php for($m=0; $m < count($producto_procesos); $m++){ $producto_proceso=$producto_procesos[$m]; 
                    $control=$this->lib->search_elemento($producto_grupo_procesos,"idppr",$producto_proceso->idppr);
                    $control2=$this->lib->search_elemento($producto_grupo_color_procesos,"idppr",$producto_proceso->idppr);
                    if($control==null && $control2==null){
                      ?>
                      <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;">
                        <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);">
                          <button type="button" class="close confirmar_producto_proceso" title="Eliminar Imagen" data-ppr="<?php echo $producto_proceso->idppr;?>" data-type-save="ppr" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="img-thumbnail img-thumbnail-45" style="height: 45px; vertical-align: middle; overflow: hidden; line-height: .9;" title="<?php echo $producto_proceso->nombre;?>">
                          <div style="height: 100%; display: table;">
                           <small style="font-size: 51%; vertical-align: middle; display: table-cell;"><?php echo $producto_proceso->nombre;?></small>
                         </div>
                       </div>
                     </td>
                     <?php }
                   }
                   ?>
                 </tr>
               </table>
             </div>
           </div>
           <i class="clearfix visible-lg-block"></i>
         </div>
       </div>
       <div class="list-group-item" style="max-width:100%">
        <div class="row"><div class="col-sm-6 offset-sm-6 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
        <div class="row">
          <div class="form-group">
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><span class='text-danger'>(*)</span>Código:</label>
            <div class="col-sm-4 col-xs-12">
              <form class="update_producto" data-p="<?php echo $producto->idp;?>">
                <div class="input-group">
                  <input class='form-control form-control-sm color-class' type="text" id="cod_1" placeholder='Código del producto' maxlength="20" minlength="2" value="<?php echo $producto->codigo;?>">
                  <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
                </div>
              </form>
              <script type="text/javascript">foco("cod_1");</script>
            </div>
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Fecha de creación:</label>
            <div class="col-sm-4 col-xs-12">
              <div class="input-group">
                <input class='form-control form-control-sm' type="date" id="fec" placeholder='2000-01-31'  value="<?php echo $producto->fecha_creacion;?>">
                <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$help4;?>><i class='fa fa-info-circle'></i></span>
              </div>
            </div>
          </div><i class='clearfix'></i>
          <div class="form-group">
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><span class='text-danger'>(*)</span>Nombre:</label>
            <div class="col-sm-10 col-xs-12">
              <form class="update_producto" data-p="<?php echo $producto->idp;?>">
                <div class="input-group">
                  <input class='form-control form-control-sm color-class' type="text" id="nom" placeholder='Nombre de Producto' maxlength="90" minlength="2" value="<?php echo $producto->nombre;?>">
                  <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$help3;?>><i class='fa fa-info-circle'></i></span>
                </div>
              </form>
            </div>

          </div><i class='clearfix'></i>
          <div class="form-group">
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Observaciónes:</label>
            <div class="col-sm-10 col-xs-12">
              <div class="input-group">
                <textarea id="obs" class="form-control form-control-sm color-class" placeholder="Observaciónes del producto" maxlength="500" minlength="0" minlength="0" rows="3"><?php echo $producto->observaciones;?></textarea>
                <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$help5;?>><i class='fa fa-info-circle'></i></span>
              </div>
            </div>
            <input type="hidden" value="" id="control-delete">
          </div>
          <i class='clearfix'></i>
          <div class="form-group">
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Código anterior:</label>
            <div class="col-sm-4 col-xs-12">
              <form class="update_producto" data-p="<?php echo $producto->idp;?>">
                <div class="input-group">
                <input class='form-control form-control-sm color-class' type="text" id="cod_2" placeholder='Código anterior' maxlength="20" minlength="0" value="<?php echo $producto->codigo_aux;?>">
                  <span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help2;?>><i class='fa fa-info-circle'></i></span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="list-group-item" style="max-width:100%">
        <div class="row">
          <div class="form-group" id="form_atr">
            <?php $atributos=$this->M_producto_atributo->get_atributo("pa.idp",$producto->idp);
            for($i=0; $i < count($atributos); $i++){ $atributo=$atributos[$i]; ?>
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
            <div class="col-sm-4 col-xs-12">
              <div class="input-group">
                <textarea id="atr<?php echo rand(1,9999).$atributo->idatr;?>" maxlength="150" minlength="0" class="form-control form-control-sm color-class" data-a='<?php echo $atributo->idatr;?>' data-typele="db" data-prod-atr="<?php echo $atributo->idpatr;?>" placeholder="<?php echo $atributo->atributo;?>"><?php echo $atributo->valor;?></textarea>
                <?php $helpa='title="'.$atributo->atributo.'" data-content="Ingrese un contenido alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';?>
                <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$helpa; ?>><i class="fa fa-info-circle"></i></span>
                <a href="javascript:" class="input-group-addon form-control-sm btn-default eliminar_atributo"><i class="fa fa-trash-o"></i></a>
              </div>
            </div>
            <span <?php if(($i+1)%2==0){?> class='clearfix'<?php }?>></span>
            <?php } ?>
          </div>
        </div>
        <div class="row text-right" style="padding-right:15px;">
          <button type="button" class="btn btn-primary btn-mini waves-effect waves-light atributos" data-type="producto">
            <i class="fa fa-plus"></i><span class="m-l-10">Adicionar atributo</span>
          </button>
        </div>
      </div>    
    </div>
    <script>$('[data-toggle="popover"]').popover({html:true});
      $(".config_producto<?php echo $rand;?>").config_producto();
      $(".material").material();
      
      
      
      
      
      $("a.categoria").categoria(); $("a.produccion").click(function(){ $(this).produccion();});$("a.pila_producto_proceso").pila_producto_proceso();
      $("a.new_producto_proceso").new_producto_proceso(); $("button.confirmar_producto_proceso").confirmar_producto_proceso();$("img.g-img-thumbnail").visor();$(".color-class").maxlength();
      $("button.producto_seg").producto_seg();$("a.producto_materiales").producto_materiales();
      $("a.producto_fotografias").producto_fotografias();$("button.confirmar_material").confirmar_material();
      $("button.confirmar_fotografia").confirmar_fotografia();$("a.new_fotografias").new_fotografias();$("a.producto_piezas").producto_piezas();$("button.confirmar_pieza").confirmar_pieza();
      $("a.new_pieza").new_pieza();$("button.atributos").atributos();$("a.eliminar_atributo").eliminar_atributo();$("form.update_producto").submit(function(e){$(this).update_producto();e.preventDefault();});
    </script>