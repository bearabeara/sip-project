<?php
  $help1='title="Código" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" id="search_3" style="max-width:100%">
		<table cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<td colspan="2">
					    <div class="input-group" style="margin:0px;">
					      <select class="form-control form-control-sm search_materiales" data-type="search" id="alm_3" style="margin:0px auto;">
					        <option value="">Seleccione...</option>
					            <?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i]; ?>
					            <option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
					          <?php } ?>
					      </select>
					      <span class="input-group-addon form-control-sm btn-default" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					    </div>
					</td>
				</tr>
				<tr>
					<td width="100%"><form class="search_materiales" data-type="search">
					<input id="s3_mat" type="search" class="form-control form-control-sm" placeholder="Material" <?php if($type=="pm-modal-change"){ ?> data-pm="<?php echo $idpm;?>"<?php } ?> data-type="<?php echo $type;?>" <?php if($type=="pgm" || $type=="pgm-modal"){ ?>data-pg="<?php echo $pg;?>"<?php } ?> <?php if(isset($pgm)){?>data-pgm="<?php echo $pgm;?>"<?php }?> <?php if($type=="pgcm" || $type=="pgcm-modal"){ ?>data-pgc="<?php echo $pgc;?>"<?php } ?> <?php if(isset($pgcm)){ ?>data-pgcm="<?php echo $pgcm;?>"<?php } ?> <?php if(isset($container)){?> data-container="<?php echo $container;?>"<?php }?>/></form></td>
					<td style="padding-left:4px;" class="hidden-sm">
						<?php 
							$search=json_encode(array('function'=>'search_materiales','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
							$all=json_encode(array('function'=>'search_materiales','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
							$this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);
						?>
						<?php //$this->load->view('estructura/botones/buscador2',['id' => '2','f_buscar'=>json_encode(array("funcion" => "search_materiales","atributos"=>"")),'f_ver'=>json_encode(array("funcion" => "all_materiales","atributos"=>""))]);?>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_4"></div>
</div>
<script>Onfocus("alm_3");$('[data-toggle="popover"]').popover({html:true});$(".search_materiales").search_materiales();</script>