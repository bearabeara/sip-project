<?php 
  $help='title="Observaciónes" data-content="Ingrese las observaciónes del material en formato alfanumérico hasta 200 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
  $popover='data-toggle="popover" data-placement="left" data-trigger="hover"';

	if(count($materiales)>0){
		$url=base_url().'libraries/img/'; ?>
	<div class="table-responsive g-table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<th class="img-thumbnail-45"><div class="img-thumbnail-45">#Item</div></th>
				<th width="width:45%">Nombre</th>
			<?php if($type!="pim" && $type!="pim-modal"){ ?><th style="width:20%">Cantidad</th><?php }?>
			<?php if($type!="pim" && $type!="pim-modal"){ ?><th style="width:35%">Observaciónes</th><?php }?>
				<th style="width:5%"></th>
			</thead>
			<tbody>
			<?php for($i=0; $i < count($materiales) ; $i++) { $material=$materiales[$i];
				$es_material=false;
				$msj="";$can_exit=0;$obs_exist=" ";//si estuvira asignado el material se guardan los datos;
					/*for ($j=0; $j < count($producto_materiales) ; $j++) { $pm=$producto_materiales[$j];
						if($pm->idm==$material->idm){
							$es_material=true; $msj="En uso el producto";$can_exit=$pm->cantidad;$obs_exit=$pm->observacion; 
							break;
						}
					}*/
				if(false){
					if(!$es_material){
						if(isset($j_pgm)){
							foreach ($j_pgm as $key => $pgm) {
								if($pgm->idm==$material->idm){
									$es_material=true; $msj="En uso en la categoría <strong>".$pgm->nombre_g."</strong>";$can_exit=$pgm->cantidad;$obs_exit=$pgm->observacion;
									break;
								}
							}
						}
					}
				}
				if(false){
					if(!$es_material){
						if(isset($j_pgcm)){
							foreach ($j_pgcm as $key => $pgcm) {
								if($pgcm->idm==$material->idm){
									$es_material=true; $msj="En uso en el color <strong>".$pgcm->nombre_c."</strong> de la categoría <strong>".$pgcm->nombre_g."</strong>";$can_exit=$pgcm->cantidad;$obs_exit=$pgcm->observacion;
									break;
								}
							}
						}
					}
				}
					$img="sistema/miniatura/default.jpg";
				    if($material->fotografia!="" && $material->fotografia!=NULL){
						$img="materiales/miniatura/".$material->fotografia;
					}
			?>
					<tr>
						<td><div id="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" width='100%' class='img-thumbnail g-img-thumbnail img-thumbnail-45' data-title="<?php echo $material->nombre;?>" data-desc="<?php echo $material->descripcion;?>">
						</td>
						<td><?php echo $material->nombre;?>
							<span style="font-weight:bold; color: <?php echo $material->codigo_c;?>"><br>(<?php echo $material->nombre_c;?>)</span>
						</td>
				<?php if($type!="pim" && $type!="pim-modal"){?>
						<td>
							<div class="input-group input-120">
						<?php if(!$es_material){?>
								<form class="<?php if($type=="pm-modal" || $type=="pm" || $type=="pgm-modal" || $type=="pgm" || $type=="pgcm-modal" || $type=="pgcm"){?>seleccionar_material<?php }?><?php if($type=="pm-modal-change" || $type=="pgm-modal-change" || $type=="pgcm-modal-change"){?>change_material<?php }?>" data-m="<?php echo $material->idm;?>">
									<input type="number" id="can_m<?php echo $material->idm;?>" class="form-control form-control-sm input-90" placeholder="0,0" min="0" step="any" max="999999.9999">
								</form>
						<?php }else{ ?>
								<input type="number" class="form-control form-control-sm input-90 " value="<?php echo $can_exit;?>" disabled>
						<?php }?>
								<span class="input-group-addon unid form-control-sm"><?php echo $material->abr_u.".";?></span>
							</div>
						</td>
				<?php }?>
				<?php if($type!="pim" && $type!="pim-modal"){?>
						<td>
							<div class="input-group">
							<?php if(!$es_material){?>
								<textarea class="form-control input-sm" id="obs_m<?php echo $material->idm;?>" placeholder="Observaciónes del material en el producto" rows="3" maxlength="200"></textarea>
							<?php }else{ ?>
								<textarea class="form-control input-sm" rows="3" disabled><?php echo $obs_exit;?></textarea>
							<?php }?>
								<span class="input-group-addon help form-control-sm btn-default" <?php echo $popover.$help;?>><i class="fa fa-info-circle"></i></span>
							</div>
						</td>
				<?php }?>
						<td>
					<?php if(!$es_material){?>
						<?php if($type=="pm-modal" || $type=="pm" || $type=="pgm-modal" || $type=="pgm" || $type=="pgcm-modal" || $type=="pgcm" || $type=="pim" || $type=="pim-modal"){?>
							<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light seleccionar_material" data-m='<?php echo $material->idm;?>'>
				                <i class="fa fa-plus"></i> add
				            </button>
				        <?php }?>
				        <?php if($type=="pm-modal-change" || $type=="pgm-modal-change" || $type=="pgcm-modal-change"){?>
								<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light change_material" data-m="<?php echo $material->idm;?>"><i class="fa fa-exchange"></i> Sel.</button>
				        <?php }?>
				    <?php }else{ echo "<span style='font-size:.6rem;line-height: 0;'>".$msj."</span>"; }?>
						</td>
					</tr>
					<?php
				}// en for
			?>
			</tbody>
		</table>
	</div>
		<?php
	}else{
		echo "<h3>0 registros encontrados...</h3>";
	}
?>
<script>$('[data-toggle="popover"]').popover({html:true});$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});$(".seleccionar_material").seleccionar_material();$(".change_material").change_material();</script>