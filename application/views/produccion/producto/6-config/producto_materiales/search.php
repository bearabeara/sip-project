<?php $rand=rand(10,99999); ?>
<div class="row">
	<div class="col-xs-12">
	    <span class="g-control-accordion">
	      	<button class="producto_materiales_2" data-type="<?php echo $type;?>"<?php if(isset($pg)){ ?> data-pg="<?php echo $pg;?>" <?php }?><?php if(isset($pgc)){ ?> data-pgc="<?php echo $pgc;?>" <?php }?>><i class="fa fa-refresh"></i></button>
	      	<button class="<?php if($type=="pm-modal"){ ?>producto_seg<?php }else{ ?>producto_grupo_color_seg<?php }?>" <?php if(isset($pg)){ ?> data-pg="<?php echo $pg;?>" <?php }?><?php if(isset($pgc)){ ?> data-pgc="<?php echo $pgc;?>" <?php }?> data-type="<?php echo $type;?>"><i class="fa fa-clock-o"></i></button>
	    </span>
	</div>
</div>
<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="100%">
						<form class="search_producto_materiales" data-type="search">
							<input id="s2_mat" type="search" class="form-control form-control-sm" placeholder="Material" data-type="<?php echo $type;?>" <?php if(isset($pg)){ ?> data-pg="<?php echo $pg;?>" <?php }?> <?php if(isset($pgc)){ ?> data-pgc="<?php echo $pgc;?>" <?php }?>>
						</form>
					</td>
				<?php
					$new="";
					$v_atr_new=array("type"=>$type);
					$grupo_color=$this->M_producto_grupo_color->get_grupo_color('pg.idp',$producto->idp);
					if($type=="pgm-modal"){
    					$varios_grupo=$this->lib->varios_grupo($grupo_color);
    					$pgc=$this->M_producto_grupo_color->get_row('idpgr',$pg);
    					if($varios_grupo && count($pgc)>1){ $new="material";}
					}
					if($type=="pgcm-modal"){
    					if(count($grupo_color)>1){ $new="material";}
					}
					if($type=="pm-modal"){
    					$new="material";
					}
				?>
					<td>
					<?php 
					$search=json_encode(array('function'=>'search_producto_materiales','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
					$all=json_encode(array('function'=>'search_producto_materiales','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
					$new=json_encode(array('function'=>'material'.$rand,'title'=>'Adiccionar material','atribs'=> json_encode(array('type' => $type))));
					$this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all,"new"=>$new]);
					?>
					<?php //$this->load->view('estructura/botones/buscador2',['id' => '1','f_buscar'=>json_encode(array("funcion" => "search_producto_materiales","atributos"=>"")),'f_ver'=>json_encode(array("funcion" => "all_producto_materiales","atributos"=>"")),'f_nuevo'=>json_encode(array("funcion" => $new,"atributos"=>json_encode($v_atr_new)))]);?></td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>Onfocus("s2_mat");$('[data-toggle="popover"]').popover({html:true});$(".search_producto_materiales").search_producto_materiales();
$(".material<?php echo $rand;?>").material();

	$("button.producto_materiales_2").producto_materiales();
	<?php if($type=="pm-modal"){ ?>
		$("button.producto_seg").producto_seg();
	<?php }else{ ?>
		$("button.producto_grupo_color_seg").producto_grupo_color_seg();
	<?php }?>
</script>