<?php
	$help1='title="Nombre" data-content="Ingrese un nombre de color alfanumérico de 2 a 50 caracteres <b>puede incluir espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="Abreviatura" data-content="Ingrese un abreviatura alfanumérica hasta 10 caracteres <b>sin espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help3='title="Código" data-content="Seleccione el color que representa el nombre."';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		  	<div class="row">
				<div class="col-md-6 col-xs-12">
					<form class="save_color">
						<div class="input-group">
							<input class='form-control form-control-xs' type="text" id="n3_nom" placeholder='Nombre de color' maxlength="50">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-md-3 col-xs-12">
					<form onsubmit="return ()">
						<div class="input-group">
							<input class='form-control form-control-xs' type="text" id="n3_abr" placeholder='Abreviatura de color' maxlength="10">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<div class="col-md-3 col-xs-12">
					<form class="save_color">
						<div class="input-group">
							<input class='form-control form-control-xs' type="color" id="n3_col" placeholder='#FFF' style="padding:.15rem; height:35px;">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<script>Onfocus("n3_nom");$('[data-toggle="popover"]').popover({html:true});$("form.save_color").submit(function(e){$(this).save_color();e.preventDefault();});</script>
			</div>
	</div>
</div>