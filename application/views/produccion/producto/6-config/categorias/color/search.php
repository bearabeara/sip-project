<?php $rand=rand(10,99999999);?>
<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="90%">
						<form class="view_color" data-type="search">
							<input id="s2_gru" type="search" class="form-control form-control-sm search_produccion<?php echo $rand;?>" placeholder="Color" <?php if($type=="new"){ ?>data-pg="<?php echo $idpgr;?>"<?php }?> data-type="<?php echo $type;?>" <?php if($type=="update"){ ?>data-pgc='<?php echo $idpgrc;?>'<?php } ?> data-module="2">
						</form>
					</td>
					<td width="20%" class="hidden-sm">
						<form class="view_color" data-type="search">
							<input id="s2_abr" type="search" class="form-control form-control-sm search_produccion<?php echo $rand;?>" placeholder="Abreviatura" data-module="2">
						</form>
					</td>
					<td width="1%">
					<?php 
						$search=json_encode(array('function'=>'view_color','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
						$all=json_encode(array('function'=>'view_color','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
						$this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);
					?>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>$(".view_color").view_color();$(".search_produccion<?php echo $rand;?>").search_produccion();</script>