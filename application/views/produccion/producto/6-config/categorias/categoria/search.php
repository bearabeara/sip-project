<?php $rand=rand(10,99999999);?>
<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="90%">
						<form class="view_grupo" data-type="search">
							<input type="search" id="s2_gru" class="form-control form-control-sm search_produccion<?php echo $rand;?>" placeholder='Buscar nombre...' data-tbl="table#tbl-container" data-type="<?php echo $type;?>" <?php if($type=="update"){ echo "data-pg='".$idpgr."'"; } ?> data-module="2"/>
						</form>
					</td>
					<td width="20%" class="hidden-sm">
						<form class="view_grupo" data-type="search">
							<input id="s2_abr" type="search" class="form-control form-control-sm search_produccion<?php echo $rand;?>" placeholder="Abreviatura" data-module="2">
						</form>
					</td>
					<td width="1%">
					<?php 
						$search=json_encode(array('function'=>'view_grupo','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
						$all=json_encode(array('function'=>'view_grupo','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
						$this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);
					?>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>$(".search_produccion<?php echo $rand;?>").search_produccion();$(".view_grupo").view_grupo();</script>