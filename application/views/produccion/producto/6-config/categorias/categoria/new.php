<?php
	$help1='title="Nombre" data-content="Ingrese un nombre de categoria alfanumérico de 2 a 50 caracteres <b>puede incluir espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$help2='title="Abreviatura" data-content="Ingrese un abreviatura alfanumérica hasta 10 caracteres <b>sin espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		  	<div class="row">
					<div class="col-md-8 col-xs-12">
						<form class="save_grupo">
							<div class="input-group">
								<input class='form-control form-control-xs'type="text" id="n3_cat" placeholder='Nombre de categoria' maxlength="50">
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
					<div class="col-md-4 col-xs-12">
						<form class="save_grupo">
							<div class="input-group">
								<input class='form-control form-control-xs'type="text" id="n3_abr" placeholder='Abreviatura de categoria' maxlength="10">
								<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form>
					</div>
			</div>
	</div>
</div>
<script>Onfocus("n3_cat");$('[data-toggle="popover"]').popover({html:true});$("form.save_grupo").submit(function(e){$(this).save_grupo();e.preventDefault();});</script>