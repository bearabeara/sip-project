<?php 
$url_mat=base_url().'libraries/img/materiales/miniatura/';
$url_img=base_url().'libraries/img/';
$help10='title="Contenedor general" data-content="Contiene todos los materiales del color de la categoría."';
$help11='title="Adicionar materiales" data-content="Adicione materiales en el color de la categoría."';
$help12='title="Contenedor general" data-content="Contenedor de todas las imágenes del producto, de sus categorias y colores."';
$help13='title="Adicionar fotografías" data-content="Adicione fotografías en el color de la categoría."';
$help14='title="Contenedor general" data-content="Contiene todos las piezas del color de la categoría."';
$help15='title="Adicionar piezas" data-content="Adicione piezas en el color de la categoría."';
$help16='title="Contenedor general" data-content="Contiene todos los procesos en el color la categoría."';
$help17='title="Adicionar procesos" data-content="Adicione procesos en el color de la categoría."';
$popover1='data-toggle="popover" data-placement="top" data-trigger="hover"';
$popover4='data-toggle="popover" data-placement="left" data-trigger="hover"';

$url=base_url().'libraries/img/productos/miniatura/';
$grupo=$this->M_producto_grupo->get($producto_grupo_color->idpgr);
$grupo=$grupo[0];
$grupo_color=$this->M_producto_grupo_color->get_grupo_color('pg.idp',$grupo->idp);
$imagenes_producto=$this->M_producto_imagen->get_row('idp',$grupo->idp);
$producto=$this->M_producto->get($grupo->idp);
$producto=$producto[0];
$atributos_producto=$this->M_producto_atributo->get_atributo("pa.idp",$grupo->idp);
$atributos_grupo=$this->M_producto_grupo_atributo->get_atributo("pga.idpgr",$grupo->idpgr);
$producto_piezas=$this->M_producto_pieza->get_row("idp",$grupo->idp);
$producto_grupo_color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$producto_grupo_color->idpgrc);
$producto_procesos=$this->M_producto_proceso->producto_proceso("pp.idp",$grupo->idp,"p.nombre","ASC");
$rand=rand(10,99999);
?>
<div class="list-group-item" style="max-width:100%; display:block;">
  <div class="row">
    <!--Manejo de materiales en el color-->
    <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
      <div class="table-responsive g-table-responsive">
        <table cellspacing="0" cellpadding="0">
          <tr>
            <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
              <div class="item help" <?php echo $popover1.$help10;?>>
                <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
              </div>
              <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_materiales<?php echo $rand;?>" data-type="pgcm-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>">
              <img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45" ></a>
            </td>
            <?php if(count($grupo_color)>1){?>
            <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
              <div class="item help" <?php echo $popover1.$help11;?>>
                <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
              </div>
              <div class="img-thumbnail-45"></div><a href"javascript:" class="material<?php echo $rand;?>" data-type="pgcm" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>">
              <img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
            </td>
            <?php } ?>
            <?php $materiales_color=$this->M_producto_grupo_color_material->get_material("pgcm.idpgrc",$producto_grupo_color->idpgrc);?>
            <?php for ($m=0; $m < count($materiales_color) ; $m++) { $material=$materiales_color[$m]; 
              $archivo="";$desc="<br>";
              $desc="Cantidad ".$material->cantidad;
              $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
              if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
              if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
              $tit=$material->nombre;
              $img_mat="sistema/miniatura/default.jpg";
              if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
              ?>
              <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $url_img.$img_mat;?>">
                <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_material<?php echo $rand;?>" title="Eliminar Imagen" data-pgcm="<?php echo $material->idpgcm;?>" data-type="pgcm" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                <div class="img-thumbnail-45">
                 <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
               </div>
             </td>
             <?php }?>

             <?php $materiales_grupo=$this->M_producto_grupo_material->get_material("pgm.idpgr",$grupo->idpgr);?>
             <?php for ($m=0; $m < count($materiales_grupo) ; $m++) { $material=$materiales_grupo[$m]; 
              $archivo="";$desc="<br>";
              $desc="Cantidad ".$material->cantidad;
              $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
              if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
              if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
              $tit=$material->nombre;
              $img_mat="sistema/miniatura/default.jpg";
              if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
              ?>
              <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $url_img.$img_mat;?>">
                <div class="img-thumbnail-45">
                  <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail thumbnail-2 g-img-thumbnail img-thumbnail-45 child" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                </div>
              </td>
              <?php }?>

              <?php $materiales_producto=$this->M_producto_material->get_material("pm.idp",$producto->idp);?>
              <?php for ($m=0; $m < count($materiales_producto) ; $m++) { $material=$materiales_producto[$m]; 
                $archivo="";$desc="<br>";
                $desc="Cantidad ".$material->cantidad;
                $unidad=$this->lib->search_elemento($unidades,"idu",$material->idu);
                if($unidad!=null){ $desc.=" ".$unidad->abr.".";}
                if($material->observacion!="" && $material->observacion!=null){ $desc.=", ".$material->observacion;}
                $tit=$material->nombre;
                $img_mat="sistema/miniatura/default.jpg";
                if($material->fotografia!="" && $material->fotografia!=NULL){ $img_mat="materiales/miniatura/".$material->fotografia;}
                ?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $url_img.$img_mat;?>">
                  <div class="img-thumbnail-45">
                    <img src="<?php echo $url_img.$img_mat;?>" class="img-thumbnail child g-img-thumbnail img-thumbnail-45" data-title="<?php echo $tit;?>" data-desc="<?php echo $desc;?>">
                  </div>
                </td>
                <?php }?>
              </tr>
            </table>
          </div>
          <i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
        </div>
        <!--Imagenes en el grupo-->
        <div class="col-lg-6 col-sm-12" style="margin-bottom:10px;">
          <div class="table-responsive g-table-responsive">
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover1.$help12;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_fotografias<?php echo $rand;?>" data-type="pgcf-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <?php if(count($grupo_color)>1){?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover1.$help13;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href"javascript:" class="new_fotografias<?php echo $rand;?>" data-type="pgcf" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <?php } ?>
                <?php $imagenes_color=$this->M_producto_imagen_color->get_row('idpgrc',$producto_grupo_color->idpgrc);?>
                <?php for ($im=0; $im < count($imagenes_color) ; $im++){  ?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_fotografia<?php echo $rand;?>" title="Eliminar Imagen" data-pgcf="<?php echo $imagenes_color[$im]->idpig;?>" data-type="pgcf"  data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                  <div class="img-thumbnail-45">
                   <?php $desc="<br>"; if($imagenes_color[$im]->descripcion!="" && $imagenes_color[$im]->descripcion!=null){ $desc=$imagenes_color[$im]->descripcion; } ?>
                   <img src="<?php echo $url.$imagenes_color[$im]->archivo;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>">
                 </div>
               </td>
               <?php }?>
               <?php for ($im=0; $im < count($imagenes_producto) ; $im++){  ?>
               <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                <div class="img-thumbnail-45">
                 <?php $desc="<br>"; if($imagenes_producto[$im]->descripcion!="" && $imagenes_producto[$im]->descripcion!=null){ $desc=$imagenes_producto[$im]->descripcion; } ?>
                 <img src="<?php echo $url.$imagenes_producto[$im]->archivo;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-45 child" data-title="<?php echo $producto->nombre;?>" data-desc="<?php echo $desc;?>">
               </div>
             </td>
             <?php } ?>
           </tr>
         </table>
       </div>
     </div>
     <!--End imagenes en el color del grupo-->
     <!--Piezas en el color del grupo-->
     <i class="clearfix visible-lg-block"></i>
     <div class="col-lg-6 col-sm-12">
      <div class="table-responsive g-table-responsive">
        <table cellspacing="0" cellpadding="0">
          <tr>
            <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
              <div class="item help" <?php echo $popover1.$help14;?>>
                <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
              </div>
              <div class="img-thumbnail-45"></div><a href"javascript:" class="producto_piezas<?php echo $rand;?>" data-type="pgcpi-modal" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
            </td>
            <?php if(count($grupo_color)>1){?>
            <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
              <div class="item help" <?php echo $popover1.$help15;?>>
                <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
              </div>
              <div class="img-thumbnail-45"></div><a href"javascript:" class="new_pieza<?php echo $rand;?>" data-type="pgcpi" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
            </td>
            <?php } ?>
            <?php for ($pi=0; $pi < count($producto_grupo_color_piezas) ; $pi++) { $pieza=$producto_grupo_color_piezas[$pi]; 
              if($pieza->fotografia!="" && $pieza->fotografia!=null){
                $archivo=$url_img."piezas/miniatura/".$pieza->fotografia;
              }else{
                $archivo=$url_img."/sistema/miniatura/default.jpg";
              }
              $desc="Cantidad ".$pieza->cantidad." unid., largo ".$pieza->largo."cm., ancho ".$pieza->ancho."cm.";
              if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc.=", ".$pieza->descripcion;}
              ?>
              <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);"><button type="button" class="close confirmar_pieza<?php echo $rand;?>" title="Eliminar pieza" data-pgcpi="<?php echo $pieza->idpgcpi;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="pgcpi" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button></div>
                <div class="img-thumbnail-45">
                  <img src="<?php echo $archivo;?>" class="img-thumbnail thumbnail-3 g-img-thumbnail img-thumbnail-45" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
                </div>
              </td>
              <?php }?>
              <?php for ($pi=0; $pi < count($producto_piezas) ; $pi++) { $pieza=$producto_piezas[$pi]; 
                if($pieza->fotografia!="" && $pieza->fotografia!=null){
                  $archivo=$url_img."piezas/miniatura/".$pieza->fotografia;
                }else{
                  $archivo=$url_img."/sistema/miniatura/default.jpg";
                }
                $desc="Cantidad ".$pieza->cantidad." unid., largo ".$pieza->largo."cm., ancho ".$pieza->ancho."cm.";
                if($pieza->descripcion!="" && $pieza->descripcion!=null){ $desc.=", ".$pieza->descripcion;}
                ?>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;" img="<?php echo $archivo;?>">
                  <div class="img-thumbnail-45">
                    <img src="<?php echo $archivo;?>" class="img-thumbnail child g-img-thumbnail img-thumbnail-45" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo $desc;?>">
                  </div>
                </td>
                <?php }?>
              </tr>
            </table>
          </div>
        </div>
        <i class="visible-md-block visible-sm-block visible-xs-block"><br></i>
        <!--Procesos en el color del grupo-->
        <div class="col-lg-6 col-sm-12">
          <div class="table-responsive g-table-responsive">
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover1.$help16;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href="javascript:" data-type-save="pgcpr-modal" class="pila_producto_proceso<?php echo $rand;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-pg="<?php echo $grupo->idpgr;?>"><img src="<?php echo base_url().'libraries/img/sistema/stack.jpg';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px;">
                  <div class="item help" <?php echo $popover1.$help17;?>>
                    <div style="font-size:.8em; "><i class="fa fa-info-circle"></i></div>
                  </div>
                  <div class="img-thumbnail-45"></div><a href="javascript:" class="new_producto_proceso" data-type-save="pgcpr" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"><img src="<?php echo base_url().'libraries/img/sistema/plus1.png';?>" class="img-thumbnail img-thumbnail-45"></a>
                </td>
                <?php for($pg3=0; $pg3 < count($producto_procesos); $pg3++){ $producto_proceso=$producto_procesos[$pg3];
                  $control=$this->lib->search_elemento($producto_grupo_procesos,"idppr",$producto_proceso->idppr);
                  $control2=$this->lib->search_elemento($producto_grupo_color_procesos,"idppr",$producto_proceso->idppr);
                  $sw=true;
                  if($control!=null){ if($control->idpgr!=$producto_grupo_color->idpgr){ $sw=false;}}
                  if($control2!=null){ if($control2->idpgrc!=$producto_grupo_color->idpgrc){ $sw=false;}}
                  if($sw){
                    ?>
                    <td class="img-thumbnail-45" style="border: 0px; padding: 0px 2px; position:relative;">
                      <?php if($control2!=null){?>
                      <div id="item" style="border:1px solid rgba(255, 82, 82, 0.74);">
                        <button type="button" class="close confirmar_producto_proceso" title="Eliminar proceso" data-ppr="<?php echo $producto_proceso->idppr;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type-save="pgcpr" style="font-size:.8rem; color:rgba(255, 82, 82, 0.74);"><i class="fa fa-times"></i></button>
                      </div>
                      <?php }?>
                      <div class="img-thumbnail <?php if($control!=null){ echo 'thumbnail-2';}?> <?php if($control2!=null){ echo 'thumbnail-3';}?> img-thumbnail-45 <?php if($control2==null){ echo 'child';}?>" style="height: 45px; vertical-align: middle; overflow: hidden; line-height: .9;" title="<?php echo $producto_proceso->nombre;?>">
                       <div style="height: 100%; display: table;">
                        <small style="font-size: 61%; vertical-align: middle; display: table-cell;"><?php echo $producto_proceso->nombre;?></small>
                      </div>
                    </div>
                  </td>
                  <?php }/* end if*/ }// end for ?>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!--Abtributo en el color del grupo-->
      <div class="list-group-item" style="max-width:100%; display:block;">
        <div class="row">
          <div class="form-group" id="pgc-atr<?php echo $producto_grupo_color->idpgrc;?>">
            <input type="hidden" value="" id="delete-color<?php echo $producto_grupo_color->idpgrc;?>">
            <?php $count=0;
            for ($ap=0; $ap < count($atributos_producto) ; $ap++){ $atributo=$atributos_producto[$ap]; $count++; ?>
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
            <div class="col-sm-4 col-xs-12">
              <div class="input-group">
                <div class="form-control form-control-sm textarea disabled"><?php echo $atributo->valor;?></div>
              </div>
            </div>
            <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
            <?php } ?>
            <?php for($ap=0; $ap < count($atributos_grupo) ; $ap++){ $atributo=$atributos_grupo[$ap]; $count++; ?>
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
            <div class="col-sm-4 col-xs-12">
              <div class="input-group">
                <div class="form-control form-control-sm textarea disabled"><?php echo $atributo->valor;?></div>
              </div>
            </div>
            <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
            <?php } ?>
            <?php $atributos=$this->M_producto_grupo_color_atributo->get_atributo("pgca.idpgrc",$producto_grupo_color->idpgrc);
            for ($k=0; $k < count($atributos) ; $k++){ $atributo=$atributos[$k]; $count++;?>
            <label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><?php echo $atributo->atributo;?></label>
            <div class="col-sm-4 col-xs-12">
              <div class="input-group">
                <textarea id="atr<?php echo rand(1,9999).$atributo->idpgca;?>" maxlength="150" class="form-control form-control-sm" data-pgca="<?php echo $atributo->idpgca;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-typele="db" data-type="color" placeholder="<?php echo $atributo->atributo;?>"><?php echo $atributo->valor;?></textarea>
                <?php $helpa='title="'.$atributo->atributo.'" data-content="Ingrese un contenido alfanumerico hasta 150 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';?>
                <span class="input-group-addon form-control-sm btn-default" <?php echo $popover4.$helpa; ?>><i class="fa fa-info-circle"></i></span>
                <a href="javascript:" class="input-group-addon form-control-sm btn-default eliminar_atributo<?php echo $rand;?>"><i class="fa fa-trash-o"></i></a>
              </div>
            </div>
            <span <?php if(($count)%2==0){?>class='clearfix'<?php }?>></span>
            <?php } ?>
          </div>
        </div>
        <div class="row text-right" style="padding-right:15px;">
          <?php if(count($grupo_color)>1){?>
          <button type="button" class="btn btn-inverse-info btn-mini waves-effect waves-light atributos<?php echo $rand;?>" data-pg="<?php echo $grupo->idpgr;?>"  data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="color"><i class="fa fa-plus"></i><span class="m-l-10">Atributo</span></button>
          <?php } ?>
          <?php if(count($grupo_color)>1){?>
          <button type="button" class="btn btn-inverse-info btn-mini waves-effect waves-light save_producto_grupo_color_atr<?php echo $rand;?>" data-pg="<?php echo $grupo->idpgr;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>" data-type="color"><i class="icon-floppy-o"></i><span class="m-l-10">Guardar</span></button>
          <?php } ?>
        </div>
      </div>
      <script>$('[data-toggle="popover"]').popover({html:true});
        $("a.material<?php echo $rand;?>").click(function(){$(this).material();});
        $("a.producto_materiales<?php echo $rand;?>").producto_materiales();
        $("img.g-img-thumbnail").click(function(){ view_imagen($(this));});
        $("a.pila_producto_proceso<?php echo $rand;?>").pila_producto_proceso();
        $("a.new_producto_proceso<?php echo $rand;?>").new_producto_proceso();
        $("button.confirmar_producto_proceso").confirmar_producto_proceso();
        
        
        $("button.confirmar_material<?php echo $rand;?>").confirmar_material();
        $("a.producto_fotografias<?php echo $rand;?>").producto_fotografias();
        $("a.new_fotografias<?php echo $rand;?>").new_fotografias();
        $("button.confirmar_fotografia<?php echo $rand;?>").confirmar_fotografia();
        $("a.producto_piezas<?php echo $rand;?>").producto_piezas();
        $("a.new_pieza<?php echo $rand;?>").new_pieza();
        $("button.confirmar_pieza<?php echo $rand;?>").confirmar_pieza();
        $("button.atributos<?php echo $rand;?>").atributos();
        $("button.save_producto_grupo_color_atr<?php echo $rand;?>").save_producto_grupo_color_atr();
        $("a.eliminar_atributo<?php echo $rand;?>").eliminar_atributo();
      </script>