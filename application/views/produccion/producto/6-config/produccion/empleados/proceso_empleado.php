<?php $url=base_url().'libraries/img/';
$t = array(0 => 'Maestro', 1 => 'Ayudante');
?>
<div class="table-responsive" id="datos-empleado-proceso" data-pr="<?php echo $proceso->idpr;?>" data-ppr="<?php echo $producto_proceso->idppr;?>" data-pgc="<?php echo $producto_grupo_color->idpgrc;?>"  data-tbl="<?php echo $tbl;?>">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%" class="img-thumbnail-50">#Item</th>
				<th width="40%">Nombre</th>
				<th width="20%">Area</th>
				<th width="30%">Calidad</th>
				<th width="5%"></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($empleados)){
				for($i=0; $i < count($empleados) ;$i++){ $empleado=$empleados[$i];
					$asignado=false;
					$elemento=$this->lib->search_elemento($producto_empleados,"ide",$empleado->ide);
					if($elemento!=null){ $asignado=true; }
						$img="sistema/default.jpg";
						if($empleado->fotografia!=null && $empleado->fotografia!=""){ $img="personas/".$empleado->fotografia;}
			?>
					<tr>
						<td>
							<div class="img-thumbnail-50"></div>
							<div class="item"><?php echo $i+1;?></div>
							<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="">
						</td>
						<td><?php echo $empleado->nombre_completo;?></td>
						<td><?php echo $t[$empleado->tipo*1]." - ".$proceso->nombre;?></td>
						<td>
						<?php if(!$asignado){?>
							<?php $id_rango=$empleado->idpre; ?>
							<div class="rangos" id="rangos_empleado<?php echo $id_rango;?>" style="width: 225px;">
								<input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
								<input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
							</div>
						<?php }else{?>
								<div class="rangos" style="width: 225px;">
								<?php for ($r=0; $r < $elemento->calidad ; $r++) { ?>
									<label class="rango fa fa-star" style="color: #1b8bf9; cursor: default; float: left;"></label>
								<?php }?>
								</div>
						<?php }?>
						</td>
						<td>
							<button type="button" class="btn btn-<?php if($asignado){ echo 'default';}else{ echo 'inverse-primary add_empleado_proceso';}?> btn-mini waves-effect waves-light"<?php if(!$asignado){?> data-pre="<?php echo $empleado->idpre;?>" data-type="producto" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{ ?> disabled="disabled" <?php }?>> <i class="fa fa-plus"></i> add </button>
						</td>
						<td>
							<span class="g-control-accordion">
								<button class="confirmar_empleado_producto" data-pre="<?php echo $empleado->idpre;?>"><i class="icon-trashcan2"></i></button>
							</span>
						</td>
					</tr>
					<?php 
					}// end for
				}else{?>
				<tr>
					<td colspan="4" class="text-center"><h5>0 empleados registrados con el proceso <strong><?php echo $proceso->nombre;?></strong></h5></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	<div class="row text-right" style="padding-right:15px;">
		<button type="button" class="btn btn-primary btn-mini waves-effect waves-light search_empleado" data-pr="<?php echo $proceso->idpr;?>">
			<i class="fa fa-plus"></i><span class="m-l-10">empleado</span>
		</button>
	</div>
	<script>
		$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});
		$("button.add_empleado_proceso").add_empleado_proceso();
		$("button.confirmar_empleado_producto").click(function(){$(this).confirmar_empleado_producto();});
		$("button.search_empleado").click(function(){ $(this).search_empleado();});
	</script>