<?php 
	
?>

<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="100%">
						<form id="f2_nom">
							<input id="s3_nom" type="search" class="form-control form-control-sm" placeholder="Nombre del empleado"<?php if(isset($idpr)){?> data-pr="<?php echo $idpr;?>"<?php }?>>
						</form>
					</td>
					<td class="text-right">
						<div style="width:67px;">
							<div class="btn-group " role="group">
								<?php 
									$b=json_encode(array("funcion" => "view_empleado","atributos"=>""));
									$v=json_encode(array("funcion" => "all_empleado","atributos"=>json_encode(array("type"=>"new_pro_emp"))));
								?>
								<?php $this->load->view('estructura/botones/buscador2',['id' => '2','f_buscar'=>$b,'f_ver'=>$v]);?>
							</div>
						</div>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="view_3"></div>
</div>
<script>Onfocus("s3_nom");$('[data-toggle="popover"]').popover({html:true}); $("#f2_nom").submit(function(){ $(this).view_empleado(); event.preventDefault(); }); </script>