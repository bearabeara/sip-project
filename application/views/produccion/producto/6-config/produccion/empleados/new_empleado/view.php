<?php $url=base_url().'libraries/img/';
$t = array(0 => 'Maestro(a)', 1 => 'Ayudante');
?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th class="img-thumbnail-50"><div class="img-thumbnail-50">#Item</div></th>
			<th width="60%">Nombre</th>
			<th width="15%">Área</th>
			<th width="20%">Grado</th>
			<th width="5%"></th>
		</tr>
	</thead>
	<tbody>
<?php if(!empty($empleados)){ ?>
	<?php for ($i=0; $i < count($empleados) ; $i++){ $empleado=$empleados[$i];
		$img="sistema/default.jpg";
		if($empleado->fotografia!=null && $empleado->fotografia!=""){ $img="personas/".$empleado->fotografia;}
		$asignado=false;
		$control=$this->lib->search_elemento($proceso_empleados,"ide",$empleado->ide);
		if($control!=null){ $asignado=true;}
	?>
		<tr>
			<td>
				<div class="img-thumbnail-50"></div>
				<div class="item"><?php echo $i+1;?></div>
				<img src="<?php echo $url.$img;?>" class="img-thumbnail g-img-thumbnail" data-title="<?php echo $empleado->nombre_completo;?>" data-desc="">
			</td>
			<td><?php echo $empleado->nombre_completo; ?></td>
			<td>
				<select class="form-control form-control-sm input-140" title="<?php echo $proceso->nombre;?>" disabled="disabled">
					<option value=""><?php echo $proceso->nombre;?></option>
				</select>
			</td>
			<td>
				<select id="tip<?php echo $empleado->ide;?>" class="form-control form-control-sm" <?php if($asignado){?> disabled="disabled" title="<?php echo $t[$control->tipo];?>"<?php }?>>
					<option value="">Seleccionar...</option>
			<?php for ($j=0; $j < count($t); $j++) { ?>
					<option value="<?php echo $j;?>"<?php if($asignado){ if($control->tipo==$j){?> selected="selected"<?php }}?>><?php echo $t[$j];?></option>
			<?php } ?>
				</select>
			</td>
			<td>
				<button type="button" class="btn btn-<?php if($asignado){ echo 'default'; }else{ echo 'inverse-primary';}?> btn-mini waves-effect waves-light <?php if(!$asignado){ echo 'adicionar_empleado'; }?>"<?php if($asignado){?> disabled="disabled" <?php }else{?> data-pr="<?php echo $proceso->idpr;?>" data-e="<?php echo $empleado->ide;?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }?>>
					<i class="fa fa-plus"></i> add</span>
				</button>
			</td>
		</tr>
	<?php } ?>
<?php }else{ ?>
		<tr>
			<td colspan="4"><h4>0 empleados encontrados.</h4></td>
		</tr>
<?php } ?>
	</tbody>
</table>
<script>$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});$("button.adicionar_empleado").click(function(){$(this).adicionar_empleado();});</script>