<?php 
$url=base_url().'libraries/img/';
$t = array(0 => 'Maestro(a)', 1 => 'Ayudante');
$empleados=[];
for($e=0; $e < count($producto_procesos_empleados) ; $e++){ $pre=$producto_procesos_empleados[$e];
	if($pre->idpr==$proceso->idpr){
		$img="sistema/default.jpg";
		if($pre->fotografia!=null && $pre->fotografia!=""){ $img="personas/miniatura/".$pre->fotografia;}
		$empleados[]=array('idproe'=>$pre->idproe,'idpgrc'=>$pre->idpgrc, 'ide'=>$pre->ide, 'ci'=>$pre->ci, 'fotografia'=>$img,'nombre'=>$pre->nombre_completo, 'calidad'=>$pre->calidad, 'tipo'=>$pre->tipo);
	}
} ?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th class="img-thumbnail-50">#Item</th>
			<th width="60%">Nombre</th>
			<th width="10%">Área</th>
			<th width="30%">Calidad</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if(!empty($empleados)){
			$j_empleados=json_decode(json_encode($empleados));
			$cont=1;
			foreach($j_empleados as $key => $empleado){
				?>
				<tr>
					<td class="img-thumbnail-50">
						<div class="img-thumbnail-50"></div>
						<div class="item"><?php echo $cont++;?></div>
						<img src="<?php echo $url.$empleado->fotografia;?>" class="img-thumbnail g-img-thumbnail img-thumbnail-50" data-title="<?php echo $empleado->nombre;?>" data-desc="">
					</td>
					<td><?php echo $empleado->nombre; ?></td>
					<td><?php echo $t[$empleado->tipo];?></td>
					<td>
						<?php $id_rango=rand(0,9999)."-".$empleado->idproe; ?>
						<div class="rangos" id="rangos_proe<?php echo $id_rango;?>" data-proe="<?php echo $empleado->idproe;?>">
							<input class="rango" id="rango<?php echo $id_rango;?>-10" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad>=10){?> checked="checked"<?php } ?> data-val="10"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-10"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-9" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==9){?> checked="checked"<?php } ?> data-val="9"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-9"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-8" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==8){?> checked="checked"<?php } ?> data-val="8"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-8"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-7" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==7){?> checked="checked"<?php } ?> data-val="7"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-7"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-6" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==6){?> checked="checked"<?php } ?> data-val="6"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-6"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-5" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==5){?> checked="checked"<?php } ?> data-val="5"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-5"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-4" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==4){?> checked="checked"<?php } ?> data-val="4"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-4"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-3" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==3){?> checked="checked"<?php } ?> data-val="3"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-3"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-2" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==2){?> checked="checked"<?php } ?> data-val="2"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-2"></label>
							<input class="rango" id="rango<?php echo $id_rango;?>-1" type="radio" name="rango<?php echo $id_rango;?>" <?php if($empleado->calidad==1){?> checked="checked"<?php } ?> data-val="1"/><label class="rango fa fa-star" for="rango<?php echo $id_rango;?>-1"></label>
						</div>
					</td>
					<td>
						<div class="g-control-accordion" style="width:60px;">
							<button class="update_producto_empleado" data-proe="<?php echo $empleado->idproe;?>" data-rangos="<?php echo $id_rango;?>" data-tbl="<?php echo $tbl;?>"><i class="icon-floppy-o"></i></button>
							<button class="confirmar_producto_empleado" data-proe="<?php echo $empleado->idproe;?>" data-tbl="<?php echo $tbl;?>"><i class="icon-trashcan2"></i></button>
						</div>
					</td>
				</tr>
				<?php
			}
		}else{
			echo "<tr><td colspan='5' class='text-center'><h4>0 empleados asignados.</h4></td></tr>";
		}
		?>
	</tbody>
</table>
<script>$("button.update_producto_empleado").click(function(){ $(this).update_producto_empleado();});$("button.confirmar_producto_empleado").click(function(){ $(this).confirmar_producto_empleado();});$("img.g-img-thumbnail").click(function(){ view_imagen($(this));});</script>