<table class="table table-bordered" style="font-size:.75em">
	<thead>
	    <tr>
	    	<th width="5%">#</th>
	    	<th width="15%">Fecha</th>
	    	<th width="20%">Usuario</th>
	    	<th width="60%">Acción</th>
	    </tr>
    </thead>
    <tbody>
<?php 
if(isset($historial)){
	if(count($historial)>0){
		for ($i=0; $i < count($historial) ; $i++) { $alerta=$historial[$i];
?>
	<tr>
		<td><?php echo $i+1;?></td>
		<td><?php echo $alerta->fecha;?></td>
		<td><?php echo $alerta->usuario;?></td>
		<td><?php echo $this->lib->alert_producto_grupo_color($alerta);?></td>
	</tr>
<?php
		}
	}else{
		echo "<tr><td colspan='4'><h3>0 registros encontrados</h3></td></tr>";
	}
}else{
	echo "<tr><td colspan='4'><h3>0 registros encontrados</h3></td></tr>";
} ?>
    </tbody>
</table>