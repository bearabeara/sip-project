<?php if(isset($historial)){?>
	<table class="table table-bordered" style="font-size:.75em">
		<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
	    </thead>
	    <tbody>
	<?php 
	if(isset($historial)){
		if(count($historial)>0){
			for ($i=0; $i < count($historial) ; $i++) { $alerta=$historial[$i];
	?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td><?php echo $alerta->fecha;?></td>
			<td><?php echo $alerta->usuario;?></td>
			<td><?php echo $this->lib->alert_producto($alerta);?></td>
		</tr>
	<?php
			}
		}else{
			echo "<tr><td colspan='4'><h3>0 registros encontrados</h3></td></tr>";
		}
	}else{
		echo "<tr><td colspan='4'><h3>0 registros encontrados</h3></td></tr>";
	} ?>
	    </tbody>
	</table>
<?php }else{ 
		if(isset($historial_producto) && isset($historial_categoria)){
	$vector = array();
	for($i=0; $i < count($historial_producto) ; $i++){ $h=$historial_producto[$i];
		$vector[] = array('idp' => $h->idp,'producto' => $h->producto,'idpgr' => 'none','idgr' => 'none','grupo' => 'none','idpgrc' => 'none','idco' => 'none','color' => 'none','accion' => $h->accion,'msj_adicional' => $h->msj_adicional,'idu' => $h->idu,'usuario' => $h->usuario,'fecha' => $h->fecha);
	}
	for($i=0; $i < count($historial_categoria) ; $i++){ $h=$historial_categoria[$i];
		$vector[] = array('idp' => $h->idp,'producto' => $h->producto,'idpgr' => $h->idpgr,'idgr' => $h->idgr,'grupo' => $h->grupo,'idpgrc' => $h->idpgrc,'idco' => $h->idco,'color' => $h->color,'accion' => $h->accion,'msj_adicional' => $h->msj_adicional,'idu' => $h->idu,'usuario' => $h->usuario,'fecha' => $h->fecha);
	}
	foreach ($vector as $key => $row) {
	    $aux[$key] = $row['fecha'];
	}
	if(count($vector)>0){ array_multisort($aux, SORT_DESC, $vector); } ?>
	<table class="table table-bordered" style="font-size:.75em">
		<thead>
		    <tr>
		    	<th width="5%">#</th>
		    	<th width="15%">Fecha</th>
		    	<th width="20%">Usuario</th>
		    	<th width="60%">Acción</th>
		    </tr>
	    </thead>
	    <tbody>
	<?php 
		$i=1;
		foreach($vector as $key => $row){
			$alerta=json_decode(json_encode($row));
	?>
		<tr>
			<td><?php echo $i++;?></td>
			<td><?php echo $alerta->fecha;?></td>
			<td><?php echo $alerta->usuario;?></td>
			<?php $msj=$this->lib->alert_producto($alerta);
				if($msj==""){ $msj=$this->lib->alert_producto_grupo_color($alerta); }
			?>
			<td><?php echo $msj;?></td>
		</tr>
	<?php } ?>
	    </tbody>
	</table>
		<?php }?>
<?php }?>