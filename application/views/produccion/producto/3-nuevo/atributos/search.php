<div class="list-group">
	<div class="list-group-item" id="search_2" style="max-width:100%">
		<table cellspacing="0" cellpadding="0">
			<tbody>
				<tr>
					<td width="100%">
						<form class="form_search_atributo">
							<input id="s2_atr" type="search" class="form-control form-control-sm" placeholder="Atributo" data-type="<?php echo $type;?>" <?php if($type=="grupo"){ ?>data-pg="<?php echo $idpg;?>"<?php }?><?php if($type=="color"){ ?>data-pgc="<?php echo $idpgc;?>"<?php }?>>
						</form>
					</td>
					<td>
						<?php $this->load->view('estructura/botones/buscador2',['id' => '2','f_buscar'=>json_encode(array("funcion" => "search_atributo","atributos"=>"")),'f_ver'=>json_encode(array("funcion" => "all_atributo","atributos"=>"")),'f_nuevo'=>json_encode(array("funcion" => "new_atributo","atributos"=>""))]);?>
					</td>
				</tr>		
			</tbody>
		</table>
	</div>
	<div class="list-group-item" style="max-width:100%" id="contenido_2"></div>
</div>
<script>$("form.form_search_atributo").submit(function(e){ console.log("Entro"); $(this).search_atributo();e.preventDefault();});$("input#s2_atr").keyup(function(){$(this).reset_input($(this).attr("id"));});
</script>