<?php
	$help1='title="Nombre de atributo" data-content="Ingrese un nombre alfanumerico de 2 a 100 caracteres <b>puede incluir espacios espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
	$popover='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th width="80%">Atributos</th>
			<th width="10%"></th>
			<th width="5%"></th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i < count($atributos); $i++){ $atributo=$atributos[$i]; ?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
				<form class="update_atributo" data-a="<?php echo $atributo->idatr;?>">
					<div class="input-group">
						<input type="text" class="form-control form-control-sm color-class" id="atr<?php echo $atributo->idatr;?>" value="<?php echo $atributo->atributo;?>" maxlength="100" minlength="2">
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
			</td>
			<td>
			<div class="g-control-accordion" style="width:60px;">
	      		<button class="update_atributo" data-a="<?php echo $atributo->idatr;?>"><i class="icon-floppy-o"></i></button>
      			<button class="confirm_atributo" data-a="<?php echo $atributo->idatr;?>"><i class="icon-trashcan2"></i></button>
			</div>
			<td>
				<button type="button" class="btn btn-inverse-primary btn-mini waves-effect waves-light adicionar_atributo" data-a="<?php echo $atributo->idatr?>">
	                <i class="fa fa-plus"></i> add
	            </button>
			</td>
		</tr>		
<?php }?>
	</tbody>
</table>
<script>$('[data-toggle="popover"]').popover({html:true});$(".color-class").maxlength();$('.update_atributo').update_atributo();$("button.confirm_atributo").confirm_atributo();$("button.adicionar_atributo").adicionar_atributo();</script>