<?php
	$help1='title="Código" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help2='title="Código anterior" data-content="Ingrese un código alfanumerico de 2 a 20 caracteres <b>sin espacios</b>, el codigo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help3='title="Nombre de producto" data-content="Ingrese un nombre alfanumerico de 2 a 90 caracteres <b>puede incluir espacios</b>, solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help4='title="Fecha de creación" data-content="Seleccione la fecha cuando se creo o finalizo el diseño del producto. en formato 2000-01-31"';
	$help5='title="Observaciónes" data-content="Ingrese un descripcion alfanumerica hasta 500 caracteres <b>puede incluir espacios, sin saltos de linea</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$popover4='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><span class='text-danger'>(*)</span>Código:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="form_save_producto">
						<div class="input-group">
							<input class='form-control form-control-sm color-class' type="text" id="cod_1" placeholder='Código del producto' maxlength="20" minlength="2">
							<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<script type="text/javascript">foco("cod_1");</script>
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Fecha de creación:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<input class='form-control form-control-sm' type="date" id="fec" placeholder='2000-01-31' value="<?php echo date('Y-m-d')?>">
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help4;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label"><span class='text-danger'>(*)</span>Nombre:</label>
				<div class="col-sm-10 col-xs-12">
					<form class="form_save_producto">
						<div class="input-group">
							<input class='form-control form-control-sm color-class' type="text" id="nom" placeholder='Nombre de Producto' maxlength="90" minlength="2">
							<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Observaciónes:</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<textarea id="obs" class="form-control form-control-sm color-class" placeholder="Observaciónes del producto" maxlength="500" rows="3" minlength="0"></textarea>
						<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help5;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12 col-form-label form-control-label">Código anterior:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="form_save_producto">
						<div class="input-group">
							<input class='form-control form-control-sm color-class' type="text" id="cod_2" placeholder='Código anterior' maxlength="20" minlength="0">
							<span tabindex="0" class="input-group-addon form-control-sm" <?php echo $popover4.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row"><div class="form-group" id="form_atr"></div></div>
		<div class="row text-right" style="padding-right:15px;">
			<button type="button" class="btn btn-primary btn-mini waves-effect waves-light atributos" data-type="producto">
				<i class="fa fa-plus"></i><span class="m-l-10">Adicionar atributo</span>
			</button>
		</div>
	</div>	  
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$(".color-class").maxlength(); $("form.form_save_producto").submit(function(e){ save_producto($(this)); e.preventDefault();});$("button.atributos").atributos();</script>