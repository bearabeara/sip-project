<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_producto', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'produccion/excel_productos?cod='.$cod.'&cod2='.$cod2."&nom=".$nom."&fec=".$fec."&est=".$est);
		$word = array('controller' => 'produccion/word_productos?cod='.$cod.'&cod2='.$cod2."&nom=".$nom."&fec=".$fec."&est=".$est);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
					<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Código</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 9%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Código anterior</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 18%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre de producto</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 14%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fecha de creación</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 16%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Categorías</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 7%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Estado</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 21%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
					</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
				<div class="table-responsive" id="area">
					<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE PRODUCTOS']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="3%" style="display: none;">#Item</th>
								<th class="celda th padding-4" data-column="2" width="3%">Fotografía</th>
								<th class="celda th padding-4" data-column="3" width="9%">Código</th>
								<th class="celda th padding-4" data-column="4" width="9%">Código anterior</th>
								<th class="celda th padding-4" data-column="5" width="18%">Nombre de producto</th>
								<th class="celda th padding-4" data-column="6" width="14%" >Fecha de creación</th>
								<th class="celda th padding-4" data-column="7" width="16%" >Categorías</th>
								<th class="celda th padding-4" data-column="8" width="7%" >Estado</th>
								<th class="celda th padding-4" data-column="9" width="21%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){
									$producto=$this->lib->search_elemento($productos,"idp",$visible);
									if($producto!=null){
										$cont++;
										$img="sistema/miniatura/default-print.jpg";
										if($producto->portada!="" && $producto->portada!=null){
											$v=explode("|", $producto->portada);
											if(count($v==2)){
												if($v[0]=="1"){ $control_img=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control_img)){ $img="productos/miniatura/".$control_img[0]->archivo;}}
												if($v[0]=="3"){ $control_img=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control_img)){ $img="productos/miniatura/".$control_img[0]->archivo;}}
											}
										}
										$grupos=$this->lib->select_from($grupos_colores,"idp",$producto->idp);
										$estado="";
										if($producto->estado=="0"){ $estado="Inactivo";}if($producto->estado=="1"){ $estado="Activo";}
							?>
								<tr class="fila">
									<td class="celda td padding-4"  data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $producto->codigo;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $producto->codigo_aux;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $producto->nombre;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $this->lib->format_date($producto->fecha_creacion,'dl ml Y'); ?></td>
									<td class="celda td padding-4" data-column="7">
									<?php for($c=0; $c < count($grupos) ; $c++){$grupo=$grupos[$c];?>
										<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
												$co="";if($grupo->nombre_c!=""){ $co=$grupo->nombre_c;}
										?>
											<div style="border: 1px solid rgba(0,0,0,.3);"><?php echo $gr.$co;?></div>
									<?php }?>
									</td>
									<td class="celda td padding-4" data-column="8"><?php echo $estado;?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $producto->observaciones;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
					</table>
				</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>