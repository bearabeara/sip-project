<?php $url=base_url().'libraries/img/';
	$fecha=date("Y-m-d");
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
	header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
	header("Content-Disposition: attachment; filename=Productos-$fecha.xls");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<table border="1">
	<thead>
		<tr>
			<td colspan="8" style="text-align: center;">
				<H3>REGISTRO DE PRODUCTOS</H3>
			</td>
		</tr>
		<tr class="fila">
			<th width="3%">#Item</th>
			<th width="9%">Código</th>
			<th width="9%">Código anterior</th>
			<th width="18%">Nombre de producto</th>
			<th width="14%" >Fecha de creación</th>
			<th width="16%" >Categorías</th>
			<th width="7%" >Estado</th>
			<th width="31%">Observaciónes</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont=0;
		for ($i=0; $i < count($productos); $i++){ $producto=$productos[$i];
				$cont++;
				$grupos=$this->lib->select_from($grupos_colores,"idp",$producto->idp);
				$estado="";
				if($producto->estado=="0"){ $estado="Inactivo";}if($producto->estado=="1"){ $estado="Activo";}
				?>
				<tr>
					<td><?php echo intval($cont);?></td>
					<td><?php echo $producto->codigo;?></td>
					<td><?php echo $producto->codigo_aux;?></td>
					<td><?php echo $producto->nombre;?></td>
					<td><?php echo $this->lib->format_date($producto->fecha_creacion,'Y-m-d'); ?></td>
					<td>
						<?php for ($c=0; $c < count($grupos) ; $c++) { $grupo=json_decode($grupos[$c]); ?>
						<?php $gr="";if($grupo->abr_g!=""){ $gr=$grupo->nombre_g." - ";}
						$co="";if($grupo->nombre_c!=""){ $co=$grupo->nombre_c;}
						?>
						<div style="border: 1px solid rgba(0,0,0,.3);"><?php echo $gr.$co;?></div>
						<?php }?>
					</td>
					<td><?php echo $estado;?></td>
					<td><?php echo $producto->observaciones;?></td>
				</tr>
			<?php }//end for ?>
					</tbody>
				</table>