<table cellspacing="0" cellpadding="0">
	<tr>
		<td class='hidden-sm img-thumbnail-60'><div class="img-thumbnail-60"></div></td>
		<td class='hidden-sm celda-sm-10'>
			<form class="view_producto" data-type="search">
				<input id='s_cod' type="search" class="form-control form-control-sm search_produccion" placeholder="Código" data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="2" data-module-search="productos">
			</form>
		</td>
		<td class='hidden-sm celda-sm-10 hidden-sm'><form class="view_producto" data-type="search"><input id='s_cod2' type="search" class="form-control form-control-sm search_produccion" placeholder="Código anterior" data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="3" data-module-search="productos"></form></td>
		<td class='celda-sm-30'>
			<form class="view_producto" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_produccion" maxlength="100" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="4" data-module-search="productos"/>
					<span class="input-group-addon form-control-sm view_producto" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class="hidden-md" style="width:16%"><input id='s_fec' type="date" class="form-control form-control-sm search_produccion" placeholder="2000-01-31" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="5" data-module-search="productos"></td>
		<td class="" style="width:20%">
			<select id="s_est" class="form-control form-control-sm view_producto">
				<option value="2">Todos</option>
				<option value="1" selected="selected">Productos activos</option>
				<option value="0">Productos inactivos</option>
			</select>
		</td>
		<td class="hidden-sm" style="width:14%"></td>
		<td class="hidden-sm">
			<?php 
				$search=json_encode(array('function'=>'view_producto','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_producto','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>		
</table>
<script>Onfocus('s_cod');$(".search_produccion").search_produccion();$(".view_producto").view_producto();$("form.search_producto").submit(function(e){ $(this).view_producto(); e.preventDefault()});$("input.form-control ").keyup(function(){ $(this).reset_input($(this).attr("id"));});$("input[type=date].form-control ").change(function(){ $(this).reset_input($(this).attr("id"));});</script>