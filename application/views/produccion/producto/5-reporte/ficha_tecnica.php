<?php 
	$url=base_url().'libraries/img/';
	$idpgrc="";
	$text_grupo_color="";
	$rand=rand(10,99999);
	if(count($grupos_colores)>0){if(!isset($pgc)){$idpgrc=$grupos_colores[0]->idpgrc;}else{$idpgrc=$pgc;}}
	$color_producto=null;
	if($idpgrc!=""){$color_producto=$this->lib->search_elemento($grupos_colores,'idpgrc',$idpgrc);}
	//Fotografias del producto
	$images=array();
	if($idpgrc!=""){
		$fotografias_color=$this->M_producto_imagen_color->get_row('idpgrc',$idpgrc);
		for($i=0; $i<count($fotografias_color); $i++){ $image=$fotografias_color[$i];
			$images[]=array('img'=>'productos/'.$image->archivo,'descripcion'=>$image->descripcion,'codigo_portada'=>'3|'.$image->idpig,'portada'=>false);
		}
	}
	$fotografias_producto=$this->M_producto_imagen->get_row('idp',$producto->idp);
	for($i=0; $i<count($fotografias_producto); $i++){ $image=$fotografias_producto[$i];
		$images[]=array('img'=>'productos/'.$image->archivo,'descripcion'=>$image->descripcion,'codigo_portada'=>'1|'.$image->idpi,'portada'=>false);
	}
	//End fotografias del producto
	//Portada de producto
	$encontro_portada=false;
	if($idpgrc!="" && $color_producto!=null){
		for($i=0; $i < count($images); $i++){$image=$images[$i];
			if($images[$i]['codigo_portada']==$color_producto->portada){
				$encontro_portada=true;
				$images[$i]['portada']=true;
				break;
			}
		}
	}
	if(!$encontro_portada){
		for($i=0; $i < count($images); $i++){$image=$images[$i];
			if($images[$i]['codigo_portada']==$producto->portada){
				$encontro_portada=true;
				$images[$i]['portada']=true;
				break;
			}
		}
	}
	//End portada de productos
	//Datos de producto
	$nombre=$producto->nombre;
	if($color_producto!=null){
		$gr="";$co="";
		if($color_producto->abr_g!=""){$gr=$color_producto->nombre_g." - ";}
		if($color_producto->nombre_c!=""){$co=$color_producto->nombre_c;}
		$nombre.=" - ".$gr.$co;
	}
	$producto=json_decode(json_encode(array('idp'=>$producto->idp,'nombre'=>$nombre,'codigo'=>$producto->codigo,'codigo_aux'=>$producto->codigo_aux,'fecha_creacion'=>$producto->fecha_creacion,'observaciones'=>$producto->observaciones,'portada'=>$producto->portada)));
	$j_images=json_decode(json_encode($images));
	//End datos de producto
	//atributos en el producto
	$atributos = array();
	for($i=0;$i<count($producto_atributos);$i++){
		$atributos[]=array('atributo'=>$producto_atributos[$i]->atributo,'valor'=>$producto_atributos[$i]->valor);
	}
	if($color_producto!=null){
		$producto_grupo_atributos=$this->M_producto_grupo_atributo->get_atributo('pga.idpgr',$color_producto->idpgr);
		for($i=0;$i<count($producto_grupo_atributos);$i++){
			$atributos[]=array('atributo'=>$producto_grupo_atributos[$i]->atributo,'valor'=>$producto_grupo_atributos[$i]->valor);
		}
		$producto_grupo_color_atributos=$this->M_producto_grupo_color_atributo->get_atributo('pgca.idpgrc',$color_producto->idpgrc);
		for($i=0;$i<count($producto_grupo_color_atributos);$i++){
			$atributos[]=array('atributo'=>$producto_grupo_color_atributos[$i]->atributo,'valor'=>$producto_grupo_color_atributos[$i]->valor);
		}
	}
	$atributos=$this->lib->sort_2d($atributos,"atributo","ASC");
	$atributos_tr = array();
	$aux=array();
	$i=0;
	foreach($atributos as $key => $atributo){
		$aux[]=array('atributo'=>$atributo->atributo,'valor'=>$atributo->valor);
		$i++;
		if($i%5==0){
			$atributos_tr[]=$aux;
			$aux=array();
			$i=0;
		}
	}
	if(count($aux)>0){$atributos_tr[]=$aux;}
	$atributos_tr=json_decode(json_encode($atributos_tr));
	//End atributos en el producto
	//Materiales en el producto
	$materiales=array();
	for($i=0;$i<count($producto_materiales);$i++){ $material=$producto_materiales[$i];
		$unidad=$this->lib->search_elemento($unidades,'idu',$material->idu);
		$nombre_u="";$abr="";if($unidad!=null){$nombre_u=$unidad->nombre;$abr=$unidad->abr;}
		$img="sistema/miniatura/default.jpg";
		if($material->fotografia!="" && $material->fotografia!=NULL){$img="materiales/miniatura/".$material->fotografia;}
		$materiales[]=array('idmi'=>$material->idmi,'codigo'=>$material->codigo,'nombre'=>$material->nombre,'color'=>$material->nombre_c,'codigo_color'=>$material->codigo_c,'cantidad'=>$material->cantidad,'estado'=>$material->verificado,'observaciones'=>$material->observacion,'unidad'=>$nombre_u,'abr'=>$abr,'img'=>$img);
	}
	if($color_producto!=null){
		$producto_grupo_materiales=$this->M_producto_grupo_material->get_material('pgm.idpgr',$color_producto->idpgr);
		for($i=0; $i < count($producto_grupo_materiales); $i++){ $material=$producto_grupo_materiales[$i];
			$unidad=$this->lib->search_elemento($unidades,'idu',$material->idu);
			$nombre_u="";$abr="";if($unidad!=null){$nombre_u=$unidad->nombre;$abr=$unidad->abr;}
			$img="sistema/miniatura/default.jpg";
			if($material->fotografia!="" && $material->fotografia!=NULL){$img="materiales/miniatura/".$material->fotografia;}
			$materiales[]=array('idmi'=>$material->idmi,'codigo'=>$material->codigo,'nombre'=>$material->nombre,'color'=>$material->nombre_c,'codigo_color'=>$material->codigo_c,'cantidad'=>$material->cantidad,'estado'=>$material->verificado,'observaciones'=>$material->observacion,'unidad'=>$nombre_u,'abr'=>$abr,'img'=>$img);
		}
		$producto_grupo_color_materiales=$this->M_producto_grupo_color_material->get_material('pgcm.idpgrc',$color_producto->idpgrc);
		for($i=0; $i < count($producto_grupo_color_materiales); $i++){ $material=$producto_grupo_color_materiales[$i];
			$unidad=$this->lib->search_elemento($unidades,'idu',$material->idu);
			$nombre_u="";$abr="";if($unidad!=null){$nombre_u=$unidad->nombre;$abr=$unidad->abr;}
			$img="sistema/miniatura/default.jpg";
			if($material->fotografia!="" && $material->fotografia!=NULL){$img="materiales/miniatura/".$material->fotografia;}
			$materiales[]=array('idmi'=>$material->idmi,'codigo'=>$material->codigo,'nombre'=>$material->nombre,'color'=>$material->nombre_c,'codigo_color'=>$material->codigo_c,'cantidad'=>$material->cantidad,'estado'=>$material->verificado,'observaciones'=>$material->observacion,'unidad'=>$nombre_u,'abr'=>$abr,'img'=>$img);
		}
	}
	$materiales=$this->lib->sort_2d($materiales,"nombre","ASC");
	$materiales_tr = array();
	$aux=array();
	$i=0;
	foreach($materiales as $key => $material){
		$aux[]=array('idmi'=>$material->idmi,'codigo'=>$material->codigo,'nombre'=>$material->nombre,'color'=>$material->color,'codigo_color'=>$material->codigo_color,'cantidad'=>$material->cantidad,'estado'=>$material->estado,'observaciones'=>$material->observaciones,'unidad'=>$material->unidad,'abr'=>$material->abr,'img'=>$material->img);
		$i++;
		if($i%5==0){
			$materiales_tr[]=$aux;
			$aux=array();
			$i=0;
		}
	}
	if(count($aux)>0){$materiales_tr[]=$aux;}
	$materiales_tr=json_decode(json_encode($materiales_tr));
	//End materiales en el producto
	//Manejo de piezas
	$piezas = array();
	for($i=0;$i<count($producto_piezas);$i++){ $pieza=$producto_piezas[$i];
		$img="sistema/miniatura/default.jpg";
		$nombre_material="Ninguno";
		$repujado_sello="Ninguno";
		$tipo_rep_sell="";
		$pieza_material=$this->lib->search_elemento($all_materiales,'idm',$pieza->idm);
		if($pieza_material!=null){$nombre_material=$pieza_material->nombre;}
		$pieza_repujado_sello=$this->lib->search_elemento($all_repujados_sellos,'idrs',$pieza->idrs);
		if($pieza_repujado_sello!=null){
			$repujado_sello=$pieza_repujado_sello->nombre;
			if($pieza_repujado_sello->tipo==0){$tipo_rep_sell="Repujado";}
			if($pieza_repujado_sello->tipo==1){$tipo_rep_sell="Sello";}
		}
		if($pieza->fotografia!="" && $pieza->fotografia!=NULL){$img="piezas/miniatura/".$pieza->fotografia;}
		$piezas[]=array('nombre'=>$pieza->nombre,'largo'=>$pieza->largo,'ancho'=>$pieza->ancho,'cantidad'=>$pieza->cantidad,'descripcion'=>$pieza->descripcion,'img'=>$img,'nombre_material'=>$nombre_material,'repujado_sello'=>$repujado_sello,'tipo_repujado_sello'=>$tipo_rep_sell);
	}
	if($color_producto!=null){
		$producto_grupo_color_piezas=$this->M_producto_grupo_color_pieza->get_row('idpgrc',$color_producto->idpgrc);
		for($i=0;$i<count($producto_grupo_color_piezas);$i++){ $pieza=$producto_grupo_color_piezas[$i];
			$img="sistema/miniatura/default.jpg";
			$nombre_material="Ninguno";
			$repujado_sello="Ninguno";
			$tipo_rep_sell="Repuj. o sello";
			$pieza_material=$this->lib->search_elemento($all_materiales,'idm',$pieza->idm);
			if($pieza_material!=null){$nombre_material=$pieza_material->nombre;}
			$pieza_repujado_sello=$this->lib->search_elemento($all_repujados_sellos,'idrs',$pieza->idrs);
			if($pieza_repujado_sello!=null){
				$repujado_sello=$pieza_repujado_sello->nombre;
				if($pieza_repujado_sello->tipo==0){$tipo_rep_sell="Repujado";}
				if($pieza_repujado_sello->tipo==1){$tipo_rep_sell="Sello";}
			}
			if($pieza->fotografia!="" && $pieza->fotografia!=NULL){$img="piezas/miniatura/".$pieza->fotografia;}
			$piezas[]=array('nombre'=>$pieza->nombre,'largo'=>$pieza->largo,'ancho'=>$pieza->ancho,'cantidad'=>$pieza->cantidad,'descripcion'=>$pieza->descripcion,'img'=>$img,'nombre_material'=>$nombre_material,'repujado_sello'=>$repujado_sello,'tipo_repujado_sello'=>$tipo_rep_sell);
		}
	}
	$piezas=$this->lib->sort_2d($piezas,"nombre","ASC");
	$piezas_tr = array();
	$aux=array();
	$i=0;
	foreach($piezas as $key => $pieza){
		$aux[]=array('nombre'=>$pieza->nombre,'largo'=>$pieza->largo,'ancho'=>$pieza->ancho,'cantidad'=>$pieza->cantidad,'descripcion'=>$pieza->descripcion,'img'=>$pieza->img,'nombre_material'=>$pieza->nombre_material,'repujado_sello'=>$pieza->repujado_sello,'tipo_repujado_sello'=>$pieza->tipo_repujado_sello);
		$i++;
		if($i%5==0){
			$piezas_tr[]=$aux;
			$aux=array();
			$i=0;
		}
	}
	if(count($aux)>0){$piezas_tr[]=$aux;}
	$piezas_tr=json_decode(json_encode($piezas_tr));
	//End manejo de piezas
?>                                     

<?php
	if(count($grupos_colores)>1){
		for($i=0;$i<count($grupos_colores);$i++){ $grupo=$grupos_colores[$i];
			$gr="";if($grupo->abr_g!=""){$gr=$grupo->nombre_g." - ";}
			$co="";if($grupo->nombre_c!=""){$co=$grupo->nombre_c;}
			$seleccionado=false;
			if($idpgrc==$grupo->idpgrc){
				$seleccionado=true;
				$text_grupo_color=$gr.$co;
			}
?>
			<button type="button" class="btn btn-<?php if(!$seleccionado){?>inverse-<?php }?>info btn-mini waves-effect view_reportes<?php echo $rand;?>" data-p="<?php echo $producto->idp;?>" data-pgc="<?php echo $grupo->idpgrc;?>" style="margin-bottom: 5px;"><?php echo $gr.$co;?></button>
<?php 	}
	}
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php
		$fun = array('function' => 'view_reportes', 'atribs' => array('p' => $producto->idp,'pgc' => $idpgrc));
		$excel = array('controller' => 'produccion/excel_producto?p='.$producto->idp.'&pgc='.$idpgrc);
		$word = array('controller' => 'produccion/word_producto?p='.$producto->idp.'&pgc='.$idpgrc);
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row" id="datos-producto" data-p="<?php echo $producto->idp;?>">
		  <div class="col-xs-12">
		    <span class="g-control-accordion">
		      <button class="producto_seg" data-p="<?php echo $producto->idp;?>" data-type="producto_complet"><i class="fa fa-clock-o"></i></button>
		    </span>
		  </div>
		</div>
		<div class="row-border table-responsive" id="area" style="overflow-y: hidden;">
			<table class="tabla tbl-bordered font-10" style="margin-bottom: 15px;">
				<thead>
					<tr class="fila title" style="text-align: center;">
						<td class="celda title" colspan="13" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
							<?php $this->load->view('estructura/print/header-print',['titulo'=>'FICHA TÉCNICA']);?>
						</td>
					</tr>
					<tr class="fila">
						<th class="celda th padding-4" colspan="5"><h2><?php echo $producto->nombre;?></h2></th>
					</tr>
				</thead>
				<tbody>
					<tr class="fila">
						<td class="celda td  text-center" rowspan="3" colspan="2" width="40%">
							<?php $portada="sistema/default-print.jpg";$desc="";
								foreach($j_images as $key => $image){
									if($image->portada){
										$portada=$image->img;
										$desc=$image->descripcion;
										break;
									}
								}
							?>
								<img src="<?php echo $url.$portada;?>" alt="img" width="85%" class="img-thumbnail" data-title="<?php echo $producto->nombre;?>" data-desc="<?php if($desc!=""){ echo $desc;}else{ echo '<br>';}?>">
								<?php if($desc!=""){?><br><label><?php echo $desc;?></label><?php }?>
						</td>
						<th class="celda th padding-4" width="20%">Codigo</th>
						<th class="celda th padding-4" width="20%">Código anterior</th>
						<th class="celda th padding-4" width="20%">Fecha de creación</th>
					</tr>
					<tr class="fila">
						<td class="celda td padding-4"><?php echo $producto->codigo;?></td>
						<td class="celda td padding-4"><?php echo $producto->codigo_aux;?></td>
						<td class="celda td padding-4"><?php echo $this->lib->format_date($producto->fecha_creacion,'dl ml Y');?></td>
					</tr>
					<tr class="fila">
					<?php $td=0;
						foreach($j_images as $key => $image){
							 if(!$image->portada){
					?>
							<td class="celda td padding-4 text-center">
								<img src="<?php echo $url.$image->img;?>" alt="image" width="90%" class="img-thumbnail" data-title="<?php echo $producto->nombre;?>" data-desc="<?php if($image->descripcion!=''){echo $image->descripcion;}else{echo '<br>';}?>">
								<?php if($image->descripcion!=""){?><br><label><?php echo $image->descripcion;?></label><?php }?>
							</td>
					<?php
								$td++;
							 }
							if($td>2){break;}
						}
						for($i=0; $i <(3-count($images));$i++){ 
					?>
						<td class="celda td padding-4 text-center">
							<img src="<?php echo $url.'sistema/default-print.jpg';?>" alt="image" width="90%" class="img-thumbnail" data-title="<br>" data-desc="<br>">
						</td>
					<?php 
						}
					?>
					</tr>
				<?php if(count($images)>4){?>
					<tr class="fila">
				<?php $td=0;
					for($i=4; $i < count($images); $i++){ $image=$images[$i];
						if($td>4){
							$td=0;
				?>
					</tr>
					<tr class="fila">
				<?php
						}// end if
				?>
						<td class="celda td padding-4 text-center">
							<img src="<?php echo $url.$image['img'];?>" alt="image" width="90%" class="img-thumbnail" data-title="<?php echo $producto->nombre;?>" data-desc="<?php if($image['descripcion']!=""){ echo $image['descripcion'];}else{ echo '<br>';}?>">
							<?php if($image['descripcion']!=""){?><br><label><?php echo $image['descripcion'];?></label><?php }?>
						</td>
				<?php
						$td++;
					}//end for
					for($i=0; $i < 5-$td; $i++){
				?>
						<td class="celda td padding-4 text-center">
							<img src="<?php echo $url.'sistema/default-print.jpg';?>" alt="image" width="90%" class="img-thumbnail" data-title="<br>" data-desc="<br>">
						</td>
				<?php 
					}// end for
				?>
					</tr>
				<?php }//end if?>
					<tr class="fila"><th class="celda td" colspan="5">Observaciónes</th></tr>
					<tr class="fila"><td class="celda td" colspan="5"><?php if($producto->observaciones!=""){echo $producto->observaciones;}else{ echo "-";}?></td>
					</tr>
				<?php
					foreach($atributos_tr as $key => $atributo_tr){
				?>
					<tr class="fila">
					<?php $tds=0;
						foreach ($atributo_tr as $key => $atributo){?>
						<th class="celda th padding-4"><?php echo $atributo->atributo;?></th>
					<?php $tds++;}//end foreach
						for($i=0;$i<5-$tds;$i++){
					?>
						<th class="celda th"></th>
					<?php }//end for?>
					</tr>
					<tr class="fila">
					<?php $tds=0;
						foreach ($atributo_tr as $key => $atributo){?>
						<td class="celda td padding-4"><?php echo $atributo->valor;?></td>
					<?php $tds++;}
						for($i=0;$i<5-$tds;$i++){
					?>
						<th class="celda th"></th>
					<?php }//end for ?>
					</tr>
				<?php
					}// end foreach
				?>
				<tr class="fila">
					<th class="celda th padding-4" colspan="5"><ins>MATERIALES</ins></th>
				</tr>
				<?php
				if(!empty($materiales)){
					foreach($materiales_tr as $key => $material_tr){
				?>
					<tr class="fila">
					<?php $tds=0;
						foreach ($material_tr as $key => $material){?>
						<td class="celda td" style="text-align: left !important; min-width: 110px;">
							<table class="tabla tbl-no-bordered font-10">
								<tr class="fila">
									<td class="img-thumbnail-45 celda td" style="vertical-align: top;"><div class="img-thumbnail-45">
									<img src="<?php echo $url.$material->img;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $material->nombre;?>" data-desc="<?php echo '<strong>Cantidad: </strong>'.$material->cantidad.'['.$material->abr.'.]. Observaciónes: '.$material->observaciones;?>">
									</div></td>
									<td style="padding: 0px 0px 0px 5px;" width="100%" class="celda td">
										<ins><?php echo $material->nombre;?></ins><br>
										<small><strong>Color: </strong><?php echo $material->color;?><br>
										<strong>Estado: </strong><?php if($material->estado==0){echo "Verificado";}else{ echo "No verificado";}?></small>
									</td>
								</tr>
								<tr class="fila">
									<td class="td celda td" colspan="2">
										<strong>Cantidad: </strong><?php echo $material->cantidad.'['.$material->abr.'.]';?><br>
										<strong>Observaciónes: </strong><?php echo $material->observaciones;?>
									</td>
								</tr>
							</table>
						</td>
					<?php $tds++;}//end foreach
						for($i=0;$i<5-$tds;$i++){
					?>
						<th class="celda th"></th>
					<?php }//end for?>
					</tr>
				<?php
					}// end foreach
				}else{//end if
				?>
				<tr class="fila">
					<td class="celda td padding-4" colspan="5">0 Materiales asignados.</td>
				</tr>
			<?php }?>
				<tr class="fila">
					<th class="celda th padding-4" colspan="5"><ins>PIEZAS</ins></th>
				</tr>
			<?php if(!empty($piezas)){
				foreach ($piezas_tr as $key => $pieza_tr) {
			?>
				<tr class="fila">
					<?php $tds=0;
						foreach ($pieza_tr as $key => $pieza){
							$dimensiones=$pieza->largo.'[cm.] x '.$pieza->ancho.'[cm.]';
					?>
						<td class="celda td" style="text-align: left !important; min-width: 110px;">
							<table class="tabla tbl-no-bordered font-10">
								<tr class="fila">
									<td class="img-thumbnail-45 celda td" style="vertical-align: top;"><div class="img-thumbnail-45">
									<img src="<?php echo $url.$pieza->img;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $pieza->nombre;?>" data-desc="<?php echo '<strong>Cantidad: </strong>'.$pieza->cantidad.'[u.]. Dimensiónes: '.$dimensiones;?>">
									</div></td>
									<td style="padding: 0px 0px 0px 5px;" width="100%" class="celda td">
										<ins><?php echo $pieza->nombre;?></ins><br>
										<small><?php echo $dimensiones;?><br>
									<?php if($pieza->tipo_repujado_sello!=""){?>
										<strong><?php echo $pieza->tipo_repujado_sello;?>: </strong><?php echo $pieza->repujado_sello;?></small>
									<?php }?>
									</td>
								</tr>
								<tr class="fila">
									<td class="td celda td" colspan="2">
										<strong>Cantidad: </strong><?php echo $pieza->cantidad.'[u.]';?><br>
										<strong>Material: </strong><?php echo $pieza->nombre_material;?><br>
										<strong>Observaciónes: </strong><?php echo $pieza->descripcion;?>
									</td>
								</tr>
							</table>
						</td>
					<?php $tds++;}//end foreach
						for($i=0;$i<5-$tds;$i++){
					?>
						<th class="celda th"></th>
					<?php }//end for?>
				</tr>
			<?php
				}//end foreach
			?>

			<?php }else{?>
				<tr class="fila">
					<td class="celda td padding-4" colspan="5">0 Piezas resgistradas.</td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
		</div>



	</div>
</div>
<script>
$(".view_reportes<?php echo $rand;?>").view_reportes();
$("img.img-thumbnail").visor();$("button.producto_seg").producto_seg();$("button.producto_grupo_color_seg").producto_grupo_color_seg();</script>