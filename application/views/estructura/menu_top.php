<?php $opciones=explode("|", $menu);
$url=base_url()."libraries/img/";
  $img="sistema/avatar.jpg"; if($this->session->userdata('fotografia')!=""){ $img="personas/miniatura/".$this->session->userdata('fotografia');}
?>
    <header class="main-header-top hidden-print">
        <nav class="navbar navbar-static-top">
            <a href="#!" data-toggle="offcanvas" class="sidebar-toggle"></a>
            <div class="content-tabs">
            <ul class="nav nav-tabs md-tabs hidden-sm" role="tablist" id="menu-top">
            <?php for ($i=0; $i < count($opciones) ; $i++) { $opcion=explode("/", $opciones[$i]);
                    if(count($opcion)>1){
                      if(isset($opcion[2])){$icon=$opcion[2];}else{$icon="";}
            ?>
                <li class="nav-item" style="float: left;">
                    <a class="nav-link" href="javascript:" role="tab" id="<?php echo $opcion[1];?>"><i class="<?php echo $icon;?>  "></i> <span><?php echo $opcion[0];?><span></a>
                    <div class="slide"></div>
                </li>
            <?php   }
                }//end for ?>
            </ul>
            </div>
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!-- chat cantidad de recibidos -->
                    <li class="pc-rheader-submenu">
                        <a href="javascript:" class="drop icon-circle displayChatbox" onclick="refresh_mensajes_user()">
                            <i class="icon-bubbles"></i>
                            <?php $alerts=$this->M_mensaje->get_row('reseptor',$this->session->userdata("id"));?>
                            <span class="badge <?php if(count($alerts)>0){ echo 'badge-danger blink';}else{ echo 'badge-info';}?> header-badge" id="badge<?php echo $this->session->userdata('id');?>msj"><?php echo count($alerts);?></span>
                        </a>
                    </li>
                    <li class="pc-rheader-submenu"></li>
                    <li class="dropdown notification-menu">
                        <a href="javascript:" data-toggle="dropdown" id="alert_seg" aria-expanded="false">
                            <i class="icon-bell"></i>
                            <span class=""></span>
                            <?php $alerts=$this->M_notificacion->get_row('reseptor',$this->session->userdata("id"));?>
                            <span class="badge <?php if(count($alerts)>0){ echo 'badge-danger blink';}else{ echo 'badge-info';}?> header-badge" id="badge_content"><?php echo count($alerts);?></span>
                        </a>
                        <ul class="dropdown-menu" id="content_alert_seg"></ul>
                    </li>
                    <li class="pc-rheader-submenu"></li>
                    <li class="dropdown notification-menu">
                        <a href="javascript:" data-toggle="dropdown" id="refresh_version" aria-expanded="false">
                            <i class="icon-wrench"></i>
                            <span class=""></span>
                            <?php $alerts=$this->M_actualizacion_usuario->get_actualizacion('au.idus',$this->session->userdata("id"),"ac.estado","0","ac.idac","ASC");?>
                            <span class="badge <?php if(count($alerts)>0){ echo 'badge-danger blink';}else{ echo 'badge-info';}?> header-badge" id="badge_update"><?php echo count($alerts);?></span>
                        </a>
                        <ul class="dropdown-menu" id="content_refresh_version">
                        <?php
                        date_default_timezone_set("America/La_Paz");
                        $ahora=date('Y-m-d H:i:s');
                        for($i=0; $i < count($alerts) ; $i++) { $alert=$alerts[$i]; 
                        ?>
                        <li class="bell-notification">
                            <div class="media" id="<?php echo 'time'.$alert->idac;?>">
                                <span class="text-muted block-time">El sistema será actualizado en:</span>
                                    <div class="row  text-center text-primary">
                                        <div class="col-xs-12">
                                            <h2 class="f-90 f-w-400 counter-text" style="line-height: 75%; margin-top: 10px;">
                                                <font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="dias">0</font></font>
                                            </h2>
                                            <p class="f-24 f-w-400">
                                                <font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-dia">Días</font></font>
                                            </p>
                                        </div>
                                        <div class="col-xs-4">
                                            <h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="horas">00</font></font></h2>
                                            <p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-hora">Horas</font></font></p>
                                        </div>
                                        <div class="col-xs-4">
                                            <h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="minutos">00</font></font></h2>
                                            <p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-minuto">Minutos</font></font></p>
                                        </div>
                                        <div class="col-xs-4">
                                            <h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="segundos">00</font></font></h2>
                                            <p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-segundo">Segundos</font></font></p>
                                        </div>
                                    </div>
                            </div>
                            <script>$(document).ready(function(){$(this).cronometro({inicio: '<?php echo $ahora;?>',fin: '<?php echo $alert->fecha_inicio;?>',id: "div#<?php echo 'time'.$alert->idac;?>",format_dia: false,ac: '<?php echo $alert->idac;?>'});});
                            </script>
                        </li>
                        <?php } ?>
                        </ul>
                    </li>
                <?php if($privilegio->pr=="1" && $privilegio->pr2r=="1"){?>
                    <li class="dropdown notification-menu">
                        <a href="javascript:" data-toggle="dropdown" id="alert_prod" aria-expanded="false">
                            <i class="fa fa-server"></i>
                            <span class=""></span>
                            <?php $alerts=$this->M_pedido->get_row('estado','1');?>
                            <span class="badge <?php if(count($alerts)>0){ echo 'badge-warning blink';}else{ echo 'badge-info';}?> header-badge" id="badge_content"><?php echo count($alerts);?></span>
                        </a>
                        <ul class="dropdown-menu" id="content_alert_prod"></ul>
                    </li>
                <?php }else{echo "<span id='alert_prod'></span>";}?>
                    <li class="pc-rheader-submenu"></li>
                    <!-- User Menu-->
                    <li class="dropdown dropdown-user">
                        <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                            <span><img class="img-circle " src="<?php echo $url.$img;?>" style="width:28px" alt="User Image"></span>
                            <span class="hidden-sm"><?php echo $this->session->userdata("nombre");?></span>
                        </a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="javascript:" id="my_history"><i class="fa fa-clock-o"></i>Mi historial</a></li>
                            <li><a href="javascript:" id="my_config_user"><i class="icon-settings"></i> Ajustes</a></li>
                            <li><a href="javascript:" id="my_pass_user"><i class="fa fa-cogs"></i> Cambiar contraseña</a></li>
                            <li class="p-0"><div class="dropdown-divider m-0"></div></li>
                            <li><a href="<?php echo base_url().'login/logout';?>"><i class="icon-logout"></i> Salir</a></li>
                        </ul>
                    </li>
                    <li class="dropdown visible-sm">
                        <a href="javascript:" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <span><i class="fa fa-ellipsis-v"></i></span>
                        </a>
                        <ul class="dropdown-menu settings-menu">
                        <?php for ($i=0; $i < count($opciones) ; $i++) { $opcion=explode("/", $opciones[$i]);
                                if(count($opcion)>1){
                                  if(isset($opcion[2])){$icon=$opcion[2];}else{$icon="";}
                        ?>
                            <li>
                                <div href="javascript:" class="nav-link" role="tab" id="<?php echo $opcion[1];?>"><i class="<?php echo $icon;?>  "></i> <span><?php echo $opcion[0];?><span></div>
                            </li>
                        <?php   }
                            }//end for ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>