<?php 
	$help1="Ingrese nombre de usuario, Tenga en cuenta que el sistema de validacion no distingue entre mayuscula y minuscula Ej. (Juan != juan)";
	$popover1='data-toggle="popover" data-placement="left" data-trigger="hover" title="Nombre de Usuario" data-content="'.$help1.'"';
	$help2="Ingrese su contraseña evitando usar letras mayusculas";
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="Ingresar Contraseña" data-content="'.$help2.'"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; padding-top: 0px;">
	<div class="row"><div class="col-xs-12"><button type="button" class="close closed_5" data-dismiss="modal">&times;</button></div></div>
<?php if(isset($titulo)){ 
	if($titulo!=""){
		echo "¿Desea ".$titulo."?";
	}else{
		echo "<h4>¡Alerta!</h4>";
	}
}else{
	echo "<h4>¡Alerta!</h4>";
}?>	
		<div class="text-center">
			<?php if(isset($img)){ ?>    
			<img class="img-resposive img-thumbnail" src="<?php echo $img;?>">
			<?php } ?>
		</div>
<?php
echo "<p class='text-danger'>$desc</p>";?>
	</div>
	
<?php if(!isset($control)){?>
<div class="list-group-item" style="max-width:100%">
<div class="form-group">
	<div class="input-group input-group-xs">
		<span class="input-group-addon" style='background-color: #fff;'><i class="fa fa-user"></i></span>
		<input type="text" id="e_user" placeholder='Ingresa tu nombre de usuario' class="form-control input-xs">
		<span class="input-group-addon form-control-sm" <?php echo $popover1;?>><i class='fa fa-info-circle'></i></span>
	</div>
</div>
<div class="form-group">
	<div class="input-group input-group-xs">
		<span class="input-group-addon" style='background-color: #fff;'><i class="fa fa-key"></i></span>
		<input type="password" id="e_password" placeholder='Ingresa tu contraseña' class="form-control input-xs">
		<span class="input-group-addon form-control-sm" <?php echo $popover2;?>><i class='fa fa-info-circle'></i></span>
	</div>	
</div>
</div>
<script>Onfocus("e_user");$('[data-toggle="popover"]').popover({html:true});</script>
<?php	}else{
	if($open_control=="true"){
		if(!empty($control)){
			for ($i=0; $i < count($control) ; $i++) { 
				echo "<br>- ".$control[$i]->nombre;
			}
		}
	}
}
if(isset($btn_ok)){
	if($btn_ok=="none"){ ?>
	<script>clear_datas("btn_52");/*Eliminando datos de boton*/$("#btn_52").removeAttr("onclick");$("#btn_52").css("display","none");</script>
	<?php }
}
?>
</div>
<script>$(".closed_5").click(function() { hide_modal("5");});</script>
