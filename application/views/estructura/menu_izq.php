 <?php 
  $cargo=$this->session->userdata("cargo");
 if($cargo!='nada_taller'){
  $grupos=array();
  $menu=array();
  $h=array();
  if(isset($privilegio)){
    if($privilegio->al==1){
      if($privilegio->al1r==1){ 
        $hijos=array();
        if(isset($almacenes)){
          $hijos[] = array('nombre' => 'Ver Almacenes', 'pestania' => 'almacen?p=1', 'icon' => 'fa fa-list-alt');
          for($i=0; $i < count($almacenes) ; $i++){ $almacen=$almacenes[$i];
            $url="";
            if($almacen->tipo=="0"){ $url="almacen/material/".$almacen->ida;}
            if($almacen->tipo=="1"){ $url="almacen/producto/".$almacen->ida;}
            $hijos[] = array('nombre' => $almacen->nombre, 'pestania' => $url, 'icon' => 'fa fa-cubes');
          }
        }
        if(count($hijos)>0){
          $h[]=array('nombre' => 'Almacenes', 'pestania' => 'almacen?p=1', 'icon' => 'icon-office', 'hijos' => $hijos);
        }else{
          $h[]=array('nombre' => 'Almacenes', 'pestania' => 'almacen?p=1', 'icon' => 'icon-office');
        }
      }
      if($privilegio->al2r==1){ $h[]=array('nombre' => 'Movimiento de Materiales', 'pestania' => 'almacen?p=2', 'icon' => 'fa fa-line-chart');}
      if($privilegio->al3r==1){ $h[]=array('nombre' => 'Movimiento de Productos', 'pestania' => 'almacen?p=3', 'icon' => 'fa fa-bar-chart-o');}
      $menu[]=array('active' => $ventana,'nombre' => 'Almacenes','pestania' => 'almacen','icon' => 'icon-building-o', 'hijos' => $h);
    }
    if($privilegio->pr==1){
      $h=array();
      if($privilegio->pr1r==1){$h[]=array('nombre' => 'Productos', 'pestania' => 'produccion', 'icon' => 'icon-suitcase');}
      if($privilegio->pr2r==1){$h[]=array('nombre' => 'Producción', 'pestania' => 'produccion?p=2', 'icon' => 'fa fa-industry');}
      if($privilegio->pr2r==1){$h[]=array('nombre' => 'Seguimiento de Producción', 'pestania' => 'produccion?p=3', 'icon' => 'fa fa-sliders');}
      if($privilegio->pr5r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'produccion?p=5', 'icon' => 'fa fa-cogs');}
      if($privilegio->pr1r==1 && $this->session->userdata("tipo")=="2"){$h[]=array('nombre' => 'Productos taller', 'pestania' => 'productos', 'icon' => 'icon-suitcase');}
      $menu[]=array('active' => $ventana,'nombre' => 'Producción','pestania' => 'produccion','icon' => 'icon-briefcase', 'hijos' => $h);
    }
    if($privilegio->mo==1){
      $h=array();
      if($privilegio->mo1r==1){$h[]=array('nombre' => 'Pedidos', 'pestania' => 'movimiento?p=1', 'icon' => 'icon-clipboard2');}
      /*if($privilegio->mo2r==1){$h[]=array('nombre' => 'Ventas', 'pestania' => 'movimiento?p=2', 'icon' => 'icon-clipboard');}*/
      if($privilegio->mo3r==1){$h[]=array('nombre' => 'Compras', 'pestania' => 'movimiento?p=3', 'icon' => 'icon-cart');}
      $menu[]=array('active' => $ventana,'nombre' => 'Movimientos','pestania' => 'movimiento','icon' => 'icon-cart', 'hijos' => $h);
    }
    if(count($menu)>0){ $grupos[]=array('nombre' => "Producción",'icon' => 'icon-stats2', 'menu' => $menu);}$menu=array();
    if($privilegio->ca==1){
      $h=array();
      if($privilegio->ca1r==1){$h[]=array('nombre' => 'Empleados', 'pestania' => 'capital_humano?p=1', 'icon' => 'fa fa-users');}
      if($privilegio->ca2r==1){$h[]=array('nombre' => 'Planillas', 'pestania' => 'capital_humano?p=2', 'icon' => 'icon-archive2');}
      if($privilegio->ca3r==1){$h[]=array('nombre' => 'Directivos', 'pestania' => 'capital_humano?p=3', 'icon' => 'fa fa-black-tie');}
      if($privilegio->ca5r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'capital_humano?p=5', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Capital humano','pestania' => 'capital_humano','icon' => 'icon-board','hijos' => $h);
    }
    if($privilegio->cl==1){
      $h=array();
      if($privilegio->cl1r==1){$h[]=array('nombre' => 'Clientes', 'pestania' => 'cliente_proveedor', 'icon' => 'icon-profile');}
      if($privilegio->cl2r==1){$h[]=array('nombre' => 'Proveedores', 'pestania' => 'cliente_proveedor?p=2', 'icon' => 'icon-addressbook');}
      if($privilegio->cl5r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'cliente_proveedor?p=3', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Cliente/Proveedor','pestania' => 'cliente_proveedor','icon' => 'icon-people','hijos' => $h);
    }
    if(count($menu)>0){$grupos[]=array('nombre' => "Personas",'icon' => 'icon-stats2', 'menu' => $menu);}$menu=array();
    /*if($privilegio->co==1){
      $h=array();
      if($privilegio->co1r==1){$h[]=array('nombre' => 'Comprobantes', 'pestania' => 'contabilidad?p=1', 'icon' => 'icon-documents');}
      if($privilegio->co2r==1){$h[]=array('nombre' => 'Libro mayor', 'pestania' => 'contabilidad?p=2', 'icon' => 'glyphicon glyphicon-book');}
      if($privilegio->co3r==1){$h[]=array('nombre' => 'Sumas y saldos', 'pestania' => 'contabilidad?p=3', 'icon' => 'icon-layout');}
      if($privilegio->co4r==1){$h[]=array('nombre' => 'Estado de costo P. y V.', 'pestania' => 'contabilidad?p=4', 'icon' => 'icon-clipboard2');}
      if($privilegio->co5r==1){$h[]=array('nombre' => 'Estado de Resultados', 'pestania' => 'contabilidad?p=5', 'icon' => 'icon-book');}
      if($privilegio->co6r==1){$h[]=array('nombre' => 'Balance General', 'pestania' => 'contabilidad?p=6', 'icon' => 'icon-paste');}
      if($privilegio->co7r==1){$h[]=array('nombre' => 'Plan de cuentas', 'pestania' => 'contabilidad?p=7', 'icon' => 'icon-file-text2');}
      if($privilegio->co8r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'contabilidad?p=10', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Contabilidad','pestania' => 'contabilidad','icon' => 'icon-calculator2','hijos' => $h);
    }*/
    if($privilegio->ac==1){
      $h=array();
      if($privilegio->ac1r==1){$h[]=array('nombre' => 'Ver activos fijos', 'pestania' => 'activos_fijos', 'icon' => 'icon-truck2');}
      if($privilegio->ac2r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'activos_fijos?p=2', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Activos Fijos','pestania' => 'activos_fijos','icon' => 'icon-truck2','hijos' => $h);
    }
    if($privilegio->ot==1){
      $h=array();
      if($privilegio->ot1r==1){$h[]=array('nombre' => 'Materiales indirectos', 'pestania' => 'material_indirecto?p=1', 'icon' => 'fa fa-cube');}
      if($privilegio->ot11r==1){$h[]=array('nombre' => 'Ingresos', 'pestania' => 'material_indirecto?p=2', 'icon' => 'fa fa-long-arrow-down');}
      if($privilegio->ot12r==1){$h[]=array('nombre' => 'Salidas', 'pestania' => 'material_indirecto?p=3', 'icon' => 'fa fa-long-arrow-up');}
      if($privilegio->ot2r==1){$h[]=array('nombre' => 'Otros materiales', 'pestania' => 'material_indirecto?p=4', 'icon' => 'fa fa-cube');}
      if($privilegio->ot3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'material_indirecto?p=5', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Otros materiales e insumos','pestania' => 'material_indirecto','icon' => 'fa fa-cubes','hijos' => $h);
    }
    if($privilegio->ad==1){
      $h=array();
      if($privilegio->ad1r==1){$h[]=array('nombre' => 'Usuarios de sistema', 'pestania' => 'administrador?p=1', 'icon' => 'icon-user');}
      if($privilegio->ad2r==1){$h[]=array('nombre' => 'Privilegios', 'pestania' => 'administrador?p=2', 'icon' => 'icon-unlocked');}
      if($privilegio->ad3r==1){$h[]=array('nombre' => 'Configuración', 'pestania' => 'administrador?p=7', 'icon' => 'fa fa-cogs');}
      $menu[]=array('active' => $ventana,'nombre' => 'Administracion','pestania' => 'administrador','icon' => 'icon-lock','hijos' => $h);
    }
    if(count($menu)>0){ $grupos[]=array('nombre' => "Otros",'icon' => 'icon-stats2', 'menu' => $menu);}$menu=array();
  }
  $grupos=json_decode(json_encode($grupos));
  $url=base_url()."libraries/img/";
  $img="sistema/avatar.jpg"; if($this->session->userdata('fotografia')!=""){ $img="personas/miniatura/".$this->session->userdata('fotografia');}
  
?>
    <aside class="main-sidebar" >
        <section class="sidebar" id="sidebar-scroll">
            <div class="user-panel">
                <div class="f-left image"><img src="<?php echo $url.$img;?>" alt="User Image" class="img-circle"></div>
                <div class="f-left info">
                    <p><?php echo $this->session->userdata("nombre")." ".$this->session->userdata("paterno");?></p>
                    <p class="designation"><?php echo $this->session->userdata("cargo");?> <i class="fa fa-caret-down m-l-5"></i></p>
                </div>
            </div>
            <ul class="nav sidebar-menu extra-profile-list">
                <li>
                    <a class="waves-effect waves-dark" href="profile.html">
                        <i class="icon-user"></i>
                        <span class="menu-text">Ver Perfil</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="javascript:void(0)">
                        <i class="icon-settings"></i>
                        <span class="menu-text">Ajustes</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="<?php echo base_url().'login/logout';?>">
                        <i class="icon-logout"></i>
                        <span class="menu-text">Salir</span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
            <ul class="sidebar-menu">
            <?php foreach ($grupos as $clave_grupo => $grupo) { ?>
              <li class="nav-level"><?php echo $grupo->nombre;?></li>
            <?php if(!empty($grupo->menu)){
                    $menu=json_decode(json_encode($grupo->menu));
                    foreach($menu as $clave_menu => $fila){//echo count($valor->hijos); echo "<br>$c<br>";$c++;?>
                      <li class="<?php if($fila->active==$fila->pestania){ echo 'active'; }?> treeview">
                        <a class="waves-effect waves-dark" href="javascript:"><i class="<?php echo $fila->icon;?>"></i><span><?php echo $fila->nombre;?></span><i class="icon-arrow-down"></i></a>
                        <ul class="treeview-menu">
                  <?php $hijos=json_decode(json_encode($fila->hijos));
                        foreach ($hijos as $key => $fila_hijo) { ?>
                          <li class="<?php if(!empty($fila_hijo->hijos)){ echo 'treeview'; }?>">
                              <a class="waves-effect waves-dark" href="<?php echo base_url().$fila_hijo->pestania;?>" <?php if($fila_hijo->pestania=="productos"){ ?>target="_black"<?php }?>>
                                  <i class="<?php echo $fila_hijo->icon;?>"></i>
                                  <?php echo $fila_hijo->nombre;?>
                                  <?php if(!empty($fila_hijo->hijos)){?><i class="icon-arrow-down"></i><?php } ?>
                              </a>
                                  <?php if(!empty($fila_hijo->hijos)){ ?>
                                  <?php $nietos=json_decode(json_encode($fila_hijo->hijos));?>
                                  <ul class="treeview-menu">
                                  <?php foreach ($nietos as $key2 => $fila_nieto) { ?>
                                      <li>
                                          <a class="waves-effect waves-dark" href="<?php echo base_url().$fila_nieto->pestania;?>" >
                                              <i class="<?php echo $fila_nieto->icon;?>"></i>
                                              <?php echo $fila_nieto->nombre;?>
                                          </a>
                                      </li>
                                  <?php } ?>
                                  </ul>
                                  <?php } ?>
                          </li>
                  <?php }// end foreach ?>
                      </ul>
                      </li>
                <?php } ?>
              <?php }// end if ?>
            <?php }//end for ?>
            </ul>
        </section>
    </aside>
    <span class="label label-primary label-tag-mobile visible-md">3 producto(s) registrado(s)</span>
<?php }?>