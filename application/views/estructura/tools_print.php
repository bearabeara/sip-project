<?php $url_tools=base_url()."libraries/img/sistema/";
	$m_top=1;$m_right=1;$m_bottom=1;$m_left=1.5;
	$page=0;$text_page="Carta";$img_page="page-vertical.gif";
	if(isset($default)){
		$default=json_decode($default);
		$m_top=$default->top;$m_right=$default->right;$m_bottom=$default->bottom;$m_left=$default->left;
		if(isset($default->page)){
			$page=$default->page;
			if($default->page==2 || $default->page==3){$text_page="Oficio";}
			if($default->page==1 || $default->page==3){$img_page="page-horizontal.gif";}
		}
	}

?>
	<div>
		<div class="input-group input-group-xs" id="tools-print">
		<?php if(isset($text_menu)){if($text_menu){?>
			<span class="input-group-addon form-control-sm hidden-md"><strong>MARGENES</strong></span>
		<?php }}?>
			<span class="input-group-addon form-control-sm hidden-sm"><img src="<?php echo $url_tools.'margin-arriba.gif';?>" width="15px" class="hidden-md"></span>
			<input type="number" id="margin-top" class="form-control input-60 input-xs hidden-sm" min="0" max="20" step="any" placeholder="<?php echo $m_top;?>" value="<?php echo $m_top;?>" data-default="<?php echo $m_top;?>">
			<span class="input-group-addon form-control-sm hidden-sm">cm.</span>
			<span class="input-group-addon form-control-sm hidden-sm"><img src="<?php echo $url_tools.'margin-derecho.gif';?>" width="15px" class="hidden-md"></span>
			<input type="number" id="margin-rigth" class="form-control input-60 input-xs hidden-sm" min="0" max="20" step="any" placeholder="<?php echo $m_right;?>" value="<?php echo $m_right;?>" data-default="<?php echo $m_right;?>">
			<span class="input-group-addon form-control-sm hidden-sm">cm.</span>
			<span class="input-group-addon form-control-sm hidden-sm"><img src="<?php echo $url_tools.'margin-abajo.gif';?>" width="15px" class="hidden-md"></span>
			<input type="number" id="margin-bottom" class="form-control input-60 input-xs hidden-sm" min="0" max="20" step="any" placeholder="<?php echo $m_bottom;?>" value="<?php echo $m_bottom;?>" data-default="<?php echo $m_bottom;?>">
			<span class="input-group-addon form-control-sm hidden-sm">cm.</span>
			<span class="input-group-addon form-control-sm hidden-sm"><img src="<?php echo $url_tools.'margin-izquierdo.gif';?>" width="15px" class="hidden-md"></span>
			<input type="number" id="margin-left" class="form-control input-60 input-xs hidden-sm" min="0" max="20" step="any" placeholder="<?php echo $m_left;?>" value="<?php echo $m_left;?>" data-default="<?php echo $m_left;?>">
			<span class="input-group-addon form-control-sm hidden-sm">cm.</span>
		<?php if(isset($text_menu)){if($text_menu){?>
			<span class="input-group-addon form-control-sm hidden-md"><strong>PAGINAS</strong></span>
		<?php }}?>
			<span class="input-group-addon form-control-sm"> <span id="text-page-print" name="<?php echo $page;?>"><?php echo $text_page;?></span> </span>
			<div class="input-group-btn">
				<button type="button" class="btn btn-default btn-mini shadow-none addon-btn waves-effect waves-light dropdown-toggle addon-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img src="<?php echo $url_tools.$img_page;?>" id="img-page-print" width="15px">
				</button>
				<div class="dropdown-menu dropdown-menu-right" id="<?php echo rand(10,9999999);?>">
					<a class="dropdown-item option-page <?php if($page==0){ echo "active";} ?>" id="opt<?php echo rand(2,9999);?>" href="javascript:" data-value="0" data-text="Carta" data-padre="div" data-type-padre="tag"><img src="<?php echo $url_tools.'page-vertical.gif';?>" width="13px"><span> Carta vertical</span></a>
					<a class="dropdown-item option-page <?php if($page==1){ echo "active";} ?>" id="opt<?php echo rand(2,9999);?>" href="javascript:" data-value="1" data-text="Carta" data-padre="div" data-type-padre="tag"><img src="<?php echo $url_tools.'page-horizontal.gif';?>" width="13px"><span> Carta horizontal</span></a>
					<a class="dropdown-item option-page <?php if($page==2){ echo "active";} ?>" id="opt<?php echo rand(2,9999);?>" href="javascript:" data-value="2" data-text="Oficio" data-padre="div" data-type-padre="tag"><img src="<?php echo $url_tools.'page-vertical.gif';?>" width="13px"><span> Oficio vertical</span></a>
					<a class="dropdown-item option-page <?php if($page==3){ echo "active";} ?>" id="opt<?php echo rand(2,9999);?>" href="javascript:" data-value="3" data-text="Oficio" data-padre="div" data-type-padre="tag"><img src="<?php echo $url_tools.'page-horizontal.gif';?>" width="13px"><span> Oficio horizontal</span></a>
				</div>
			</div>
			
		<?php if(isset($xls)){
				$function=json_decode($xls);
		?>
			<a class="form-control input-xs" href="<?php echo base_url().$function->controller;?>" style="font-size: 14px;padding: .5rem .75rem;" title="Exportar Excel"><strong><i class="icon-file-excel"></i></strong></a>
		<?php } ?>
		<?php if(isset($doc)){
				$function=json_decode($doc);
		?>
			<span class="input-group-addon form-control-sm" style="width: 0px;padding: 0px;"></span>
			<a class="form-control input-xs" href="<?php echo base_url().$function->controller;?>" style="font-size: 14px;padding: .5rem .75rem;" title="Exportar Word"><strong><i class="fa fa-file-word-o"></i></strong></a>
		<?php } ?>
		<?php if(isset($refresh)){
				$function=json_decode($refresh);
				$datas=json_decode(json_encode($function->atribs));
		?>
			<span class="input-group-addon form-control-sm" style="width: 0px;padding: 0px;"></span>
			<button class="form-control input-xs btn-print" data-area="div#area" data-tools="div#tools-print" title="Imprimir"><strong><i class="fa fa-print"></i></strong></button>
			<span class="input-group-addon form-control-sm" style="width: 0px;padding: 0px;"></span>
			<button class="form-control input-xs <?php echo $function->function;?>" <?php foreach($datas as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }?>><strong><i class="fa fa-refresh"></i></strong></button>
		<?php } ?>
		</div>
	</div>
<script>
	<?php if(isset($excel)){?>$("button.<?php echo $funcion_excel;?>").click(function(){ $(this).<?php echo $funcion_excel;?>('<?php echo $atrs_excel;?>'); });<?php } ?>
	$("button.btn-print").click(function(){$(this).print();});$("a.option-page").click(function(){ $(this).option_page_print();});
	<?php if(isset($refresh)){?>$("button.<?php echo $function->function;?>").<?php echo $function->function;?>();<?php } ?>
	Onfocus("margin-top");
</script>