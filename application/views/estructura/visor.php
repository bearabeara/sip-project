
<div class="container-fluid" style="padding:0px;">
    <div id="my-pics" class="carousel slide" data-ride="carousel" style="width:100%;margin:auto;">
      <ol class="carousel-indicators">
      <?php $c=0; foreach ($imagenes as $clave => $imagen){ $c++;}?>
  <?php if($c>1){
      $sw=0; $c=0;
      foreach ($imagenes as $clave => $imagen){ ?>
          <li data-target="#my-pics" data-slide-to="<?php echo $c;?>" class="<?php if($sw==0){ echo 'active';$sw=1;}?>"></li>
      <?php $c++;}
      } ?>
      </ol>
      <div class="carousel-inner" role="listbox">
      <?php $sw=0;
        foreach($imagenes as $clave => $imagen){ ?>
        <div class="carousel-item <?php if($sw==0){ echo 'active';$sw=1;}?>" title="<?php echo $imagen->titulo;?>" data-desc="">
          <div class="modal-header sub-modal-header">
            <span><span id="title-img"><?php if($imagen->titulo!=""){echo $imagen->titulo;}else{?>&#160<?php }?></span>
            <br><span id="desc-img"><?php if(isset($imagen->descripcion)){if($imagen->descripcion!=""){echo $imagen->descripcion;}else{?>&#160<?php }}?></span></span>
          </div>
          <img src="<?php echo $imagen->url;?>" alt="" width="100%">
        </div>
        <?php } ?>
      </div>
      <?php if($c>1){?>
        <a class="left carousel-control" href="#my-pics" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#my-pics" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
      <?php } ?>
    </div>
</div>
<script>$(function(){ $('[data-toggle="tooltip"]').tooltip();});$(function(){ $('[data-toggle="popover"]').popover();});</script>
