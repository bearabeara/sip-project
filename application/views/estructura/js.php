<?php $dir="/final";$min=".min";$version="19";?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<!--<script src="<?php /*echo base_url(); */?>libraries/js/dev/jquery-ui.min.js"></script>-->
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/tether<?php echo $min;?>.js"></script>
    <script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/bootstrap<?php echo $min;?>.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/waves<?php echo $min;?>.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/jquery.slimscroll<?php echo $min;?>.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/main<?php echo $min;?>.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/menu<?php echo $min;?>.js"></script>
<?php if ($js!=""){?>
	<script src='<?php echo base_url();?>libraries/js/<?php echo $js;?>?v=<?php echo $version;?>'></script>	
<?php }?>
<script src='<?php echo base_url();?>libraries/js<?php echo $dir;?>/lib<?php echo $min;?>.js?v=<?php echo $version;?>'></script>
<script src='<?php echo base_url();?>libraries/js<?php echo $dir;?>/ajax<?php echo $min;?>.js?v=<?php echo $version;?>'></script>
<script src='<?php echo base_url();?>libraries/js<?php echo $dir;?>/websocket<?php echo $min;?>.js?v=4'></script>
<script src="<?php echo base_url();?>libraries/js<?php echo $dir;?>/tools<?php echo $min;?>.js"></script>
<script>$(document).ready(init('<?php echo base_url();?>'));</script>