<?php
	$width=0;
	if(isset($f_buscar)){ if($f_buscar!=""){$width+=33;}}
	if(isset($f_ver)){ if($f_ver!=""){$width+=33;}}
	if(isset($f_nuevo)){ if($f_nuevo!=""){$width+=33;}}
	if(isset($f_imprimir)){ if($f_imprimir!=""){$width+=33;}}
?>
<div style="width:<?php echo $width;?>px;">
	<div class="btn-group " role="group">
		<?php if(isset($f_buscar)){ if($f_buscar!=""){?><button type="button" class="btn btn-inverse-default btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Buscar" onclick="<?php echo $f_buscar;?>"><i class="fa fa-search"></i></button><?php } }?>
		<?php if(isset($f_ver)){ if($f_ver!=""){?><button type="button" class="btn btn-inverse-default btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Ver todo" onclick="<?php echo $f_ver;?>"><i class="fa fa-list-alt"></i></button><?php } }?>
		<?php if(isset($f_nuevo)){ if($f_nuevo!=""){?><button type="button" class="btn btn-inverse-default btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Nuevo" onclick="<?php echo $f_nuevo;?>"><i class="fa fa-plus"></i></button><?php } }?>
		<?php if(isset($f_imprimir)){ if($f_imprimir!=""){?><button type="button" id="print" class="btn btn-inverse-default btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Configuración de impresión" onclick="<?php echo $f_imprimir;?>"><i class="fa fa-print"></i></button><?php } }?>	
	</div>
</div>