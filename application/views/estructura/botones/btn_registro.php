<?php 
	$width=0;
	if(isset($search)){ if($search!=""){ $width+=33; $j_search=json_decode($search);}}
	if(isset($view_all)){ if($view_all!=""){ $width+=33; $j_view_all=json_decode($view_all);}}
	if(isset($details)){ if($details!=""){ $width+=33; $j_details=json_decode($details);}}
	if(isset($config)){ if($config!=""){ $width+=33; $j_config=json_decode($config);}}
	if(isset($delete)){ if($delete!=""){ $width+=33; $j_delete=json_decode($delete);}}
	if(isset($save)){ if($save!=""){ $width+=33; $j_save=json_decode($save);}}
	if(isset($new)){ if($new!=""){ $width+=33; $j_new=json_decode($new);}}
?>
<div class="btn-group">
  	<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button"><i class="fa fa-ellipsis-v"></i></a>
  	<div class="dropdown-menu g-dropdown-menu-right">
	<?php if(isset($search)){ if($search!=""){?>
	    <button class="dropdown-item <?php echo $j_search->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_search->title;?>" <?php if(isset($j_search->atribs)){foreach(json_decode($j_search->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="icon-magnifier" style="font-weight: 900;"></i><span class="dropdown-item-text"> <?php echo $j_search->title;?></span></button>
	<?php }}?>
	<?php if(isset($view_all)){ if($view_all!=""){?>
	    <button class="dropdown-item <?php echo $j_view_all->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_view_all->title;?>" <?php if(isset($j_view_all->atribs)){foreach(json_decode($j_view_all->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="icon-reload" style="font-weight: 900;"></i><span class="dropdown-item-text"> <?php echo $j_view_all->title;?></span></button>
	<?php }}?>
	<?php if(isset($details)){ if($details!=""){?>
	    <button class="dropdown-item <?php echo $j_details->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_details->title;?>" <?php if(isset($j_details->atribs)){foreach(json_decode($j_details->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="icon-clipboard5"></i><span class="dropdown-item-text"> <?php echo $j_details->title;?></span></button>
	<?php }}?>
	<?php if(isset($save)){ if($save!=""){?>
	    <button class="dropdown-item <?php echo $j_save->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_save->title;?>" <?php if(isset($j_save->atribs)){foreach(json_decode($j_save->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="icon-floppy2"></i><span class="dropdown-item-text"> <?php echo $j_save->title;?></span></button>
	<?php }}?>
	<?php if(isset($new)){ if($new!=""){?>
	    <button class="dropdown-item <?php echo $j_new->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_new->title;?>" <?php if(isset($j_new->atribs)){foreach(json_decode($j_new->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="fa fa-plus"></i><span class="dropdown-item-text"> <?php echo $j_new->title;?></span></button>
	<?php }}?>
	<?php if(isset($config)){ if($config!=""){?>
	    <button class="dropdown-item <?php echo $j_config->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_config->title;?>" <?php if(isset($j_config->atribs)){foreach(json_decode($j_config->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }}?>><i class="icon-tools2"></i><span class="dropdown-item-text"> <?php echo $j_config->title;?></span></button>
	<?php }}?>
	<?php if(isset($delete)){ if($delete!=""){?>
	    <button class="dropdown-item <?php echo $j_delete->function;?>" type="button" data-toggle="tooltip" data-placement="left" title="<?php echo $j_delete->title;?>" <?php foreach(json_decode($j_delete->atribs) as $atrib => $val){?> data-<?php echo $atrib;?>="<?php echo $val;?>"<?php }?>><i class="fa fa-trash"></i><span class="dropdown-item-text"> <?php echo $j_delete->title;?></span></button>
	<?php }}?>
  	</div>
</div>