<?php
	$width=0;
if(isset($id)){
	if(isset($f_buscar)){ $f_b=json_decode($f_buscar); $width+=33;}
	if(isset($f_ver)){ $f_v=json_decode($f_ver); $width+=33;}
	if(isset($f_nuevo)){ $f_n=json_decode($f_nuevo);$width+=33;}
	if(isset($f_imprimir)){ if($f_imprimir!=""){$width+=33;}}
?>
<div style="width:<?php echo $width;?>px;">
	<div class="btn-group" role="group">
		<?php if(isset($f_buscar)){ ?><button type="button" id="btn_s1<?php echo $id;?>" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Buscar"><i class="fa fa-search"></i></button><?php } ?>
		<?php if(isset($f_ver)){ ?><button type="button" id="btn_s2<?php echo $id;?>" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Ver todo"><i class="fa fa-table"></i></button><?php } ?>
		<?php if(isset($f_nuevo)){ ?><button type="button" id="btn_s3<?php echo $id;?>"<?php if($f_n->atributos!="" && $f_n->atributos!="[]"){ $j_atr=json_decode($f_n->atributos);foreach ($j_atr as $atr => $valor) { ?> data-<?php echo $atr;?>="<?php echo $valor;?>"<?php }} ?> class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Nuevo"><i class="fa fa-plus-square"></i></button><?php } ?>
		<?php if(isset($f_imprimir)){ ?><button type="button" id="print<?php echo $id;?>" class="btn btn-inverse-primary btn-mini waves-effect waves-light" style="font-size:.8em;width:33px;" title="Configuración de impresión" onclick="<?php echo $f_imprimir;?>"><i class="fa fa-print"></i></button><?php }?>
	</div>
</div>
<?php if(isset($f_buscar)){ ?><script>$("#btn_s1<?php echo $id;?>").click(function(){$("#btn_s1<?php echo $id;?>").<?php echo $f_b->funcion;?>();});</script><?php } ?>
<?php if(isset($f_ver)){ ?><script>$("#btn_s2<?php echo $id;?>").click(function(){$("#btn_s2<?php echo $id;?>").<?php echo $f_v->funcion;?>();});</script><?php } ?>
<?php if(isset($f_nuevo)){ ?><script>$("#btn_s3<?php echo $id;?>").click(function(){$("#btn_s3<?php echo $id;?>").<?php echo $f_n->funcion;?>();});</script><?php } ?>
<?php if(isset($f_imprimir)){ ?><script>$("#print<?php echo $id;?>").click(function(){$("#print<?php echo $id;?>").<?php echo $f_imprimir;?>();});</script><?php } ?>
<?php
}
?>