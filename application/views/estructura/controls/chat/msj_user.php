<?php
    $url=base_url()."libraries/img/";
    $img_emisor="sistema/avatar.jpg"; if($this->session->userdata('fotografia')!=""){ $img_emisor="personas/miniatura/".$this->session->userdata('fotografia');}
    $img_reseptor="sistema/avatar.jpg"; if($amigo->fotografia!="" && $amigo->fotografia!=NULL){ $img_reseptor="personas/miniatura/".$amigo->fotografia;}

?>
<div id="<?php echo $this->session->userdata('id').'personal'.$amigo->idus;?>">

    <?php for($i=0; $i <count($mensajes); $i++){ $mensaje=$mensajes[$i];
            $tiempo=$this->lib->mensaje_tiempo_transcurrido($mensaje->fecha,"","Y-m-d");
            if($mensaje->emisor==$amigo->idus){ ?>
            <div class="media chat-messages">
                <a class="media-left photo-table" href="javascript:">
                    <img class="media-object img-circle m-t-5" src="<?php echo $url.$img_reseptor;?>" alt="Generic placeholder image">
                </a>
                <div class="media-body chat-menu-content">
                    <div class="">
                        <p class="chat-cont"><?php echo $mensaje->mensaje;?></p>
                        <small class="chat-time" style="font-size: .53rem;"><?php echo $tiempo->tiempo; ?></small>
                        <small style="float:right;margin-right:5px;padding-top:7px;font-size:x-small; color:" class="text-<?php if($mensaje->status==1){?>muted<?php }else{?>success<?php }?>" title="<?php if($mensaje->status==1){?>Enviado<?php }else{?>Visto<?php }?>"><i class="fa fa-check"></i></small>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="media chat-messages">
                <div class="media-body chat-menu-reply">
                    <div class="">
                        <p class="chat-cont"><?php echo $mensaje->mensaje;?></p>
                        <small class="chat-time" style="font-size: .53rem;"><?php echo $tiempo->tiempo; ?></small>
                        <small style="float:right;margin-right:5px;padding-top:7px;font-size:x-small;" class="text-<?php if($mensaje->status==1){?>muted<?php }else{?>success<?php }?>" title="<?php if($mensaje->status==1){?>Enviado<?php }else{?>Visto<?php }?>"><i class="fa fa-check"></i></small>
                    </div>
                </div>
                <div class="media-right photo-table">
                    <a href="javascript:">
                        <img class="media-object img-circle m-t-5" src="<?php echo $url.$img_emisor;?>" alt="Generic placeholder image">
                    </a>
                </div>
            </div>
    <?php }
        } ?>        

    </div>
    <script>scroll_final("msgs");if(!esmovil()){ $('#msgs').slimScroll({ height: $(this).height()-200+"px"});}</script>