<?php ?>
<li class="not-head">Tienes <b class="text-primary"><?php echo count($alerts)?></b> nueva<?php if(count($alerts)>1){echo 's';}?> notificación<?php if(count($alerts)>1){echo 'es';}?></li>
<?php
date_default_timezone_set("America/La_Paz");
$ahora=date('Y-m-d H:i:s');
for($i=0; $i < count($alerts) ; $i++) { $alert=$alerts[$i]; 
?>
<li class="bell-notification">

    <div class="media" id="<?php echo 'time'.$alert->idac;?>">
    	<span class="text-muted block-time">El sistema será actualizado en:</span>
			<div class="row  text-center text-primary">
				<div class="col-xs-12">
					<h2 class="f-90 f-w-400 counter-text" style="line-height: 75%; margin-top: 10px;">
						<font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="dias">0</font></font>
					</h2>
					<p class="f-24 f-w-400">
						<font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-dia">Días</font></font>
					</p>
				</div>
				<div class="col-xs-4">
					<h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="horas">00</font></font></h2>
					<p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-hora">Horas</font></font></p>
				</div>
				<div class="col-xs-4">
					<h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="minutos">00</font></font></h2>
					<p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-minuto">Minutos</font></font></p>
				</div>
				<div class="col-xs-4">
					<h2 class="f-64 f-w-400 counter-text" style="line-height: 80%;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="segundos">00</font></font></h2>
					<p class="f-15 f-w-400"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" class="text-segundo">Segundos</font></font></p>
				</div>
			</div>
    </div>
    <script>
		$(document).ready(function(){
			$(this).cronometro({
				inicio: '<?php echo $ahora;?>',
				fin: '<?php echo $alert->fecha_inicio;?>',
				id: "div#<?php echo 'time'.$alert->idac;?>",
				format_dia: false,
				ac: '<?php echo $alert->idac;?>'
			});
		});
    </script>
</li>
<?php } ?>
<li class="not-footer">
    <a href="javascript:" style="font-size: .8rem;" class="all_updates">Ver todas las actualizaciónes</a>
    <script>$(this).clear_all_intervals(); $("a.all_updates").click(function(){ $(this).all_updates();});</script>
</li>
