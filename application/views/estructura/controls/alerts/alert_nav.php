<?php $url=base_url()."libraries/img/"; ?>
<li class="not-head">Tienes <b class="text-primary"><?php echo count($alerts)?></b> nueva<?php if(count($alerts)>1){echo 's';}?> notificación<?php if(count($alerts)>1){echo 'es';}?></li>
<?php
for($i=0; $i < count($alerts) ; $i++) { $alert=$alerts[$i]; 
    $usuario=$this->M_usuario->get_persona($alert->emisor);
    $img="sistema/avatar.jpg";
    $nombre="Desconocido";
    if(!empty($usuario)){ 
        if($usuario[0]->fotografia!="" && $usuario[0]->fotografia!=NULL){ $img="personas/miniatura/".$usuario[0]->fotografia;}
        $nom=explode(' ', $usuario[0]->nombre); $nombre=$nom[0];
    }
?>
<li class="bell-notification">
    <a href="javascript:" class="media" onclick="user_alters(this)" id="notification" data-b="<?php echo $alert->emisor;?>">
    <span class="media-left media-icon"><img class="img-circle" src="<?php echo $url.$img;?>" alt="User Image"></span>
    <div class="media-body">
    <?php $msj="";$plural="";
        if($alert->almacen>1){ $plural="s";}
        if($alert->almacen>0){ $msj="hizo ".$alert->almacen." cambio".$plural." de almacen";}
        $plural="";$plural2="";$plural3="un";
        if($alert->material>1){ $plural="s";$plural2="es";$plural3="varios";}
        if($alert->material>0){ if($msj==""){ $msj="hizo ".$alert->material." cambio".$plural." en material".$plural2;}else{ $msj.=", ".$alert->material." cambio".$plural." en material".$plural2; }}
        $plural="";$plural3="un";
        if($alert->producto>1){ $plural="s";$plural3="";}
        if($alert->producto>0){ if($msj==""){ $msj="hizo ".$alert->producto." cambio".$plural." en ".$plural3." producto".$plural;}else{ $msj.=", ".$alert->producto." cambio".$plural." en ".$plural3." producto".$plural; }}
        $plural="";$plural2="un ";
        if($alert->producto_categoria>1){ $plural="s";$plural2="";}
        if($alert->producto_categoria>0){ if($msj==""){ $msj="hizo ".$alert->producto_categoria." cambio".$plural." en la".$plural." categoria".$plural." de ".$plural2."producto".$plural;}else{ $msj.=", ".$alert->producto_categoria." cambio".$plural." en la".$plural." categoria".$plural." de ".$plural2."producto".$plural; }}
        if($privilegio->mo==1 && $privilegio->mo1r==1 && $privilegio->mo1u==1){
            $plural="";$plural2="un ";
            if($alert->pedido>1){ $plural="s";$plural2="";}
            if($alert->pedido>0){ if($msj==""){ $msj="hizo ".$alert->pedido." cambio".$plural." en pedido";}else{ $msj.=", ".$alert->pedido." cambio".$plural." en pedido"; }}
        }
    ?>
    <span class="block"> <?php echo $nombre." ".$msj.".";?></span>
    <?php $tiempo=$this->lib->mensaje_tiempo_transcurrido($alert->fecha,"","Y-m-d");?>
    <span class="text-muted block-time"><?php echo $tiempo->tiempo?></span></div></a>
</li>
<?php } ?>
<li class="not-footer">
    <a href="javascript:" style="font-size: .8rem;" onclick="all_user_alters();">Ver todas las notificaciones</a>
</li>
