<?php echo $empleado->nombre." ¿desea cambiar tu contraseña?";
  $help2="Ingrese su contraseña evitando usar letras mayusculas";
  $popover2='data-toggle="popover" data-placement="left" data-trigger="hover" title="Ingresar su contraseña" data-content="'.$help2.'"';
  $help3="Ingresa tu nueva contraseña evitando usar letras mayusculas de <strong>4 a 25 caracteres alfanuméricos</strong>";
  $popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="Nueva contraseña" data-content="'.$help3.'"';
  $help4="Repite tu nueva contraseña evitando usar letras mayusculas";
  $popover4='data-toggle="popover" data-placement="left" data-trigger="hover" title="Repite tu contraseña" data-content="'.$help4.'"';
  $img="default.png";
  $url=base_url().'libraries/img/personas/miniatura/';
  if($empleado->fotografia!=NULL && $empleado->fotografia!=""){ $img=$empleado->fotografia;}
?>
<div class="text-center">
	<img class="img-resposive img-thumbnail" src="<?php echo $url.$img;?>">
</div>
<hr>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='fa fa-key'></i></span>
			<input type="password" id="e_pass" placeholder='Ingresa tu contraseña' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover2;?>><i class='fa fa-info-circle'></i></span>
		</div>
	</div>
<hr>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='fa fa-key'></i></span>
			<input type="password" id="new_pass" placeholder='Ingresa tu nueva contraseña' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover3;?>><i class='fa fa-info-circle'></i></span>
		</div>
	</div>
	<div class="form-group">
		<div class="input-group input-group-xs">
			<span class="input-group-addon" style='background-color: #fff;'><i class='fa fa-key'></i></span>
			<input type="password" id="rep_pass" placeholder='Repite tu nueva contraseña' class="form-control input-xs">
			<span class="input-group-addon input-sm" <?php echo $popover4;?>><i class='fa fa-info-circle'></i></span>
		</div>
	</div>

	<script language='javascript'>Onfocus("e_pass");$('[data-toggle="popover"]').popover({html:true});</script>	
