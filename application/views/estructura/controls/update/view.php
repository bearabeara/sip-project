<?php
date_default_timezone_set("America/La_Paz");
$ahora=date('Y-m-d H:i:s');
$fin=$actualizacion->fecha_fin;
?>
<?php $dir="/final";$min=".min";$version="2";?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>Actualizando</title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta name="mobile-web-app-capable" content="yes">
<meta name="description" content="" />
<meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
<meta name="author" content="bearabeara.co.uk" />
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/font-awesome<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/simple-line-icons<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/bootstrap<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/main<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/responsive<?php echo $min;?>.css">
</head>
<body>
<section id="actualizando" class="coming"><div style="width: 100%; background-color: rgba(0, 0, 0, .86); height: 100%; padding-top: 6%;"><div class="container"><div class="row comming-soon"><h1 class="m-b-70">ACTUALIZANDO</h1><p class="m-b-50">Estamos trabajando duro para mejorar nuestro sistema web, estaremos listos para volver después de</p><div class="counter m-b-70"><div class="row"><div class="col-xs-6 col-sm-3"><h2 class="dias">00</h2><p class="text-dia">Dias</p></div><div class="col-xs-6 col-sm-3"><h2 class="horas">00</h2><p class="text-hora">Horas</p></div><div class="col-xs-6 col-sm-3"><h2 class="minutos">00</h2><p class="text-minuto">Minutos</p></div><div class="col-xs-6 col-sm-3"><h2 class="segundos">00</h2><p class="text-segundo">Segundos</p></div></div></div></div></div></div></section>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/tether.min.js"></script>
<script src="<?php echo base_url(); ?>libraries/js<?php echo $dir;?>/bootstrap.min.js"></script>
<script>
var stack_intervals=[];(function($){ $.fn.cronometro=function(options){var fini="";var ffin="";var settings={'inicio': null,'fin': null,'id': null, 'format_dia': null, 'ac': null};if(options){ $.extend(settings, options);}function cronometro_proc(settings){ if(fini==""){ fini = Date.parse(settings['inicio'])/1000;}if(ffin==""){ ffin = Date.parse(settings['fin'])/1000;}if(ffin < fini){var atrib=new FormData();atrib.append('ac',settings['ac']);$.ajax({url:"update/terminar_actualizacion",async:true,type: "POST",contentType:false,data:atrib,processData:false, timeout:30000,beforeSend: function(){},success: function(result){if(result!="logout" && result!="error" && result!="fail"){if(result=="actualizado"){window.location.href='<?php echo base_url();?>';}else{var v=result.split("|");fini=Date.parse(v[0])/1000;ffin=Date.parse(v[1])/1000;cronometro_proc(settings);}}else{msj(result);}},error:function(){e.error_envio(controls);}});}else{seconds = ffin - fini;days = Math.floor(seconds/(60*60*24));seconds-=days*60*60*24;hours=Math.floor(seconds/(60*60));seconds-=hours*60*60;minutes=Math.floor(seconds/60);seconds-=minutes*60;if(days==1){$(settings['id']).find(".text-dia").text("dia"); }else {$(settings['id']).find(".text-dia").text("dias"); }if(hours==1){$(settings['id']).find(".text-hora").text("hora"); }else {$(settings['id']).find(".text-hora").text("horas"); }if(minutes==1){$(settings['id']).find(".text-minuto").text("minuto"); }else {$(settings['id']).find(".text-minuto").text("minutos"); }if(seconds==1){$(settings['id']).find(".text-segundo").text("segundo"); }else {$(settings['id']).find(".text-segundo").text("segundos"); }if(settings['format_dia']==true){days = (String(days).length >= 2) ? days : "0" + days;}hours = (String(hours).length >= 2) ? hours : "0" + hours;minutes = (String(minutes).length >= 2) ? minutes : "0" + minutes;seconds = (String(seconds).length >= 2) ? seconds : "0" + seconds;if(!isNaN(ffin)){$(settings['id']).find(".dias").text(days);$(settings['id']).find(".horas").text(hours);$(settings['id']).find(".minutos").text(minutes);$(settings['id']).find(".segundos").text(seconds);if((days=="0" || days=="00") && hours=="00" && (minutes*1)<=1){var msj_1="minuto";var msj_2="segundo";if(minutes!=1){ msj_1+="s";}if(seconds!=1){ msj_2+="s";}$("#alerta_sistema #hora_alerta").text(minutes+" "+msj_1+" "+seconds+" "+msj_2);}else{}}else {console.log("Fecha inválida");clearInterval(interval);}var fecha = new Date(fini*1000);fecha.setSeconds(fecha.getSeconds()+1);fini=Math.floor(Date.parse(fecha)/1000);}}cronometro_proc(settings);interval = setInterval(function(){ cronometro_proc(settings);}, 1000);stack_intervals.push(interval);}
$.fn.extend({clear_all_intervals: function(){ for(var i=0; i < stack_intervals.length; i++){ clearInterval(stack_intervals[i]);}}});})(jQuery);$(document).ready(function(){$(this).cronometro({inicio: '<?php echo $ahora;?>',fin: '<?php echo $actualizacion->fecha_fin;?>',id: "section#actualizando",format_dia: false,ac: '<?php echo $actualizacion->idac;?>'});});
</script>
</body>
</html>
