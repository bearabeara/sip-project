<?php $v = array('0'=>'pendiente','1'=>'en proceso','2'=>'terminada');?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th width="3%">#</th>
						<th width="21%">Inicio</th>
						<th width="21%">Finalización</th>
						<th width="45%">Detalle de actualización</th>
						<th width="10%">Estado de actualización</th>
					</tr>
				</thead>
				<tbody>
			<?php if(!empty($actualizaciones)){ ?>
			<?php for ($i=0; $i < count($actualizaciones) ; $i++) { $actualizacion=$actualizaciones[$i]; ?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo $this->lib->format_date($actualizacion->fecha_inicio,"dl ml Y");?></td>
						<td><?php echo $this->lib->format_date($actualizacion->fecha_fin,"dl ml Y");?></td>
						<td><?php echo $actualizacion->descripcion;?></td>
						<td><span class="label label-<?php if($actualizacion->estado==0){ echo 'danger';}if($actualizacion->estado==1){ echo 'info';}if($actualizacion->estado==2){ echo 'success';}?> label-md"><?php echo $v[$actualizacion->estado];?></span></td>
					</tr>
			<?php }?>
			<?php }else{ ?>
					<tr>
						<td colspan="5" class="text-center"><h3>0 registros encontrados...</h3></td>
					</tr>
			<?php }?>
				</tbody>
			</table>
		</div>
	</div>
</div>