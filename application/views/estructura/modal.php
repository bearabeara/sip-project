<div id="alerta" style="display:none;">
  <button type="button" class="close" onclick="close_alerta('alerta')">&times;</button>
  <img src="" id="alerta_img" style="width:30px;">
  <span id='alerta_text'></span>
</div>
<div id="alerta_sistema" class="alert alert-refresh" role="alert" style="display: none;">
  <span style="color:rgb(255, 255, 255);">El sistema será actualizado en <strong id="hora_alerta"></strong></span>
</div>
<!-- Modal Bootstrap Nivel 1-->
<div class="modal fade" id="modal_1" role="dialog">
    <div id="dialog_1">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title titulo_1"></h4>
          </div>
          <div class="modal-body" id="content_1"></div>
            <div class="modal-footer">  
                <a href="javascript:" id="btn_11" data-dismiss="modal" class="btn">Cerrar</a>
                <button id="btn_12" class="btn btn-primary">Guardar cambios</button>
                <a href="javascript:" id="btn_13" class="btn btn-info">Imprimir</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nivel 2-->
<div class="row" id="modal_2">
  <div id="dialog_2">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closed_2" data-dismiss="modal">&times;</button>
          <h4 class="modal-title titulo_2"></h4>
        </div>
          <div class="modal-body" id="content_2"></div>
            <div class="modal-footer">
                <a href="javascript:" id="btn_21" data-dismiss="modal" class="btn">Cerrar</a>
                <button id="btn_22" class="btn btn-primary">Guardar cambios</button>
                <button id="btn_23" class="btn btn-info">Imprimir</button>
            </div>
        </div>
  </div>
</div>
<!-- Modal Nivel 3-->
<div class="row" id="modal_3">
     <div id="dialog_3">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_3" data-dismiss="modal">&times;</button>
              <h4 class="modal-title titulo_3"></h4>
          </div>
          <div class="modal-body" id="content_3"></div>
            <div class="modal-footer">
                <a href="javascript:" id="btn_31" data-dismiss="modal" class="btn">Cerrar</a>
                <button id="btn_32" class="btn btn-primary">Guardar cambios</button>
                <a href="javascript:" id="btn_33" class="btn btn-info">Imprimir</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nivel 5-->
<div class="row" id="modal_4">
     <div id="dialog_4">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_4" data-dismiss="modal">&times;</button>
              <h4 class="modal-title titulo_4"></h4>
          </div>
          <div class="modal-body" id="content_4"></div>
            <div class="modal-footer">
                <a href="javascript:" id="btn_41" data-dismiss="modal" class="btn">Cerrar</a>
                <button id="btn_42" class="btn btn-primary">Guardar cambios</button>
                <a href="javascript:" id="btn_43" class="btn btn-info">Imprimir</a> 
            </div>
        </div>
    </div>
</div>
<!--Modal Nivel 5 Ventana de confirmacion-->
<div class="row" id="modal_5">
  <div id="dialog_5">
    <div class="modal-content">
      <div class="modal-body" id="content_5" style="padding: 0px;"></div>
        <div class="modal-footer">
                <a href="javascript:" id="btn_51" data-dismiss="modal" class="btn">Cerrar</a>
                <a href="javascript:" id="btn_52" class="btn btn-danger">Aceptar</a>
        </div>
        </div>
    </div>
</div>
<!--Modal Nivel 5 Validadcion-->
<!--Visor-->
<div class="row" id="modal_6">
  <div id="dialog_6">
    <div class="modal-content">
      <div class="modal-header modal-header-visor">
        <button type="button" class="close closed_6" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="content_6" style="padding:0px;"></div>
    </div>
  </div>
</div>

<!-- Modal Nivel 5-->
<div class="row" id="modal_7">
     <div id="dialog_7">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_7" data-dismiss="modal">&times;</button>
              <h4 class="modal-title titulo_7"></h4>
          </div>
          <div class="modal-body" id="content_7"></div>
            <div class="modal-footer">
                <a href="javascript:" id="btn_71" data-dismiss="modal" class="btn">Cerrar</a>
                <a href="javascript:" id="btn_72" class="btn btn-primary">Guardar</a>
                <a href="javascript:" id="btn_73" class="btn btn-info">Imprimir</a> 
            </div>
        </div>
    </div>
</div>
<div class="row" id="modal_8">
     <div id="dialog_8">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close closed_8" data-dismiss="modal">&times;</button>
              <h4 class="modal-title titulo_8"></h4>
          </div>
          <div class="modal-body" id="content_8"></div>
            <div class="modal-footer">
                <a href="javascript:" id="btn_81" data-dismiss="modal" class="btn">Cerrar</a>
                <a href="javascript:" id="btn_82" class="btn btn-primary">Guardar</a>
                <a href="javascript:" id="btn_83" class="btn btn-info">Imprimir</a> 
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_11" role="dialog">
    <div id="dialog_11">
        <div class="modal-content">
          <div class="modal-body">
            <div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div id="content_11"></div>
          </div>
            <div class="modal-footer">  
                <a href="javascript:" id="btn_111" data-dismiss="modal" class="btn">Cerrar</a>
                <button id="btn_112" class="btn btn-primary">Guardar cambios</button>
                <button id="btn_113" class="btn btn-info">Imprimir</button>
            </div>
        </div>
    </div>
</div>
<div id="preload-modal" style="display: none;">
  <i class='fa fa-circle-o-notch'></i>
</div>
