<?php $users=$this->M_usuario->search_usuarios("","",$this->session->userdata("id"));
    $url=base_url()."libraries/img/";
    $img="sistema/avatar.jpg";
?>
    <div id="sidebar" class="p-fixed header-users showChat">
        <div class="had-container">
            <div class="card card_main header-users-main">
                <div class="card-content user-box">
                    <div class="md-group-add-on p-15">
                                 <span class="md-add-on" style="font-size:1.2em;">
                                    <i class="icon-magnifier"></i>
                                 </span>
                        <div class="md-input-wrapper">
                            <input type="text" class="md-form-control"  name="username" id="search-friends">
                            <label for="username">Search</label>
                        </div>
                    </div>
                    <div class="main-friend-list">
                    <?php for ($i=0; $i < count($users) ; $i++) { $user=$users[$i]; 
                            if($user->idus!=$this->session->userdata("id") && ($user->tipo=="1" || $user->tipo=="2")){
                    ?>
                        <div class="media friendlist-box" data-id="1" data-status="online" data-username="<?php echo $user->nombre.' '.$user->nombre2.' '.$user->paterno; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $user->nombre.' '.$user->nombre2.' '.$user->paterno; ?>">
                            <a class="media-left" href="javascript:void(0)" onclick="user_msj(this)" data-a="<?php echo $user->idus;?>">
                            <?php $img="sistema/avatar.jpg"; if($user->fotografia!="" && $user->fotografia!=NULL){ $img="personas/miniatura/".$user->fotografia; }?>
                                <img class="media-object img-circle" src="<?php echo $url.$img;?>" alt="Generic placeholder image">
                                <!--<div class="live-status bg-success"></div>-->
                                <span class="" id="badge<?php echo $user->idus;?>"></span>
                            </a>
                            <div class="media-body"  onclick="user_msj(this)" data-a="<?php echo $user->idus;?>">
                                <div class="friend-header"><?php echo $user->nombre.' '.$user->nombre2.' '.$user->paterno; ?></div>
                                <small class="text-muted" id="control-active<?php echo $user->idus;?>" style="font-size: 10px;">
                                <?php 
                                    if($user->fecha_ingreso!="" && $user->fecha_ingreso!=NULL){
                                        $tiempo=$this->lib->mensaje_tiempo_transcurrido($user->fecha_ingreso."","","Y-m-d");
                                        echo "Activo ".$tiempo->tiempo;
                                    }
                                ?>
                                </small>
                            </div>
                        </div>                        
                    <?php }
                    } ?>

                    </div>
                    <div id="main-friend"></div>
                </div>
            </div>
        </div>
    </div>
<!-- Sidebar chat start -->
    <div class="showChat_inner" id="showChat_inner">
        <div class="media chat-inner-header">
            <a class="back_chatBox" onclick="return_chat()">
                <i class="fa fa-chevron-left" style="font-size:.8em;"></i><span></span>
            </a>
        </div>
        <div id="content_msj"></div>
    </div>
<!-- Sidebar chat end-->
