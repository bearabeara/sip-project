<?php $dir="/final";$min=".min";$version="19";?>
<title><?php echo $title;?></title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta name="mobile-web-app-capable" content="yes">
<meta name="description" content="" />
<meta name="keywords" content="marroquineria, cuero, maletines, billeteras" />
<meta name="author" content="bearabeara.co.uk" />
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/icomon<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/font-awesome<?php echo $min?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/simple-line-icons<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/bootstrap<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/menu<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/clase_generica<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/waves<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/main<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/responsive<?php echo $min;?>.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css<?php echo $dir;?>/style<?php echo $min;?>.css?v=<?php echo $version;?>">
<?php if ($css!="") { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>libraries/css/<?php echo $css;?>?v=<?php echo $version;?>">	
<?php } ?>
<!--<script src="--><?php //echo base_url(); ?><!--libraries/js/dev/jquery-3.1.1.min.js"></script>-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous""></script>