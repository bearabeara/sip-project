<?php date_default_timezone_set("America/La_Paz");?>
<div class="encabezado_pagina">
	<table class="tabla tabla-border-false"><tr class="fila">
		<td class="celda td" width="5%"><img src="<?php echo base_url().'libraries/img/sistema/killa-print.jpg'?>"></td>
		<td class="celda td" width="88%">
			<div class='encabezado_titulo'><ins><?php if(isset($titulo)){ echo $titulo;}else{ echo "SIN TITULO";}?></ins></div>
			<div class='sub_titulo'><?php if(isset($sub_titulo)){ echo $sub_titulo;}?></div>
		</td>
		<td class="celda td" class="celda td" width="5%"><div class="encabezado_descripcion">
			<b>Fecha: </b><span><?php echo date('Y-m-d').' '.date('H:m');?></span><br>
			<b>Usuario: </b><span><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('nombre2').' '.$this->session->userdata('paterno').' '.$this->session->userdata('materno');?></span><br>
	<?php if(isset($nro)){if($nro){?><b>Página: </b><?php echo $pagina.' de '?><span class='nroPagina'></span></div><?php }}?>
		</td></tr>
	</table>
</div>