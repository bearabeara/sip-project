<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Administrador','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
			<?php $almacenes=$this->M_almacen->get_all(); ?>
			<?php $v_menu="";
				if($privilegio[0]->ad1r==1){ $v_menu.="Usuarios/usuario/icon-user|"; }
				if($privilegio[0]->ad2r==1){ $v_menu.="Privilegios/privilegio/icon-unlocked|";}
				if($privilegio[0]->ad3r==1){ $v_menu.="Configuración/config/fa fa-cogs";}
			?>
			<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
			<?php $this->load->view('estructura/chat');?>
			<?php $almacenes=$this->M_almacen->get_all(); ?>
			<?php $this->load->view('estructura/menu_izq',['ventana'=>'administrador','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>
			<div id="search"></div>
			<div id="contenido"></div>
		<?php $this->load->view('estructura/js',['js'=>$dir.'administrador'.$min.'.js']);?>
	</body>
	<?php 
	$title="";$activo="";$search="";$view="";
	switch($pestania){
		case '1': 
			if($privilegio[0]->ad==1 && $privilegio[0]->ad1r==1){
				$title="Usuarios de sistema"; $activo='usuario'; $search="administrador/search_usuario"; $view="administrador/view_usuario";
			}else{
				$title="locked"; $view=base_url()."login/locked";
			} break;
		case '2': 
			if($privilegio[0]->ad==1 && $privilegio[0]->ad2r==1){
				$title="Privilegios"; $activo='privilegio'; $search="administrador/search_privilegio"; $view="administrador/view_privilegio";
			}else{
				$title="locked"; $view=base_url()."login/locked";
			}
			 break;
		case '7': $title="Configuración usuario"; $activo='config'; $search=""; $view="administrador/view_config"; break;
		default: $title="Error!";$activo='';$search="NULL";$view="NULL"; break;
	} ?>
	<script>$(this).get_2n('<?php echo $search;?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','administrador?p=<?php echo $pestania;?>');</script></html>