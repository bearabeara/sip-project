<table table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm img-thumbnail-50'><div class="img-thumbnail-50"></div></td>
		<td style="width:22%">
			<form class="view_ingresos">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_ingreso" placeholder='Buscar...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_ingresos" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' style="width:12%">
			<form class="view_ingresos">
				<input class='form-control form-control-sm search_ingreso' type="number" id="s_can" placeholder='Stock' min='0' onwheel="$(this).reset_input()">
			</form>
		</td>
		<td class='hidden-sm' style="width:61%"></td>
		<td class='hidden-sm text-right' style="width:5%">
			<?php 
				$search=json_encode(array('function'=>'view_ingresos','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_ingresos','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus("s_nom");$(".view_ingresos").view_ingresos();$(".search_ingreso").search_ingreso();</script>