<?php
	$fecha=date("Y-m-d");
	if($file=="xls"){
		header("Content-type: application/vnd.ms-excel; name='excel'");	
	}
	if($file=="doc"){
		header("Content-type: application/vnd.ms-word; name='word'");
	}
	header('Pragma: public');
	header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
	header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Content-Transfer-Encoding: none');
	if($file=="xls"){
		header('Content-type: application/vnd.ms-excel;charset=utf-8');// This should work for IE & Opera 
		header('Content-type: application/x-msexcel; charset=utf-8'); // This should work for the rest 
		header("Content-Disposition: attachment; filename=Otros-Materiales-$fecha.xls");
	}
	if($file=="doc"){
		header('Content-type: application/vnd.ms-word;charset=utf-8');
		header('Content-type: application/x-msword; charset=utf-8');
		header("Content-Disposition: attachment; filename=Otros-Materiales-$fecha.doc");
	}
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	$url=base_url().'libraries/img/';
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<center><h3>REPORTE OTROS MATERIALES</h3></center>
<table border="1" cellpadding="5" cellspacing="0">
	<thead>
		<tr class="fila">
			<th>#</th>
		<?php if($file=="doc"){?>
			<th>Fot.</th>
		<?php }?>
			<th>Código</th>
			<th>Nombre</th>
			<th>Observaciónes</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i < count($materiales); $i++) { $material=$materiales[$i];
		$img='sistema/miniatura/default.jpg';
		if($material->fotografia!="" & $material->fotografia!=NULL){$img="materiales/miniatura/".$material->fotografia;}
?>	
		<tr>
			<td><?php echo $i+1;?></td>
		<?php if($file=="doc"){?>
			<td><img src="<?php echo $url.$img;?>" style="width: 50px;"></td>
		<?php }?>
			<td><?php echo $material->codigo; ?></td>  
			<td><?php echo $material->nombre; ?></td>
			<td><?php echo $material->descripcion;?></td>
		</tr>
<?php }?>
	</tbody>
</table>