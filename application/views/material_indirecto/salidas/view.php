<?php date_default_timezone_set("America/La_Paz");
	$url=base_url().'libraries/img/';
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 300 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
?>

<table class="table table-bordered table-hover">
<thead>
	<tr>
		<th class='img-thumbnail-50'><div class="img-thumbnail-50">#Item</div></th>
		<th width="24%">Nombre</th>
		<th width="12%">Stock actual</th>
		<th width="15%">Solicitante</th>
		<th width="16%">Fecha de salida</th>
		<th width="8%" >Cantidad</th>
		<th width="22%">Observaciónes</th>
		<?php if($privilegio->ot12u==1){ ?>
		<th width="3%" ></th>
		<?php } ?>
	</tr>
</thead><tbody>
<?php 
if(count($materiales)>0){
	for($i=0;$i<count($materiales);$i++){ $material=$materiales[$i];
	    $img="sistema/miniatura/default.jpg";
	    if($material->fotografia!="" && $material->fotografia!=NULL){
			$img="materiales/miniatura/".$material->fotografia;
		}
?>
	<tr id="tr-<?php echo $material->idme;?>">
		<td>
			<div id="item"><?php echo $i+1;?></div>
			<img src="<?php echo $url.$img;?>" class='img-thumbnail img-thumbnail-50' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
		</td>
		<td><?php echo $material->nombre; ?></td>
		<td>
			<div class="input-group input-120">
				<input type="text" id="c<?php echo $material->idme;?>" class="form-control form-control-sm" value="<?php echo $material->cantidad; ?>" disabled>
				<span class="input-group-addon form-control-sm"><?php echo $material->abr."."; ?></span>
			</div>
		</td>
		<td>
			<select id="emp<?php echo $material->idme;?>" class="form-control form-control-sm">
				<option value="">Seleccionar...</option>
			<?php for ($e=0; $e < count($empleados) ; $e++){ $empleado=$empleados[$e]; 
					if($empleado->estado=="1"){ ?>
					<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php 	}
				}?>
			</select>
		</td>
		<td><input type="datetime-local" id="fech<?php echo $material->idme;?>" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" class="form-control form-control-sm"></td>
		<td>
			<div class="input-group input-120">
				<form class="save_movimiento" data-me="<?php echo $material->idme;?>" data-type="s" data-item="<?php echo $i+1;?>">
					<input type="number" id="can<?php echo $material->idme;?>" class="form-control form-control-sm input-100" placeholder="0" min='0' step="any" min='0' max="999999999.9999999">
				</form>
				<span class="input-group-addon form-control-sm"><?php echo $material->abr."."; ?></span>
			</div>
		</td>
		<td>
			<div class="input-group">
				<textarea id="obs<?php echo $material->idme;?>" class="form-control form-control-sm" placeholder="Observaciónes de ingreso de material" rows="2"></textarea>	
				<span class="input-group-addon form-control-sm" <?php echo $popover3;?>><i class='fa fa-info-circle'></i></span>
			</div>
		</td>
		<?php if($privilegio->ot12u==1){ ?>
		<td>
		<?php $save=json_encode(array('function'=>'save_movimiento','atribs'=> json_encode(array('me' => $material->idme,'type'=>'s','item'=>($i+1))),'title'=>'Guardar'));?>
		<?php $this->load->view("estructura/botones/btn_registro",['save'=>$save]);?>
		</td>
		<?php }?>
	</tr>
<?php }}else{
	echo "<tr><td colspan='10'><h2>0 registros encontrados...</h2></td></tr>";
} ?>
</tbody>
</table>
<script>$("img.img-thumbnail-50").visor();$('.save_movimiento').save_movimiento();$('[data-toggle="popover"]').popover({html:true});</script>