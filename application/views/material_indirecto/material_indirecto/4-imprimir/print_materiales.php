<?php $url=base_url().'libraries/img/';?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_materiales', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'material_indirecto/export_materiales?cod='.$cod.'&nom='.$nom."&can=".$can."&file=xls");
		$word = array('controller' => 'material_indirecto/export_materiales?cod='.$cod.'&nom='.$nom."&can=".$can."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 8%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Código</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 45%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cantidad</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 45%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
			</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive" id="area">
			<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="19" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'REGISTRO DE MATERIALES INDIRECTOS']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="3%" style="display: none;">#</th>
								<th class="celda th padding-4" data-column="2" width="6%">Fot.</th>
								<th class="celda th padding-4" data-column="3" width="8%">Código</th>
								<th class="celda th padding-4" data-column="4" width="45%">Nombre</th>
								<th class="celda th padding-4" data-column="5" width="10%">Cantidad</th>
								<th class="celda th padding-4" data-column="6" width="45%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){
									$material=$this->lib->search_elemento($materiales,"idme",$visible);
									if($material!=null){
										$img='sistema/miniatura/default.jpg';
										if($material->fotografia!=NULL && $material->fotografia!=""){ $img="materiales/miniatura/".$material->fotografia;}
										$cont++;
							?>
								<tr class="fila">
									<td class="celda td padding-4" data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $material->codigo;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $material->nombre;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $material->cantidad." ".$material->abr."."; ?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $material->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
			</table>
		</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>