<?php
$help1='title="Subir Fotografías" data-content="Seleccione las fotografías, preferiblemente una imagen que no sobrepase <strong>1.5MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>, puede seleccionar <strong>hasta 10 imágenes</strong>."';
$help2='title="Ingresar nombre de material" data-content="Ingrese un nombre alfanumerico de<b> 2 a 100</b> caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
$help3='title="Ingresar codigo de material" data-content="Ingrese un código alfanumerico de<b> 2 a 15</b> caracteres <b>puede incluir espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
$help6='title="Unidad de medida de material" data-content="Seleccione una unidad de medida del material, El valor vacio no es aceptado, si desea adicionar una nueva unidad de material lo puede hacer en el menu superior (Configuracion), o dando click en el boton <b>+</b>"';
$help7='title="Descripción del material" data-content="Ingrese un descripcion alfanumerica de 0 a 300 caracteres puede incluir espacios, ademas la descripcion solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class="text-danger">(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Fotografía: </label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<input class='form-control input-xs' type="file" multiple="multiple" id="fot">
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Nombre:</label>
				<div class="col-sm-10 col-xs-12">
					<form class="update_material" data-me="<?php echo $material->idme;?>">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="nom" placeholder='Nombre de Material' maxlength="100" value="<?php echo $material->nombre;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Código:</label>
				<div class="col-sm-4 col-xs-12">
					<form class="update_material" data-me="<?php echo $material->idme;?>">
						<div class="input-group">
							<input class='form-control input-xs' type="text" id="cod" placeholder='Código' maxlength="15" value="<?php echo $material->codigo;?>">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span>Unidad de medida:</label>
				<div class="col-sm-4 col-xs-12">
					<div class="input-group">
						<select class='form-control input-xs' id="med">
							<option value="">Seleccione...</option>
							<?php for ($i=0; $i < count($unidades);$i++){ $u=$unidades[$i]; ?>
							<option value="<?php echo $u->idu;?>" <?php if($material->idu==$u->idu){echo "selected";} ?>><?php echo $u->nombre;?></option>
							<?php } ?>
						</select>
						<a href="<?php echo base_url();?>material_indirecto?p=5" target="_blank" title="Ver Configuraciónes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div><i class='clearfix'></i>
			<div class="form-group">
				<label for="example-text-input" class="col-sm-2 col-xs-12  col-form-label form-control-label">Descripción:</label>
				<div class="col-sm-10 col-xs-12">
					<div class="input-group">
						<textarea class="form-control input-xs" id="des" placeholder='Descripción del material' maxlength="300" rows="3"><?php echo $material->descripcion; ?></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>Onfocus("nom");$('[data-toggle="popover"]').popover({html:true});$("form.update_material").submit(function(e){$(this).update_material();e.preventDefault();});
</script>