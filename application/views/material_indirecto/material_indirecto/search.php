<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='img-thumbnail-60 hidden-sm'><div class="img-thumbnail-60"></div></td>
		<td class='hidden-sm celda-sm-10'>
			<form class="view_materiales">
				<input class='form-control form-control-sm search_material' type="search" id="s_cod" placeholder='Código'/>
			</form>
		</td>
		<td class='celda-sm-30'>
			<form class="view_materiales">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_material" maxlength="100" placeholder='Buscar...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_materiales" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' style="width:13%">
			<input type="number" class="form-control form-control-sm search_material" id="s_can" placeholder="Cantidad" onwheel="$(this).reset_input()" min="0">
		</td>
		<td class='hidden-sm' style="width:42%"></td>
		<td style="width:5%" class="hidden-sm text-right">
			<?php 
				$search=json_encode(array('function'=>'view_materiales','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_materiales','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>		
</table>
<script>Onfocus('s_cod');$(".search_material").search_material();$(".view_materiales").view_materiales();</script>