<?php 
	$j_materiales=json_encode($materiales);
	$url=base_url().'libraries/img/';
?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
		<tr>
			<th class='img-thumbnail-60'><div class="img-thumbnail-60">#Item</div></th>  
			<th class='hidden-sm celda-sm-10'>Código</th>  
			<th class='celda-sm-30'>Nombre</th>
			<th class='' style='width:10%'>Cantidad</th>
			<th class='hidden-sm' style='width:45%'>Descripcion</th>
			<th style='width:5%'></th>
    	</tr>
    </thead>
    <tbody>
<?php 
    if(count($materiales)>0){
    for($i=0;$i<count($materiales);$i++){ $material=$materiales[$i];?>
    <tr data-me="<?php echo $material->idme;?>">
    	<td><?php $img="sistema/miniatura/default.jpg";
				if($material->fotografia!="" & $material->fotografia!=NULL){
					$img="materiales/miniatura/".$material->fotografia;
				}
			?>
			<div id="item"><?php echo $i+1;?></div>
			<img src="<?php echo $url.$img;?>" class='img-thumbnail img-thumbnail-60' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
    	</td>  
	    <td class='hidden-sm'><?php echo $material->codigo; ?></td>  
		<td><?php echo $material->nombre; ?></td>
		<td class='celda-sm text-right'>
			<div class="label-main">
			    <label class="label label-inverse-<?php if($material->cantidad<=0){ echo 'danger';}else{ if($material->cantidad>10){echo 'success';}else{ echo 'warning';}}?>"><?php echo $material->cantidad.' '.$material->abr.'.';?></label>
			</div>
		</td>
		<td class='hidden-sm'><?php echo $material->descripcion;?></td>
		<td class="text-right">
		<?php 
			$det=json_encode(array('function'=>'detalle_material','atribs'=> json_encode(array('me' => $material->idme)),'title'=>'Detalle'));
			$conf=""; if($privilegio->ot1u=="1"){ $conf=json_encode(array('function'=>'config_material','atribs'=> json_encode(array('me' => $material->idme)),'title'=>'Configurar'));}
			$del=""; if($privilegio->ot1d=="1"){ $del=json_encode(array('function'=>'confirm_material','atribs'=> json_encode(array('me' => $material->idme)),'title'=>'Eliminar'));}
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
		</td>
    </tr>
	<?php }}else{
		echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";
	}?>
	</tbody>
</table>
<?php if($privilegio->ot1r=="1" && ($privilegio->ot1p=="1" || $privilegio->ot1c=="1")){?>
	<div id="fab-indirecto">
		<div id="btns-fab-wrapper" class="btns-fab-wrapper">
		<?php if($privilegio->ot1r=="1" && $privilegio->ot1p=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-yellow-darken waves-effect waves-light print_materiales" data-tbl="table#tbl-container" data-toggle="tooltip" data-placement="left" title="Reportes">
			        <span><i class="icon-clipboard7"></i></span>
			    </button>
			</div>
		<?php }?>
		<?php if($privilegio->ot1r=="1" && $privilegio->ot1c=="1"){?>
			<div class="horizontal-fab">
			    <button class="sub_fab_btn bg-success waves-effect waves-light new_material" data-toggle="tooltip" data-placement="left" title="Nuevo Empleado">
			        <span>+</span>
			    </button>
			</div>
		<?php }?>
		</div>
		<button id="btn-fab-main" class="gfab-main-btn bg-danger waves-effect waves-light"><span><i class="icon-menu"></i></span></button>
	</div>
<?php }?>
<script>$("img.img-thumbnail-60").visor();$(".detalle_material").detalle_material();$(".config_material").config_material();$(".confirm_material").confirm_material();$(document).blur_all("");
<?php if($privilegio->ot1r=="1" && $privilegio->ot1c=="1" || $privilegio->ot1p=="1"){?>
	$("div#fab-indirecto").jqueryFab();
	<?php if($privilegio->ot1c=="1"){?>
		$("button.new_material").new_material();
	<?php }?>
	<?php if($privilegio->ot1p=="1"){?>
		$("button.print_materiales").print_materiales();
	<?php }?>
<?php }?></script>