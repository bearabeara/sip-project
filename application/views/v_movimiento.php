<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Movimiento','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
			if($privilegio[0]->mo1r==1){ $v_menu.="Pedidos/pedido/icon-clipboard2|"; }
			if($privilegio[0]->mo3r==1){ $v_menu.="Compras/compra/icon-cart";}
		?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'movimiento','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>		
		<div id="search"></div>
		<div id="contenido"></div>
	</body>
	<?php $this->load->view('estructura/js',['js'=>$dir.'movimiento'.$min.'.js']);?>
<?php 
	$title="";$activo="";$search="";$view="";
	switch($pestania){
		case '1': if($privilegio[0]->mo1r==1){ $title="Pedidos"; $activo='pedido'; $search="movimiento/search_pedido"; $view="movimiento/view_pedido";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
		case '2': if($privilegio[0]->mo2r==1){ $title="Ventas"; $activo='venta'; $search="movimiento/search_venta"; $view="movimiento/view_venta";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		case '3': if($privilegio[0]->mo3r==1){ $title="Compras"; $activo='compra'; $search="movimiento/search_compra"; $view="movimiento/view_compra";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		default: $title="404"; $view=base_url()."login/error";
	}
?>
	<script>$(this).get_2n('<?php echo $search;?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','movimiento?p=<?php echo $pestania;?>');</script>
</html>