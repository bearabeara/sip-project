<!DOCTYPE html>
<?php $dir="final/";$min=".min";?>
<html lang="es" class="no-js">
	<head><?php $this->load->view('estructura/head',['title'=>'Capital Humano','css'=>'']);?></head>
	<body class="sidebar-mini fixed sidebar-collapse">
		<?php $this->load->view('estructura/modal');?>
		<?php $v_menu="";
			if($privilegio[0]->ca1r==1){ $v_menu.="Capital humano/empleado/fa fa-users|"; }
			if($privilegio[0]->ca2r==1){ $v_menu.="Planilla/planilla/icon-archive2|";}
			if($privilegio[0]->ca3r==1){ $v_menu.="Directivos/directivo/fa fa-black-tie|";}
			if($privilegio[0]->ca5r==1){ $v_menu.="Configuracion/config/glyphicon glyphicon-cog";}
		?>
		<?php $this->load->view('estructura/menu_top',['menu'=>$v_menu,'privilegio'=>$privilegio[0]]);?>
		<?php $this->load->view('estructura/chat');?>
		<?php $almacenes=$this->M_almacen->get_all(); ?>
		<?php $this->load->view('estructura/menu_izq',['ventana'=>'capital_humano','privilegio'=>$privilegio[0],"almacenes" => $almacenes]);?>			
		<div id="search"></div>
		<div id="contenido"></div>
	</body>
	<?php $this->load->view('estructura/js',['js'=>$dir.'capital_humano'.$min.'.js']);?>
<?php 
	$title="";$activo="";$search="";$view="";
	switch($pestania){
		case '1': if($privilegio[0]->ca1r==1){ $title="Empleados"; $activo='empleado'; $search="capital_humano/search_empleado"; $view="capital_humano/view_empleado";}else{ $title="locked"; $view=base_url()."login/locked"; } break;
		case '2': if($privilegio[0]->ca2r==1){ $title="Planilla de sueldos"; $activo='planilla'; $search="capital_humano/search_planilla"; $view="capital_humano/view_planilla";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		case '3': if($privilegio[0]->ca2r==1){ $title="Directivos"; $activo='directivo'; $search="capital_humano/search_directivo"; $view="capital_humano/view_directivo";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		case '5': if($privilegio[0]->ca5r==1){ $title="Configuración de capital humano"; $activo='config'; $search=""; $view="capital_humano/view_config";}else{ $title="locked"; $view=base_url()."login/locked"; }  break;
		default: $title="404"; $view=base_url()."login/error";
	} ?>
		<script>$(this).get_2n('<?php echo $search;?>',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'<?php echo $view; ?>',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar('<?php echo $activo;?>','<?php echo $title;?>','capital_humano?p=<?php echo $pestania;?>');</script>
</html>