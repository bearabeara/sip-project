<?php 
	$help1='title="<h4>Posición<h4>" data-content="Representa al orden en el que se generar los comprobantes, de las compras o egresos."';
	$help2='title="<h4>Cuenta<h4>" data-content="Seleccione las cuentas que seran usadas en el comprobante, la opcion <b>CUENTA DE EGRESO</b> es fijo pues toma la cuenta seleccionada en el registro de compra o egreso antes de generar el comprobante."';
	$popover='data-toggle="popover" data-placement="bottom" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
	if (count($usuarios)>0) {  $url=base_url().'libraries/img/personas/miniatura/'; 
?>
<table width="100%" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th class="img-thumbnail-60"><div class="img-thumbnail-60">#Item</div></th>
			<th class="celda-sm-10 hidden-sm">NIT/CI</th>
			<th class="celda-sm-30">Nombre completo</th>
			<th width="6.666666667%">Alm.</th>
			<th width="6.666666667%">Prod.</th>
			<th width="6.666666667%">Mov.</th>
			<th width="6.666666667%">Cap. Hum.</th>
			<th width="6.666666667%">Clie. Prov.</th>
			<th width="6.666666667%">Act. Fijo</th>
			<th width="6.666666667%">Otr. Mat. Ins.</th>
			<!--<th width="6.666666667%">Contab.</th>-->
			<th width="6.666666667%">Admin.</th>
		</tr>
	</thead>
	<tbody>
	<?php for ($j=0; $j < count($usuarios); $j++) { $usuario=$usuarios[$j]; 
			$img="default.png";
			if($usuario->fotografia!='' && $usuario->fotografia!=NULL){ $img=$usuario->fotografia; } ?>
		<tr>
		<td class="img-thumbnail-60">
			<div id="item"><?php echo $j+1;?></div><img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno;?>" data-desc="<br/>s">
		</td>
			<td class="hidden-sm"><?php echo $usuario->ci.' '.$usuario->abreviatura;?></td>
			<td><?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno; ?></td>
	<?php $privilegio=$this->M_privilegio->get_row('idus',$usuario->idus); 
			
	?>
			<td><center>
				<?php if($privilegio[0]->al==1){ ?>
					<div class="btn-circle btn-circle-grey btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="al" data-color="grey" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="al" data-color="grey" data-size="30"></div>
				<?php } ?>
				<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="al">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->pr==1){ ?>
					<div class="btn-circle btn-circle-success btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="pr" data-color="success" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="pr" data-color="success" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="pr">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->mo==1){ ?>
					<div class="btn-circle btn-circle-info btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="mo" data-color="info" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="mo" data-color="info" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="mo">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ca==1){ ?>
					<div class="btn-circle btn-circle-purple btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ca" data-color="purple" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ca" data-color="purple" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ca">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->cl==1){ ?>
					<div class="btn-circle btn-circle-teal btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="cl" data-color="teal" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="cl" data-color="teal" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="cl">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ac==1){ ?>
					<div class="btn-circle btn-circle-orange btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ac" data-color="orange" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ac" data-color="orange" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ac">+Detalle</a>
			</center></td>
			<td><center>
				<?php if($privilegio[0]->ot==1){ ?>
					<div class="btn-circle btn-circle-yellow btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ot" data-color="yellow" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ot" data-color="yellow" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ot">+Detalle</a>
			</center></td>
			<!--Contabilidad aun no desarrollado-->
			<!--<td><center>
				<?php if($privilegio[0]->co==1){ ?>
					<div class="btn-circle btn-circle-cyan btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="co" data-color="cyan" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="co" data-color="cyan" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="co">+Detalle</a>
			</center></td>-->
			<td><center>
				<?php if($privilegio[0]->ad==1){ ?>
					<div class="btn-circle btn-circle-red btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ad" data-color="red" data-size="30"></div>
				<?php }else{ ?>
					<div class="btn-circle btn-circle-default btn-circle-30 update_privilegio" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ad" data-color="red" data-size="30"></div>
				<?php } ?>
			<a href="javascript:" class="detail_modal" data-pri="<?php echo $privilegio[0]->idpri;?>" data-col="ad">+Detalle</a>
			</center></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php } else{ ?>
<h3>No existen registros</h3>
<?php }?>
<script>$('[data-toggle="popover"]').popover({html:true});$("img.img-thumbnail-60").visor();$("div.update_privilegio").update_privilegio();$(".detail_modal").detail_modal();</script>