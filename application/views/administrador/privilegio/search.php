<?php $v = array('1' => 'Administrador', '0' => 'Usuarios Simple' );?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm img-thumbnail-60'><div class="img-thumbnail-60"></div></td>
		<td class='celda-sm-10 hidden-sm'><form class="view_privilegios"><input type="text" class="form-control form-control-sm search_privilegio" placeholder="CI" id="s_ci"></form></td>
		<td class='celda-sm-30'>
			<form class="view_privilegios">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_privilegio" placeholder='Buscar...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_privilegios" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' width="60%"></td>
		<td class="hidden-sm">
		<?php 
			$search=json_encode(array('function'=>'view_privilegios','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
			$all=json_encode(array('function'=>'view_privilegios','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus('s_ci');$(".view_privilegios").view_privilegios();$(".search_privilegio").search_privilegio();</script>