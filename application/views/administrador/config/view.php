<?php
	$help1='title="Nombre de pais" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-°.,:;ªº)<b>"';
	$help2='title="Nombre de ciudad" data-content="Ingrese un nombre de ciudad alfanumerico <strong>de 2 a 100 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-°.,:;ªº)<b>"';
	$help3='title="Nombre de pais" data-content="Seleccione el pais donde pertenece la ciudad, el valor vacio no es aceptado."';
	$help4='title="Sigla de ciudad" data-content="Representa a la sigla o abreviatura de la ciudad, es usada principalmente en los numero de cédula de identidad. se acepta valores alfanumeros<strong> sin estacion, de 1 a 4 caracteres</strong>"';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
	$v = array('0'=>'Actualización pendiente','1'=>'Actualización en proceso','2'=>'Actualización terminada');
?>
<div class="row" style="margin-right: 0px; padding-left:5px;">
	<div class="col-md-6" style="padding-right: 5px;">
		<h4>PAISES</h4>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="85%">
					<div class="input-group">
						<div class="form-control form-control-sm" disabled>Nombre</div>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div></th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php for($i=0; $i<count($paises); $i++){ $pais=$paises[$i]; $c=$this->M_ciudad->get_row('idpa',$pais->idpa);?>
			<tr><td><?php echo $i+1;?></td>
				<td>
				<form onsubmit="return update_pais('<?php echo $pais->idpa;?>')"><input type="text" class="form-control form-control-sm" id="p<?php echo $pais->idpa;?>" placeholder='Nombre de pais' value="<?php echo $pais->nombre;?>" maxlength="100"></form></td>
				<?php $str="";
					if(count($c)>0){
						$str="<span class='text-danger'>¡Imposible Eliminar el color, <strong>esta siendo usado por ".count($c)." ciudad(es)</strong>!</span>";
						$fun="disabled";
					}else{
						$fun="alerta_pais('".$pais->idpa."')";
					}
					$help='title="Detalles de pais" data-content="<b>Nombre: </b>'.$pais->nombre.'<br>'.$str.'"';
				?>
				<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover2.$help,'guardar'=>"update_pais('".$pais->idpa."')",'eliminar'=>$fun]);?></td>
			</tr>
		<?php } ?>
			</tbody>
			<thead>
				<tr>
					<th colspan="3" class="text-center">NUEVO</th>
				</tr>
				<tr>
					<td colspan="2"><form onsubmit="return save_pais()">
						<div class="input-group">
							<input type="text" id="p" class="form-control form-control-sm" placeholder='Nombre de pais'>
							<span class="input-group-addon form-control-sm" <?php echo $popover3.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form></td>
					<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_pais()",'detalle'=>"",'eliminar'=>""]);?></td>
				</tr>
			</thead>
		</table>
		<h4>ACTUALIZACIONES DEL SISTEMA</h4>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="5%">#</th>
					<th width="35%">Inicio</th>
					<th width="35%">Finalización</th>
					<th width="15%">Estado</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>
		<?php if(!empty($actualizaciones)){?>
			<?php for($i=0; $i<count($actualizaciones); $i++){ $actualizacion=$actualizaciones[$i];?>
				<tr><td><?php echo $i+1;?></td>
					<td><?php if($actualizacion->fecha_inicio!=null){ $f=explode(" ",$actualizacion->fecha_inicio); echo $this->lib->format_date($f[0],"d ml Y")." ".$f[1]; } ?></td>
					<td><?php if($actualizacion->fecha_fin!=null){ $f=explode(" ",$actualizacion->fecha_fin); echo $this->lib->format_date($f[0],"d ml Y")." ".$f[1]; } ?></td>
					<td><span class="label label-<?php if($actualizacion->estado==0){ echo 'danger';}if($actualizacion->estado==1){ echo 'info';}if($actualizacion->estado==2){ echo 'success';}?> label-md"><?php echo $v[$actualizacion->estado];?></span></td>
					<td>
						<div class="g-control-accordion" style="width:60px;">
				      		<button class="change_actualizacion" data-ac="<?php echo $actualizacion->idac;?>"><i class="fa fa-cog"></i></button>
			      			<button class="confirmar_actualizacion" data-ac="<?php echo $actualizacion->idac;?>"><i class="icon-trashcan2"></i></button>
						</div>
					</td>
				</tr>
			<?php } ?>
			<?php }else{ ?>
				<tr>
					<td colspan="5" class="text-center"><h5>0 registros encontrados...</h5></td>
				</tr>
			<?php } ?>
			</tbody>
			<thead>
				<tr>
					<th colspan="5">
						<div class="row text-right" style="padding-right:15px;">
							<button type="button" class="btn btn-flat flat-primary txt-primary waves-effect btn-mini new_actualizacion"><i class="fa fa-plus"></i><span class="m-l-10">Actualización</span></button>
						</div>
					</th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-6" style="padding-right: 5px;">
		<h4>CIUDADES</h4>
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="45%">
						<div class="input-group">
							<div class="form-control form-control-sm" disabled>Nombre</div>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div></th>
						<th width="10%">
						<div class="input-group">
							<div class="form-control form-control-sm" disabled>Sigla</div>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div></th>
						<th width="30%">
						<div class="input-group">
							<div class="form-control form-control-sm" disabled>Pais</div>
							<span class="input-group-addon form-control-sm" <?php echo $popover2.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div></th>
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
			<?php for ($i=0; $i < count($ciudades) ; $i++) { $ciudad=$ciudades[$i]; $p=$this->M_persona->get_row('idci',$ciudad->idci);?>
				<tr>
					<td><?php echo $i+1;?></td>
					<td><form onsubmit="return update_ciudad('<?php echo $ciudad->idci;?>')"><input type="text" class="form-control form-control-sm" id="c<?php echo $ciudad->idci;?>" placeholder='Nombre de ciudad' value="<?php echo $ciudad->ciudad;?>" maxlength="100"></form></td>
					<td><form onsubmit="return update_ciudad('<?php echo $ciudad->idci;?>')"><input type="text" class="form-control form-control-sm" id="s<?php echo $ciudad->idci;?>" placeholder='Sigla de ciudad' value="<?php echo $ciudad->abreviatura;?>" maxlength="4"></form></td>
					<td>
						<select id="cp<?php echo $ciudad->idci;?>" class="form-control form-control-sm">
							<option value="">Seleccionar...</option>
						<?php for ($j=0; $j < count($paises) ; $j++) { $pais=$paises[$j]; ?>
							<option value="<?php echo $pais->idpa;?>" <?php if($pais->idpa==$ciudad->idpa){ echo "selected";}?>><?php echo $pais->nombre;?></option>
						<?php }?>
						</select>
					</td>
					<?php $str="";
						if(count($p)>0){
							$str="<hr><span class='text-danger'>¡Imposible Eliminar el la ciudad, <strong>esta siendo usada por ".count($p)." persona(s)</strong>!</span>";
							$fun="disabled";
						}else{
							$fun="alerta_ciudad('".$ciudad->idci."')";
						}
						$help='title="Detalles de ciudad" data-content="<b>Nombre: </b>'.$ciudad->ciudad.'<br>'.$str.'"';
					?>
					<td><?php $this->load->view('estructura/botones/botones_registros',['detalle'=>$popover2.$help,'guardar'=>"update_ciudad('".$ciudad->idci."')",'eliminar'=>$fun]);?></td>
				</tr>
			<?php } ?>
				</tbody>
				<thead>
					<tr><th colspan="3" class="text-center">NUEVO</th></tr>
					<tr>
						<td colspan="2"><form onsubmit="return save_ciudad()">
							<div class="input-group">
								<input type="text" id="c" class="form-control form-control-sm" placeholder='Nombre de ciudad' maxlength="100">
								<span class="input-group-addon form-control-sm" <?php echo $popover3.$help2;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form></td>
						<td><form onsubmit="return save_ciudad()">
							<div class="input-group">
								<input type="text" id="s" class="form-control form-control-sm" placeholder='Sigla de ciudad' maxlength="4">
								<span class="input-group-addon form-control-sm" <?php echo $popover3.$help4;?>><i class='fa fa-info-circle'></i></span>
							</div>
						</form></td>
						<td>
						<div class="input-group">
							<select id="cp" class="form-control form-control-sm">
								<option value="">Seleccionar...</option>
							<?php for ($i=0; $i < count($paises) ; $i++) { $pais=$paises[$i]; ?>
								<option value="<?php echo $pais->idpa;?>"><?php echo $pais->nombre;?></option>
							<?php }?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover3.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
						</td>
						<td><?php $this->load->view('estructura/botones/botones_registros',['guardar'=>"save_ciudad()",'detalle'=>"",'eliminar'=>""]);?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script language='javascript'>$('[data-toggle="popover"]').popover({html:true}); $("button.new_actualizacion").click(function(){ $(this).new_actualizacion();});$("button.change_actualizacion").click(function(){ $(this).change_actualizacion();});$("button.confirmar_actualizacion").click(function(){ $(this).confirmar_actualizacion();});</script>