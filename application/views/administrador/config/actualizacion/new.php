<?php
date_default_timezone_set("America/La_Paz");
$help1='title="Inicio de actualización" data-content="Ingrese la fecha y hora cuando se iniciara la actualización del sistema."';
$help2='title="Fin de actualización" data-content="Ingrese la fecha y hora cuando se concluira la actualización del sistema."';
$help3='title="Detalle de la actualización" data-content="ingrese un detalle informativo de la actualización del sistema, el contenido debe ser alfanumerico de 5 a 1000 caracteres <b>puede incluir espacios</b>, solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;)<b>"';
$help4='title="Estado de actualización" data-content="Seleccione una opción, si la actualización esta pendiente, en proceso o finalizado"';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
$v = array('0'=>'Actualización pendiente','1'=>'Actualización en proceso','2'=>'Actualización terminada');
?>
<div class="row"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><span class='text-danger'>(*)</span> Campo obligatorio</div></div>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Inicio de actualización:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="n_ini" type="datetime-local" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" placeholder="2000-01-31 20:00:00">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Fin de actualización:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<input class="form-control form-control-xs" id="n_fin" type="datetime-local" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" placeholder="2000-01-31 20:00:00">
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Detalle:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<textarea class="form-control form-control-xs" id="n_des" placeholder='Detalle de la actualización' minlength="5" maxlength="1000" rows="5"></textarea>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>	
			<div class="form-group">
				<div class="col-sm-2 col-xs-12"><label for="example-text-input" class="form-control-label"><span class='text-danger'>(*)</span> Estado de actualización:</label></div>
				<div class="col-sm-10 col-xs-12">
					<form onsubmit="return save_cliente()">
						<div class="input-group">
							<select class="form-control form-control-xs" id="n_est">
								<option value="">Seleccionar...</option>
						<?php for ($i=0; $i < count($v); $i++) { ?>
								<option value="<?php echo $i;?>"><?php echo $v[$i];?></option>
						<?php } ?>
							</select>
							<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</div>
			</div><i class="clearfix"></i>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="img-thumbnail-50">#Item</th>
					<th width="95%">Usuarios a ser bloqueados</th>
					<th width="5%"></th>
				</tr>
			</thead>
			<tbody id="content_user"></tbody>
		</table>
		<br>
	<div class="row text-right" style="padding-right:15px;">
		<button type="button" class="btn btn-flat flat-primary txt-primary waves-effect btn-mini new_usuario" data-contenedor="content_user" id="btn_new_usuario"><i class="fa fa-plus"></i><span class="m-l-10">Usuarios</span></button>
	</div>
	</div>
</div>
</div>
<script language='javascript'>Onfocus("n_ini");$('[data-toggle="popover"]').popover({html:true});$("button.new_usuario").click(function(){ $(this).new_config_usuario();});</script>
