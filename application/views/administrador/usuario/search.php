<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm img-thumbnail-60'><div class="img-thumbnail-60"></div></td>
		<td class='celda-sm-10 hidden-sm'><form class="view_usuarios"><input type="text" class="form-control form-control-sm search_usuario" placeholder="CI" id="s_ci" value=""></form></td>
		<td class='celda-sm-30'>
			<form class="view_usuarios">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_usuario" placeholder='Buscar...' data-tbl="table#tbl-container"/>
					<span class="input-group-addon form-control-sm view_usuarios" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' width="15%"><form class="view_usuarios"><input type="text" class="form-control form-control-sm search_usuario" placeholder="Teléfono" id="s_tel" value=""></form></td>
		<td class='hidden-sm' width="15%"><form class="view_usuarios"><input type="text" class="form-control form-control-sm search_usuario" placeholder="Cargo" id="s_car" value=""></form></td>
		<td class='hidden-sm' width="17%"><form class="view_usuarios"><input type="text" class="form-control form-control-sm search_usuario" placeholder="Nombre de usuario" id="s_usu" value=""></form></td>
		<td class="hidden-sm" width="13%"></td>
		<td class="hidden-sm">
		<?php 
			$search=json_encode(array('function'=>'view_usuarios','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
			$all=json_encode(array('function'=>'view_usuarios','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>
</table>
<script>Onfocus('s_ci');$(".view_usuarios").view_usuarios();$(".search_usuario").search_usuario();</script>