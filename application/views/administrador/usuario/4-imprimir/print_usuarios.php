<?php $url=base_url().'libraries/img/';
$tipos=array("Acceso solo a productos","Acceso solo a sistema","Acceso a sistema y productos");
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%; overflow: initial;">
	<?php $fun = array('function' => 'print_usuarios', 'atribs' => array('tbl' => $tbl));
		$excel = array('controller' => 'administrador/export_usuarios?ci='.$ci.'&nom='.$nom."&tel=".$tel."&car=".$car."&usu=".$usu."&file=xls");
		$word = array('controller' => 'administrador/export_usuarios?ci='.$ci.'&nom='.$nom."&tel=".$tel."&car=".$car."&usu=".$usu."&file=doc");
		$this->load->view('estructura/tools_print',['refresh'=> json_encode($fun),'xls'=>json_encode($excel),'doc'=>json_encode($word),'text_menu'=>true]);
	?>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive">
			<table class="tabla tabla-border-true">
						<tr class="fila">
							<th class="celda td" style="vertical-align: top; width: 3%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="1" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>#</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 6%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="2" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Fot.</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 8%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="3" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>CI</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 45%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="4" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Nombre</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="5" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Cargo</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="6" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Teléfono</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 5%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="7" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Email</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="8" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Usuario</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 10%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="9" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Tipo de acceso</small></label>
								</div>
							</th>
							<th class="celda td" style="vertical-align: top; width: 45%">
								<div class="checkbox-fade fade-in-primary">
									<label class='g-label-print'><input type="checkbox" data-column="10" checked="checked"><span class="cr custom-control-description"><i class="cr-icon fa fa-check txt-primary "></i></span><small>Observaciónes</small></label>
								</div>
							</th>
						</tr>
			</table>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%;">
		<div class="table-responsive" id="area">
			<table class="tabla tbl-bordered font-10">
						<thead>
							<tr class="fila title" style="text-align: center;">
								<td class="celda title" colspan="19" style="border-top: hidden;border-left: hidden;border-right: hidden;padding: 0px 0px 2px 0px;">
									<?php $this->load->view('estructura/print/header-print',['titulo'=>'USUARIOS DE SISTEMA']);?>
								</td>
							</tr>
							<tr class="fila">
								<th class="celda th padding-4" data-column="1" width="3%" style="display: none;">#</th>
								<th class="celda th padding-4" data-column="2" width="6%">Fot.</th>
								<th class="celda th padding-4" data-column="3" width="8%">CI</th>
								<th class="celda th padding-4" data-column="4" width="45%">Nombre</th>
								<th class="celda th padding-4" data-column="5" width="10%">Cargo</th>
								<th class="celda th padding-4" data-column="6" width="10%">Teléfono</th>
								<th class="celda th padding-4" data-column="7" width="10%">Email</th>
								<th class="celda th padding-4" data-column="8" width="10%">Usuario</th>
								<th class="celda th padding-4" data-column="9" width="10%">Tipo de acceso</th>
								<th class="celda th padding-4" data-column="10" width="45%">Observaciónes</th>
							</tr>
						</thead>
						<tbody>
							<?php $cont=0;
								foreach($visibles as $key => $visible){
									$usuario=$this->lib->search_elemento($usuarios,"idus",$visible);
									if($usuario!=null){
										$img='sistema/miniatura/default.jpg';
										if($usuario->fotografia!=NULL && $usuario->fotografia!=""){ $img="personas/miniatura/".$usuario->fotografia;}
										$cont++;
							?>
								<tr class="fila">
									<td class="celda td padding-4" data-column="1" style="display: none;"><?php echo $cont;?></td>
									<td class="celda td img" data-column="2">
										<div class="item"><?php echo $cont;?></div>
										<img src="<?php echo $url.$img;?>" class="img-thumbnail-50">
									</td>
									<td class="celda td padding-4" data-column="3"><?php echo $usuario->ci.$usuario->abreviatura;?></td>
									<td class="celda td padding-4" data-column="4"><?php echo $usuario->nombre_completo;?></td>
									<td class="celda td padding-4" data-column="5"><?php echo $usuario->cargo;?></td>
									<td class="celda td padding-4" data-column="6"><?php echo $usuario->telefono;?></td>
									<td class="celda td padding-4" data-column="7"><?php echo $usuario->email;?></td>
									<td class="celda td padding-4" data-column="8"><?php echo $usuario->usuario;?></td>
									<td class="celda td padding-4" data-column="9"><?php echo $tipos[$usuario->tipo];?></td>
									<td class="celda td padding-4" data-column="10"><?php echo $usuario->descripcion;?></td>
								</tr>
							<?php }//end if
								}//end for ?>
						</tbody>
			</table>
		</div>
	</div>
</div>
<script>$("input[type='checkbox']").change_column_print();</script>