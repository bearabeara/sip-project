<?php 
	$url=base_url().'libraries/img/personas/';
	$img="default.png";
	if($usuario->fotografia!=NULL && $usuario->fotografia!=''){$img=$usuario->fotografia;}
?>
<div class="row">
	<div class="col-sm-4 col-xs-12 text-center"><img src="<?php echo $url.$img;?>" class="img-thumbnail" alt=""><hr></div>
	<div class="col-sm-8 cols-xs-12">
		<div class="table-responsive">
			<table border="0" class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th" width="25%">CI</th>
					<td class="celda td" width="75%"><?php echo $usuario->ci." ".$usuario->abreviatura;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Nombre completo:</th><td class="celda td"><?php echo $usuario->nombre." ".$usuario->nombre2." ".$usuario->paterno." ".$usuario->materno;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Teléfono:</th><td class="celda td"><?php echo $usuario->telefono;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Cargo:</th><td class="celda td"><?php echo $usuario->cargo;?></td>
				</tr>
				<tr class="fila">
					<th class="celda th">Usuario:</th><td class="celda td"><?php echo $usuario->usuario;?></td>
				</tr>
			</table>
		</div>
	</div>
</div>