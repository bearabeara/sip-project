<?php 
	$help1='title="Subir Fotografía" data-content="Seleccione una fotografía, preferiblemente una imagen que no sobrepase <strong>2MB de tamaño</strong>, los tipos de imagenes aceptadas son: <strong>*.jpeg, *.jpg, *.gif, *.png</strong>"';
	$help2='title="Nº Cédula" data-content="Ingrese un numero cédula de identidad con valores numericos <b>sin espacios</b>, de 6 a 9 digitos"';
	$help3='title="Ciudad" data-content="Seleccione la ciudad donde se difundio la cédula de identidad. Si desea adicionar una nueva ciudad puede hacerlo en la sección de configuración en el menu superior, o puede dar click el el boton <b>+</b>"';
	$help4='title="Primer nombre" data-content="Ingrese un nombre alfanumerico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help5='title="Segundo nombre" data-content="Ingrese un segundo nombre (si tuviera) alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el segundo nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help6='title="Apellido Paterno" data-content="Ingrese un apellido paterno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help7='title="Apellido Materno" data-content="Ingrese un apellido materno alfanumérico de 2 a 20 caracteres <b>puede incluir espacios</b>, ademas el apellido solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)<b>"';
	$help8='title="Teléfono" data-content="Ingrese el numero de telefono del usuario, <b>se acepta hasta 15 caracteres numericos</b>."';
	$help9='title="Nombre de usuario" data-content="Ingrese una nombre de usuario de sistema, <b>solo se acepta texto alfanumérico en minuscula de 4 de 15 caractereres sin espacios</b>."';
	$help10='title="Cargo" data-content="Ingrese el cargo o cargos que realiza. Ej. Encargado de cortado de piezas. Solo se acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº°)</b>, con un <b>máximo de 100 caractereres</b>."';
	$help11='title="Restablece contraseña" data-content="Cambia la contraseña anterior y pone como contraseña el número de cédula de identidad."';
	$help12='title="Tipo de acceso"  data-content="Seleccione el tipo de acceso del usuario, si solo puede ver los productos <strong>(Solo productos)</strong> o solo puede ver el sistema <strong>(Solo sistema)</strong> y puede ver ambos <strong>(Sistema y productos)</strong>"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover"';
?>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
<div class="row">
	<div class="hidden-sm"><div class="col-sm-4 offset-sm-8 col-xs-12 text-xs-right"><small><span class="text-danger">(*)</span> Campo obligatorio</small></div></div>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Fotografía:</label>
		<div class="col-sm-10 col-xs-12">
			<div class="input-group">
				<input class="form-control input-xs" id="n_fot" type="file" placeholder='Seleccione fotografia'>
				<span class="input-group-addon form-control-sm input-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Nº Cédula:</label>
		<div class="col-sm-4 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_ci" type="number" placeholder='Número de cedula de identidad' min='100000' max='999999999' step='any' value="<?php echo $usuario->ci;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Ciudad:</label>
		<div class="col-sm-4 col-xs-12">
			<div class="input-group">
				<select class="form-control input-xs" id="n_ciu">
					<option value="">Seleccionar...</option>
			<?php for($i=0; $i<count($ciudades); $i++){ $ciudad=$ciudades[$i]; ?>
					<option value="<?php echo $ciudad->idci;?>" <?php if($usuario->idci==$ciudad->idci){ echo "selected";}?>><?php echo $ciudad->nombre.'('.$ciudad->abreviatura.')';?></option>
			<?php } ?>
				</select>
				<a href="<?php echo base_url();?>administrador?p=7" target="_blank" title="Ver configuraciónes" class="input-group-addon form-control-sm"><i class="fa fa-plus"></i></a>
				<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
			</div>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Primer nombre:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_nom1" type="text" placeholder='Primero nombre del empleado' maxlength="20" value="<?php echo $usuario->nombre;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help4;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Segundo nombre:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_nom2" type="text" placeholder='Segundo nombre del empleado' maxlength="20" value="<?php echo $usuario->nombre2;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help5;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Apellido paterno:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_pat" type="text" placeholder='Apellido paterno del empleado' maxlength="20" value="<?php echo $usuario->paterno;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help6;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Apellido materno:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_mat" type="text" placeholder='Apellido materno del empleado' maxlength="20" value="<?php echo $usuario->materno;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Tipo de acceso:</label>
		<div class="col-sm-4 col-xs-12">
				<div class="input-group">
					<select class="form-control form-control-xs" id="n_tip">
						<option value="0" <?php if($usuario->tipo=="0"){ echo "selected";} ?>>Solo productos</option>
						<option value="1" <?php if($usuario->tipo=="1"){ echo "selected";} ?>>Solo sistema</option>
						<option value="2" <?php if($usuario->tipo=="2"){ echo "selected";} ?>>Sistema y productos</option>
					</select>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
				</div>
		</div>
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"> Teléfono/Celular:</label>
		<div class="col-sm-4 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" type="phone" id="n_tel" placeholder='N° de teléfono o celular' maxlength="15" value="<?php echo $usuario->telefono;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help8;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label"><span class='text-danger'>(*)</span> Nombre de usuario:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" type="text" id="n_usu" placeholder='Nombre de usuario de sistema' maxlength="15" value="<?php echo $usuario->usuario;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help9;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Cargo:</label>
		<div class="col-sm-10 col-xs-12">
			<form class="update_usuario" data-us="<?php echo $usuario->idus;?>">
				<div class="input-group">
					<input class="form-control input-xs" id="n_car" type="text" placeholder='Cargo del empleado' maxlength="100" value="<?php echo $usuario->cargo;?>">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help10;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</form>
		</div>
	</div><i class="clearfix"></i>
	<div class="form-group">
		<label for="example-text-input" class="col-sm-2 col-sm-2 col-xs-12  col-form-label form-control-label">Restablecer contraseña:</label>
		<div class="col-sm-10 col-xs-12">
				<div class="input-group">
					<input class="form-control input-xs" type="text" disabled="" value="Contraseña encriptada">
					<a href="javascript:" title="Restablecer..." class="input-group-addon form-control-sm confirm_reset_pass" data-us="<?php echo $usuario->idus;?>"><i class="fa fa-refresh"></i></a>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help11;?>><i class='fa fa-info-circle'></i></span>
				</div>
		</div>
	</div><i class="clearfix"></i>
</div>
</div>
</div>
<script>Onfocus("n_ci");$('[data-toggle="popover"]').popover({html:true});$("a.confirm_reset_pass").confirm_reset_pass();$("form.update_usuario").submit(function(e){$(this).update_usuario();e.preventDefault();});</script>