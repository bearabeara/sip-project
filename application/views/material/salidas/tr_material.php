<?php 
	date_default_timezone_set("America/La_Paz");
	$url=base_url().'libraries/img/';
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 300 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
	$img="sistema/miniatura/default.jpg";
	if($atrib!="" && $val!="" && $type_search!=""){
		$control=$this->lib->search_where($material,$atrib,$val,$type_search);
	}
	if($material->fotografia!="" && $material->fotografia!=NULL){
		$img="materiales/miniatura/".$material->fotografia;
	}
	$rand=rand(10,999999);
?>
<td>
	<div id="item"><?php echo $item;?></div>
	<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $material->nombre;?>" data-desc="<?php if(strlen($material->descripcion)>0){ echo $material->descripcion;}else{ echo "<br>";}?>" width='100%'>
</td>
<td><span><?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($material->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $material->nombre;}?></span>
	<br><span class="label label-inverse-default label-md visible-sm"><?php echo $material->cantidad.' '.$material->abr_u.'.';?></span>
</td>
<td class="hidden-sm">
	<div class="input-group input-120">
		<input type="text" id="c<?php echo $material->idam;?>" class="form-control form-control-sm" value="<?php echo $material->cantidad; ?>" disabled>
		<span class="input-group-addon form-control-sm"><?php echo $material->abr_u."."; ?></span>
	</div>
</td>
<td><input type="datetime-local" id="fech<?php echo $material->idam;?>" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" class="form-control form-control-sm"></td>
<td>
	<select id="emp<?php echo $material->idam;?>" class="form-control form-control-sm">
		<option value="">Seleccionar...</option>
		<?php for ($e=0; $e < count($empleados) ; $e++){ $empleado=$empleados[$e]; 
			if($empleado->estado=="1"){ ?>
			<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php 	}
		}?>
	</select>
</td>
<td>
	<div class="input-group input-120">
		<form class="save_movimiento-<?php echo $rand;?>" data-am="<?php echo $material->idam;?>" data-type="salida" data-item="<?php echo $item;?>">
			<input type="number" id="can<?php echo $material->idam;?>" class="form-control form-control-sm input-100" placeholder="0" min='0' step="any" min='0' max="999999999.9999999">
		</form>
		<span class="input-group-addon form-control-sm"><?php echo $material->abr_u."."; ?></span>
	</div>
</td>
<td>
	<div class="input-group">
		<textarea id="obs<?php echo $material->idam;?>" class="form-control form-control-sm" placeholder="Observaciónes de salida de material"></textarea>	
		<span class="input-group-addon form-control-sm" <?php echo $popover3;?>><i class='fa fa-info-circle'></i></span>
	</div>
</td>
<?php if($privilegio->al13u==1){ ?>
<td>
	<?php $save=json_encode(array('function'=>'save_movimiento-'.$rand,'atribs'=> json_encode(array('am' => $material->idam,'type' => "salida",'item'=>$item)),'title'=>'Guardar salida')); ?>
	<?php $this->load->view("estructura/botones/btn_registro",["save"=>$save]);?><script>$(".save_movimiento-<?php echo $rand;?>").save_movimiento();$("img.img-thumbnail-45").visor();</script>
</td>
<?php } ?>