<?php date_default_timezone_set("America/La_Paz");
	$url=base_url().'libraries/img/';
	$popover3='data-toggle="popover" data-placement="left" data-trigger="hover" title="Observaciónes" data-content="la observacion puede poseer un formato alfanumerico de 0 a 300 caracteres <b>puede incluir espacios, sin saltos de linea</b>, ademas la observacion solo acepta los siguientes caracteres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:;ªº)<b>"';
?>
<table class="table table-bordered table-hover" id="tbl-container">
<thead>
	<tr>
		<th class='img-thumbnail-45'><div class="img-thumbnail-45">#Item</div></th>
		<th width="20%">Nombre</th>
		<th width="12%" class="hidden-sm">Stock actual</th>
		<th width="16%">Fecha de Salida</th>
		<th width="16%">Solicitante</th>
		<th width="8%" >Cantidad</th>
		<th width="25%">Observaciónes</th>
		<?php if($privilegio->al13u==1){ ?>
		<th width="3%" ></th>
		<?php }?>
	</tr>
</thead><tbody>
<?php 
if(count($materiales)>0){
	for($i=0;$i<count($materiales);$i++){ $material=$materiales[$i];
		if($atrib!="" && $val!="" && $type_search!=""){
			$control=$this->lib->search_where($material,$atrib,$val,$type_search);
		}
	    $img="sistema/miniatura/default.jpg";
	    if($material->fotografia!="" && $material->fotografia!=NULL){
			$img="materiales/miniatura/".$material->fotografia;
		}
?>
	<tr<?php if($atrib!="" && $val!="" && $type_search!=""){if($control==null){?> style="display: none"<?php }}?> data-m="<?php echo $material->idm;?>" id="tr-<?php echo $material->idam;?>">
		<td>
			<div id="item"><?php echo $i+1;?></div>
			<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-45" data-title="<?php echo $material->nombre;?>" data-desc="<?php if(strlen($material->descripcion)>0){ echo $material->descripcion;}else{ echo "<br>";}?>" width='100%'>
		</td>
		<td><span data-col='1'><?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($material->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $material->nombre;}?></span>
			<br><span class="label label-inverse-default label-md visible-sm"><?php echo $material->cantidad.' '.$material->abr_u.'.';?></span>
		</td>
		<td class="hidden-sm">
			<div class="input-group input-120">
				<input type="text" id="c<?php echo $material->idam;?>" class="form-control form-control-sm" value="<?php echo $material->cantidad; ?>" disabled>
				<span data-col='2' data-value="<?php echo $material->cantidad;?>"></span>
				<span class="input-group-addon form-control-sm" data-col='3' data-value="<?php echo $material->idu;?>"><?php echo $material->abr_u."."; ?></span>
			</div>
		</td>
		<td><input type="datetime-local" id="fech<?php echo $material->idam;?>" value="<?php echo str_replace(' ', 'T', date('Y-m-d H:i'));?>" class="form-control form-control-sm"></td>
		<td>
			<select id="emp<?php echo $material->idam;?>" class="form-control form-control-sm">
				<option value="">Seleccionar...</option>
			<?php for ($e=0; $e < count($empleados) ; $e++){ $empleado=$empleados[$e]; 
					if($empleado->estado=="1"){ ?>
					<option value="<?php echo $empleado->ide;?>"><?php echo $empleado->nombre." ".$empleado->nombre2." ".$empleado->paterno." ".$empleado->materno;?></option>
			<?php 	}
				}?>
			</select>
		</td>
		<td>
			<div class="input-group input-120">
				<form class="save_movimiento" data-am="<?php echo $material->idam;?>" data-type="salida" data-item="<?php echo $i+1;?>">
					<input type="number" id="can<?php echo $material->idam;?>" class="form-control form-control-sm input-100" placeholder="0" min='0' step="any" min='0' max="999999999.9999999">
				</form>
				<span class="input-group-addon form-control-sm"><?php echo $material->abr_u."."; ?></span>
			</div>
		</td>
		<td>
			<div class="input-group">
				<textarea id="obs<?php echo $material->idam;?>" class="form-control form-control-sm" placeholder="Observaciónes de salida de material"></textarea>	
				<span class="input-group-addon form-control-sm" <?php echo $popover3;?>><i class='fa fa-info-circle'></i></span>
			</div>
		</td>
		<?php if($privilegio->al13u==1){ ?>
		<td>
			<?php $save=json_encode(array('function'=>'save_movimiento','atribs'=> json_encode(array('am' => $material->idam,'type' => "salida",'item'=>($i+1))),'title'=>'Guardar salida')); ?>
			<?php $this->load->view("estructura/botones/btn_registro",["save"=>$save]);?>
		</td>
		<?php } ?>
	</tr>
<?php }}else{
	echo "<tr><td colspan='10'><h2>0 registros encontrados...</h2></td></tr>";
} ?>
</tbody>
</table>
<?php 
	if($privilegio->al13r=="1" && $privilegio->al13a=="1"){
		if($privilegio->al13a=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_help"=>$help]);
	}
?>
<script>$('[data-toggle="popover"]').popover({html:true});$("img.img-thumbnail-45").visor();$(".save_movimiento").save_movimiento();</script>