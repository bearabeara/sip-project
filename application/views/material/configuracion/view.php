<?php 
	$help1='title="Nombre de unidad de medida" data-content="Ingrese un nombre alfanumerico <strong>de 2 a 40 caracteres con espacios</strong>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help2='title="Abreviatura" data-content="Ingrese una abreviatura alfanumerica <strong>de 1 a 8 caracteres con espacios</strong>, ademas la abreviatura solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>. Ej. Centímetros = cm"';
	$help3='title="Equivalencia de unidad" data-content="Ingrese una equivalencia numerica mayor que cero y con un maximo de 7 decimales. Esta equivalencia en usada por el sistema para calcular los materiales necesarios en producción."';
	$help4='title="Descripción de equivalencia" data-content="Ingrese una descripción de equivalencia alfanumerica de 0 a 200 caracteres <b>con espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>. Esta descripción debe contener a que unidad pertenece la equivalencia."';
	$help5='title="Nombre de color" data-content="Ingrese un nombre de color alfanumerico de 2 a 50 caracteres <b>con espacios</b>, ademas el nombre solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help55='title="Abreviatura" data-content="Ingrese un abreviatura del nombre de color alfanumerico hasta 10 caracteres <b>sin espacios</b>, ademas solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help6='title="Código de color" data-content="Seleccione un color que representa al color en cuestion"';
	$help7='title="Nombre de grupo" data-content="Ingrese un nombre de grupo alfanumerico <strong>de 2 a 50 caracteres con espacios</strong>, ademas el nombre de grupo solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>"';
	$help8='title="Descripción de grupo" data-content="Ingrese una descripción de grupo alfanumerica de 0 a 200 caracteres <b>con espacios</b>, ademas la descripción solo acepta los siguientes caractereres especiales <b>(áÁéÉíÍóÓúÚñÑ+-.,:°;ªº)<b>."';
	$popover='data-toggle="popover" data-placement="right" data-trigger="hover"';
	$popover2='data-toggle="popover" data-placement="left" data-trigger="hover"';
	$popover3='data-toggle="popover" data-placement="top" data-trigger="hover"';
?>
<div class="row" style="margin-right: 0px; padding-left:5px;">
<div class="col-md-6" style="padding-right: 5px;">
	<h4>Material: Unidades de Medida</h4>
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="	7%">#</th>
				<th width="	28%">
					<div class="input-group config">
						<div class="form-control form-control-sm">Nombre</div>
						<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</th>
				<th width="	10%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>A.</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help2;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="	10%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>Eq.</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help3;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="	45%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>Descripción</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover2.$help4;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="0%"></th>
			</tr>
		</thead>
		<tbody>
		<?php for($i=0; $i < count($unidades); $i++){ $unidad=$unidades[$i]; $m=$this->M_material_item->get_row('idu',$unidad->idu); ?>
			<tr>
				<td><?php echo $i+1;?></td>
				<td><form class="update_unidad" data-u="<?php echo $unidad->idu;?>"><input type="text" id="nom_u<?php echo $unidad->idu;?>" value='<?php echo $unidad->nombre;?>' class="form-control form-control-sm" placeholder='Nombre de unidad de medida' maxlength="40"></form></td>
				<td><form class="update_unidad" data-u="<?php echo $unidad->idu;?>"><input type="text" id="abr_u<?php echo $unidad->idu;?>" value="<?php echo $unidad->abr;?>" class="form-control form-control-sm" placeholder='Abreviatura de unidad de medida' maxlength="8"></form></td>
				<td><form class="update_unidad" data-u="<?php echo $unidad->idu;?>"><input type="number" id="equ_u<?php echo $unidad->idu;?>" value="<?php echo $unidad->equ;?>" class="form-control form-control-sm" placeholder='Equivalencia de unidad de medida' min="0" value="0"></form></td>
				<td><textarea id="des_equ<?php echo $unidad->idu;?>" class="form-control form-control-sm" placeholder='Descripcion de equivalencia' maxlength="200"><?php echo $unidad->descripcion_equ;?></textarea></td>
				<td>
					<?php $save="";$eli="";
						$det=json_encode(array('function'=>'detalle_unidad','atribs'=>json_encode(array('u'=>$unidad->idu)),'title'=>'Detalle de unidad'));
						if($privilegio->al15u==1){$save=json_encode(array('function'=>'update_unidad','atribs'=>json_encode(array('u'=>$unidad->idu)),'title'=>'Guardar cambios'));}
						$control=$this->M_material_item->get_row('idu',$unidad->idu);
						if($privilegio->al15d==1 && empty($control)){$eli=json_encode(array('function'=>'confirm_unidad','atribs'=>json_encode(array('u'=>$unidad->idu)),'title'=>'Eliminar unidad'));}
					?>
					<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	<?php if($privilegio->al15c==1){ ?>
		<thead>
			<tr>
				<th colspan="6" class="text-center">NUEVO</th>
			</tr>
			<tr>
				<td colspan="2"><form class="save_unidad"><input type="text" id="nom_u" class="form-control form-control-sm" placeholder='Nombre de unidad de medida' maxlength="40"></form></td>
				<td><form class="save_unidad"><input type="text" id="abr_u" class="form-control form-control-sm" placeholder='Abreviatura de unidad de medida' maxlength="8"></form></td>
				<td><form class="save_unidad"><input type="number" id="equ_u" class="form-control form-control-sm" placeholder='Equivalencia de unidad de medida' min="0" value="0"></form></td>
				<td><textarea id="des_equ" class="form-control form-control-sm" placeholder='Descripcion de equivalencia' maxlength="200"></textarea></td>
				<td>
					<?php $save="";$det="";$eli="";
						if($privilegio->al15c==1){$save=json_encode(array('function'=>'save_unidad','title'=>'Guardar nuevo'));}
					?>
					<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
				</td>
			</tr>
		</thead>
	<?php } ?>
	</table>
	</div>
	<h4>Material: Colores</h4>
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="10%">#</th>
				<th width="45%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>Nombre</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover3.$help5;?>><i class='fa fa-info-circle'></i></span>
				</div></th>
				<th width="15%">Color</th>
				<th width="30%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>Abr.</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover3.$help55;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="0%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for($i=0; $i < count($colores) ; $i++) { $color=$colores[$i]; $m=$this->M_material->get_row('idco',$color->idco);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
				<form class="update_color" data-c="<?php echo $color->idco;?>">
					<input type="text" id="nom_c<?php echo $color->idco;?>" value="<?php echo $color->nombre;?>" class="form-control form-control-sm" placeholder='Nombre de color' maxlength="50">
				</form>
			</td>
			<td><input type="color" id="cod_c<?php echo $color->idco;?>" value="<?php echo $color->codigo;?>" class="form-control form-control-sm" placeholder='#FFF'></td>
			<?php /*$str="";
				if(count($m)>0){
					$str="<span class='text-danger'>¡Imposible Eliminar el color, <strong>esta siendo usado por ".count($m)." materiales</strong> en almacenes!</span>";
					$eli="disabled";
				}else{
					$eli="alerta_color('".$color->idco."')";
				}
				$help='title="Detalles de color" data-content="<b>Nombre: </b>'.$color->nombre.'<br><b>Abreviatura: </b>'.$color->abr.'<br>'.$str.'"';
			?>
			<?php $mod="update_color('".$color->idco."')"; if($privilegio->al15u!=1){ $mod="";}if($privilegio->al15d!=1){ $eli="";}*/?>
			<td>
				<form class="update_color" data-c="<?php echo $color->idco;?>">
					<input type="text" id="abr_c<?php echo $color->idco;?>" value="<?php echo $color->abr;?>" class="form-control form-control-sm" placeholder='Abreviatura de color' maxlength="10">
				</form>
			</td>
			<td>
				<?php $save="";$eli="";
					$det=json_encode(array('function'=>'detalle_color','atribs'=>json_encode(array('c'=>$color->idco)),'title'=>'Detalle de color'));
					if($privilegio->al15u==1){$save=json_encode(array('function'=>'update_color','atribs'=>json_encode(array('c'=>$color->idco)),'title'=>'Guardar cambios'));}
					$control=$this->M_material->get_row('idco',$color->idco);
					if($privilegio->al15d==1 && empty($control)){$eli=json_encode(array('function'=>'confirm_color','atribs'=>json_encode(array('c'=>$color->idco)),'title'=>'Eliminar color'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
			</td>
		</tr>
	<?php }?>
		</tbody>
	<?php if($privilegio->al15c==1){ ?>
		<thead>
			<tr><th colspan="5" class="text-center">NUEVO</th></tr>
			<tr>
				<td colspan="2">
				<form class="save_color">
					<div class="input-group config">
						<input type="text" id="nom_c" class="form-control form-control-sm" placeholder='Nombre de color' maxlength="50">
						<span class="input-group-addon form-control-sm" <?php echo $popover3.$help5;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</form>
				</td>
				<td><input type="color" id="cod_c" class="form-control form-control-sm" placeholder='#FFF'></td>
				<td>
					<form class="save_color">
						<div class="input-group config">
							<input type="text" id="abr_c" class="form-control form-control-sm" placeholder='Abreviatura de color' maxlength="10">
							<span class="input-group-addon form-control-sm" <?php echo $popover3.$help55;?>><i class='fa fa-info-circle'></i></span>
						</div>
					</form>
				</td>
				<td>
					<?php $save="";$det="";$eli="";
						if($privilegio->al15c==1){$save=json_encode(array('function'=>'save_color','title'=>'Guardar nuevo'));}
					?>
					<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
				</td>
			</tr>
		</thead>
	<?php } ?>
	</table>
	</div>
</div>
<div class="col-md-6" style="padding-right: 5px;">
	<h4>Material: Grupos</h4>
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="7%">#</th>
				<th width="30%"><div class="input-group config">
					<div class="form-control form-control-sm" disabled>Nombre</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="13%"><div class="input-group config">
					<div class="form-control form-control-sm" disabled>Abr.</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help55;?>><i class='fa fa-info-circle'></i></span>
				</div>
				</th>
				<th width="40%">
				<div class="input-group config">
					<div class="form-control form-control-sm" disabled>Descripción</div>
					<span class="input-group-addon form-control-sm" <?php echo $popover2.$help8;?>><i class='fa fa-info-circle'></i></span>
				</div></th>
				<th width="10%"></th>
			</tr>
		</thead>
		<tbody>
	<?php for ($i=0; $i < count($grupos) ; $i++) { $grupo=$grupos[$i]; $m=$this->M_material->get_row('idmg',$grupo->idmg);?>
		<tr>
			<td><?php echo $i+1;?></td>
			<td>
				<form class="update_grupo" data-g="<?php echo $grupo->idmg;?>">
					<input type="text" id="nom_g<?php echo $grupo->idmg;?>" value="<?php echo $grupo->nombre;?>" class="form-control form-control-sm" placeholder='Nombre de Grupo' maxlength="50">
				</form>
			</td>
			<td>
				<form class="update_grupo" data-g="<?php echo $grupo->idmg;?>">
					<input type="text" id="abr_g<?php echo $grupo->idmg;?>" value="<?php echo $grupo->abr;?>" class="form-control form-control-sm" placeholder='Abreviatura de Grupo' maxlength="10">
				</form>
			</td>
			<td>
				<textarea id="des_g<?php echo $grupo->idmg;?>" cols="30" class="form-control form-control-sm" placeholder='Descripción del grupo' maxlength="100"><?php echo $grupo->descripcion;?></textarea>
			</td>
			<td>
				<?php $save="";$eli="";
					$det=json_encode(array('function'=>'detalle_grupo','atribs'=>json_encode(array('g'=>$grupo->idmg)),'title'=>'Detalle de grupo'));
					if($privilegio->al15u==1){$save=json_encode(array('function'=>'update_grupo','atribs'=>json_encode(array('g'=>$grupo->idmg)),'title'=>'Guardar cambios'));}
					$control=$this->M_material->get_row('idmg',$grupo->idmg);
					if($privilegio->al15d==1 && empty($control)){$eli=json_encode(array('function'=>'confirm_grupo','atribs'=>json_encode(array('g'=>$grupo->idmg)),'title'=>'Eliminar grupo'));}
				?>
				<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	<?php if($privilegio->al15c==1){ ?>
		<thead>
			<tr><th colspan="4" class="text-center">NUEVO</th></tr>
			<tr>
				<td colspan="2"><form class="save_grupo">
				<div class="input-group config">
					<input type="text" id="nom_g" class="form-control form-control-sm" placeholder='Nombre de Grupo' maxlength="50">
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help7;?>><i class='fa fa-info-circle'></i></span>
				</div></form>
				</td>
				<td>
					<form class="save_grupo">
						<input type="text" id="abr_g" class="form-control form-control-sm" placeholder='Abreviatura de Grupo' maxlength="10">
					</form>
				</td>
				<td>
					<div class="input-group config">
						<textarea id="des_g" cols="30" class="form-control form-control-sm" placeholder='Descripcion del grupo' maxlength="100"></textarea>
						<span class="input-group-addon form-control-sm" <?php echo $popover2.$help8;?>><i class='fa fa-info-circle'></i></span>
					</div>
				</td>
				<td>
					<?php $save="";$det="";$eli="";
						if($privilegio->al15c==1){$save=json_encode(array('function'=>'save_grupo','title'=>'Guardar nuevo'));}
					?>
					<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,"save"=>$save,"delete"=>$eli]);?>
				</td>
			</tr>
		</thead>
	<?php } ?>
	</table>
	</div>
</div>
</div>
<?php 
	if($privilegio->al15r=="1" && $privilegio->al15a=="1"){
		if($privilegio->al15a=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_help"=>$help]);
	}
?>
<script>$(".detalle_unidad").detalle_unidad();$(".update_unidad").update_unidad();$(".confirm_unidad").confirm_unidad();$(".save_unidad").save_unidad();$(".detalle_color").detalle_color();$(".update_color").update_color();$(".confirm_color").confirm_color();$(".save_color").save_color();$(".detalle_grupo").detalle_grupo();$(".update_grupo").update_grupo();$(".confirm_grupo").confirm_grupo();$(".save_grupo").save_grupo();$('[data-toggle="popover"]').popover({html:true});</script>