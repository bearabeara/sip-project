<?php 
	$j_materiales=json_encode($materiales);
	$url=base_url().'libraries/img/';
?>
<table class="table table-bordered table-hover" id="tbl-container">
	<thead>
    <tr>
    	<th class='img-thumbnail-60'><div class="img-thumbnail-60">#Item</div></th>  
	    <th class='hidden-sm celda-sm-10'>Código</th>  
		<th class='celda-sm-30'>Nombre</th>
		<th class='hidden-sm' width='9%'>Color</th>
		<th class='hidden-sm' width='11%'>Grupo</th>
		<th width='9%'>Cantidad</th>
		<th class='hidden-sm' width='31%'>Descripcion</th>
		<td width='0%'></td>
    </tr>
    </thead>
    <tbody>
    <?php 
    if(count($materiales)>0){
		$cont=0;
	    for($i=0;$i<count($materiales);$i++){ $material=$materiales[$i];
		 	if($atrib!="" && $val!="" && $type_search!=""){
		 		$control=$this->lib->search_where($material,$atrib,$val,$type_search);
		 	}
	    	$img="sistema/miniatura/default.jpg";
	    	if($material->fotografia!=NULL && $material->fotografia!=""){
	    		$img="materiales/miniatura/".$material->fotografia;
			}
    ?>
    <tr<?php if($atrib!="" && $val!="" && $type_search!=""){if($control==null){?> style="display: none"<?php }else{$cont++;}}else{$cont++;}?> data-m="<?php echo $material->idm;?>">
    	<td>
			<div class="item"><?php echo $cont;?></div>
			<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-60" data-title="<?php echo $material->nombre;?>" data-desc="<?php if(strlen($material->descripcion)>0){ echo $material->descripcion;}else{ echo '<br>';}?>" width='100%'>
    	</td>  
	    <td class="hidden-sm" data-col='1'>
	    	<?php if($atrib=="codigo" && $val!="" && $control!=null){echo $this->lib->str_replace_all($material->codigo,$this->lib->all_minuscula($val),"mark");}else{echo $material->codigo;}?>
	    </td>  
		<td><span data-col='2'><?php if($atrib=="nombre" && $val!="" && $control!=null){echo $this->lib->str_replace_all($material->nombre,$this->lib->all_minuscula($val),"mark");}else{echo $material->nombre;}?></span>
			<br><strong class="visible-md" style="font-size: .8rem;color:<?php echo $material->codigo_c;?>">(<?php echo $material->nombre_c; ?>)</strong>
		</td>
		<td class='hidden-sm'>
			<ul class="list-group" style="font-size:10px; padding: 1px">
				<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->nombre_c; ?></li>
			</ul>
			<span data-col='4' data-value="<?php echo $material->idco;?>"></span>
		</td>
		<td class='hidden-sm' data-col='5' data-value="<?php echo $material->idmg;?>"><?php echo $material->nombre_g; ?></td>
		<td style="text-align: right;"><span class="label label-inverse-<?php if($material->cantidad>=90){ echo 'success';}else{ if($material->cantidad>=10 && $material->cantidad<=89){ echo 'warning';}else{echo 'danger';}}?> label-md" data-col='6'><?php echo $material->cantidad."  ".$material->abr_u."."; ?></span></td>
		<td class='hidden-sm'><?php echo $material->descripcion;?></td>
		<td>
		<?php 
			$det=json_encode(array('function'=>'reportes_material','atribs'=> json_encode(array('am' => $material->idam)),'title'=>'Detalle'));
			$conf=""; if($privilegio->al11u=="1"){$conf=json_encode(array('function'=>'config_material','atribs'=> json_encode(array('am' => $material->idam,'m' => $material->idm)),'title'=>'Configurar'));}
			$del=""; if($privilegio->al11d=="1"){$del=json_encode(array('function'=>'confirm_material','atribs'=> json_encode(array('am' => $material->idam)),'title'=>'Eliminar'));}
		?>
		<?php $this->load->view("estructura/botones/btn_registro",["details"=>$det,'config'=>$conf,'delete'=>$del]);?>
		</td>
    </tr>
	<?php }
		}else{
		echo "<tr><td colspan='8'><h2>0 registros encontrados...</h2></td></tr>";
	} ?>
</tbody></table>
<?php $report="";$new="";$help="";
	if($privilegio->al11r=="1" && ($privilegio->al11p=="1" || $privilegio->al11c=="1" || $privilegio->al11a=="1")){
		if($privilegio->al11p=="1"){$report=json_encode(array('function'=>'print_material','title'=>'Reportes','atribs'=> json_encode(array('tbl' => "table#tbl-container"))));}
		if($privilegio->al11c=="1"){$new=json_encode(array('function'=>'new_material','title'=>'Nuevo almacen'));}
		if($privilegio->al11a=="1"){$help=json_encode(array('function'=>'manual','title'=>'Manual de uso','atribs'=> json_encode(array('module' => "almacen"))));}
		$this->load->view("estructura/botones/fab_button",["fab_report"=>$report,"fab_new"=>$new,"fab_help"=>$help]);
	}
?>
<script>$("img.img-thumbnail").visor();$("button.config_material").config_material();$("button.reportes_material").reportes_material();$("button.confirm_material").confirm_material();if(!esmovil()){$('[data-toggle="tooltip"]').tooltip({});}</script>