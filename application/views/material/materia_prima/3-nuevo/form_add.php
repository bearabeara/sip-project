<?php
$help1='title="Almacen" data-content="Seleccione el almacen donde desea buscar el material, si no selecciona ningun almacen se mostrara por defectos todos los materiales de todos los almacenes"';
$popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link new_material">Nuevo material</a></li>
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link active add_material">Adicionar material</a></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-sm-7 offset-sm-5 col-xs-12">
				<div class="input-group col-xs-12 input-sm">
					<select class="form-control form-control-sm" id="alm">
						<option value="">Seleccione...</option>
						<?php for ($i=0; $i < count($almacenes) ; $i++) { $almacen=$almacenes[$i]; ?>
						<option value="<?php echo $almacen->ida;?>"><?php echo $almacen->nombre;?></option>
						<?php } ?>
					</select>
					<span class="input-group-addon form-control-sm">Almacen</span>
					<span class="input-group-addon form-control-sm" <?php echo $popover.$help1;?>><i class='fa fa-info-circle'></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="list-group-item" style="max-width:100%">
		<div class="row">
			<div class="col-xs-12" id="contenido_2"></div>
		</div>
	</div>
</div>
<script>Onfocus("alm");$('[data-toggle="popover"]').popover({html:true});$("a.new_material").new_material();$("a.add_material").add_material();
$("select#alm").search_add_material();</script>