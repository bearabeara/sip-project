<?php 
	$url=base_url().'libraries/img/';
	$help1='title="<h4>Imposible quitar el material<h4>" data-content="Verifique que la cantidad de material en el almacen sea cero, y vuelva a intentarlo"';
	$popover='data-toggle="popover" data-placement="left" data-trigger="hover" ';
	$rand=rand(10,999999);
?>
<div class="table-responsive g-table-responsive">
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th class="img-thumbnail-45"><div class="img-thumbnail-45">#Item</div></th>
				<th class="celda-sm-10 hidden-sm">Codigo</th>
				<th class="celda-sm-30">Nombre</th>
				<th style="width:30%">Almacen</th>
				<th style="width:14%" class="hidden-sm">Color</th>
			<?php if($privilegio->al==1 && $privilegio->al11r==1 && $privilegio->al11c==1 && $privilegio->al11u==1){ ?>
				<th style="width:6%"></th>
			<?php } ?>
			</tr>
		</thead>
		<tbody>
		<?php for ($i=0; $i < count($materiales); $i++) { $material=$materiales[$i]; 
		    	$img="sistema/miniatura/default.jpg";
		    	if($material->fotografia!="" && $material->fotografia!=NULL){
					$img="materiales/miniatura/".$material->fotografia;
				}
				$almacen=$this->M_almacen->get($material->ida);
			?>
			<tr>
				<td><div id="item"><?php echo $i+1;?></div>
				<img src="<?php echo $url.$img;?>" class='img-thumbnail img-thumbnail-<?php echo $rand;?> img-thumbnail-45' width='100%' data-title="<?php echo $material->nombre;?>" data-desc="<br>">
				</td>
				<td class="hidden-sm"><?php echo $material->codigo;?></td>
				<td ><?php echo $material->nombre;?></td>
				<td class="celda-sm"><?php echo $almacen[0]->nombre;?></td>
				<td class="hidden-sm">
				<ul class="list-group" style="font-size:10px; padding: 1px">
					<li class="list-group-item" style="padding-top: 5px;padding-bottom: 5px;color:white; background:<?php echo $material->codigo_c;?>"><?php echo $material->nombre_c; ?></li>
				</ul>
				</td>
				<?php if($privilegio->al==1 && $privilegio->al11r==1 && $privilegio->al11c==1 && $privilegio->al11u==1){ ?>
				<?php $control=$this->M_almacen_material->get_row_2n('ida',$ida,'idm',$material->idm);?>
				<td>
					<button type="button" class="btn btn-<?php if(empty($control)){?>inverse-primary add_material_almacen<?php }else{?>default<?php }?> btn-mini waves-effect waves-light"<?php if(empty($control)){?> data-m="<?php echo $material->idm?>" data-a="<?php echo $ida;?>" data-success="disabled" data-class="btn btn-default btn-mini waves-effect waves-light"<?php }else{?> disabled="disabled"<?php }?>> <i class="fa fa-plus"></i> add </button>
				</td>
				<?php } ?>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
<script>$('[data-toggle="popover"]').popover({html:true});$("img.img-thumbnail-<?php echo $rand;?>").visor();$("button.add_material_almacen").add_material_almacen();</script>