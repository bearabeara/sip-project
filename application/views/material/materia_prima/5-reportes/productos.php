<?php $url=base_url().'libraries/img/'; 
	$rand=rand(10,999999);
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link reportes_material" data-am='<?php echo $material->idam;?>'>Detalle material</a></li>
	<li class="nav-item"><a href="javascript:" role="tab" class="nav-link active material_productos" data-am="<?php echo $material->idam;?>"><?php echo $material->nombre; ?> en uso</a></li>
</ul>
<div class="list-group">
	<div class="list-group-item" style="max-width:100%">
		<div class="table-responsive">
			<table border="0" class="tabla tabla-border-true">
				<tr class="fila">
					<th class="celda th img-thumbnail-50"><div class="img-thumbnail-50">#Item</div></th>
					<th class="celda th" width="15%">Código</th>
					<th class="celda th" width="65%">Nombre</th>
					<th class="celda th" width="20%">Cantidad</th>
				</tr>
		<?php if(!empty($productos_material) || !empty($productos_grupos_material) || !empty($productos_colores_material)){
				$productos=$this->lib->material_productos($productos,$material,$productos_material,$productos_grupos_material,$productos_colores_material);
				$cont=1;
		?>
		<?php for ($k=0; $k < count($productos); $k++){ ?>
			<?php foreach ($productos[$k] as $key => $producto){
				$img="sistema/miniatura/default.jpg";
				$portada="";
				if($producto->idpgrc!="" && $producto->idpgrc!=null){
					$aux=$this->M_producto_grupo_color->get($producto->idpgrc);
					if(!empty($aux)){
						$portada=$aux[0]->portada;
					}else{
						if($producto->idp!="" && $producto->idp!=null){ $aux=$this->M_producto->get($producto->idp);$portada=$aux[0]->portada; }
					}
				}else{
					if($producto->idp!="" && $producto->idp!=null){ $aux=$this->M_producto->get($producto->idp);$portada=$aux[0]->portada; }
				}
				$v=explode("|", $portada);
				if(count($v==2)){
					if($v[0]=="1"){ $control=$this->M_producto_imagen->get_row('idpi',$v[1]); if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
					if($v[0]=="3"){ $control=$this->M_producto_imagen_color->get_row('idpig',$v[1]);if(!empty($control)){ $img="productos/miniatura/".$control[0]->archivo;}}
				}
			?>
				<tr class="fila">
					<?php 
						$codigo=$producto->codigo;
						if($producto->abr_g!="" && $producto->abr_g!=null){ if($codigo!=""){ $codigo.="-";} $codigo.=$producto->abr_g; }
						if($producto->abr_c!="" && $producto->abr_c!=null){ if($codigo!=""){ $codigo.="-";} $codigo.=$producto->abr_c; }
						$nombre=$producto->nombre_p;
						if($producto->nombre_g!=""){ if($nombre!=""){ $nombre.=" - ";} $nombre.=$producto->nombre_g; }
						if($producto->nombre_c!=""){ if($nombre!=""){ $nombre.=" - ";} $nombre.=$producto->nombre_c; }
					?>
					<td class="celda td"><div id="item"><?php echo $cont++; ?></div>
						<img src="<?php echo $url.$img;?>" class="img-thumbnail img-thumbnail-50 img-thumbnail-<?php echo $rand;?>" data-title="<?php echo $nombre;?>" data-desc="<?php echo "Cantidad: ".$producto->cantidad." [".$producto->abr_u.".]";?>"></td>
					<td class="celda td"><?php echo $codigo;?></td>
					<td class="celda td"><?php echo $nombre;?></td>
					<td class="celda td text-right"><?php echo $producto->cantidad." [".$producto->abr_u.".]";?></td>
				</tr>
			<?php } ?>
		<?php } ?>
		<?php }else{ ?>
				<tr class="fila"><td class="celda td text-center" colspan="4"><h3>0 Registros encontrados</h3></td></tr>
		<?php }?>
			</table>
		</div>
	</div>
</div>
<script>$("a.material_productos").material_productos();$('a.reportes_material').reportes_material();
$("img.img-thumbnail-<?php echo $rand;?>").visor();
</script>