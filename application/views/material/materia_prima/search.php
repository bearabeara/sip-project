<table cellpadding="0" cellspacing="0">
	<tr>
		<td class='hidden-sm img-thumbnail-60'><div class="img-thumbnail-60"></div></td>
		<td class='hidden-sm celda-sm-10'>
			<form class="view_material" data-type="search">
				<input class='form-control form-control-sm search_material' type="search" id="s_cod" placeholder='Código' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="1">
			</form>
		</td>
		<td class='celda-sm-30'>
			<form class="view_material" data-type="search">
				<div class="input-group input-group-sm">
					<input type="search" id="s_nom" class="form-control form-control-sm search_material" maxlength="100" placeholder='Buscar nombre...' data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="2"/>
					<span class="input-group-addon form-control-sm view_material" data-type="search"><i class='icon-search2'></i></span>
				</div>
			</form>
		</td>
		<td class='hidden-sm' width="11%">
			<select class='form-control form-control-sm search_material' id="s_col" title="Seleccione color." data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="4">
				<option value="">Seleccionar...</option>
				<?php for($i=0; $i<count($color);$i++){$res=$color[$i];?>
					<option value="<?php echo $res->idco; ?>"><?php echo $res->nombre;?></option>	
				<?php } ?>
			</select>
		</td>
		<td class='hidden-sm' width="11%">
			<select class='form-control form-control-sm search_material' id="s_gru" title="Seleccione grupo" data-container="table#tbl-container" data-compare="data" data-name-data="value" data-type-compare="equals" data-col="5">
				<option value="">Seleccione...</option>
				<?php for($i=0; $i<count($grupo);$i++){$res=$grupo[$i];?>
					<option value="<?php echo $res->idmg; ?>"><?php echo $res->nombre;?></option>
				<?php } ?>
			</select>
		</td>
		<td class='hidden-sm' width="10%">
			<form class="view_material" data-type="search">
				<input class='form-control form-control-sm search_material' type="number" id="s_can" placeholder='Cantidad' min="0" step="any" data-container="table#tbl-container" data-compare="text" data-type-compare="like" data-col="6">
			</form>
		</td>
		<td class='hidden-sm' width="55%"></td>
		<td class="hidden-sm" width="13%">
			<?php
				$search=json_encode(array('function'=>'view_material','title'=>'Buscar','atribs'=> json_encode(array('type' => "search"))));
				$all=json_encode(array('function'=>'view_material','title'=>'Ver todos','atribs'=> json_encode(array('type' => "all"))));
			?>
			<?php $this->load->view("estructura/botones/btn_registro",["search"=>$search,"view_all"=>$all]);?>
		</td>
	</tr>		
</table>
<script>Onfocus('s_cod');$(".search_material").search_material();$(".view_material").view_material();</script>