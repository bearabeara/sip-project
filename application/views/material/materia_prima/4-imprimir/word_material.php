<?php $url=base_url().'libraries/img/';
$fecha=date("Y-m-d");
header("Content-type: application/vnd.ms-word; name='word'");
header('Pragma: public');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: pre-check=0, post-check=0, max-age=0');
header('Pragma: no-cache');
header('Expires: 0');
header('Content-Transfer-Encoding: none');
header('Content-type: application/vnd.ms-word;charset=utf-8');
header('Content-type: application/x-msword; charset=utf-8');
header("Content-Disposition: attachment; filename=Material-".$almacen->nombre."-$fecha.doc");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
if(!empty($almacen) && !empty($materiales)){
	$sub_total=0;
	$total=0;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<center><h3>REPORTE DE MATERIALES</h3><?php echo "<strong>Almacen: </strong>".$almacen->nombre;?></center>
<table class="font-10" border="1" cellspacing="0" cellpadding="5" width="100%">
	<thead>
		<tr>
			<th width="3%">#</th>
			<th width="6%">Fot.</th>
			<th width="8%">Código</th>
			<th width="25%">Nombre</th>
			<th width="10%">Grupo</th>
			<th width="10%">Color</th>
			<th width="7%">Cantidad</th>
			<th width="7%">C/U</th>
			<th width="7%">Total</th>
			<th width="17%">Observaciónes</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0; $i<count($materiales); $i++){ $material=$materiales[$i];
		$img='sistema/miniatura/default.jpg';
		if($material->fotografia!=NULL && $material->fotografia!=""){ $img="materiales/miniatura/".$material->fotografia;}
?>
	<tr>
		<td><?php echo $i+1;?></td>
		<td><img src="<?php echo $url.$img;?>" width="40px"></td>
		<td><?php echo $material->codigo;?></td>
		<td><?php echo $material->nombre;?></td>
		<td><?php echo $material->nombre_g;?></td>
		<td><?php echo $material->nombre_c;?></td>
		<td><?php if($material->cantidad>0){ echo $material->cantidad." ".$material->abr_u; } ?></td>
		<td><?php if($material->costo_unitario>0){ echo $material->costo_unitario; } ?></td>
		<td><?php if(($material->cantidad*$material->costo_unitario)>0){ echo ($material->cantidad*$material->costo_unitario); $sub_total+=($material->cantidad*$material->costo_unitario); $total+=($material->cantidad*$material->costo_unitario);} ?></td>
		<td><?php echo $material->descripcion;?></td>
	</tr>
<?php } ?>
	</tbody>
</table>
<?php }?>

