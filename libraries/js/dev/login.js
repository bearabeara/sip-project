var x=$(document);
(function($){
	$.fn.ifrgvdi=function(){/*formulario de ingreso*/
	    $(this).submit(function(event){
	    	if($(this).data("type")=="idrt"){/*nombre de usuario*/
	    		validate_user($(this));
	    	}
	    	if($(this).data("type")=="osddeptf"){/*contraseña de usuario*/
	    		validte_password($(this));
	    	}
	    	event.preventDefault();
	    });
	    function validate_user(e){
	    	var username=$("input#username").val();
	    	if($(this).usuario_password(username,2,15)){
	    		var atrib=new FormData();
 				atrib.append('username',username);
 				var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 				var controls2=JSON.stringify({destino:"div.form",refresh:true,preload:true,type:"html"});
 				e.set_1n('validate_user',atrib,controls1,'user_session',{},controls2);
	    	}else{
	    		$(".input-alert").html("Ingrese un nombre de usuario válido");
	    	}
	    }
	    function validate_password(e){
	    	var password=$("input#password").val();
	    	var username=$("input#username").val();
	    	if($(this).usuario_password(password,4,25)){
	    		var atrib=new FormData();
 				atrib.append('password',password);
 				atrib.append('username',username);
 				var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
 				var controls2=JSON.stringify({destino:"div.form",refresh:true,type:"html"});
 				e.set_1n('validate_password',atrib,controls1,'inicio_sistema',{},controls2);
	    	}else{
	    		$(".input-alert").html("Ingrese un contraseña válida");
	    	}
	    }
	}
	$.fn.ersdfff=function(){/*Boton  volver*/
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				closed_user($(this));
 			}
 		});
 		function closed_user(e){
			var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
	 		var controls2=JSON.stringify({destino:"div.form",refresh:true,type:"html"});
	 		e.set_1n('closed_user',{},controls1,'user_session',{},controls2);
 		}
	}
})(jQuery);
(function($){
    $.fn.get_1n=function(ruta,atrib,controls){
        if(ruta!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
            url:ruta,
            async:true,
            type: "POST",
            contentType:false,
            data:atrib,
            processData:false, /*Debe estar en false para que JQuery no procese los datos a enviar*/
            timeout:300000,/*5min*/
            beforeSend: function(){e.enviar(controls);},
            success: function(result){e.recibir(result,controls);},
            error:function(){e.error_envio(controls);}
          }); 
        }
    }
    $.fn.set_1n=function(ruta,atrib,controls,ruta1,atrib1,controls1){
        if(ruta!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib,
              processData:false, 
              timeout:300000,
              beforeSend: function(){ e.enviar(controls);if(ruta1="user_session"){$("div.progress").removeAttr("style");}},
              success: function(result){ e.recibir_set(result,controls,ruta1,atrib1,controls1);},
              error: function(){ e.error_envio(controls1);}
          });
        }else{
          $(this).get_1n(ruta2,atrib2,controls2);
        }
    }
    $.fn.enviar=function(controls){
        var js=JSON.parse(controls);
        if(js.type!=undefined){
          if(js.type=="set"){
            if(js.preload!=undefined){
              if(js.preload){
                $("div.progress").removeAttr("style");
              }
            }
          }else{
               if(js.preload!=undefined){
              if(js.preload){
                $("div.progress").removeAttr("style");
              }
            }
          }
        }
      }
    $.fn.recibir=function(resultado,controls){
    	if($(this).isJSON(controls)){
    		var js=JSON.parse(controls);
    		if($(this).isset(js.destino) && $(this).isset(js.type)){
    			switch(js.type){
    				case "html": $(js.destino).html(resultado); break;
    			}
          if(js.destino="div.form"){$("div.progress").attr("style","display:none;");}
    		}
    	}
       
    }
    $.fn.recibir_set=function(resultado,control_this,ruta1,atrib1,controls1){
    	
        switch(resultado){
          case "ok": $(this).get_1n(ruta1,atrib1,controls1);break;
          case "user_invalid": $(".input-alert").html("Ingrese un nombre de usuario válido");$("div.progress").attr("style","display:none;"); break;
          case "user_no_exist": $(".input-alert").html("El usuario no está registrado");$("div.progress").attr("style","display:none;"); break;
          case "password_invalido": $(".input-alert").html("Contraseña inválida");$("div.progress").attr("style","display:none;"); break;
          case "permiso_denegado": alert("Permiso denegado");break;
          case "login": if($(".base").data("base")!=undefined){window.location.href=$(".base").data("base");}break;
          default:  break;
        }
      }
    $.fn.error_envio=function(controls){

	}
    $.fn.isset=function(val){
	  if(typeof val!='undefined'){
	    return true;
	  }else{
	    return false;
	  }
	}
	$.fn.isJSON=function(str){
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}
	$.fn.usuario_password=function(cad,min,max){
	  if(cad!=undefined){
	    var val="^[a-zA-Z0-9)+-:(]{"+min+","+max+"}$";
	    var expr=new RegExp(val);
	    if(cad.match(expr)){
	      return true;
	    }else{
	      return false;
	    }
	  }else{
	    return false
	  }
	}
})(jQuery);
x.ready(function(){
	var atrib=new FormData();if($("div.form").data("alert")!=undefined){atrib.append('alert',$("div.form").data("alert"));}
	$(this).get_1n('user_session',atrib,JSON.stringify({destino:"div.form",refresh:true,preload:true,type:"html"}));
});
