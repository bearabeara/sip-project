(function( $ ) {
     $.fn.search_producto = function() {
      if($("#nom").val()!=undefined){
        if(strSpace($("#nom").val(),0,90)){
          var atrib=new FormData();
          atrib.append('nom',$("#nom").val());
          get('productos/view',atrib,'container',true);
        }
      }   
    };
   $.fn.visor=function(){
    $(this).click(function(){
      if($(this).prop("tagName")!="FORM"){
        visor_img($(this));
      }
    });
    function visor_img(e){
      $("#dialog_visor").html("");
        var type_view="";
        if(e.data("title")!=undefined && e.data("desc")!=undefined){
          type_view="this";
        }else{
          if(e.data("type")!=undefined && e.data("id")!=undefined){
            type_view="db";
          }
        }
        if(type_view!=""){
          visor();
          if(type_view=="this"){
            if(e.attr("src")!=undefined){
              var url=e.attr("src");
              url=url.replace("/miniatura","");
              var titulo=e.data("title");
              var desc=e.data("desc");
              var content='<div class="carousel g-carousel carousel-slider center initialized" data-indicators="true" style="height: 680.4px;"><div class="carousel-item active" style="z-index: 0; opacity: 1; display: block; transform: translateX(0px) translateX(0px) translateX(0px) translateZ(0px);"><figure><figcaption><b>'+titulo+'</b><br></figcaption><img width="100%" src="'+url+'"></figure></div><ul class="indicators"><li class="indicator-item active"></li></ul></div>';
              $("#dialog_visor").html(content);
            }
          }
          if(type_view=="db"){
              /*var atrib=new FormData();
              atrib.append("id",e.data("id"));
              atrib.append("type",e.data("type"));
              var controls=JSON.stringify({id:"content_6",refresh:true,type:"html"});
              e.get_1n('controls/view_img',atrib,controls);*/
          }
        }
    }
  }
}( jQuery ));
function get(ruta,atrib,id,control){//con mensaje de inicio
  $.ajax({
  	url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){llegada(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
    $.ajax({
        url:ruta1,
        async:true,
        type: "POST",
        contentType:false,
        data:atrib1,
        processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
        timeout:9000,
        beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
        success: function(result){llegada_2n(id1,result,ruta2,atrib2,id2,control2)},
        error: function(){problemas(id1)}
    });
  return false;
}
function inicioEnvio(div){
  if(div=="container"){
      $("#preloader").css({display:"block"});
  }else{
    if(div=="search"){
      $("#search").html("<div id='preload-2'><center><i class='fa fa-circle-o-notch'></i></center></div>");
    }else{
      if(div=="content_1"){
        $("#preload-modal").css("display","inline");
      }else{
        $("#"+div).html("<img src='"+base+"libraries/img/sistema/load.gif'>"); 
      }
    }
  }
}
function llegada(div,result){ 
  if(result!="logout"){
    $("#"+div).html(result);
    if(div=="container"){
      $("#preloader").css("display","none");
    }
  }else{
    alert("Su Session a expirado, inicie session por favor"); 
    window.location.href=base;
  }
}
function llegada_2n(div,result,ruta,atrib,id,control){
  if(result!="logout"){ 
    $("#"+div).html(result+"");
    if(div=="search"){
        $("#form_search").submit(function(event) {
        $(this).search_producto();
        event.preventDefault();
      });
    }
    if(div=="container"){
      $("#preloader").css("display","none");
    }
    get(ruta,atrib,id,control);
  }else{
    alert("Su Session a expirado, inicie session por favor"); 
    window.location.href=base;
  }
}
function problemas(div){ 
  //msj("problem");  
  console.log("problem");  
  $("#preload-modal").css("display","none");
}
function strSpace(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(°;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}

function strNoSpace(cad,min,max){
  var val="^[a-zA-Z0-9áÁéÉíÍóÓúÚñÑ)+-.,:(°;ªº]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function detalle(e){
  var atrib=new FormData();atrib.append('pgc',e.data("pgc"));
  get('../../productos/view_detalle',atrib,'container',true);
}
/*modal*/

$("#modal_closed_visor").click(function() { hide_modal("visor");});
$(".closed_visor").click(function() { hide_modal("visor");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_visor" || $(e.target).attr("class")=="carousel-item active"){ hide_modal("visor");} });
function hide_modal(nivel){//cerrar modal bootstrap
  switch(nivel){
    case "1": $("#modal_"+nivel).modal('hide'); break;
    default: 
      $("#modal_"+nivel).fadeOut(250);$("#dialog_"+nivel).fadeOut(150); $("#dialog_"+nivel).css("transform","translateY(-200%)");
      $('body').removeAttr("style");
    break;
  }
}
function visor(){
    $("#dialog_visor").html("");
    $("#modal_visor").fadeIn(150);
    $("#dialog_visor").fadeIn(0);
    $("#dialog_visor").css({"transform":"translateY(0)","display":"block"});
    $('body').css("overflow","hidden");
}
function view_visor(e){
  visor();
  var atrib=new FormData();atrib.append('idp',e.data("p"));atrib.append('id',e.data("id"));atrib.append('type',e.data("type"));atrib.append('position',e.data("position"));atrib.append('pgc',e.data("pgc"));
  if(e.data("type")=="producto" || e.data("type")=="pieza"){ atrib.append('type-img',e.data("type-img")); }
  get('../../productos/view_visor',atrib,'dialog_visor',false);
}