 var x=$(document);
 x.ready(inicio);
 (function($){
 	$.fn.reset_input=function(){
	 	var id=$(this).attr("id");
	 	if(id!="sp_num"){ $("#sp_num").val("");}
	 	if(id!="sp_nom"){ $("#sp_nom").val("");}
		if(id!="sp_cli"){ $("#sp_cli").val("");}
		if(id!="sp_est"){ $("#sp_est").val("");}
		//caso produccion
		if(id!="s1_pro"){ if(id!="s1_mes"){$("#s1_pro").val("");}}
 		if(id!="s1_fe1"){ if(id=="s1_pro" || id=="s1_mes"){$("#s1_fe1").val("");}}
 		if(id!="s1_fe2"){ if(id=="s1_pro" || id=="s1_mes"){$("#s1_fe2").val("");}}
		if(id!="s1_mes"){ if(id!="s1_pro"){$("#s1_mes").val("");}}
	}
	$.fn.search_movimiento=function(){
	 	$(this).keyup(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			search_movimiento($(this));
	 		}
	 	});
	 	$(this).change(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			search_movimiento($(this));
	 		}
		 });
		 $(this).on("mousewheel",function(e){
			if($(this).prop("tagName")!="FORM"){
				e.preventDefault();
			}
		});
	 	function search_movimiento(e){
			 e.reset_input();
			 e.search_elements();
	 	}
	}
/*------- MANEJO DE PEDIDOS -------*/
	/*--- Buscador ---*/
	$.fn.view_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_pedido($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_pedido($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_pedido($(this));
	    	event.preventDefault();
	    });
 		function view_pedido(e){
		 	var atrib3=new FormData();
		 	atrib3.append('num',$("#sp_num").val());
		 	atrib3.append('nom',$("#sp_nom").val());
			atrib3.append('cli',$("#sp_cli").val());
			atrib3.append('est',$("#sp_est").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).get_1n('movimiento/view_pedido',atrib3,controls);
 		}
	}
	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
   	$.fn.reporte_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				reporte_pedido($(this));
 			}
 		});
 		function reporte_pedido(e){
 			if(e.data("pe")!=undefined){
		   		modal("","lg","11");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
		   		var atrib=new FormData();
		   		atrib.append('pe',e.data("pe"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
		 		$(this).get_1n('movimiento/reporte_pedido',atrib,controls);
 			}
 		}
   	}
   	$.fn.taller_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				taller_pedido($(this));
 			}
 		});
 		function taller_pedido(e){
 			if(e.data("pe")!=undefined){
 				modal("","xlg","11");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
	   			var atrib=new FormData();
	   			atrib.append('pe',e.data("pe"));
	   			if(e.data("pp")!=undefined){
	   				atrib.append('pp',e.data("pp"));
	   			}
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			 	e.get_1n('movimiento/taller_pedido',atrib,controls);
 			}
 		}
   	}
   	$.fn.informe_economico_pedido_detallado=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				informe_economico_pedido_detallado($(this));
 			}
 		});
 		function informe_economico_pedido_detallado(e){
 			if(e.data("pe")!=undefined){
		   		modal("","lg","11");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
		   		var atrib=new FormData();atrib.append('pe',e.data("pe"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('movimiento/informe_economico_pedido_detallado',atrib,controls);
 			}
 		}
   	}
   	$.fn.informe_economico_pedido_general=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				informe_economico_pedido_general($(this));
 			}
 		});
 		function informe_economico_pedido_general(e){
 			if(e.data("pe")!=undefined){
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
		   		var atrib=new FormData();atrib.append('pe',e.data("pe"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('movimiento/informe_economico_pedido_general',atrib,controls);
 			}
 		}
   	}
   	$.fn.informe_depositos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				informe_depositos($(this));
 			}
 		});
 		function informe_depositos(e){
 			if(e.data("pe")!=undefined){
				modal("","xlg","11");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
		   		var atrib=new FormData();atrib.append('pe',e.data("pe"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('movimiento/informe_depositos',atrib,controls);
 			}
 		}
   	}
   	$.fn.informe_materiales=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				informe_materiales($(this));
 			}
 		});
 		function informe_materiales(e){
 			if(e.data("pe")!=undefined){
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'11');
		   		var atrib=new FormData();
		   		atrib.append('pe',e.data("pe"));
		   		if(e.data("pp")!=undefined){
		   			atrib.append('pp',e.data("pp"));
		   		}
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				e.get_1n('movimiento/informe_materiales',atrib,controls);
 			}
 		}
   	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				config_pedido($(this));
 			}
 		});
 		function config_pedido(e){
 			if(e.data("pe")!=undefined){
		   		modal("","xlg","11");
		   		var atr="this|"+JSON.stringify({pe:e.data("pe")});
		   		btn_modal('update_pedido',atr,'',"","11");
		   		var atrib=new FormData();atrib.append('pe',e.data("pe"));
				var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				$(this).get_1n('movimiento/config_pedido',atrib,controls);
 			}
 		}
   	}
   	$.fn.update_pedido=function(){
   		if($(this).data("pe")!=undefined && $("#p_nro").val() * 1 > 0){
			var nro=$("#p_nro").val();
			var nom=$("#p_nom").val();
	   		var obs=$("#p_obs").val();
	   		var des=$("#p_des").val();
	   		var est=$("#p_est").val();
	   		var est_pag=$("#p_est_pag").val();
	   		if(strSpace(nom,3,150)){
	   			if(entero(est,0,1) && est>=0 && est<=3){
	   				if(entero(est_pag,0,1) && est_pag>=0 && est_pag<=1){
	   					var control=true;
			   			if(des!=""){ if(!decimal(des,6,1) || des<0 || des>999999.9){ control=false; alerta("Ingrese un valor válido","top","p_des");}}
			   			if(obs!=""){ if(!textarea(obs,0,300)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
			   			if(control){
			   				var atrib=new FormData();
			   				atrib.append("pe",$(this).data("pe"));
							atrib.append("nro", nro);
			   				atrib.append("nom",nom);
			   				atrib.append("obs",obs);
			   				atrib.append("des",des);
			   				atrib.append("est",est);
			   				atrib.append("est_pag",est_pag);
			   				var atrib3=new FormData();
						 	atrib3.append('num',$("#sp_num").val());
						 	atrib3.append('nom',$("#sp_nom").val());
							 atrib3.append('cli',$("#sp_cli").val());
							 atrib3.append('est',$("#sp_est").val());
				 			var controls1=JSON.stringify({type:"set",preload:true});
				 			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
				 			$(this).set('movimiento/update_pedido',atrib,controls1,'movimiento/view_pedido',atrib3,controls2);
			   			}
	   				}else{
	   					alerta("Seleccione un estado de válido","top","p_est_pag");
	   				}
	   			}else{
	   				alerta("Seleccione un estado de produccion","top","p_est");
	   			}
	   		}else{
	   			alerta("Ingrese una nombre de pedido válido","top","p_nom");
	   		}
   		}else{
   			console.log("¡Error de variable...!");
   		}
   	}
   		/*--- Configuración de parte de pedido ---*/
	   	$.fn.config_parte_pedido=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				config_parte_pedido($(this));
	 			}
	 		});
	 		function config_parte_pedido(e){
	 			if(e.data("pp")!=undefined){
			   		var atr="this|"+JSON.stringify({pp:e.data("pp")});
			   		btn_modal('update_parte_pedido',atr,'',"",'11');
			   		var atrib=new FormData();atrib.append('pp',e.data("pp"));
					var controls=JSON.stringify({id:"content_11",refresh:true,type:"html"});
					e.get_1n('movimiento/config_parte_pedido',atrib,controls);
	 			}
	 		}
	   	}
	   	$.fn.confirmar_parte_pedido=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				confirmar_parte_pedido($(this));
	 			}
	 		});  
	 		function confirmar_parte_pedido(e){
	 			if(e.data("pp")!=undefined && e.data("pe")!=undefined){
			   		modal("Eliminar","xs","5");
			   		var atr="this|"+JSON.stringify({pp:e.data("pp"),pe:e.data("pe")});
			   		btn_modal('drop_parte_pedido',atr,'',"","5");
			   		var atrib=new FormData();atrib.append('pp',e.data("pp"));
					var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					e.get_1n('movimiento/confirmar_parte_pedido',atrib,controls);
	 			}
	 		}
	   	}
	   	$.fn.drop_parte_pedido=function(){
	   		if($(this).data("pp")!=undefined && $(this).data("pe")!=undefined){
	   			var atrib=new FormData();
	   			atrib.append('pp',$(this).data("pp"));
	   			atrib.append('pe',$(this).data("pe"));
	   			var atrib3=new FormData();
	   			atrib3.append('num',$("#sp_num").val());
	   			atrib3.append('nom',$("#sp_nom").val());
				atrib3.append('cli',$("#sp_cli").val());
				atrib3.append('est',$("#sp_est").val());
	 			var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
	 			var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
	 			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	   			$(this).set_2n("movimiento/drop_parte_pedido",atrib,controls,"movimiento/config_pedido",atrib,controls1,"movimiento/view_pedido",atrib3,controls2);
	   		}else{
	   			msj("fail");
	   		}
	   	}
	 	$.fn.refresh_producto_pedido=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				refresh_producto_pedido($(this));
	 			}
	 		});
	 		function refresh_producto_pedido(e){
	 			if(e.data("elemento")!=undefined && e.data("type-refresh")!=undefined && e.data("type")!=undefined && e.data("p")!=undefined && e.data("pp")!=undefined && e.data("tbl")!=undefined){
			 		var atrib=new FormData();
			 		if(e.data("type")!=undefined){
			 			atrib.append('type-refresh',e.data("type-refresh"));atrib.append("tbl",e.data("tbl"));atrib.append("elemento",e.data("elemento"));
			 			if(e.data("type-refresh")=="db"){
			 				atrib.append('type',e.data("type"));
			 				atrib.append("p",e.data("p"));
			 				if($("#datos_parte_pedido").data("pp")!=undefined){ atrib.append('pp',$("#datos_parte_pedido").data("pp"));}
			 				var controls1=JSON.stringify({id:e.data("tbl")+"",refresh:true,type:"html",callback:JSON.stringify({c1:"total_costo_venta",c2:"refresh_stack_pedido_productos",c3:"costo_venta_tbl"})});
			 				e.get_1n('movimiento/refresh_producto_pedido',atrib,controls1);
			 			}
			 		}
	 			}else{
	 				console.log("Error de variables...");
	 			}
	 		}
	 	}
	 	$.fn.refresh_producto_detalle_pedido=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				refresh_producto_detalle_pedido($(this));
	 			}
	 		});
	 		function refresh_producto_detalle_pedido(e){
	 			if(e.data("container-tr")!=undefined && e.data("dp")!=undefined && e.data("p")!=undefined && e.data("item")!=undefined && e.data("tbl")!=undefined && e.data("container")!=undefined && e.data("container-2")!=undefined){
			 		var atrib=new FormData();
			 		atrib.append("dp",e.data("dp"));
			 		atrib.append("p",e.data("p"));
			 		atrib.append("tbl",e.data("tbl"));
			 		atrib.append("item",e.data("item"));
			 		atrib.append("container",e.data("container"));
			 		atrib.append("container-2",e.data("container-2"));
			 		var controls1=JSON.stringify({id:e.data("container-tr"),refresh:false,type:"html",callback:JSON.stringify({c1:"total_costo_venta",c2:"costo_venta_tbl"})});
			 		e.get_1n('movimiento/refresh_producto_detalle_pedido',atrib,controls1);
	 			}else{
	 				console.log("Error de variables...");
	 			}
	 		}
	 	}
		$.fn.update_producto_colores=function(){
	      	$(this).click(function(){
	      		if($(this).prop("tagName")!="FORM"){
	      			update_producto_colores($(this));
	      		}
	      	});
	      	function update_producto_colores(e){
				if(e.data("tbl")!=undefined && e.data("p")!=undefined && e.data("pp")!=undefined && e.data("container")!=undefined && e.data("container-2")!=undefined){
					if($("table#"+e.data("tbl"))!=undefined){
						var table=$("table#"+e.data("tbl"));
						if($(table).data("accordion")!=undefined){
							var accordion=$($(table).data("accordion"));
							if($("#"+$(accordion).attr("id")+" div.item")!=undefined){
								var pos=$("#"+$(accordion).attr("id")+" div.item").html();
								e.attr("disabled","disabled");
								var detalle=[];
								var status=$("table#"+e.data("tbl")+" "+e.data("container-2")).html();
								var productos=JSON.parse(e.lista_productos("config_tabla_parte_pedido"));
						   		if(productos['control']){
									var atrib=new FormData();
						   			atrib.append("detalles",productos['productos']);
				 					atrib.append('pp',e.data("pp"));
				 					atrib.append('pos',pos);
				 					atrib.append("status",status);
									var atrib3=new FormData();
									atrib3.append('type',"update");
				 					atrib3.append("p",e.data("p"));
				 					atrib3.append('pp',$("#datos_parte_pedido").data("pp"));
									atrib3.append('type-refresh',"db");
									atrib3.append("tbl",e.data("tbl"));
									atrib3.append("elemento",e.data("elemento"));
				 					var controls1=JSON.stringify({type:"set",preload:true});
				 					var controls3=JSON.stringify({id:e.data("tbl")+"",refresh:false,type:"html",callback:JSON.stringify({c1:"total_costo_venta",c2:"refresh_stack_pedido_productos"})});
			 						e.set('movimiento/update_detalles_pedido',atrib,controls1,'movimiento/refresh_producto_pedido',atrib3,controls3);
				 				}else{
				 					e.removeAttr("disabled");
				 					msj("¡Error!, revice los valores por favor.");
				 				}
							}
						}
					}
				}
	      	}
		}
	   	$.fn.update_parte_pedido=function(){
	   		$(this).attr("disabled","disabled");
	   		var e=$(this);
	 		if(e.data("pp")!=undefined){
			   	var status=$("span#detalles").html();
			   	var tip=$("#p_tip").val()+"";
			   	var fec=$("#p_fec").val();
			   	var obs=$("#p_obs").val();
			   	if(status!=undefined){
			   		if(tip=="0" || tip=="1" || tip=="2"){
			   			if(fecha(fec)){
			   				var control=true;
			   				if(obs!=""){ if(!textarea(obs,0,300)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
			   						var productos=JSON.parse($(this).lista_productos("config_parte_pedido"));
			   						if(productos['control']){
			   							console.log(productos['productos']);
			   							var atrib=new FormData();
			   							atrib.append("detalles",productos['productos']);
			   							atrib.append("status",status);//pila de productos en el db
			   							atrib.append("pp",e.data("pp"));
			   							atrib.append("fec",fec);
			   							atrib.append("obs",obs);
			   							atrib.append("tipo",productos['tipo']);
			   							atrib.append("cl",productos['cl']);
			   							var atrib3=new FormData();
			   							atrib3.append('num',$("#sp_num").val());
			   							atrib3.append('nom',$("#sp_nom").val());
										atrib3.append('cli',productos['cl']);
										atrib3.append('est',$("#sp_est").val());
			 							var controls=JSON.stringify({type:"set",preload:true});
			 							var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
			   							$(e).set_2n("movimiento/update_parte_pedido",atrib,controls,"movimiento/config_parte_pedido",atrib,controls1,"movimiento/view_pedido",atrib3,controls2);
			   						}else{
			   							msj("¡Error!, Verifique los datos en los prodcutos por favor.");
			   							$(e).removeAttr("disabled");
			   						}
			   			}else{
			   				alerta("Ingrese una fecha válida","top","p_fec");
			   				$(e).removeAttr("disabled");
			   			}
			   		}else{
			   			alerta("Seleccione una opcion porfavor.","top","p_tip");
			   			$(e).removeAttr("disabled");
			   		}
			   	}else{
			   		msj("fail");
			   		$(e).removeAttr("disabled");
			   	}
	 		}else{
	 			msj("fail");
	 			$(e).removeAttr("disabled");
	 		}
	   	}
 	$.fn.lista_productos=function(module){
 		var colores=[];
	 	var control=true;
	 	var idcl="";
	 	var tipo="";
 		if(module=="new_pedido" || module=="new_parte_pedido" || module=="config_parte_pedido"){
	 		var pos=0;
	 		$("div#list_product div.accordion-content.accordion-desc table.table").each(function(id,table){
	 			if($(table).attr("id")!=undefined){
	 				pos++;
	 				$("table#"+$(table).attr("id")+" tbody tr.row-producto").each(function(id2,tr){
	 					if($(tr).attr("id")!=undefined){
	 						var sucursales=[];
	 						var control_cantidad=0;
	 						$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").each(function(id3,input){
								if(entero($(input).val(),1,9999) && $(input).val()*1>0){
	 								control_cantidad++;
	 							}
	 							if($(input).data("sdp")!=undefined){
	 								sucursales[sucursales.length]={sc:$(input).data("sc"),sdp:$(input).data("sdp"),cantidad:$(input).val(),type:$(input).data("type")};
	 							}else{
	 								sucursales[sucursales.length]={sc:$(input).data("sc"),cantidad:$(input).val(),type:$(input).data("type")};
	 							}
	 						});
	 						/*verificando validez*/
	 						 if(control_cantidad==0){
	 						 	$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").css("border","1px solid rgba(255, 0, 0, 0.38)");
	 						 	control=false;	
	 						 }else{
	 						 	$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").removeAttr("style");
	 						 }
	 						/*verificando validez*/
							if($("tr#"+$(tr).attr("id")).data("pgc")!=undefined && $("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")!=undefined && $("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val()!=undefined){
								if(textarea($("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),0,300)){
									$("tr#"+$(tr).attr("id")+" textadrea[id*=obs]").removeAttr("style");
								}else{
									$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").css("border","1px solid rgba(255, 0, 0, 0.38)");
	 								control=false;
								}
								if($(tr).data("dp")!=undefined){
									colores[colores.length]={pgc:$("tr#"+$(tr).attr("id")).data("pgc"),dp:$(tr).data("dp"),cu:$("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")*1,observacion:$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),posicion:pos+"",sucursales:sucursales};
								}else{
									colores[colores.length]={pgc:$("tr#"+$(tr).attr("id")).data("pgc"),cu:$("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")*1,observacion:$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),posicion:pos+"",sucursales:sucursales};
								}
							}
	 					}
	 				});
	 			}
	 		});
 		}
 		if(module=="config_tabla_parte_pedido"){//Actualiza solo tabla de productos
 			if($(this).data("tbl")!=undefined){
 				var table=$("table#"+$(this).data("tbl"));
 				//buscando posicion
 				if(table.data("accordion")!=undefined){pos=$(table.data("accordion")+" div.accordion-heading div.item").html()*1;}
				$("table#"+$(this).data("tbl")+" tbody tr.row-producto").each(function(id2,tr){
		 					if($(tr).attr("id")!=undefined){
		 						var sucursales=[];
		 						var control_cantidad=0;
		 						$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").each(function(id3,input){
									if(entero($(input).val(),1,9999) && $(input).val()*1>0){
		 								control_cantidad++;
		 							}
		 							if($(input).data("sdp")!=undefined){
		 								sucursales[sucursales.length]={sc:$(input).data("sc"),sdp:$(input).data("sdp"),cantidad:$(input).val(),type:$(input).data("type")};
		 							}else{
		 								sucursales[sucursales.length]={sc:$(input).data("sc"),cantidad:$(input).val(),type:$(input).data("type")};
		 							}
		 						});
		 						/*verificando validez*/
		 						 if(control_cantidad==0){
		 						 	$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").css("border","1px solid rgba(255, 0, 0, 0.38)");
		 						 	control=false;	
		 						 }else{
		 						 	$("tr#"+$(tr).attr("id")+" input[id*=cantidad]").removeAttr("style");
		 						 }
		 						/*verificando validez*/
								if($("tr#"+$(tr).attr("id")).data("pgc")!=undefined && $("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")!=undefined && $("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val()!=undefined){
									if(textarea($("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),0,300)){
										$("tr#"+$(tr).attr("id")+" textadrea[id*=obs]").removeAttr("style");
									}else{
										$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").css("border","1px solid rgba(255, 0, 0, 0.38)");
		 								control=false;
									}
									if($(tr).data("dp")!=undefined){
										colores[colores.length]={pgc:$("tr#"+$(tr).attr("id")).data("pgc"),dp:$(tr).data("dp"),cu:$("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")*1,observacion:$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),posicion:pos+"",sucursales:sucursales};
									}else{
										colores[colores.length]={pgc:$("tr#"+$(tr).attr("id")).data("pgc"),cu:$("tr#"+$(tr).attr("id")+" input[id*=cu]").data("value")*1,observacion:$("tr#"+$(tr).attr("id")+" textarea[id*=obs]").val(),posicion:pos+"",sucursales:sucursales};
									}
								}
		 					}
		 		});
 					
 				
 			}	
 		}
		if(colores.length<=0){
	 		control=false;
	 	}
	 	if(control){
	 		if($("div#list_product div[class*=accordion-panel]").length>0){
	 			$("div#list_product div[class*=accordion-panel]").each(function(index,div){
	 				if($(div).data("cl")!=undefined && $(div).data("tipo")!=undefined){
	 					idcl=$(div).data("cl");
	 					tipo=$(div).data("tipo");
	 					return false;
	 				}
	 			});
	 		}
	 	}
 		return JSON.stringify({control:control,productos:JSON.stringify(colores),cl:idcl,tipo:tipo});
 	}
   	$.fn.new_parte_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_parte_pedido($(this));
 			}
 		});
 		function new_parte_pedido(e){
	   		if(e.data("pe")!=undefined){
	   			var atr="this|"+JSON.stringify({pe:e.data("pe")});
	   			btn_modal('save_parte_pedido',atr,'',"",'11');
	   			var atrib=new FormData();atrib.append('pe',e.data("pe"));
				var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
			 	e.get_1n('movimiento/new_parte_pedido',atrib,controls1);
	   		}else{
	   			msj("fail");
	   		}
 		}
   	}
   	$.fn.save_parte_pedido=function(){
   		if($(this).data("pe")!=undefined){
	   		var tip=$("#p_tip").val()+"";
	   		var fec=$("#p_fec").val();
	   		var obs=$("#p_obs").val();
	   		$(this).attr("disabled","disabled");
	   		if(tip=="0" || tip=="1" || tip=="2"){
	   			if(fecha(fec)){
	   				var control=true;
	   				if(obs!=""){ if(!textarea(obs,0,300)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
	   				var productos=JSON.parse($(this).lista_productos("new_parte_pedido"));
	   				if(productos['control']){
						var atrib=new FormData();
	   					atrib.append("detalles",productos['productos']);
	   					atrib.append("pe",$(this).data("pe"));
	   					atrib.append("tipo",productos['tipo']);
	   					atrib.append('cl',productos['cl']);
	   					atrib.append("fec",fec);
	   					atrib.append("obs",obs);
	   					var atrib3=new FormData();
	   					atrib3.append('num',$("#sp_num").val());
	   					atrib3.append('nom',$("#sp_nom").val());
						atrib3.append('cli',productos['cl']);
						atrib3.append('est',$("#sp_est").val());
	 					var controls=JSON.stringify({type:"set",preload:true});
	 					var controls1=JSON.stringify({id:"content_11",refresh:false,type:"html"});
	 					var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 					//console.log(productos['productos']);
	   					$(this).set_2n("movimiento/save_parte_pedido",atrib,controls,"movimiento/new_parte_pedido",atrib,controls1,"movimiento/view_pedido",atrib3,controls2);
	   				}else{
	   					msj("fail");
	   					$(this).removeAttr("disabled");
	   				}
	   			}else{
	   				alerta("Ingrese una fecha válida","top","p_fec");
	   				$(this).removeAttr("disabled");
	   			}
	   		}else{
	   			alerta("Seleccione una opcion porfavor.","top","p_tip");
	   			$(this).removeAttr("disabled");
	   		}
   		}
   	}
	   	/*-- Pagos --*/
		$.fn.pagos=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				pagos($(this));
	 			}
	 		});
	 		function pagos(e){
		   		if(e.data("pe")!=undefined){
		   			modal("","xlg","11");
		   			btn_modal('',"",'',"",'11');
		   			var atrib=new FormData();
		   			atrib.append('pe',e.data("pe"));
					var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				 	$(this).get_1n('movimiento/pagos',atrib,controls1);
		   		}else{
		   			msj("fail");
		   		}
	 		}
	   	}
	   	$.fn.new_pago=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_pago($(this));
	 			}
	 		});
	 		function new_pago(e){
	 			if(e.data("pe")!=undefined && $("#datas").data("max")!=undefined){
					modal("DEPÓSITO: Nuevo","xmd","2");
			   		var atr="this|"+JSON.stringify({pe:e.data("pe")});
			   		btn_modal('save_pago',atr,'',"",'2');
			   		var atrib=new FormData();
			   		atrib.append('pe',e.data("pe"));
			   		atrib.append('max',$("#datas").data("max"));
					var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					e.get_1n('movimiento/new_pago',atrib,controls1);
	 			}
	 		}
	   	}
	   		/*-Personas-*/
	   		$.fn.view_personas=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_personas($(this));
		 			}
		 		});
		 		function view_personas(e){
					modal("PERSONAS: lista","md","3");
			   		btn_modal('',"",'',"",'3');
					var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
					e.get_1n('movimiento/view_personas',{},controls1);
		 		}
	   		}
	   		$.fn.select_persona=function(){
		 		$(this).change(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				select_persona($(this));
		 			}
		 		});
		 		function select_persona(e){
		 			if(e.data("banco")!=undefined && e.data("cuenta")!=undefined){
		 				var atrib=new FormData();
		 				atrib.append("ci",e.val());
			 			var controls=JSON.stringify({id:e.data("banco"),refresh:false,type:"html"});
						var controls2=JSON.stringify({id:e.data("cuenta"),refresh:false,type:"html"});
			 			e.get_2n('movimiento/options_bancos',atrib,controls,'movimiento/options_cuentas',atrib,controls2);
		 			}
		 		}
	   		}
	   		/*-End personas-*/
	   		/*-Bancos-*/
	   		$.fn.view_bancos=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_bancos($(this));
		 			}
		 		});
		 		function view_bancos(e){
		 			if($("select#p_pro").val()!=""){
						modal("ENTIDADES BANCARIAS: lista","md","3");
				   		btn_modal('',"",'',"",'3');
						var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						e.get_1n('movimiento/view_bancos',{},controls1);
		 			}else{
		 				alerta("Seleccione una propietario de cuenta","top","p_pro");
		 			}
		 		}
	   		}
	   		$.fn.new_banco=function(){
	   			$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				new_banco($(this));
		 			}
		 		});
		 		function new_banco(e){
		 			if(e.data("type")!=undefined){
			 			if($("select#p_pro").val()!=""){
							modal("ENTIDAD BANCARIA: Nuevo","sm","4");
							var atr="this|"+JSON.stringify({type:e.data("type"),bancos:"p_ban",cuentas:"p_cta"});
					   		btn_modal('save_banco',atr,'',"",'4');
					   		var atrib=new FormData();
					   		atrib.append("type",e.data("type"));
							var controls1=JSON.stringify({id:"content_4",refresh:true,type:"html"});
							e.get_1n('movimiento/new_banco',atrib,controls1);
			 			}else{
			 				if(e.data("type")=="new"){alerta("Seleccione una propietario de cuenta","top","p_pro");}
			 				if(e.data("type")=="new-modal"){msj("Seleccione una propietario de cuenta");}
			 			}
		 			}
		 		}
	   		}
	   		$.fn.save_banco=function(){
	   			if($("select#p_pro").val()!="" && $(this).data("type")!=undefined && $(this).data("bancos")!=undefined && $(this).data("cuentas")!=undefined){
	   				var atrib=new FormData();
		 			var fot=$("#b_fot").prop("files");
		 			var raz=$("#b_raz").val();
		 			var url=$("#b_url").val();
		 			var des=$("#b_des").val();
		 			if(strSpace(raz,2,200)){
		 				var control=true;
		 				if(des!=""){if(!textarea(des,0,500)){alerta("Ingrese una contenido válido","top","b_des");control=false;}}
		 				if(control){
		 					atrib.append("ci",$("select#p_pro").val());
		 					atrib.append("archivo",fot[0]);
		 					atrib.append("raz",raz);
		 					atrib.append("url",url);
		 					atrib.append("des",des);
		 					atrib.append("selected_banco",$("#"+$(this).data("bancos")).val());
		 					atrib.append("selected_cuenta",$("#"+$(this).data("cuentas")).val());
		 					var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
							var controls3=JSON.stringify({id:$(this).data("bancos"),refresh:false,type:"html"});
					 		var controls4=JSON.stringify({id:$(this).data("cuentas"),refresh:false,type:"html"});
		 					if($(this).data("type")=="new-modal"){
						 		var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
					 			$(this).set_3n("movimiento/save_banco",atrib,controls,"movimiento/view_bancos",{},controls2,"movimiento/options_bancos",atrib,controls3,"movimiento/options_cuentas",atrib,controls4);
				 			}
					 		if($(this).data("type")=="new"){
					 			$(this).set_2n("movimiento/save_banco",atrib,controls,"movimiento/options_bancos",atrib,controls3,"movimiento/options_cuentas",atrib,controls4);					 			
					 		}
		 				}
		 			}else{
		 				alerta("Ingrese una valor válido","top","b_raz");
		 			}
	   			}
	   		}
	   		$.fn.config_banco=function(){
	   			$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				config_bancos($(this));
		 			}
		 		});
		 		function config_bancos(e){
		 			if(e.data("ba")!=undefined){
				 		if($("select#p_pro").val()!=""){
							modal("ENTIDAD BANCARIA: Modificar","sm","4");
							var atr="this|"+JSON.stringify({ba:e.data("ba"),bancos:"p_ban",cuentas:"p_cta"});
						   	btn_modal('update_banco',atr,'',"",'4');
						   	var atrib=new FormData();
						   	atrib.append("ba",e.data("ba"));
							var controls1=JSON.stringify({id:"content_4",refresh:true,type:"html"});
							e.get_1n('movimiento/config_banco',atrib,controls1);
				 		}else{
				 			msj("Seleccione una propietario de cuenta");
				 		}
		 			}
		 		}
	   		}
	   		$.fn.update_banco=function(){
	   			if($("select#p_pro").val()!="" && $(this).data("ba")!=undefined && $(this).data("bancos")!=undefined && $(this).data("cuentas")!=undefined){
		   			var atrib=new FormData();
			 		var fot=$("#b_fot").prop("files");
			 		var raz=$("#b_raz").val();
			 		var url=$("#b_url").val();
			 		var des=$("#b_des").val();
			 		if(strSpace(raz,2,200)){
			 			var control=true;
			 			if(des!=""){if(!textarea(des,0,500)){alerta("Ingrese una contenido válido","top","b_des");control=false;}}
			 			if(control){
			 				atrib.append("ba",$(this).data("ba"));
			 				atrib.append("ci",$("select#p_pro").val());
			 				atrib.append("archivo",fot[0]);
			 				atrib.append("raz",raz);
			 				atrib.append("url",url);
			 				atrib.append("des",des);
		 					atrib.append("selected_banco",$("#"+$(this).data("bancos")).val());
		 					atrib.append("selected_cuenta",$("#"+$(this).data("cuentas")).val());
						 	var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
							var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
						 	var controls3=JSON.stringify({id:$(this).data("bancos"),refresh:false,type:"html"});
						 	var controls4=JSON.stringify({id:$(this).data("cuentas"),refresh:false,type:"html"});
						 	$(this).set_3n("movimiento/update_banco",atrib,controls,"movimiento/view_bancos",{},controls2,"movimiento/options_bancos",atrib,controls3,"movimiento/options_cuentas",atrib,controls4);
			 			}
			 		}else{
			 			alerta("Ingrese una valor válido","top","b_raz");
			 		}
		   		}
	   		}
			$.fn.confirm_banco=function(e){
				$(this).click(function(){
				   	if($(this).prop("tagName")!="FORM"){
				   		confirm_banco($(this));
				   	}
				});
				function confirm_banco(e){
					if(e.data("ba")!=undefined){
						modal("Eliminar","xs","5");
						var atr="this|"+JSON.stringify({ba:e.data("ba"),bancos:"p_ban",cuentas:"p_cta"});
						btn_modal('drop_banco',atr,'',"",'5');
						var atrib=new FormData();
						atrib.append("ba",e.data("ba"));
						var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
						$(this).get_1n('movimiento/confirm_banco',atrib,controls);
					}
				}
			}
			$.fn.drop_banco=function(){
				if($("select#p_pro").val()!="" && $(this).data("ba")!=undefined && $(this).data("bancos")!=undefined && $(this).data("cuentas")!=undefined){
					var atrib=new FormData();
					atrib.append("ba",$(this).data("ba"));
					atrib.append("ci",$("select#p_pro").val());
		 			atrib.append("selected_banco",$("#"+$(this).data("bancos")).val());
		 			atrib.append("selected_cuenta",$("#"+$(this).data("cuentas")).val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:$(this).data("bancos"),refresh:false,type:"html"});
					var controls4=JSON.stringify({id:$(this).data("cuentas"),refresh:false,type:"html"});
					$(this).set_3n("movimiento/drop_banco",atrib,controls,"movimiento/view_bancos",{},controls2,"movimiento/options_bancos",atrib,controls3,"movimiento/options_cuentas",atrib,controls4);
				}
			}
			$.fn.select_banco=function(){
		 		$(this).change(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				select_banco($(this));
		 			}
		 		});
		 		function select_banco(e){
		 			if(e.data("cuenta")!=undefined){
		 				if($("select#p_pro").val()!=""){
			 				var atrib=new FormData();
			 				atrib.append("ci",$("select#p_pro").val());
			 				atrib.append("ba",e.val());
							var controls1=JSON.stringify({id:e.data("cuenta"),refresh:false,type:"html"});
				 			e.get_1n('movimiento/options_cuentas',atrib,controls1);
		 				}else{
		 					alerta("Seleccione una propietario de cuenta","top","p_pro");
		 				}
		 			}
		 		}
			}
	   		/*-End bancos-*/
	   		$.fn.new_cuenta=function(){
	   			$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				new_cuenta($(this));
		 			}
		 		});
		 		function new_cuenta(e){
			 		if($("select#p_pro").val()!=""){
			 			if($("select#p_ban").val()!=""){
							modal("CUENTA: Nuevo","sm","4");
							var atr="this|"+JSON.stringify({ci:$("select#p_pro").val(),ba:$("select#p_ban").val(),cuentas:"p_cta"});
						  		btn_modal('save_cuenta',atr,'',"",'4');
						  		var atrib=new FormData();
						  		atrib.append('ci',$("select#p_pro").val());
						  		atrib.append('ba',$("select#p_ban").val());
							var controls1=JSON.stringify({id:"content_4",refresh:true,type:"html"});
							e.get_1n('movimiento/new_cuenta',atrib,controls1);
			 			}else{
			 				alerta("Seleccione un banco","top","p_ban");
			 			}
			 		}else{
			 			alerta("Seleccione una propietario de cuenta","top","p_pro");
			 		}
		 		}
	   		}
	   		$.fn.save_cuenta=function(){
	   			if($(this).data("ci")!=undefined && $(this).data("ba")!=undefined && $(this).data("cuentas")){
	   				var cta=$("input#n_cta").val();
	   				if(strNoSpace(cta,5,100)){
	   					var atrib=new FormData();
	   					atrib.append("cta",cta);
	   					atrib.append("ci",$(this).data("ci"));
	   					atrib.append("ba",$(this).data("ba"));
						var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
						var controls2=JSON.stringify({id:$(this).data("cuentas"),refresh:false,type:"html"});
						$(this).set("movimiento/save_cuenta",atrib,controls,"movimiento/options_cuentas",atrib,controls2);

	   				}else{
	   					alerta("Ingrese una valor válido","top","n_cta");
	   				}
	   			}
	   		}
	   		/*-End cuentas-*/
	   		/*- Tipo de cambio -*/
	   		$.fn.view_tipos=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_tipos($(this));
		 			}
		 		});
		 		function view_tipos(e){
					modal("TIPO DE CAMBIO: Lista","md","3");
				   	btn_modal('',"",'',"",'3');
					var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
					e.get_1n('movimiento/view_tipos',{},controls1);
		 		}
	   		}
	   		$.fn.new_tipo_cambio=function(){
	   			$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				new_tipo_cambio($(this));
		 			}
		 		});
		 		function new_tipo_cambio(e){
		 			if(e.data("type")!=undefined){
						modal("TIPO DE CAMBIO: Nuevo","sm","4");
						var atr="this|"+JSON.stringify({type:e.data("type"),tipos:"p_tip"});
					   	btn_modal('save_tipo_cambio',atr,'',"",'4');
					   	var atrib=new FormData();
					   	atrib.append("type",e.data("type"));
						var controls1=JSON.stringify({id:"content_4",refresh:true,type:"html"});
						e.get_1n('movimiento/new_tipo_cambio',atrib,controls1);
		 			}
		 		}
	   		}
	   		$.fn.save_tipo_cambio=function(){
	   			if($(this).data("type")!=undefined && $(this).data("tipos")!=undefined){
	   				var nom=$("#tc_nom").val();
	   				var abr=$("#tc_abr").val();
	   				var mon=$("#tc_mon").val();
	   				if(strSpace(nom,2,100)){
	   					if(!abr.includes(" ")){
	   						if(decimal(mon,6,2) && mon>0 && mon<=9999.99){
	   							var atrib=new FormData();
			   					atrib.append("nom",nom);
			   					atrib.append("abr",abr);
			   					atrib.append("mon",mon);
			   					atrib.append("type",$(this).data("type"));
			   					atrib.append('selected',$("#"+$(this).data("tipos")).val());
								var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
								var controls3=JSON.stringify({id:$(this).data("tipos"),refresh:false,type:"html"});
								if($(this).data("type")=="new-modal"){
									var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
									$(this).set_2n("movimiento/save_tipo_cambio",atrib,controls,"movimiento/view_tipos",atrib,controls2,"movimiento/options_tipos_moneda",atrib,controls3);
								}else{
									$(this).set("movimiento/save_tipo_cambio",atrib,controls,"movimiento/options_tipos_moneda",atrib,controls3);
								}
	   						}else{
	   							alerta("Ingrese una valor válido","top","tc_mon");
	   						}
	   					}else{
	   						alerta("Ingrese una contenido válido","top","tc_abr");
	   					}
	   				}else{
	   					alerta("Ingrese una contenido válido","top","tc_nom");
	   				}
	   			}
	   		}
	   		$.fn.config_tipo_cambio=function(){
	   			$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				config_tipo_cambio($(this));
		 			}
		 		});
		 		function config_tipo_cambio(e){
		 			if(e.data("tc")!=undefined){
						modal("TIPO DE CAMBIO: Modificar","sm","4");
						var atr="this|"+JSON.stringify({tipos:"p_tip",tc:e.data("tc")});
					   	btn_modal('update_tipo_cambio',atr,'',"",'4');
					   	var atrib=new FormData();
					   	atrib.append("tc",e.data("tc"));
						var controls1=JSON.stringify({id:"content_4",refresh:true,type:"html"});
						e.get_1n('movimiento/config_tipo_cambio',atrib,controls1);
		 			}
		 		}
	   		}
	   		$.fn.update_tipo_cambio=function(){
	   			if($(this).data("tipos")!=undefined && $(this).data("tc")!=undefined){
	   				var nom=$("#tc_nom").val();
	   				var abr=$("#tc_abr").val();
	   				var mon=$("#tc_mon").val();
	   				if(strSpace(nom,2,100)){
	   					if(!abr.includes(" ")){
	   						if(decimal(mon,6,2) && mon>0 && mon<=9999.99){
	   							var atrib=new FormData();
			   					atrib.append("nom",nom);
			   					atrib.append("abr",abr);
			   					atrib.append("mon",mon);
			   					atrib.append("tc",$(this).data("tc"));
			   					atrib.append('selected',$("#"+$(this).data("tipos")).val());
								var controls=JSON.stringify({type:"set",preload:true,closed:"4"});
								var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
								var controls3=JSON.stringify({id:$(this).data("tipos"),refresh:false,type:"html"});
								$(this).set_2n("movimiento/update_tipo_cambio",atrib,controls,"movimiento/view_tipos",atrib,controls2,"movimiento/options_tipos_moneda",atrib,controls3);
	   						}else{
	   							alerta("Ingrese una valor válido","top","tc_mon");
	   						}
	   					}else{
	   						alerta("Ingrese una contenido válido","top","tc_abr");
	   					}
	   				}else{
	   					alerta("Ingrese una contenido válido","top","tc_nom");
	   				}
	   			}
	   		}
			$.fn.confirm_tipo_cambio=function(e){
				$(this).click(function(){
				   	if($(this).prop("tagName")!="FORM"){
				   		confirm_tipo_cambio($(this));
				   	}
				});
				function confirm_tipo_cambio(e){
					if(e.data("tc")!=undefined){
						modal("Eliminar","xs","5");
						var atr="this|"+JSON.stringify({tc:e.data("tc"),tipos:"p_tip"});
						btn_modal('drop_tipo_cambio',atr,'',"",'5');
						var atrib=new FormData();
						atrib.append("tc",e.data("tc"));
						var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
						$(this).get_1n('movimiento/confirm_tipo_cambio',atrib,controls);
					}
				}
			}
			$.fn.drop_tipo_cambio=function(){
				if($(this).data("tc")!=undefined && $(this).data("tipos")!=undefined){
					var atrib=new FormData();
					atrib.append("tc",$(this).data("tc"));
		 			atrib.append('selected',$("#"+$(this).data("tipos")).val());
					var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_3",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:$(this).data("tipos"),refresh:false,type:"html"});
					$(this).set_2n("movimiento/drop_tipo_cambio",atrib,controls,"movimiento/view_tipos",atrib,controls2,"movimiento/options_tipos_moneda",atrib,controls3);
				}
			}
	   		/*- Edn tipo de cambio -*/
	   	$.fn.select_tipo_cambio=function(){
		 	$(this).change(function(){
		 		if($(this).prop("tagName")!="FORM"){
		 			select_tipo_cambio($(this));
		 		}
		 	});
		 	function select_tipo_cambio(e){
		 		if(e.data("abr-cta")!=undefined){
		 			var monto=$("#p_mon").val();
		 			if(entero(e.val(),0,10)){
					 		var atrib=new FormData();
					 		atrib.append("tc",e.val());
					 		atrib.append("monto",$("#p_mon").val());
							var controls1=JSON.stringify({id:e.data("abr-cta"),refresh:false,type:"html"});
							var controls2=JSON.stringify({id:"p_bs",refresh:false,type:"value"});
							e.get_2n('movimiento/abreviatura_tipo_cuenta',atrib,controls1,'movimiento/moneda_nacional',atrib,controls2);
		 			}else{
		 				$("#"+e.data("abr-cta")).html("");
		 				$("#p_bs").val("");
		 			}
		 		}
		 	}
	   	}
	   	$.fn.moneda_nacional=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				moneda_nacional($(this));
	 			}
	 		});
	 		$(this).keyup(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				moneda_nacional($(this));
	 			}
	 		});
	 		$(this).on("mousewheel",function(e){
	 			if($(this).prop("tagName")!="FORM"){
	 				e.preventDefault();
	 			}
	 		});
	 		function moneda_nacional(e){
		   		var monto=$("#p_mon").val();
		   		if($("#p_tip").val()!="" && decimal(monto,10,2) && monto>0 && monto<999999999.92){
		   			var atrib=new FormData();
		   			atrib.append("tc",$("#p_tip").val());
		   			atrib.append("monto",monto);
		   			var controls1=JSON.stringify({id:"p_bs",refresh:false,type:"value"});
					$(this).get_1n('movimiento/moneda_nacional',atrib,controls1);
		   		}else{
		   			$("#p_bs").val("");
		   		}
	 		}
	   	}
	   	$.fn.save_pago=function(){
	   		if($(this).data("pe")!=undefined && $(this).data("pe")!=undefined && $("input#p_bs").data("max")!=undefined){
	   			var fec=$("#p_fec").val();
	   			var dep=$("#p_dep").val();
	   			var ci=$("#p_pro").val();
	   			var ban=$("#p_ban").val();
	   			var idbp=$("#p_cta").val();
	   			var tip=$("#p_tip").val();
	   			var mon=$("#p_mon").val();
	   			var mon_bs=$("#p_bs").val();
	   			var des=$("#p_des").val();
	   			if(fecha(fec)){
	   				if(strSpace(dep,2,500)){
		   				if(entero(ci,0,10)){
			   				if(entero(ban,0,10)){
					   			if(entero(idbp,0,10)){
				   					if(entero(tip,0,10)){
					   					if(decimal(mon,10,2) && mon>0 && mon<=999999999.99){
					   						if(mon_bs>0 && mon_bs<=($("input#p_bs").data("max")*1)){
					   							var control=true;
						   						if(des!=""){if(!textarea(des,0,900)){alerta("Ingrese un contenido válido.","top","p_des");control=false;}}
						   						if(control){
						   							var atrib=new FormData();
						   							atrib.append("fec",fec);
						   							atrib.append("dep",dep);
						   							atrib.append("ci",ci);
						   							atrib.append("idba",ban);
						   							atrib.append("idbp",idbp);
						   							atrib.append("idtc",tip);
						   							atrib.append("mon",mon);
						   							atrib.append("des",des);
						   							atrib.append('pe',$(this).data("pe"));
						   							atrib.append("max",$("input#p_bs").data("max"));
											   		var atrib3=new FormData();
											   		atrib3.append('num',$("#sp_num").val());
											   		atrib3.append('nom',$("#sp_nom").val());
													atrib3.append('cli',$("#sp_cli").val());
													atrib3.append('est',$("#sp_est").val());
													var controls=JSON.stringify({type:"set",preload:true,closed:"2"});
						   							var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
						   							var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
													$(this).set_2n("movimiento/save_pago",atrib,controls,"movimiento/pagos",atrib,controls2,"movimiento/view_pedido",atrib3,controls3);
						   						}
					   						}else{
					   							alerta("Valor no válido, el valor de ser mayor a cero y menor o igual a "+$("input#p_bs").data("max"),"right","p_bs");	
					   						}
						   				}else{
						   					alerta("Ingrese un valor válido","top","p_mon");	
						   				}
					   				}else{
					   					alerta("Seleccione un valor válido.","top","p_tip");	
					   				}
				   				}else{
				   					alerta("Seleccione un valor válido.","top","p_cta");	
				   				}		   					
			   				}else{
			   					alerta("Seleccione un valor válido.","top","p_ban");	
			   				}
		   				}else{
		   					alerta("Seleccione un valor válido.","top","p_pro");	
		   				}
	   				}else{
	   					alerta("Ingrese una contenido válido","top","p_dep");
	   				}
	   			}else{
	   				alerta("Ingrese un valor válido.","top","p_fec");
	   			}
	   		}
	   	}
	   	$.fn.config_pago=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				config_pago($(this));
	 			}
	 		});
	 		function config_pago(e){
	 			if(e.data("pe")!=undefined && e.data("pa")!=undefined && $("#datas").data("max")!=undefined){
					modal("DEPÓSITO: Nuevo","xmd","2");
			   		var atr="this|"+JSON.stringify({pa:e.data("pa"),pe:e.data("pe")});
			   		btn_modal('update_pago',atr,'',"",'2');
			   		var atrib=new FormData();
			   		atrib.append('pa',e.data("pa"));
			   		atrib.append('max',$("#datas").data("max"));
					var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					e.get_1n('movimiento/config_pago',atrib,controls1);
	 			}
	 		}
	   	}
	   	$.fn.update_pago=function(){
	   		if($(this).data("pe")!=undefined && $(this).data("pa")!=undefined && $("input#p_bs").data("max")!=undefined){
	   			var fec=$("#p_fec").val();
	   			var dep=$("#p_dep").val();
	   			var ci=$("#p_pro").val();
	   			var ban=$("#p_ban").val();
	   			var idbp=$("#p_cta").val();
	   			var tip=$("#p_tip").val();
	   			var mon=$("#p_mon").val();
	   			var mon_bs=$("#p_bs").val();
	   			var des=$("#p_des").val();
	   			if(fecha(fec)){
	   				if(strSpace(dep,2,500)){
		   				if(entero(ci,0,10)){
			   				if(entero(ban,0,10)){
					   			if(entero(idbp,0,10)){
				   					if(entero(tip,0,10)){
					   					if(decimal(mon,10,2) && mon>0 && mon<=999999999.99){
					   						if(mon_bs>0 && mon_bs<=($("input#p_bs").data("max")*1)){
					   							var control=true;
						   						if(des!=""){if(!textarea(des,0,900)){alerta("Ingrese un contenido válido.","top","p_des");control=false;}}
						   						if(control){
						   							var atrib=new FormData();
						   							atrib.append("fec",fec);
						   							atrib.append("dep",dep);
						   							atrib.append("ci",ci);
						   							atrib.append("idba",ban);
						   							atrib.append("idbp",idbp);
						   							atrib.append("idtc",tip);
						   							atrib.append("mon",mon);
						   							atrib.append("des",des);
						   							atrib.append('pa',$(this).data("pa"));
						   							atrib.append('pe',$(this).data("pe"));
						   							atrib.append("max",$("input#p_bs").data("max"));
						   							var atrib3=new FormData();
											   		atrib3.append('num',$("#sp_num").val());
											   		atrib3.append('nom',$("#sp_nom").val());
													atrib3.append('cli',$("#sp_cli").val());
													atrib3.append('est',$("#sp_est").val());
													var controls=JSON.stringify({type:"set",preload:true,closed:"2"});
						   							var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
						   							var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
													$(this).set_2n("movimiento/update_pago",atrib,controls,"movimiento/pagos",atrib,controls2,"movimiento/view_pedido",atrib3,controls3);
						   						}
					   						}else{
					   							alerta("Valor no válido, el valor de ser mayor a cero y menor o igual a "+$("input#p_bs").data("max"),"right","p_bs");	
					   						}
						   				}else{
						   					alerta("Ingrese un valor válido","top","p_mon");	
						   				}
					   				}else{
					   					alerta("Seleccione un valor válido.","top","p_tip");	
					   				}
				   				}else{
				   					alerta("Seleccione un valor válido.","top","p_cta");	
				   				}		   					
			   				}else{
			   					alerta("Seleccione un valor válido.","top","p_ban");	
			   				}
		   				}else{
		   					alerta("Seleccione un valor válido.","top","p_pro");	
		   				}
	   				}else{
	   					alerta("Ingrese una valor válido","top","p_dep");
	   				}
	   			}else{
	   				alerta("Ingrese un valor válido.","top","p_fec");
	   			}
	   		}
	   	}
	   	$.fn.confirm_pago=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				confirm_pago($(this));
	 			}
	 		});
	 		function confirm_pago(e){
	 			if(e.data("pe")!=undefined && e.data("pa")!=undefined){
			   		modal("DEPÓSITO: Eliminar","xs","5");
			   		var atr="this|"+JSON.stringify({pe:e.data("pe"),pa:e.data("pa")});
			   		btn_modal('drop_pago',atr,'',"","5");
					var atrib=new FormData();
			   		atrib.append('pe',e.data("pe"));
			   		atrib.append('pa',e.data("pa"));
					var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					$(this).get_1n('movimiento/confirm_pago',atrib,controls1);
	 			}
	 		}
	   	}
	   	$.fn.drop_pago=function(){
	   		if($(this).data("pe")!=undefined && $(this).data("pa")!=undefined){
		   		var atrib=new FormData();
			   	atrib.append('pe',$(this).data("pe"));
			   	atrib.append('pa',$(this).data("pa"));
		   		var atrib3=new FormData();
		   		atrib3.append('num',$("#sp_num").val());
		   		atrib3.append('nom',$("#sp_nom").val());
				   atrib3.append('cli',$("#sp_cli").val());
				   atrib3.append('est',$("#sp_est").val());
				var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html"});
				$(this).set_2n("movimiento/drop_pago",atrib,controls1,"movimiento/pagos",atrib,controls2,"movimiento/view_pedido",atrib3,controls3);
	   		}
	   	}
	   		/*--- Depositos ---*/
		   	$.fn.view_descuentos=function(){
		 		$(this).click(function(e){
		 			if($(this).prop("tagName")!="FORM"){
		 				view_descuentos($(this));
		 			}
		 			e.preventDefault();
		 		});
		 		function view_descuentos(e){
		 			if(e.data("pa")!=undefined && e.data("pe")!=undefined){
				   		modal("DESCUENTOS: Lista","md","2");
				   		btn_modal('',"",'',"","2");
				   		var atrib=new FormData();
				   		atrib.append('pe',e.data("pe"));
				   		atrib.append('pa',e.data("pa"));
						var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
						$(this).get_1n('movimiento/view_descuentos',atrib,controls1);
		 			}
		 		}
		   	}
		   	$.fn.new_descuento=function(){
				$(this).click(function(e){
		 			if($(this).prop("tagName")!="FORM"){
		 				new_descuento($(this));
		 			}
		 		});
		 		function new_descuento(e){
		 			if($("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
				   		modal("DESCUENTOS: Nuevo","sm","3");
				   		btn_modal('save_descuento',"this",'',"","3");
				   		var atrib=new FormData();
				   		atrib.append("pa",$("#datos-pago").data("pa"));
						var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						$(this).get_1n('movimiento/new_descuento',atrib,controls1);
		 			}
		 		}
		   	}
		   	$.fn.save_descuento=function(){
		   		if($("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
					var mon=$("input#p3_mon").val();
					var obs=$("textarea#p3_obs").val();
					if(decimal(mon,8,1) && mon>0 && mon<9999999.9){
						if(textarea(obs,0,500)){
							var atrib=new FormData();
							atrib.append("mon",mon);
							atrib.append("obs",obs);
					   		atrib.append('pe',$("#datos-pago").data("pe"));
					   		atrib.append('pa',$("#datos-pago").data("pa"));
							var controls1=JSON.stringify({type:"set",preload:true,closed:"3"});
							var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
							var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
							$(this).set_2n("movimiento/save_descuento",atrib,controls1,"movimiento/view_descuentos",atrib,controls2,"movimiento/pagos",atrib,controls3);
						}else{
							alerta("Ingrese un contenido válido","top","p3_obs");	
						}
					}else{
						alerta("Ingrese un monto válido","top","p3_mon");
					}
		   		}
		   	}
		   	$.fn.config_descuento=function(){
				$(this).click(function(e){
		 			if($(this).prop("tagName")!="FORM"){
		 				config_descuento($(this));
		 			}
		 		});
		 		function config_descuento(e){
		 			if(e.data("dpa")!=undefined && $("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
				   		modal("DESCUENTOS: Nuevo","sm","3");
				   		var atr="this|"+JSON.stringify({dpa:e.data("dpa")});
				   		btn_modal('update_descuento',atr,'',"","3");
				   		var atrib=new FormData();
				   		atrib.append("dpa",e.data("dpa"));
				   		atrib.append("pa",$("#datos-pago").data("pa"));
						var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						$(this).get_1n('movimiento/config_descuento',atrib,controls1);
		 			}
		 		}
		   	}
		   	$.fn.update_descuento=function(){
		   		if($(this).data("dpa")!=undefined && $("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
					var mon=$("input#p3_mon").val();
					var obs=$("textarea#p3_obs").val();
					if(decimal(mon,8,1) && mon>0 && mon<9999999.9){
						if(textarea(obs,0,500)){
							var atrib=new FormData();
							atrib.append("mon",mon);
							atrib.append("obs",obs);
							atrib.append("dpa",$(this).data("dpa"));
					   		atrib.append('pe',$("#datos-pago").data("pe"));
					   		atrib.append('pa',$("#datos-pago").data("pa"));
							var controls1=JSON.stringify({type:"set",preload:true,closed:"3"});
							var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
							var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
							$(this).set_2n("movimiento/update_descuento",atrib,controls1,"movimiento/view_descuentos",atrib,controls2,"movimiento/pagos",atrib,controls3);
						}else{
							alerta("Ingrese un contenido válido","top","p3_obs");	
						}
					}else{
						alerta("Ingrese un monto válido","top","p3_mon");
					}
		   		}
		   	}
		   	$.fn.confirm_descuento=function(){
		 		$(this).click(function(){
		 			if($(this).prop("tagName")!="FORM"){
		 				confirm_descuento($(this));
		 			}
		 		});
		 		function confirm_descuento(e){
		 			if(e.data("dpa")!=undefined && $("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
				   		modal("DESCUENTO: Eliminar","xs","5");
				   		var atr="this|"+JSON.stringify({dpa:e.data("dpa")});
				   		btn_modal('drop_descuento',atr,'',"","5");
						var atrib=new FormData();
				   		atrib.append('dpa',e.data("dpa"));
				   		atrib.append('pe',$("#datos-pago").data("pe"));
				   		atrib.append('pa',$("#datos-pago").data("pa"));
						var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
						$(this).get_1n('movimiento/confirm_descuento',atrib,controls1);
		 			}
		 		}
		   	}
		   	$.fn.drop_descuento=function(){
		   		if($(this).data("dpa")!=undefined && $("#datos-pago").data("pa")!=undefined && $("#datos-pago").data("pe")!=undefined){
			   		var atrib=new FormData();
				   	atrib.append('dpa',$(this).data("dpa"));
				   	atrib.append('pe',$("#datos-pago").data("pe"));
				   	atrib.append('pa',$("#datos-pago").data("pa"));
			   		atrib.append('num',$("#sp_num").val());
			   		atrib.append('nom',$("#sp_nom").val());
			   		atrib.append('cli',$("#sp_cli").val());
					var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
					$(this).set_2n("movimiento/drop_descuento",atrib,controls1,"movimiento/view_descuentos",atrib,controls2,"movimiento/pagos",atrib,controls3);
		   		}
		   	}
	   		/*--- End depositos ---*/
	   	/*-- End pagos --*/
	   /*-- Descuentos de productos --*/
	   	$.fn.descuento_producto=function(){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				descuento_producto($(this));
	 			}
	 		});
	 		function descuento_producto(e){
	 			if(e.data("pe")!=undefined){
			   		btn_modal('',"",'',"","11");
			   		var atrib=new FormData();
			   		atrib.append('pe',e.data("pe"));
					var controls1=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				 	$(this).get_1n('movimiento/descuento_producto',atrib,controls1);
	 			}
	 		}
	   	}
	   	$.fn.new_descuento_producto=function(){
			$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				new_descuento_producto($(this));
	 			}
	 		});
	 		function new_descuento_producto(e){
	 			if(e.data("pe")!=undefined){
					modal("DESCUENTOS: Nuevo","lg","2");
				   	btn_modal('',"",'',"","2");
			   		var atrib=new FormData();
			   		atrib.append('pe',e.data("pe"));
			   		if(e.data("pp")!=undefined){ atrib.append("pp",e.data("pp"));};
					var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				 	$(this).get_1n('movimiento/new_descuento_producto',atrib,controls1);
	 			}
	 		}
	   	}
	   	$.fn.total_descuento_producto=function(){
			$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				total_descuento_producto($(this));
	 			}
	 		});
	 		$(this).keyup(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				total_descuento_producto($(this));
	 			}
	 		});
	 		$(this).on("mousewheel",function(e){
	 			if($(this).prop("tagName")!="FORM"){
	 				e.preventDefault();
	 			}
	 		});
	 		function total_descuento_producto(e){
	 			if(e.data("id")!=undefined){
	 				if($("input#cantidad"+e.data("id")).val()!=undefined && $("input#cantidad"+e.data("id")).data("cu")!=undefined && $("input#cantidad"+e.data("id")).data("cantidad")!=undefined && $("input#porcentaje"+e.data("id")).val()!=undefined && $("input#descuento"+e.data("id")).val()!=undefined){
	 					if($("input#cantidad"+e.data("id")).val()*1>0 && $("input#porcentaje"+e.data("id")).val()*1>0){
	 						//console.log("Entro");
	 						if($("input#cantidad"+e.data("id")).val()*1<=$("input#cantidad"+e.data("id")).data("cantidad")*1){
	 							$("input#cantidad"+e.data("id")).removeAttr("style");
	 							if($("input#porcentaje"+e.data("id")).val()*1<=100 && $("input#porcentaje"+e.data("id")).val()*1>=0){
	 								$("input#porcentaje"+e.data("id")).removeAttr("style");
	 								var porcentaje=($("input#porcentaje"+e.data("id")).val()*1)/100;
	 								var cu=$("input#cantidad"+e.data("id")).data("cu")*1;
	 								var cantidad=$("input#cantidad"+e.data("id")).val()*1;
	 								//console.log(cu);
	 								$("input#descuento"+e.data("id")).val(number_format((cu*cantidad)*porcentaje,1,'.',','));
	 							}else{
	 								$("input#porcentaje"+e.data("id")).css("border","1px solid red");	 							
									$("input#descuento"+e.data("id")).val("0");
	 							}
	 						}else{
								$("input#cantidad"+e.data("id")).css("border","1px solid red");	 							
								$("input#descuento"+e.data("id")).val("0");
	 						}
	 					}else{
	 						$("input#descuento"+e.data("id")).val("0");
	 					}
	 				}else{
	 					console.log("Error de variables...");
	 				}
	 			}else{
	 				console.log("Error de variables...");
	 			}
	 		}
	   	}
	   	$.fn.save_descuento_producto=function(){
			$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				save_descuento_producto($(this));
	 			}
	 		});
	 		function save_descuento_producto(e){
				if(e.data("sdp")!=undefined && e.data("pe")!=undefined && $("input#cantidad"+e.data("id")).val()!=undefined && $("input#cantidad"+e.data("id")).data("cu")!=undefined && $("input#cantidad"+e.data("id")).data("cantidad")!=undefined && $("input#cantidad"+e.data("id")).data("max")!=undefined && $("input#porcentaje"+e.data("id")).val()!=undefined && $("input#descuento"+e.data("id")).val()!=undefined){
						var cantidad=$("input#cantidad"+e.data("id")).val()*1;
						var max=$("input#cantidad"+e.data("id")).data("max")*1;
						var porcentaje=($("input#porcentaje"+e.data("id")).val()*1);
						if(max*1>0){
							if(entero(cantidad,0,10) && cantidad>0 && cantidad<=max){
								$("input#cantidad"+e.data("id")).removeAttr("style");
								if(decimal(porcentaje,3,1) && porcentaje*1<=100 && porcentaje>0){
									$("input#porcentaje"+e.data("id")).removeAttr("style");
									porcentaje/=100;
									var cu=$("input#cantidad"+e.data("id")).data("cu")*1;
									var descuento=(cu*cantidad)*porcentaje;
									if(descuento>0){
										var obs=$("textarea#obs"+e.data("id")).val();
										if(textarea(obs,0,900)){
											var atrib=new FormData();
											atrib.append("sdp",e.data("sdp"));
											atrib.append('pe',e.data("pe"));
											atrib.append("cantidad",cantidad);
											atrib.append("porcentaje",$("input#porcentaje"+e.data("id")).val()*1);
											atrib.append("obs",obs);
											var atrib3=new FormData();
											atrib3.append('num',$("#sp_num").val());
											atrib3.append('nom',$("#sp_nom").val());
											atrib3.append('cli',$("#sp_cli").val());
											atrib3.append('est',$("#sp_est").val());
										   	var controls1=JSON.stringify({type:"set",preload:true,closed:"2"});
											var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
											var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
										  	$(this).set_2n('movimiento/save_descuento_producto',atrib,controls1,'movimiento/descuento_producto',atrib,controls2,'movimiento/view_pedido',atrib3,controls3);
										}else{
											alerta("Ingrese un valor válido","top","obs"+e.data("id"));
										}
									}else{
										console.log("Error en variables...")
									}
								}else{
									alerta("Ingrese una valor válido mayor a cero y menor o igual a 100, solo se acepta valores con una sola decimal.","top","porcentaje"+e.data("id"));
									$("input#porcentaje"+e.data("id")).css("border","1px solid red");	 							
								   $("input#descuento"+e.data("id")).val("0");
								}
							}else{
								alerta("Ingrese una valor mayor a 0 y menor o igual a "+max,"top","cantidad"+e.data("id"));
								$("input#descuento"+e.data("id")).val("0");
							}
						}else{
							msj("La cantidad de descuento fue excedida a la cantidad del pedido.");
						}
				}else{
					console.log("Error en variables.....");
				}
	 		}
		}
		$.fn.config_descuento_producto=function(){
			$(this).click(function(){
				if($(this).prop("tagName")!="FORM"){
					config_descuento_producto($(this));
				}
			});
			function config_descuento_producto(e){
				if(e.data("pe")!=undefined && e.data("des")!=undefined){
					modal("DESCUENTO PRODUCTO: Modificar","md","2");
					var atr="this|"+JSON.stringify({des:e.data("des"),pe:e.data("pe")});
				   	btn_modal('update_descuento_producto',atr,'',"","2");
			   		var atrib=new FormData();
			   		atrib.append('des',e.data("des"));
					var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				 	$(this).get_1n('movimiento/config_descuento_producto',atrib,controls1);
	 			}
			}
		}
		$.fn.total_update_descuento_producto=function(){
			$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				total_update_descuento_producto($(this));
	 			}
	 		});
	 		$(this).keyup(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				total_update_descuento_producto($(this));
	 			}
	 		});
	 		$(this).on("mousewheel",function(e){
	 			if($(this).prop("tagName")!="FORM"){
	 				e.preventDefault();
	 			}
	 		});
	 		function total_update_descuento_producto(e){
	 			if($("input#cu-2").data("value")!=undefined && $("input#cantidad-2").data("max")!=undefined){
					var cu=$("input#cu-2").data("value");
					var cantidad=$("input#cantidad-2").val();
					var porcentaje=$("input#porcentaje-2").val();
					var costo_total=cu*cantidad;
					if(porcentaje>0 && porcentaje<=100 && cantidad<=($("input#cantidad-2").data("max")*1) && costo_total>0){
						$("input#ct-2").val(number_format(costo_total,1,'.',''));
						$("input#descuento-2").val(number_format(costo_total*(porcentaje/100),1,'.',''));
					}else{
						$("input#ct-2").val("");
						$("input#descuento-2").val("");
					}
	 			}else{
	 				console.log("Error de variables...");
	 			}
	 		}
		   }
		$.fn.update_descuento_producto=function(){
			if($(this).data("pe")!=undefined && $(this).data("des")!=undefined && $("input#cantidad-2").data("max")!=undefined && $("input#cu-2").data("value")!=undefined){
				var cantidad=$("input#cantidad-2").val();
				var porcentaje=$("input#porcentaje-2").val();
				var observacion=$("textarea#observacion-2").val();
				var cu=$("input#cu-2").data("value");
				var max=$("input#cantidad-2").data("max")*1;
				if(entero(cantidad,0,4) && cantidad>0 && cantidad<=max){
					if(decimal(porcentaje,3,1) && porcentaje>0 && porcentaje<=100){
						if(textarea(observacion,0,900)){
							var atrib=new FormData();
							atrib.append('pe',$(this).data("pe"));   
							atrib.append('des',$(this).data("des"));
							atrib.append('cantidad',cantidad);
							atrib.append('porcentaje',porcentaje);
							atrib.append('observacion',observacion);
							atrib.append('max',max);
							var atrib3=new FormData();
							atrib3.append('num',$("#sp_num").val());
							atrib3.append('nom',$("#sp_nom").val());
							atrib3.append('cli',$("#sp_cli").val());
							atrib3.append('est',$("#sp_est").val());
							var controls1=JSON.stringify({type:"set",preload:true,closed:"2"});
							var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
							var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
							$(this).set_2n('movimiento/update_descuento_producto',atrib,controls1,'movimiento/descuento_producto',atrib,controls2,'movimiento/view_pedido',atrib3,controls3);							
						}else{
							alerta("Ingrese un contenido válido","top","observacion-2");
						}
					}else{
						alerta("Ingrese una valor válido mayor a cero y menor o igual a 100.0, solo se acepta una decimal","top","porcentaje-2");
					}
				}else{
					alerta("Ingrese un valor válido mayor a cero y menor o igual a "+max,'top',"cantidad-2");
				}
	 		}
		}
			$.fn.config_descuento_producto_detalle=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						config_descuento_producto_detalle($(this));
					}
				});
				function config_descuento_producto_detalle(e){
					if(e.data("des")!=undefined){
						modal("DESCUENTO PRODUCTO: Modificar","md","2");
						btn_modal('',"",'',"","2");
						var atrib=new FormData();
						atrib.append('des',e.data("des"));
						var controls1=JSON.stringify({id:"content_2",refresh:true,type:"html"});
						$(this).get_1n('movimiento/config_descuento_producto_detalle',atrib,controls1);
					}
				}
			}
			$.fn.adicionar_detalle_descuento=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						adicionar_detalle_descuento($(this));
					}
				});
				function adicionar_detalle_descuento(e){
					if(e.data("des")!=undefined && e.data("pe")!=undefined){
						modal("DETALLE DESCUENTO: Nuevo","sm","3");
						var atr="this|"+JSON.stringify({des:e.data("des"),pe:e.data("pe")});
						btn_modal('save_detalle_descuento',atr,'',"","3");
						var atrib=new FormData();
						atrib.append('des',e.data("des"));
						var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						$(this).get_1n('movimiento/adicionar_detalle_descuento',atrib,controls1);
					}
				}
			}
			$.fn.save_detalle_descuento=function(){
				if($(this).data("des")!=undefined && $(this).data("pe")!=undefined){
					var fot=$("input#d_fot3").prop("files");
					var obs=$("textarea#d_obs3").val();
					if(fot.length>0){
						if(textarea(obs,0,500)){
							var atrib=new FormData();
							atrib.append("archivo",fot[0]);
							atrib.append("obs",obs);
							atrib.append('des',$(this).data("des"));
							atrib.append('pe',$(this).data("pe"));
							var controls1=JSON.stringify({type:"set",preload:true,closed:"3"});
							var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
							var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
							$(this).set_2n('movimiento/save_detalle_descuento',atrib,controls1,'movimiento/config_descuento_producto_detalle',atrib,controls2,'movimiento/descuento_producto',atrib,controls3);
						}else{
							alerta("Ingrese un valor válido","top","d_obs3");
						}
					}else{
						alerta("Debe seleccionar una fotografia","top","d_fot3");
					}
				}
			}
			$.fn.change_detalle_descuento=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						change_detalle_descuento($(this));
					}
				});
				function change_detalle_descuento(e){
					if(e.data("desp")!=undefined && e.data("des")!=undefined && e.data("pe")!=undefined){
						modal("DETALLE DESCUENTO: Modificar","sm","3");
						var atr="this|"+JSON.stringify({des:e.data("des"),desp:e.data("desp"),pe:e.data("pe")});
						btn_modal('update_detalle_descuento',atr,'',"","3");
						var atrib=new FormData();
						atrib.append('desp',e.data("desp"));
						var controls1=JSON.stringify({id:"content_3",refresh:true,type:"html"});
						$(this).get_1n('movimiento/change_detalle_descuento',atrib,controls1);
					}
				}
			}
			$.fn.update_detalle_descuento=function(){
				if($(this).data("desp")!=undefined && $(this).data("des")!=undefined && $(this).data("pe")!=undefined){
					var fot=$("input#d_fot3").prop("files");
					var obs=$("textarea#d_obs3").val();
					if(textarea(obs,0,500)){
						var atrib=new FormData();
						atrib.append("archivo",fot[0]);
						atrib.append("obs",obs);
						atrib.append('desp',$(this).data("desp"));
						atrib.append('des',$(this).data("des"));
						atrib.append('pe',$(this).data("pe"));
						var controls1=JSON.stringify({type:"set",preload:true,closed:"3"});
						var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
						var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
						$(this).set_2n('movimiento/update_detalle_descuento',atrib,controls1,'movimiento/config_descuento_producto_detalle',atrib,controls2,'movimiento/descuento_producto',atrib,controls3);
					}else{
						alerta("Ingrese un valor válido","top","d_obs3");
					}
				}
			}
			$.fn.confirm_detalle_descuento=function(){
				$(this).click(function(){
					if($(this).prop("tagName")!="FORM"){
						confirm_detalle_descuento($(this));
					}
				});
				function confirm_detalle_descuento(e){
					if(e.data("desp")!=undefined && e.data("des")!=undefined && e.data("pe")!=undefined){
						modal("DESCUENTO: Eliminar","xs","5");
						var atr="this|"+JSON.stringify({des:e.data("des"),desp:e.data("desp"),pe:e.data("pe")});
						btn_modal('drop_detalle_descuento',atr,'',"","5");
						var atrib=new FormData();
						atrib.append('desp',e.data("desp"));
						var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
						$(this).get_1n('movimiento/confirm_detalle_descuento',atrib,controls1);
					}
				}
			}
			$.fn.drop_detalle_descuento=function(){
				if($(this).data("desp")!=undefined && $(this).data("des")!=undefined && $(this).data("pe")!=undefined){
					var atrib=new FormData();
					atrib.append('desp',$(this).data("desp"));
					atrib.append('des',$(this).data("des"));
					atrib.append('pe',$(this).data("pe"));
					var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
					var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html"});
					var controls3=JSON.stringify({id:"content_11",refresh:false,type:"html"});
					$(this).set_2n('movimiento/drop_detalle_descuento',atrib,controls1,'movimiento/config_descuento_producto_detalle',atrib,controls2,'movimiento/descuento_producto',atrib,controls3);
				}
			}
		$.fn.confirm_descuento_producto=function(){
			$(this).click(function(){
				if($(this).prop("tagName")!="FORM"){
					confirm_descuento_producto($(this));
				}
			});
			function confirm_descuento_producto(e){
				if(e.data("pe")!=undefined && e.data("des")!=undefined){
					modal("DESCUENTO: Eliminar","xs","5");
					var atr="this|"+JSON.stringify({pe:e.data("pe"),des:e.data("des")});
					btn_modal('drop_descuento_producto',atr,'',"","5");
					var atrib=new FormData();
					atrib.append('des',e.data("des"));
					var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
					$(this).get_1n('movimiento/confirm_descuento_producto',atrib,controls1);
				}
			}
		}
		$.fn.drop_descuento_producto=function(){
			if($(this).data("des")!=undefined && $(this).data("pe")!=undefined){
				var atrib=new FormData();
				atrib.append('des',$(this).data("des"));
				atrib.append('pe',$(this).data("pe"));
				var atrib3=new FormData();
				atrib3.append('num',$("#sp_num").val());
				atrib3.append('nom',$("#sp_nom").val());
				atrib3.append('cli',$("#sp_cli").val());
				atrib3.append('est',$("#sp_est").val());
				var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls2=JSON.stringify({id:"content_11",refresh:true,type:"html"});
				var controls3=JSON.stringify({id:"contenido",refresh:true,type:"html"});
				$(this).set_2n('movimiento/drop_descuento_producto',atrib,controls1,'movimiento/descuento_producto',atrib,controls2,'movimiento/view_pedido',atrib3,controls3);				
			}
		}
	   /*-- End descuentos de productos --*/
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirmar_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirmar_pedido($(this));
 			}
 		});
 		function confirmar_pedido(e){
 			if(e.data("pe")!=undefined){
		   		modal("PEDIDO: Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({pe:e.data("pe")});
		   		btn_modal('drop_pedido',atr,'',"","5");
		   		var atrib=new FormData();atrib.append('idpe',e.data("pe"));
				var controls1=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			 	$(this).get_1n('movimiento/confirmar_pedido',atrib,controls1);
 			}
 		}
   	}
   	$.fn.drop_pedido=function(){
   		if($(this).data("pe")!=undefined){
	   		var atrib=new FormData();
	   		atrib.append('u',$("#e_user").val());
	   		atrib.append('p',$("#e_password").val());
	   		atrib.append('idpe',$(this).data("pe"));
	   		var atrib3=new FormData();
	   		atrib3.append('num',$("#sp_num").val());
	   		atrib3.append('nom',$("#sp_nom").val());
			   atrib3.append('cli',$("#sp_cli").val());
			   atrib3.append('est',$("#sp_est").val());
			var controls1=JSON.stringify({type:"set",preload:true});
			var controls3=JSON.stringify({id:"contenido",refresh:false,type:"html",closed:"5"});
			$(this).set('movimiento/drop_pedido',atrib,controls1,'movimiento/view_pedido',atrib3,controls3);
   		}
   	}
   	/*--- End eliminar ---*/
	/*--- Nuevo ---*/
	$.fn.new_pedido=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_pedido($(this));
 			}
 		});
 		function new_pedido(e){
 			modal("PEDIDO: Nuevo","xlg","1");
		 	btn_modal('save_pedido',"this",'',"",'1');
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
	 		$(this).get_1n('movimiento/new_pedido',{},controls);
 		}
	}
 	$.fn.save_pedido=function(){
 		var e=$(this);
	 	var nro=$("#p_nro").html();
	 	var nom=$("#p_nom").val();
	 	var cli=$("#p_cli").val();
	 	var fec=$("#p_fec").val();
	 	var obs=$("#p_obs").val();
	 	var des=$("#p_des").val();
	 	if(entero(nro,0,10)){
	 		if(strSpace(nom,3,150)){
	 			if(entero(cli,0,10)){
	 				if(fecha(fec)){
	 					var control=true;
	 					if(des!=""){ if(!decimal(des,6,1) || des<0 || des>999999.9){ control=false; alerta("Ingrese un valor válido","top","p_des");}}
	 					if(obs!=""){ if(!textarea(obs,0,300)){ control=false; alerta("Ingrese un contenido válido","top","p_obs");}}
	 					if(control){
	 						if($("div#list_product table tr.row-producto").length>0){
								var productos=JSON.parse($(this).lista_productos("new_pedido"));
					   			if(productos['control']){
									var atrib=new FormData();
					   				atrib.append("detalles",productos['productos']);
					   				atrib.append("nro",nro);
			 						atrib.append("nom",nom);
			 						atrib.append("fec",fec);
			 						atrib.append("obs",obs);
			 						atrib.append("des",des);
									atrib.append("cl",productos['cl']);
			 						atrib.append("tipo",productos['tipo']);
			 						var atrib3=new FormData();
			 						atrib3.append('num',$("#sp_num").val());
			 						atrib3.append('nom',$("#sp_nom").val());
									 atrib3.append('cli',$("#sp_cli").val());
									 atrib3.append('est',$("#sp_est").val());
			 						var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
			 						var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			 						$(this).set('movimiento/save_pedido',atrib,controls1,'movimiento/view_pedido',atrib3,controls2);
					   			}else{
					   				msj("Error, verifique los datos en los productos por favor.");
					   				e.removeAttr("disabled");
					   			}
	 						}else{
	 							alerta("El pedido debe tener al menos 1 producto registrado","top","view_producto");
	 							e.removeAttr("disabled");
	 						}
	 					}else{
	 						e.removeAttr("disabled");
	 					}
	 				}else{
	 					alerta("Ingrese una fecha válida","top","p_fec");
	 					e.removeAttr("disabled");
	 				}
	 			}else{
	 				alerta("Seleccione un cliente","top","p_cli");
	 				e.removeAttr("disabled");
	 			}
	 		}else{
	 			alerta("Ingrese una nombre de pedido válido","top","p_nom");
	 			e.removeAttr("disabled");
	 		}
	 	}else{
	 		alerta("Número de pedido invalido, actualice el número porfavor.","top","p_act");
	 		e.removeAttr("disabled");
	 	}
 	}
	/*--- End nuevo ---*/
 	/*--- Imprimir ---*/
   	$.fn.print_pedidos=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_pedidos($(this));
 			}
 		});
 		function print_pedidos(e){
 			if(e.data("tbl")!=undefined){
 				modal("PRODUCTOS: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("pe")!=undefined){
		 				visibles[visibles.length]=$(tr).data("pe");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		   		atrib.append('tbl',e.data("tbl"));
	 			atrib.append('num',$("#sp_num").val());
	 			atrib.append('nom',$("#sp_nom").val());
	 			atrib.append('cli',$("#sp_cli").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('movimiento/print_pedidos',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
	/*---- librerias pedido ----*/
 	$.fn.pedido_seg=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				pedido_seg($(this));
 			}
 		});
 		function pedido_seg(e){
 			if(e.data("pe")!=undefined){
		 		modal("Historial de cambios en pedido","lg","2");btn_modal('',"",'',"",'2');
		 		var atrib=new FormData();atrib.append('pe',e.data("pe"));
		 		var controls=JSON.stringify({id:"content_2",refresh:true,type:"html",closed:false});
		 		e.get_1n('movimiento/pedido_seg',atrib,controls);
 			}else{
 				console.log("¡Error de variables...!");
 			}
 		}
 	}
	$.fn.parte_seg=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				parte_seg($(this));
 			}
 		});
 		function parte_seg(e){
 			if(e.data("pp")!=undefined && e.data("pe")!=undefined){
	 			modal("Historial de cambios en parte pedido","lg","2");btn_modal('',"",'',"",'2');
	 			var atrib = new FormData();atrib.append('pp',e.data("pp"));atrib.append('pe',e.data("pe"));
	 			var controls=JSON.stringify({id:"content_2",refresh:true,type:"html",closed:false});
	 			e.get_1n('movimiento/parte_seg',atrib,controls);
 			}else{
 				console.log("¡Error de variables...!");
 			}
 		}
 	}
	 $.fn.costo_venta_tbl=function(control){//calcula costos tabla de colores en el producto
	 	if(control===undefined){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				costo_venta_tbl($(this));
	 			}
	 		});
	 		$(this).keyup(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				costo_venta_tbl($(this));
	 			}
	 		});
	 		$(this).on("mousewheel",function(e){
	 			if($(this).prop("tagName")!="FORM"){
	 				e.preventDefault();
	 			}
	 		});
	 	}else{
	 		costo_venta_tbl($(this));
	 	}
 		function costo_venta_tbl(e){
 			if(e.data("tbl")!=undefined){
 				var t_cantidad=0;
 				var t_costo=0;
 				var t_venta=0;
 				var table=e.data("tbl");
 				if(table.indexOf("table#")==-1 && table.indexOf("#")==-1){table="table#"+table;}
 				$(table+" tbody tr.row-producto").each(function(idx,tr){
 					if($(tr).attr("id")!=undefined){
 						var o_can=$("tr#"+$(tr).attr("id")+" input[id*=cantidad]");
 						var o_cu=$("tr#"+$(tr).attr("id")+" input[id*=cu]");
 						var o_ct=$("tr#"+$(tr).attr("id")+" input[id*=ct]");
 						var o_st=$("tr#"+$(tr).attr("id")+" input[id*=st]");
 						var o_costo_venta=$("tr#"+$(tr).attr("id")+" input[id*=costo_venta]")
 						if(o_can.length>0 && o_cu.data("value")!=undefined && o_ct.length>0 && o_st.length>0 && o_costo_venta.length>0){
 							var cu=o_cu.data("value")*1;
 							var cantidad=0;
 							o_can.each(function(idx2,input){
 								var c=($(input).val()*1)+"";
 								if(entero(c,0,9999) && c>0){cantidad+=(c*1);}
 							});
 							o_ct.val(cantidad);
 							o_st.val(number_format((cantidad*cu),1,'.',''));
 							o_costo_venta.val(number_format((cantidad*cu),1,'.',''));
 							t_cantidad+=cantidad;
 							t_costo+=number_format((cantidad*cu),1,'.','');
 							t_venta+=number_format((cantidad*cu),1,'.','');
 						}
 					}
 				});
 				$(table+" thead tr.row-total th.sub_can_pro").html(t_cantidad);
 				$(table+" thead tr.row-total th.sub_cos_pro").html(t_costo);
 				$(table+" thead tr.row-total th.sub_tot_cos_pro").html(t_venta);
 				if($(table).data("badge")!=undefined){
 					var msj=" Total: "+t_cantidad+" unidad";
 					if(t_cantidad==0 || t_cantidad>1){msj+="es";}
 					if(t_cantidad>0){
	 					$($(table).data("badge")).attr("class","badge badge-inverse-primary");
	 				}else{
	 					$($(table).data("badge")).attr("class","badge badge-inverse-danger");
	 				}
 					$($(table).data("badge")).html(msj);
 				}
 				$(this).costo_venta("ready");
 			}
 		}
	}
	$.fn.costo_venta=function(control){//calcula costo total parte pedido
	 	if(control===undefined){
	 		$(this).click(function(){
	 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 				costo_venta($(this));
	 			}
	 		});
	 		$(this).keyup(function(){
	 			if($(this).prop("tagName")!="FORM"){
	 				costo_venta($(this));
	 			}
	 		});
	 		$(this).on("mousewheel",function(e){
	 			if($(this).prop("tagName")!="FORM"){
	 				e.preventDefault();
	 			}
	 		});
	 	}else{
	 		costo_venta($(this));
	 	}
	 	function costo_venta(e){
	 		var total=0;
	 		$("div#list_product input[id*=costo_venta]").each(function(idx,input){
	 			if($(input).val()!=undefined && $(input).val()!="" && ($(input).val()*1)>0){
	 				total+=$(input).val()*1;
	 			}
	 		});
	 		$("input#p_cos").val(total);
	 		if($("input#p_des").val()!=undefined && $("input#p_tot").val()!=undefined){
	 			var descuento=$("input#p_des").val()*1;
	 			var final=total-descuento;
	 			if(final>0){
	 				$("input#p_tot").val(final);
	 			}else{
	 				$("input#p_tot").val(0);
	 			}
	 		}
	 	}
	}
	$.fn.total_productos=function(id_table){//Calculando costo total de la fila
		if($(this).valida_input_tabla(id_table)){
			var sw=true;
			var totales=[];
			var cantidad=0;
			$("table#"+id_table+" tr.row-producto").each(function(idx,tr){
				var c=0;
				if(sw){
					var cw=false;
					$("tr#"+$(tr).attr("id")+" input").each(function(idx,input){ 
						if(cw){
							if($(input).val()!="" && $(input).val()!=null){
								totales[c]=parseFloat($(input).val())*1;
							}else{
								totales[c]=0; 
							}
							c++;
						}
						cw=true;
					});
					sw=false;		
				}else{
					var cw=false;
					$("tr#"+$(tr).attr("id")+" input").each(function(idx,input){
						if(cw){
							if($(input).val()!="" && $(input).val()!=null){
								totales[c]+=parseFloat($(input).val())*1;
							}
							c++;
						}
						cw=true;
					});
				}
			});
			sw=false;
			c=0;
			$("table#"+id_table+" tr.row-total th").each(function(idx,th){
				if(sw){
					var total=0;
					if(totales[c]!=undefined){
						total=totales[c];
					}else{
						if(!esNumero($(th).html())){ total=undefined; }
					}
					$(th).html(total);
	 				if($(th).attr('class')=='sub_can_pro'){//cantidad total de productos
	 					var padre=buscar_padre($("table#"+id_table),"accordion-panel");
	 					if(padre!=null){
	 						var label=$("#"+$(padre).attr("id")+" label.badge");
	 						if(label.length>0){
	 							if(total>0){
	 								$(label).attr("class","badge badge-inverse-primary");
	 							}else{
	 								$(label).attr("class","badge badge-inverse-danger");
	 							}
	 							var msj="Total: "+total+" unidad";
	 							if(totales[c]>1){ msj+="es";}
	 							$(label).html(msj);
	 						}
	 					}
	 				}
	 				c++;
	 			}
	 			sw=true;
	 		});
			$(this).total_costo_venta();	
		}else{
			console.log("Error en el formulario de los productos, verifique los datos.");
		}
	}
 	$.fn.valida_input_tabla=function(id_table){//valida que todas las filas tenganla misma cantidad de inputs
 		var control=true;
 		var sw=true;
 		var inputs=0;
 		$("table#"+id_table+" tr.row-producto").each(function(idx,tr){
 			var e=$("tr#"+$(tr).attr("id")+" input");
 			if(sw){
 				inputs=e.length;sw=false;
 			}else{
 				if(inputs!=e.length){
 					control=false;
 				}
 			}

 		});
 		if(inputs<0){ control=false; }
 		return control;
 	}
 	$.fn.colores_producto=function(){/*vista de categorias del producto del pedido*/
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			colores_producto($(this));
	 		}
	 	});
	 	function colores_producto(e){
	 		if(e.data("p")!=undefined && e.data("tbl")!=undefined){
	 			var tabla=$("table#"+e.data("tbl"));
	 			if(tabla.length>0){
	 				modal("PRODUCTOS: Adicionar color","sm","2");
	 				btn_modal('',"",'',"",'2');
	 				var v=[];
	 				var tipo="";
	 				$("table#"+e.data("tbl")+" tr.row-producto").each(function(idx,tr){ v[v.length]=$(tr).data("pgc");if($(tr).data("tipo")!=undefined){ tipo=$(tr).data("tipo")+"";}});
					if(tipo==""){if($("#p_tip").val()!=undefined){ if(($("#p_tip").val()+""=="0" || $("#p_tip").val()+""=="1" || $("#p_tip").val()+""=="2") && $("#p_tip").attr("disabled")==undefined){tipo=$("#p_tip").val()+"";}}}
					console.log("tipo: "+tipo);
	 				var atrib = new FormData();
	 				if(tipo!=""){atrib.append("tipo",tipo);}
	 				atrib.append('p',e.data("p"));
	 				atrib.append('pgcs',JSON.stringify(v));
	 				atrib.append("tbl",e.data("tbl"));
	 				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html",closed:false});
	 				e.get_1n('movimiento/colores_producto',atrib,controls);
 				}
 			}
 		}
 	}
	$.fn.add_producto_color=function(){/*adiciona categorias en el producto del pedido*/
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			add_producto_color($(this));
	 		}
	 	});
	 	function add_producto_color(e){
	 		if(e.data("pgc")!=undefined && $("div#datos_producto_add_color").data("tbl")!=undefined && $("div#datos_producto_add_color").data("p")!=undefined && $("div#list_product div[class*=accordion-panel]").length>0){
	 			var idcl="";
	 			var tipo="";
	 			$("div#list_product div[class*=accordion-panel]").each(function(index,div){
			 		if($(div).data("cl")!=undefined && $(div).data("tipo")!=undefined){ console.log("Entro"); idcl=$(div).data("cl")+"";tipo=$(div).data("tipo")+"";return false;}
			 	});
			 	if(idcl!="" && tipo!=""){	
			 		var adicionados=[];//catogorias de del prodcuto seleccionados
				 	$("table#"+$("div#datos_producto_add_color").data("tbl")+" tr.row-producto").each(function(idx,ele){ adicionados[adicionados.length]=$(ele).data("pgc");});
				 	adicionados[adicionados.length]=e.data("pgc");
				 	var atrib = new FormData();
				 	atrib.append('pgc',e.data("pgc"));
				 	atrib.append('p',$("div#datos_producto_add_color").data("p"));
				 	atrib.append('tbl',$("div#datos_producto_add_color").data("tbl"));
					atrib.append('pgcs',JSON.stringify(adicionados));
			 		atrib.append("cl",idcl);
			 		atrib.append("tipo",tipo);
			 		if($("#datos_parte_pedido").data("pp")!=undefined){ atrib.append('pp',$("#datos_parte_pedido").data("pp"));}
					//var acordion=buscar_padre($("table#"+$("div#datos_producto_add_color").data("tbl")),);
					console.log($("table#"+$("div#datos_producto_add_color").data("tbl")).data("accordion"));
				 	var controls1=JSON.stringify({id:$($("table#"+$("div#datos_producto_add_color").data("tbl")).data("accordion")).attr("id"),refresh:false,type:"append",child:"tbody",closed:false,callback:JSON.stringify({c:"num_prod_ped",c2:"disabled_input_pedido"})});
				 	var controls2=JSON.stringify({id:"content_2",refresh:false,type:"html",closed:false});
				 	e.get_2n('movimiento/add_producto_color',atrib,controls1,'movimiento/colores_producto',atrib,controls2);
			 	}else{
			 		console.log("Error de variables");
			 	}
	 		}
	 	}
	}
	$.fn.view_producto=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			view_producto($(this));
	 		}
	 	});
	 	function view_producto(e){
		 	if(e.data("type")!=undefined){
		 		var idcl="";
		 		var control=true;
		 		if(e.data("type")=="new_parte"){
		 			var tipo="";
		 			if($("div#list_product div[class*=accordion-panel]").length>0){
						$("div#list_product div[class*=accordion-panel]").each(function(index,div){
			 				if($(div).data("tipo")=="0" || $(div).data("tipo")=="1" || $(div).data("tipo")=="2"){tipo=$(div).data("tipo")+""; return false;}
			 			});
		 			}else{
		 				if($("#p_tip").val()=="0" || $("#p_tip").val()=="1" || $("#p_tip").val()=="2"){tipo=$("#p_tip").val();}
		 			}

		 			if(tipo==""){alerta("Seleccione un tipo válido","top","p_tip");control=false;}
		 			if(e.data("cl")!=undefined){idcl=e.data("cl");}else{control=false;}
		 		}
		 		if(e.data("type")=="new"){
		 			if($("div#list_product div[class*=accordion-panel]").length>0){
		 				$("div#list_product div[class*=accordion-panel]").each(function(index,div){
			 				if($(div).data("cl")!=undefined){console.log("Entro");idcl=$(div).data("cl");return false;}
			 			});
		 			}else{
		 				if(entero($("select#p_cli").val(),0,10)){
		 					idcl=$("select#p_cli").val();
		 				}else{
		 					alerta("Seleccione un cliente","top","p_cli");
		 					control=false;
		 				}
		 			}
		 		}
		 		//console.log("Control "+control+" cliente "+idcl);
		 		if(control && idcl!=""){
			 		var text="";
			 		if(e.data("type")=="new" || e.data("type")=="new_parte"){text="Adicionar";}
			 		if(e.data("type")=="new_change"){text="Cambiar";}
					var pa=[];//productos asignados en el pedido
					$("div.list-group-item div#list_product div.accordion-content").each(function(index,div){
						if($(div).data("pr")!=undefined){ pa[pa.length]=$(div).data("pr");}
					});
			 		modal("PRODUCTOS: "+text,"md","2");
			 		btn_modal('',"",'',"",'2');
					var atrib=new FormData();
			 		atrib.append("type",e.data("type"));
					atrib.append("pa",JSON.stringify(pa));
					if(e.data("type")=="new_parte"){if(control){
						atrib.append("tipo",tipo);
					}}
					atrib.append("cl",idcl);
					//if(e.data("type")=="new_change"){atrib.append('elemento',e.data("elemento"));}
					//if(e.data("element-padre")!=undefined){atrib.append('element-padre',e.data("element-padre"));}
					//if(e.data("container")!=undefined){atrib.append('container',e.data("container"));}
				 	var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					var controls2=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
				 	e.get_2n('movimiento/search_producto',atrib,controls,'movimiento/view_producto',atrib,controls2);
		 		}
			}
	 	}
	}
 	$.fn.search_producto=function(){
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
	 			if($(this).data("type")!=undefined){
	 				if($(this).data("type")=="all"){
	 					$(this).reset_input("");
	 				}
	 				search_producto($(this));
	 			}
	 		}
	 	});
	 	$(this).change(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			search_producto($(this));
	 		}
	 	});
	    $(this).submit(function(event){
	    	search_producto($(this));
	    	event.preventDefault();
	    });
	    function search_producto(e){
 			var atrib=new FormData();
 			atrib.append('nom',$("#s2_nom").val());
 			atrib.append('type',$("#s2_nom").data("type"));
			var pa=[];//productos asignados en el pedido
			$("div.list-group-item div#list_product div.accordion-content").each(function(index,div){
				if($(div).data("pr")!=undefined){ pa[pa.length]=$(div).data("pr"); }
			});
			atrib.append("pa",JSON.stringify(pa));
			var controls=JSON.stringify({id:"contenido_2",refresh:true,type:"html"});
 			e.get_1n('movimiento/view_producto',atrib,controls);
	    }
	}
 	$.fn.add_producto=function(){	
	 	$(this).click(function(){
	 		if($(this).prop("tagName")!="FORM"){
	 			add_producto($(this));
	 		}
	 	});
	 	function add_producto(e){
	 		if(e.data("p")!=undefined && $("input#s2_nom").data("type")!=undefined && $("input#s2_nom").data("cl")!=undefined){
	 			var control=true;
	 			if($("input#s2_nom").data("type")=="new_parte"){
	 				var tipo=$("input#s2_nom").data("tipo");

	 				if(tipo!="0" && tipo!="1" && tipo!="2"){control=false;}
	 			}
	 			if(control){
					var atrib=new FormData();
					atrib.append("p",e.data("p"));
					atrib.append("type",$("input#s2_nom").data("type"));
					atrib.append("cl",$("input#s2_nom").data("cl"));
	 				if($("input#s2_nom").data("type")=="new_parte"){if(control){
	 					//console.log(tipo);
						atrib.append("tipo",tipo);
					}}
					var controls1=JSON.stringify({id:"list_product",refresh:false,type:"append",child:false,closed:false,callback:JSON.stringify({c:"renumber_items_productos",c2:"disabled_input_pedido"})});
		 			e.get_1n('movimiento/add_producto',atrib,controls1);
	 			}
				//e.get_1n('movimiento/change_producto',atrib,controls1);
	 		}
	 	}
	}

	/*$.fn.total_costo_venta=function(){
		var sw=false;
		var total=0;
		var control=$("#list_product .accordion-panel tr.row-total th.sub_tot_cos_pro");
		if(control.length>0){
			$("#list_product .accordion-panel tr.row-total th.sub_tot_cos_pro").each(function(idx,th){
				total+=(parseFloat($(th).html())*1);
			});
		}else{
			if($("#list_product").html()!=undefined){
				$("#p_cos").val(0);
			}
			total=$("#p_cos").val();
		}
		total*=1;
		$("#p_cos").val(total);
		if((total-($("#p_des").val()*1))>0){
			$("#p_tot").val((total-($("#p_des").val()*1)));
		}else{
			$("#p_tot").val(0);
		}
	}*/
 	/*---- End librerias ----*/
/*------- END MANEJO DE PEDIDOS -------*/
/*------- MANEJO DE COMPRAS -------*/

	$.fn.blur_all=function(id){
		if(id!="s_cod"){ OnBlur("s_cod"); }
		if(id!="s_nom"){ OnBlur("s_nom"); }
		if(id!="s_can"){ OnBlur("s_can"); }
	}
   	/*--- Buscador ---*/
	$.fn.view_compras=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input("");
 					}
 					view_compras($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_compras($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_compras($(this));
	    	event.preventDefault();
	    });
 		function view_compras(e){
		 	var atrib3=new FormData();
	   		atrib3.append("pro",$("#s1_pro").val());
	   		atrib3.append("fe1",$("#s1_fe1").val());
	   		atrib3.append("fe2",$("#s1_fe2").val());
	   		atrib3.append("mes",$("#s1_mes").val());
			var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
	 		$(this).get_1n('movimiento/view_compra',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
 	/*--- Reportes ---*/
 	$.fn.reporte_compra=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				reporte_compra($(this));
 			}
 		});
 		function reporte_compra(e){
 			if(e.data("c")!=undefined){
		 		modal("Detalle de compra",'md','1');
				btn_modal('',"",'',"",'1');
				var atrib=new FormData();
				atrib.append('c',e.data("c"));
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('movimiento/detalle_compra',atrib,controls);
 			}
 		}
 	}
 	/*--- End reportes ---*/
 	/*--- Configurar ---*/
 	$.fn.config_compra=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				config_compra($(this));
 			}
 		});
 		function config_compra(e){
 			if(e.data("c")!=undefined){
		 		modal("Modificar de compra",'xmd','1');
		 		var atr="this|"+JSON.stringify({c:e.data("c")});
				btn_modal('update_compra',atr,'',"",'1');
				var atrib=new FormData();
				atrib.append('c',e.data("c"));
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('movimiento/config_compra',atrib,controls);
 			}
 		}
 	}
 	$.fn.update_compra=function(){
 		if($(this).data("c")!=undefined){
 			var num_doc=$("input#f1_num_doc").val().trim();
 			var fec=$("input#f1_fec").val();
 			var mat=$("div#f1_mat div.card-group").data("mi");
 			var cos=$("input#f1_cos").val().trim();
 			var can=$("input#f1_can").val().trim();
 			var uni=$("select#f1_uni").val().trim();
 			var pro=$("select#f1_pro").val().trim();
 			var des=$("textarea#f1_des").val().trim();
			if(mat!=undefined){
				if(decimal(cos,9,1) && cos>=0 && cos<=99999999.9){
					if(decimal(can,9,2) && can>0 && can<=99999999.99){
						if(entero(uni,0,10)){
							var control=true;
							if(num_doc!="" || num_doc!="0"){if(!entero(num_doc,0,15)){alerta("Ingrese un número de documento válido","top","f1_num_doc");control=false;}}
							if(pro!=""){if(!entero(pro,0,10)){alerta("Ingrese un valor válido","top","f1_pro"); control=false;}}
							if(des!=""){if(!textarea(des,0,700)){alerta("Ingrese un valor válido","top","f1_des"); control=false;}}
							if(control){
								var atrib=new FormData();
								atrib.append('idc',$(this).data("c"));
								atrib.append('num_doc',num_doc);
								atrib.append('fec',fec);
								atrib.append('mat',mat);
								atrib.append('can',can);
								atrib.append('cos',cos);
								atrib.append('uni',uni);
								atrib.append('pro',pro);
								atrib.append('des',des);
								var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
	 							var atrib3=new FormData();
						   		atrib3.append("pro",$("#s1_pro").val());
						   		atrib3.append("fe1",$("#s1_fe1").val());
						   		atrib3.append("fe2",$("#s1_fe2").val());
						   		atrib3.append("mes",$("#s1_mes").val());
	 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 							$(this).set('movimiento/update_compra',atrib,controls1,'movimiento/view_compra',atrib3,controls2);
							}
						}else{
							alerta("Seleccione una unidad válida","top","f1_uni");
						}
					}else{
						alerta("Ingrese una cantidad válida","top","f1_can");
					}
				}else{
					alerta("Ingrese un costo válido","top","f1_cos");
				}
			}else{
				alerta("Seleccione un material","top","f1_mat");	
			}
 		}
 	}
 	$.fn.confirm_compra=function(idc){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				confirm_compra($(this));
 			}
 		});
 		function confirm_compra(e){
 			if(e.data("c")!=undefined){
		   		modal("Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({c:e.data("c")});
		   		btn_modal('drop_compra',atr,'',"","5");
		   		var atrib=new FormData();
		   		atrib.append('c',e.data("c"));
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('movimiento/confirmar_compra',atrib,controls);
 			}
 		}
 	}
 	$.fn.drop_compra=function(){
 		if($(this).data("c")!=undefined){
	   		var atrib=new FormData();
	   		atrib.append('c',$(this).data("c"));
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
 			var atrib3=new FormData();
			atrib3.append("pro",$("#s1_pro").val());
			atrib3.append("fe1",$("#s1_fe1").val());
			atrib3.append("fe2",$("#s1_fe2").val());
			atrib3.append("mes",$("#s1_mes").val());
 			var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
 			$(this).set('movimiento/drop_compra',atrib,controls1,'movimiento/view_compra',atrib3,controls2);
 		}
 	}
 	/*--- End Configurar ---*/
   	/*--- Nuevo ---*/
    $.fn.new_compra=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_compra($(this));
 			}
 		});
 		function new_compra(e){
 			if(e.data("type")!=undefined){
 				modal("Nueva compra",'xmd','1');
 				var atr="this|"+JSON.stringify({type:e.data("type")});
				btn_modal('save_compra',atr,'',"",'1');
				var atrib=new FormData();
				atrib.append('type',e.data("type"));
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('movimiento/new_compra',atrib,controls);
 			}
 		}
	}
	$.fn.view_materiales=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_materiales($(this));
 			}
 		});
 		function view_materiales(e){
 			if(e.data("pro")!=undefined && e.data("uni")!=undefined){
 				modal("Materiales: seleccionar",'sm','2');
				btn_modal('',"",'',"",'2');
				var atrib=new FormData();
				atrib.append("container",e.attr("id"));
				atrib.append("pro",e.data("pro"));
				atrib.append("uni",e.data("uni"));
				var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
				$(this).get_1n('movimiento/view_materiales',atrib,controls);
 			}
 		}
	}
	$.fn.add_material_compra=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				add_material_compra($(this));
 			}
 		});
 		function add_material_compra(e){
 			if(e.data("mi")!=undefined && $("#s2_mat").data("container")!=undefined && $("#s2_mat").data("container-unidad")!=undefined){
 				var atrib=new FormData();
 				atrib.append('mi',e.data("mi"));
				var controls=JSON.stringify({id:$("#s2_mat").data("container"),refresh:false,type:"html"});
				if($("#s2_mat").data("container-proveedor")!=undefined){
					var controls2=JSON.stringify({id:$("#s2_mat").data("container-unidad"),refresh:false,type:"html"});
					var controls3=JSON.stringify({id:$("#s2_mat").data("container-proveedor"),refresh:false,type:"html",closed:"2"});
					$(this).get_3n("movimiento/add_material_compra",atrib,controls,"movimiento/unidad_material",atrib,controls2,"movimiento/proveedor_material",atrib,controls3);
				}else{
					var controls2=JSON.stringify({id:$("#s2_mat").data("abr"),refresh:false,type:"html",closed:"2"});
					$(this).get_2n("movimiento/add_material_compra",atrib,controls,"movimiento/proveedor_material",atrib,controls2);
				}
 			}
 		}
	}
	/*--- Manejo de unidad de medida ---*/
	$.fn.new_unidad=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_unidad($(this));
 			}
 		});
		$(this).submit(function(event){
			new_unidad($(this));
			event.preventDefault();
		});
 		function new_unidad(e){
 			if(e.data("container")!=undefined){
	 			if(e.data("type")=="add"){
	 				var mat=$("div#f1_mat div.card-group").data("mi");
	 			}
	 			if(e.data("type")=="create"){
	 				var mat=$("input#f1_mat").val();
	 			}
	 			if(mat!=undefined){
	 				modal("Unidad: nuevo",'sm','2');
	 				var atr="this|"+JSON.stringify({container:e.data("container"),selected:$("#"+e.data("container")).val()});
					btn_modal('save_unidad',atr,'',"",'2');
					var atrib=new FormData();
					atrib.append("container",e.data("container"));
					atrib.append("type",e.data("type"));
					var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					e.get_1n('movimiento/new_unidad',atrib,controls);
	 			}else{
	 				alerta("Seleccione un material","top","f1_mat");
	 			}
 			}
 		}
	}
	$.fn.save_unidad=function(){
		if($(this).data("container")!=undefined && $(this).data("selected")!=undefined){
			var uni=$("input#f2_uni").val();
			var abr=$("input#f2_abr").val();
			if(strSpace(uni,2,40)){
				if(strSpace(abr,1,8)){
					var atrib=new FormData();
					atrib.append("uni",uni);
					atrib.append("abr",abr);
					atrib.append("selected",$(this).data("selected"));
					var controls1=JSON.stringify({type:"set",preload:true,closed:"2"});
 					var controls2=JSON.stringify({id:$(this).data("container"),refresh:false,type:"html"});
 					$(this).set('movimiento/save_unidad',atrib,controls1,'movimiento/option_unidades',atrib,controls2);
				}else{
					alerta("Ingrese un contenido válido","top","f2_abr");
				}
			}else{
				alerta("Ingrese un contenido válido","top","f2_uni");
			}
		}
	}
	$.fn.new_proveedor=function(){
		console.log("Entro0");
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				new_proveedor($(this));
 			}
 		});
 		function new_proveedor(e){
 			if(e.data("container")!=undefined){
	 			if(e.data("type")=="add"){
	 				var mat=$("div#f1_mat div.card-group").data("mi");
	 			}
	 			if(e.data("type")=="create"){
	 				var mat=$("input#f1_mat").val();
	 			}
	 			if(mat!=undefined){
	 				modal("Unidad: nuevo",'md','2');
	 				var atr="this|"+JSON.stringify({container:e.data("container")});
					btn_modal('save_proveedor',atr,'',"",'2');
					var atrib=new FormData();
					atrib.append("container",e.data("container"));
					atrib.append("type",e.data("type"));
					var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
					e.get_1n('movimiento/new_proveedor',atrib,controls);
	 			}else{
	 				alerta("Seleccione un material","top","f1_mat");
	 			}
 			}
 		}
	}
	$.fn.save_proveedor=function(){
		if($(this).data("container")!=undefined){
			var nit=$("input#f2_nit").val();
			var raz=$("input#f2_raz").val();
			if(nit==""){nit="0";}
			if(entero(nit,0,25)){
				if(strSpace(raz,2,100)){
					var atrib=new FormData();
					atrib.append("nit",nit);
					atrib.append("raz",raz);
					var controls1=JSON.stringify({type:"set",preload:true,closed:"2"});
 					var controls2=JSON.stringify({id:$(this).data("container"),refresh:false,type:"html"});
 					$(this).set('movimiento/save_proveedor',atrib,controls1,'movimiento/option_proveedores',{},controls2);
				}else{
					alerta("Ingrese un contenido válido","top","f2_raz");
				}
			}else{
				alerta("Ingrese un contenido válido","top","f2_nit");
			}
		}
	}
	/*--- End manejo de unidad de medida ---*/
    $.fn.save_compra=function(){
 		if($(this).data("type")!=undefined){
 			var num_doc=$("input#f1_num_doc").val().trim();
 			var fec=$("input#f1_fec").val();
 			var control=false;
 			if($(this).data("type")=="add"){
 				var mat=$("div#f1_mat div.card-group").data("mi");
 				if(entero(mat,0,10)){control=true;}
 			}
 			if($(this).data("type")=="create"){
 				var mat=$("input#f1_mat").val();
 				var fot=$("input#f1_fot").prop("files");
 				if(strSpace(mat,2,100)){control=true; console.log("Entro");}
 			}
 			var cos=$("input#f1_cos").val().trim();
 			var can=$("input#f1_can").val().trim();
 			var uni=$("select#f1_uni").val();
 			var pro=$("select#f1_pro").val();
 			var des=$("textarea#f1_des").val().trim();
			if(control){
				if(decimal(can,9,2) && can>0 && can<=99999999.99){
					if(entero(uni,0,10)){
						if(decimal(cos,9,1) && cos>=0 && cos<=99999999.9){
							var control=true;
							if(num_doc!="" || num_doc!="0"){if(!entero(num_doc,0,15)){alerta("Ingrese un número de documento válido","top","f1_num_doc");control=false;}}
							if(pro!=""){if(!entero(pro,0,10)){alerta("Ingrese un valor válido","top","f1_pro"); control=false;}}
							if(des!=""){if(!textarea(des,0,700)){alerta("Ingrese un valor válido","top","f1_des"); control=false;}}
							if(control){
								var atrib=new FormData();
								atrib.append('type',$(this).data("type"));
								atrib.append('num_doc',num_doc);
								atrib.append('fec',fec);
								if($(this).data("type")=="create"){
									atrib.append('archivo',fot[0]);
								}
								atrib.append('mat',mat);
								atrib.append('cos',cos);
								atrib.append('can',can);
								atrib.append('uni',uni);
								atrib.append('pro',pro);
								atrib.append('des',des);
								var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
	 							var atrib3=new FormData();
						   		atrib3.append("pro",$("#s1_pro").val());
						   		atrib3.append("fe1",$("#s1_fe1").val());
						   		atrib3.append("fe2",$("#s1_fe2").val());
						   		atrib3.append("mes",$("#s1_mes").val());
	 							var controls2=JSON.stringify({id:"contenido",refresh:false,type:"html"});
	 							$(this).set('movimiento/save_compra',atrib,controls1,'movimiento/view_compra',atrib3,controls2);
							}
						}else{
							alerta("Ingrese un costo válido","top","f1_cos");
						}
					}else{
						alerta("Seleccione una unidad de medída","top","f1_uni");
					}
				}else{
					alerta("Ingrese una cantidad válida","top","f1_can");
				}
			}else{
				if($(this).data("type")=="add"){
					alerta("Seleccione un material","top","f1_mat");
				}else{
					if($(this).data("type")=="create"){
						alerta("Ingrese un contenido válido","top","f1_mat");
					}else{
						msj("¡Error de variables!");
					}
				}
			}
 		}
	}
   	/*--- End nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_compras=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_compras($(this));
 			}
 		});
 		function print_compras(e){
 			if(e.data("tbl")!=undefined){
 				modal("COMPRAS: Configuración de impresión","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("c")!=undefined){
		 				visibles[visibles.length]=$(tr).data("c");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
				atrib.append('tbl',e.data("tbl"));
	 			atrib.append('pro',$("#s1_pro").val());
	 			atrib.append('fe1',$("#s1_fe1").val());
	 			atrib.append('fe2',$("#s1_fe2").val());
	 			atrib.append('mes',$("#s1_mes").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('movimiento/print_compras',atrib,controls);
 			}
 		}
   	}
   	/*--- End imprimir ---*/
/*------- END MANEJO DE COMPRAS -------*/
 	$.fn.drop_aux=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				drop_aux($(this));
 			}
 		});
 		function drop_aux(e){
	 		if(e.data("mi")!=undefined){
	 			var atrib=new FormData();
	 			atrib.append('mi',e.data("mi"));
				var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
				e.set('material_indirecto/drop_aux',atrib,controls1,'',{},null);
	 		}
 		}
 	}
})(jQuery);
function update_pedido(e){e.update_pedido();}
function drop_parte_pedido(e){e.drop_parte_pedido();}
function save_banco(e){e.save_banco();}
function update_banco(e){e.update_banco();}
function drop_banco(e){e.drop_banco();}
function save_cuenta(e){e.save_cuenta();}
function save_tipo_cambio(e){e.save_tipo_cambio();}
function update_tipo_cambio(e){e.update_tipo_cambio();}
function drop_tipo_cambio(e){e.drop_tipo_cambio();}
function save_pago(e){e.save_pago();}
function save_descuento(e){e.save_descuento();}
function update_descuento(e){e.update_descuento();}
function drop_descuento(e){e.drop_descuento();}
function update_pago(e){e.update_pago();}
function drop_pago(e){e.drop_pago();}
function update_parte_pedido(e){e.update_parte_pedido();}
function save_parte_pedido(e){e.save_parte_pedido();}
function drop_pedido(e){e.drop_pedido();}
function save_pedido(e){e.attr("disabled","disabled");e.save_pedido();}
function save_unidad(e){e.save_unidad();}
function save_proveedor(e){e.save_proveedor();}
function update_compra(e){e.update_compra();}
function drop_compra(e){e.drop_compra();}
function save_compra(e){e.save_compra();}
function update_descuento_producto(e){e.update_descuento_producto();}
function save_detalle_descuento(e){e.save_detalle_descuento();}
function update_detalle_descuento(e){e.update_detalle_descuento();}
function drop_detalle_descuento(e){e.drop_detalle_descuento();}
function drop_descuento_producto(e){e.drop_descuento_producto();}
function inicio(){
 	$('div#pedido').click(function(){$(this).get_2n('movimiento/search_pedido',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'movimiento/view_pedido',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("pedido","Pedidos","movimiento?p=1");});
 	$('a#pedido').click(function(){$(this).get_2n('movimiento/search_pedido',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'movimiento/view_pedido',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("pedido","Pedidos","movimiento?p=1");});
 	$('div#compra').click(function(){
		$(this).get_2n('movimiento/search_compra',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'movimiento/view_compra',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("compra","Compras","movimiento?p=3");
 	});
 	$('a#compra').click(function(){
 		$(this).get_2n('movimiento/search_compra',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'movimiento/view_compra',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("compra","Compras","movimiento?p=3");
 	});
 }
	 /*--- End Buscador ---*/
	 /*--- Ver Todo ---*/
	 /*--- End Ver Todo ---*/
	 /*--- Nuevo ---*/

 	/*--- End Nuevo ---*/

   	/*--- Pagos ---
   	--- End Pagos ---*/


   	/*--- End Eliminar ---*/
   	/*------- END MANEJO DE PEDIDO -------*/
