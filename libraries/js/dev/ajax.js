/*! ajax.min.js v2.0 | (c) 2017 | @utor: Gonzalo Mamani Guachalla | email:gmg.software.developer@gmail.com */
var base;
function init(b){base=b;}
var stack_intervals=[];
(function($){
  $.fn.get_1n=function(ruta,atrib,controls){
    if(ruta!=""){
      $(this).attr("disabled","disabled");
      var e=$(this);
      $.ajax({
        url:ruta,
        async:true,
        type: "POST",
        contentType:false,
        data:atrib,
        processData:false, /*Debe estar en false para que JQuery no procese los datos a enviar*/
        timeout:300000,/*5min*/
        beforeSend: function(){e.enviar(controls);},
        success: function(result){e.recibir(result,controls);},
        error:function(){e.error_envio(controls);}
      }); 
    }
  }
    $.fn.extend({
      get_2n: function(ruta1,atrib1,controls1,ruta2,atrib2,controls2){
        if(ruta1!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta1,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib1,
              processData:false,
              timeout:300000,
              beforeSend: function(){ e.enviar(controls1);e.enviar(controls2);},
              success: function(result){e.recibir_2n(result,controls1,ruta2,atrib2,controls2);},
              error: function(){e.error_envio(controls1);}
          });
        }else{
          var datos=JSON.parse(controls1);
          $("#"+datos.id).css("display","none");
          $(this).get_1n(ruta2,atrib2,controls2);
        }
        return false;
      }
    });
    $.fn.get_3n=function(ruta1,atrib1,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3){
        if(ruta1!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta1,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib1,
              processData:false,
              timeout:300000,
              beforeSend: function(){ e.enviar(controls1);},
              success: function(result){e.recibir_3n(result,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3);},
              error: function(){e.error_envio(controls1);}
          });
        }else{
          $(this).get_1n(ruta2,atrib2,controls2);
        }
    }
    $.fn.extend({
      set_2n: function(ruta,atrib,controls,ruta1,atrib1,controls1,ruta2,atrib2,controls2){
        if(ruta!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib,
              processData:false,
              timeout:600000,/*10min*/
              beforeSend: function(){ e.enviar(controls);e.enviar(controls1);},
              success: function(result){ e.recibir_set_2n(result,controls,ruta1,atrib1,controls1,ruta2,atrib2,controls2);},
              error: function(){ e.error_envio(controls1);}
          });
        }else{
          $(this).get_1n(ruta2,atrib2,controls2);
        }
        return false;
      }
    });
    $.fn.extend({
      set: function(ruta,atrib,controls,ruta1,atrib1,controls1){
        if(ruta!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib,
              processData:false, 
              timeout:600000,
              beforeSend: function(){ e.enviar(controls);},
              success: function(result){ e.recibir_set(result,controls,ruta1,atrib1,controls1);},
              error: function(){ e.error_envio(controls1);}
          });
        }else{
          $(this).get_1n(ruta2,atrib2,controls2);
        }
        return false;
      }
    });
    $.fn.set_3n=function(ruta,atrib,controls,ruta1,atrib1,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3){
        if(ruta!=""){
          $(this).attr("disabled","disabled");
          var e=$(this);
          $.ajax({
              url:ruta,
              async:true,
              type: "POST",
              contentType:false,
              data:atrib,
              processData:false,
              timeout:600000,/*10min*/
              beforeSend: function(){ e.enviar(controls);},
              success: function(result){ e.recibir_set_3n(result,controls,ruta1,atrib1,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3);},
              error: function(){ e.error_envio(controls1);}
          });
        }else{
          $(this).get_1n(ruta2,atrib2,controls2);
        }
    }
  /*---- PARA BOTON CIRCULAR ----*/
  $.fn.set_button=function(ruta,atrib,controls,ruta2,atrib2,controls2){
    var e=$(this);
    $.ajax({
      url:ruta,
      async:true,
      type: "POST",
      contentType:false,
      data:atrib,
      processData:false,
      timeout:300000,
      beforeSend: function(){$(this).enviar(controls);},
      success: function(v_resp){ 
        if(isJSON(v_resp)){
          var JSONdata = JSON.parse(v_resp);
          try{
            send(v_resp);
          }catch(e){
            console.log(e);
          }
          v_resp=JSONdata[0].result;
        }
        resp=v_resp.split("|");
        switch(resp[1]){
          case "ok": e.color_button(e,resp[0]);msj("ok");if(ruta2!=""){ $(this).get_1n(ruta2,atrib2,controls2);} break;
          case "error": msj(resp[1]); break;
          case "fail": msj(resp[1]); break;
          case "permiso_bloqueado": msj(resp[1]); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base;  break;
          default: msj('default'); $("#alerta_text").text(resp[1]);
        }
      },
      error:function(){problemas(id)}
    }); 
  }
  /*---- END PARA BOTON CIRCULAR ----*/
    $.fn.extend({
      enviar: function(controls){
        var js=JSON.parse(controls);
        if(js.type!=undefined){
          if(js.type=="set"){
            if(js.preload!=undefined){
              if(js.preload){
                msj("load");
              }
            }
          }else{
                if(js.preload!=undefined){
                  if(js.preload){
                    msj("load");
                  }
                }
            if(js.refresh){
              if(js.id=="contenido"){
                  var content=$("#contenido").html();
                  if(content.toLowerCase().indexOf("preload-content")==-1){
                    var alto=$(document).height()-10;
                    var ancho=$("#contenido").width(); 
                    $("#contenido").html("<div id='preload-content'>"+content+"</div><div id='preload'><i class='fa fa-circle-o-notch'></i></div>");
                    $("#preload").css('height',alto+'px');
                    $("#preload").css('width',ancho+'px');
                    $("#preload").css('top','0px');
                  }
              }else{
                if(js.id=="search"){
                  $("#search").html("<div id='preload-2'><center><i class='fa fa-circle-o-notch'></i></center></div>");
                }else{
                  if(js.id=="content_1" || js.id=="content_11"){
                    $("#preload-modal").css("display","inline");
                  }else{
                    $("#"+js.id).html("<img src='"+base+"libraries/img/sistema/load.gif'>"); 
                  }
                }
              }
            }
          }
        }
      }
    });
    $.fn.extend({
      recibir: function(resultado,controls){
        var e=$(this);
        var js=JSON.parse(controls);
        if(resultado!="logout"){
          e.removeAttr("disabled");
          if(js.type=="html"){ $("#"+js.id).html(resultado); }
          if(js.type=="append"){
            if(resultado!="fail" && resultado!="error"){
              if(js.child){
                $("#"+js.id+" "+js.child).append(resultado);
              }else{
                $("#"+js.id).append(resultado);
              }
            }else{
              msj(resultado);
            }
          }
          if(js.type=="value"){ $("#"+js.id).val(resultado); }
          if(js.type=="form_value"){
            $(this).valores_formulario(js.id,resultado);
          }
          if(js.closed!=undefined){
            if(js.closed){
              hide_modal(js.closed);
            }
          }
          if(js.disableds!=undefined){
            var disableds=js.disableds.split(",");
            if(disableds.length>0){
              for(var i = 0; i < disableds.length; i++){
                //console.log(disableds[i]);
                $(disableds[i]).attr("disabled","disabled");
              }
            }
          }
          if(js.callback!=undefined){
            var callbacks=JSON.parse(js.callback);
            $.each(callbacks, function(i, callback) {
              switch(callback){
                case "renumber_items_productos": e.renumber_items("div#list_product div.accordion-panel a.accordion-msg div.item");/*renumerar productos*/ break;
                case "disabled_input_pedido": e.disabled_input_pedido();/*bloquea o habilita select*/ break;
                case "costo_venta": e.costo_venta("ready");break;
                case "refresh_elements":
                  //console.log(e.data("padre"));
                  var padre=e.buscar_padre();
                  //console.log(padre);
                  if(e.data("container")!=undefined && padre!=null){
                    var eliminados=[];
                    $("#"+padre.attr("id")+" .row-producto").each(function(idx,tr){
                      if($(tr).data("dp")!=undefined){
                        eliminados[eliminados.length]=$(tr).data("dp");
                      }
                    });
                    e.stack_elements(eliminados,e.data("container"),"1","dp");
                  }
                break;
                case "refresh_stack_pedido_productos":
                  if(e.data("container")!=undefined && $("div#list_product")!=undefined){
                    var detalles=[];
                    $("div#list_product table tr td span[id*=detalles-producto-]").each(function(id,span){
                      var producto=JSON.parse($(span).html());
                      $.each(producto, function(i, producto){
                        detalles[detalles.length]={"dp":producto.dp,"status":producto.status};
                      });
                    });
                    $(e.data("container")).html(JSON.stringify(detalles));
                  }
                break;
                case "pila_productos": 
                  var ids=[]; 
                  var padre=buscar_padre(e,"accordion-panel");
                  if(padre!=null){
                    if($("span#detalle").html()!=undefined){
                      
                    }else{
                      if($("span#detalles-pro-emp").html()!=undefined){

                      }
                    }
                    pila_productos(ids,"0");
                  }
                break;
                case "renumber-productos-n1": $("div#list_product").renumber_producto_empleado({padre:"list_product",type:"atr",item:"div.item.item-1"}); break;
                case "renumber-productos-n2": $("div#"+js.id).renumber_producto_empleado({padre:js.id,type:"atr",item:"div.item.item-2"}); break;
                case "total_costo_venta": $(this).costo_venta("ready"); break;
                case "costo_venta_tbl": e.costo_venta_tbl("ready");
              }
            });
          }
          if($(this).data("success")!=undefined){/*caso para botones*/
            if($(this).data("success")=="disabled"){
              if($(this).data("class")!=undefined){
                $(this).removeAttr("class");
                $(this).attr("class",$(this).data("class"));
                clear_all_data($(this));
                $(this).attr("disabled","disabled");
              }
            }
          }
          $("#preload-modal").css("display","none");
          if($("#alerta").attr('class')=="alert alert-loading"){
            $("#alerta").css("display","none");
          }
        }else{
          alert("Su Session a expirado, inicie session por favor"); 
          window.location.href=base;
        }
      }
    });
    $.fn.extend({
      recibir_2n: function(resultado,control_this,ruta,atrib,controls){
        var js=JSON.parse(control_this);
        if(resultado!="logout"){
          if(js.id=="search"){$("#search").removeAttr("style");}
          if(js.type=="html"){ $("#"+js.id).html(resultado); }
          if(js.type=="append"){
            if(js.child){
              $("#"+js.id+" "+js.child).append(resultado); 
            }  
          }
          if(js.callback!=undefined){
            var callbacks=JSON.parse(js.callback);
            $.each(callbacks, function(i, callback) {
              switch(callback){
                case "num_prod_ped": $(this).num_prod_ped();/*numeracion automatica*/ break;
                case "disabled_input_pedido": $(this).disabled_input_pedido();/*bloquea o habilita select*/ break;
                case "total_costo_venta": $(this).costo_venta("ready"); break;
                case "pila_productos": pila_productos(ids,"0"); break;
              }
            });
          }
          $("#preload-modal").css("display","none");

          $(this).get_1n(ruta,atrib,controls);
        }else{
          alert("Su Session a expirado, inicie session por favor"); 
          window.location.href=base;
        }
      }
    });
    $.fn.recibir_3n=function(resultado,control_this,ruta1,atrib1,controls1,ruta2,atrib2,controls2){
        var js=JSON.parse(control_this);
        if(resultado!="logout"){
          if(js.type=="html"){ $("#"+js.id).html(resultado); }
          if(js.type=="append"){
            if(js.child){
              $("#"+js.id+" "+js.child).append(resultado); 
            }  
          }
          if(js.callback!=undefined){
            var callbacks=JSON.parse(js.callback);
            $.each(callbacks, function(i, callback) {
              switch(callback){
                case "num_prod_ped": $(this).num_prod_ped();/*numeracion automatica*/ break;
                case "disabled_input_pedido": $(this).disabled_input_pedido();/*bloquea o habilita select*/ break;
                case "total_costo_venta": $(this).costo_venta("ready"); break;
                case "pila_productos": pila_productos(ids,"0"); break;
              }
            });
          }
          $("#preload-modal").css("display","none");

          $(this).get_2n(ruta1,atrib1,controls1,ruta2,atrib2,controls2);
        }else{
          alert("Su Session a expirado, inicie session por favor"); 
          window.location.href=base;
        }
    }
    $.fn.extend({
      error_envio: function(controls){
        $(this).removeAttr("disabled");
        msj("problem");$("#preload-modal").css("display","none");
      }
    });
    $.fn.extend({
      recibir_set: function(resultado,control_this,ruta1,atrib1,controls1){
        var e=this;
        if(isJSON(resultado)){
          var JSONdata = JSON.parse(resultado);
          try{
            send(resultado);
          }catch(e){
            console.log(e);
          }
          resultado=JSONdata[0].result;
        }
        var js=JSON.parse(control_this);
        switch(resultado){
          case "ok": msj(resultado); 
                    if(js.closed!="NULL" && js.closed!=""  && js.closed!=undefined){ hide_modal(js.closed);}; 
                    if(js.callback!=undefined){
                      var callbacks=JSON.parse(js.callback);
                      $.each(callbacks, function(i, callback){
                        if(js.callback!=undefined){
                          var callbacks=JSON.parse(js.callback);
                          $.each(callbacks, function(i, callback){
                            switch(callback){
                              case "costo_promedio": 
                                if($(e).data("p")!=undefined){ $(e).costo_promedio();} break;
                            }
                          });
                        }
                      });
                    }
                    if(ruta1!=''){ $(this).get_1n(ruta1,atrib1,controls1);}else{ $(this).removeAttr("disabled");}
          break;
          case "remove": $(e).drop_this_elemento(); break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
          default: msj(resultado); $(this).removeAttr("disabled"); break;
        }
      }
    });
    $.fn.extend({
      recibir_set_2n: function(resultado,control_this,ruta1,atrib1,controls1,ruta2,atrib2,controls2){
        if(isJSON(resultado)){
          var JSONdata = JSON.parse(resultado);
          try{
            send(resultado);
          }catch(e){
            console.log(e);
          }
          resultado=JSONdata[0].result;
        }
        var js=JSON.parse(control_this);
        switch(resultado){
          case "ok": msj(resultado); 
                    if(js.closed!="NULL" && js.closed!=""  && js.closed!=undefined){ hide_modal(js.closed);}; 
                    if(ruta1!=''){ $(this).get_2n(ruta1,atrib1,controls1,ruta2,atrib2,controls2);}
          break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
          default: msj(resultado); $(this).removeAttr("disabled"); break;
        }
      }
    });
    $.fn.extend({
      recibir_set_3n: function(resultado,control_this,ruta1,atrib1,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3){
        if(isJSON(resultado)){
          var JSONdata = JSON.parse(resultado);
          try{
            send(resultado);
          }catch(e){
            console.log(e);
          }
          resultado=JSONdata[0].result;
        }
        var js=JSON.parse(control_this);
        switch(resultado){
          case "ok": msj(resultado); 
                    if(js.closed!="NULL" && js.closed!=""  && js.closed!=undefined){ hide_modal(js.closed);}; 
                    if(ruta1!=''){ $(this).get_3n(ruta1,atrib1,controls1,ruta2,atrib2,controls2,ruta3,atrib3,controls3);}
          break;
          case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
          default: msj(resultado); $(this).removeAttr("disabled"); break;
        }
      }
    });
  $.fn.cronometro=function(options){
    var fini="";
    var ffin="";
    var settings={'inicio': null,'fin': null,'id': null, 'format_dia': null, 'ac': null};
    if(options){ $.extend(settings, options);}
      function cronometro_proc(settings){
        if(fini==""){ fini = Date.parse(settings['inicio'])/1000;}
        if(ffin==""){ ffin = Date.parse(settings['fin'])/1000;}
        if(ffin < fini){
          var atrib=new FormData();atrib.append('ac',settings['ac']);
          $.ajax({
            url:"controls/iniciar_actualizacion",
            async:true,
            type: "POST",
            contentType:false,
            data:atrib,
            processData:false,
            timeout:30000,
            beforeSend: function(){},
            success: function(result){
              if(result!="logout" && result!="error" && result!="fail"){
                if(result=="actualizar"){
                  window.location.href=base;
                }else{
                  var v=result.split("|");
                  fini=Date.parse(v[0])/1000;
                  ffin=Date.parse(v[1])/1000;
                  cronometro_proc(settings);
                }
              }else{
                msj(result);
              }
            },
            error:function(){$(this).error_envio(null);}
          });
        }else{
          seconds = ffin - fini;
          days = Math.floor(seconds/(60*60*24));
          seconds-=days*60*60*24;
          hours=Math.floor(seconds/(60*60));
          seconds-=hours*60*60;
          minutes=Math.floor(seconds/60);
          seconds-=minutes*60;
          if(days<=0){ $(settings['id']).find(".dias").css("display","none");$(settings['id']).find(".text-dia").css("display","none");}else{$(settings['id']).find(".dias").css("display","visible");$(settings['id']).find(".text-dia").css("display","visible"); }
          if(days==1){$(settings['id']).find(".text-dia").text("dia"); }else {$(settings['id']).find(".text-dia").text("dias"); }
          if(hours==1){$(settings['id']).find(".text-hora").text("hora"); }else {$(settings['id']).find(".text-hora").text("horas"); }
          if(minutes==1){$(settings['id']).find(".text-minuto").text("minuto"); }else {$(settings['id']).find(".text-minuto").text("minutos"); }
          if(seconds==1){$(settings['id']).find(".text-segundo").text("segundo"); }else {$(settings['id']).find(".text-segundo").text("segundos"); }
           if(settings['format_dia']==true){
              days = (String(days).length >= 2) ? days : "0" + days;
            }
            hours = (String(hours).length >= 2) ? hours : "0" + hours;
            minutes = (String(minutes).length >= 2) ? minutes : "0" + minutes;
            seconds = (String(seconds).length >= 2) ? seconds : "0" + seconds;
          if(!isNaN(ffin)){
            $(settings['id']).find(".dias").text(days);
            $(settings['id']).find(".horas").text(hours);
            $(settings['id']).find(".minutos").text(minutes);
            $(settings['id']).find(".segundos").text(seconds);
            if((days=="0" || days=="00") && hours=="00" && (minutes*1)<=1){
              msj("refresh");
              var msj_1="minuto";
              var msj_2="segundo";
              if(minutes!=1){ msj_1+="s";}
              if(seconds!=1){ msj_2+="s";}
              $("#alerta_sistema #hora_alerta").text(minutes+" "+msj_1+" "+seconds+" "+msj_2);
            }else{
              
            }
          }else {
            console.log("Fecha inválida");
            clearInterval(interval);
          }
          var fecha = new Date(fini*1000);
          fecha.setSeconds(fecha.getSeconds()+1);
          fini=Math.floor(Date.parse(fecha)/1000);
        }
      }
      cronometro_proc(settings);
      interval = setInterval(function(){ cronometro_proc(settings);}, 1000);
      stack_intervals.push(interval);
    }
    $.fn.extend({
      clear_all_intervals: function(){
        for(var i=0; i < stack_intervals.length; i++){ clearInterval(stack_intervals[i]);}
      }
    });
    $.fn.extend({
      all_updates: function(){
        modal("ACTUALIZACIÓNES","xmd","1");
        btn_modal('',"",'',"",'1');
        var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
        $(this).get_1n('controls/all_updates',{},controls);
      }
    });
  $.fn.visor=function(){
    $(this).click(function(){
      if($(this).prop("tagName")!="FORM"){
        visor($(this));
      }
    });
    function visor(e){
        var type_view="";
        if(e.data("title")!=undefined && e.data("desc")!=undefined){
          type_view="this";
        }else{
          if(e.data("type")!=undefined && e.data("id")!=undefined){
            type_view="db";
          }
        }
        if(type_view!=""){
          modal("",null,"6");
          $("#content_6").html("");
          if(type_view=="this"){
            if(e.attr("src")!=undefined){
              var url=e.attr("src");
              url=url.replace("/miniatura","");
              var titulo=e.data("title");
              var desc=e.data("desc");
              var content='<div class="container-fluid" style="padding:0px;"><div id="my-pics" class="carousel slide" data-ride="carousel" style="width:100%;margin:auto;"><div class="carousel-inner" role="listbox"><div class="carousel-item active"><div class="modal-header sub-modal-header"><span><span id="title-img">'+titulo+'</span><br><span id="desc-img">'+desc+'</span></span></div><img src="'+url+'" alt="" width="100%"></div></div></div></div>';
              $("#content_6").html(content);
            }
          }
          if(type_view=="db"){
              var atrib=new FormData();
              atrib.append("id",e.data("id"));
              atrib.append("type",e.data("type"));
              var controls=JSON.stringify({id:"content_6",refresh:false,type:"html"});
              e.get_1n('controls/view_img',atrib,controls);
          }
        }
    }
  }
})(jQuery);
function get(ruta,atrib,id,control){
  $.ajax({
  	url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){ llegada(id,result)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
	if(ruta1!=""){
		Visible(id1);
		$.ajax({
		    url:ruta1,
		    async:true,
		    type: "POST",
		    contentType:false,
		    data:atrib1,
		    processData:false,
		    timeout:9000,
		    beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
		    success: function(result){llegada_2n(id1,result,ruta2,atrib2,id2,control2)},
		    error: function(){problemas(id1)}
		});
	}else{
		Disabled(id1);
		get(ruta2,atrib2,id2,control2);
	}
  return false;
}
function get_3n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3){
  $.ajax({
    url:ruta1,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib1,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control1){ inicioEnvio(id1);}},
    success: function(result){llegada_3n(id1,result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3)},
    error:function(){problemas(id1)}
  });
  return false;
}
function get_input(ruta,atrib,id,inputs,control){//en uso en: capital humano
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){llegada_input(inputs,result,true)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_input_v2(ruta,atrib,id,inputs,control){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){ llegada_input(inputs,result,false)},
    error:function(){problemas(id)}
  }); 
  return false;
}
function get_append(ruta,atrib,id,control){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id)} },
    success: function(result){ llegada_append(id,result);},
    error:function(){ problemas(id)}
  }); 
  return false;
}
function get_append_disabled(ruta,atrib,id,control,ele){/* desactiva un elemeto mientras carga la respuesta*/
  $(ele).attr('disabled','disabled');
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){ 
      $(ele).removeAttr("disabled");
      llegada_append(id,result)
      if($(ele).data("success")!=undefined){
        if($(ele).data("success")=="hidden"){
          $(ele).remove();
        }
      }
    },
    error:function(){  $(ele).removeAttr("disabled"); problemas(id)}
  }); 
  return false;
}
function get_disabled(ruta,atrib,id,control,ele){
  $(ele).attr('disabled','disabled');
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio(id);}},
    success: function(result){
      $(ele).removeAttr("disabled"); 
      llegada(id,result);
      if($(ele).data("success")!=undefined){
        if($(ele).data("success")=="hidden"){
          $(ele).remove();
        }
      }
    },
    error:function(){$(ele).removeAttr("disabled"); problemas(id)}
  }); 
  return false;
}
function get_array(ruta,atrib,array,control){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ if(control){ inicioEnvio("")} },
    success: function(results){ llegada_array(array,results)},
    error:function(){ problemas("")}
  }); 
  return false;
}

function inicioEnvio(div){
  if(div=="contenido"){
      var content=$("#"+div).html();
      var alto=$(this).height()-60;
      var ancho=$("#"+div).width();
      $("#"+div).html("<div id='preload-content'>"+content+"</div><div id='preload'><i class='fa fa-circle-o-notch'></i></div>");
      $("#preload").css('height',alto+'px');
      $("#preload").css('width',ancho+'px');
      $("#preload").css('top','50px');
  }else{
    if(div=="search"){
      $("#search").html("<div id='preload-2'><center><i class='fa fa-circle-o-notch'></i></center></div>");
    }else{
      if(div=="content_1" || div=="content_11"){
        $("#preload-modal").css("display","inline");
      }else{
        $("#"+div).html("<img src='"+base+"libraries/img/sistema/load.gif'>"); 
      }
    }
  }
}
function llegada(div,result){ 
	if(result!="logout"){
		$("#"+div).html(result);
    $("#preload-modal").css("display","none");
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_2n(div,result,ruta,atrib,id,control){
	if(result!="logout"){ 
		$("#"+div).html(result+"");
		get(ruta,atrib,id,control);
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_3n(div,result,ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2){
	if(result!="logout"){ 
		$("#"+div).html(result);
		get_2n(ruta1,atrib1,id1,control1,ruta2,atrib2,id2,control2);
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_input(inputs,result,disabled){
  var inp=inputs.split("|");
	if(result!="logout"){ 
		if(result!="fail"){ 
			if(result!=""){
				var res=result.split("|");
				for(var i=0;i<res.length;i++){
					var ele=$("#"+inp[i]);
					if(ele.prop("tagName")=='INPUT' || ele[0].tagName=='TEXTAREA'){
            if(ele.attr("type")!="file"){
              ele.val(res[i]);
            }
					}else{
						if(ele.prop("tagName")=='SELECT'){
							ele.html("");
							ele.html(res[i]);
						}else{
							ele.html("");
							ele.html(res[i]);
						}
					}
					if(disabled){ ele.attr({'disabled':'disabled'});}
				}
			}else{
				for(var i=0;i<inp.length;i++){
					var ele=$("#"+inp[i]);
					
					if((ele[0].tagName).toUpperCase()=='INPUT' || (ele[0].tagName).toUpperCase()=='TEXTAREA'){
            if($(ele).attr("disabled")=="disabled"){
              ele.val("");
            }
					}else{
						if((ele[0].tagName).toUpperCase()=='SELECT'){}
						else{
              if($(ele).attr("disabled")=="disabled"){
							 ele.html("...");
              }
						}
					}
          if(disabled){ ele.removeAttr('disabled');}
				}
			}
		}else{
			msj(result);
		}

	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_append(div,result){
	if(result!="logout"){
		if(result!="fail"){
			$("#"+div).append(result);
      if(div=="list_product"){ $("#p_cli").attr("disabled","disabled");}
			try{
				refresh_totales_compras();/*solo para el caso de actualizar compras (nuevo y modificar);*/
			}catch(e){}
			try{
				refresh_totales_cuentas();/*solo para el caso de actualizar cuenta (nuevo y modificar);*/
			}catch(e){}
		}else{
			msj(result)
		}
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function llegada_array(array,results){
	if(results!="logout"){
		if(results!="fail"){
			var vt=(array.types).split("|");
			var vd=(array.divs).split("|");
			var vr=results.split("|");
			if(vt.length==vd.length && vd.length==vr.length){
				for(var i = 0; i < vt.length; i++){
					if(vt[i]=="div"){ $("#"+vd[i]).html(vr[i]); }
					if(vt[i]=="input"){ $("#"+vd[i]).val(vr[i]); }
				}
			}else{
				msj("fail "+vt.length+" "+vd.length+" "+vr.length);
			}
		}else{
			msj(results);
		}
	}else{
		alert("Su Session a expirado, inicie session por favor"); 
		window.location.href=base;
	}
}
function problemas(div){ msj("problem");  $("#preload-modal").css("display","none");}

/*--- funciones de set de datos ---*/
function set(ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:300000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_set(result,ruta2,atrib2,id2,control2,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function set_disabled(ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed,ele){/*decativa el elemento mientras se carag*/
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:300000,
    beforeSend: function(){ ele.attr("disabled","disabled"); if(control){ msj('load');}},
    success: function(result){ ele.removeAttr("disabled");llegada_set(result,ruta2,atrib2,id2,control2,id_closed)},
    error:function(){ ele.removeAttr("disabled"); problemas(id);}
  }); 
}
function llegada_set(result,ruta,atrib,id,control,id_closed){
  if(isJSON(result)){
    var JSONdata = JSON.parse(result);
    try{
      send(result);
    }catch(e){
      console.log(e);
    }
    result=JSONdata[0].result;
  }
  switch(result){
    case "ok": msj(result); if(id_closed!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get(ruta,atrib,id,control);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  }
}
function set_2n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false, 
    timeout:300000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_2n_set(result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_2n_set(result,ruta,atrib,id,control,ruta2,atrib2,id2,control2,id_closed){
  if(isJSON(result)){
    var JSONdata = JSON.parse(result);
    try{
      send(result);
    }catch(e){
      console.log(e);
    }
    result=JSONdata[0].result;
  }
  switch(result){
    case "ok": msj(result); if(id!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get_2n(ruta,atrib,id,control,ruta2,atrib2,id2,control2);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  }
}
function set_3n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,ruta4,atrib4,id4,control4,id_closed){
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:300000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ llegada_3n_set(result,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,ruta4,atrib4,id4,control4,id_closed)},
    error:function(){problemas(id)}
  }); 
}
function llegada_3n_set(result,ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3,id_closed){
  if(isJSON(result)){
    var JSONdata = JSON.parse(result);
    try{
      send(result);
    }catch(e){
      console.log(e);
    }
    result=JSONdata[0].result;
  }
  switch(result){
    case "ok": msj(result); if(id!="NULL"){ hide_modal(id_closed);}; if(ruta!=''){ get_3n(ruta,atrib,id,control,ruta2,atrib2,id2,control2,ruta3,atrib3,id3,control3);}break;
    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj(result); break;
  } 
}

function set_id(ruta,atrib,id,control){/* en uso movimiento de materiales*/
  $.ajax({
    url:ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:300000,
    beforeSend: function(){ if(control){ msj('load');}},
    success: function(result){ var res=result.split("|"); llegada_set_id(res[0],res[1],id);},
    error:function(){problemas(id)}
  }); 
}
function llegada_set_id(result,valor,id) {
	switch(result){
	    case "ok": msj(result); reset_rev_mov(valor,id); break;
	    case "error": msj(result); break;
	    case "fail": msj(result); break;
	    case "logout": alert("Su Session a expirado, inicie session por favor"); window.location.href=base; break;
    default: msj('default'); $("#alerta_text").text(result); break;
  }
}

/*manejo de las alertas*/
var menu=document.querySelector("#alert_seg");
menu.onclick= function(){
  if($(this).attr('aria-expanded')!="true"){
    get(base+'controls/view_nav_alerts',{},'content_alert_seg',true);
  }
}
/*manejo de las alertas*/
var menu=document.querySelector("#alert_prod");
menu.onclick= function(){
  if($(this).attr('aria-expanded')!="true"){
    console.log("Entro");
    get(base+'controls/view_nav_produccion',{},'content_alert_prod',true);
  }
}
/*manejo de las alertas actulizacion de sistema*/
var menu=document.querySelector("#refresh_version");
menu.onclick= function(){
  if($(this).attr('aria-expanded')!="true"){
    get(base+'controls/nav_refresh_version',{},'content_refresh_version',true);
  }
}
function user_alters(e){
  var atrib=new FormData();atrib.append('b',e.dataset.b);
  modal('Alertas','lg','1');
  btn_modal('',"",'',"",'1','lg');
  $("#content_1").html("");
  get(base+'controls/user_alters',atrib,'content_1',true);
}
function refresh_notifications(ruta,atrib,id){
  $.ajax({
    url:base+ruta,
    async:true,
    type: "POST",
    contentType:false,
    data:atrib,
    processData:false,
    timeout:9000,
    beforeSend: function(){ },
    success: function(result){ if(result>0){ $("#"+id).attr({ 'class': 'badge badge-danger header-badge'}); }else{ $("#"+id).attr({ 'class': 'badge badge-info header-badge'}); }$("#"+id).html(result);  },
    error:function(){problemas(id)}
  }); 
  return false;
}
function all_user_alters(){
    modal('Alertas','lg','1');
    btn_modal('',"",'',"",'1','lg');
    $("#content_1").html("");
    get(base+'controls/all_user_alters',{},'content_1',true);
}
function search_user_alerts(e){
  if(e.value!=""){
    var atrib=new FormData(); atrib.append('b',e.value);
    get(base+'controls/user_alters',atrib,'content_11',true);
  }else{
    $("#content_11").html("");
  }
}
function user_msj(e){
    setTimeout(function() {
      if($("#showChat_inner").css("display")=="block"){
        var atrib=new FormData();atrib.append('a',e.dataset.a);atrib.append('c','1');
        get_2n(base+'controls/user_form_msj',atrib,"content_msj",false,base+'controls/user_msj',atrib,'msgs',true);
      }else{
        $("#content_msj").html("");$("#showChat_inner .chat-inner-header span").html("");
      }
    }, 501);
}
function return_chat(){
  setTimeout(function() {
          if($("#showChat_inner").css("display")!="block"){
            $("#content_msj").html("");$("#showChat_inner .chat-inner-header span").html("");

          }
        }, 550);
  refresh_mensajes_user();
}
function send_mensaje(){
  var atrib=new FormData();
  if(strSpace($("#msj").val(),1,900)){
    atrib.append('msj',$("#msj").val());atrib.append('a',document.querySelector("#msj").dataset.r);
    $.ajax({
      url:base+"controls/send_mensaje",
      async:true,
      type: "POST",
      contentType:false,
      data:atrib,
      processData:false,
      timeout:9000,
      beforeSend: function(){},
      success: function(result){
        send(result);
        get(base+'controls/user_msj',atrib,'msgs',false);
        $("#msj").val("");
      },
      error: function(){problemas("error")}
    }); 
  }else{
    msj("Ingrese un mensaje de 1 a 900 caracteres, solo se aceptan los caracteres especiales (áÁéÉíÍóÓúÚñÑ+-.,:°;ªº).");
  }
  return false;
}
function refresh_mensajes_user(){
            var atrib=new FormData();
            $.ajax({
              url:base+"controls/refresh_mensajes_user",
              async:true,
              type: "POST",
              contentType:false,
              data:atrib,
              processData:false,
              timeout:9000,
              beforeSend: function(){},
              success: function(result){
                var JSONdata    = JSON.parse(result); 
                for(var i = 0; i < JSONdata.length; i++){
                  if(JSONdata[i].msj>0){
                    $("#badge"+JSONdata[i].idus).attr("class","badge badge-danger header-badge");
                    $("#badge"+JSONdata[i].idus).html(JSONdata[i].msj+"");
                  }else{
                    $("#badge"+JSONdata[i].idus).removeAttr("class");
                    $("#badge"+JSONdata[i].idus).html("");
                  }
                  if(JSONdata[i].msj_ingreso!=""){
                    $("small#control-active"+JSONdata[i].idus).html(JSONdata[i].msj_ingreso);
                  }
                };
              },
              error:function(){problemas("error")}
            }); 
            return false;
}
function actualiza_alerta_msj(id){
  $.ajax({
    url:base+"controls/actualiza_alerta_msj",
    async:true,
    type: "POST",
    contentType:false,
    data:{},
    processData:false,
     timeout:9000,
    beforeSend: function(){},
    success: function(result){
      var ele=$("#"+id);
      ele.html(result);
      if(result>0){
        ele.attr('class','badge badge-danger header-badge');
      }else{
        ele.attr('class','badge badge-info header-badge');
      }
    },
    error:function(){problemas("error")}
  }); 
  return false;
}
function actualiza_alerta_notificacion(){
  $.ajax({
    url:base+"controls/actualiza_alerta_notificacion",
    async:true,
    type: "POST",
    contentType:false,
    data:{},
    processData:false,
     timeout:9000,
    beforeSend: function(){},
    success: function(result){
      var ele=$("#badge_content");
      ele.html(result);
      if(result>0){
        ele.attr('class','badge badge-danger header-badge');
      }else{
        ele.attr('class','badge badge-info header-badge');
      }
    },
    error:function(){problemas("error")}
  }); 
  return false;
}
function actualiza_alerta_update(){
  $.ajax({
    url:base+"controls/actualiza_alerta_update",
    async:true,
    type: "POST",
    contentType:false,
    data:{},
    processData:false,
     timeout:9000,
    beforeSend: function(){},
    success: function(result){
      var ele=$("#badge_update");
      ele.html(result);
      if(result>0){
        console.log("Entro");
        ele.attr('class','badge badge-danger blink header-badge');
      }else{
        ele.attr('class','badge badge-info header-badge');
      }
    },
    error:function(){problemas("error")}
  }); 
  return false;
}
function isJSON(str){
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
$("#my_history").click(function(event) {
    modal('Historial de movimientos','lg','1');
    btn_modal('',"",'',"",'1');
    $("#content_1").html("");
    get(base+"controls/my_history",{},'content_1',true);
});
$("#my_config_user").click(function(event) {
    modal('Modificar','md','1');
    $("#content_1").html("");
    btn_modal('my_update_usuario',"",'',"",'1','md');
    get(base+"controls/my_config_usuario",{},'content_1',true);
});
  function my_update_usuario(){
    var fot=$('#n_fot').prop('files');
    var ci=$('#n_ci').val();
    var ciudad=$('#n_ciu').val();
    var nom=$('#n_nom1').val();
    var nom2=$('#n_nom2').val();
    var pat=$('#n_pat').val();
    var mat=$('#n_mat').val();
    var tel=$('#n_tel').val();
    var usu=$('#n_usu').val();
    var car=$('#n_car').val();
    if(entero(ci,6,9) && ci>=100000 && ci<=999999999){
      if(entero(ciudad,0,10)){
        if(strSpace(nom,2,20)){
          if(strNoSpace(usu,4,15)){
            var control=true;
            if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
            if(pat!=""){ if(!strSpace(pat,2,20)){ control=false; alerta('Ingrese un apellido válido',"top",'n_pat');}}
            if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese Apellido Paterno válido',"top",'n_mat');}}
            if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
            if(tel!=""){ if(!entero(tel,0,15)){ control=false; alerta('Ingrese un número de telefono válido',"top",'n_tel');}}
            if(control){
              var atribs= new FormData();
              atribs.append("ci",ci);
              atribs.append("ciu",ciudad);
              atribs.append("nom1",nom);
              atribs.append("nom2",nom2);
              atribs.append("pat",pat);
              atribs.append("mat",mat);
              atribs.append("tel",tel);
              atribs.append("usu",usu);
              atribs.append("car",car);
              atribs.append("archivo",fot[0]);
              
              $.ajax({
                url:base+'controls/my_update_usuario',
                async:true,
                type: "POST",
                contentType:false,
                data:atribs,
                processData:false,
                timeout:240000,
                beforeSend: function(){ },
                success: function(result){ if(result=="ok"){window.location.href=base;}else{msj(result);}},
                error:function(e){msj(e);}
              }); 
            }
          }else{
            alerta('Ingrese un valor válido',"top",'n_usu');
          }
        }else{
          alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
        }
      }else{
        alerta('Seleccione una ciudad','top','n_ciu');
      }
    }else{
      alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
    }
    return false;
  }
$("#my_pass_user").click(function(event) {
    modal('Modificar','xs','1');
    btn_modal('cambiar_password',"",'',"",'1','xs');
    $("#content_1").html("");
    get(base+"controls/form_cambiar_password",{},'content_1',true);
});

function cambiar_password(){
  var pass=$("#e_pass").val();
  var nuevo=$("#new_pass").val();
  var nuevo2=$("#rep_pass").val();
  if(pass!=""){
    if(password(nuevo,4,25)){
      if(nuevo==nuevo2){
        var atrib=new FormData();atrib.append('pass',pass);atrib.append('nuevo',nuevo);atrib.append('nuevo2',nuevo2);
        set(base+'controls/cambiar_password',atrib,"",true,"",{},"","","1");
      }else{
        alerta("Las contraseñas no son iguales","top","new_pass");
        alerta("Las contraseñas no son iguales","top","rep_pass");
      }
    }else{
      alerta("Ingrese una contraseña válida","top","new_pass");
    }
  }else{
    alerta("Ingrese una contraseña","top","e_pass");
  }
}
function view_img_db(e){
  modal("",null,"6");
  var atrib=new FormData();atrib.append("id",e.data("id"));atrib.append("type",e.data("type"));
  $("#content_6").html("");
  get(base+'controls/view_img',atrib,'content_6',false);
}