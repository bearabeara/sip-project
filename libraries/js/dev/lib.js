$(function(){ $("#side-menu").MenuIzq(); });
$(document).ready(function(){
  window.onscroll = function (){ $("span.bootstrap-maxlength.label").remove();}
  document.getElementById("modal_11").onscroll = function (){ $("span.bootstrap-maxlength.label").remove();}
  document.getElementById("modal_1").onscroll = function (){ $("span.bootstrap-maxlength.label").remove();}
  document.getElementById("modal_2").onscroll = function (){ $("span.bootstrap-maxlength.label").remove();}
  document.getElementById("modal_3").onscroll = function (){ $("span.bootstrap-maxlength.label").remove();}
});
(function($){
  var nombrePluging="MenuIzq";
  defaults={toggle:true};
  function Menu(elemento,opciones){
    this.elemento=elemento;
    this.settings=$.extend({},defaults,opciones);
    this.inicio();
  }
  Menu.prototype={
    inicio: function(){
      var $this=$(this.elemento);
      var $toggle=this.settings.toggle;
      $this.find("li.active").has("ul").children("ul").addClass("collapse in");
      $this.find("li").not(".active").has("ul").children("ul").addClass("collapse");
      $this.find("li").has("ul").children("a").on("click", function (e) {
        e.preventDefault();
        $(this).parent("li").toggleClass("active").children("ul").collapse("toggle");
        if ($toggle) {
          $(this).parent("li").siblings().removeClass("active").children("ul.in").collapse("hide");
        }
      });
    },
  };
  $.fn[nombrePluging]=function(opciones){
    return this.each(function(){  
      if (!$.data(this, "plugin_" + nombrePluging)) {
        $.data(this, "plugin_" + nombrePluging, new Menu(this, opciones));
      }
    });
  }
  $.fn.num_prod_ped=function(){//numeracion de las filas de productos en pedido
    var c1=1;
    $("div#list_product div.accordion-panel").each(function(idx,e){
      if($(e).attr("id")!=undefined){
        $("div#"+$(e).attr("id")+" a div.item").html(c1);c1++;
        var c2=1;
        $("div#"+$(e).attr("id")+" table tr.row-producto div.item").each(function(idx2,e2){
          $(e2).html(c2);c2++;
        });
      }
    });
  }
  $.fn.renumber_items=function(ele,child){//numeracion de los items de una tabla
    var c1=1;
    $(ele).each(function(idx,e){$(e).html(c1);c1++;});
  }
  $.fn.renumber_table=function(id_table){//numeracion de los items de una tabla
    if($("table#"+id_table).html()!=undefined){
      var cont=1;
      $("table#"+id_table+" tr div.item").each(function(idx,item){
        $(item).html(cont++);
      });
    }
  }
  $.fn.extend({//numeracion de las filas de productos en pedido
    disabled_input_pedido: function(){
      var elementos=0;
      $("div#list_product div.accordion-panel table tr.row-producto").each(function(idx,e){
        elementos++;
      });
      if(elementos>0){
        $("select#p_cli").attr("disabled","disabled");
        $("select#p_tip").attr("disabled","disabled");
      }else{
        $("select#p_cli").removeAttr("disabled");
        $("select#p_tip").removeAttr("disabled");   
      }
    }
  });
  $.fn.print=function(){
    if($(this).data("area")!=undefined && $(this).data("tools")){
      var settings={'area': $(this).data("area"),'tools': $(this).data("tools")};
      var frame1;
      function imprimir(){
         window.frames["frame1"].focus();
         window.frames["frame1"].print();
         frame1.remove();
      }
      function armar(){
          if($(settings['area']).html()!=undefined){
            var pagina="letter portrait";
            var top="1";
            var right="1";
            var bottom="1";
            var left="2";
            if($(settings['tools']+" input#margin-top")!=undefined && $(settings['tools']+" input#margin-rigth")!=undefined && $(settings['tools']+" input#margin-bottom")!=undefined && $(settings['tools']+" input#margin-left")!=undefined){
              if($(settings['tools']+" input#margin-top").data("default")!=undefined && $(settings['tools']+" input#margin-rigth").data("default")!=undefined && $(settings['tools']+" input#margin-bottom").data("default")!=undefined && $(settings['tools']+" input#margin-left").data("default")!=undefined){
                top=$(settings['tools']+" input#margin-top").data("default")*1;
                right=$(settings['tools']+" input#margin-rigth").data("default")*1;
                bottom=$(settings['tools']+" input#margin-bottom").data("default")*1;
                left=$(settings['tools']+" input#margin-left").data("default")*1;
              }
              if($(settings['tools']+" input#margin-top").val()*1>0){
                top=$(settings['tools']+" input#margin-top").val()*1;
              }
              if($(settings['tools']+" input#margin-rigth").val()*1>0){
                right=$(settings['tools']+" input#margin-rigth").val()*1;
              }
              if($(settings['tools']+" input#margin-bottom").val()*1>0){
                bottom=$(settings['tools']+" input#margin-bottom").val()*1;
              }
              if($(settings['tools']+" input#margin-left").val()*1>0){
                left=$(settings['tools']+" input#margin-left").val()*1;
              }
            }
            if($(settings['tools']+" #text-page-print").attr("name")!=undefined){
              switch($(settings['tools']+" #text-page-print").attr("name")+""){
                case "0": pagina="letter portrait";break;
                case "1": pagina="letter landspace";break;
                case "2": pagina="legal portrait"; bottom+=2.54; break;
                case "3": pagina="legal landspace"; left+=2.54; break;
              }
            }
            var contents = $(settings['area']).html();
            var css='<link rel="stylesheet" type="text/css" href="'+base+'libraries/css/final/imprimir.min.css?v=8" media="print">';
            css+="<style>@page{ size: "+pagina+"; counter-increment: pagina;margin-top: "+top+"cm;margin-right: "+right+"cm;margin-bottom: "+bottom+"cm;margin-left: "+left+"cm;}</style>";

            frame1 = $('<iframe/>');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head>'+css+'</head><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
          }
      }
      armar();
      setTimeout(function(){imprimir();}, 600);
    }else{
      console.log("Error print...")
    }
  }
  /*--- para impresion ---*/
  $.fn.option_page_print=function(){
    if($(this).attr("id")!=undefined && $(this).data("value")!=undefined && $(this).data("text")!=undefined && $("#text-page-print").val()!=undefined && $("img#img-page-print").attr("src")!=undefined){
      $("#text-page-print").html($(this).data("text"));
      $("#text-page-print").attr("name",$(this).data("value"));
      if($("#"+$(this).attr("id")+" img").attr("src")!=undefined){
        $("img#img-page-print").attr("src",$("#"+$(this).attr("id")+" img").attr("src"));
      }
      var div=$(this).buscar_padre();
      if(div!=null){
        if(div.attr("id")!=undefined){
          var activo=$(this).attr("id");
          $("div#"+div.attr("id")+" a.dropdown-item").each(function(id,a){
            if($(a).attr("id")==activo){
              $(a).attr("class","dropdown-item option-page active");
            }else{
              $(a).attr("class","dropdown-item option-page");
            }
          });
        }
      }
    }
  }
  $.fn.drop_elemento=function(){
    $(this).click(function(){
      if($(this).prop("tagName")!="FORM"){
        drop_elemento($(this));
      }
    });
    function drop_elemento(e){
      var padre=e.buscar_padre();
      //console.log(padre)
      if(padre!=null){
        if(e.data("control")!=undefined){
            if(e.data("container")!=undefined){
              if(e.data("control")=="db-producto"){//productos en el pedido
                var eliminados=[];
                if(e.data("dp")!=undefined){
                  eliminados[eliminados.length]=e.data("dp");
                }else{
                  $("#"+padre.attr("id")+" .row-producto").each(function(idx,tr){
                    if($(tr).data("dp")!=undefined){
                      eliminados[eliminados.length]=$(tr).data("dp");
                    }
                  });
                }
                e.stack_elements(eliminados,e.data("container"),"0","dp");
                if(e.data("container-2")!=undefined){
                  e.stack_elements(eliminados,e.data("container-2"),"0","dp");
                }
              }
              if(e.data("control")=="db-proceso-empleado"){//procesos en el pedido
                var eliminados=[];
                eliminados[eliminados.length]=e.data("pre");
                e.stack_elements(eliminados,e.data("container"),"0","pre");
              }
              if(e.data("control")=="cuentas_directivo"){//cuentas en el empleado
                var eliminados=[];
                eliminados[eliminados.length]=e.data("bp");
                e.stack_elements(eliminados,e.data("container"),"0","bp");
              }
            }
        }
          padre.remove();
          if(e.data("module")=="producto_pedido"){/*Producto del pedido eliminado*/
            e.costo_venta("ready");//total costo de venta
            e.renumber_items("div#list_product div.accordion-panel a.accordion-msg div.item");//renumerar productos
            var products=$("div#list_product div.accordion-panel");
            if(products.length<=0){$("#p_tip").removeAttr("disabled");$("#p_cli").removeAttr("disabled");}//desbloquear tipo de parte de pedido
          }
          if(e.data("module")=="producto_color_pedido"){
            if(e.data("tbl")!=undefined){
              e.renumber_items("div#list_product "+e.data("tbl")+" div.item");//renumerar productos
              //console.log(e);
              e.costo_venta_tbl("ready");
            }
          }
      }
    }
  }
  $.fn.stack_elements=function(elementos,container,status,id){//lista de elementos
    var p=[];
    if($(container).html()!=undefined){
      var pila_elementos=JSON.parse($(container).html());
      for(var j = 0; j < pila_elementos.length; j++){
        var control=false;
        for(var i=0; i<elementos.length; i++){
          if(elementos[i]!=undefined){
            if(pila_elementos[j][id]==elementos[i]){ control=true; break; }
          }
        }
        if(control){
          if(id=="dp"){p[p.length]={"dp":pila_elementos[j][id],"status":status };}
          if(id=="bp"){p[p.length]={"bp":pila_elementos[j][id],"status":status };}
          if(id=="pre"){p[p.length]={"pre":pila_elementos[j][id],"status":status };}
        }else{
          if(id=="dp"){p[p.length]={"dp":pila_elementos[j][id],"status":pila_elementos[j]['status']};}
          if(id=="bp"){p[p.length]={"bp":pila_elementos[j][id],"status":pila_elementos[j]['status']};}
          if(id=="pre"){p[p.length]={"pre":pila_elementos[j][id],"status":pila_elementos[j]['status']};}
        }
      }
      $(container).html(JSON.stringify(p));
    }
  }
  $.fn.buscar_padre=function(a_padre,a_type_padre){
    var control=false;
    var resp=null;
    if($(this).data("padre")!=undefined && $(this).data("type-padre")!=undefined){
      var e=$(this);
      var padre=$(this).data("padre");
      if($(this).data("type-padre")=="atr"){
        if(e.attr("id")!=padre && e.attr("class")!=padre){
          for(var i = 0; i<=20; i++){
            e=e.parent();
            if(e.attr("id")==padre || e.attr("class")==padre){
              control=true;
              break;
            }
          }
        }else{
          control=true;
        }
      }
      if($(this).data("type-padre")=="tag"){
        if(e!=undefined){
          for (var i = 0; i <= 20; i++){
            e=e.parent();
            if(e.prop("tagName")!=undefined){
                if(minuscula(e.prop("tagName"))==minuscula(padre)){
                control=true;
                break;
              }
            }
          }
        }
      }
      if(control){
        resp=e;
      }else{
        resp=null;
      }
    }else{
      if($(this).data("id_padre")!=undefined){
        if($("#"+$(this).data("id_padre")).html()!=undefined){
          resp=$("#"+$(this).data("id_padre"));
        }
      }
    }
    return resp;
  }
  $.fn.tag_padre=function(tag){
    var e=$(this);
    var control=false;
    if(e!=undefined){
      for(var i = 0; i <= 20; i++){
        e=e.parent();
       // console.log(e.attr("class")+" "+tag);
        if(e.prop("tagName")!=undefined){
            if(minuscula(e.prop("tagName"))==minuscula(tag)){
            control=true;
            break;
          }
        }
      }
    }
    if(control){
      return e;
    }else{
      return null;
    }
  }
  $.fn.atr_padre=function(atr){
    var e=$(this);
    var control=false;
    if(e!=undefined){
      if(e.attr("id")!=atr && e.attr("class")!=atr){
        for(var i = 0; i<=20; i++){
          e=e.parent();
          if(e.attr("id")==atr || e.attr("class")==atr){
            control=true;
             break;
          }
        }
      }else{
        control=true;
      }
    }
    if(control){
      return e;
    }else{
      return null;
    }
  }
    function pila_productos(iddps,status){//actualiza el status de los productos
      var p=[];
      if($("span#detalles").html()!=undefined){
        var productos=JSON.parse($("span#detalles").html());
        for(var j = 0; j < productos.length; j++){
          var control=false;
          var eliminados=JSON.parse(iddps);
          for(var i = 0; i < eliminados.length; i++){
            if(eliminados[i]!=undefined){
              if(productos[j]['iddp']==eliminados[i]){ control=true; break; }
            }
          }
          if(control){
            p[p.length]={ "iddp":productos[j]['iddp'],"status":status };
          }else{
            p[p.length]={ "iddp":productos[j]['iddp'],"status":productos[j]['status'] };
          }
        }
        $("span#detalles").html(JSON.stringify(p));
      }
    }
    function pila_sub_productos(elemento,iddp,status){//actualiza el status de los productos
      if(elemento.html()!="[]" && elemento.html()!="" && elemento.html()!=undefined){
        var productos=JSON.parse(elemento.html());
        var p=[];
        for(var j = 0; j < productos.length; j++){           
          if(productos[j]['iddp']==iddp){
            p[p.length]={ "iddp":productos[j]['iddp'],"status":status };
          }else{
            p[p.length]={ "iddp":productos[j]['iddp'],"status":productos[j]['status'] };
          }
        }
        elemento.html(JSON.stringify(p));
      }
    }






  $.fn.search=function(){
    $(this).click(function(){
      if($(this).prop("tagName")=="A" || $(this).prop("tagName")=="BUTTON"){/**cuando se hace click en boton o enlace */
        if($(this).data("type")!=undefined){
          if($(this).data("type")=="table-material-modal"){
            search_table_material_modal($(this));
          }
        }
      }
    });
    $(this).keyup(function(){
      if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")=="INPUT"){
        if($(this).data("type")!=undefined){
          if($(this).data("type")=="table-material-modal" || $(this).data("type")=="table"){
            search_table_material_modal($(this));
          }
        }
      }
    });
    $(this).change(function(){
      if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")=="SELECT" || $(this).data("type")=="table"){
        if($(this).data("type")!=undefined){
          if($(this).data("type")=="table-material-modal"){
            search_table_material_modal($(this));
          }
        }
      }
    });
    function search_table_material_modal(e){
      if(e.data("table-search")!=undefined && e.data("column")!=undefined){
        if(e.prop("tagName")=="INPUT"){
          var valor=e.val().toLowerCase();
        }
        if(e.prop("tagName")=="SELECT"){
          var valor=$(e.data("material")).val().toLowerCase();
        }
        var text;
        var tr;
        var item=1;
        var children;
        $("table#"+e.data("table-search")+" tbody tr "+e.data("column")).each(function(idx,ele){
          $(ele).text($(ele).text().replace("<mark>",""));
          $(ele).text($(ele).text().replace("</mark>",""));
          text=$(ele).text().trim();
          tr=$(ele).tag_padre('tr');
          if(tr!=null){
            var a="";
            var control=true;
            if(e.prop("tagName")!="SELECT"){
              if(e.data("almacen")!=undefined){
                a=$(e.data("almacen")).val();
              }
            }else{
              a=e.val();
            }
            if(a!="" && $(ele).data("a")!=undefined){
              if(a!=$(ele).data("a")){ control=false;}
            }
            if(control){
                  if(text.toLowerCase().indexOf(valor)!==-1){
                      tr.removeAttr('style');
                      children=$(tr).children().children();
                      if($(children[0]).attr("class")=="item"){$(children[0]).html(item);item++;}
                      if(valor!=""){text=replace_all(text,valor,'mark')}
                      $(ele).html(text);
                  }else{
                      tr.css('display','none');
                  }
            }else{
              tr.css('display','none');
            }
          }
        });
      }
    }
  }
  $.fn.search_elements=function(){
    if($(this).data("container")!=undefined && $(this).data("compare")!=undefined && $(this).data("type-compare")!=undefined && $(this).data("col")!=undefined){
          var This=$(this);
          var container=$(This.data('container'));
          var search_element=This.val().toLowerCase()+"";
          if(container.prop("tagName")=="TABLE"){
            var item=1;
            var children;
            $(This.data("container")+" tbody tr td").each(function(idx,td){
              var content_td=$(td).html();
              if(content_td.indexOf("mark>")!==-1){
                $(td).html($(td).html().replace("<mark>",""));
                $(td).html($(td).html().replace("</mark>",""));
              }
            });
            $(This.data("container")+" tbody tr [data-col='"+This.data('col')+"']").each(function(idx,ele){
              var valor="";
              if(This.data("compare")=="text"){ valor=$(ele).text();}
              if(This.data("compare")=="data"){if(This.data("name-data")!=undefined){valor=$(ele).data(This.data("name-data"))+"";}}
              tr=$(ele).tag_padre('tr');
              tr.removeAttr('style');
              if(tr!=null && valor!=undefined  && valor!=null){
                valor=valor.trim();
                var control=false;
                switch(This.data("type-compare")){
                  case "like": if(valor.toLowerCase().indexOf(search_element)!==-1){control=true;} break;
                  case "equals": if(search_element!=""){if(valor.toLowerCase()==search_element){control=true;}}else{control=true;} break;
                }
                /**Verificar si se esta buscando de productos */
                
                if(This.data("module-search")=="productos"){
                  if($(ele).data("estado")!=undefined && $("select#s_est").val()!=undefined){
                    if($("select#s_est").val()!="2"){//Mostrar todos
                      if($(ele).data("estado")==$("select#s_est").val()){
                        if(control){
                          control=true;
                        }else{
                          if(search_element==""){
                            control=true;
                          }
                        }
                      }else{
                        control=false;
                      }
                    }
                  }
                }
                /**End verificar si se esta buscando de productos */
                if(control){
                  if(search_element!="" && search_element!=null && search_element!=undefined){
                    if(This.data("compare")=="text"){
                      valor=replace_all(valor,search_element,'mark');
                    }
                    if(This.data("compare")=="data"){
                      valor="<mark>"+$(ele).text()+"</mark>";
                    }
                  }else{
                    
                    if(This.data("compare")=="data"){
                      valor=$(ele).text();
                    }
                  }
                  $(ele).html(valor);
                  //numeracion
                  children=$(tr).children().children();
                  if($(children[0]).attr("class")=="item"){$(children[0]).html(item);item++;}
                }else{
                  tr.css('display','none');
                }
              }else{
                tr.css('display','none');
              }
            });
          }else{
          }
    }else{
      console.log("Error de variables.");
    }
  }
  $.fn.change_column_print=function(){/*Esconde las columnas de la tabla impresion*/
    $(this).change(function(){
      if($(this).prop("tagName")!="FORM"){
        change_column($(this));
      }
    });
    function change_column(e){
      if(e.data("column")!=undefined){
        var column=e.data("column");
        var control=true;
        if(e.data("column")==1 || e.data("column")==2){
          if($("#area table div.item").html()!=undefined){
            if($("input[data-column='1']").prop("checked")){
              if($("input[data-column='2']").prop("checked")){
                $("#area table div.item").removeAttr("style");
                $("#area table [data-column='2']").removeAttr("style");
                $("#area table [data-column='1']").css("display","none");
              }else{
                $("#area table div.item").removeAttr("style");
                $("#area table [data-column='2']").css("display","none");
                $("#area table [data-column='1']").removeAttr("style");
              }
              control=false;
            }else{
              if($("input[data-column='2']").prop("checked")){
                $("#area table div.item").css("display","none");
                $("#area table [data-column='2']").removeAttr("style");
                $("#area table [data-column='1']").css("display","none");
              }else{
                $("#area table div.item").removeAttr("style");
                $("#area table [data-column='2']").css("display","none");
                $("#area table [data-column='1']").css("display","none");
              }
            }
          }
        }
        if(control){
          if(e.prop("checked")){
            $("#area table [data-column='"+column+"']").removeAttr("style");
          }else{
            $("#area table [data-column='"+column+"']").css("display","none");
          }
        }
      }
    }
  }
  $.fn.color_button=function(ele,resp){
    if(resp==1){
      ele.attr("class","btn-circle btn-circle-"+ele.data("color")+" btn-circle-"+ele.data("size"));
    }else{
        ele.attr("class","btn-circle btn-circle-default btn-circle-"+ele.data("size"));
    }
  }
  $.fn.manual=function(){
    $(this).click(function(){
      if($(this).prop("tagName")!="FORM"){
        manual($(this));
      }
    });
    function manual(e){
      if(e.data("module")!=undefined){
        var mywindow = window.open(base+"controls/help/"+e.data("module"),"Almacen","location=no, width=600,height=600",'_brank');
      }
    }
  }
$.fn.isJSON=function(str){
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
$.fn.onfocus=function(){
  if(!esmovil()){$(this).focus();}
}
})(jQuery);
function print_aux(e){e.print();}






function replace_all(cad,search,tag){
    if (cad.length <= 0 && cad.length<search.length){
        return "";
    }else{
        var cad_aux=cad.trim().toLowerCase();
        var pos=cad_aux.indexOf(search);
        if(pos!==-1){
          var cad_new=cad.substr(pos+search.length,cad.length);
          if(pos==0){
            //sub cadena
            var cad_search=cad.substr(0,search.length);
          }else{
            var cad_search=cad.substr(pos,search.length);
          }
          switch(tag){
            case 'mark': cad=cad.substr(0,pos)+"<mark>"+cad_search+"</mark>"; break;
            default: cad=cad.substr(0,pos)+"<mark>"+cad_search+"</mark>"; break;
          }
        }else{
          cad_new="";
        }
        return(cad+replace_all(cad_new,search,tag));
    }
}
function printer(e){e.print();}

/*window.addEventListener("load",function(){
  setTimeout(function(){ 
    window.scrollTo(0,1);
  },0);
});*/
$("#sidebar div.card").css("height",$(this).height()+"px");//para menu de friends chat
//CAMBIA CLASE DE BOTON
$("#btn_21").click(function() { hide_modal("2");});
$(".closed_2").click(function() { hide_modal("2");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_2"){ hide_modal("2");} });

$("#btn_31").click(function() { hide_modal("3");});
$(".closed_3").click(function() { hide_modal("3");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_3"){ hide_modal("3");} });

$("#btn_41").click(function() { hide_modal("4");});
$(".closed_4").click(function() { hide_modal("4");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_4"){ hide_modal("4");} });

$("#btn_51").click(function() { hide_modal("5");});
$(".closed_5").click(function() { hide_modal("5");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_5"){ hide_modal("5");} });

$("#modal_closed_6").click(function() { hide_modal("6");});
$(".closed_6").click(function() { hide_modal("6");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_6"){ hide_modal("6");} });
$("#modal_closed_visor").click(function() { hide_modal("visor");});
$(".closed_visor").click(function() { hide_modal("visor");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_visor"){ hide_modal("visor");} });

$("#btn_71").click(function() { hide_modal("7");});
$(".closed_7").click(function() { hide_modal("7");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_7"){ hide_modal("7");} });

$("#btn_81").click(function() { hide_modal("8");});
$(".closed_8").click(function() { hide_modal("8");});
$(document).click(function(e){ if($(e.target).attr("id")=="modal_8"){ hide_modal("8");} });

function hide_modal(nivel){//cerrar modal bootstrap
  switch(nivel){
    case "1": $("#modal_"+nivel).modal('hide'); break;
    case "11": $("#modal_"+nivel).modal('hide'); break;
    default: $("#modal_"+nivel).fadeOut(250);$("#dialog_"+nivel).fadeOut(150); $("#dialog_"+nivel).css("transform","translateY(-200%)");
    var modal_1=$("#modal_1").attr("style");
    var modal_11=$("#modal_11").attr("style");
    var eliminar=false;
    if(modal_1==undefined && modal_11==undefined){
        eliminar=true;
    }else{
      if(modal_1==undefined){
        if(modal_11.indexOf("display: none")!=-1 || modal_11.indexOf("display:none")!=-1){
          eliminar=true;
        }
      }else{
        if(modal_11==undefined){
          if(modal_1.indexOf("display: none")!=-1 || modal_1.indexOf("display:none")!=-1){
            eliminar=true;
          }
        }else{
          if((modal_1.indexOf("display: none")!=-1 || modal_1.indexOf("display:none")!=-1) && (modal_11.indexOf("display: none")!=-1 || modal_11.indexOf("display:none")!=-1)){
            eliminar=true;
          }
        }
      }
    }
    if(eliminar){ $('body').removeClass('modal-open');}
    break;  
  }
  $("div.popover.fade").remove();
}
function activar(activo,titulo,dir){
  $("ul#menu-top .nav-item a").each(function(el) {
    $(this).removeAttr("class");
  });
  $("ul#menu-top .nav-item a").each(function(el) {
    if($(this).attr("id")==activo){
      $(this).attr("class","nav-link active");
    }else{
      $(this).attr("class","nav-link");
    }
  });
  $("span.label-tag-mobile").html(titulo);
  /*
  var padre=document.getElementById("menu-top").firstChild;//obtenemos todos los elemntos <li>
  while(padre!=null){//recorremos todos los <li>
    if(padre.nodeType==Node.ELEMENT_NODE){
        var hijo=padre.firstChild;
        if(hijo.id==activo){
          padre.className="activado";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }else{
          padre.className="";
          hijos=padre.firstChild;
          hijos=hijos.firstChild;
          while(hijos!=null){
            if(hijos.id=="txt_btn"){
              hijos.className="g-btn-sm";
              break;
            }
            hijos=hijos.nextSibling;
          }
        }
      }
      padre=padre.nextSibling;
    }*/
    url(titulo,dir);
  }
  function url(titulo,url){
    var data={foo: "bar"};
    $(document).attr('title',titulo);
    window.history.replaceState(data,titulo,url);
  }
  function modal(encabezado,ancho,nivel){
    $(".titulo_"+nivel).html(encabezado);
    $("#dialog_"+nivel).attr({"class":"modal-dialog modal-"+ancho});
    if(nivel=="1" || nivel=="11"){
      if(nivel=="1"){ $("#content_11").html("");}
      if(nivel=="11"){ $("#content_1").html("");}
      $("#modal_"+nivel).modal({keyboard:true}); 
    }else{
      $("#modal_"+nivel).fadeIn(150);
      $("#dialog_"+nivel).fadeIn(0);
      $("#dialog_"+nivel).css({"transform":"translateY(0)","visibility":"visible"});
      $('body').addClass('modal-open');
    }
  }
  function btn_modal(funcion_g,atribs_g,funcion_i,atribs_i,nivel){
    clear_datas("btn_"+nivel+"2");/*Eliminando datos de boton*/
    var id_g="btn_"+nivel+"2";
    var id_i="btn_"+nivel+"3";
    if(funcion_g=="" || funcion_g==null){
     $("#"+id_g).css({"display":"none"});
     $("#"+id_g).removeAttr('onclick');
   }else{
    $("#"+id_g).removeAttr("style"); addClick(id_g,funcion_g,atribs_g);
  }
  if(funcion_i=="" || funcion_i==null){
   $("#"+id_i).css({"display":"none"});
   $("#"+id_i).removeAttr('onclick');
 }else{
   $("#"+id_i).removeAttr("style"); addClick(id_i,funcion_i,atribs_i);
 }
}
function addClick(id,funcion,atribs){
  var ele=$("#"+id);
  if(funcion!=""){
      var vatr=atribs.split("|");
      if(vatr.length>0){
        if(vatr[0]=="this"){
          ele.attr({"onclick":funcion+"($(this))"});
          if(vatr.length>1){
            var e=JSON.parse(vatr[1]);
            $.each(e,function(indice,valor){
              ele.attr("data-"+indice,valor);
            });
          }
        }else{
          ele.attr({"onclick":funcion+"("+atribs+")"});
        }
      }
  }else{
    ele.attr({"onclick":""});
  }
}
function addClick_v2(id,funcion,atribs){
  var ele=$("#"+id+" button");
  if(funcion!=""){
    ele.attr({"onclick":funcion+"("+atribs+")"});
  }else{
    ele.attr({"onclick":""});
  }
}

/*mensaje de globos popover bootstrap*/
function alerta(content,align,id){
  $("#"+id).attr({'data-trigger':'focus','data-placement':align,'data-content':content});
  $('#'+id).popover('show');
  $("#"+id).click(function(){ $("#"+id).popover('dispose');});
}

/*--- ver alerta costado superior derecho ---*/
function msj(tipo){
  var img="";
  var clase="";
  switch(tipo){
    case "ok": clase="success"; img=base+"libraries/img/sistema/alert-success.png"; texto="Datos Actualizados con éxito !!!"; break;
    case "fail": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Error en la actualización, revise los datos porfavor..."; break;
    case "error":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Datos no actualizados, problemas de conexion !!!"; break;
    case "problem":  clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="problemas con la conexion al servidor!!!"; break;
    case "validate": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, usuario o contraseña incorrecta !!!"; break;
    case "permiso_bloqueado": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Usted no tiene permiso para realizar la accion, comuníquese con el administrador."; break;
    case "numero_orden_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de orden ya registrado, actualice número de orden !!!"; break;
    case "numero_pedido_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de pedido ya registrado, actualice número de pedido!"; break;
    case "numero_folio_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Datos no actualizados, Número de folio ya registrado, actualice número de orden !!!"; break;
    case "error_type": clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Tipo de archivo no aceptado, verifique el(los) archivo(s)"; break;
    case "error_type_imgs": clase="danger"; img=base+"libraries/img/sistema/alert-danger.png"; texto="Algunas imágenes seleccionada no son aceptadas, verifique que los archivos seleccionados sean imágenes"; break;
    case "error_size_img": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Tamaño de imagen no aceptado, verifique que la imagen tenga un tamaño menor a 1.5MB"; break;
    case "error_size_imgs": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Algunas imágenes seleccionada no son aceptadas, verifique que las imagenes tengan un tamaño menor a 1.5MB"; break;
    case "ci_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El número de CI ya se encuentra registrado en el sistema, revise los datos porfavor..."; break;
    case "ci_exist_directivo": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El número de CI ya se encuentra registrado como directivo en el sistema, revise los datos porfavor..."; break;
    case "name_user_exist": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El nombre de usuario ya en encuentra en uso, ingreso otro nombre de usuario."; break;
    case "user_registrer": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El usuario ya en encuentra registrado."; break;
      case "exists_cuenta": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="El código de la cuenta ya existe"; break;//caso plan de cuentas
      case "file_error": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Imposible leer el archivo, verifique que el archivo no este protegido contra escritura y vuelva a intentarlo."; break;//caso plan de cuentas
      case "file_content_error": clase="warning"; img=base+"libraries/img/sistema/alert-warning.png"; texto="Archivo no admitido, el contenido del archivo no cumple con el formato establecido en el sistema, es decir no tiene al menos 4 columnas y mas de una registro de hora."; break;//caso plan de cuentas
      case "load": clase="loading"; img=base+"libraries/img/sistema/alert-load.gif"; texto="Realizando cambios... espere por favor"; break;
      default: clase="default"; img=base+"libraries/img/sistema/alert-warning.png"; texto=tipo; break;
    }
    if(tipo!="refresh"){
      $("#alerta").stop(); 
      $("#alerta").removeAttr('style');
      $("#alerta").removeAttr('class');
      $("#alerta").css({'opacity':'0.93'});
      $("#alerta").attr({'class':'alert alert-'+clase});
      $("#alerta_text").text(texto);
      $("#alerta_img").attr({'src':img});
      $("#alerta_img").attr({'class':"img-thumbnail img-circle"});
      if(tipo!="load"){
        $("#alerta").fadeOut(7000);
        $("#alerta").hover(function(){ $("#alerta").stop(); $("#alerta").css({'opacity':'.95'});});
      }
    }else{
      if($("#alerta_sistema").attr("style")=="display: none;" || $("#alerta_sistema").attr("style")=="display:none;"){
        $("#alerta_sistema").removeAttr('style');
      }
      $("#alerta_sistema").draggable();
    }
  }
  function close_alerta(id){
    $("#"+id).css({display:'none'});
  }
  /*function imprimir(id){
    var contents = $("#"+id).html();
    var css='<link rel="stylesheet" type="text/css" href="'+base+'libraries/css/final/print.min.css?v=6" media="print">';
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head>'+css+'</head><body>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () { window.frames["frame1"].focus(); window.frames["frame1"].print(); frame1.remove();}, 600);
  }
  function print_this(id){
    /*$("#"+id).printThis({
      loadCSS: base+"libraries/css/imprimir.css",
      removeInline: true
    });*/
    /*var contents = $("#"+id).html();
    var css='<link rel="stylesheet" type="text/css" href="'+base+'libraries/css/final/imprimir.min.css?v=7" media="print">';
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head>'+css+'</head><body>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () { window.frames["frame1"].focus(); window.frames["frame1"].print(); frame1.remove();}, 600);
  }*/
  function Onfocus(id){
    if(!esmovil()){ document.getElementById(id).focus();}
  }
  function OnBlur(id){ 
    if(esmovil()){ $("#"+id).blur(); }
  }
  function foco(id){
    if(!esmovil()){document.getElementById(id+"").focus();}
  }

  //ele.className="";
  //ele.className=";


/*para ingreso y salida de materiales o productos*/
function block(ele,pos){
  var a=ele.value;
  if(!esNumero(a)){ 
    ele.value=0;
  }else{ 
    if($("#i"+pos).val()>0 || $("#s"+pos).val()>0){
      var saldo=parseFloat($("#c"+pos).val())+parseFloat($("#i"+pos).val())-parseFloat($("#s"+pos).val());  
      $("#sa"+pos).val(saldo); 
      if(saldo<0){
        input_invalido('sa'+pos)
      }else{
        input_valido('sa'+pos);
      }
    }
  }
}
function input_valido(id){
  var ele=$("#"+id);
  ele.css({background: "rgba(0,64,0,0.5)",color: "white"});

}
function input_invalido(id){
  var ele=$("#"+id);
  ele.css({background: "rgba(255,0,0,0.5)",color: "white"});
}
function reset_rev_mov(val,id){
  $("#c"+id).val(val);
  $("#i"+id).val(0);
  $("#s"+id).val(0);
  $("#sa"+id).val(val);
  $("#sa"+id).removeAttr('style');
}

/*esconde search*/
function Disabled(id){
  $("#"+id).css({"display":"none"});
}
function Visible(id){
  $("#"+id).removeAttr("style");
}

/*validaciones*/
function alfaNumerico(cad,min,max){
  var val="^[a-z0-9áÁéÉíÍóÓúÚñÑ]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function strSpace(cad,min,max){
  if(cad!=undefined){
    var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ)+-¿.,:(?°;ªº]{"+min+","+max+"}$";
    var expr=new RegExp(val);
    if(cad.match(expr)){
      return true;
    }else{
      return false;
    }    
  }else{
    return false
  }
}

function strNoSpace(cad,min,max){
  if(cad!=undefined){
    var val="^[a-zA-Z0-9áÁéÉíÍóÓúÚñÑ)+-.¿,:(°;?ªº]{"+min+","+max+"}$";
    var expr=new RegExp(val);
    if(cad.match(expr)){
      return true;
    }else{
      return false;
    }
  }else{
    return false
  }
}
function textarea(cad,min,max){
  if(cad!=undefined){
    var val="^[a-zA-Z 0-9áÁéÉíÍóÓúÚñÑ)+-.¿,:(°;?ªº]{"+min+","+max+"}$";
    var expr=new RegExp(val);
    if(cad.match(expr)){
      return true;
    }else{
      return false;
    }
  }else{
    return false
  }
}
function entero(num,min,max){
  if(isset(num) && isset(min) && isset(max)){
    if(esNumero(num)){
      num+="";
      var val="^[0-9]{"+min+","+max+"}$";
      var expr=new RegExp(val);
      if(num.match(expr)){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function decimal(num,maxentero,maxdecimal){
  if(isset(num) && isset(maxentero) && isset(maxdecimal)){
    num+="";
    var val="^[0-9]{1,"+maxentero+"}[.]?[0-9]{0,"+maxdecimal+"}$";
    var expr=new RegExp(val);
    if(num.match(expr)){
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function fecha(fecha){
  if(fecha!=undefined){
    var val="^[0-9]{4,4}[-][0-9]{2,2}[-][0-9]{2,2}$";
    var expr=new RegExp(val);
    if(fecha.match(expr)){
      var vf=fecha.split("-");
      if(parseFloat(vf[0])>=1600 && parseFloat(vf[1])>0 && parseFloat(vf[1])<=12 && parseFloat(vf[2])>0 && parseFloat(vf[2])<=31){
        return true;      
      }else{
        return false;
      }
    }else{
      return false;
    }
  }else{
    return false;
  }
}
function password(cad,min,max){
  var val="^[a-z0-9)+-:(]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function direccion(cad,min,max){
  var val="^[a-z A-Z0-9áÁéÉíÍóÓúÚñÑ/ªº)+-.,:(;]{"+min+","+max+"}$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function email(cad){
  var val="^[_a-z.0-9-.]*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
function webs(cad){
  var val="^(https?:\/\/)?([\da-zA-Z0-9\.-]+)\.([a-z0-9\.]{2,6})([\/\w \?=.-]*)*\/?$";
  var expr=new RegExp(val);
  if(cad.match(expr)){
    return true;
  }else{
    return false;
  }
}
/*validadciones*/
function esNumero(nro){
  if(!isNaN(nro) & nro != null & !/^\s*$/.test(nro) & nro>=0)
    return true;
  return false;
}
function esCadena(cad){
  if(cad == null || cad.length == 0 || /^\s*$/.test(cad))
    return false;
  return true;
}
function isset(val){
  if(typeof val!='undefined'){
    return true;
  }else{
    return false;
  }
}
function number_format(num,des,decimal,miles){
  var res=0;
  if(num!="" && des!=""){
    var dig=10*des;
    res=Math.round(num*dig)/dig;
  }
  return res;
}
/*--- Manejo de cadenas ---*/
function mayuscula(cad) {
  return cad.toUpperCase();
}
function minuscula(cad) {
  return cad.toLowerCase();
}
function view_imagen(e){
  $("#content_6").html("");
  if(e.attr("src")!=undefined && e.data("title")!=undefined && e.data("desc")!=undefined){
    var url=e.attr("src");
    url=url.replace("/miniatura","");
    var titulo=e.data("title");
    var desc=e.data("desc");
    modal("",null,"6");
    //var content='<div class="container-fluid" style="padding:0px;"><div id="my-pics" class="carousel slide" data-ride="carousel" style="width:100%;margin:auto;"><div class="carousel-inner" role="listbox"><div class="carousel-item active"><img src="'+url+'" alt="" width="100%" class="img-thumbnail"><div class="carousel-caption"><h4>'+titulo+'</h4><p>'+desc+'</p></div></div></div></div></div>';
    var content='<div class="container-fluid" style="padding:0px;"><div id="my-pics" class="carousel slide" data-ride="carousel" style="width:100%;margin:auto;"><div class="carousel-inner" role="listbox"><div class="carousel-item active"><div class="modal-header sub-modal-header"><span><span id="title-img">'+titulo+'</span><br><span id="desc-img">'+desc+'</span></span></div><img src="'+url+'" alt="" width="100%"></div></div></div></div>';
    $("#content_6").html(content);
  }
}
  function view_img(e){//llamamos de una archivo a
    var img=e.children('img.img-thumbnail');
    modal("",null,"6");
    var src=img.attr("src");
    imagen=src.replace("/miniatura","");
    var content='<div class="container-fluid" style="padding:0px;"><div id="my-pics" class="carousel slide" data-ride="carousel" style="width:100%;margin:auto;"><div class="carousel-inner" role="listbox"><div class="carousel-item active"><div class="modal-header sub-modal-header"><span><span id="title-img">'+img.data("title")+'</span><br><span id="desc-img">'+img.data("desc")+'</span></span></div><img src="'+imagen+'" alt="" width="100%"></div></div></div></div>';
    $("#content_6").html(content);
  }
  function esmovil(){
    var result=false;
    if(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/iPhone|iPad|iPod/i) || navigator.userAgent.match(/Opera Mini/i) || navigator.userAgent.match(/IEMobile/i)){
      result=true;
    }
    return result;
  }
  function scroll_final(id){
    var div=document.querySelector("#"+id);
    div.scrollTop = "9999";
  }
  function clear_datas(id){
    $("#"+id).removeData();$("#"+id).removeAttributes(null,['href','id','onclick','class']);
  }
  function clear_all_data(e){
    e.removeData();e.removeAttributes(null,['href','id','onclick','class']);
  }
  function clear_data(ele,atr){
    $(ele).removeData(atr);$(ele).removeAttr("data-"+atr);
  }

$.fn.removeAttributes = function(only, except) {
  if (only) {
    only = $.map(only, function(item) {
      return item.toString().toLowerCase();
    });
  };
  if (except) {
    except = $.map(except, function(item) {
      return item.toString().toLowerCase();
    });
    if (only) {
      only = $.grep(only, function(item, index) {
        return $.inArray(item, except) == -1;
      });
    };
  };
  return this.each(function() {
    var attributes;
    if(!only){
      attributes = $.map(this.attributes, function(item) {
        return item.name.toString().toLowerCase();
      });
      if (except) {
        attributes = $.grep(attributes, function(item, index) {
          return $.inArray(item, except) == -1;
        });
      };
    } else {
      attributes = only;
    }      
    var handle = $(this);
    $.each(attributes, function(index, item) {
      handle.removeAttr(item);
    });
  });
};/*
function drop_elemento(e){
  var type=e.data("type");
  var rm_padre=e.data("padre");
  if(rm_padre!=undefined){
    var eliminar=buscar_padre($(e),rm_padre);
    if(eliminar!=null){
      var e=$(eliminar).parent();
      var prod_eli=[];
      if(type!=undefined){
        /*if(type=="empleado_proceso"){//caso empleado
          if(rm_padre=="accordion-panel"){
            $("div#"+eliminar.attr("id")+" table tr.row-proceso").each(function(index,tr){
                if($(tr).data("proe")!=undefined){
                  prod_eli[prod_eli.length]=$(tr).data("proe");
                }
            });
          }
          if(rm_padre=="row-producto"){
            if($(eliminar).attr("id")!=undefined){
              $("tr#"+$(eliminar).attr("id")+" td table tr.row-proceso").each(function(index,tr){
                if($(tr).data("proe")!=undefined){
                  prod_eli[prod_eli.length]=$(tr).data("proe");
                }
              });
            }
          }
          if(rm_padre=="row-proceso"){
            if(eliminar.data("proe")!=undefined){
                prod_eli[prod_eli.length]=eliminar.data("proe");
              }
          }
          pila_procesos(JSON.stringify(prod_eli),"0");
        }*/
     /* }else{//caso pedido
        if(rm_padre=="accordion-panel"){
          $("div#"+eliminar.attr("id")+" table tr.row-producto").each(function(index,tr){
              if($(tr).data("dp")!=undefined){
                prod_eli[prod_eli.length]=$(tr).data("dp");
              }
          });
        }
        if(rm_padre=="row-producto"){
          if(eliminar.data("dp")!=undefined){
            prod_eli[prod_eli.length]=eliminar.data("dp");
          }
        }
        pila_productos(JSON.stringify(prod_eli),"0");
        var tabla=tag_padre($(eliminar),"table");
        if(tabla!=null){
          if(tabla.attr("id")!=undefined){
            var detalles=$("table#"+tabla.attr("id")+" div.detalles-producto");
            if(detalles.length>0){
              pila_sub_productos($("table#"+tabla.attr("id")+" div.detalles-producto"),eliminar.data("dp"),"0");
            }
          }
        }
      }
      $(eliminar).remove();
      if(type!=undefined){

      }else{
        if(rm_padre=="row-producto"){//totales de los productos en el pedido
          var table=tag_padre($(e),"table");
          if(table!=null){
            total_productos($(table).attr("id"));//actuliza cantidad de productos
            var padre_prod=buscar_padre($(table),"accordion-panel");
            var colors=$("div#list_product div#"+$(padre_prod).attr("id")+" table tr.row-producto");
            if(colors.length==0){ $("div#"+$(padre_prod).attr("id")).remove();}
          }
        }
      }
      if((rm_padre=="accordion-panel" || rm_padre=="row-producto") && type==undefined){
          total_costo_venta();
          e.disabled_input_pedido();
      }
      if(rm_padre=="accordion-panel" || rm_padre=="row-producto"){
        e.num_prod_ped();
      }      
    }
  }
}*/
  function buscar_padre(e,padre){
    var control=false;
    if(e.attr("id")!=padre && e.attr("class")!=padre){
      for (var i = 0; i <= 20; i++) {
        e=e.parent();
        if(e.attr("id")==padre || e.attr("class")==padre){
          control=true;
          break;
        }
      }
    }else{
      control=true;
    }
    if(control){
      return e;
    }else{
      return null;
    }
  }

  function buscar_hijo(e,hijo){
    var control=false;
    if(e.attr("id")!=hijo && e.attr("class")!=hijo){
      for (var i = 0; i <= 20; i++) {
        e=e.children();
        console.log(e);
        if(e.attr("id")==hijo || e.attr("class")==hijo){
          control=true;
          break;
        }
      }
    }else{
      control=true;
    }
    if(control){
      return e;
    }else{
      return null;
    }
  }

    /*function pila_procesos(iddps,status){//actualiza el status de los productos
      var p=[];
      if($("span#detalles").html()!=undefined){
        var productos=JSON.parse($("span#detalles").html());
        for(var j = 0; j < productos.length; j++){
          var control=false;
          var eliminados=JSON.parse(iddps);
          for(var i = 0; i < eliminados.length; i++){
            if(eliminados[i]!=undefined){
              if(productos[j]['idproe']==eliminados[i]){ control=true; break; }
            }
          }
          if(control){
            p[p.length]={ "idproe":productos[j]['idproe'],"status":status };
          }else{
            p[p.length]={ "idproe":productos[j]['idproe'],"status":productos[j]['status'] };
          }
        }
        $("span#detalles").html(JSON.stringify(p));
      }
    }*/
    /*Boton circular*/


(function($){
    if(!$.jfab){
        $.jfab = new Object();
    };
    $.jfab.fab = function(el){
        var base = this,mainBtn,subBtns;
        base.$el = $(el);
        base.el = el;
        var toogleAnimation=function(e){
            subBtns.animate({
                opacity: "toggle",
                height: "toggle"
            }, 200);
        };
        var hide=function(e){
          var time=100;
            subBtns.animate({
                opacity: "hide",
                height: "hide"
            }, time);
        };
        base.init=function(){
            mainBtn = base.$el.find("#btn-fab-main");
            subBtns = base.$el.find("#btns-fab-wrapper");
            mainBtn.click(toogleAnimation);
            subBtns.click(hide);
        };
        base.init();
    };
    $.jfab.fab.defaultOptions={};
    $.fn.jqueryFab=function(){
        return this.each(function(){
            (new $.jfab.fab(this));
        });
    };
})(jQuery);
