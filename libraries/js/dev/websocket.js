var FancyWebSocket = function(url){
	var callbacks = {};
	var ws_url = url;
	var conn;
	
	this.bind = function(event_name, callback){
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;
	};
	this.send = function(event_name, event_data){
		this.conn.send( event_data );
		return this;
	};
	this.connect = function() {
		if ( typeof(MozWebSocket) == 'function' )
		this.conn = new MozWebSocket(url);
		else
		this.conn = new WebSocket(url);
		
		this.conn.onmessage = function(evt)
		{
			dispatch('message', evt.data);
		};
		
		this.conn.onclose = function(){dispatch('close',null)}
		this.conn.onopen = function(){dispatch('open',null)}
	};
	
	this.disconnect = function(){
		this.conn.close();
	};
	var dispatch = function(event_name, message){
		if(message == null || message == ""){//aqui es donde se realiza toda la accion
		}
		else{
			var JSONdata = JSON.parse(message); //parseo la informacion
				switch(JSONdata[0].tipo){
					//que tipo de actualizacion vamos a hacer(un nuevo mensaje, solicitud de amistad nueva, etc )
					case '1': actualiza_mensaje(message); break;
					case '2': actualiza_notificacion(message); break;
					case '3': actualiza_update(message); break;
				}
			
			//aqui se ejecuta toda la accion
		}
	}
};
var Server;
function send(text){
    Server.send( 'message', text );
}
$(document).ready(function(){
	Server = new FancyWebSocket('ws://104.236.234.40:8080');
    Server.bind('open', function(){
    });
    Server.bind('close', function( data ) {
    });
    Server.bind('message', function( payload ) {
    });
    Server.connect();
});
function actualiza_mensaje(message){
	var JSONdata = JSON.parse(message); //parseo la informacion
	var destino = JSONdata[0].destino;
	var img_origen = JSONdata[0].img_origen;
	var mensaje = JSONdata[0].mensaje;
	var fecha = JSONdata[0].fecha;
	if($("#showChat_inner").css('display')=="block"){
		if($("#"+destino).html()!=undefined){
			var contenidoDiv  = $("#"+destino).html();
			var mensajehtml  = '<div class="media chat-messages"><a class="media-left photo-table" href="javascript:"><img class="media-object img-circle m-t-5" src="'+img_origen+'" alt="Generic image"></a><div class="media-body chat-menu-content"><div class=""><p class="chat-cont">'+mensaje+'</p></div></div></div>';
			$("#"+destino).html(contenidoDiv+''+mensajehtml);
			scroll_final("msgs");
		}else{
			console.log($("#"+destino).html());
			actualiza_alerta_msj(JSONdata[0].badge_msj);
		}
	}else{
		if($("#sidebar").css('display')=="block"){
			refresh_mensajes_user();
		}
		actualiza_alerta_msj(JSONdata[0].badge_msj);
	}
}
function actualiza_notificacion(message){
	var JSONdata = JSON.parse(message); //parseo la informacion
	actualiza_alerta_notificacion();
}
function actualiza_update(message){
	var JSONdata = JSON.parse(message); //parseo la informacion
	actualiza_alerta_update();
}
