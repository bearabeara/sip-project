var x=$(document);
x.ready(inicio);
(function($){
	$.fn.reset_input=function(){
 		var id=$(this).attr("id");
		if(id!="s_ci"){ $("#s_ci").val("");}
		if(id!="s_nom"){ $("#s_nom").val("");}
		if(id!="s_tel"){ $("#s_tel").val("");}
		if(id!="s_car"){ $("#s_car").val("");}
		if(id!="s_usu"){ $("#s_usu").val("");}
 	}
	$.fn.blur_all=function(id){
		OnBlur("s_ci");
		OnBlur("s_nom");
		OnBlur("s_tel");
		OnBlur("s_car");
		OnBlur("s_usu");
	}
/*------- MANEJO DE USUARIOS -------*/
   	/*--- Buscador ---*/
   	$.fn.search_usuario=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_usuario($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_usuario($(this));
 			}
 		});
 		function search_usuario(e){
 			e.reset_input();
 		}
	}
	$.fn.view_usuarios=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_usuarios($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_usuarios($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_usuarios($(this));
	    	event.preventDefault();
	    });
 		function view_usuarios(e){
			var atrib3=new FormData();
	   		atrib3.append('ci',$("#s_ci").val());
	   		atrib3.append('nom',$("#s_nom").val());
	   		atrib3.append('tel',$("#s_tel").val());
	   		atrib3.append('car',$("#s_car").val());
	   		atrib3.append('usu',$("#s_usu").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('administrador/view_usuario',atrib3,controls);
 		}
	}
   	/*--- End Buscador ---*/
   	/*--- Reportes ---*/
   	$.fn.detalle_usuario=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				detalle_usuario($(this));
			}
		});
		function detalle_usuario(e){
			if(e.data("us")!=undefined){
		   		modal("Detalle","md","1");
		 		btn_modal('',"",'',"",'1');
		 		var atrib=new FormData();
		 		atrib.append('idus',e.data("us"));
			 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('administrador/detalle_usuario',atrib,controls);
			}
		}
	}
   	/*--- End Reportes ---*/
   	/*--- configuracion ---*/
   	$.fn.config_usuario=function(){
		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				config_usuario($(this));
			}
		});
		function config_usuario(e){
			if(e.data("us")!=undefined){
				modal('USUARIO DE SISTEMA: Modificar','xmd','1');
				var atr="this|"+JSON.stringify({us:e.data("us")});
				btn_modal('update_usuario',atr,'',"",'1');
				var atrib=new FormData();
				atrib.append('idus',e.data("us"));
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('administrador/config_usuario',atrib,controls);
			}
		}
   	}
   	$.fn.update_usuario=function(){
   		if($(this).data("us")!=undefined){
			var fot=$('#n_fot').prop('files');
			var ci=$('#n_ci').val();
			var ciudad=$('#n_ciu').val();
			var nom=$('#n_nom1').val();
			var nom2=$('#n_nom2').val();
			var pat=$('#n_pat').val();
			var mat=$('#n_mat').val();
			var tel=$('#n_tel').val();
			var usu=$('#n_usu').val();
			var car=$('#n_car').val();
			var tip=$('#n_tip').val();
			if(entero(ci,6,9) && ci>=100000 && ci<=999999999){
				if(entero(ciudad,0,10)){
					if(strSpace(nom,2,20)){
						if(strSpace(pat,2,20)){
							if(tip=="0" || tip =="1" || tip=="2"){
								if(strNoSpace(usu,4,15)){
									var control=true;
									if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
									if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese Apellido Paterno válido',"top",'n_mat');}}
									if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
									if(tel!=""){ if(!entero(tel,0,15)){ control=false; alerta('Ingrese un número de telefono válido',"top",'n_tel');}}
									if(control){
										var atrib= new FormData();
										atrib.append("idus",$(this).data("us"));
										atrib.append("ci",ci);
										atrib.append("ciu",ciudad);
										atrib.append("nom1",nom);
										atrib.append("nom2",nom2);
										atrib.append("pat",pat);
										atrib.append("mat",mat);
										atrib.append("tel",tel);
										atrib.append("usu",usu);
										atrib.append("car",car);
										atrib.append("tip",tip);
										atrib.append("archivo",fot[0]);
										var atrib3=new FormData();
										atrib3.append('ci',$("#s_ci").val());
										atrib3.append('nom',$("#s_nom").val());
										atrib3.append('tel',$("#s_tel").val());
										atrib3.append('car',$("#s_car").val());
										atrib3.append('usu',$("#s_usu").val());
							 			var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
							 			var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
							 			$(this).set('administrador/update_usuario',atrib,controls1,'administrador/view_usuario',atrib3,controls2);
									}
								}else{
									alerta('Ingrese un valor válido',"top",'n_usu');
								}
							}else{
								alerta('Seleccione un opcion válida',"top",'n_tip');
							}
						}else{
							alerta('Ingrese un apellido válido',"top",'n_pat');
						}
					}else{
						alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
					}
				}else{
					alerta('Seleccione una ciudad','top','n_ciu');
				}
			}else{
				alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
			}
   		}
   	}
   	$.fn.confirm_reset_pass=function(){
   		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_reset_pass($(this));
			}
		});
		function confirm_reset_pass(e){
	   		if(e.data("us")!=undefined){
	   			var text="desactivar";
	   			if(e.data("estado")!="1"){text="activar";}
				modal("Confirmar","xs","5");
			   	var atr="this|"+JSON.stringify({us:e.data("us")});
			 	btn_modal('reset_pass',atr,'',"",'5');
			 	var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
			 	$("#content_5").html('<div class="list-group"><div class="list-group-item" style="max-width:100%"><h5>¿Desea restituir la contraseña del usuario?</h5><span>por defecto la contraseña sera el número de Cedula de identidad del usuario</span></div></div>');
	   		}
		}
   	}
	$.fn.reset_pass=function(){
		if($(this).data("us")!=undefined){
			var atrib=new FormData(); 
			atrib.append('idus',$(this).data("us"));
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"",refresh:false,type:"html"});
			$(this).set('administrador/reset_pass',atrib,controls1,'',{},controls2);
		}
	}
   	/*--- End configuracion ---*/
   	/*--- Eliminar ---*/
   	$.fn.confirm_usuario=function(){
   		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				confirm_usuario($(this));
			}
		});
		function confirm_usuario(e){
			if(e.data("us")!=undefined){
		   		modal("Eliminar","xs","5");
		   		var atr="this|"+JSON.stringify({us:e.data("us")});
		   		btn_modal('drop_usuario',atr,'',"",'5');
		   		var atrib=new FormData();atrib.append('idus',e.data("us"));
				var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
				$(this).get_1n('administrador/confirmar_usuario',atrib,controls);
			}
		}
   	}
   	$.fn.drop_usuario=function(){
   		if($(this).data("us")!=undefined){
			var atrib=new FormData();
			atrib.append('u',$("#e_user").val());
			atrib.append('p',$("#e_password").val());
			atrib.append('idus',$(this).data("us"));
			var atrib3=new FormData();
			atrib3.append('ci',$("#s_ci").val());
			atrib3.append('nom',$("#s_nom").val());
			atrib3.append('tip',$("#s_tip").val());
			atrib3.append('car',$("#s_car").val());
			atrib3.append('usu',$("#s_usu").val());
			var controls1=JSON.stringify({type:"set",preload:true,closed:"5"});
			var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).set('administrador/drop_usuario',atrib,controls1,'administrador/view_usuario',atrib3,controls2);
   		}
	}
   	/*--- End Eliminar ---*/
   	/*--- Nuevo ---*/
   	$.fn.new_usuario=function(){
   		$(this).click(function(){
			if($(this).prop("tagName")!="FORM"){
				new_usuario($(this));
			}
		});
		function new_usuario(e){
			modal('USUARIO DE SISTEMA: Nuevo','xmd','1');
			btn_modal('save_usuario',"this",'',"",'1');
			var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
			$(this).get_1n('administrador/new_usuario',{},controls);
		}
   	}
   	$.fn.search_ci=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				search_ci($(this));
 			}
 		});
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_ci($(this));
 			}
 		});
 		$(this).on("mousewheel",function(e){
 			if($(this).prop("tagName")!="FORM"){
 				e.preventDefault();
 			}
 		});
 		function search_ci(e){
	   		var ci=e.val();
	   		if(ci!=""){
		   		var inputs="n_ciu|n_nom1|n_nom2|n_pat|n_mat|n_car|n_tel";
		   		var atrib=new FormData();
		   		atrib.append('ci',ci);
		   		get_input('administrador/search_ci',atrib,"NULL",inputs,false);
	   		}
 		}
   	}
   	$.fn.save_usuario=function(){
		var fot=$('#n_fot').prop('files');
		var ci=$('#n_ci').val();
		var ciudad=$('#n_ciu').val();
		var nom=$('#n_nom1').val();
		var nom2=$('#n_nom2').val();
		var pat=$('#n_pat').val();
		var mat=$('#n_mat').val();
		var tel=$('#n_tel').val();
		var usu=$('#n_usu').val();
		var car=$('#n_car').val();
		var tip=$('#n_tip').val();
		if(entero(ci,6,9) && ci>=100000 && ci<=999999999){
			if(entero(ciudad,0,10)){
				if(strSpace(nom,2,20)){
					if(strSpace(pat,2,20)){
						if(tip=="0" || tip =="1" || tip=="2"){
							if(password(usu,4,15)){
								var control=true;
								if(nom2!=""){ if(!strSpace(nom2,2,20)){ control=false; alerta('Ingrese un nombre válido',"top",'n_nom2');}}
								if(mat!=""){ if(!strSpace(mat,2,20)){ control=false; alerta('Ingrese Apellido Paterno válido',"top",'n_mat');}}
								if(car!=""){ if(!strSpace(car,0,100)){ control=false; alerta('Ingrese un contenido válido',"top",'n_car');}}
								if(tel!=""){ if(!entero(tel,0,15)){ control=false; alerta('Ingrese un número de telefono válido',"top",'n_tel');}}
								if(control){
									var atrib=new FormData();
									atrib.append("ci",ci);
									atrib.append("ciu",ciudad);
									atrib.append("nom1",nom);
									atrib.append("nom2",nom2);
									atrib.append("pat",pat);
									atrib.append("mat",mat);
									atrib.append("tel",tel);
									atrib.append("usu",usu);
									atrib.append("car",car);
									atrib.append("tip",tip);
									atrib.append("archivo",fot[0]);
									var atrib3=new FormData();
									atrib3.append('ci',$("#s_ci").val());
									atrib3.append('nom',$("#s_nom").val());
									atrib3.append('tel',$("#s_tel").val());
									atrib3.append('car',$("#s_car").val());
									atrib3.append('usu',$("#s_usu").val());
									var controls1=JSON.stringify({type:"set",preload:true,closed:"1"});
									var controls2=JSON.stringify({id:"contenido",refresh:true,type:"html"});
									$(this).set('administrador/save_usuario',atrib,controls1,'administrador/view_usuario',atrib3,controls2);
								}
							}else{
								alerta('Ingrese un valor válido',"top",'n_usu');
							}
						}else{
							alerta('Seleccione un opcion válida',"top",'n_tip');
						}
					}else{
						alerta('Ingrese un apellido válido',"top",'n_pat');
					}
				}else{
					alerta('El campo de primer nombre es obligatorio',"top",'n_nom1');
				}
			}else{
				alerta('Seleccione una ciudad','top','n_ciu');
			}
		}else{
			alerta('Ingrese un nro de Carnet Válido',"top",'n_ci');
		}
   	}
   	/*--- End Nuevo ---*/
   	/*--- Imprimir ---*/
   	$.fn.print_usuarios=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				print_usuarios($(this));
 			}
 		});
 		function print_usuarios(e){
 			if(e.data("tbl")!=undefined){
 				modal("USUARIOS: Configuracion de impresion","lg","1");
	 			var atr="this|"+JSON.stringify({"area":"div#area","tools":"div#tools-print"});
		 		btn_modal('',"",'print_aux',atr,'1');
		 		var atrib=new FormData();
		 		var visibles=[];
		 		$(e.data("tbl")+" tbody tr").each(function(id,tr){
		 			if($(tr).attr("style")==undefined && $(tr).data("us")!=undefined){
		 				visibles[visibles.length]=$(tr).data("us");
		 			}
		 		});
		 		atrib.append('visibles',JSON.stringify(visibles));
		 		atrib.append('tbl',e.data("tbl"));
				atrib.append('ci',$("#s_ci").val());
				atrib.append('nom',$("#s_nom").val());
				atrib.append('tel',$("#s_tel").val());
				atrib.append('car',$("#s_car").val());
				atrib.append('usu',$("#s_usu").val());
		 		var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('administrador/print_usuarios',atrib,controls);
 			}
 		}
   	}
   	/*--- End Imprimir ---*/
   	function img_usuario(idus){
	    modal("",null,"6");
	    var atrib=new FormData();atrib.append('idus',idus);
	    get('administrador/img_usuario',atrib,'content_6',true);
	}

/*------- END MANEJO DE USUARIOS -------*/
/*------- MANEJO DE PRIVILEGIOS -------*/
   	/*--- Buscador ---*/
   	$.fn.search_privilegio=function(){
 		$(this).keyup(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_privilegio($(this));
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				search_privilegio($(this));
 			}
 		});
 		function search_privilegio(e){
 			e.reset_input();
 		}
	}
	$.fn.view_privilegios=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM" && $(this).prop("tagName")!="SELECT"){
 				if($(this).data("type")!=undefined){
 					if($(this).data("type")=="all"){
 						$(this).reset_input();
 					}
 					view_privilegios($(this));
 				}
 			}
 		});
 		$(this).change(function(){
 			if($(this).prop("tagName")!="FORM"){
 				view_privilegios($(this));
 			}
 		});
	    $(this).submit(function(event){
	    	view_privilegios($(this));
	    	event.preventDefault();
	    });
 		function view_privilegios(e){
			var atrib3=new FormData();
	   		atrib3.append('ci',$("#s_ci").val());
	   		atrib3.append('nom',$("#s_nom").val());
		 	var controls=JSON.stringify({id:"contenido",refresh:true,type:"html"});
			$(this).get_1n('administrador/view_privilegio',atrib3,controls);
 		}
	}

   	/*--- End Buscador ---*/
   	/*--- Nuevo ---*/
	$.fn.update_privilegio=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				update_privilegio($(this));
 			}
 		});
 		function update_privilegio(e){
			var atrib=new FormData();
			atrib.append("idpri",e.data("pri"));
			atrib.append("col",e.data("col"));
			var controls1=JSON.stringify({type:"set",preload:true});
			var controls2=JSON.stringify({id:"",refresh:false,type:"html"});
			e.set_button('administrador/update_privilegio',atrib,controls1,"",{},controls2);
 		}
	}
	$.fn.detail_modal=function(){
 		$(this).click(function(){
 			if($(this).prop("tagName")!="FORM"){
 				detail_modal($(this));
 			}
 		});
 		function detail_modal(e){
 			if(e.data("pri")!=undefined && e.data("col")!=undefined){
				switch(e.data("col")){
					case "al": var v="almacen"; break;
					case "pr": var v="produccion"; break;
					case "mo": var v="movimientos"; break;
					case "ca": var v="capital humano"; break;
					case "cl": var v="cliente/proveedor"; break;
					case "ac": var v="activos fijos"; break;
					case "ot": var v="otros materiales e insumos"; break;
					case "co": var v="contabilidad"; break;
					case "ad": var v="administracion"; break;
				}
		   		modal("PRIVILEGIOS: Detalle de "+v,"lg","1");
		 		btn_modal('',"",'',"",'1','lg');
				var atrib=new FormData();
				atrib.append('idpri',e.data("pri"));
				atrib.append('col',e.data("col"));
				var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
				$(this).get_1n('administrador/detail_modal',atrib,controls);
 			}
 		}
	}
   	/*--- End Nuevo ---*/
/*------- END MANEJO DE PRIVILEGIOS -------*/
/*------- MANEJO DE CONFIGURACIONES -------*/
   	/*--- Actualizacion ---*/
    $.fn.extend({
      new_actualizacion: function(){
	 	modal("Nueva actualizacion de sistema","md","1");
	 	btn_modal('save_actualizacion',"this",'',"",'1');
	 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
	 	$(this).get_1n('administrador/new_actualizacion',{},controls);
        return false;
      }
    });
    $.fn.extend({
      new_config_usuario: function(){
      	if($(this).data("contenedor")!=undefined){
      		var asignados=[];
			$("div.list-group-item table tbody#content_user tr.row-user").each(function(index,tr){
				if($(tr).data("u")!=undefined){ asignados[asignados.length]=$(tr).data("u");}
			});
		 	modal("Seleccionar usuarios","sm","2");
		 	btn_modal('',"",'',"",'2');
		 	var atrib=new FormData();atrib.append("content",$(this).data("contenedor"));atrib.append("asignados",JSON.stringify(asignados));
		 	var controls=JSON.stringify({id:"content_2",refresh:true,type:"html"});
		 	$(this).get_1n('administrador/new_config_usuario',atrib,controls);
	        return false;
      	}
      }
    });
    $.fn.extend({
      add_user_config: function(){
      	if($(this).data("u")!=undefined && $("div#datos_config_cliente").data("content")!=undefined){
		 	var atrib=new FormData();atrib.append("u",$(this).data("u"));
		 	var controls=JSON.stringify({id:$("div#datos_config_cliente").data("content"),refresh:false,type:"append"});
		 	$(this).get_1n('administrador/add_user_config',atrib,controls);
	        return false;
      	}
      }
    });
    $.fn.extend({
      	save_actualizacion: function(){
	      	var ini=$("input#n_ini").val().trim();
	      	var fin=$("input#n_fin").val().trim();
	      	var des=$("textarea#n_des").val().trim();
	      	var est=$("select#n_est").val().trim();
	      	if(ini!="" && ini!=undefined){
	      		if(fin!="" && fin!=undefined){
	      			if(textarea(des,5,1000)){
	      				if(est=="0" || est=="1" || est=="2"){
	      					if($("div.list-group-item table tbody#content_user tr.row-user").length>0){
					      		var asignados=[];
								$("div.list-group-item table tbody#content_user tr.row-user").each(function(index,tr){
									if($(tr).data("u")!=undefined){ asignados[asignados.length]=$(tr).data("u");}
								});
	      						var atrib=new FormData();atrib.append("ini",ini);atrib.append("fin",fin);atrib.append("des",des);atrib.append("est",est);atrib.append("asignados",JSON.stringify(asignados));
						      	var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
						 		var controls1=JSON.stringify({id:"contenido",refresh:false,type:"html"});
						      	$(this).set("administrador/save_actualizacion",atrib,controls,'administrador/view_config',{},controls1);
	      					}else{
	      						alerta("Seleccione al menos un usuario.","top","btn_new_usuario");
	      					}
	      				}else{
	      					alerta("Seleccione una opción válida","top","n_est");
	      				}
	      			}else{
	      				alerta("Ingrese un contenido válido","top","n_des");
	      			}
	      		}else{
	      			alerta("Ingrese una fecha válida","top","n_fin");
	      		}
	      	}else{
	      		alerta("Ingrese una fecha válida","top","n_ini");
	      	}
	        return false;
      	}
    });
    $.fn.extend({
      change_actualizacion:function(){
      	if($(this).data("ac")!=undefined){
      		modal("Modificar actualizacion","md","1");
      		var atr="this|"+JSON.stringify({ac:$(this).data("ac")});
		 	btn_modal('update_actualizacion',atr,'',"",'1');
		 	var controls=JSON.stringify({id:"content_1",refresh:true,type:"html"});
		 	var atrib=new FormData();atrib.append("ac",$(this).data("ac"));
		 	$(this).get_1n('administrador/change_actualizacion',atrib,controls);
	        return false;
      	}
      }
    });
    $.fn.extend({
      	update_actualizacion: function(){
	      	var ini=$("input#n_ini").val().trim();
	      	var fin=$("input#n_fin").val().trim();
	      	var des=$("textarea#n_des").val().trim();
	      	var est=$("select#n_est").val().trim();
	      	var users=$("div.list-group-item table tbody#content_user tr.row-user");
	      	var users_delete=$("span#users_db");
	      	if(ini!="" && ini!=undefined){
	      		if(fin!="" && fin!=undefined){
	      			if(textarea(des,5,1000)){
	      				if(est=="0" || est=="1" || est=="2"){
	      					if(users.length>0){
	      						if(users_delete.html()!=undefined && $(this).data("ac")!=undefined){
						      		var nuevos=[];
									$("div.list-group-item table tbody#content_user tr.row-user").each(function(index,tr){
										if($(tr).data("u")!=undefined && $(tr).data("save")=="new"){ nuevos[nuevos.length]=$(tr).data("u");}
									});
									//console.log(users_delete.html());
		      						var atrib=new FormData();atrib.append("ini",ini);atrib.append("fin",fin);atrib.append("des",des);atrib.append("est",est);atrib.append("nuevos",JSON.stringify(nuevos)); atrib.append("users_delete",users_delete.html());atrib.append("ac",$(this).data("ac"));
							      	var controls=JSON.stringify({type:"set",preload:true,closed:"1"});
							 		var controls1=JSON.stringify({id:"contenido",refresh:false,type:"html"});
							      	$(this).set("administrador/update_actualizacion",atrib,controls,'administrador/view_config',{},controls1);
	      						}else{
	      							msj("¡Error, varible de sistema no encontrada, actualice la página por favor.");
	      						}
	      					}else{
	      						alerta("Seleccione al menos un usuario.","top","btn_new_usuario");
	      					}
	      				}else{
	      					alerta("Seleccione una opción válida","top","n_est");
	      				}
	      			}else{
	      				alerta("Ingrese un contenido válido","top","n_des");
	      			}
	      		}else{
	      			alerta("Ingrese una fecha válida","top","n_fin");
	      		}
	      	}else{
	      		alerta("Ingrese una fecha válida","top","n_ini");
	      	}
	        return false;
      	}
    });
    $.fn.extend({
    	confirmar_actualizacion: function(){
    		if($(this).data("ac")!=undefined){
	    		modal("Eliminar","xs","5");
	    		var atr="this|"+JSON.stringify({ac:$(this).data("ac")});
		   		btn_modal('drop_actualizacion',atr,'',"",'5');
		   		var atrib=new FormData();atrib.append('ac',$(this).data("ac"));
		   		var controls=JSON.stringify({id:"content_5",refresh:true,type:"html"});
		   		$(this).get_1n('administrador/confirmar_actualizacion',atrib,controls);
    		}
    	}
    });
    $.fn.extend({
    	drop_actualizacion: function(){
    		if($(this).data("ac")!=undefined){
    			var atrib=new FormData();atrib.append("ac",$(this).data("ac"));
				var controls=JSON.stringify({type:"set",preload:true,closed:"5"});
				var controls1=JSON.stringify({id:"contenido",refresh:false,type:"html"});
				$(this).set("administrador/drop_actualizacion",atrib,controls,'administrador/view_config',{},controls1);
    		}
    	}
    });
    $.fn.extend({
      drop_this_elemento: function(){
      	if($(this).data("padre")!=undefined){
      		var eliminar=buscar_padre($(this),$(this).data("padre"));
      		if(eliminar!=null){
      			var control=false;
      			if($(this).data("padre")=="row-user"){
      				if($(eliminar).data("save")!=undefined && $("span#users_db").html()!=undefined){
      					if($(eliminar).data("save")=="update"){
      						if($(eliminar).data("au")!=undefined){
      							$(this).pila_users_db("span#users_db",$(eliminar).data("au"));
      						}
      					}
      				}
      			}
      			var e=$(eliminar).parent();
      			$(eliminar).remove();
      		}
      	}
      }
    });
    $.fn.extend({
      pila_users_db:function(dir,idau){
      	if($(dir).html()!=undefined){
      		var users=$(dir).html();
	      	console.log(users);
	      	var users=JSON.parse(users);
	      	var result=[];
	      	for(var i = 0; i<users.length; i++){
				if(users[i]!=undefined){
					var status=users[i].status;
					if(users[i].idau==idau){
						status="0";
					}
					result[result.length]={idau:users[i].idau, status:status};
				}
			}
			$(dir).html(JSON.stringify(result));
      	}
      }
  	});
    /*--- end actualizacion ---*/
/*------- END MANEJO DE CONFIGURACIONES -------*/
	/*$.fn.update_privilegio=function(){
		$(this).click(function(){
		  	if($(this).prop("tagName")!="FORM"){
		  		update_privilegio($(this));
		  	}
		});
		function update_privilegio(e){
			if(e.data("pri")!=undefined && e.data("col")!=undefined && e.data("color")!=undefined && e.data("size")!=undefined){
				var atrib=new FormData();
				atrib.append("idpri",e.data("pri"));
				atrib.append("col",e.data("col"));
				set_button('administrador/update_privilegio',atrib,"",{},"",false,e);
			}
		}
	}*/
})(jQuery);
function update_usuario(e){e.update_usuario();}
function drop_usuario(e){e.drop_usuario();}
function save_usuario(e){e.save_usuario();}





function inicio(){
	$('a#usuario').click(function(){$(this).get_2n('administrador/search_usuario',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_usuario',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("usuario","Usuarios de sistema","administrador?p=1");});
	$('div#usuario').click(function(){$(this).get_2n('administrador/search_usuario',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_usuario',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("usuario","Usuarios de sistema","administrador?p=1");});
	$('a#privilegio').click(function(){$(this).get_2n('administrador/search_privilegio',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_privilegio',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("privilegio","Privilegios de usuario","administrador?p=2");});
	$('div#privilegio').click(function(){$(this).get_2n('administrador/search_privilegio',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_privilegio',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("privilegio","Privilegios de usuario","administrador?p=2");});
	$('a#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de usuario","administrador?p=7");});
	$('div#config').click(function(){$(this).get_2n('',{},JSON.stringify({id:"search",refresh:true,type:"html"}),'administrador/view_config',{},JSON.stringify({id:"contenido",refresh:true,type:"html"}));activar("config","Configuración de usuario","administrador?p=7");});
}








function save_actualizacion(){ $(this).save_actualizacion();}
function update_actualizacion(e){ e.update_actualizacion();}
function drop_actualizacion(e){ e.drop_actualizacion();}
function reset_pass(e){e.reset_pass();}









/*------- MANEJO DE CONFIGURACION -------*/
   	/*--- Paises ---*/
   	function save_pais(){
   		var p=$("#p").val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('p',p);
   			set('administrador/save_pais',atrib,'NULL',true,'administrador/view_config',{},'contenido',true);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p');
   		}
   		return false;
   	}
   	function update_pais(idpa) {
   		var p=$("#p"+idpa).val();
   		if(strSpace(p,2,100)){
   			var atrib=new FormData();
   			atrib.append('idpa',idpa);
   			atrib.append('p',p);
   			set('administrador/update_pais',atrib,'NULL',true,'administrador/view_config',{},'contenido',true);
   		}else{
   			alerta('Ingrese un nombre de pais válido','top','p'+idpa);
   		}
   		return false;
   	}
   	function alerta_pais(idpa){
		modal("Eliminar","xs","5");
   		btn_modal('drop_pais',"'"+idpa+"'",'',"",'5');
   		var atrib=new FormData();atrib.append('idpa',idpa);
   		get('administrador/alerta_pais',atrib,'content_5',true);
   	}
   	function drop_pais(idpa) {
   		var atrib=new FormData();atrib.append('idpa',idpa);
   		set('administrador/drop_pais',atrib,'NULL',true,'administrador/view_config',{},'contenido',true,'5');
   		return false;
   	}
   	/*--- End Paises ---*/
   	/*--- Ciudades ---*/
   	function save_ciudad() {
   		var ciu=$("#c").val();
   		var sig=$("#s").val();
   		var pac=$("#cp").val();
   		if(strSpace(ciu,2,100)){
   			if(strNoSpace(sig,1,4)){
   				if(entero(pac,0,10)){
		   			var atrib=new FormData();atrib.append('ciu',ciu);atrib.append('sig',sig);atrib.append('pac',pac);
		   			set('administrador/save_ciudad',atrib,'NULL',true,'administrador/view_config',{},'contenido',true);
	   			}else{
	   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp');
	   			}
   			}else{
   				alerta('Ingrese una sigla válida','top','s');
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c');
   		}
   		return false;
   	}
   	function update_ciudad(idci){
   		var ciu=$("#c"+idci).val();
   		var sig=$("#s"+idci).val();
   		var pac=$("#cp"+idci).val();
   		if(strSpace(ciu,2,100)){
   			if(strNoSpace(sig,1,4)){
   				if(entero(pac,0,10)){
		   			var atrib=new FormData();atrib.append('idci',idci);atrib.append('ciu',ciu);atrib.append('sig',sig);atrib.append('pac',pac);
		   			set('administrador/update_ciudad',atrib,'NULL',true,'administrador/view_config',{},'contenido',true);
	   			}else{
	   				alerta('Seleccione un pais donde pertenece la ciudad','top','cp'+idci);
	   			}
   			}else{
   				alerta('Ingrese una sigla válida','top','s'+idci);
   			}
   		}else{
   			alerta('Ingrese un nombre de ciudad válido','top','c'+idci);
   		}
   		return false;
   	}
   	function alerta_ciudad(idci){
		modal("Eliminar","xs","5");
   		btn_modal('drop_ciudad',"'"+idci+"'",'',"",'5');
   		var atrib=new FormData();atrib.append('idci',idci);
   		get('administrador/alerta_ciudad',atrib,'content_5',true);
   	}
   	function drop_ciudad(idci){
   		var atrib=new FormData();atrib.append('idci',idci);
   		set('administrador/drop_ciudad',atrib,'NULL',true,'administrador/view_config',{},'contenido',true,'5');
   		return false;
   	}
   	/*--- End Ciudades ---*/
/*------- END MANEJO DE CONFIGURACION -------*/